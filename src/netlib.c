/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/**
 * \file netlib.c
 *
 * \brief Library with commonly used network structures and functions.
 *
 * In this file are grouped some commonly used data structures and functions
 * related to a networking.
 *
 * Window handling routines
 *
 * Create new window structure
 * Initialize windows size
 * Check if there are free IDs
 * Obtain ID for request to be sent
 * Check if there is corresponding request for received ack/response
 * Commit received ack/response (i.e. update window)
 */

#define __NETLIB_C

#define LOGGERNAME	"netlib"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <linux/ipsec.h>
#include <netpacket/packet.h>

#include <glib.h>

#include "logging.h"
#include "netlib.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * WINDOW CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/

/**
 * Allocate and initialize new state variables for window
 */
struct nl_win *nl_win_new(void)
{
	struct nl_win *win;

	LOG_FUNC_START(1);

	win = g_malloc0(sizeof(struct nl_win));

	win->wsize = 1;

	LOG_FUNC_END(1);
	return win;
}

/**
 * Deallocate state variables for window
 */
void nl_win_free(struct nl_win *win)
{
	LOG_FUNC_START(1);

	g_free(win);

	LOG_FUNC_END(1);
}

/*******************************************************************************
 * WINDOW GETTERS AND SETTERS
 ******************************************************************************/

void nl_win_set_wsize(struct nl_win *nl_win, guint16 wsize)
{
	nl_win->wsize = wsize;
}

guint16 nl_win_get_wsize(struct nl_win *nl_win)
{
	return nl_win->wsize;
}

void nl_win_set_lo(struct nl_win *nl_win, guint32 lo)
{
	nl_win->lo = lo;
}

guint32 nl_win_get_lo(struct nl_win *nl_win)
{
	return nl_win->lo;
}

void nl_win_set_hi(struct nl_win *nl_win, guint32 hi)
{
	nl_win->hi = hi;
}

guint32 nl_win_get_hi(struct nl_win *nl_win)
{
	return nl_win->hi;
}

/*******************************************************************************
 * WINDOW MANIPULATION FUNCTIONS
 ******************************************************************************/

/**
 * Check if window is open
 *
 * @param nl_win
 *
 * \return TRUE there are free IDs, FALSE window is full
 */
guint32 nl_win_open(struct nl_win *nl_win)
{
	return (nl_win->hi - nl_win->lo < nl_win->wsize);
}

/**
 * Get next available ID
 *
 * @param nl_win
 *
 * \return ID for the next packet
 *
 * This function doesn't check nor it has possibility to return error value,
 * so be sure to first check if there are free IDs with function nl_win_check
 * 
 */
guint32 nl_win_get_next_id(struct nl_win *nl_win)
{
	return nl_win->hi++;
}

/**
 * For a given ACK determine if it's expected and update window if it is
 * 
 * @param nl_win
 * @param msgid
 *
 * \return TRUE This ACK has been expected, FALSE it was not expected
 */
gboolean nl_win_commit_ack(struct nl_win *nl_win, guint32 msgid)
{
	/*
	 * First, check if we are inside currently active window...
	 */
	if (nl_win->lo <= msgid && msgid < nl_win->hi) {

		/*
		 * Next, check if we already received response to this
		 * request...
		 */
		if (nl_win->bitfield[(msgid % MAX_WIN_SIZE) >> 5] &
				(1 << ((msgid % MAX_WIN_SIZE) & 0x1F)))
			return FALSE;

		/*
		 * We are inside a window and we didn't receive response
		 * to this one so mark it...
		 */
		nl_win->bitfield[(msgid % MAX_WIN_SIZE) >> 5] |=
				(1 << ((msgid % MAX_WIN_SIZE) & 0x1F));

		/*
		 * Try to increase lower window bound...
		 *
		 * Note that we do not check against hi during increase since
		 * this information is indirectly written inside bit field...
		 */
		while (nl_win->bitfield[(nl_win->lo % MAX_WIN_SIZE) >> 5] &
				(1 << ((nl_win->lo % MAX_WIN_SIZE) & 0x1F))) {
			nl_win->bitfield[(nl_win->lo % MAX_WIN_SIZE) >> 5] &=
					~(1 << ((nl_win->lo % MAX_WIN_SIZE)
						& 0x1F));
			nl_win->lo++;
		}

		return TRUE;
	}

	return FALSE;
}

/*******************************************************************************
 * NETADDR CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/

/**
 * Allocate and initialize a new netaddr structure
 *
 * \return Pointer to a structure or NULL in case of an error
 */
struct netaddr *netaddr_new()
{
	struct netaddr *netaddr;

	LOG_FUNC_START(1);

	netaddr = g_malloc0(sizeof(struct netaddr));

	netaddr->refcnt = 1;

	LOG_FUNC_END(1);

	return netaddr;
}

/**
 * Make read-only copy of a network address...
 *
 * \return Pointer to a duplicate structure or NULL in case of an error
 */
struct netaddr *netaddr_dup_ro(struct netaddr *addr)
{
	g_atomic_int_inc(&addr->refcnt);

	return addr;
}

/**
 * Duplicate existing netaddr structure
 *
 * \return Pointer to a duplicate structure or NULL in case of an error
 */
struct netaddr *netaddr_dup(struct netaddr *addr)
{
	struct netaddr *na = NULL;

	if (addr) {
		na = g_memdup(addr, sizeof(struct netaddr));
		na->refcnt = 1;
	} else
		LOG_DEBUG("Trying to dup NULL address!");

	return na;
}

/**
 * Allocate and initialize a new netaddr structure from in_addr structure!
 *
 * @param addr
 *
 * \return Pointer to a structure or NULL in case of an error
 */
struct netaddr *netaddr_new_from_inaddr(struct in_addr *addr)
{
	struct netaddr *netaddr;

	LOG_FUNC_START(1);

	netaddr = netaddr_new();

	netaddr->prefix = 32;
	netaddr->sa.sa_family = AF_INET;
	memcpy(&(netaddr->sin.sin_addr.s_addr), addr,
			sizeof(struct in_addr));

	LOG_FUNC_END(1);

	return netaddr;
}

/**
 * Allocate and initialize a new netaddr structure from in6_addr structure!
 *
 * @param addr
 *
 * \return Pointer to a structure or NULL in case of an error
 */
struct netaddr *netaddr_new_from_in6addr(struct in6_addr *addr)
{
	struct netaddr *netaddr;

	LOG_FUNC_START(1);

	netaddr = netaddr_new();

	netaddr->prefix = 128;
	netaddr->sa.sa_family = AF_INET6;
	memcpy(&(netaddr->sin6.sin6_addr.s6_addr), addr,
			sizeof(struct in6_addr));

	LOG_FUNC_END(1);

	return netaddr;
}

/**
 * Remove the address from list of addresses
 *
 * @param addr Address to be removed
 * @param list List of addresses from which to remove the address
 *
 * \return Pointer to the head of the new list
 */
GSList *netaddr_remove_from_list(struct netaddr *addr, GSList *list) {

	GSList *ptr, *retval;

	LOG_FUNC_START(1);

	retval = ptr = list;
	while(ptr) {
		if(!netaddr_cmp_ip2ip(addr, (struct netaddr *)ptr->data)) {
			if(retval == ptr)
				retval = ptr->next;
			ptr = g_slist_delete_link(ptr, ptr);
		}
		else
			ptr = ptr->next;
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Allocate and initialize a new netaddr structure from char array.
 *
 * @param addr_buf	Buffer received in ADDITIONAL_IP4_ADDRESS notification
 * @param family	Address family
 *
 * \return Pointer to a structure or NULL in case of an error
 */
struct netaddr *netaddr_new_from_char(unsigned char *addr_buf, gint family) {

	struct netaddr *netaddr = NULL;
	struct sockaddr_in ip4addr;
	struct sockaddr_in6 ip6addr;
	char tmp_str[48];
	char hex_token[4];
	int i;

	LOG_FUNC_START(1);

	memset(tmp_str, 0, sizeof(tmp_str));
	if (family == AF_INET) {
		sprintf(tmp_str, "%d.%d.%d.%d",
				addr_buf[0], addr_buf[1], addr_buf[2], addr_buf[3]);
		inet_pton(AF_INET, tmp_str, &ip4addr.sin_addr);
		netaddr = netaddr_new_from_inaddr(&ip4addr);
		netaddr_set_port(netaddr, 500);
	}
	else if (family == AF_INET6) {
		for(i = 0; i < 15; ++i) {
			sprintf(hex_token, "%x:", addr_buf[i]);
			strcat(tmp_str, hex_token);
		}
		sprintf(hex_token, "%x", addr_buf[15]);
		strcat(tmp_str, hex_token);
		inet_pton(AF_INET6, tmp_str, &ip6addr.sin6_addr);
		netaddr = netaddr_new_from_in6addr(&ip6addr);
	}

	LOG_FUNC_END(1);

	return netaddr;
}

/**
 * Free netaddr structure
 *
 * @param netaddr	Pointer to a structure
 */
void netaddr_free(struct netaddr *netaddr)
{
	if (netaddr && g_atomic_int_dec_and_test(&(netaddr->refcnt))) {

		if (netaddr->ifname)
			g_free(netaddr->ifname);

		g_free(netaddr);
	}
}

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/

/**
 * Get network prefix size from address
 *
 * @param netaddr	Network address
 *
 * \return Prefix size in bits
 */
guint8 netaddr_get_prefix(struct netaddr *netaddr)
{
	return netaddr->prefix;
}

/**
 * Set network prefix size from address
 *
 * @param netaddr	Network address
 * @param prefix	Prefix size in bits
 */
void netaddr_set_prefix(struct netaddr *netaddr, guint8 prefix)
{
	netaddr->prefix = prefix;
}

/**
 * Get interface name from the address
 *
 * @param netaddr	Network address
 *
 * \return Read-only pointer to a ASCIIZ interface name
 */
gchar *netaddr_get_ifname(struct netaddr *netaddr)
{
	return netaddr ? netaddr->ifname : NULL;
}

/**
 * Set interface name for the address
 *
 * @param netaddr	Network address
 * @param ifname	ASCIIZ name of the interface
 *
 * This function will copy argument, so the caller has to free it if
 * it's not necessary any more.
 */
void netaddr_set_ifname(struct netaddr *netaddr, gchar *ifname)
{
	netaddr->ifname = g_strdup(ifname);
}

/**
 * Get port from socket address
 *
 * @param addr	Socket address
 *
 * \return port	Port in networka address in host byte order
 */
guint16 netaddr_get_port(struct netaddr *addr)
{
	if (addr)
		switch(addr->sa.sa_family) {
		case AF_INET:
			return ntohs(addr->sin.sin_port);

		case AF_INET6:
			return ntohs(addr->sin6.sin6_port);
			break;

		default:
			LOG_BUG("family not set on addr structure");
			break;
		}

	return 0;
}

/**
 * Set port in network address
 *
 * @param addr	Network address
 * @param port	Port to set in host byte order
 */
void netaddr_set_port(struct netaddr *addr, guint16 port)
{
	switch(addr->sa.sa_family) {
	case AF_INET:
		addr->sin.sin_port = htons(port);
		break;

	case AF_INET6:
		addr->sin6.sin6_port = htons(port);
		break;

	default:
		LOG_BUG("family not set on addr structure");
		break;
	}
}

/**
 * Set IP address in network address
 *
 * @param addr		Network address
 * @param sin_addr	IPv6 (sin6_addr) or IPv4 (in_addr) structure
 *
 * Note that before calling this function you have to declare addr's
 * family by calling netaddr_set_family funtcion. This function takes
 * a copy of sin_addr region of memory.
 */
void netaddr_set_ipaddr(struct netaddr *addr, void *sin_addr)
{
	switch(addr->sa.sa_family) {
	case AF_INET:
		memcpy(&addr->sin.sin_addr, sin_addr, 4);
		break;

	case AF_INET6:
		memcpy(&addr->sin6.sin6_addr, sin_addr, 16);
		break;

	default:
		LOG_BUG("family not set on addr structure");
		break;
	}
}

/**
 * Get IP address in network address
 *
 * @param addr		Network address
 *
 * \return IPv6 (sin6_addr) or IPv4 (in_addr) structure, NULL in
 * case of an error
 *
 * Pointer returned by this function should not be changed or free'd
 */
void *netaddr_get_ipaddr(struct netaddr *addr)
{
	switch(addr->sa.sa_family) {
	case AF_INET:
		return &addr->sin.sin_addr;

	case AF_INET6:
		return &addr->sin6.sin6_addr;
		break;

	default:
		LOG_BUG("family not set on addr structure");
		return NULL;
	}
}

/*******************************************************************************
 * NETWORK ADDRESS COMPARING FUNCTIONS
 ******************************************************************************/

/**
 * Compare two IP addresses.
 *
 * @param addr1	First IP address.
 * @param addr2	Second IP address
 *
 * \return	-1 if addr1 < addr2,
 *		0 if addr1 == addr2 
 *		1 if addr1 > addr2 
 *
 * Note: Caller has to assure that both network addresses belong to
 * same families
 */
gint netaddr_cmp_ip2ip(struct netaddr *addr1, struct netaddr *addr2)
{
	if (addr1->sa.sa_family == AF_INET)
		return memcmp(&addr1->sin.sin_addr, &addr2->sin.sin_addr,
				sizeof(struct in_addr));
	else
		return memcmp(&addr1->sin6.sin6_addr, &addr2->sin6.sin6_addr,
				sizeof(struct in6_addr));
}

/**
 * Compare IP and network addresses.
 *
 * @param addr	IP address.
 * @param net	IP Network address
 *
 * \return 1 if they are not equal, 0 if they are equal
 */
gint netaddr_cmp_net2ip(struct netaddr *addr, struct netaddr *net)
{
	int addrsize;
	guint32 addr1, addr2;

	LOG_FUNC_START(2);

	if (addr->sa.sa_family != net->sa.sa_family)
		return 1;

	if (addr->sa.sa_family == AF_INET) {
		addr1 = addr->sin.sin_addr.s_addr & ipv4_netmasks[net->prefix];
		addr2 = net->sin.sin_addr.s_addr & ipv4_netmasks[net->prefix];
		return (addr1 != addr2);
	}

	if (addr->sa.sa_family == AF_INET6) {
		addrsize = sizeof(struct in6_addr);
		/*
		 * TODO: Compare IPv6 addresses
		 */
		return -1;
	}

	/*
	 * Control should not reach this point. Log an error!
	 */
	LOG_BUG("End of a function reached");

	return 1;
}

/**
 * Check if two addresses belong to the same address family
 *
 * @param addr1
 * @param addr2
 *
 * \return	1 If they are same family (both AF_INET, or both AF_INET6)
 *		0 If ther are in different families
 */
gint netaddr_same_family(struct netaddr *addr1, struct netaddr *addr2)
{
	return addr1->sa.sa_family == addr2->sa.sa_family;
}

/*******************************************************************************
 * MISC NETWORK ADDRESS FUNCTIONS
 ******************************************************************************/

/**
 * Return size of netaddr structure
 *
 * \return Size of netaddr strucutre
 */
gint netaddr_size(void)
{
	return sizeof(struct netaddr);
}

/**
 * Return the actual size of address used in netaddr structure.
 *
 * @param addr	Network address
 *
 * \return Size or -1 in case of an error.
 */
gint netaddr_get_ipaddr_size(struct netaddr *addr)
{
	if (addr->sa.sa_family == AF_INET)
		return 4;

	if (addr->sa.sa_family == AF_INET6)
		return 16;

	return -1;
}

/**
 * Return the pointer to sockaddr structure.
 *
 * @param addr	Network address
 *
 * \return Pointer to sockaddr structure...
 */
struct sockaddr *netaddr_get_sa(struct netaddr *addr)
{
	return (struct sockaddr *)addr;
}

/**
 * Return the actual size of sockaddr structure.
 *
 * @param addr	Network address
 *
 * \return Size or -1 in case of an error.
 */
gint netaddr_get_sa_size(struct netaddr *addr)
{
	if (addr->sa.sa_family == AF_INET)
		return sizeof(struct sockaddr_in);

	if (addr->sa.sa_family == AF_INET6)
		return sizeof(struct sockaddr_in6);

	if (addr->sa.sa_family == AF_PACKET)
		return sizeof(struct sockaddr_ll);

	return -1;
}

/**
 * Return the address family of network address.
 *
 * @param addr	Network address
 *
 * \return Address family
 */
gint netaddr_get_family(struct netaddr *addr)
{
	if (addr)
		return addr->sa.sa_family;

	return AF_UNSPEC;
}

/**
 * Set address family of network address.
 *
 * @param addr		Network address
 * @param family	Address family
 *
 */
void netaddr_set_family(struct netaddr *addr, gint family)
{
	if (addr)
		addr->sa.sa_family = family;
}

/**
 * Translate network address to character representation.
 *
 * @param addr		Address to convert
 * @param dst		Destination buffer
 * @param cnt		Destination buffer size
 *
 * \return
 *
 * TODO: This function should replace all the other
 * analogous functions! dst should be larger than
 * INET6_ADDRSTRELN because it also holds ports
 */
char *netaddr2str(struct netaddr *addr, char *dst, socklen_t cnt)
{
	const char *retval;
	char port[7];
	char dsst[INET6_ADDRSTRLEN + 7];

	if (!addr)
		return NULL;

	switch (addr->sa.sa_family) {
	case AF_INET:
		retval = inet_ntop(netaddr_get_family(addr),
					netaddr_get_ipaddr(addr),
					dst, cnt);

		if (retval && netaddr_get_port(addr)) {

			snprintf(port, 7, ":%u", netaddr_get_port(addr));

			if (cnt > strlen(dst) + strlen(port))
				strcat(dst, port);
		}
		break;
	case AF_INET6:
		retval = inet_ntop(netaddr_get_family(addr),
					netaddr_get_ipaddr(addr),
					dst, cnt);

		if (retval && netaddr_get_port(addr)) {
			strcpy(dsst, "\[");
			strcat(dsst, dst);
			snprintf(port, 7, ":%u", netaddr_get_port(addr));
			if (cnt > strlen(dst) + strlen(port)){
				strcat(dsst, "]");
				strcat(dsst, port);
				strcpy(dst, dsst);
			}
		}

		break;
	case AF_PACKET:
		dst[0] = 0;
		break;
	default:
		dst[0] = 0;
		LOG_BUG("Unsuported address family");
	}

	return dst;
}

/**
 * Translate IP address to character representation.
 *
 * @param addr		Address to convert
 * @param dst		Destination buffer
 * @param cnt		Destination buffer size
 *
 * \return Returns pointer to the  destination buffer, or NULL
 *	 	in case of an error
 */
const char *netaddr_ip2str(struct netaddr *addr, char *dst, socklen_t cnt)
{
	const char *retval;
	char port[7];
	gint offset = 0;

	if (addr->sa.sa_family == AF_INET)
		retval = inet_ntop(AF_INET, &(addr->sin.sin_addr),
				dst, cnt);
	else {

		if (netaddr_get_port(addr)) {
			offset = 1;
			dst[0] = '[';
		}

		retval = inet_ntop(AF_INET6, &(addr->sin6.sin6_addr),
				dst + offset, cnt - offset);
	}

	if (retval && netaddr_get_port(addr)) {

		snprintf(port, 7, ":%u", netaddr_get_port(addr));

		if (offset && cnt > strlen(dst) + 1)
			strcat(dst, "]");

		if (cnt > strlen(dst) + strlen(port) + 1)
			strcat(dst, port);
	}

	return retval;
}

/**
 * Translate network address to character representation.
 *
 * @param net		Network address to convert
 * @param dst		Destination buffer
 * @param cnt		Destination buffer size
 *
 * \return
 */
const char *netaddr_net2str(struct netaddr *net, char *dst, socklen_t cnt)
{
	char mask[5];

	memset(dst, 0, cnt);

	if (net->sa.sa_family == AF_INET)
		inet_ntop(AF_INET, &(net->sin.sin_addr), dst, cnt);
	else
		inet_ntop(AF_INET6, &(net->sin6.sin6_addr), dst, cnt);

	snprintf(mask, 5, "/%d", net->prefix);

	if (strlen(dst) < (cnt + 4))
		strcat (dst, mask);

	return dst;
}

/**
 * Translate character IP address representation into numerical representation.
 *
 * @param af		Address family. If AF_UNSPEC is given then we
 *			try first AF_INET, then AF_INET6 and if none
 *			succeeds, we return error notification.
 * @param src		Pointer to beginning of character representation
 *
 * \return Pointer to new netaddr structure, or NULL in case of an error.
 *
 * We also check if netmask is given, and if it is, we parse it.
 */
struct netaddr *netaddr_new_from_str(int af, const char *src)
{
	struct netaddr *na = NULL;
	char *netmask;
	struct in_addr in;
	struct in6_addr in6;

	if ((netmask = strchr(src, '/')))
		*netmask = 0;

	if (af == AF_UNSPEC || af == AF_INET) {
		if (inet_pton(AF_INET, src, &in) < 0) {
			if (af == AF_INET) {
				if (netmask)
					*netmask = '/';
					return NULL;
			}
		} else
			na = netaddr_new_from_inaddr(&in);
	}

	if ((af == AF_UNSPEC || af == AF_INET6) && !na) {
		if (inet_pton(AF_INET6, src, &in6) < 0) {
			if (af == AF_INET6) {
				if (netmask)
					*netmask = '/';
					return NULL;
				}
		} else
			na = netaddr_new_from_in6addr(&in6);
	}

	if (netmask) {
		if (na)
			na->prefix = atoi(netmask + 1);

		*netmask = '/';
	}

	return na;
}

/**
 * Return netmask for a given network prefix. Works only for IPv4 addresses
 *
 * @param prefix	Network prefix
 *
 * TODO: Family parameter should be introduced as an argument to this
 * function!
 */
guint32 netaddr_get_netmask(guint8 prefix)
{
	if (prefix <= 32)
		return ipv4_netmasks[prefix];

	LOG_BUG("Incorrect prefix (%u) handled to function netaddr_get_netmask",
		prefix);

	return 256;
}

struct netaddr *netaddr_calc_prefix(struct netaddr *addr1,
			struct netaddr *addr2)
{
	struct netaddr *netaddr;
	guint32 ipv4_addr1, ipv4_addr2;
	int i, mask;

	netaddr = netaddr_new();

	if (addr1->sa.sa_family != addr2->sa.sa_family) {
		LOG_ERROR("Addresses are not of the same family!");
		return NULL;
	}

	if (addr1->sa.sa_family == AF_INET) {

		netaddr->sin.sin_family = AF_INET;

		ipv4_addr1 = htonl(addr1->sin.sin_addr.s_addr);
		ipv4_addr2 = htonl(addr2->sin.sin_addr.s_addr);
		netaddr->sin.sin_addr.s_addr = ntohl(ipv4_addr1 & ipv4_addr2);

		if (addr1->sin.sin_addr.s_addr != addr2->sin.sin_addr.s_addr) {
			/*
			 * Calculate network mask...
			 */
			for (i = 0, mask = 32; i <= 32; i++) {
				if ((ipv4_addr1 & 1) == (ipv4_addr2 & 1))
					break;

				mask--;
				ipv4_addr1 >>= 1;
				ipv4_addr2 >>= 1;
			}
			netaddr->prefix = mask;
		} else
			netaddr->prefix = 32;

	} else {
		printf ("UNIMPLEMENTED: %s:%s:%d\n", __FILE__, __FUNCTION__,
				__LINE__);
		netaddr_free (netaddr);
	}

	return netaddr;
}

/**
 * Free a list of netaddr structures
 *
 * @param list		Pointer to a list head
 */
void netaddr_free_list(GSList *list)
{
	while (list) {
		if (list->data)
			netaddr_free(list->data);

		list = g_slist_remove(list, list->data);
	}
}

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/

#warning "Remove this function from IKEv2"
void netaddr_dump(char *loggername, struct netaddr *netaddr)
{
}

void netaddr_dump_sockaddr(char *loggername, struct sockaddr *sa)
{
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	char ipaddr[256];

	if (sa->sa_family == AF_INET) {
		sin = (struct sockaddr_in *)sa;
		inet_ntop(sa->sa_family, &sin->sin_addr, ipaddr, 255);
		if (sin->sin_port)
			ikev2_log(loggername, LOG_PRIORITY_DEBUG,
				"%s:%d\n", ipaddr, ntohs(sin->sin_port));
		else
			ikev2_log(loggername, LOG_PRIORITY_DEBUG,
				"%s\n", ipaddr);
	} else {
		sin6 = (struct sockaddr_in6 *)sa;
		inet_ntop(sa->sa_family, &sin6->sin6_addr, ipaddr, 255);
		if (sin6->sin6_port)
			ikev2_log(loggername,
				LOG_PRIORITY_DEBUG, "%s:%d\n",
				ipaddr, ntohs(sin6->sin6_port));
		else
			ikev2_log(loggername, LOG_PRIORITY_DEBUG,
				"%s\n", ipaddr);
	}

	return;
}
