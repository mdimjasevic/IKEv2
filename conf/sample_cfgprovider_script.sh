#!/bin/bash

# This is a sample script on how to configure responder's
# side in roadwarrior scenario

# Interface connecing us to protected network
PROTECTED_NET_IF=vmnet1

if [ -z "$1" ]; then
	echo "Usage: $0 <up|down>"
	exit 1
fi

case "$1" in
up)
	echo "ADDING CONFIGURATION PARAMETERS FOR A CLIENT"

	echo "IPv4 address $IKEV2_IPV4_ADDR"
	echo "IPv6 address $IKEV2_IPV6_ADDR"
	echo "Interface to access road warrior $IKEV2_INTERFACE"
	echo "VPN gateway's public IP address $IKEV2_VPNGW_IPV4_ADDR"
	echo "Client's public IPv4 address $IKEV2_RW_IPV4_ADDR"
	echo "Client's public IPv6 address $IKEV2_RW_IPV6_ADDR"

	# We add route via loopback address since it's not used anyway
	# and this is the only way to make VPN gateway do proxyarp
	/sbin/ip route add $IKEV2_IPV4_ADDR via $IKEV2_VPNGW_IPV4_ADDR \
			dev $IKEV2_INTERFACE

	echo 1 > /proc/sys/net/ipv4/ip_forward
	echo 1 > /proc/sys/net/ipv4/conf/$PROTECTED_NET_IF/proxy_arp

	exit 0
	;;
down)
	echo "REMOVING CONFIGURATION PARAMETERS FOR A CLIENT"

	echo "IPv4 address $IKEV2_IPV4_ADDR"
	echo "IPv6 address $IKEV2_IPV6_ADDR"
	echo "Interface to access road warrior $IKEV2_INTERFACE"
	echo "VPN gateway's public IP address $IKEV2_VPNGW_IPV4_ADDR"
	echo "Client's public IPv4 address $IKEV2_RW_IPV4_ADDR"
	echo "Client's public IPv6 address $IKEV2_RW_IPV6_ADDR"

	for i in $IKEV2_IPV4_ADDR
	do
		/sbin/ip route del $i via $IKEV2_VPNGW_IPV4_ADDR
	done

	exit 0
	;;
*)
	echo "Unrecognized parameter"
	exit 1;
esac
