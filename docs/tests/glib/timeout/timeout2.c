/*
 * This code tests how precise are GLib's timeout mechanisms.
 *
 * To measure precision, timeout is started called every interval
 * during total_time period. For each call only sample is taken
 * and control returned back to GLib. If enough samples has been
 * collected then main loop is terminated. Afterwards, all the
 * samples are shown, as well as average value.
 *
 * The difference between this test and the previous one (test1.c)
 * is that here we add some amount of wait time in timeout
 * handler. This way we simulate some havier processing and test
 * GLib's behavior in that case.
 */

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

GMainLoop *main_loop;
glong sample = 0;
glong max_samples;
guint source_id;
GTimeVal *samples;
glong sleep_time;

gboolean timeout_func(gpointer data)
{
	g_get_current_time(&samples[sample++]);

	if (sample == max_samples) {
		g_main_loop_quit(main_loop);
		return FALSE;
	}

	g_usleep(sleep_time);

	return TRUE;
}

int main(int argc, char **argv)
{
	int interval;
	int total_time;
	glong tv_usec, tv_sec, count, total_usec;
	GTimeVal start, stop;

	/*
	 * Check commmand line arguments...
	 */
	if (argc != 4) {
		fprintf (stderr, "Usage: %s <interval> <total time> <sleep time>\n",
				argv[0]);
		return 1;
	}

	/*
	 * Parse command line arguments...
	 */
	interval = atoi(argv[1]);
	total_time = atoi(argv[2]);
	sleep_time = atoi(argv[3]) * 1000;

	max_samples = (total_time + interval - 1) / interval;

	printf ("Test duration %u miliseconds\n", total_time);
	printf ("Interval set to %u miliseconds\n", interval);
	printf ("Sleep time set to %u miliseconds\n", sleep_time / 1000);
	printf ("Expected number of samples %u\n", max_samples);

	samples = g_malloc0(sizeof(GTimeVal) * max_samples);

	/*
	 * Initialize GLib's main loop
	 */
	if ((main_loop = g_main_loop_new(NULL, FALSE)) == NULL) {
		fprintf (stderr, "Error creating new main loop");
		return 1;
	}

	if ((source_id = g_timeout_add(interval, timeout_func, NULL)) < 0) {
		fprintf (stderr, "Error adding new timeout");
		return 1;
	}

	/*
	 * Start measurement
	 */
	g_get_current_time(&start);
	g_main_loop_run(main_loop);
	g_get_current_time(&stop);

	/*
	 * Print data...
	 */
	if (stop.tv_usec < start.tv_usec) {
		stop.tv_usec = start.tv_usec - stop.tv_usec;
		stop.tv_sec--;
	} else
		stop.tv_usec -= start.tv_usec;

	stop.tv_sec -= start.tv_sec;

	printf ("\n\n");
	printf ("Total running time: %u sec %u usecs\n\n", stop.tv_sec, stop.tv_usec);

	for (count = 1; count < sample; count++) {

		tv_usec = samples[count].tv_usec;
		tv_sec = samples[count].tv_sec;

		if (tv_usec < samples[count - 1].tv_usec) {
			tv_usec = 1000000 - samples[count - 1].tv_usec + tv_usec;
			tv_sec--;
		} else
			tv_usec -= samples[count - 1].tv_usec;

		tv_sec -= samples[count - 1].tv_sec;

		total_usec += tv_usec;
		total_usec += tv_sec * 1000000;

		printf ("Sample[%u] %u sec %u usecs (abs %u sec %u usec)\n",
			count, tv_sec, tv_usec,
			samples[count - 1].tv_sec, samples[count - 1].tv_usec);
	}

	sample--;
	printf ("Total usecs %u, samples %u, average %u\n",
			total_usec, sample, total_usec / sample);

	g_free(samples);

	return 0;
}
