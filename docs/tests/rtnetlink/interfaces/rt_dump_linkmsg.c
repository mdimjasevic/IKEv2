/**
 * The code in this file demonstrates how to list all the interfaces
 * in the system as well as how to obtain their addresses.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <net/if.h>

#define BUFFER_SIZE	10240
#define MAX_ADDR_LEN	64

void rt_dump_linkmsg(struct nlmsghdr *nlh)
{
	int len;
	struct ifinfomsg *ifinfomsg;
	struct rtattr *rta;
	struct net_device_stats *nds;

	printf ("nlmsghdr->nlmsg_len=%u\n", nlh->nlmsg_len);
	printf ("nlmsghdr->nlmsg_type= ");
	switch(nlh->nlmsg_type) {
	case RTM_NEWLINK:
		printf ("RTM_NEWLINK\n");
		break;
	case RTM_DELLINK:
		printf ("RTM_DELLINK\n");
		break;
	case RTM_GETLINK:
		printf ("RTM_GETLINK\n");
		break;
	default:
		printf ("unknown\n");
		return;
	}

	printf ("nlmsghdr->nlmsg_flags=");
	if (nlh->nlmsg_flags) {
		if (nlh->nlmsg_flags & NLM_F_REQUEST)
			printf ("NLM_F_REQUEST ");
		if (nlh->nlmsg_flags & NLM_F_MULTI)
			printf ("NLM_F_MULTI ");
		if (nlh->nlmsg_flags & NLM_F_ACK)
			printf ("NLM_F_ACK ");
		if (nlh->nlmsg_flags & NLM_F_ECHO)
			printf ("NLM_F_ECHO ");
		if (nlh->nlmsg_flags & NLM_F_ROOT)
			printf ("NLM_F_ROOT ");
		if (nlh->nlmsg_flags & NLM_F_MATCH)
			printf ("NLM_F_MATCH ");
		if (nlh->nlmsg_flags & NLM_F_ATOMIC)
			printf ("NLM_F_ATOMIC ");
		if (nlh->nlmsg_flags & NLM_F_DUMP)
			printf ("NLM_F_DUMP ");
		if (nlh->nlmsg_flags & NLM_F_REPLACE)
			printf ("NLM_F_REPLACE ");
		if (nlh->nlmsg_flags & NLM_F_EXCL)
			printf ("NLM_F_EXCL ");
		if (nlh->nlmsg_flags & NLM_F_CREATE)
			printf ("NLM_F_CREATE ");
		if (nlh->nlmsg_flags & NLM_F_APPEND)
			printf ("NLM_F_APPEND");
	} else
		printf ("(none)");
	printf ("\n");

	printf ("nlmsghdr->nlmsg_seq=%u\n", nlh->nlmsg_seq);
	printf ("nlmsghdr->nlmsg_pid=%u\n", nlh->nlmsg_pid);

	len = nlh->nlmsg_len - NLMSG_LENGTH(0);

	ifinfomsg = NLMSG_DATA(nlh);

	printf ("\tifinfomsg->ifi_family=%u\n", ifinfomsg->ifi_family);
	printf ("\tifinfomsg->ifi_type=%04X\n", ifinfomsg->ifi_type);
	printf ("\tifinfomsg->ifi_index=%u\n", ifinfomsg->ifi_index);

	printf ("\tifinfomsg->ifi_flags=");
	if (ifinfomsg->ifi_flags & IFF_UP)		// Interface is running.
		printf ("UP ");
	if (ifinfomsg->ifi_flags & IFF_BROADCAST)	// Valid broadcast address set.
		printf ("BROADCAST ");
	if (ifinfomsg->ifi_flags & IFF_DEBUG)	// Internal debugging flag.
		printf ("DEBUG ");
	if (ifinfomsg->ifi_flags & IFF_LOOPBACK)	// Interface is a loopback interface.
		printf ("LOOPBACK ");
	if (ifinfomsg->ifi_flags & IFF_POINTOPOINT)	// Interface is a point-to-point link.
		printf ("POINTTOPOINT ");
	if (ifinfomsg->ifi_flags & IFF_RUNNING)	// Resources allocated.
		printf ("RUNNING ");
	if (ifinfomsg->ifi_flags & IFF_NOARP)	// No arp protocol, L2 destination address not set.
		printf ("NOARP ");
	if (ifinfomsg->ifi_flags & IFF_PROMISC)	// Interface is in promiscuous mode.
		printf ("PROMISC ");
	if (ifinfomsg->ifi_flags & IFF_NOTRAILERS)	// Avoid use of trailers.
		printf ("NOTRAILERS ");
	if (ifinfomsg->ifi_flags & IFF_ALLMULTI)	// Receive all multicast packets.
		printf ("ALLMULTI ");
	if (ifinfomsg->ifi_flags & IFF_MASTER)	// Master of a load balancing bundle.
		printf ("MASTER ");
	if (ifinfomsg->ifi_flags & IFF_SLAVE)	// Slave of a load balancing bundle.
		printf ("SLAVE ");
	if (ifinfomsg->ifi_flags & IFF_MULTICAST)	// Supports multicast
		printf ("MULTICAST ");
	if (ifinfomsg->ifi_flags & IFF_PORTSEL)	// Is able to select media type via ifmap.
		printf ("PORTSEL ");
	if (ifinfomsg->ifi_flags & IFF_AUTOMEDIA)	// Auto media selection active.
		printf ("AUTOMEDIA ");
	if (ifinfomsg->ifi_flags & IFF_DYNAMIC)	// The  addresses  are lost when the interface goes down.
		printf ("DYNAMIC ");
	printf ("\n");

	/*
	 * This is uncertain wether this is correctly decoded...
	 */
	printf ("\tifinfomsg->ifi_change=");
	if (ifinfomsg->ifi_change != 0xFFFFFFFF) {
		if (ifinfomsg->ifi_flags & IFF_UP)		// Interface is running.
			printf ("UP ");
		if (ifinfomsg->ifi_flags & IFF_BROADCAST)	// Valid broadcast address set.
			printf ("BROADCAST ");
		if (ifinfomsg->ifi_flags & IFF_DEBUG)	// Internal debugging flag.
			printf ("DEBUG ");
		if (ifinfomsg->ifi_flags & IFF_LOOPBACK)	// Interface is a loopback interface.
			printf ("LOOPBACK ");
		if (ifinfomsg->ifi_flags & IFF_POINTOPOINT)	// Interface is a point-to-point link.
			printf ("POINTTOPOINT ");
		if (ifinfomsg->ifi_flags & IFF_RUNNING)	// Resources allocated.
			printf ("RUNNING ");
		if (ifinfomsg->ifi_flags & IFF_NOARP)	// No arp protocol, L2 destination address not set.
			printf ("NOARP ");
		if (ifinfomsg->ifi_flags & IFF_PROMISC)	// Interface is in promiscuous mode.
			printf ("PROMISC ");
		if (ifinfomsg->ifi_flags & IFF_NOTRAILERS)	// Avoid use of trailers.
			printf ("NOTRAILERS ");
		if (ifinfomsg->ifi_flags & IFF_ALLMULTI)	// Receive all multicast packets.
			printf ("ALLMULTI ");
		if (ifinfomsg->ifi_flags & IFF_MASTER)	// Master of a load balancing bundle.
			printf ("MASTER ");
		if (ifinfomsg->ifi_flags & IFF_SLAVE)	// Slave of a load balancing bundle.
			printf ("SLAVE ");
		if (ifinfomsg->ifi_flags & IFF_MULTICAST)	// Supports multicast
			printf ("MULTICAST ");
		if (ifinfomsg->ifi_flags & IFF_PORTSEL)	// Is able to select media type via ifmap.
			printf ("PORTSEL ");
		if (ifinfomsg->ifi_flags & IFF_AUTOMEDIA)	// Auto media selection active.
			printf ("AUTOMEDIA ");
		if (ifinfomsg->ifi_flags & IFF_DYNAMIC)	// The  addresses  are lost when the interface goes down.
			printf ("DYNAMIC ");
	} else
		printf ("(none)");
	printf ("\n");

	rta = (struct rtattr *)(ifinfomsg + 1);

	while (RTA_OK(rta, len)) {
		switch(rta->rta_type) {
		case IFLA_UNSPEC:
			printf("IFA_UNSPEC\n");
			break;
		case IFLA_ADDRESS:
			printf ("\t\tAddress:\n");
			break;
		case IFLA_BROADCAST:
			printf ("\t\tBroadcast address:\n");
			break;
		case IFLA_IFNAME:
			printf ("\t\t%s\n", (char *)RTA_DATA(rta));
			break;
		case IFLA_MTU:
			printf ("\t\tMTU: %u\n", (unsigned short int *)RTA_DATA(rta));
			break;
		case IFLA_LINK:
			printf ("\t\tLink type: %08X\n", (unsigned int *)RTA_DATA(rta));
			break;
		case IFLA_QDISC:
			printf ("\t\tQdisc: %s\n", (char *)RTA_DATA(rta));
			break;
		case IFLA_STATS:
			printf ("\t\tStats (undecoded)\n");
			break;
		}
		rta = RTA_NEXT(rta, len);
	}
}
