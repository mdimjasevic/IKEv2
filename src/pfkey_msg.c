/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __PFKEY_MSG_C

#define LOGGERNAME	"pfkey_msg"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#include <stdlib.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "ts.h"
#include "transforms.h"
#include "proposals.h"
#include "config.h"
#include "crypto.h"
#include "pfkey_msg.h"

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE PFKEY_MSG STRUCTURE
 ******************************************************************************/

/**
 * Allocate memory for pfkey subsystem message.
 *
 * \return Pointer to a new message structure or NULL in case of an error.
 */
struct pfkey_msg *pfkey_msg_new(void)
{
	struct pfkey_msg *msg;

	LOG_FUNC_START(1);

	msg = g_malloc0(sizeof(struct pfkey_msg));

	msg->msg_type = PFKEY_MSG;

	LOG_FUNC_END(1);

	return msg;
}

/**
 * Free pfkey message...
 *
 * @param msg
 */
void pfkey_msg_free(struct pfkey_msg *msg)
{

	LOG_FUNC_START(1);

	if (msg) {
		switch (msg->action) {
		case SADB_ACQUIRE:
			if (msg->acquire.saddr)
				netaddr_free(msg->acquire.saddr);

			if (msg->acquire.daddr)
				netaddr_free(msg->acquire.daddr);

			break;

		case SADB_EXPIRE:
			if (msg->expire.saddr)
				netaddr_free(msg->expire.saddr);

			if (msg->expire.daddr)
				netaddr_free(msg->expire.daddr);

			break;

		case SADB_X_SPDGET:
		case SADB_X_SPDDUMP:

			if (msg->spd.tsi)
				ts_free(msg->spd.tsi);

			if (msg->spd.tsr)
				ts_free(msg->spd.tsr);

			if (msg->spd.tunnel_saddr)
				netaddr_free(msg->spd.tunnel_saddr);

			if (msg->spd.tunnel_daddr)
				netaddr_free(msg->spd.tunnel_daddr);

			break;
		}

		g_free (msg);
	}

	LOG_FUNC_END(1);
}

/**
 * Dump pfkey message...
 *
 * @param msg
 */
void pfkey_msg_dump(struct pfkey_msg *msg)
{
	char netaddr_str[128];

	LOG_FUNC_START(1);

	if (msg) {

		switch (msg->action) {
		case SADB_ACQUIRE:

			LOG_DEBUG("Dumping ACQUIRE message");

			if (msg->acquire.saddr) {
				netaddr_net2str(msg->acquire.saddr,
						netaddr_str, 128);
				LOG_DEBUG("Source address: %s", netaddr_str);
			}

			if (msg->acquire.daddr) {
				netaddr_net2str(msg->acquire.daddr,
							netaddr_str, 128);
				LOG_DEBUG("Destination address: %s",
						netaddr_str);
			}

			break;

		case SADB_EXPIRE:
		case SADB_X_SPDGET:
		case SADB_X_SPDDUMP:
			break;
		}

		LOG_DEBUG("seq=%d", msg->seq);
		LOG_DEBUG("pid=%d", msg->pid);
		LOG_DEBUG("errno_val=%d", msg->errno_val);
	}

	LOG_FUNC_END(1);
}
