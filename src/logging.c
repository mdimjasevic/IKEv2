/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __LOGGING_C

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <errno.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <time.h>
#include <sys/time.h>

#include <glib.h>

/*
 * TODO: We can not include syslog.h since there is overlap between
 * constants defined there and in this file. Until this is resolved,
 * i.e. we rename logging macros, some parts of syslog are copied
 * here.
 */
#include "syslog.h"

#include "logging.h"
#include "netlib.h"
#include "config.h"

#ifndef DISABLE_LOGGING

static struct config_log *logging = NULL;
GSList *buffered_items = NULL;

/*******************************************************************************
 * FUNCTION THAT MANIPULATE CONFIGURATION STRUCTURES
 ******************************************************************************/
config_log_t *config_log_new(void)
{
	config_log_t *cl;

	cl = g_malloc0(sizeof(struct config_log));
	cl->facility = -1;

	return cl;
}

void config_log_free(config_log_t *cl)
{
}

void config_log_add_config_log_item(config_log_t *cl, config_log_item_t *cli)
{
	cl->log_items = g_slist_append(cl->log_items, cli);
}

config_log_item_t *config_log_item_new(void)
{
	return g_malloc0(sizeof(struct config_log_item));
}

void config_log_item_free(struct config_log_item *cli)
{
	LOG_FUNC_START(1);

	if (cli) {

		while (cli->subsystem) {
			g_free(cli->subsystem->data);
			cli->subsystem = g_slist_remove(cli->subsystem,
						cli->subsystem->data);
		}

		if (cli->filename != NULL &&
				strcmp(cli->filename, "stderr") &&
				cli->fileio) {
			fclose(cli->fileio);
			g_free(cli->filename);
		}
	}

	LOG_FUNC_END(1);
}

/**
 *
 */
void config_log_item_add_subsystem(struct config_log_item *cli,
		gchar *subsystem)
{
	cli->subsystem=g_slist_append(cli->subsystem, subsystem);
}

/*******************************************************************************
 * FUNCTION THAT MANIPULATE logging_item STRUCTURE
 ******************************************************************************/

struct logging_item *logging_item_new (void)
{
        struct logging_item *li;

        li = g_malloc0(sizeof(struct logging_item));

        return li;
}

/*******************************************************************************
 * INTERNAL FUNCTIONS
 ******************************************************************************/

/**
 * Find logging configuration for a given subsystem
 *
 * @param subsystem
 *
 * \return Pointer to logging structure or NULL if not found
 *
 * Note that we treat LOGGING_SUBSYSTEM_ANY  keyword as a wildcard
 * that matches any subsystem!
 */
struct config_log_item *logging_find_by_subsystem(char *subsystem)
{
	struct config_log_item *cli;
	GSList *counter, *subsys;

	cli = NULL;
	if (logging != NULL) {

		counter = logging->log_items;

		while(counter) {
			cli = counter->data;
			counter = counter->next;

			subsys = cli->subsystem;

			while (subsys) {
				if (!strcmp((gchar *)subsys->data,
						LOGGING_SUBSYSTEM_ANY))
					goto out;

				if (!strcmp((gchar *)subsys->data, subsystem))
					goto out;

				subsys = subsys->next;
			}
		}

		cli = NULL;
	}

out:
	return cli;
 
}

/**
 * Convert numerical value of logging level into a printable string
 *
 * @param level
 *
 * \return Pointer to a static string.
 */
const char *logging_level2str(int level)
{
	char *str_level;
	if (level >= 1000) {
		str_level = "UNKNOWN"; 
	} else if (level >= 900) {
		str_level = "NOTSET"; 
	} else if (level >= 800) {
		str_level = "TRACE"; 
	} else if (level >= 700) {
		str_level = "DEBUG"; 
	} else if (level >= 600) {
		str_level = "INFO"; 
	} else if (level >= 500) {
		str_level = "NOTICE"; 
	} else if (level >= 400) {
		str_level = "WARN"; 
	} else if (level >= 300) {
		str_level = "ERROR"; 
	} else if (level >= 200) {
		str_level = "CRIT"; 
	} else if (level >= 100) {
		str_level = "ALERT";
	} else {
		str_level = "FATAL";
	}

	return str_level;
}

/**
 * Convert IKEv2 logging level into syslog equivalent
 *
 * @param level
 *
 * \return syslog equivalent level
 */
int logging_level2sys(int level) 
{
	if (level >= 700)
		level = 700;

	return level / 100;
}	

/*******************************************************************************
 * LOGGING FUNCTIONS
 ******************************************************************************/

/**
 * Record a log item
 *
 * @param logger_name	Subsystem which requested logging
 * @param level		Log level
 * @param buffer	NULL terminated string to record in log
 */
void ikev2_log_str(char *logger_name, int level, char *buffer) 
{
	struct config_log_item *cli;
	struct logging_item *li;
	const char *level2str;
	int level2sys;
	struct timeval tv;

	/*
	 * Check if we initialized logging subsystem. If not, then simply
	 * record logging item in memory until we do!
	 */
	if (logging == NULL) {

		if ((li = logging_item_new()) != NULL) {
			li->buffer = g_strdup(buffer);
			li->logger_name = g_strdup(logger_name);
			li->level = level;

			buffered_items = g_slist_append(buffered_items, li);
		}

	} else {

	        /*
		 * Find logging configuration for certain subsystem
		 */
		if ((cli = logging_find_by_subsystem(logger_name)) == NULL)
			return;

		/*
		 * Check logging level, if below defined for logging
		 * skip further execution.
		 */
		if (cli->level < level)
			return;

		if (cli->filename == NULL)  {

			level2sys = logging_level2sys(level);
			syslog(level2sys, "%-s %-s", logger_name, buffer);

		} else if (cli->fileio) { 

			level2str = logging_level2str(level);
			gettimeofday(&tv, NULL);	

			fprintf(cli->fileio, "%d.%03ld %s %s - %s\n",
					(int)tv.tv_sec, tv.tv_usec / 1000,
					logger_name, level2str, buffer);
			fflush(cli->fileio);

		}

	}
}

/**
 * Record a log item with variable number of arguments
 *
 * @param logger_name	Subsystem which requested logging
 * @param level		Log level
 * @param format	NULL terminated string to record in log
 */
void ikev2_log(char *logger_name, int level, const char *format, ...)
{
	va_list ap;
	char buffer[LOG_LINE_LENGTH];

	va_start(ap, format);
	vsnprintf (buffer, LOG_LINE_LENGTH, format, ap);
	va_end(ap);

	ikev2_log_str(logger_name, level, buffer);
}

/**
 * Log system error reported through errno variable
 *
 * @param loggername
 * @param level
 * @param function
 * @param line
 */
void ikev2_log_perror(char *loggername, int level, const char *function,
		int line)
{
	ikev2_log(loggername, level, "%s:%u %s", function, line, strerror(errno));
}

/**
 * Log system error with custom value
 *
 * @param loggername
 * @param level
 * @param function
 * @param line
 * @param errno
 */
void ikev2_log_errno(char *loggername, int level, const char *function,
		int line, int err_no)
{
	ikev2_log(loggername, level, "%s:%u %s", function, line,
			strerror(err_no));
}

/**
 * Log contents of buffer that will fit into one line!
 *
 * @param loggername
 * @param data
 * @param len
 */
void log_data(char *loggername, char *data, ssize_t len)
{
	ssize_t j, size;
	unsigned char c;
	char line[4096];
	int offset;

	for (j = 0, size = 4096, offset = 0; j < len && size > 2;
			j++, size -= 3, offset += 3) {
		c = (unsigned char)data[j] >> 4;
		c = (c < 10) ? (c + '0') : (c + 'A' - 10);
		snprintf (line + offset, size, "%c", c);

		c = (unsigned char)data[j] & 0x0F;
		c = (c < 10) ? (c + '0') : (c + 'A' - 10);
		snprintf (line + offset + 1, size - 1, "%c ", c);
	}

	ikev2_log_str(loggername, LOG_PRIORITY_DEBUG, line);
}

/**
 * Dump buffer that will NOT fit into one line!
 */
void log_buffer(char *loggername, char *data, ssize_t len)
{
	ssize_t i;

	ikev2_log(loggername, LOG_PRIORITY_DEBUG,
			"Dumping buffer at %p size %d",
			data, len);

	for (i = 0; i < len; i += 16)
		log_data(loggername, data + i, (len - i < 16) ? len - i : 16);
}

/*******************************************************************************
 * LOGGING SUBSYSTEM INITIALIZATION AND DEINITIALIZATION FUNCTIONS
 ******************************************************************************/

/**
 * Unload logging subsystem
 */
void logging_unload(void)
{
	GSList *counter;
	struct logging_item *li;

	if (logging) {

		counter = logging->log_items;

		/*
		 * FIXME: Freeing of configuration items has to be done in
		 * config.[ch].
		 */
		while (counter) {
//			config_log_item_free(counter->data);
			counter = g_slist_remove(counter, counter->data);
		}

		if (logging->facility > -1)
			closelog();
	}

        while (buffered_items) {
                li = buffered_items->data;
                buffered_items = buffered_items->next;

		if (li->level <= LOG_MAX_STDERR) {
                	fprintf(stderr, "%s %s %s\n", li->logger_name,
					logging_level2str(li->level),
					li->buffer);
			fflush(stderr);
		}

                g_free(li->buffer);
                buffered_items = g_slist_remove(buffered_items, li);
        }
}

/**
 * Initialize logging subsystem
 *
 * \return 0 If initialization was successful, -1 if not
 */
gint logging_init(struct config_log *config_log)
{
	GSList *cl;
	gint err;
	struct config_log_item *cli;
	struct logging_item *li;

	err = -1;
	cl = config_log->log_items;

	/*
	 * TODO
	 * Check if we have logging configuration for subsystem
	 * "logging", i.e. for this module (recursive calls that might
	 * create problems).
 	 */
	while (cl) {
		cli = cl->data;
		cl = cl->next;

		if (cli->filename != NULL) {

			if (!strcmp(cli->filename, "stderr")) {
				cli->fileio = stderr;
				break;
			}
			cli->fileio = fopen(cli->filename, "w");
			
			if (cli->fileio == NULL) {
				/*
				 * TODO: Check IF!
				 * This can be dangerous if someone tries to
				 * put specific filename that might break
				 * IKEv2 daemon.
				 *
				 * Also, this has to be somehow integrated
				 * into ike's logging system!
				 */
				perror(cli->filename);
				logging_unload();
				goto out;
			}
		}
	}

	if (config_log->facility > -1)
		openlog(IKEV2_SYSLOG_IDENT, IKEV2_SYSLOG_OPTIONS,
				config_log->facility);

	logging = config_log;

	/*
	 * Dump all buffered items into log destination
	 */
	while (buffered_items) {
		li = buffered_items->data;
		buffered_items = buffered_items->next;

		ikev2_log_str(li->logger_name, li->level, li->buffer);

		g_free(li->buffer);
		buffered_items = g_slist_remove(buffered_items, li);
	}

	err = 0;

out:
	return err;
}

#else

struct config_log_item *logging_find_by_subsystem(char *subsystem)
{
	return NULL;
}

struct logging_item *logging_item_new (void)
{
	return NULL;
}

void ikev2_log_str(char *logger_name, int level, char *buffer)
{
	return;
}

void ikev2_log(char *logger_name, int level, const char *format, ...)
{
	return;
}

void ikev2_log_perror(char *logger_name, int level, const char *function, int line)
{
	return;
}

void logging_unload(void) 
{
	return;
}

gint logging_init(GSList *config_logging)
{
	return 0;
}

#endif /* DISABLE_LOGGING */
