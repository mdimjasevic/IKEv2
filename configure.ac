AC_PREREQ(2.59)
AC_INIT(ikev2, 1.1, ikev2-devel@lists.sourceforge.net)
AM_INIT_AUTOMAKE(ikev2, 1.1)

# Set required versions for different packages
m4_define([glib_required_version], [2.4.0])

AC_CONFIG_SRCDIR([src/ikev2_consts.h])

AC_CONFIG_MACRO_DIR([m4])

AC_CANONICAL_HOST

AC_LANG_C
AC_PROG_CC
AC_PROG_CPP
AC_PROG_YACC
AM_PROG_LEX
AC_PROG_LIBTOOL

# check for curl-config
AC_PATH_PROG(CURLCONFIG, curl-config)

dnl we require bison for building the parser
AC_PATH_PROG(BISON_PATH, bison, no)
if test x$BISON_PATH = xno; then
  AC_MSG_ERROR(Could not find bison)
fi

dnl check bison version
AC_MSG_CHECKING([bison version])

#if $BISON_PATH --version | head -n 1 | $AWK '{ if ($4 < 1.875) exit 1; else exit 0;}'; 
#then 
#  AC_MSG_RESULT([ok])
#else 
#  AC_MSG_RESULT([too old.])
#  AC_MSG_ERROR([Your bison version is too old, v1.875 or later is required.])
#fi

CFLAGS_ADD="$CFLAGS_ADD -Wall -Werror -Wno-unused"
CPPFLAGS="-D_GNU_SOURCE -DOPENSSL -DCERT_AUTH $CPPFLAGS"

# Checks for libraries.
AC_CHECK_LIB([crypto], [DH_new])
AC_CHECK_LIB(curl,curl_easy_perform, ,
	AC_MSG_ERROR([You have to install curl library]))

#
# Check if we should completly disable logging. This is
# only for testing purposes and should not be normaly
# used!
#
disable_logs=yes
AC_MSG_CHECKING(if --disable-logging option is specified)
AC_ARG_ENABLE(logging,
	[  --disable-logging       disable logging in daemon],
	[disable_logs=yes], [disable_logs=no])
AC_MSG_RESULT(${disable_logs})
if test x$disable_logs = xyes; then
	CPPFLAGS="-DDISABLE_LOGGING $CPPFLAGS"
fi

#
# Checks related to dynamic configuration
#
enable_cfgmodule=no

#
# Check if we should enable DHCPv4 configuration provider.
#
AC_MSG_CHECKING(if --enable-dhcpv4 option is specified)
AC_ARG_ENABLE(dhcpv4,
	[  --enable-dhcpv4         enable dhcpv4 configuration provider],
	[enable_dhcpv4=yes], [enable_dhcpv4=no])
AC_MSG_RESULT(${enable_dhcpv4})
if test x$enable_dhcpv4 = xyes; then
	CPPFLAGS="-DCFG_DHCPV4 $CPPFLAGS"
	enable_cfgmodule=yes
fi

#
# Check if we should enable PRIVATE configuration provider.
#
AC_MSG_CHECKING(if --enable-private-provider option is specified)
AC_ARG_ENABLE(private_provider,
	[  --enable-private-provider enable dhcpv4 configuration provider],
	[enable_private_provider=yes], [enable_private_provider=no])
AC_MSG_RESULT(${enable_private_provider})
if test x$enable_private_provider = xyes; then
	CPPFLAGS="-DCFG_PRIVATE $CPPFLAGS"
	enable_cfgmodule=yes
fi

#
# If any of the configuration providers is enabled then
# also enable configuration module
#
AC_MSG_CHECKING(should we enable cfg module)
if test x$enable_cfgmodule = xyes; then
	CPPFLAGS="-DCFG_MODULE $CPPFLAGS"
	AC_MSG_RESULT(yes)
else
	AC_MSG_RESULT(no)
fi

#
# Check if client support for dynamic configuration has been requested
#
AC_MSG_CHECKING(if --enable-cfg-client option is specified)
AC_ARG_ENABLE(cfg-client,
	[  --enable-cfg-client     include initiator support for dynamic configuration],
	[enable_cfg_client=yes], [enable_cfg_client=no])
AC_MSG_RESULT(${enable_cfg_client})
if test "$enable_cfg_client" == "yes"; then
	CPPFLAGS="-DCFG_CLIENT $CPPFLAGS"
fi

#
# Check if we should support NAT-T
#
AC_MSG_CHECKING(if --enable-natt option is specified)
AC_ARG_ENABLE(natt,
	[  --enable-natt           include NAT-T support in IKEv2 daemon],
	[enable_natt=yes], [enable_natt=no])
AC_MSG_RESULT(${enable_natt})
if test "$enable_natt" == "yes"; then
	CPPFLAGS="-DNATT $CPPFLAGS"
fi

#
# Check if we should support MOBIKE
#
AC_MSG_CHECKING(if --enable-mobike option is specified)
AC_ARG_ENABLE(mobike,
	[  --enable-mobike           include MOBIKE support in IKEv2 daemon],
	[enable_mobike=yes], [enable_mobike=no])
AC_MSG_RESULT(${enable_mobike})
if test "$enable_mobike" == "yes"; then
	CPPFLAGS="-DMOBIKE $CPPFLAGS"
fi

#
# Check for efence
# (adopted from racoon's configure.in)
#
efence_dir=no
AC_MSG_CHECKING(if --enable-efence option is specified)
AC_ARG_ENABLE(efence,
	[  --enable-efence         specify ElectricFence directory],
	[enable_efence=yes], [enable_efence=no])
AC_MSG_RESULT(${enable_efence})
if test "$enable_efence" != "no"; then

	AC_CHECK_LIB([efence], [EF_Abort], ,
		AC_MSG_ERROR([You have to install ElectricFence library]))
	LIBS="$LIBS -lefence"
fi

#
# Check if we should enable building of binaries in tests/ subdirectory
#
AC_MSG_CHECKING(if --enable-framework-tests option is specified)
AC_ARG_ENABLE(framework-tests,
	[  --enable-framework-tests    build framework tests in the tests subdirectory],
	[enable_framework_tests=yes], [enable_framework_tests=no])
AC_MSG_RESULT(${enable_framework_tests})

AM_CONDITIONAL(BUILD_FRAMEWORK_TESTS, test $enable_framework_tests = yes)

#
# Run AM_PATH_GLIB_2_0 to make sure that GLib is installed and working
# (adopted from gtk2)
#

AM_PATH_GLIB_2_0(glib_required_version, :,
	AC_MSG_ERROR([
*** GLIB glib_required_version or better is required. The latest version of
*** GLIB is always available from ftp://ftp.gtk.org/pub/gtk/.]),
	gthread)

#
# Check for openssl
# (adopted from racoon's configure.in)
#

AC_MSG_CHECKING(if --with-openssl option is specified)
AC_ARG_WITH(openssl, [  --with-openssl=DIR      specify OpenSSL directory],
        [crypto_dir=$withval])
AC_MSG_RESULT(${crypto_dir-"default"})

if test "x$crypto_dir" = "x"; then
	case $host_os in
	netbsd*) crypto_dir="/usr/pkg";;
	freebsd*)
		if test -d /usr/local/ssl; then
			crypto_dir="/usr/local/ssl"
		else
			crypto_dir="/usr/local"
		fi
		;;
	esac
else
	LIBS="$LIBS -L${crypto_dir}/lib"
	CPPFLAGS="-I${crypto_dir}/include $CPPFLAGS"
fi

AC_TRY_COMPILE([#include <sys/types.h>
#include <stdio.h>
#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/des.h>], [], [], [openssl_compile_failed=yes])

if test "x$openssl_compile_failed" = "xyes"; then
	echo
	echo "Fatal: crypto library and headers not found."
	echo Specify proper directory by using --with-openssl.
	if test `uname -s` = FreeBSD; then
		echo Use ports/security/OpenSSL to install OpenSSL, or visit
	elif test `uname -s` = NetBSD; then
		echo Use pkgsrc/security/OpenSSL to install OpenSSL, or visit
	else
		echo -n "Visit "
	fi
	echo ftp://psych.psy.uq.oz.au/pub/Crypto/SSL/, or visit
	echo http://www.openssl.org/
	exit 1
fi

# Quick hack to allow compilation on FC8T3
LIBS="$LIBS -lssl"

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([arpa/inet.h netinet/in.h stdint.h stdlib.h string.h sys/socket.h unistd.h])
AC_CHECK_HEADERS(curl/curl.h, , AC_MSG_ERROR(Cannot find curl.h.))

#
# Check if the user requested suppliant to be compiled in
#
AC_MSG_CHECKING(if --enable-eap option is specified)
AC_ARG_ENABLE(eap,
	[  --enable-eap            build EAP support on the initiator],
	[enable_eap=yes], [enable_eap=no])
AC_MSG_RESULT(${enable_eap})

if test $enable_eap = yes; then

	#
	# Check for wpa_supplicant development libraries
	#

	AC_MSG_CHECKING(if --with-wpa-lib option is specified)
	AC_ARG_WITH(wpa, [  --with-wpa-lib=DIR          specify wpa_supplicant library directory], [wpa_dir_lib=$withval])
	if test x$wpa_dir_lib = x; then
		AC_MSG_RESULT(no, trying /usr/local/lib)
		wpa_dir_lib=/usr/local/lib
	else
		AC_MSG_RESULT(yes)
	fi

	AC_MSG_CHECKING(if --with-wpa-include option is specified)
	AC_ARG_WITH(wpa, [  --with-wpa-include=DIR          specify wpa_supplicant include directory], [wpa_dir_include=$withval])
	if test x$wpa_dir_include = x; then
		AC_MSG_RESULT(no, trying /usr/local/include)
		wpa_dir_include=/usr/local/include
	else
		AC_MSG_RESULT(yes)
	fi

	SAVED_LIBS=$LIBS
	SAVED_CPPFLAGS=$CPPFLAGS

	LIBS="-lsupplicant -L${wpa_dir_lib} $LIBS"
	CPPFLAGS="-I${wpa_dir_include} $CPPFLAGS"

	AC_MSG_CHECKING(if wpa_supplicant libaries are installed)
	AC_TRY_COMPILE([#include <stdio.h>
#include <sys/types.h>
#include <wpa/common.h>
#include <wpa/config.h>
#include <wpa/eap.h>
], [], [wpa_compile_failed=no], [wpa_compile_failed=yes])

	AM_CONDITIONAL(BUILD_SUPPLICANT, test $wpa_compile_failed = no)

	if test "x$wpa_compile_failed" = "xyes"; then
		AC_MSG_RESULT(no)
		AC_MSG_WARN([No wpa_supplicant libraries. Disablig EAP on initiator!])
	else
		AC_MSG_RESULT(yes)
# Constant IEEE8021X_EAPOL is necessary for wpa_supplicant to include all the
# necessary definitions in it's configuration file.
		WPA_CPPFLAGS="-DSUPPLICANT -DIEEE8021X_EAPOL -I${wpa_dir_include}"
		WPA_LDFLAGS="-lsupplicant -L${wpa_dir_lib}"
		AC_SUBST(WPA_CPPFLAGS)
		AC_SUBST(WPA_LDFLAGS)
	fi

	# Restore saved variables
	LIBS=$SAVED_LIBS
	CPPFLAGS=$SAVED_CPPFLAGS
else
	AM_CONDITIONAL(BUILD_SUPPLICANT, false)
fi

#
# Check if client support for accessing RADIUS servers has been requested
#
AC_MSG_CHECKING(if --enable-radius-client option is specified)
AC_ARG_ENABLE(radius-client,
	[  --enable-radius-client  include support to access RADIUS servers],
	[enable_radius_client=yes], [enable_radius_client=no])
AC_MSG_RESULT(${enable_radius_client})
if test "$enable_radius_client" == "yes"; then
	CPPFLAGS="-DRADIUS_CLIENT -DAAA_CLIENT $CPPFLAGS"
fi

#
# Check for pfkeyv2.h and ipsec.h
# (adopted from racoon's configure.in)
#

case "$host_os" in
	*linux*)
		AC_ARG_WITH(kernel-headers,
			AC_HELP_STRING([--with-kernel-headers=/lib/modules/<uname>/build/include],
				[where your Linux Kernel headers are installed]),
			[ KERNEL_INCLUDE="$with_kernel_headers"
			CONFIGURE_AMFLAGS="--with-kernel-headers=$with_kernel_headers"
			AC_SUBST(CONFIGURE_AMFLAGS) ],
		[ KERNEL_INCLUDE="/lib/modules/`uname -r`/build/include" ])

		AC_CHECK_HEADER($KERNEL_INCLUDE/linux/pfkeyv2.h, ,
			[ AC_CHECK_HEADER(/usr/src/linux/include/linux/pfkeyv2.h,
				KERNEL_INCLUDE=/usr/src/linux/include ,
				[ AC_MSG_ERROR([Unable to find linux-2.6 kernel headers. Aborting.]) ] ) ] )

		AC_SUBST(KERNEL_INCLUDE)
		# We need the configure script to run with correct kernel headers.
		# However we don't want to point to kernel source tree in compile time,
		# i.e. this will be removed from CPPFLAGS at the end of configure.
		CPPFLAGS="-I$KERNEL_INCLUDE $CPPFLAGS"

		AC_CHECK_MEMBER(struct sadb_x_policy.sadb_x_policy_priority,
			[AC_DEFINE(HAVE_PFKEY_POLICY_PRIORITY, [],
				[Are PF_KEY policy priorities supported?])], [],
			[#include "$KERNEL_INCLUDE/linux/pfkeyv2.h"])

		# Look up ipsec.h header
		AC_CHECK_HEADER($KERNEL_INCLUDE/linux/ipsec.h,
			[have_ipsec=yes], [have_ipsec=no])

		if test "$have_ipsec" == "no"; then
			AC_MSG_ERROR([Could not find ipsec.h. Aborting.])
		fi

		CPPFLAGS="-DPFKEY_LINUX $CPPFLAGS"

		;;
	*)
		AC_MSG_ERROR([Could not find pfkeyv2.h. Unsupported host os. Aborting.])
		;;
esac

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_SIZE_T

# Checks for library functions.
AC_FUNC_MEMCMP
AC_CHECK_FUNCS([memset socket])

#
# Check if we use SCTP instead of UDP. This feature is used only
# for experimental purposes, and nothing else...
#
sctp=
AC_MSG_CHECKING(if --enable-sctp option is specified)
AC_ARG_ENABLE(sctp,
	[  --enable-sctp           use SCTP as transport protocol for IKEv2 messages instead of UDP],
	[sctp=yes], [sctp=no])
AC_MSG_RESULT(${sctp})
if test x$sctp = xyes; then
	CPPFLAGS="-DUSE_SCTP $CPPFLAGS"
fi

#
# Generate libcurl flags and libs
#
if test "x$CURLCONFIG" = "x"; then
   AC_MSG_ERROR(curl-config not found.)
else
  CPPFLAGS="`$CURLCONFIG --cflags` $CPPFLAGS"
  LIBS="$LIBS `$CURLCONFIG --libs`"
fi

AC_CONFIG_FILES([Makefile
		src/Makefile
		docs/Makefile
		tests/Makefile
		conf/ikev2.conf
		conf/psk.txt])
AC_OUTPUT
