/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SPD_H
#define __SPD_H

enum {
	SP_OWNER_NONE,
	SP_OWNER_IKEV2,
	SP_OWNER_KERNEL,
	SP_OWNER_UNKNOWN
};

/**
 * Structure with configuration data for the policy item
 */
struct config_csa {
	/**
	 * How many times is this structure referenced...
	 */
	gint refcnt;

	/**
	 * This is for debugging purposes, we mark where SAINFO section
	 * starts and when this SAINFO is selected we write it's line
	 * number...
	 */
	guint32 cfg_line;

	/**
	 * Traffic selectors for this CHILD SA (local - tsl, and
	 * remote (tsr)
	 */
	GSList *tsl, *tsr;

	/**
	 * Supported encryption algorithms
	 */
	GSList *encr_algs;

	/**
	 * Supported authentication algorithms
	 */
	GSList *auth_algs;

	/**
	 * A list of ID's, i.e. struct id_type structures...
	 */
	GSList *id;

	/**
	 * Flag field, determines the handling of this CHILD SA.
	 * This field is used in conjunction with kernel_spd parameter
	 * and there are three possibilities:
	 *
	 * (i)   When the parameter is set to "sync" then CHILD SAs, in
	 *       order to be used, have to have this field set to
	 *       CSA_ACTIVE. The CSA_ACTIVE value is assigned to this
	 *       field by IKEv2 daemon itself during IKEv2 startup.
	 *
	 * (ii)  If the parameter is "generate" and this parameter
	 *       has value CSA_INSTALL, then traffic selectors from
	 *       this child_sa will be placed in kernel. CSA_INSTALL
	 *       value is defined in configuration file.
	 *
	 * (iii) Finally, when the parameter is "flush" then this 
	 *       field behaves as generate, but all sainfo sections
	 *       are valid!
	 */
	guint8 status;

	/**
	 * Should we use DH keys when rekeying/creating this CHILD SA?
	 */
	guint8 pfs_flag;
	guint32 dh_group;

	/**
	 * Hard and soft limits constraints configured for CHILD SA
	 */
	struct limits *soft_limits;
	struct limits *hard_limits;

	/**
	 * Tunnel endpoints...
	 */
	struct netaddr *tunnel_saddr;
	struct netaddr *tunnel_daddr;

	/**
	 * Level of protection for the traffic. One of the default
	 * (IPSEC_LEVEL_DEFAULT), use (IPSEC_LEVEL_USE), require
	 * (IPSEC_LEVEL_REQUIRE) or unique (IPSEC_LEVEL_UNIQUE)
	 */
	guint8 ipsec_level;

	/**
	 * SPD mode that should be used, either tunnel (IPSEC_MODE_TUNNEL),
	 * transport (IPSEC_MODE_TRANSPORT), or any (IPSEC_MODE_ANY);
	 */
	guint8 ipsec_mode;

	/**
	 * IPsec protocol to use for this SA. One of the IKEV2_PROTOCOL_ESP,
	 * IKEV2_PROTOCOL_AH, or IKEV2_PROTOCOL_UNSPEC. The default value is
	 * IKEV2_PROTOCOL_UNSPEC.
	 */
	guint ipsec_proto;
};

/**
 * Data specific to a tunnel mode of operation...
 */
struct tunnel {
	struct {
		/*
		 * DSCP, if true(1) then appropriate filed is copied from
		 * inner header, otherwise, if false(0) copy from a table
		 * (table is not yet implemented)
		 */
		int dscp:1;

		/*
		 * FIXME
		 */
		int ecn:1;

		/*
		 * Don't fragment bit: clear(0), set(1) or copy(2)
		 */
		int df:2;
	};

	/*
	 * Local and remote addresses of tunnel...
	 */
	struct netaddr *local;
	struct netaddr *remote;
};

/**
 * Structure for selector item
 *
 * This structure is simplified version of RFC4301 model. The reason for
 * simplification is that IKEv2 protocol does not support address ranges
 * during TS negotiations. In case address ranges are offered IKEv2 peer
 * will treat them as different possibilites of which, only one has to
 * be selected.
 */
struct selector {
	/*
	 * Ranges for local and remote addresses
	 */
	struct netaddr *laddr_start;
	struct netaddr *laddr_end;
	struct netaddr *raddr_start;
	struct netaddr *raddr_end;

	/*
	 * Next layer protocol. Possible values are:
	 *
	 * 0			ANY protocol
	 * 1			ICMP protocol
	 * 2-54, 56-254		Specific protocol
	 * 55			Mobile IP protocol
	 * 255			OPAQUE
	 *
	 * In case next layer protocol has ports, or in case of ICMP,
	 * type and code, or in case of Mobile IP, mobility header,
	 * those values are stored inside laddr_start, laddr_end,
	 * raddr_start, and raddr_end.
 	 */
	guint8 proto;
};

/**
 * This is a structure for SAD items! We suppose that SPD item represents
 * up to three SAD items in a kernel, one for out, in and fwd. This spd_item
 * is for out elements. If in exist that it's ID is in pid_in, if fwd exists
 * then it's ID is in pid_fwd.
 */
struct spd_item {
	/**
	 * Number of references to this structure...
	 */
	gint refcnt;

	/**
	 * Who installed policy, i.e. who is it's owner. We know that
	 * the policy is in the kernel if there is IDs for it.
	 */
	guint8 owner;

	/**
	 * Mode of this SPD. The value can be one of IPSEC_MODE_ANY,
	 * IPSEC_MODE_TRANSPORT, IPSEC_MODE_TUNNEL.
 	 */
	gint mode;

	/**
	 * Mode of this SPD. The value can be one of IPSEC_LEVEL_DEFAULT,
	 * IPSEC_LEVEL_USE, IPSEC_LEVEL_REQUIRE, IPSEC_LEVEL_UNIQUE
 	 */
	gint level;

	/**
	 * Protection protocol. One of ESP or AH.
	 */
	guint8 proto;

	/**
	 * In/out/fwd policy item...
	 *
	 * This is a transient field. It is used only during dumping
	 * SPD from the kernel.
	 */
	guint8 dir;

	/**
	 * Traffic selectors. Note two thing here that maybe have to
	 * be changed. First, there are no separate values for the other
	 * direction since we are assuming those are equal but reversed.
	 * Second, we assume that each SPD has single traffic selector.
	 *
	 * tsl -> local ts, tsr -> remote ts
	 */
	GSList *tsl;
	GSList *tsr;

	/**
	 * ID of a policies
	 *
	 * If these fields are zero, then the policy is not in the kernel
	 */
	guint32 pid, pid_in, pid_fwd;

	/**
	 * Req ID of a policy
	 */
	guint32 reqid, reqid_in, reqid_fwd;

	/**
	 * In case it's tunnel mode, here are tunnel's endpoints...
	 *
	 * There is no special fields for reverse direction since we are
	 * assuming they are the same, although reversed.
	 */
	struct netaddr *tunnel_saddr;
	struct netaddr *tunnel_daddr;

	/**
	 * Configuration data for this SPD
	 *
	 * We allow single config csa to be bound to multiple policies
	 * but the other way around is not possible!
	 */
	struct config_csa *ccsa;
};

#ifdef __SPD_C

const char *ipsec_mode_strs[] = {
	"IPSEC_MODE_ANY",
	"IPSEC_MODE_TRANSPORT",
	"IPSEC_MODE_TUNNEL"
};

const char *ipsec_dir_strs[] = {
	"IPSEC_DIR_ANY",
	"IPSEC_DIR_INBOUND",
	"IPSEC_DIR_OUTBOUND",
	"IPSEC_DIR_FWD"
};

#else /* __SPD_C */
extern const char *ipsec_mode_strs[];
extern const char *ipsec_dir_strs[];
#endif /* __SPD_C */

/**
 * Structure used to register a protocol for SAD manipulation
 */
struct spd_op {
	/*
	 * Pointer to data specific to a subsystem used for communication
	 * with kernels SA database.
	 */
	void *priv_data;

	void (*spd_update)();
	int (*spd_add)(struct spd_item *);
	int (*spd_delete)(struct spd_item *);
	void (*spd_get)();
	void (*spd_acquire)();
	void (*spd_register)();
	void (*spd_expire)();
	int (*spd_flush)();
	int (*spd_dump)(GSList **);

	/*
	 * Function to unload subsystem
	 */
	void (*spd_running)();
	void (*spd_shutdown)();
	void (*spd_unload)();
};

/**
 * State data for SPD in IKEv2
 */
struct spd_data {
	/**
	 * Queue for upper modules, i.e. state machines
	 */
	GAsyncQueue *sm_queue;

	/**
	 * Queue for receiving responses and requests from kernel
	 */
	GAsyncQueue *queue;

	/**
	 * Operation structure, with pointer to specific functions
	 * to perform different tasks on SAD.
	 */
	struct spd_op *spd_op;

	/**
	 * Thread that waits on a queue and processes all the
	 * received messages 
	 */
	GThread *thread;

	/**
	 * Mutex for protecting list of policies
	 */
	GMutex *kspd_mutex;

	/**
	 * List of all policies in the kernel
	 */
	GSList *kspd;
};

/*******************************************************************************
 * config_csa STRUCTURE FUNCTIONS
 ******************************************************************************/
struct config_csa *config_csa_new();
struct config_csa *config_csa_dup_ro(struct config_csa *);
void config_csa_free(struct config_csa *);

guint32 config_csa_get_cfg_line(struct config_csa *);

GSList *config_csa_get_encr_transforms(struct config_csa *);
GSList *config_csa_get_auth_transforms(struct config_csa *);

struct config_csa *config_csa_find_by_ts(GSList *, GSList *, GSList *,
		GSList **, GSList **);

/*******************************************************************************
 * spd_item FUNCTIONS
 ******************************************************************************/
struct spd_item *spd_item_new();
struct spd_item *spd_item_dup(struct spd_item *);
struct spd_item *spd_item_ref(struct spd_item *);
struct spd_item *spd_item_new_from_prototype(struct spd_item *,
		GSList *, GSList *);
void spd_item_free(struct spd_item *);
void spd_item_list_free(GSList *);

void spd_list_dump(GSList *);

GSList *spd_item_get_encr_algs(struct spd_item *);
GSList *spd_item_get_auth_algs(struct spd_item *);

guint8 spd_item_get_ipsec_mode(struct spd_item *);
void spd_item_set_ipsec_mode(struct spd_item *, guint8);

guint8 spd_item_get_ipsec_proto(struct spd_item *);
void spd_item_set_ipsec_proto(struct spd_item *, guint8);

guint8 spd_item_get_ipsec_level(struct spd_item *);
void spd_item_set_ipsec_level(struct spd_item *, guint8);

guint8 spd_item_get_dir(struct spd_item *);
void spd_item_set_dir(struct spd_item *, guint8);

guint32 spd_item_get_pid(struct spd_item *);
guint32 spd_item_get_pid_in(struct spd_item *);
guint32 spd_item_get_pid_fwd(struct spd_item *);
guint32 spd_item_get_reqid(struct spd_item *);
guint32 spd_item_get_reqid_in(struct spd_item *);
guint32 spd_item_get_reqid_fwd(struct spd_item *);
GSList *spd_item_get_tsl(struct spd_item *);
GSList *spd_item_get_tsr(struct spd_item *);

struct netaddr *spd_item_get_tunnel_saddr(struct spd_item *);
struct netaddr *spd_item_get_tunnel_daddr(struct spd_item *);

struct spd_item *spd_find_by_pid(guint32);
struct spd_item *spd_find_by_ccsa(struct config_csa *);
struct spd_item *spd_item_find_by_ts(GSList *, GSList *,
		GSList *, GSList **, GSList **);

struct config_csa *spd_item_get_config_csa(struct spd_item *);

struct limits *spd_item_get_soft_limits(struct spd_item *);
struct limits *spd_item_get_hard_limits(struct spd_item *);

/*******************************************************************************
 * FUNCTIONS FOR SPD MAINTENANCE
 ******************************************************************************/
void spd_update(struct spd_item *);
int spd_add(struct spd_item *);
void spd_delete();
struct spd_item *spd_get();
void spd_acquire();
void spd_dump();
void spd_flush();
void spd_setidx();
void spd_setexpire();
void spd_delete2();

/*******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/
void spd_set_shutdown_state(void);
void spd_unload();
int spd_init(GMainContext *, GAsyncQueue *, GSList *, guint8);

#endif /* __SPD_H */
