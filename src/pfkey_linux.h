/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __PFKEY_H
#define __PFKEY_H

#define PFKEY_STATE_INIT		1
#define PFKEY_STATE_RUNNING		2
#define PFKEY_STATE_SHUTDOWN		3

/*
 * Maximum number of bytes exptected to be received
 * from kernel through PFKEY socket.
 */
#define MAX_PFKEY_RECV_SIZE	4096

struct pfkey_data {
	/*
	 * State of the PFKEY subsystem
	 *
	 * During INIT state we are not running GLib's main
	 * loop so we can not rely on callback to devliver us
	 * a response message.
	 */
	guint8 state;

	/*
	 * Fields for communication with kernel and monitoring
	 */
	int pfkeyfd;
	GIOChannel *pfkeyfd_io_channel;
	GSource *pfkey_cb;

	/*
	 * All transforms supported by kernel.
	 */
	GSList *transforms;

        /*
	 * Sequence number of next PF_KEY message.
	 */
	gint pfkey_seq;

	/*
	 * Queue to send messages to
	 */
	GAsyncQueue *queue;

	/*
	 * Local queue and associated data for local communication
	 * in PFKEY subsystem
	 */
	GAsyncQueue *lqueue;
	GMutex *mlqueue;
	guint32 seq;

#if 0
	/*
	 * Pointer to a SPD database copies and appropriate locks
	 */
	GStaticRWLock kspd_lock;
	GSList *kspd;
#endif
};

#ifdef __PFKEY_C
static guint8 sadb_ext_min_len[] = {
	[SADB_EXT_RESERVED]		= (guint8) 0,
	[SADB_EXT_SA]			= (guint8) sizeof(struct sadb_sa),
	[SADB_EXT_LIFETIME_CURRENT]	= (guint8) sizeof(struct sadb_lifetime),
	[SADB_EXT_LIFETIME_HARD]	= (guint8) sizeof(struct sadb_lifetime),
	[SADB_EXT_LIFETIME_SOFT]	= (guint8) sizeof(struct sadb_lifetime),
	[SADB_EXT_ADDRESS_SRC]		= (guint8) sizeof(struct sadb_address),
	[SADB_EXT_ADDRESS_DST]		= (guint8) sizeof(struct sadb_address),
	[SADB_EXT_ADDRESS_PROXY]	= (guint8) sizeof(struct sadb_address),
	[SADB_EXT_KEY_AUTH]		= (guint8) sizeof(struct sadb_key),
	[SADB_EXT_KEY_ENCRYPT]		= (guint8) sizeof(struct sadb_key),
	[SADB_EXT_IDENTITY_SRC]		= (guint8) sizeof(struct sadb_ident),
	[SADB_EXT_IDENTITY_DST]		= (guint8) sizeof(struct sadb_ident),
	[SADB_EXT_SENSITIVITY]		= (guint8) sizeof(struct sadb_sens),
	[SADB_EXT_PROPOSAL]		= (guint8) sizeof(struct sadb_prop),
	[SADB_EXT_SUPPORTED_AUTH]	= (guint8) sizeof(struct sadb_supported),
	[SADB_EXT_SUPPORTED_ENCRYPT]	= (guint8) sizeof(struct sadb_supported),
	[SADB_EXT_SPIRANGE]		= (guint8) sizeof(struct sadb_spirange),
	[SADB_X_EXT_KMPRIVATE]		= (guint8) sizeof(struct sadb_x_kmprivate),
	[SADB_X_EXT_POLICY]		= (guint8) sizeof(struct sadb_x_policy),
	[SADB_X_EXT_SA2]		= (guint8) sizeof(struct sadb_x_sa2),
	[SADB_X_EXT_NAT_T_TYPE]		= (guint8) sizeof(struct sadb_x_nat_t_type),
	[SADB_X_EXT_NAT_T_SPORT]	= (guint8) sizeof(struct sadb_x_nat_t_port),
	[SADB_X_EXT_NAT_T_DPORT]	= (guint8) sizeof(struct sadb_x_nat_t_port),
	[SADB_X_EXT_NAT_T_OA]		= (guint8) sizeof(struct sadb_address),
};

char *pfkey_message_types[] = {
	"SADB_RESERVED",
	"SADB_GETSPI",
	"SADB_UPDATE",
	"SADB_ADD",
	"SADB_DELETE",
	"SADB_GET",
	"SADB_ACQUIRE",
	"SADB_REGISTER",
	"SADB_EXPIRE",
	"SADB_FLUSH",
	"SADB_DUMP",
	"SADB_X_PROMISC",
	"SADB_X_PCHANGE",
	"SADB_X_SPDUPDATE",
	"SADB_X_SPDADD",
	"SADB_X_SPDDELETE",
	"SADB_X_SPDGET",
	"SADB_X_SPDACQUIRE",
	"SADB_X_SPDDUMP",
	"SADB_X_SPDFLUSH",
	"SADB_X_SPDSETIDX",
	"SADB_X_SPDEXPIRE",
	"SADB_X_SPDDELETE2",
	"SADB_X_NAT_T_NEW_MAPPING"
};

int _pfkey_parse_exthdrs(struct sadb_msg *, void **);
gint16 _pfkey_parse_address_ext(struct netaddr **, struct sadb_address *);
int _pfkey_send(struct sadb_msg *, void **);
ssize_t _pfkey_recv(void **);
guint32 pfkey_get_seq(guint32);
guint32 _pfkey_parse_sa_ext(struct sadb_sa *);
int pfkey_parse_sadb_acquire(struct sadb_msg *, struct pfkey_msg *);
gboolean pfkey_parse_sadb_x_spdget(struct sadb_msg *, struct pfkey_msg *);

#endif /* __PFKEY_C */

/*******************************************************************************
 * PFKEY OPERATIONS
 ******************************************************************************/
GSList *pfkey_register(guint8, gboolean);
int pfkey_spdflush();
int pfkey_flush(guint8);
gint pfkey_spddump();
guint32 pfkey_getspi(struct netaddr *, struct netaddr *, guint8,
		guint8, guint32, guint32);
int pfkey_update(guint8, guint8, guint32, guint32, guint32,
		transform_t *, char *, transform_t *, char *,
		struct netaddr *, struct netaddr *,
		struct limits *, struct limits *, gboolean);
int pfkey_add(struct sad_item *si, struct netaddr *, struct netaddr *,
		gboolean, gboolean);
struct pfkey_msg *pfkey_spdget(guint32, guint32);
int pfkey_delete_single_sa(guint32, guint8, struct netaddr *,
		struct netaddr *, guint32);
int pfkey_delete(struct sad_item *, struct netaddr *, struct netaddr *);
gboolean ikev2_pfkey_data_pending_cb(GIOChannel *, GIOCondition, gpointer);

/*******************************************************************************
 * COMPLEX PFKEY FUNCTIONS
 ******************************************************************************/
struct spd_item *pfkey_find_spd_by_ts(GSList *, GSList *, guint8);
struct spd_item *pfkey_get_spd_by_id(guint32);

/*******************************************************************************
 * MISC FUNCTIONS
 ******************************************************************************/
GSList *pfkey_supported_proposals_get(void);

/*******************************************************************************
 * PFKEY INITIALIZATION FUNCTIONS
 ******************************************************************************/
void pfkey_schedule_shutdown();
void pfkey_unload();
struct sad_op *pfkey_linux_sad_init(GMainContext *, GAsyncQueue *);
struct spd_op *pfkey_linux_spd_init(GMainContext *, GAsyncQueue *);
int pfkey_init(GMainContext *, GSList *, GAsyncQueue *);

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/
int pfkey_dump(struct sadb_msg *, void **);
const char *pfkey_msg2str(guint8);

#endif /* __PFKEY_H */
