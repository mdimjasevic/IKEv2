/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __PFKEY_MSG_H
#define __PFKEY_MSG_H

#define PFKEY_MSG	0

/*
 * Structure used for communication between PFKEY subsystem
 * and main controller
 */
struct pfkey_msg {
	/*
	 * Different message types could be sent to state machine subsystem
	 * and the following field holds discriminator to exact type. For
	 * pfkey_msg type is set to PFKEY_MSG.
	 */
	guint8 msg_type;

	/*
	 * Action requested from the kernel. One of SADB_ACQUIRE, SADB_EXPIRE
	 */
	guint8 action;

	/*
	 * Data received from a header of a message
	 */
	guint8 errno_val;
	guint32 seq;		/* Request number */
	guint32 pid;		/* Process ID that sent request */

	union {
		/*
		 * Data specific for ACQUIRE message...
		 */
		struct {

			/*
			 * Policy ID that triggered this ACQUIRE message
			 */
			guint32 pid;

			/*
			 * Source and destination addresses from a
			 * request...
			 */
			struct netaddr *saddr;
			struct netaddr *daddr;

		} acquire;

		/*
		 * Data sent in EXPIRE message...
		 */
		struct {
			/*
			 * SPI of expired SA
			 */
			guint32 spi;

			/*
			 * SA protocol, IKE or AH
			 */
			guint8 protocol;

			/*
			 * Did hard limit expired
			 */
			gboolean hard_limit;

			/*
			 * Source and destination addresses of SA
			 */
			struct netaddr *saddr, *daddr;
		} expire;

		/*
		 * Data returned from SADB_GETSPI request...
		 */
		guint32 getspi_spi;

		/*
		 * Data specific for DELETE message...
		 */
		struct {
			/*
			 * Requested mode for SA (tunnel or transport)
		 	 */
			guint8 mode;

			/*
			 * Protocol that is requested. One of ESP or AH.
			 */
			gint16 protocol;

			/*
			 * Source and destination addresses from a
			 * request...
			 */
			struct netaddr *saddr;
			struct netaddr *daddr;

			/*
			 * Traffic selectors from a policy that
			 * triggered acquire message.
			 */
			struct ts *s_ts;
			struct ts *d_ts;

			/*
			 * ID of a policy that triggered acquire message.
			 */
			guint32 policy_id;

			/*
			 * Req ID of a policy that triggered acquire message.
			 * (if non-zero)!
			 */
			guint32 reqid;

			/*
			 * In case it's tunnel mode, here are tunnel's ends...
			 */
			struct netaddr *tunnel_saddr;
			struct netaddr *tunnel_daddr;
		} delete;

		/*
		 * Data specific for SPDDUMP and SPDGET messages...
		 */
		struct {
			/*
			 * Requested mode for SA (tunnel or transport)
		 	 */
			guint8 mode;

			/*
			 * Protocol that is requested. One of ESP or AH.
			 */
			gint16 protocol;

			/*
			 * One of IN, OUT or FWD
			 */
			guint8 dir;

			/*
			 * Traffic selectors from a policy that
			 * triggered acquire message.
			 */
			struct ts *tsi;
			struct ts *tsr;

			/*
			 * ID of a policy that triggered acquire message.
			 */
			guint32 policy_id;

			/*
			 * Req ID of a policy that triggered acquire message.
			 * (if non-zero)!
			 */
			guint32 reqid;

			/*
			 * In case it's tunnel mode, here are tunnel's ends...
			 */
			struct netaddr *tunnel_saddr;
			struct netaddr *tunnel_daddr;
		} spd;
	};
};

/*******************************************************************************
 * PFKEY_MSG FUNCTIONS
 ******************************************************************************/
struct pfkey_msg *pfkey_msg_new(void);
void pfkey_msg_free(struct pfkey_msg *);
void pfkey_msg_dump(struct pfkey_msg *);

#endif /* __PFKEY_MSG_H */
