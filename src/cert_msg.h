/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CERT_MSG_H
#define __CERT_MSG_H

#define CERT_MSG 8

/*
 * Structure placed into queue when download finishes in cert module.
 */
struct cert_msg {
	/*
	 * Message type discriminator
	 */
	guint8 msg_type;
	/*
	 * Download request success (only true if every item downloaded)
	 */
	gboolean success;
	/*
	 * Pointer to session that initiated download 
	 */
	gpointer session;
	/*
	 * Pointer to the message that this session received
	 * (to preserve the context)
	 */
	gpointer sm_msg;
};

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
struct cert_msg *cert_msg_alloc(void);
void cert_msg_free(struct cert_msg *);

#endif /* __CERT_MSG_H */
