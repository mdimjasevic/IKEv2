/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/**
 * \file message.c
 *
 * \brief Functions used to create and parse IKEv2 messages
 *
 * This file contains functions that are used by state machines to parse
 * and create IKEv2 messages. There is a function per message. So, for
 * example, IKE SA INIT exchange consists of two messages, request - sent by
 * an initiator, and response - sent by a responder. So, there are two
 * functions, one to create and send IKE SA INIT request message (called
 * message_send_ike_sa_i) and one to send IKE SA INIT response message
 * (called, and now something totaly different, message_send_ike_sa_r).
 * Likewise, there are two separate functions for parsing those messages.
 * One parses request and the other response. The same goes for IKE AUTH
 * exchange.
 *
 * CREATE CHILD SA exchange is a bit different. Namely, it is used for three
 * different purposes. The first is to rekey existing child SA. The second
 * is used to create new child SA, and finally, the third form is used to
 * rekey IKE SA. Thus, there are actually twelve functions for CREATE CHILD
 * SA exchange (three types, each has request and response, each requiring
 * create/send and parse functionality, thus, 3 x 2 x 2 = 12).
 *
 * Apart of those main functions. There are different auxiliary functions
 * also. For example, while creating IKEv2 message it can be very complicated
 * to deduce what next payload would be. To make it easier for understanding
 * and thus to introduce new payloads, the functionality of the more complex
 * functions used to create messages is split into two parts/functions. The
 * first part deduces what is the next payload based on the current one (and
 * of course, on the session to which this message belongs) while the other
 * part does actual composition in a while loop that iterates until there is
 * no next payload (IKEV2_PAYLOAD_NONE).
 */

#define __MESSAGE_C

#define LOGGERNAME	"message"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#include <linux/udp.h>
#ifndef SOL_UDP
#	define SOL_UDP 17
#endif
#include <sys/time.h>

#include <openssl/rand.h>
#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "timeout.h"
#include "netlib.h"
#include "transforms.h"
#include "proposals.h"
#include "ts.h"
#include "auth.h"
#include "cert.h"
#include "config.h"
#include "network_msg.h"
#include "network.h"
#if defined(CFG_MODULE) || defined(CFG_CLIENT)
#include "cfg.h"
#endif /* defined(CFG_MODULE) || define(CFG_CLIENT) */
#include "payload.h"
#include "sad.h"
#include "supplicant.h"
#include "session.h"
#include "message_msg.h"
#include "message.h"
#include "aes_xcbc.h"
#include "crypto.h"
#ifdef SUPPLICANT
#include "supplicant.h"
#endif /* SUPPLICANT */
#ifdef AAA_CLIENT
#include "aaa_wrapper.h"
#endif /* AAA_CLIENT */
#ifdef CFG_MODULE
#include "cfg_module.h"
#endif /* CFG_MODULE */
#ifdef CFG_CLIENT
#include "cfg_client.h"
#endif /* CFG_CLIENT */
#ifdef DMALLOC
#include "dmalloc.h"
#endif

/**
 * Structure with all the private data for messaging subsystem
 */
struct message_data *message_data = NULL;

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE TIMEOUT_MSG STRUCTURE
 ******************************************************************************/

/**
 * Allocate memory for timeout message.
 *
 * \return	Pointer to a new timeout message structure
 *		or NULL in case of an error.
 */
struct timeout_msg *timeout_msg_alloc(void)
{
	struct timeout_msg *to;

	LOG_FUNC_START(1);

	to = g_malloc0(sizeof(struct timeout_msg));

	to->msg_type = TIMEOUT_MSG;

	LOG_FUNC_END(1);

	return to;
}

/**
 * Free memory occupied by timeout message.
 *
 * @param to	 Pointer to a message structure
 */
void timeout_msg_free(struct timeout_msg *to)
{

	LOG_FUNC_START(1);

	if (to)
		g_free (to);

	LOG_FUNC_END(1);
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SENT_MSG STRUCTURE
 ******************************************************************************/

/**
 * Allocate new sent_msg structure
 */
struct sent_msg *sent_msg_new(void)
{
	struct sent_msg *sm;

	sm = g_malloc0(sizeof(struct sent_msg));

	sm->refcnt = 1;

	sm->lock = g_mutex_new();

	return sm;
}

/**
 * Free memory occupied by sent_msg queue message.
 *
 * @param sent	 Pointer to a sent structure
 */
void sent_msg_free(struct sent_msg *sent)
{
	if (sent) {

		sent_msg_lock(sent);

		if (sent_msg_unref_and_test(sent)) {
			
			LOG_DEBUG("Freeing sent_msg %p (id=%u)", 
					sent, sent->msg_id);

			if (sent->to)
				timeout_cancel(sent->to);

			if (sent->daddr)
				netaddr_free(sent->daddr);

			if (sent->packet)
				netaddr_free(sent->packet);

			if (sent->lock) {
				sent_msg_unlock(sent);
				g_mutex_free(sent->lock);
			}

			if (sent->session)
				session_free(sent->session);

			g_free(sent);

		} else
			sent_msg_unlock(sent);
	}
}

/**
 * Lock sent_msg structure
 */
void sent_msg_lock(struct sent_msg *sent)
{
	if (sent && sent->lock)
		g_mutex_lock(sent->lock);
}

/**
 * Unlock sent_msg structure
 */
void sent_msg_unlock(struct sent_msg *sent)
{
	if (sent && sent->lock)
		g_mutex_unlock(sent->lock);
}

/**
 * Increase reference count of a structure
 */
void sent_msg_ref(struct sent_msg *sent)
{
	g_atomic_int_inc(&sent->refcnt);
}

/**
 * Decrease reference count of a structure
 *
 * @param sent
 *
 * \return TRUE if the reference counter is on zero, otherwise FALSE
 */
gboolean sent_msg_unref_and_test(struct sent_msg *sent)
{
	LOG_DEBUG("refcnt %d", sent->refcnt);
	return g_atomic_int_dec_and_test(&sent->refcnt);
}

/**
 * Compare to sent_msg structures by msg_id field in reverse order.
 */
gint sent_cmp_msgid(gconstpointer _s1, gconstpointer _s2)
{
	const struct sent_msg *s1 = _s1;
	const struct sent_msg *s2 = _s2;

	return s1->msg_id - s2->msg_id;
}

/*******************************************************************************
 * FUNCTIONS FOR MANIPULATING COOKIES
 ******************************************************************************/

/**
 * Calculate cookie value
 *
 * @param msg
 * @param secret
 * @param digest
 *
 * TODO: Maybe AES replace with SHA-1 or MD5 because of a possible longer
 * secret len...
 *
 * Calculate cookie and check if it's a valid one. We are calculating cookie
 * over following data:
 *
 *	  &lt;secret&gt(64) | i_spi(8) | src ip address(16) |
 *		&lt;received message without header and cookie payload&gt;
 *
 */
void _message_calculate_cookie(struct message_msg *msg, char *secret,
		char *digest)
{
	guint buf_len;
	unsigned char *p, *buffer;

	buf_len = MESSAGE_SECRET_LEN + 8 + 16 +
			msg->size - sizeof(struct ikev2_header);

	if (msg->ike_sa_init.cookie)
		buf_len -= sizeof(struct payload_notify) + MESSAGE_COOKIE_LEN;

	/*
	 * Round buf_len to block size used for algorithm (128 bits for AES)
	 */
	buf_len = (buf_len + 127) & ~127;

	p = buffer = g_malloc0(buf_len);

	/*
	 * Copy secret into buffer...
	 */
	memcpy(p, secret, MESSAGE_SECRET_LEN);
	p += MESSAGE_SECRET_LEN;

	/*
	 * Copy i_spi into buffer
	 */
	memcpy(p, &msg->i_spi, 8);
	p += 8;

	/*
	 * Copy ip address into buffer
	 */
	memcpy(p, netaddr_get_ipaddr(msg->srcaddr),
			netaddr_get_ipaddr_size(msg->srcaddr));
	p += netaddr_get_ipaddr_size(msg->srcaddr);

	if (msg->ike_sa_init.cookie)
		memcpy(p, msg->ike_sa_init.cookie + MESSAGE_COOKIE_LEN,
				msg->size - MESSAGE_COOKIE_LEN -
				sizeof(struct ikev2_header) -
				sizeof(struct payload_notify));
	else
		memcpy(p, msg->buffer + sizeof(struct ikev2_header),
				msg->size - sizeof(struct ikev2_header));

	LOG_DEBUG("Data for calculating COOKIE value");
	log_buffer(LOGGERNAME, (char *)buffer, buf_len);

	/*
	 * TODO: Maybe we should use SHA or similar function?
	 */
	aes_xcbc_mac_128((unsigned char *)secret, MESSAGE_SECRET_LEN,
		buffer, buf_len, digest);

	g_free(buffer);	
}

/**
 * Calculate cookie value
 *
 * @param msg
 *
 */
void message_calculate_cookie(struct message_msg *msg)
{
	msg->ike_sa_init.expected_cookie = g_malloc(MESSAGE_COOKIE_LEN);

	_message_calculate_cookie(msg, message_data->secret,
			msg->ike_sa_init.expected_cookie);
}

/**
 * Check if given cookie is a valid one
 *
 * @param msg
 *
 * \return
 */
gboolean message_check_cookie(struct message_msg *msg)
{
	gboolean retval;

	LOG_FUNC_START(1);

	LOG_DEBUG("msg->cookie");
	log_data(LOGGERNAME, msg->ike_sa_init.cookie,
			MESSAGE_COOKIE_LEN);
	LOG_DEBUG("msg->expected_cookie");
	log_data(LOGGERNAME, msg->ike_sa_init.expected_cookie,
			MESSAGE_COOKIE_LEN);
	LOG_DEBUG("data->secret");
	log_data(LOGGERNAME, message_data->secret,
			MESSAGE_SECRET_LEN);
	LOG_DEBUG("data->old_secret");
	log_data(LOGGERNAME, message_data->old_secret,
			MESSAGE_SECRET_LEN);

	retval = TRUE;

	if (!memcmp(msg->ike_sa_init.expected_cookie, msg->ike_sa_init.cookie,
			MESSAGE_COOKIE_LEN))
		goto out;

	_message_calculate_cookie(msg, message_data->old_secret,
				msg->ike_sa_init.expected_cookie);

	if (!memcmp(msg->ike_sa_init.expected_cookie, msg->ike_sa_init.cookie,
			MESSAGE_COOKIE_LEN))
		goto out;

	retval = FALSE;

out:
	LOG_FUNC_END(1);

	return retval;
}

gpointer message_regenerate_secret_thread(gpointer _data)
{
	LOG_FUNC_START(1);

	if (message_data->shutdown)
		goto out;

	memcpy(message_data->old_secret, message_data->secret,
			MESSAGE_SECRET_LEN);

	/*
	 * FIXME: Maybe we shouldn't abort thread but try again later?
	 */
	if (!rand_bytes((guchar *)message_data->secret, MESSAGE_SECRET_LEN))
		LOG_ERROR("Error while generating random data"
				" for secret. Aborting thread!");

out:
	LOG_FUNC_END(1);

	return NULL;
}

/*******************************************************************************
 * FUNCTIONS FOR CONSTRUCTING AND PARSING DIFFERENT IKEv2 MESSAGES...
 *
 * All those functions, given session (and where appropriate sad_item structure)
 * construct and send a message.
 ******************************************************************************/

/**
 * Construct and send COOKIE message...
 *
 * @param msg
 *
 * \return
 *
 * Note that hash transform used in this function has nothing to do with
 * transforms defined and/or negotiated for IKE SA and/or CHILD SA. It's
 * only requirement is that it's of good quality and fast because this
 * function is supposedly called under DOS attack.
 */
int message_send_cookie(struct message_msg *msg)
{
	char *p, *buffer;
	struct ikev2_header *hdr;
	guint16 msg_length;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate message header with data
	 */
	hdr->i_spi = msg->i_spi;
	hdr->r_spi = 0;
	hdr->next = IKEV2_PAYLOAD_NOTIFY;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_SA_INIT;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	hdr->msg_id = 0;

	p += sizeof(struct ikev2_header);

	/*
	 * Add COOKIE NOTIFY.
	 */
	p += payload_notify_create(IKEV2_PAYLOAD_NONE, N_COOKIE, 
			msg->ike_sa_init.expected_cookie, MESSAGE_COOKIE_LEN,
			0, 0, p);

	/*
	 * Set length and generate packet
	 */
	msg_length = p - buffer;
	hdr->length = htonl(msg_length);

	buffer = g_realloc(buffer, msg_length);

	/*
	 * TODO: The sending of the message has to be done via
	 * message_send_packet!
	 */
	network_send_packet(msg->ns, buffer, msg_length, msg->srcaddr);

	LOG_FUNC_END(1);

	return 0;
}

/**
 * Function used for sending keepalive packets. Called when
 * NAT box is detected in front of peer.
 *
 * @param session
 *
 * \return
 *
 */
int message_send_keepalive_packet(struct session *session)
{
	char data;

	LOG_FUNC_START(1);

	data = 0xFF;

	/*
	 * ERROR: Keepalive packet has to go from the port 4500
	 * and not from the port 500 (send_socket_natt socket is
	 * bound to a port 4500).
	 */
	if (message_data->natt_send_sock != NULL)
		network_send_packet(message_data->natt_send_sock,
			&data, sizeof(data),
			session_get_r_addr(session));
	else
		LOG_ERROR("No socket defined!");

	LOG_FUNC_END(1);

	return 0;
}


/**
 * Create IKE SA INIT request
 *
 * @param session
 * @param msg
 *
 * \return
 *
 * Note that this function checks for that input parameter message_msg, and
 * if it's non-NULL than assumes there is cookie value in it that has to be
 * incorporated into the request message.
 */
int message_send_ike_sa_init_i(struct session *session,
			struct message_msg *msg)
{
	char *buffer, *p;
	struct ikev2_header *hdr;
	guint16 msg_length;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate message header with data
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = 0;
	if (msg && msg->ike_sa_init.cookie)
		hdr->next = IKEV2_PAYLOAD_NOTIFY;
	else
		hdr->next = IKEV2_PAYLOAD_SA;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_SA_INIT;
	hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->msg_id = 0;

	p += sizeof(struct ikev2_header);

	/*
	 * If we have cookie put it into payload
	 */
	if (msg && msg->ike_sa_init.cookie)
		p += payload_notify_create(IKEV2_PAYLOAD_SA, N_COOKIE,
				msg->ike_sa_init.cookie,
				msg->ike_sa_init.cookie_len, 0, 0, p);

	p += payload_sa_create(IKEV2_PAYLOAD_KE, session_get_proposals(session),
				0, 0, p);

	p += payload_ke_create(IKEV2_PAYLOAD_NONCE, session->dh_group, 
				session->dh, p);

#ifdef NATT
	/*
	 * Here NAT notification payload has to be inserted if NAT-T is
	 * supported for this IKE SA
	 *
	 * NAT_DETECTION_SOURCE_IP, NAT_DETECTION_DESTINATION_IP
	 */
	if (session->nat_src_hashes) {

		p += payload_nonce_create(IKEV2_PAYLOAD_NOTIFY, 
						session->i_nonce, p);

		while(session->nat_src_hashes) {

			p += payload_notify_create(IKEV2_PAYLOAD_NOTIFY,
					N_NAT_DETECTION_SOURCE_IP, 
					session->nat_src_hashes->data,
					SHA_DIGEST_LENGTH,
					0, 0, p);

			g_free(session->nat_src_hashes->data);

			session->nat_src_hashes = g_slist_remove(
						session->nat_src_hashes,
						session->nat_src_hashes->data);
		}

		/*
		 *  N_NAT_DETECTION_.. instead of NAT_DETECTION_.. used!
		 * 
		 * Maybe we should check whether session->nat_dest_hashes is
		 * 	not NULL.. 
		 */
		p += payload_notify_create(IKEV2_PAYLOAD_NONE,
					N_NAT_DETECTION_DESTINATION_IP,
					session->nat_dest_hashes,
					SHA_DIGEST_LENGTH,
					0, 0, p);

	} else
#endif /* NATT */
		p += payload_nonce_create(IKEV2_PAYLOAD_NONE, 
						session->i_nonce, p);

	/*
	 * Set total length, and reallocate memory to fit packet size
	 */
	msg_length = p - buffer - 4;
	hdr->length = htonl(msg_length);

	/*
	 * A copy of this msg is stored for auth
	 * FIXME: This is probably unnecessary...
	 */
	if (session->ikesa_init_i)
		g_free (session->ikesa_init_i);

	session->ikesa_init_i = g_memdup(buffer + 4, msg_length);
	session->ikesa_init_i_len = msg_length;

	/*
	 * Initially, we are accessing port 500, and later, in case of NAT, we
	 * change port to 4500. The only question is, if this is a right place
	 * to set this information?
	 */
	netaddr_set_port(session_get_r_addr(session), IKEV2_PORT);

	message_send_packet(msg ? msg->ns : NULL,
			session, buffer, msg_length + 4,
			session_get_r_addr(session));

	LOG_FUNC_END(1);

	return 0;
}

/**
 *
 */
int message_send_ike_sa_init_r(struct session *session,
		struct message_msg *message_msg)
{
	char *p, *buffer;
	struct ikev2_header *hdr;
	GSList *sig_ca_items = NULL;
	GSList *hash_ca_items = NULL;
	GSList lproposal;
	guint16 msg_length;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate message header with data
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_SA;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_SA_INIT;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	hdr->msg_id = htonl(message_msg->msg_id);

	p += sizeof(struct ikev2_header);

	/*
	 * Selected transforms should be generated from proposals announced
	 * by the peer, and proposals defined in configuration file for
	 * given peer.
	 */
	lproposal.data = session_get_selected_proposal(session);
	lproposal.next = NULL;
	p += payload_sa_create(IKEV2_PAYLOAD_KE, &lproposal, 0, 0, p);

	/*
	 * TODO: Check for dh group (and initialize dhg) according to
	 * local policy (configuration file) and remote offer.
	 */
	p += payload_ke_create(IKEV2_PAYLOAD_NONCE, session->dh_group, 
				session->dh, p);

#ifdef NATT
	/*
	 * NAT support.
	 */		
	if (session->nat_src_hashes) {

		p += payload_nonce_create(IKEV2_PAYLOAD_NOTIFY,
					session->r_nonce, p);

		p += payload_notify_create(IKEV2_PAYLOAD_NOTIFY,
					N_NAT_DETECTION_SOURCE_IP,
					session->nat_src_hashes->data,
					SHA_DIGEST_LENGTH,
					0, 0, p);

		/*
		 * Perhaps we should check whether 
		 * session->nat_dest_hashes is not NULL!
		 * 
		 * I think it's a BUG in that case, so, there is no need
		 * to check it, though, some assertion can be inserted.
		 * TODO: Remove this comment in the next revision.
		 */

		p += payload_notify_create(session->cg && session->cg->ca_list
						? IKEV2_PAYLOAD_NOTIFY
						: IKEV2_PAYLOAD_NONE,
						N_NAT_DETECTION_DESTINATION_IP,
						session->nat_dest_hashes,
						SHA_DIGEST_LENGTH,
						0, 0, p);
	} else
#endif /* NATT */
	/* TODO: define more detailed condition for notify payload once 
	 * LOOKUP_SUPPPORTED option is included in daemon configuration
	 */
		p += payload_nonce_create(session->cg && session->cg->ca_list
						? IKEV2_PAYLOAD_NOTIFY
						: IKEV2_PAYLOAD_NONE,
					session->r_nonce, p);

	/*
	 * If at least one CA is configured, the CERTREQ payload is sent due
	 * to t	he giving opportunity to peer to authenticate with the RSA based
	 * authenication (while in the same time we can authenticate either with
	 * the PSK or the RSA authentication method) [rfc4306, 2.15].
	 *
	 * If we have configured more than one CA, public keys of the CAs with 
	 * the same Encoding type are sent in the same CERTREQ as concatenated
	 * list of SHA1 hashes [rfc4306, 3.7].
	 *
	 * CERTREQ payload(s) are formed only based on the
	 * config_ikesa->ca_items and not on the CA directory (path
	 * certificate configuration parameter).
	 */
	if (session->cg && session->cg->ca_list != NULL) {

		if (config_find_ca_by_type(session->cg->ca_list, 
					&sig_ca_items, &hash_ca_items)) {

			/*
			 * We support Hash-and-URL type of x509 certificates;
			 * therefore we send Notify HTTP_CERT_LOOKUP_SUPPORTED
			 * by default.
			 */
			if ((sig_ca_items != NULL) || (hash_ca_items != NULL)) {
				p += payload_notify_create(
						IKEV2_PAYLOAD_CERTREQ,
						N_HTTP_CERT_LOOKUP_SUPPORTED,
						NULL, 0, 0, 0, p);
				LOG_DEBUG("Created HTTP_CERT_LOOKUP_SUPPORTED notify");
			}

			if (sig_ca_items != NULL)
				p += payload_certreq_create(
					g_slist_length(hash_ca_items)
						?  IKEV2_PAYLOAD_CERTREQ
						: IKEV2_PAYLOAD_NONE,
					IKEV2_CERT_X509_SIGNATURE,
					sig_ca_items, p);

			if (hash_ca_items != NULL)
				p += payload_certreq_create(IKEV2_PAYLOAD_NONE,
						IKEV2_CERT_HASH_AND_URL,
						hash_ca_items, p);

			cert_ca_items_remove(sig_ca_items);
			cert_ca_items_remove(hash_ca_items);
		}
	}

	/*
	 * Set length and generate packet
	 */
	msg_length = p - buffer - 4;
	hdr->length = htonl(msg_length);

	/*
	 * A copy of this msg is stored for auth
	 */
	session->ikesa_init_r = g_memdup(buffer + 4, msg_length);
	session->ikesa_init_r_len = msg_length;

	message_send_packet(message_msg->ns, session, buffer, msg_length + 4,
			session_get_i_addr(session));

	LOG_FUNC_END(1);

	return 0;
}

/**
 * This function constructs a message to be used for authentication
 * purposes.
 *
 * @param prf_alg	PRF algorithm to use for calculations
 * @param packet	One of the packets in IKE SA exchange
 * @param packet_len	Number of octets in packet
 * @param payloadid	ID payload with header
 * @param payloadid_len	ID payload
 * @param nonce		Nonce value for calculation
 * @param sk_p		SK_pi or SK_pr depending for whom we are
 *			constructing authentication data. The length
 *			of this key can be deduced from the PRF
 *			function.
 * @param msg		Return buffer. It has to be freed with call
 *			to g_free().
 *
 * \return Size of message for authentication or -1 in case of an error
 *
 * Note that this function is generic, i.e. it constructs a message
 * that has to be signed or MAC-ed depending on selected authentication
 * protocol.
 *
 * The message returned by this function is one of those:
 *
 *	MSGi = ikesa_init_i message | Nr | prf(SK_pi, IDi')
 *	MSGr = ikesa_init_r message | Ni | prf(SK_pr, IDr')
 */
int construct_auth_data(transform_t *prf_alg,
		char *packet, int packet_len,
		gpointer *payloadid, int payloadid_len,
		BIGNUM *nonce, char *sk_p,
		char **msg)
{
	char *prf_id;
	int prf_id_len, id_len, nonce_len, retval;

#warning "This function has to cope with different types of IDs"
	LOG_FUNC_START(1);

	retval = -1;

	LOG_DEBUG("PRF key used for authentication");
	log_buffer(LOGGERNAME, sk_p, transform_get_key_len(prf_alg));

	id_len = payloadid_len - sizeof(struct payload_generic_header);

	nonce_len = BN_num_bytes(nonce);

	prf_id_len = prf(prf_alg, (guchar *)sk_p,
				transform_get_key_len(prf_alg),
				(unsigned char *)payloadid +
					sizeof(struct payload_generic_header),
				id_len, &prf_id);

	*msg = g_malloc(packet_len + nonce_len + prf_id_len);

	memcpy(*msg, packet, packet_len);

	if (BN_bn2bin(nonce, (guchar *)*msg + packet_len) != nonce_len) {
		LOG_ERROR("Out of memory");
		g_free(*msg);
		*msg = NULL;
		goto out;
	}

	memcpy(*msg + packet_len + nonce_len, prf_id, prf_id_len);

	LOG_DEBUG("Data to be signed or MACed");
	log_buffer(LOGGERNAME, *msg, packet_len + nonce_len + prf_id_len);

	retval = packet_len + nonce_len + prf_id_len;

out:
	g_free(prf_id);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Calculate AUTH data
 *
 * @param prf_alg	PRF algorithm to use for calculations
 * @param auth_data	Authentication data, including key
 * @param payloadid	ID payload with header
 * @param payloadid_len	ID payload
 * @param packet	One of the packets in IKE SA exchange
 * @param packet_len	Number of octets in packet
 * @param nonce		Nonce value for calculation
 * @param sk_p		SK_pi or SK_pr depending for whom we are
 *			constructing authentication data. The length
 *			of this key can be deduced from the PRF
 *			function.
 * @param auth_payload	Return buffer. It has to be freed with call
 *			to g_free().
 *
 * \return Size of message for authentication or -1 in case of an error
 *
 */
int auth_data_calculate(transform_t *prf_alg,
		struct auth_data *auth_data,
		gpointer *payloadid, int payloadid_len,
		char *packet, int packet_len,
		BIGNUM *nonce, char *sk_p, char **auth_payload)
{
	char *msg1, *kpad, md[SHA1_DIGEST_SIZE];
	int msg1_len, kpad_len, auth_payload_len, err;
	FILE *pkfp;
	EVP_PKEY *rpk;

	LOG_FUNC_START(1);

	auth_payload_len = -1;

	LOG_DEBUG("Key used for authentication");
	log_buffer(LOGGERNAME, auth_data->key, auth_data->key_len);

	msg1_len = construct_auth_data(prf_alg, packet, packet_len,
				payloadid, payloadid_len,
				nonce, sk_p, &msg1);
	if (msg1_len < 0)
		goto out;

	switch(auth_data->auth_type) {
	case IKEV2_AUTH_EAP:

		kpad_len = prf(prf_alg, (guchar *)auth_data->key,
				auth_data->key_len,
				(guchar *)"Key Pad for IKEv2", 17, &kpad);
		if (kpad_len < 0)
			goto out_free;

		auth_payload_len = prf(prf_alg, (guchar *)kpad, kpad_len,
				(guchar *)msg1, msg1_len, auth_payload);

		g_free(kpad);
		break;

	case IKEV2_AUTH_PSK:

		kpad_len = prf(prf_alg, (guchar *)auth_data->key,
				auth_data->key_len,
				(guchar *)"Key Pad for IKEv2", 17, &kpad);
		if (kpad_len < 0)
			goto out_free;

		auth_payload_len = prf(prf_alg, (guchar *)kpad, kpad_len,
				(guchar *)msg1, msg1_len, auth_payload);

		g_free(kpad);

		break;

	case IKEV2_AUTH_RSA:

		/*
		 * FIXME: This function call, as well as all the call
		 * directly into OpenSSL should be moved to cert.c!
		 */
		if (!(pkfp = fopen(auth_data->priv_key_file, "r"))) {
			LOG_ERROR("Could not open private key file (%s)",
					auth_data->priv_key_file);
			goto out_free;
		}

		rpk = PEM_read_PrivateKey(pkfp, NULL, NULL, NULL);
		fclose (pkfp);

		if (!rpk)
			goto out_free;

		*auth_payload = g_malloc(RSA_size(rpk->pkey.rsa));

		SHA1((guchar *)msg1, msg1_len, (guchar *)md);
		err = RSA_sign(NID_sha1, (guchar *)md, SHA1_DIGEST_SIZE,
				(guchar *)*auth_payload,
				(guint *)&auth_payload_len, rpk->pkey.rsa);

		if (!err)
			auth_payload_len = -1;

		break;

	case IKEV2_AUTH_DSS:
	default:
		LOG_ERROR("Unsupported authentication mechanism");
		return -1;
	}

out_free:
	g_free(msg1);

	LOG_DEBUG("Calculated authentication payload");
	log_buffer(LOGGERNAME, *auth_payload, auth_payload_len);

out:

	LOG_FUNC_END(1);

	return auth_payload_len;
}

/**
 * Verify AUTH data
 *
 * @param prf_alg		PRF algorithm to use for calculations
 * @param auth_data
 * @param id
 * @param packet		One of the packets in IKE SA exchange
 * @param packet_len		Number of octets in packet
 * @param nonce			Nonce value for calculation
 * @param key			Shared key for calculation
 * @param key_len		Size of a shared key
 * @param auth_payload
 * @param auth_payload_len
 *
 * \return	-1	if an error occured
 *		1	if authentication didn't succedded
 *		0	if authentication was successful
 *
 */
int auth_data_verify(transform_t *prf_alg,
		struct auth_data *auth_data,
		struct id *id,
		char *packet, int packet_len,
		BIGNUM *nonce, char *sk_p,
		char *auth_payload, int auth_payload_len)
{
	char *auth_expected, *msg1;
	unsigned char *pubkey;
	gint result, auth_len, payloadid_len;
	RSA *rsa;
	unsigned char md[SHA1_DIGEST_SIZE];
	unsigned long msg1_len;
	gpointer payloadid;

	LOG_FUNC_START(1);

	result = -1;
	pubkey = NULL;

	LOG_DEBUG("Expected authentication payload");
	log_buffer(LOGGERNAME, auth_payload, auth_payload_len);

	payloadid = g_malloc(TB_SIZE);
	payloadid_len = payload_id_create(IKEV2_PAYLOAD_NONE, id, payloadid);

	switch(auth_data->auth_type) {
#if defined(SUPPLICANT) || defined(AAA_CLIENT)
	case IKEV2_AUTH_EAP:
#endif /* defined(SUPPLICANT) || defined(AAA_CLIENT) */
	case IKEV2_AUTH_PSK:

		auth_len = auth_data_calculate(prf_alg, auth_data,
				payloadid, payloadid_len,
				packet, packet_len,
				nonce, sk_p, &auth_expected);

		if (auth_len < 0)
			goto out;

		result = 0;
		if (auth_payload_len != auth_len ||
				memcmp(auth_payload, auth_expected, auth_len))
			result = 1;

		g_free(auth_expected);

		break;

	case IKEV2_AUTH_RSA:

		if ((auth_data->public_key == NULL) || 
				(auth_data->public_key_len == 0))
			goto out;

		msg1_len = construct_auth_data(prf_alg, packet, packet_len, 
				payloadid, payloadid_len,
				nonce, sk_p, &msg1);

		if (msg1_len < 0)
			goto out;

		SHA1((unsigned char *)msg1, msg1_len, md);	

		/**
		 * FIXME: There is a warning about 2. arg.
		 */
		pubkey = auth_data->public_key;
		rsa = d2i_RSAPublicKey(NULL, 
				(const unsigned char **)&pubkey,
				auth_data->public_key_len);

		if (rsa == NULL)
			goto out;

		result = 1;
		if (RSA_verify(NID_sha1, md, SHA1_DIGEST_SIZE, 
				auth_payload, auth_payload_len, rsa))
			result = 0;

		RSA_free(rsa);
		g_free(msg1);

		break;

	case IKEV2_AUTH_DSS:
	default:
		LOG_ERROR("Unsupported authentication mechanism");
		break;
	}

out:

	g_free(payloadid);

	LOG_FUNC_END(1);

	return result;
}

/**
 * Based on the current payload, determine the next one in
 * IKE AUTH request message
 *
 * @param session	Session structure
 * @param curr		Current payload (and exact notify if notify payload)
 *
 * \return Next payload
 *
 * In case that current or next payload is NOTIFY, then notify type is
 * shifted left 8 bits and encoded (or'ed) with payload type! The highest
 * eight bits are unused.
 *
 * This code can not be used in case some payload repeates. For example,
 * if there are multiple CERT payloads, this function will indicate that
 * the next payload is CERT, and on the subsequent call it will indicate
 * different payload. It is the task of the callee to put as much as
 * necessary payloads of the same type.
 */
guint32 message_ike_auth_i_payload_next(struct session *session, guint32 curr)
{
	guint32 next;

	switch (curr) {
	case IKEV2_PAYLOAD_ENCRYPTED:
		next = IKEV2_PAYLOAD_IDI;
		break;

	case IKEV2_PAYLOAD_IDI:
		if (session->certs_for_peer) {
			next = IKEV2_PAYLOAD_CERT;
			break;
		}

	case IKEV2_PAYLOAD_CERT:

		/*
		 * We should check here if next payload is
		 * INITIAL_CONTACT notify. But since we do
		 * not support this notify for now, there
		 * is no code yet.
		 */

	case (N_INITIAL_CONTACT << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * TODO: Enter the code that will check if
		 * HTTP_CERT_LOOKUP_SUPPORTED notify should
		 * be sent. Take into account that if there
		 * is no CERTREQ payload, there should be
		 * no HTTP_CERT_LOOKUP_SUPPORTED notify either.
		 */
		if (session->cg->ca_list) {
			next = N_HTTP_CERT_LOOKUP_SUPPORTED << 8 
				| IKEV2_PAYLOAD_NOTIFY;
			break;
		}

	case (N_HTTP_CERT_LOOKUP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		if (session->cg->ca_list) {
			next = IKEV2_PAYLOAD_CERTREQ;
			break;
		}

	case IKEV2_PAYLOAD_CERTREQ:

		/*
		 * Should we send IDr payload? This is quite
		 * straitforward to implement.
		 */

	case IKEV2_PAYLOAD_IDR:

		if (session->auth_data->auth_type != IKEV2_AUTH_EAP) {
			next = IKEV2_PAYLOAD_AUTH;
			break;
		}

	case IKEV2_PAYLOAD_AUTH:

#ifdef CFG_CLIENT
		if (session->icfg) {
			next = IKEV2_PAYLOAD_CONFIG;
			break;
		}

	case IKEV2_PAYLOAD_CONFIG:
#endif /* CFG_CLIENT */

		/*
		 * Here we should introduce code to check if IPCOMP
		 * notify (or notifies) should be sent. For now, we
		 * do not support this feature.
		 */

	case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		if (sad_item_get_ipsec_mode(session_get_sad_item(session)) ==
				IPSEC_MODE_TRANSPORT) {
			next = N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

	case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here we should introduce code to check if
		 * ESP_TFC_PADDING_NOT_SUPPORTED notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here a check should be made if notify
		 * NON_FIRST_FRAGMENTS_ALSO should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):

		next = IKEV2_PAYLOAD_SA;
		/*
		 * FIXME
		 * We assume next payload is IKEV2_PAYLOAD_SA and change
		 * it to N_MOBIKE_SUPPROTED if peer has to include it.
		 */
#ifdef MOBIKE
		if(session->mobike_supported &&
		   session->ike_auth_exchange_counter == 1)
			next = N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY;
#endif /* MOBIKE */
		break;

#ifdef MOBIKE
	case (N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv4_addresses_ptr)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else if(session->local_ipv6_addresses_ptr)
			next = (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

	case (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv4_addresses_ptr->next)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else if(session->local_ipv6_addresses_ptr)
			next = (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

	case (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv6_addresses_ptr->next)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

#endif /* MOBIKE */

	case IKEV2_PAYLOAD_SA:

		next = IKEV2_PAYLOAD_TSI;
		break;

	case IKEV2_PAYLOAD_TSI:

		next = IKEV2_PAYLOAD_TSR;
		break;

	case IKEV2_PAYLOAD_TSR:

		/*
		 * Should we send notify ADDITIONAL_TS_POSSIBLE?
		 * As for now we don't support this feature, there is
		 * no code yet.
		 */

	case (N_ADDITIONAL_TS_POSSIBLE << 8 | IKEV2_PAYLOAD_NOTIFY):

	default:
		next = IKEV2_PAYLOAD_NONE;
	}

	return next;
}

/**
 * Create and send IKE AUTH request
 *
 * @param session
 *
 * \return	0 if request has been successfuly sent,
 *		1 try later,
 *		-1 in case of an error
 *
 * At the moment, return value 1 is not used.
 */
int message_send_ike_auth_i(struct session *session)
{
	gint retval;
	guint32 next, curr;
	gpointer payloadid = NULL;
	char *p, *buffer, *encrp = NULL, *chksum, *auth_payload;
	gint len = 0, auth_payload_len;
	struct sad_item *si;
	struct ikev2_header *hdr;
	guint32 enclen, ilen, payloadid_len = 0;
	GSList *certs_for_peer;
	GSList *sig_ca_items = NULL, *hash_ca_items = NULL;
#ifdef MOBIKE
	/*
	 * IPv4 and IPv6 address buffers
	 */
	unsigned long addr;
	char addr6[16];
	char ispis[100];

	struct in_addr *addr4_data;
	struct in6_addr *addr6_data;
#endif /* MOBIKE */

	LOG_FUNC_START(1);

#ifdef MOBIKE
	session->ike_auth_exchange_counter++;

	if(netaddr_get_family(session_get_i_addr(session)) == AF_INET)
		session->local_ipv4_addresses =
			netaddr_remove_from_list(session_get_i_addr(session),
				session->local_ipv4_addresses);
	else
		session->local_ipv6_addresses =
			netaddr_remove_from_list(session_get_i_addr(session),
				session->local_ipv6_addresses);

	session->local_ipv4_addresses_ptr = session->local_ipv4_addresses;
	session->local_ipv6_addresses_ptr = session->local_ipv6_addresses;
#endif /* MOBIKE */
	retval = -1;
	buffer = NULL;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	/*
	 * Find corresponding CHILD SA structure
	 */
	si = session_get_sad_item(session);

	hdr = (struct ikev2_header *)p;
	p += sizeof(struct ikev2_header);

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(session_get_next_msgid(session));

	curr = IKEV2_PAYLOAD_ENCRYPTED;
	next = message_ike_auth_i_payload_next(session, curr);

	while (curr != IKEV2_PAYLOAD_NONE) {

		switch (curr) {

		case IKEV2_PAYLOAD_SA:

			len = payload_sa_create(next,
					sad_item_get_proposals(si),
					0, 0, p);
			LOG_TRACE("Created SA payload, len = %u", len);
			break;

		case IKEV2_PAYLOAD_IDI:
			payloadid = p;

			len = payload_id_create(next, session->cp->lid, p);
			LOG_TRACE("Created ID payload, len = %u", len);

			payloadid_len = len;

			break;

		case IKEV2_PAYLOAD_IDR:
			break;

		case IKEV2_PAYLOAD_CERT:

			/**
			 * Data for creating the CERT payload(s) is stored in
			 * session->certs_for_peer.
			 */
			certs_for_peer = session->certs_for_peer;
			len = 0;

			while (certs_for_peer) { 

				if (len < 0) {
					LOG_ERROR("Error creating payload!");
					goto out;
				}

				p += len;
				len = payload_cert_create(
						certs_for_peer->next
							? IKEV2_PAYLOAD_CERT
							: next,
						certs_for_peer->data, p);
				LOG_TRACE("Created CERT payload, len = %u",
							len);

				certs_for_peer = certs_for_peer->next;
			}
			LOG_TRACE("Created CERT payload, len = %u", len);
			break;

		case IKEV2_PAYLOAD_CERTREQ:

			if (config_find_ca_by_type(session->cg->ca_list, 
					&sig_ca_items, &hash_ca_items)) {

				len = 0;

				if (sig_ca_items) {
					len = payload_certreq_create(
						hash_ca_items
							? IKEV2_PAYLOAD_CERTREQ
							: next,
						IKEV2_CERT_X509_SIGNATURE,
						sig_ca_items, p);
					LOG_TRACE("Created CERTREQ payload, "
							"len = %u", len);
				}

				/*
				 * Actually, test should be at the end of while
				 * loop only, but for now it's not possible to
				 * do it this way.
				 */
				if (hash_ca_items && len >= 0) {

					/*
					 * Pointer increment should also be
					 * at the end of while loop only, but
					 * as it was already mentioned. For
					 * this case it's not possible.
					 */
					p += len;

					len = payload_certreq_create(next,
							IKEV2_CERT_HASH_AND_URL,
							hash_ca_items, p);
					LOG_TRACE("Created CERTREQ HASH payload"
							" len = %u", len);
				}

				cert_ca_items_remove(sig_ca_items);
				cert_ca_items_remove(hash_ca_items);
			}

			LOG_TRACE("Created CERTREQ payload len = %u", len);
			break;

		case IKEV2_PAYLOAD_AUTH:

			auth_payload_len = auth_data_calculate(
					session->prf_type,
                                       	session->auth_data,
					payloadid, payloadid_len,
					session->ikesa_init_i,
					session->ikesa_init_i_len,
					session->r_nonce,
					session->sk_pi,
					&auth_payload);

			if (auth_payload_len < 0)
				goto out;

			LOG_TRACE("auth_payload=%p, auth_payload_len=%d",
					auth_payload, auth_payload_len);

			LOG_DEBUG("Authentication data to send");
			log_buffer(LOGGERNAME, auth_payload, auth_payload_len);

			len = payload_auth_create(next,
					session->auth_data->auth_type,
					auth_payload, auth_payload_len, p);
			LOG_TRACE("Created AUTH payload len = %u", len);

			g_free(auth_payload);

			break;

		case (N_INITIAL_CONTACT << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_HTTP_CERT_LOOKUP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_create(next,
					N_HTTP_CERT_LOOKUP_SUPPORTED,
					NULL, 0, 0, 0, p);
			LOG_DEBUG("Created HTTP_CERT_LOOKUP_SUPPORTED notify");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			break;

		case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_create(next, N_USE_TRANSPORT_MODE,
						NULL, 0, 0, 0, p);
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			break;

		case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 |
				IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case IKEV2_PAYLOAD_TSI:

			LOG_DEBUG("TSi value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
			len = payload_ts_create(next, sad_item_get_tsl(si), p);
			LOG_TRACE("Created TSI payload len = %u", len);
			break;

		case IKEV2_PAYLOAD_TSR:

			LOG_DEBUG("TSr value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));
			len = payload_ts_create(next, sad_item_get_tsr(si), p);
			LOG_TRACE("Created TSR payload len = %u", len);
			break;

		case IKEV2_PAYLOAD_ENCRYPTED:

			/**
			 * Prepare containter for encrypted payloads
			 */
			len = payload_encr_prepare(next, session->encr_type, p);
			encrp = p;
			break;

#ifdef CFG_CLIENT
		case IKEV2_PAYLOAD_CONFIG:

			len = payload_cfgreq_create(next, session->icfg, p);
			LOG_TRACE("Created CONFIG payload len = %u", len);
			break;
#endif /* CFG_CLIENT */
		/*
		 * FIXME
		 */
#ifdef MOBIKE
		case (N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_mobike_create(next, N_MOBIKE_SUPPORTED,
						0, 0, p);
			LOG_DEBUG("Created MOBIKE_SUPPORTED notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			break;

		case (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):
			addr4_data = (struct in_addr *)netaddr_get_ipaddr(
					(netaddr_t *)(session->local_ipv4_addresses_ptr->data));
			addr = addr4_data->s_addr;

			len = payload_notify_mobike_create(next, N_ADDITIONAL_IP4_ADDRESS,
						(char *)&addr, (guint16)sizeof(addr), p);
			LOG_DEBUG("Created ADDITIONAL_IP4_ADDRESS MOBIKE notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			session->local_ipv4_addresses_ptr =
					session->local_ipv4_addresses_ptr->next;
			break;

		case (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):
			addr6_data = (struct in6_addr *)netaddr_get_ipaddr(
					(netaddr_t *)(session->local_ipv6_addresses_ptr->data));

			len = payload_notify_mobike_create(next, N_ADDITIONAL_IP6_ADDRESS,
						(char *)addr6_data->s6_addr,
						(guint16)sizeof(addr6_data->s6_addr), p);
			LOG_DEBUG("Created ADDITIONAL_IP6_ADDRESS MOBIKE notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			session->local_ipv6_addresses_ptr =
					session->local_ipv6_addresses_ptr->next;
			break;
#endif /* MOBIKE */
		}

		if (len < 0) {
			LOG_ERROR("Error creating payload!");
			goto out;
		}

		p += len;

		curr = next;
		next = message_ike_auth_i_payload_next(session, curr);
	}

	LOG_TRACE("encrp=%p", encrp);

	enclen =  payload_encr_create(session->encr_type,
			session->sk_ei, (guint)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ai,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
				session_get_r_addr(session));

	retval = 0;

out:
	if (retval < 0 && buffer)
		g_free(buffer);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Based on the current payload, determine the next one for
 * IKE AUTH response
 *
 * @param session	Session structure
 * @param curr		Current payload (and exact notify if notify payload)
 *
 * \return Next payload
 *
 * In case that current or next payload is NOTIFY, then notify type is
 * shifted left 8 bits and encoded (or'ed) with payload type! The highest
 * eight bits are unused.
 *
 * This code can not be used in case some payload repeates. For example,
 * if there are multiple CERT payloads, this function will indicate that
 * the next payload is CERT, and on the subsequent call it will indicate
 * different payload. It is the task of the callee to put as much as
 * necessary payloads of the same type.
 */
guint32 message_ike_auth_r_payload_next(struct session *session, guint32 curr)
{
	struct sad_item *si;
	guint32 next;

	switch (curr) {
	case IKEV2_PAYLOAD_ENCRYPTED:
		next = IKEV2_PAYLOAD_IDR;
		break;

	case IKEV2_PAYLOAD_IDR:
		if (session->certs_for_peer) {
			next = IKEV2_PAYLOAD_CERT;
			break;
		}

	case IKEV2_PAYLOAD_CERT:
		next = IKEV2_PAYLOAD_AUTH;
		break;

	case IKEV2_PAYLOAD_AUTH:

#ifdef AAA_CLIENT
		if (session->eap_packet) {
			next = IKEV2_PAYLOAD_EAP;
			break;
		}
#endif /* AAA_CLIENT */

#ifdef CFG_MODULE
		if (session->rcfg) {
			if (session->rcfg == GUINT_TO_POINTER(1) ||
					cfg_proxy_is_error(session->rcfg))
				next = N_INTERNAL_ADDRESS_FAILURE << 8
						| IKEV2_PAYLOAD_NOTIFY;
			else
				next = IKEV2_PAYLOAD_CONFIG;

			break;
		}

	case IKEV2_PAYLOAD_CONFIG:
#endif /* CFG_MODULE */

		/*
		 * Here we should check if the next payload chould be
		 * IPCOMP_SUPPORTED payload. Since we do not support
		 * this mode yet, the code has to be added later.
		 */

	case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		si = session_get_sad_item(session);

		if (!si->spd) {
			next = N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

		if (!si->proposals) {
			next = N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

		if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TRANSPORT) {
			next = N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

	case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here we should introduce code to check if
		 * ESP_TFC_PADDING_NOT_SUPPORTED notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here we should introduce code to check if
		 * NON_FIRST_FRAGMENTS_ALSO notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):

		next = IKEV2_PAYLOAD_SA;
		/*
		 * FIXME
		 * We assume next payload is IKEV2_PAYLOAD_SA and change
		 * it to N_MOBIKE_SUPPROTED if peer has to include it.
		 */
#ifdef MOBIKE
		if(session->mobike_supported &&
		   session->ike_auth_exchange_counter == 1)
			next = N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY;
#endif /* MOBIKE */
		break;

#ifdef MOBIKE
	case (N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv4_addresses_ptr)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else if(session->local_ipv6_addresses_ptr)
			next = (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

	case (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv4_addresses_ptr->next)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else if(session->local_ipv6_addresses_ptr)
			next = (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

	case (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):

		if(session->local_ipv6_addresses_ptr->next)
			next = (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY);
		else
			next = IKEV2_PAYLOAD_SA;
		break;

#endif /* MOBIKE */

	case IKEV2_PAYLOAD_SA:

		next = IKEV2_PAYLOAD_TSI;
		break;

	case IKEV2_PAYLOAD_TSI:

		next = IKEV2_PAYLOAD_TSR;
		break;

	case IKEV2_PAYLOAD_TSR:

		/*
		 * Here we should introduce code to check if
		 * ADDITIONAL_TS_POSSIBLE notify should be sent.
		 * For now, we do not support this feature.
		 */

	case IKEV2_PAYLOAD_EAP:

	case (N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY):
	case (N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY):
	case (N_INTERNAL_ADDRESS_FAILURE << 8 | IKEV2_PAYLOAD_NOTIFY):
	default:
		next = IKEV2_PAYLOAD_NONE;
	}

	return next;
}

/**
 * Create an IKEV2 AUTH msg for responder
 *
 * @param session
 * @param msg
 *
 * \return	0 if everything is OK
 * 		-1 in the case of an error
 */
int message_send_ike_auth_r(struct session *session,
		struct message_msg *msg)
{
	gint retval;
	guint32 curr, next;
	struct sad_item *si;
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint32 len, enclen, ilen;
	char *auth_payload;
	gpointer payloadid = NULL;
	int auth_payload_len, payloadid_len = 0;
	GSList *certs_for_peer;
#ifdef MOBIKE
	/*
	 * IPv4 and IPv6 address buffers
	 */
	unsigned long addr;
	char addr6[16];

	struct in_addr *addr4_data;
	struct in6_addr *addr6_data;
#endif /* MOBIKE */

	LOG_FUNC_START(1);

#ifdef MOBIKE
	session->ike_auth_exchange_counter++;

	if(netaddr_get_family(session_get_r_addr(session)) == AF_INET)
		session->local_ipv4_addresses =
			netaddr_remove_from_list(session_get_r_addr(session),
				session->local_ipv4_addresses);
	else
		session->local_ipv6_addresses =
			netaddr_remove_from_list(session_get_r_addr(session),
				session->local_ipv6_addresses);

	session->local_ipv4_addresses_ptr = session->local_ipv4_addresses;
	session->local_ipv6_addresses_ptr = session->local_ipv6_addresses;

#endif /* MOBIKE */

	/*
	 * Find corresponding CHILD SA structure
	 */
	si = session_get_sad_item(session);

	buffer = NULL;
	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	hdr->i_spi = session_get_i_spi(session);
	hdr->r_spi = session_get_r_spi(session);
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	hdr->msg_id = htonl(msg->msg_id);

	p += sizeof(struct ikev2_header);

	curr = IKEV2_PAYLOAD_ENCRYPTED;
	next = message_ike_auth_r_payload_next(session, curr);

	while (curr != IKEV2_PAYLOAD_NONE) {

		switch (curr) {

		case IKEV2_PAYLOAD_SA:

			len = payload_sa_create(next,
					sad_item_get_proposals(si),
					0, 0, p);
			break;

		case IKEV2_PAYLOAD_IDR:
			payloadid = p;
			len = payload_id_create(next, session->cp->lid, p);
			payloadid_len = len;
			break;

		case IKEV2_PAYLOAD_CERT:

			/**
			 * Data for creating the CERT payload(s) is stored in
			 * session->certs_for_peer.
			 */
			certs_for_peer = session->certs_for_peer;
			len = 0;

			while (certs_for_peer) { 

				if (len < 0) {
					LOG_ERROR("Error creating payload!");
					goto out;
				}

				p += len;
				len = payload_cert_create(
						certs_for_peer->next
							? IKEV2_PAYLOAD_CERT
							: next,
						certs_for_peer->data, p);

				certs_for_peer = certs_for_peer->next;
			}
			break;

		case IKEV2_PAYLOAD_AUTH:

			auth_payload_len = auth_data_calculate(
						session->prf_type,
						session->auth_data,
						payloadid, payloadid_len,
						session->ikesa_init_r,
						session->ikesa_init_r_len,
						session->r_nonce,
						session->sk_pr,
						&auth_payload);

			if (auth_payload_len < 0) {
				LOG_ERROR("Failed to generate "
						"authentication data");
				msg->notify = N_AUTHENTICATION_FAILED;
				goto out;
			}


			LOG_TRACE("auth_payload=%p, auth_payload_len=%d",
					auth_payload, auth_payload_len);

			LOG_DEBUG("Authentication data to send");
			log_buffer(LOGGERNAME, auth_payload, auth_payload_len);

			len = payload_auth_create(next,
					session->auth_data->auth_type,
					auth_payload, auth_payload_len, p);

			g_free(auth_payload);

			break;

		case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_create(next, N_USE_TRANSPORT_MODE,
						NULL, 0, si->spi,
						sad_item_get_protocol(si), p);
			break;

		case (N_INTERNAL_ADDRESS_FAILURE << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_create(next,
						N_INTERNAL_ADDRESS_FAILURE,
						NULL, 0, 0, 0, p);
			break;

		case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 |
				IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case IKEV2_PAYLOAD_TSI:

			LOG_DEBUG("TSi value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));
			len = payload_ts_create(next, sad_item_get_tsr(si), p);
			break;

		case IKEV2_PAYLOAD_TSR:

			LOG_DEBUG("TSr value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
			len = payload_ts_create(next, sad_item_get_tsl(si), p);
			break;

		case IKEV2_PAYLOAD_ENCRYPTED:

			/**
			 * Prepare containter for encrypted payloads
			 */
			len = payload_encr_prepare(next, session->encr_type, p);
			encrp = p;
			break;

#ifdef CFG_MODULE
		case IKEV2_PAYLOAD_CONFIG:

			len = payload_cfgresp_create(next,
					cfg_proxy_get_cfg(session->rcfg),
					session->cp->subnets, p);
			break;
#endif /* CFG_MODULE */

#ifdef AAA_CLIENT
		case IKEV2_PAYLOAD_EAP:

			len = payload_eap_create(next, session->eap_packet,
					session->eap_packet_len, p);
			break;
#endif
		case (N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY):

  			len = payload_notify_create(next, N_TS_UNACCEPTABLE,
						NULL, 0, 0, 0, p);
			break;

		case (N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY):

  			len = payload_notify_create(next, N_NO_PROPOSAL_CHOSEN,
						NULL, 0, 0, 0, p);
			break;

			/*
			 * FIXME
			 */
#ifdef MOBIKE
		case (N_MOBIKE_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

			len = payload_notify_mobike_create(next, N_MOBIKE_SUPPORTED,
					0, 0, p);
			LOG_DEBUG("Created MOBIKE_SUPPORTED notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			break;

		case (N_ADDITIONAL_IP4_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):
			addr4_data = (struct in_addr *)netaddr_get_ipaddr(
					(netaddr_t *)(session->local_ipv4_addresses_ptr->data));
			addr = addr4_data->s_addr;

			len = payload_notify_mobike_create(next, N_ADDITIONAL_IP4_ADDRESS,
						(char *)&addr, (guint16)sizeof(addr), p);
			LOG_DEBUG("Created ADDITIONAL_IP4_ADDRESS MOBIKE notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			session->local_ipv4_addresses_ptr =
					session->local_ipv4_addresses_ptr->next;
			break;

		case (N_ADDITIONAL_IP6_ADDRESS << 8 | IKEV2_PAYLOAD_NOTIFY):
			addr6_data = (struct in6_addr *)netaddr_get_ipaddr(
					(netaddr_t *)(session->local_ipv6_addresses_ptr->data));

			len = payload_notify_mobike_create(next, N_ADDITIONAL_IP6_ADDRESS,
						(char *)addr6_data->s6_addr,
						(guint16)sizeof(addr6_data->s6_addr), p);
			LOG_DEBUG("Created ADDITIONAL_IP6_ADDRESS MOBIKE notification");
			LOG_TRACE("Created NOTIFY payload len = %u", len);
			session->local_ipv6_addresses_ptr =
					session->local_ipv6_addresses_ptr->next;
			break;

#endif /* MOBIKE */

		}

		if (len < 0) {
			LOG_ERROR("Error creating payload!");
			goto out;
		}

		p += len;

		curr = next;
		next = message_ike_auth_r_payload_next(session, curr);
	}

	enclen =  payload_encr_create(session->encr_type,
			session->sk_er, (guint)(p - encrp), encrp);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(msg->ns, session, buffer, p - buffer + ilen,
				session_get_i_addr(session));

	retval = 0;

out:
	if (retval < 0) {
		if (buffer)
			g_free(buffer);

		if (msg->notify)
			message_send_notify(session, msg);
	}

	LOG_FUNC_END(1);

	return retval;
}

#ifdef SUPPLICANT
/**
 * Send IKEv2 request message with embedded EAP response from initator
 *
 * @param session
 *
 * \return
 */
int message_send_ike_eap_i(struct session *session)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint32 len, enclen, ilen;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	hdr->i_spi = session_get_i_spi(session);
	hdr->r_spi = session_get_r_spi(session);
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(session_get_next_msgid(session));

	p += sizeof(struct ikev2_header);

	len = payload_encr_prepare(IKEV2_PAYLOAD_EAP, session->encr_type,
				p);

	encrp = p;
	p += len;

	len = payload_eap_create(IKEV2_PAYLOAD_NONE,
			supplicant_get_eap_data(session->sd),
			supplicant_get_eap_data_len(session->sd), p);

	p += len;

	enclen = payload_encr_create(session->encr_type,
			session->sk_ei, (int)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ai,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
			session_get_r_addr(session));

	LOG_FUNC_END(1);

	return 0;
}

#endif /* SUPPLICANT */

#ifdef AAA_CLIENT
/**
 * Send IKEv2 response message with embedded EAP request
 *
 * @param session
 * @param msg
 *
 * \return
 */
int message_send_ike_eap_r(struct session *session, struct message_msg *msg)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint32 len, enclen, ilen;
	void *eap_packet;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	hdr->i_spi = session_get_i_spi(session);
	hdr->r_spi = session_get_r_spi(session);
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	hdr->msg_id = htonl(msg->msg_id);

	p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare(IKEV2_PAYLOAD_EAP, session->encr_type,
			p);
	encrp = p;
	p += len;

	len = payload_eap_create(IKEV2_PAYLOAD_NONE, session->eap_packet,
					session->eap_packet_len, p);
	p += len;

	enclen = payload_encr_create(session->encr_type,
				session->sk_er, (int)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen -4);

	integ(session->integ_type, session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(msg->ns, session, buffer, p - buffer + ilen,
			session_get_i_addr(session));

	LOG_FUNC_END(1);

	return 0;
}
#endif /* AAA_CLIENT */

/**
 * Send final message with AUTH payload after successful EAP authentication
 *
 * @param session
 *
 * \return
 */
int message_send_ike_auth_final_i(struct session *session)
{
	char *buffer, *p, *encrp, *chksum;
	char *auth_payload;
	struct ikev2_header *hdr;
	int len, auth_payload_len, payloadid_len;
	guint32 enclen, ilen;
	struct sad_item *si;
	gpointer payloadid;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	/*
	 * Find corresponding CHILD SA structure
	 */
	si = session_get_sad_item(session);

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(session_get_next_msgid(session));

	p += sizeof(struct ikev2_header);

	len = payload_encr_prepare(IKEV2_PAYLOAD_AUTH, session->encr_type,
			p);
	encrp = p;
	p += len;

	payloadid = g_malloc(TB_SIZE);
	payloadid_len = payload_id_create(IKEV2_PAYLOAD_NONE,
					session->cp->lid, payloadid);
	auth_payload_len = auth_data_calculate(session->prf_type,
					session->auth_data,
					payloadid, payloadid_len,
					session->ikesa_init_i,
					session->ikesa_init_i_len,
					session->r_nonce,
					session->sk_pi,
					&auth_payload);
	g_free(payloadid);

	if (auth_payload_len < 0) {
		g_free(hdr);
		return -1;
	}

	LOG_TRACE("auth_payload=%p, auth_payload_len=%d",
			auth_payload, auth_payload_len);

	LOG_DEBUG("Authentication data to send");
	log_buffer(LOGGERNAME, auth_payload, auth_payload_len);

	len = payload_auth_create(IKEV2_PAYLOAD_NONE,
				session->auth_data->auth_type,
				auth_payload, auth_payload_len, p);

	p += len;
	g_free(auth_payload);

	LOG_TRACE("encrp=%p", encrp);

	enclen =  payload_encr_create(session->encr_type,
				session->sk_ei, (int)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ai,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
				session_get_r_addr(session));

	LOG_FUNC_END(1);

	return 0;
}

#ifdef AAA_CLIENT
/**
 * Based on the current payload, determine the next one for
 * final IKE AUTH response after successful EAP authentication
 *
 * @param session	Session structure
 * @param curr		Current payload (and exact notify if notify payload)
 *
 * \return Next payload
 */
guint32 message_ike_auth_final_r_next(struct session *session, guint32 curr)
{
	struct sad_item *si;
	guint32 next;

	switch (curr) {
	case IKEV2_PAYLOAD_ENCRYPTED:
		next = IKEV2_PAYLOAD_AUTH;
		break;

	case IKEV2_PAYLOAD_AUTH:
#ifdef CFG_MODULE
		if (session->rcfg) {
			if (session->rcfg == GUINT_TO_POINTER(1) ||
					cfg_proxy_is_error(session->rcfg))
				next = N_INTERNAL_ADDRESS_FAILURE << 8
						| IKEV2_PAYLOAD_NOTIFY;
			else
				next = IKEV2_PAYLOAD_CONFIG;
		}

		break;

	case IKEV2_PAYLOAD_CONFIG:
#endif /* CFG_MODULE */

		/*
		 * Here we should check if the next payload chould be
		 * IPCOMP_SUPPORTED payload. Since we do not support
		 * this mode yet, the code has to be added later.
		 */

	case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		si = session_get_sad_item(session);

		if (!si->spd) {
			next = N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

		if (!si->proposals) {
			next = N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

		if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TRANSPORT) {
			next = N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY;
			break;
		}

	case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here we should introduce code to check if
		 * ESP_TFC_PADDING_NOT_SUPPORTED notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):

		/*
		 * Here we should introduce code to check if
		 * NON_FIRST_FRAGMENTS_ALSO notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):

		next = IKEV2_PAYLOAD_SA;
		break;

	case IKEV2_PAYLOAD_SA:

		next = IKEV2_PAYLOAD_TSI;
		break;

	case IKEV2_PAYLOAD_TSI:

		next = IKEV2_PAYLOAD_TSR;
		break;

	case IKEV2_PAYLOAD_TSR:

		/*
		 * Here we should introduce code to check if
		 * ADDITIONAL_TS_POSSIBLE notify should be sent.
		 * For now, we do not support this feature.
		 */

	case (N_INTERNAL_ADDRESS_FAILURE << 8 | IKEV2_PAYLOAD_NOTIFY):
	case (N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY):
	case (N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY):
	default:
		next = IKEV2_PAYLOAD_NONE;
	}

	return next;
}

/**
 * Send final response message after successful EAP authentication
 *
 * @param session
 * @param msg
 *
 * \return
 */
int message_send_ike_auth_final_r(struct session *session,
		struct message_msg *msg)
{
	guint32 curr, next;
	char *buffer, *p, *encrp, *auth_payload, *chksum;
	struct ikev2_header *hdr;
	int len, auth_payload_len, payloadid_len;
	guint32 enclen, ilen;
	struct sad_item *si;
	gint retval;
	gchar *payloadid;

	LOG_FUNC_START(1);

	retval = -1;
	buffer = NULL;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	/*
	 * Find corresponding CHILD SA structure
	 */
	si = session_get_sad_item(session);

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	hdr->msg_id = htonl(msg->msg_id);;

	p += sizeof(struct ikev2_header);

	curr = IKEV2_PAYLOAD_ENCRYPTED;
	next = message_ike_auth_final_r_next(session, curr);

	while (curr != IKEV2_PAYLOAD_NONE) {

		switch (curr) {
		case IKEV2_PAYLOAD_SA:

			len = payload_sa_create(next, si->proposals,
					0, 0, p);
			break;


		case (N_NO_PROPOSAL_CHOSEN << 8 | IKEV2_PAYLOAD_NOTIFY):

			len = payload_notify_create(next, N_NO_PROPOSAL_CHOSEN,
					NULL, 0, 0, 0, p);
			break;

		case IKEV2_PAYLOAD_AUTH:

			payloadid = g_malloc0(TB_SIZE);
			payloadid_len = payload_id_create(IKEV2_PAYLOAD_NONE,
					session->cp->lid, payloadid);

			auth_payload_len = auth_data_calculate(
					session->prf_type,
					session->auth_data,
					payloadid, payloadid_len,
					session->ikesa_init_r,
					session->ikesa_init_r_len,
					session->r_nonce,
					session->sk_pr,
					&auth_payload);
			g_free(payloadid);

			if (auth_payload_len < 0) {
				LOG_ERROR("Failed to generate "
						"authentication data");
				msg->notify = N_AUTHENTICATION_FAILED;
				goto out;
			}

			LOG_TRACE("auth_payload=%p, auth_payload_len=%d",
			auth_payload, auth_payload_len);

			LOG_DEBUG("Authentication data to send");
			log_buffer(LOGGERNAME, auth_payload, auth_payload_len);

			len = payload_auth_create(next,
					session->auth_data->auth_type,
					auth_payload, auth_payload_len, p);

			g_free(auth_payload);

			break;

		case (N_IPCOMP_SUPPORTED << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_USE_TRANSPORT_MODE << 8 | IKEV2_PAYLOAD_NOTIFY):
			len = payload_notify_create(next, N_USE_TRANSPORT_MODE,
						NULL, 0, si->spi,
						sad_item_get_protocol(si), p);
			break;

		case (N_ESP_TFC_PADDING_NOT_SUPPORTED << 8 |
				IKEV2_PAYLOAD_NOTIFY):
			break;

		case (N_NON_FIRST_FRAGMENTS_ALSO << 8 | IKEV2_PAYLOAD_NOTIFY):
			break;

		case IKEV2_PAYLOAD_TSI:

			LOG_DEBUG("Remote (initiator's) traffic selectors");
			ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));
			len = payload_ts_create(next, sad_item_get_tsr(si), p);
			break;

		case IKEV2_PAYLOAD_TSR:

			LOG_DEBUG("Local (responder's) traffic selectors");
			ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
			len = payload_ts_create(next, sad_item_get_tsl(si), p);
			break;

		case IKEV2_PAYLOAD_ENCRYPTED:

			/**
			 * Prepare containter for encrypted payloads
			 */
			len = payload_encr_prepare(next, session->encr_type, p);
			encrp = p;
			break;

		case IKEV2_PAYLOAD_CONFIG:
#ifdef CFG_MODULE
			len = payload_cfgresp_create(next,
					cfg_proxy_get_cfg(session->rcfg),
					session->cp->subnets, p);
#else /* CFG_MODULE */
			LOG_WARNING("Payload type not compiled in! Ignoring!");
#endif /* CFG_MODULE */
			break;

		case (N_TS_UNACCEPTABLE << 8 | IKEV2_PAYLOAD_NOTIFY):

  			len = payload_notify_create(next, N_TS_UNACCEPTABLE,
						NULL, 0, 0, 0, p);
			break;
		}

		if (len < 0) {
			LOG_ERROR("Error creating payload!");
			goto out;
		}

		p += len;

		curr = next;
		next = message_ike_auth_final_r_next(session, curr);
	}

	LOG_TRACE("encrp=%p", encrp);

	enclen = payload_encr_create(session->encr_type,
			session->sk_er, (int)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(msg->ns, session, buffer, p - buffer + ilen,
			session_get_i_addr(session));

	retval = 0;

out:
	if (retval < 0 && buffer) {
		g_free(buffer);

		if (msg->notify)
			message_send_notify(session, msg);
	}

	LOG_FUNC_END(1);

	return retval;
}
#endif /* AAA_CLIENT */

/**
 * Create an IKEV2 CREATE_CHILD_SA initiator message for rekeying or creating
 * CHILD SA
 *
 * @param session	IKE SA data used to create CHILD SA
 * @param si		CHILD SA that is rekeyed, or created, with si->nsi
 *			representing the new CHILD SA that will replace the
 *			old one. In case we are creating new CHILD SA then
 *			si->nsi is NULL
 *
 * \return 0 if message has been successfuly sent, -1 otherwise
 */
int message_send_child_sa_i(struct session *session, struct sad_item *si)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint16 len, enclen, ilen;
	struct sad_item *nsi;
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	nsi = si->nsi ? si->nsi : si;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_CREATE_CHILD_SA;

	hdr->flags = session_get_ike_type(session) == IKEV2_INITIATOR
			? IKEV2_FLAG_INITIATOR : 0;
	hdr->msg_id = htonl(sad_item_get_msg_id(si));

	p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare((sad_item_get_peer_spi(si) ||
					(sad_item_get_ipsec_mode(nsi) ==
						IPSEC_MODE_TRANSPORT))
				? IKEV2_PAYLOAD_NOTIFY
				: IKEV2_PAYLOAD_SA,
				session->encr_type, p);
	encrp = p;
	p += len;

	if (sad_item_get_peer_spi(si)) {

		LOG_TRACE("SPI value to rekey %08x",
				sad_item_get_peer_spi(si));

		len = payload_notify_create((sad_item_get_ipsec_mode(nsi) ==
						IPSEC_MODE_TRANSPORT)
					? IKEV2_PAYLOAD_NOTIFY
					: IKEV2_PAYLOAD_SA,
						N_REKEY_SA, NULL, 0,
						sad_item_get_peer_spi(si),
						sad_item_get_protocol(si), p);
		p += len;
	}

	/*
	 * TODO: NOTIFY IPCOMP_SUPPORTED+ PAYLOAD
	 */

	if (sad_item_get_ipsec_mode(nsi) == IPSEC_MODE_TRANSPORT) {
		len = payload_notify_create(IKEV2_PAYLOAD_SA,
					N_USE_TRANSPORT_MODE,
					NULL, 0, 0, 0, p);
		p += len;
	}

	/*
	 * TODO: NOTIFY ESP_TFC_PADDING_NOT_SUPPORTED PAYLOAD
	 */

	/*
	 * TODO: NOTIFY NON_FIRST_FRAGMENTS_ALSO PAYLOAD
	 */

	/*
	 * Take proposals from existing CHILD SA and replace all SPI
	 * values with a new SPI.
	 */
	len = payload_sa_create(IKEV2_PAYLOAD_NONCE,
				sad_item_get_proposals(nsi), 0, 0, p);
	p += len;

	len = payload_nonce_create(sad_item_get_dh(nsi)
					? IKEV2_PAYLOAD_KE
					: IKEV2_PAYLOAD_TSI,
					sad_item_get_i_nonce(nsi), p);
	p += len;

	if (sad_item_get_dh(nsi)) {
		len = payload_ke_create(IKEV2_PAYLOAD_TSI,
					sad_item_get_dh_group(nsi), 
					sad_item_get_dh(nsi), p);
		p += len;
	}

	len = payload_ts_create(IKEV2_PAYLOAD_TSR,
			sad_item_get_tsl(nsi), p);
	p += len;

	len = payload_ts_create(IKEV2_PAYLOAD_NONE,
			sad_item_get_tsr(nsi), p);
	p += len;

	enclen =  payload_encr_create(session->encr_type,
			session->ike_type == IKEV2_INITIATOR
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);

	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
				sad_item_get_rekey_dstaddr(nsi));

	retval = 0;

	LOG_FUNC_END(1);

	return retval;
}

int message_send_ike_sa_rekey_i(struct session *session)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	struct netaddr *dstaddr;
	guint16 len, enclen, ilen;
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_CREATE_CHILD_SA;

	hdr->flags = session_get_ike_type(session) == IKEV2_INITIATOR
			? IKEV2_FLAG_INITIATOR : 0;
	hdr->msg_id = htonl(session->msgid);

	p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare(IKEV2_PAYLOAD_SA, session->encr_type, p);
	encrp = p;

	p += len;

	len = payload_sa_create(IKEV2_PAYLOAD_NONCE,
				session_get_proposals(session),
				8, session->session->i_spi, p);
	p += len;

	len = payload_nonce_create(IKEV2_PAYLOAD_KE,
				session->session->i_nonce, p);
	p += len;

	/*
	 * TODO: KE payload is actually optional!
	 */
	len = payload_ke_create(IKEV2_PAYLOAD_NONE, session->session->dh_group, 
				session->session->dh, p);
	p += len;

	enclen =  payload_encr_create(session->encr_type,
			session->ike_type == IKEV2_INITIATOR
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);

	hdr->length = htonl(p - buffer + ilen);

	integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		dstaddr = session_get_r_addr(session);
	else
		dstaddr = session_get_i_addr(session);

	message_send_packet(NULL, session, buffer, p - buffer + ilen, dstaddr);

	retval = 0;

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Function that creates IKEv2 message for rekeying/creating CHILD SA
 *
 * @param session
 * @param si		CHILD SA that is rekeyed, with si->nsi representing
 *			the new CHILD SA that will replace the old one. If
 *			si->nsi is NULL then we are creating new CHILD SA.
 * @param message_msg
 *
 * \return
 */
int message_send_child_sa_r(struct session *session,
		struct sad_item *si,
		struct message_msg *message_msg)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	struct sad_item *nsi;
	guint16 len, enclen, ilen;
	int retval;

	LOG_FUNC_START(1);

	nsi = si->nsi ? si->nsi : si;

	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_CREATE_CHILD_SA;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		hdr->flags |= IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(message_msg->msg_id);

	p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare(sad_item_get_ipsec_mode(nsi)
							== IPSEC_MODE_TRANSPORT
						|| message_msg->notify
					? IKEV2_PAYLOAD_NOTIFY
					: IKEV2_PAYLOAD_SA, session->encr_type,
					p);
	encrp = p;
	p += len;

	if (message_msg->notify) {

		len = payload_notify_create(IKEV2_PAYLOAD_NONE,
					message_msg->notify, NULL, 0, 0, 0, p);
		p += len;

	} else {
		/*
		 * TODO: NOTIFY IPCOMP_SUPPORTED PAYLOAD
		 */

		if (sad_item_get_ipsec_mode(nsi) == IPSEC_MODE_TRANSPORT) {
			len = payload_notify_create(IKEV2_PAYLOAD_SA,
						N_USE_TRANSPORT_MODE,
						NULL, 0, 0, 0, p);
			p += len;
		}

		/*
		 * TODO: NOTIFY ESP_TFC_PADDING_NOT_SUPPORTED PAYLOAD
		 */

		/*
		 * TODO: NOTIFY NON_FIRST_FRAGMENTS_ALSO PAYLOAD
		 */

		len = payload_sa_create(IKEV2_PAYLOAD_NONCE,
				sad_item_get_proposals(nsi),
				0, 0, p);
		p += len;

		len = payload_nonce_create(sad_item_get_dh(nsi)
						? IKEV2_PAYLOAD_KE
						: IKEV2_PAYLOAD_TSI,
						sad_item_get_r_nonce(nsi), p);
		p += len;

		if (sad_item_get_dh(nsi)) {
			len = payload_ke_create(IKEV2_PAYLOAD_TSI,
						sad_item_get_dh_group(nsi), 
						sad_item_get_dh(nsi), p);
			p += len;
		}

		len = payload_ts_create(IKEV2_PAYLOAD_TSR,
				sad_item_get_tsl(nsi), p);
		p += len;

		len = payload_ts_create(IKEV2_PAYLOAD_NONE,
				sad_item_get_tsr(nsi), p);
		p += len;

		/*
		 * TODO: NOTIFY ADDITIONAL_TS_POSSIBLE PAYLOAD
		 */
	}

	enclen = payload_encr_create(session->encr_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(message_msg->ns, session, buffer, p - buffer + ilen,
				sad_item_get_rekey_dstaddr(nsi));

	retval = 0;

	LOG_FUNC_END(1);

	return retval;
}

int message_send_ike_sa_rekey_r(struct session *session,
		struct session *nsession,
		struct message_msg *message_msg)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint16 len, enclen, ilen;
	int retval;
	GSList lproposal;

	LOG_FUNC_START(1);

	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_CREATE_CHILD_SA;
	hdr->flags = IKEV2_FLAG_RESPONSE;
	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		hdr->flags |= IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(message_msg->msg_id);

	p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare(IKEV2_PAYLOAD_SA, session->encr_type, p);
	encrp = p;
	p += len;

	lproposal.data = session_get_selected_proposal(session);
	lproposal.next = NULL;
	len = payload_sa_create(IKEV2_PAYLOAD_NONCE, &lproposal, 8,
				nsession->r_spi, p);
	p += len;

	len = payload_nonce_create(IKEV2_PAYLOAD_KE,
					nsession->r_nonce, p);
	p += len;

	/*
	 * TODO: Actually KE payload is optional!
	 */
	len = payload_ke_create(IKEV2_PAYLOAD_NONE, nsession->dh_group,
		nsession->dh, p);
	p += len;

	enclen = payload_encr_create(session->encr_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen);

	integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(message_msg->ns, session, buffer, p - buffer + ilen,
				session_get_i_addr(session));

	retval = 0;

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Create and send DELETE message request or response
 *
 * @param session	If message field is non-NULL and
 *			request then response is sent,
 *			otherwise request is sent...
 * @param msg
 * @param deletes
 *
 * \return
 */
int message_send_delete(struct session *session, struct message_msg *msg,
		GSList *deletes)
{
	char *buffer, *p, *encrp, *chksum;
	struct ikev2_header *hdr;
	guint16 len, enclen, ilen;
	struct payload_delete_data *pdd;

	LOG_FUNC_START(1);

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_INFORMATIONAL;
	hdr->flags = session_get_ike_type(session) == IKEV2_INITIATOR
			? IKEV2_FLAG_INITIATOR : 0;

	if (msg) {
		hdr->msg_id = htonl(msg->msg_id);
		hdr->flags |= IKEV2_FLAG_RESPONSE;
	} else
		hdr->msg_id = htonl(session_get_next_msgid(session));

		p += sizeof(struct ikev2_header);

	len =  payload_encr_prepare(IKEV2_PAYLOAD_DELETE,
					session->encr_type, p);

	encrp = p;
	p += len;

	while (deletes) {
		pdd = deletes->data;
		len = payload_delete_create(IKEV2_PAYLOAD_NONE, pdd, p);
		p += len;
		deletes = deletes->next;
	}

	enclen =  payload_encr_create(session->encr_type,
			session->ike_type == IKEV2_INITIATOR
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);

	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(msg ? msg->ns : NULL, session,
		buffer, p - buffer + ilen,
		session_get_ike_type(session) == IKEV2_INITIATOR
				? session_get_r_addr(session)
				: session_get_i_addr(session));

	LOG_FUNC_END(1);

	return 0;
}

/**
 * The following function prepares and sends response notify message with
 * notify as set in received message structure.
 *
 * @param session	Session to which notify belongs.
 * @param msg		Message that triggered notify.
 * 
 * \return 0 if everything was OK, -1 in case of an error
 *
 * NOTE: In case notify field in msg structure is set to 0 (or msg is
 *	 NULL, then empty message is sent...)
 */
int message_send_notify(struct session *session, struct message_msg *msg)
{
#warning "It seeems there is a bug in this function. It should be thoroughly tested!"
	char *buffer, *p, *encrp = NULL, *chksum;
	struct ikev2_header *hdr;
	int len, enclen, ilen, msg_length, retval = 0;
	guint16 data16;
	guint32 data32;

	LOG_FUNC_START(1);

	LOG_TRACE("Parameters: session=%p, msg=%p", session, msg);

	if (msg && msg->notify == -1)
		goto out;

	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;

	LOG_TRACE("Populating header SPI fields");
	if (msg && !msg->response) {
		hdr->i_spi = msg->i_spi;
		hdr->r_spi = msg->r_spi;
	} else {
		hdr->i_spi = session->i_spi;
		hdr->r_spi = session->r_spi;
	}

	if (session && session->sk_d)
		hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	else
		hdr->next = IKEV2_PAYLOAD_NOTIFY;

	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;

	if (msg && !msg->response)
		hdr->exchg_type = msg->exchg_type;
	else
		hdr->exchg_type = IKEV2_EXT_INFORMATIONAL;

	if (msg && !msg->response) {
		hdr->msg_id = htonl(msg->msg_id);
		hdr->flags = IKEV2_FLAG_RESPONSE;
	} else
		hdr->msg_id = htonl(session_get_next_msgid(session));

	if (session && session_get_ike_type(session) == IKEV2_INITIATOR)
		hdr->flags |= IKEV2_FLAG_INITIATOR;

	p += sizeof(struct ikev2_header);

	if (session && session->sk_d) {
		LOG_TRACE("Preparing encryption payload");
		len =  payload_encr_prepare((msg && msg->notify)
						? IKEV2_PAYLOAD_NOTIFY
						: IKEV2_PAYLOAD_NONE,
					session->encr_type, p);
		encrp = p;
		p += len;
	}

	if (msg)
		switch (msg->notify) {
		case N_INVALID_KE_PAYLOAD:
			LOG_TRACE("Creating INVALID_KE_PAYLOAD notify");
			data16 = htons(msg->notify_data16);
			len = payload_notify_create(IKEV2_PAYLOAD_NONE,
				msg->notify, (char *)&data16, 2, 0, 0, p);
			p += len;
			break;

		case N_INVALID_MESSAGE_ID:
			LOG_TRACE("Creating INVALID_MESSAGE_ID notify");
			data32 = htons(msg->notify_data32);
			len = payload_notify_create(IKEV2_PAYLOAD_NONE,
				msg->notify, (char *)&data32, 4, 0, 0, p);
			p += len;
			break;

		case 0:
			break;

		default:
			LOG_TRACE("Creating notify payload");
			len = payload_notify_create(IKEV2_PAYLOAD_NONE,
				msg->notify, NULL, 0, 0, 0, p);
			p += len;
			break;
		}

	if (session && session->sk_d) {
		LOG_TRACE("Encrypting payload");
		enclen = payload_encr_create(session->encr_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ei
				: session->sk_er,
			(int)(p - encrp), encrp);
		p = encrp + enclen;

		ilen = integ_chksumlen_get(session->integ_type);

		((struct payload_generic_header *)encrp)->length =
			htons(ntohs(((struct payload_generic_header *)encrp)->length)
				+ ilen);
		msg_length = p - buffer + ilen;
		hdr->length = htonl(msg_length - 4);

		integ(session->integ_type,
			(session_get_ike_type(session) == IKEV2_INITIATOR)
				? session->sk_ai
				: session->sk_ar,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);

		memcpy(p, chksum, ilen);
		g_free(chksum);
	} else {
		msg_length = p - buffer;
		hdr->length = htonl(msg_length - 4);
	}

	message_send_packet(msg->ns, session, buffer, msg_length, msg->srcaddr);

out:
	LOG_FUNC_END(1);

	return retval;
}

#ifdef CFG_CLIENT
/**
 * Send CFG request in informational message
 *
 * @param session
 *
 * \return	0 if request has been successfuly sent,
 *		-1 in case of an error
 */
int message_send_informational_renew_i(struct session *session)
{
	gint retval;
	char *p, *buffer, *encrp = NULL, *chksum;
	gint len = 0;
	struct ikev2_header *hdr;
	guint32 enclen, ilen;

	LOG_FUNC_START(1);

	retval = -1;
	buffer = NULL;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;
	p += sizeof(struct ikev2_header);

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session->i_spi;
	hdr->r_spi = session->r_spi;
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_IKE_AUTH;
	hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->msg_id = htonl(session_get_next_msgid(session));

	len = payload_encr_prepare(IKEV2_PAYLOAD_CONFIG, session->encr_type, p);
	encrp = p;
	p += len;

	len = payload_cfgreq_create(IKEV2_PAYLOAD_NONE, session->icfg, p);
	LOG_TRACE("Created CONFIG payload len = %u", len);

	enclen =  payload_encr_create(session->encr_type,
			session->sk_ei, (guint)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, session->sk_ai,
			session->integ_type->key_len, (char *)hdr,
			p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
				session_get_r_addr(session));

	retval = 0;

	if (retval < 0 && buffer)
		g_free(buffer);

	LOG_FUNC_END(1);

	return retval;
}
#endif /* CFG_CLIENT */

/**
 * Send informational response message
 *
 * @param session
 * @param msg
 *
 * \return	0 if request has been successfuly sent,
 *		-1 in case of an error
 */
int message_send_informational_r(struct session *session,
		struct message_msg *msg)
{
	gint retval;
	char *p, *buffer, *encrp = NULL, *chksum;
	char *sk_a, *sk_e;
	gint len = 0;
	struct ikev2_header *hdr;
	struct payload_delete_data *pdd;
	guint32 enclen, ilen;
	guint8 next;
	struct netaddr *peer_addr;

	LOG_FUNC_START(1);

	retval = -1;

	/**
	 * Allocate buffer to store constructed message
	 */
	p = buffer = g_malloc0(TB_SIZE);

	p += 4;

	hdr = (struct ikev2_header *)p;
	p += sizeof(struct ikev2_header);

	/*
	 * Populate header with data...
	 */
	hdr->i_spi = session_get_i_spi(session);
	hdr->r_spi = session_get_r_spi(session);
	hdr->next = IKEV2_PAYLOAD_ENCRYPTED;
	hdr->mj_ver = IKEV2_MAJOR_VERSION;
	hdr->mn_ver = IKEV2_MINOR_VERSION;
	hdr->exchg_type = IKEV2_EXT_INFORMATIONAL;
	if (session->ike_type == IKEV2_INITIATOR)
		hdr->flags = IKEV2_FLAG_INITIATOR;
	hdr->flags |= IKEV2_FLAG_RESPONSE;
	hdr->msg_id = htonl(msg->msg_id);

	if (msg->informational.deletes)
		next = IKEV2_PAYLOAD_DELETE;
#ifdef CFG_MODULE
	else if (msg->informational.cfg)
		next = IKEV2_PAYLOAD_CONFIG;
#endif /* CFG_MODULE */
	else
		next = IKEV2_PAYLOAD_NONE;

	len = payload_encr_prepare(next, session->encr_type, p);
	encrp = p;
	p += len;

	do {
		switch (next) {
		case IKEV2_PAYLOAD_DELETE:

			while (msg->informational.deletes) {
				pdd = (msg->informational.deletes)->data;
				len = payload_delete_create(IKEV2_PAYLOAD_NONE,
						pdd, p);
				p += len;
				msg->informational.deletes = g_slist_remove(
						msg->informational.deletes,
						pdd);
			}

#ifdef CFG_MODULE
			if (msg->informational.cfg)
				next = IKEV2_PAYLOAD_CONFIG;
			else
#endif /* CFG_MODULE */
				next = IKEV2_PAYLOAD_NONE;
			break;

#ifdef CFG_MODULE
		case IKEV2_PAYLOAD_CONFIG:
			len = payload_cfgresp_create(next, session->rcfg,
						session->cp->subnets, p);
			p += len;

			next = IKEV2_PAYLOAD_NONE;
			break;
#endif /* CFG_MODULE */
		}

	} while (next != IKEV2_PAYLOAD_NONE);

	if (session->ike_type == IKEV2_INITIATOR) {
		sk_e = session->sk_ei;
		sk_a = session->sk_ai;
		peer_addr = netaddr_dup(session_get_r_addr(session));
	} else {
		sk_e = session->sk_er;
		sk_a = session->sk_ar;
		peer_addr = netaddr_dup(session_get_i_addr(session));
	}

	enclen =  payload_encr_create(session->encr_type, sk_e,
				(guint)(p - encrp), encrp);

	LOG_TRACE("encrp=%p, enclen=%d", encrp, enclen);

	p = encrp + enclen;

	ilen = integ_chksumlen_get(session->integ_type);

	/*
	 * TODO: Replace with htons(enclen + ilen)!
	 */
	((struct payload_generic_header *)encrp)->length =
		htons(ntohs(((struct payload_generic_header *)encrp)->length)
			+ ilen);
	hdr->length = htonl(p - buffer + ilen - 4);

	integ(session->integ_type, sk_a, session->integ_type->key_len,
			(char *)hdr, p - buffer - 4, &chksum);
	memcpy(p, chksum, ilen);
	g_free(chksum);

	message_send_packet(NULL, session, buffer, p - buffer + ilen,
			peer_addr);

	netaddr_free(peer_addr);

	retval = 0;

	if (retval < 0 && buffer)
		g_free(buffer);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Quick search through message for N(COOKIE) payload. Since this method will
 * be typically called when an assumption of DOS attack is made, this function
 * will make its job simple and quick. No memory allocations will be made. 
 * However some care will be taken to detect packet length malversations.
 * 
 * @param msg		message from networking subsystem
 *
 * \return		0	if no cookie has been found
 *			1	if cookie has been found
 *			-1	if there is an error in packet
 */
int message_parse_quick(struct message_msg *msg)
{
	char *p;
	struct ikev2_header *hdr;
	struct payload_notify *nhdr;
	guint16 total_length;
	int retval;

	LOG_FUNC_START(1);

	p = (char *)(msg->buffer);

	/*
	 * If the first four octets of the message are zero then
	 * it must be UDP encapsulated IKEv2 message so skip
	 * the marker.
	 */
	if (*(guint32 *)p == 0)
		p += 4;

	hdr = (struct ikev2_header *)p;

	/*
	 * Assume error (most cases...)
	 */
	msg->ike_sa_init.cookie = NULL;
	retval = -1;

	/*
	 * Check sizes...
	 */
	if (msg->size < sizeof(struct ikev2_header)) {
		LOG_ERROR("Received message is to small (size=%u)",
				msg->size);
		goto out;
	}

	total_length = ntohl(hdr->length);
	if (msg->size < total_length) {
		LOG_ERROR("Received message has incorrect length value (%u)",
			ntohl(hdr->length));
		goto out;
	}

	/*
	 * Message ID has to be zero
	 */
	if (hdr->msg_id)
		goto out;

	/*
	 * If it's not IKE SA INIT exchange, ignore it...
	 */
	if (hdr->exchg_type != IKEV2_EXT_IKE_SA_INIT)
		goto out;

	/*
	 * This is some response, so ignore it...
	 */
	if (hdr->flags & IKEV2_FLAG_RESPONSE)
		goto out;

	/*
	 * Is first payload after header NOTIFY? It has to be according to spec.
	 */
	if (hdr->next != IKEV2_PAYLOAD_NOTIFY) {
		retval = 0;
		goto out;
	}

	p += sizeof(struct ikev2_header);
	nhdr = (struct payload_notify *)p;

	/*
	 * Check length, if NOTIFY payload is still within bounds
	 * of a packet...
	 */
	if ((char *)hdr + total_length < p + ntohs(nhdr->length))
		goto out;

	/*
	 * Is it N_COOKIE payload?
	 */
	if (ntohs(nhdr->n_type) != N_COOKIE) {
		retval = 0;
		goto out;
	}

	/*
	 * Quick sanity check of the other fields
	 */
	if (nhdr->protocol_id != IKEV2_PROTOCOL_IKE || nhdr->spi_size)
		goto out;

	if (ntohs(nhdr->length) - sizeof(struct payload_notify)
						!= MESSAGE_COOKIE_LEN)
		goto out;

	/*
	 * Return pointer to a cookie...
	 */
	msg->ike_sa_init.cookie = p + sizeof(struct payload_notify);
	retval = 1;

	LOG_DEBUG("Found a cookie in received message");

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Find next payload
 *
 * @param p		Pointer to a next payload, output parameter
 * @param ptype		Payload type value, output parameter
 * @param hdr		Pointer to IKEv2 header message
 *
 * \return	0	Everything went OK
 *			Otherwise, one of the flags MSGPARSE_*
 *			If notify should be sent then it's encoded into lower
 *			16 bits of return value!
 *
 * This function  skips all the unrecognized payloads. It also checks
 * that each skipped payload doesn't have critical bit set. In that
 * case it signals an error.
 */
guint32 message_find_next_payload(gchar **p, guint8 *ptype, 
		struct ikev2_header *hdr) 
{
	guint32 retval;

	LOG_FUNC_START(1);

	/*
	 * For first payload *p == hdr and ptype has been
	 * taken from ikev2_hdr. For other payloads we take ptype
	 * from payload_generic_header here..
	 */
	if (*p != (gchar *)hdr) {

		*ptype = ((struct payload_generic_header *)*p)->next;
		*p += ntohs(((struct payload_generic_header *)*p)->length);

	} else {

		*ptype = hdr->next;
		*p += sizeof(struct ikev2_header);

	}

	if (*ptype && (*p > (gchar *)hdr + ntohl(hdr->length))) {
		LOG_ERROR("Inconsistent message length in header "
				"of payload %u", *ptype);
		LOG_DEBUG("Total message length %u", ntohl(hdr->length));
		retval = MSGPARSE_TERMINATE_MSG |
				MSGPARSE_SEND_NOTIFY |
				N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * Repeat the following code until we find recognizable payload
	 */
	while (*ptype &&
			(*ptype == IKEV2_PAYLOAD_VENDORID ||
			*ptype < IKEV2_PAYLOAD_SA ||
			*ptype > IKEV2_PAYLOAD_EAP)) {

		if (((struct payload_generic_header *)p)->c) {
			/**
			 * Send notify and reject message
			 */
			LOG_ERROR("Unimplemented payload id %u with "
					"critical bit set, ptype = ", *ptype);

			retval = MSGPARSE_TERMINATE_MSG |
					N_UNSUPPORTED_CRITICAL_PAYLOAD;

			if (!(hdr->flags & IKEV2_FLAG_RESPONSE))
				retval |= MSGPARSE_SEND_NOTIFY;

			goto out;

		}

		/*
		 * Process next payload...
		 */
		*ptype = ((struct payload_generic_header *)*p)->next;
		*p += ntohs(((struct payload_generic_header *)*p)->length);

		if (*ptype && (*p > (gchar *)hdr + ntohl(hdr->length))) {
			LOG_ERROR("Inconsistent message length in header "
					"of payload %u", *ptype);
			LOG_DEBUG("Total message length %u",
					ntohl(hdr->length));
			retval = MSGPARSE_TERMINATE_MSG |
					N_INVALID_SYNTAX;

			if (!(hdr->flags & IKEV2_FLAG_RESPONSE))
				retval |= MSGPARSE_SEND_NOTIFY;

			goto out;
		}
	}

	/*
	 * Everything went ok..
	 */	
	retval = 0;

	LOG_TRACE("hdr=%p, next=%u, p=%p", hdr, *ptype, *p);

out:
	LOG_FUNC_END(1);
	return retval;
}

/**
 * Parse IKE SA INIT request message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, IKE SA INIT request message
 * has the following format:
 *
 *	[N(COOKIE)],
 *	SA, KE, Ni,
 *	[N(NAT_DETECTION_SOURCE_IP)+,
 *	N(NAT_DETECTION_DESTINATION_IP)],
 *	[V+]
 */
gint message_parse_ikesa_init_i(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint r1;
	guint32 r2;

	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		msg->notify = r2 & 0xFFFF;
		goto out;
	}

	np = (struct payload_notify *)p;

	if (ptype == IKEV2_PAYLOAD_NOTIFY && ntohs(np->n_type) == N_COOKIE) {

		msg->ike_sa_init.cookie = p;
		msg->ike_sa_init.cookie_len = ntohs(np->length) -
					sizeof(struct payload_notify);
	
		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	if (ptype == IKEV2_PAYLOAD_SA) {
		msg->ike_sa_init.proposals = payload_sa_parse(p);
		if (msg->ike_sa_init.proposals == NULL)
			goto out;

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	
	} else {

		LOG_ERROR("Expected SA payload, got %u", ptype);
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;

	}

	if (ptype == IKEV2_PAYLOAD_KE) {
		msg->ike_sa_init.peer_pub_key = payload_ke_parse(
					&msg->ike_sa_init.peer_dh_group, p);
		if (msg->ike_sa_init.peer_pub_key == NULL) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {
		LOG_ERROR("Expected KE payload, got %u", ptype);
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_NONCE) {
		msg->ike_sa_init.peer_nonce = payload_nonce_parse(p);
		if (msg->ike_sa_init.peer_nonce == NULL) {
			r1 |= MSGPARSE_TERMINATE_SESSION;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {
		LOG_ERROR("Expected Ni payload, got %u", ptype);
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	while (ptype == IKEV2_PAYLOAD_NOTIFY) {

		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		np = (struct payload_notify *)p;

#warning "Correct size has to be checked here! We expect SHA1 sum!"
		switch(ntohs(np->n_type)) {
		case N_NAT_DETECTION_SOURCE_IP:
			msg->ike_sa_init.nat_sourceip = g_slist_append(
					msg->ike_sa_init.nat_sourceip, p + 
					sizeof(struct payload_notify));
			break;

		case N_NAT_DETECTION_DESTINATION_IP:
			msg->ike_sa_init.nat_destip = p + 
				sizeof(struct payload_notify);
			break;

		default:
			break;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	while (ptype == IKEV2_PAYLOAD_VENDORID) {
		LOG_NOTICE("Found VENDORID payload. Ignoring.");
	
		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse IKE SA INIT response message
 *
 * @param hdr
 * @param p
 * @param msg	Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, IKE SA INIT response message
 * has the following format (no cookie):
 *
 *	SA, KE, Nr,
 *	[N(NAT_DETECTION_SOURCE_IP),
 *	N(NAT_DETECTION_DESTINATION_IP)],
 *	[[N(HTTP_CERT_LOOKUP_SUPPORTED)], CERTREQ+],
 *	[V+]
 *
 * Otherwise, one of the following notifies can be sent:
 *
 *	INVALID_MAJOR_VERSION, INVALID_SYNTAX, INVALID_KE_PAYLOAD,
 *	NO_PROPOSAL_CHOSEN, COOKIE
 *
 */
gint16 message_parse_ikesa_init_r(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint32 r1, r2;
	struct certreq *certreq;

	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (gchar *)(msg->buffer);
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
		r1 |= (r2 & 0xFFFF0000);
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_NOTIFY) {

		if (payload_notify_check(p) < 0)
			goto out;

		np = (struct payload_notify *)p;

		switch (ntohs(np->n_type)) {
		case N_INVALID_MAJOR_VERSION:
			msg->ike_sa_init.invalid_major_version = TRUE;
			break;
		case N_INVALID_SYNTAX:
			msg->ike_sa_init.invalid_syntax = TRUE;
			break;
		case N_INVALID_KE_PAYLOAD:
			msg->ike_sa_init.dh_group =
				-ntohs(*(guint16 *)
					(p + sizeof(struct payload_notify)));
			break;
		case N_NO_PROPOSAL_CHOSEN:
			msg->ike_sa_init.no_proposal_chosen = TRUE;
			break;
		case N_COOKIE:
			msg->ike_sa_init.cookie = p;
			msg->ike_sa_init.cookie_len = ntohs(np->length) -
						sizeof(struct payload_notify);
			break;
		default:
			LOG_ERROR("Received unexpected notify %u",
					ntohs(np->n_type));
			break;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else {

		if (ptype == IKEV2_PAYLOAD_SA) {

			msg->ike_sa_init.proposals = payload_sa_parse(p);
			if (msg->ike_sa_init.proposals == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		} else {

			LOG_ERROR("Expected SA payload, got %u", ptype);
			goto out;

		}

		if (ptype == IKEV2_PAYLOAD_KE) {

			msg->ike_sa_init.peer_pub_key = payload_ke_parse(
					&msg->ike_sa_init.peer_dh_group,
					p);

			if (msg->ike_sa_init.peer_pub_key == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		
		} else {

			LOG_ERROR("Expected KE payload, got %u", ptype);
			goto out;

		}

		if (ptype == IKEV2_PAYLOAD_NONCE) {
			msg->ike_sa_init.peer_nonce = payload_nonce_parse(p);
			if (msg->ike_sa_init.peer_nonce == NULL) {
				r1 |= MSGPARSE_TERMINATE_SESSION;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		
		} else {

			LOG_ERROR("Expected Ni payload, got %u", ptype);
			goto out;

		}

		while (ptype == IKEV2_PAYLOAD_NOTIFY) {

			if (payload_notify_check(p) < 0) {
				goto out;
			}

			np = (struct payload_notify *)p;

			switch(ntohs(np->n_type)) {
			case N_NAT_DETECTION_SOURCE_IP:
				msg->ike_sa_init.nat_sourceip = g_slist_append(
					msg->ike_sa_init.nat_sourceip,
					p + sizeof(struct payload_notify));
				break;
			case N_NAT_DETECTION_DESTINATION_IP:
				msg->ike_sa_init.nat_destip = p + 
						sizeof(struct payload_notify);
				break;
			case N_HTTP_CERT_LOOKUP_SUPPORTED:
				LOG_DEBUG("peer sent "
					"HTTP_CERT_LOOKUP_SUPPORTED");
				msg->ike_sa_init.http_cert_lookup = TRUE;
				break;
			default:
				break;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		}

		while (ptype == IKEV2_PAYLOAD_CERTREQ) {

			if ((certreq = payload_certreq_parse(p)) == NULL)
				goto out;

			/**
			 * FIXME:
			 * Free this memory later (in message_msg_free function
			 * that frees memory occupied by the message subsystem
			 * message)!
			 */
			msg->ike_sa_init.certreq_list = g_slist_append(
					msg->ike_sa_init.certreq_list, certreq);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		while (ptype == IKEV2_PAYLOAD_VENDORID) {
			LOG_NOTICE("Found VENDORID payload. Ignoring.");

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}
	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse IKE AUTH request message
 *
 * @param hdr
 * @param p
 * @param msg	Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, IKE AUTH request message
 * has the following format (version without EAP autentification):
 *
 *	IDi, [CERT+],
 *	[N(INITIAL_CONTACT)],
 *	[[N(HTTP_CERT_LOOKUP_SUPPORTED)], CERTREQ+],
 *	[IDr],
 *	AUTH,
 *	[CP(CFG_REQUEST)],
 *	[N(IPCOMP_SUPPORTED)+],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	SA, TSi, TSr,
 *	[V+]
 *
 *
 * Following messages (for EAP scenario are not implemented)!
 * 
 * In case of EAP autentification the first request has the following format
 * (Not implemented!)
 *	IDi,
 *	[N(INITIAL_CONTACT)],
 *	[[N(HTTP_CERT_LOOKUP_SUPPORTED)], CERTREQ+],
 *	[IDr],
 *	[CP(CFG_REQUEST)],
 *	[N(IPCOMP_SUPPORTED)+],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	SA, TSi, TSr,
 *	[V+]
 *
 * Following meesage has the format:
 *	EAP
 *
 * And the last request is simply:
 *	AUTH
 * 
 */
gint message_parse_ike_auth_i(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint32 r1, r2;
	struct cert *cert;
	struct certreq *certreq;
#ifdef MOBIKE
	gboolean no_additional_addresses = FALSE;
	struct netaddr *peer_addr;
	guint16 mobike_notify_len;
	unsigned char addr_dump[48];

	unsigned char *addr_buf;
	unsigned char hex_token[4];
	int i;
#endif /* MOBIKE */

	LOG_FUNC_START(1);

#ifdef MOBIKE
	/*
	 * Create peer's address list of only one element at the moment -
	 * source address found in header.
	 */
	if(netaddr_get_family(msg->srcaddr) == AF_INET)
		msg->ike_auth.peer_ipv4_addresses =
				g_slist_append(NULL, msg->srcaddr);
	else if(netaddr_get_family(msg->srcaddr) == AF_INET6)
		msg->ike_auth.peer_ipv6_addresses =
				g_slist_append(NULL, msg->srcaddr);
#endif /* MOBIKE */

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		msg->notify = r2 & 0xFFFF;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;

	}

	if (ptype == IKEV2_PAYLOAD_EAP) {

		msg->ike_auth.eap = payload_eap_parse(p,
						&msg->ike_auth.eap_len);

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else if (ptype == IKEV2_PAYLOAD_AUTH) {

		msg->ike_auth.auth_type = payload_auth_parse(
				(void *)&(msg->ike_auth.auth_payload),
				&(msg->ike_auth.auth_payload_len), p);

		if (!msg->ike_auth.auth_type) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {

		if (ptype == IKEV2_PAYLOAD_IDI) {
			msg->ike_auth.i_id = payload_id_parse(p);
			id_dump(LOGGERNAME, msg->ike_auth.i_id);

			if (msg->ike_auth.i_id == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		} else {

			LOG_ERROR("Expected IDi payload, got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}	


		while (ptype == IKEV2_PAYLOAD_CERT) {

			if ((cert = payload_cert_parse(p)) == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			/**
		 	 * FIXME:
		 	 * Free this memory later (in message_msg_free function
			 * that frees memory occupied by the message subsystem
			 * message)!
		 	 */
			msg->ike_auth.cert_list = g_slist_append(
						msg->ike_auth.cert_list, cert);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}

		np = (struct payload_notify *)p;
		if (ptype == IKEV2_PAYLOAD_NOTIFY && 
				ntohs(np->n_type) == N_INITIAL_CONTACT) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			msg->ike_auth.initial_contact = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY && 
				ntohs(np->n_type) ==
						N_HTTP_CERT_LOOKUP_SUPPORTED) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			LOG_DEBUG("peer sent HTTP_CERT_LOOKUP_SUPPORTED");
			msg->ike_auth.http_cert_lookup = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}

#warning "This is not properly parsed! Some illegal combinations are possible!"
		while (ptype == IKEV2_PAYLOAD_CERTREQ) {

			if ((certreq = payload_certreq_parse(p)) == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			msg->ike_auth.certreq_list = g_slist_append(
						msg->ike_auth.certreq_list,
						certreq);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}

		if (ptype == IKEV2_PAYLOAD_IDR) {
			msg->ike_auth.r_id = payload_id_parse(p);

			if (msg->ike_auth.r_id == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}

		if (ptype == IKEV2_PAYLOAD_AUTH) {

			msg->ike_auth.auth_type = payload_auth_parse(
					(void *)&(msg->ike_auth.auth_payload),
					&(msg->ike_auth.auth_payload_len), p);

			if (!msg->ike_auth.auth_type) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		}

#ifdef CFG_MODULE
		if (ptype == IKEV2_PAYLOAD_CONFIG) {

			msg->ike_auth.cfg = payload_cfg_parse(p);

			if (!msg->ike_auth.cfg) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}
#endif /* CFG_MODULE */

		np = (struct payload_notify *)p;
		while (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) == N_IPCOMP_SUPPORTED) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			LOG_WARNING("Received IPCOMP notification. Ignoring!");

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) == N_USE_TRANSPORT_MODE) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			LOG_NOTICE("Received TRANSPORT mode notification!");
			msg->ike_auth.transport_mode = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) ==
					N_ESP_TFC_PADDING_NOT_SUPPORTED) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			LOG_NOTICE("Received TFC not supported notification!");
			msg->ike_auth.no_tfc = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) ==
					N_NON_FIRST_FRAGMENTS_ALSO) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}


			LOG_DEBUG("Received non first fragment notification!");
			msg->ike_auth.non_first_fragment = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

#ifdef MOBIKE
		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_MOBIKE_SUPPORTED) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}


			LOG_DEBUG("Received MOBIKE_SUPPORTED notification");
			msg->ike_auth.peer_supports_mobike = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		/*
		 * Process ADDITIONAL_*_ADDRESS notify payloads
		 */
		while(ptype == IKEV2_PAYLOAD_NOTIFY &&
			(ntohs(np->n_type) == N_ADDITIONAL_IP4_ADDRESS ||
			 ntohs(np->n_type) == N_ADDITIONAL_IP6_ADDRESS)) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			payload_notify_parse(&addr_buf, &mobike_notify_len, p);

			if(ntohs(np->n_type) == N_ADDITIONAL_IP4_ADDRESS) {
				LOG_DEBUG("Received ADDITIONAL_IP4_ADDRESS MOBIKE notification");
/*
 * TODO: Debug and implement adding received address to msg structure
 */
/*				peer_addr = netaddr_new_from_char(addr_buf, AF_INET);

				if(peer_addr == NULL) {
					LOG_ERROR("Invalid IPv4 address in ADDITIONAL_IP4_ADDRESS"
							  "notification");
					r1 |= MSGPARSE_SEND_NOTIFY;
					msg->notify = N_INVALID_SYNTAX;
					goto out;
				}

				inet_ntop(AF_INET, netaddr_get_ipaddr(peer_addr), addr_dump,
						  INET_ADDRSTRLEN);
				netaddr2str(peer_addr, addr_dump, sizeof(addr_dump));
				msg->ike_auth.peer_ipv4_addresses = g_slist_prepend(
						msg->ike_auth.peer_ipv4_addresses, peer_addr);
*/
				sprintf(addr_dump, "%d.%d.%d.%d",
						addr_buf[0], addr_buf[1], addr_buf[2], addr_buf[3]);
				LOG_DEBUG("MOBIKE - Dumping received IPv4 address: %s",
						  addr_dump);
			}
			else {
				LOG_DEBUG("Received ADDITIONAL_IP6_ADDRESS MOBIKE notification");
/*
 * TODO: Debug and implement adding received address to msg structure
 */
/*				netaddr_set_family(peer_addr, AF_INET6);
				netaddr2str(peer_addr, addr_dump, sizeof(addr_dump));

				msg->ike_auth.peer_ipv6_addresses = g_slist_prepend(
						msg->ike_auth.peer_ipv6_addresses, peer_addr);
*/
				memset(addr_dump, 0, sizeof(addr_dump));
				for(i = 0; i < 15; ++i) {
					sprintf(hex_token, "%02x", addr_buf[i]);
					if(i % 2 == 1)
						strcat(hex_token, ":");
					strcat(addr_dump, hex_token);
				}
				sprintf(hex_token, "%x", addr_buf[15]);
				strcat(addr_dump, hex_token);
				LOG_DEBUG("MOBIKE - Dumping received IPv6 address: %s",
						  addr_dump);
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_NO_ADDITIONAL_ADDRESSES) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			no_additional_addresses = TRUE;

			LOG_DEBUG("Received NO_ADDITIONAL_ADDRESSES notification!");

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

			np = (struct payload_notify *)p;
		}
#endif /* MOBIKE */

		if (ptype == IKEV2_PAYLOAD_SA) {
			msg->ike_auth.proposals = payload_sa_parse(p);

			if (msg->ike_auth.proposals == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		} else {

			LOG_ERROR("Expected SA payload, got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;

		}

		if (ptype == IKEV2_PAYLOAD_TSI) {
			msg->ike_auth.tsi = payload_ts_parse(p);

			if (msg->ike_sa_init.tsi == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		} else {

			LOG_ERROR("Expected TSi payload, got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;

		}

		if (ptype == IKEV2_PAYLOAD_TSR) {
			msg->ike_auth.tsr = payload_ts_parse(p);
			if (msg->ike_auth.tsr == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		} else {

			LOG_ERROR("Expected TSr payload, got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;

		}

		while (ptype == IKEV2_PAYLOAD_VENDORID) {
			LOG_NOTICE("Found VENDORID payload. Ignoring.");
	
			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}
	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * Check if we received N_NO_ADDITIONAL_ADDRESSES and if so,
	 * assert header source address is the only one, hence there was
	 * no ADDITIONAL_*_ADDRESS notify payload.
	 */
#ifdef MOBIKE
/*
 * TODO: Uncomment this part of code once adding additional addresses
 * to msg structure is finished
 */
/*	if(no_additional_addresses == TRUE)
		if((msg->ike_auth.peer_ipv4_addresses &&
			msg->ike_auth.peer_ipv4_addresses->next) ||
		   (msg->ike_auth.peer_ipv6_addresses &&
			msg->ike_auth.peer_ipv6_addresses->next)) {
			LOG_ERROR("Received notify payload for additional address as"
					  "well as NO_ADDITIONAL_ADDRESSES");
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}
*/
#endif MOBIKE

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse IKE AUTH response message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, IKE AUTH response message
 * has the following format (version without EAP autentification):
 *
 *	IDr, [CERT+],
 *	AUTH,
 *	[CP(CFG_REPLY)],
 *	[N(IPCOMP_SUPPORTED)],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	{SA, TSi, TSr | N(NO_PROPOSAL_CHOSEN) | N(TS_UNACCEPTABLE)
 *		| N(SINGLE_PAIR_REQUIRED)},
 *	[N(ADDITIONAL_TS_POSSIBLE)],
 *	[V+]
 *
 * FIXME:
 * TODO: Insert MOBIKE-related payloads in the list above (and maybe
 *   bellow too)
 *
 * In case request for IP address failed, then the response has the following
 * format:
 *
 *	IDr, [CERT+],
 *	AUTH,
 *	N(INTERNAL_ADDRESS_FAILURE),
 *	[V+]
 *
 * With EAP autentification, first response has the following contents:
 *
 *	IDr, [CERT+], AUTH,
 *	EAP,
 *	[V+]
 *
 * Then, the last response has the following format:
 * 
 *	AUTH,
 *	[CP(CFG_REPLY)],
 *	[N(IPCOMP_SUPPORTED)],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	SA, TSi, TSr | N(NO_PROPOSAL_CHOSEN) | N(TS_UNACCEPTABLE) |,
 *		N(INTERNAL_ADDRESS_FAILURE) | N(SINGLE_PAIR_REQUIRED)},
 *	[N(ADDITIONAL_TS_POSSIBLE)],
 *	[V+]
 *
 * And, again, if address assignment hasn't succedded, then the response
 * has the following format:
 *
 *	AUTH,
 *	N(INTERNAL_ADDRESS_FAILURE),
 *	[V+]
 *
 * Error responses to AUTH request message can include on of the
 * following notifications:
 *
 *	INVALID_IKE_SPI, INVALID_SYNTAX, INVALID_MESSAGE_ID,
 *	AUTHENTICATION_FAILED
 *
 */
gint message_parse_ike_auth_r(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint32 r1, r2;
	struct cert *cert;
	gboolean error;
#ifdef MOBIKE
	gboolean no_additional_addresses = FALSE;
	struct netaddr *peer_addr;
	guint16 mobike_notify_len;
	unsigned char addr_dump[48];

	unsigned char *addr_buf;
	unsigned char hex_token[4];
	int i;
#endif /* MOBIKE */

	LOG_FUNC_START(1);

#ifdef MOBIKE
	/*
	 * Create peer's address list of only one element at the moment -
	 * source address found in header.
	 */
	if(netaddr_get_family(msg->srcaddr) == AF_INET)
		msg->ike_auth.peer_ipv4_addresses =
				g_slist_append(NULL, msg->srcaddr);
	else if(netaddr_get_family(msg->srcaddr) == AF_INET6)
		msg->ike_auth.peer_ipv6_addresses =
				g_slist_append(NULL, msg->srcaddr);
#endif /* MOBIKE */

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) { 
		r1 |= (r2 & 0xFFFF0000);
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		goto out;

	}

	if (ptype == IKEV2_PAYLOAD_NOTIFY) {

		/*
		 * We don't send notify on notify, so just ignore error
		 */
		if (payload_notify_check(p) < 0)
			goto out;

		np = (struct payload_notify *)p;
		
		switch (ntohs(np->n_type)) {
		case N_INVALID_IKE_SPI:
			msg->ike_auth.invalid_ike_spi = TRUE;
			break;
		case N_INVALID_SYNTAX:
			msg->ike_auth.invalid_syntax = TRUE;
			break;
		case N_INVALID_MESSAGE_ID:
			break;
		case N_AUTHENTICATION_FAILED:
			msg->ike_auth.authentication_failed = TRUE;
			break;
		default:
			LOG_ERROR("Received unexpected notify %u",
					ntohs(np->n_type));
			break;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else if (ptype == IKEV2_PAYLOAD_IDR ||
			ptype == IKEV2_PAYLOAD_CERT ||
			ptype == IKEV2_PAYLOAD_AUTH) {

		if (ptype == IKEV2_PAYLOAD_IDR) {
			msg->ike_auth.r_id = payload_id_parse(p);

			if (msg->ike_auth.r_id == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

			while (ptype == IKEV2_PAYLOAD_CERT) {

				if ((cert = payload_cert_parse(p)) == NULL)
					goto out;

			/*
			 * FIXME:
			 * Free this memory later (in message_msg_free function
			 * that frees memory occupied by the message subsystem
			 * message)!
			 */

				msg->ike_auth.cert_list = g_slist_append(
						msg->ike_auth.cert_list, cert);

				if ((r2 = message_find_next_payload(&p, &ptype,
						hdr))) {
					r1 |= (r2 & 0xFFFF0000);
					goto out;
				}
			}
		}

		if (ptype == IKEV2_PAYLOAD_AUTH) {
			msg->ike_auth.auth_type = payload_auth_parse(
				(void *)&(msg->ike_auth.auth_payload),
				&(msg->ike_auth.auth_payload_len), p);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		}

		if (ptype == IKEV2_PAYLOAD_NOTIFY) {

			if (payload_notify_check(p) < 0)
				goto out;

			np = (struct payload_notify *)p;

			if (ntohs(np->n_type) == N_INTERNAL_ADDRESS_FAILURE) {
				LOG_ERROR("Received INTERNAL_ADDRESS_FAILURE"
						" notification.");

				msg->ike_auth.internal_address_failure = TRUE;

				if ((r2 = message_find_next_payload(&p,
						&ptype, hdr))) {
					r1 |= (r2 & 0xFFFF0000);
					goto out;
				}

				goto out_finish;
			}
		}

		if (ptype == IKEV2_PAYLOAD_EAP) {
			msg->ike_auth.eap = payload_eap_parse(p,
						&msg->ike_auth.eap_len);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

			/*
			 * This is a dirty hack to skip identing all the code
			 * that follows after this if statement!
			 */
			goto out_finish;
		}

#ifdef CFG_CLIENT
		if (ptype == IKEV2_PAYLOAD_CONFIG) {

			msg->ike_auth.cfg = payload_cfg_parse(p);

			if (!msg->ike_auth.cfg)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}
#endif /* CFG_CLIENT */

#warning "Additional checks in the following loop have to be made to guarantee correct number and occurence of notify payloads"
		while (ptype == IKEV2_PAYLOAD_NOTIFY) {

			if (payload_notify_check(p) < 0)
				goto out;

			error = FALSE;

			np = (struct payload_notify *)p;
	
			switch(ntohs(np->n_type)) {
			case N_IPCOMP_SUPPORTED:
				LOG_ERROR("Received IPCOMP notification."
						" Ignoring!");
				break;

			case N_USE_TRANSPORT_MODE:
				LOG_DEBUG("Received TRANSPORT mode "
						"notification!");
				msg->ike_auth.transport_mode = TRUE;
				break;

			case N_ESP_TFC_PADDING_NOT_SUPPORTED:
				LOG_DEBUG("Received TFC not supported "
						"notification!");
				msg->ike_auth.no_tfc = TRUE;
				break;

			case N_NON_FIRST_FRAGMENTS_ALSO:
				LOG_DEBUG("Received non first fragment "
						"notification!");
				msg->ike_auth.non_first_fragment = TRUE;
				break;

			case N_NO_PROPOSAL_CHOSEN:
				msg->ike_auth.no_proposal_chosen = TRUE;
				error = TRUE;
				break;

			case N_TS_UNACCEPTABLE:
				msg->ike_auth.ts_unacceptable = TRUE;
				error = TRUE;
				break;

			case N_INTERNAL_ADDRESS_FAILURE:
				msg->ike_auth.internal_address_failure = TRUE;
				error = TRUE;
				break;

#ifdef MOBIKE
			case N_MOBIKE_SUPPORTED:
				LOG_DEBUG("Received MOBIKE_SUPPORTED notification");
				msg->ike_auth.peer_supports_mobike = TRUE;
				break;

			case N_ADDITIONAL_IP4_ADDRESS:
				LOG_DEBUG("Received ADDITIONAL_IP4_ADDRESS MOBIKE "
						  "notification");
				payload_notify_parse(&addr_buf, &mobike_notify_len, p);
/*				msg->ike_auth.peer_ipv4_addresses = g_slist_prepend(
						msg->ike_auth.peer_ipv4_addresses, peer_addr);
*/
				sprintf(addr_dump, "%d.%d.%d.%d",
						addr_buf[0], addr_buf[1], addr_buf[2], addr_buf[3]);
				LOG_DEBUG("MOBIKE - Dumping received IPv4 address: %s",
						  addr_dump);
				break;

			case N_ADDITIONAL_IP6_ADDRESS:
				LOG_DEBUG("Received ADDITIONAL_IP6_ADDRESS MOBIKE "
						  "notification");
				payload_notify_parse(&addr_buf, &mobike_notify_len, p);
/*				msg->ike_auth.peer_ipv6_addresses = g_slist_prepend(
						msg->ike_auth.peer_ipv6_addresses, peer_addr);
*/
				memset(addr_dump, 0, sizeof(addr_dump));
				for(i = 0; i < 15; ++i) {
					sprintf(hex_token, "%02x", addr_buf[i]);
					if(i % 2 == 1)
						strcat(hex_token, ":");
					strcat(addr_dump, hex_token);
				}
				sprintf(hex_token, "%x", addr_buf[15]);
				strcat(addr_dump, hex_token);
				LOG_DEBUG("MOBIKE - Dumping received IPv6 address: %s",
						  addr_dump);
				break;

			case N_NO_ADDITIONAL_ADDRESSES:
				LOG_DEBUG("Received NO_ADDITIONAL_ADDRESSES MOBIKE"
						  "notification");
				no_additional_addresses = TRUE;
				break;

#endif /*MOBIKE */

			default:
				LOG_NOTICE("Ignoring unexpected notification "
						"%u", ntohs(np->n_type));
				break;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

			if (error) {
				/*
				 * This is a dirty hack to skip identing all
				 * the code that follows after this if
				 * statement!
				 */
				goto out_finish;
			}

		}

		if (ptype == IKEV2_PAYLOAD_SA) {
			msg->ike_auth.proposals = payload_sa_parse(p);

			if (msg->ike_auth.proposals == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p,
						&ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		} else {
			LOG_ERROR("Expected SA payload, got %u", ptype);
			goto out;
		}

		if (ptype == IKEV2_PAYLOAD_TSI) {

			msg->ike_auth.tsi = payload_ts_parse(p);
			if (msg->ike_auth.tsi == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p,
							&ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		} else {
			LOG_ERROR("Expected TSi payload, got %u",
					ptype);
			goto out;
		}

		if (ptype == IKEV2_PAYLOAD_TSR) {

			msg->ike_auth.tsr = payload_ts_parse(p);
					
			if (msg->ike_auth.tsr == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p,
							&ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		} else {
			LOG_ERROR("Expected TSr payload, got %u",
					ptype);
			goto out;
		}

		np = (struct payload_notify *)p;
		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) == N_ADDITIONAL_TS_POSSIBLE) {

			if (payload_notify_check(p) < 0)
				goto out;

			LOG_ERROR("Received ADDITIONAL_TS_POSSIBLE "
					"notification. Ignoring!");

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		while (ptype == IKEV2_PAYLOAD_VENDORID) {
			LOG_DEBUG("Found VENDORID payload. Ignoring.");

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

	} else if (ptype == IKEV2_PAYLOAD_EAP) {

		msg->ike_auth.eap = payload_eap_parse(p,
						&msg->ike_auth.eap_len);

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else {

		LOG_ERROR("Unexpected payload, got %u", ptype);
		goto out;
	}

out_finish:
	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		goto out;
	}

	/*
	 * Check if we received N_NO_ADDITIONAL_ADDRESSES and if so,
	 * assert header source address is the only one, hence there was
	 * no ADDITIONAL_*_ADDRESS notify payload.
	 */
#ifdef MOBIKE
	if(no_additional_addresses == TRUE)
		if((msg->ike_auth.peer_ipv4_addresses &&
			msg->ike_auth.peer_ipv4_addresses->next) ||
		   (msg->ike_auth.peer_ipv6_addresses &&
			msg->ike_auth.peer_ipv6_addresses->next)) {
			LOG_ERROR("Received notify payload for additional address as"
					  "well as NO_ADDITIONAL_ADDRESSES");
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}
#endif MOBIKE

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse CREATE CHILD SA request message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, CREATE CHILD SA request has
 * the following format for rekeying existing CHILD SA or creating a new one:
 *
 *	[N(REKEY_SA)],
 *	[N(IPCOMP_SUPPORTED)+],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	SA, Ni, [KEi], TSi, TSr
 *
 * In case IKE SA is rekeyed, then the format is as follows:
 *
 *	SA, Ni, [KEi]
 */
gint message_parse_create_child_sa_i(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint32 r1, r2;

	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		msg->notify = r2 & 0xFFFF;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		LOG_TRACE("Found encrypted payload!");
		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;

	}

	np = (struct payload_notify *)p;
	if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_REKEY_SA) {

		LOG_TRACE("Found REKEY_SA notify payload!");
		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		msg->create_child_sa.rekey_spi = *(guint32 *)(p +
						sizeof(struct payload_notify));
		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	np = (struct payload_notify *)p;
	while (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_IPCOMP_SUPPORTED) {

		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		LOG_ERROR("Received IPCOMP notification. Ignoring!");
		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	np = (struct payload_notify *)p;

	if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_USE_TRANSPORT_MODE) {

		LOG_TRACE("Found use transport mode notify payload!");
		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		msg->create_child_sa.transport_mode = TRUE;

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	np = (struct payload_notify *)p;
	if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_ESP_TFC_PADDING_NOT_SUPPORTED) {

		LOG_TRACE("Found tfc padding not supported "
				"mode notify payload!");
		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		msg->create_child_sa.no_tfc = TRUE;

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	if (ptype == IKEV2_PAYLOAD_NOTIFY &&
			ntohs(np->n_type) == N_NON_FIRST_FRAGMENTS_ALSO) {

		LOG_TRACE("Found non first fragments also notify payload");
		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		msg->create_child_sa.non_first_fragment = TRUE;

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	if (ptype == IKEV2_PAYLOAD_SA) {

		LOG_TRACE("Found SA payload");
		msg->create_child_sa.proposals = payload_sa_parse(p);
		if (msg->create_child_sa.proposals == NULL) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {
		LOG_ERROR("Expected SA payload, got %u", ptype);
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_NONCE) {

		LOG_TRACE("Found NONCE payload");
		msg->create_child_sa.peer_nonce = payload_nonce_parse(p);
		if (msg->create_child_sa.peer_nonce == NULL) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;

			goto out;
		}

	} else {
		LOG_TRACE("Expected Ni payload, got %u", ptype);
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_KE) {
		msg->create_child_sa.peer_pub_key = payload_ke_parse(
				&msg->create_child_sa.peer_dh_group, p);

		if (msg->create_child_sa.peer_pub_key == NULL) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}
	}

	/*
	 * Check if TSi, TSr payloads are present and if so, parse
	 * them.
	 */
	if (ptype == IKEV2_PAYLOAD_TSI) {
		msg->create_child_sa.tsi = payload_ts_parse(p);

		if (msg->create_child_sa.tsi == NULL) {
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

		if (ptype == IKEV2_PAYLOAD_TSR) {
			msg->create_child_sa.tsr = payload_ts_parse(p);

			if (msg->create_child_sa.tsr == NULL) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		} else {

			LOG_ERROR("Expected TSr payload, got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

	} else {

		if (msg->create_child_sa.rekey_spi) {
			LOG_ERROR("Rekey NOTIFY present. Expected TSi payload,"
					" got %u", ptype);
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

		if (!msg->create_child_sa.tsi &&
				!msg->create_child_sa.peer_pub_key) {
			LOG_ERROR("Message without TSi, TSr and KE is illegal");
			r1 |= MSGPARSE_SEND_NOTIFY;
			msg->notify = N_INVALID_SYNTAX;
			goto out;
		}

	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse CREATE CHILD SA response message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, CREATE CHILD SA response has
 * the following format for rekeying existing CHILD SA or creating a new one:
 *
 *	[N(IPCOMP_SUPPORTED)],
 *	[N(USE_TRANSPORT_MODE)],
 *	[N(ESP_TFC_PADDING_NOT_SUPPORTED)],
 *	[N(NON_FIRST_FRAGMENTS_ALSO)],
 *	SA, Nr, [KEr], TSi, TSr,
 *	[N(ADDITIONAL_TS_POSSIBLE)]
 *
 * And for rekeying IKE SA it has the following format:
 *
 *	SA, Nr, [KEr]
 *
 * The following notify payloads can also be received
 *
 *	INVALID_SYNTAX, 
 *	NO_PROPOSAL_CHOSEN, 
 *	TS_UNNACEPTABLE, 
 */
gint message_parse_create_child_sa_r(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_notify *np;
	guint32 r1, r2;
	gboolean error;

	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a received message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		goto out;

	}

	/*
	 * TODO: Since error notifies have exact range defined, we
	 * can use that fact so that no individual notifies are
	 * checked! Ofcourse, the assumption is that error notifies
	 * prevent creating of a new CHILD SA.
	 */
	np = (struct payload_notify *)p;
	if (ptype == IKEV2_PAYLOAD_NOTIFY && 
			ntohs(np->n_type) != N_IPCOMP_SUPPORTED &&
			ntohs(np->n_type) != N_USE_TRANSPORT_MODE &&
			ntohs(np->n_type) != N_ESP_TFC_PADDING_NOT_SUPPORTED &&
			ntohs(np->n_type) != N_NON_FIRST_FRAGMENTS_ALSO) {

		error = FALSE;

		if (payload_notify_check(p) < 0) {
			r1 |= MSGPARSE_TERMINATE_MSG;
			goto out;
		}

		switch (ntohs(np->n_type)) {
		case N_INVALID_SYNTAX:
			msg->create_child_sa.invalid_syntax = TRUE;
			break;
		case N_NO_ADDITIONAL_SAS:
			msg->create_child_sa.no_additional_sas = TRUE;
			break;
		case N_NO_PROPOSAL_CHOSEN:
			msg->create_child_sa.no_proposal_chosen = TRUE;
			error = TRUE;
			break;
		case N_TS_UNACCEPTABLE:
			msg->create_child_sa.ts_unacceptable = TRUE;
			error = TRUE;
			break;
#if 0
		case N_INVALID_MESSAGE_ID:
			msg->create_child_sa.invalid_message_id = TRUE;
			error = TRUE;
			break;
#endif
		default:
			LOG_ERROR("Received unexpected notify %u",
					ntohs(np->n_type));
			break;
		}

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

		if (error) {
			/*
			 * This is a dirty hack to skip identing all
			 * the code that follows after this if
			 * statement!
			 */
			goto out_finish;
		}

	} else {

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) == N_IPCOMP_SUPPORTED) {

			if (payload_notify_check(p) < 0)
				goto out;

			LOG_WARNING("Received IPCOMP notification. Ignoring!");

			if ((r2 = message_find_next_payload(&p, &ptype,
					hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		}

		np = (struct payload_notify *)p;
		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type) == N_USE_TRANSPORT_MODE) {

			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_TERMINATE_MSG;
				goto out;
			}

			msg->create_child_sa.transport_mode = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		np = (struct payload_notify *)p;

		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type)
					== N_ESP_TFC_PADDING_NOT_SUPPORTED) {

			if (payload_notify_check(p) < 0)
				goto out;

			msg->create_child_sa.no_tfc = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		np = (struct payload_notify *)p;
		if (ptype == IKEV2_PAYLOAD_NOTIFY &&
				ntohs(np->n_type)
						== N_NON_FIRST_FRAGMENTS_ALSO) {

			if (payload_notify_check(p) < 0)
				goto out;

			msg->create_child_sa.non_first_fragment = TRUE;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		if (ptype == IKEV2_PAYLOAD_SA) {
			msg->create_child_sa.proposals = payload_sa_parse(p);

			if (msg->create_child_sa.proposals == NULL) {
				r1 |= MSGPARSE_TERMINATE_MSG;
				goto out;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		} else {
			LOG_ERROR("Expected SA payload, got %u", ptype);
			r1 |= MSGPARSE_TERMINATE_MSG;
			goto out;
		}


		if (ptype == IKEV2_PAYLOAD_NONCE) {
			msg->create_child_sa.peer_nonce =
							payload_nonce_parse(p);

			if (msg->create_child_sa.peer_nonce == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}


		} else {
			LOG_ERROR("Expected Nr payload, got %u", ptype);
			r1 |= MSGPARSE_TERMINATE_MSG;
			goto out;
		}

		if (ptype == IKEV2_PAYLOAD_KE) {
			msg->create_child_sa.peer_pub_key = payload_ke_parse(
					&msg->create_child_sa.peer_dh_group, p);

			if (msg->create_child_sa.peer_pub_key == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		if (ptype == IKEV2_PAYLOAD_TSI) {
			msg->create_child_sa.tsi = payload_ts_parse(p);

			if (msg->create_child_sa.tsi == NULL)
				goto out;

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

			if (ptype == IKEV2_PAYLOAD_TSR) {
				msg->create_child_sa.tsr = payload_ts_parse(p);

				if (msg->create_child_sa.tsr == NULL)
					goto out;

				if ((r2 = message_find_next_payload(&p, &ptype,
						hdr))) {
					r1 |= (r2 & 0xFFFF0000);
					goto out;
				}

			} else {
				LOG_ERROR("Expected TSr payload, got %u",
						ptype);
				goto out;
			}

			np = (struct payload_notify *)p;
			if (ptype == IKEV2_PAYLOAD_NOTIFY &&
					ntohs(np->n_type) ==
						N_ADDITIONAL_TS_POSSIBLE) {

				if (payload_notify_check(p) < 0)
					goto out;

				LOG_ERROR("Received ADDITIONAL_TS_POSSIBLE"
						" notify. Ignoring!");

				if ((r2 = message_find_next_payload(&p, &ptype,
						hdr))) {
					r1 |= (r2 & 0xFFFF0000);
					goto out;
				}
			}
		}
	}

out_finish:
	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse INFORMATIONAL request message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, INFORMATIONAL request message
 * has the following format:
 *
 *	[N+],
 *	[D+],
 *	[CP(CFG_REQUEST)]
 */
gint message_parse_informational_i(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_delete_data *delete;
	struct payload_notify *np;
	guint32 r1, r2;

	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received request...
	 */

	/*
	 * Parsing of a informational request message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		msg->notify = r2 & 0xFFFF;
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			msg->notify = r2 & 0xFFFF;
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;

	}

#warning "Processing of different Notifies in informational msgs not implemented!"
	while (ptype == IKEV2_PAYLOAD_NOTIFY 
			|| ptype == IKEV2_PAYLOAD_DELETE
			|| ptype == IKEV2_PAYLOAD_CONFIG) {

		while (ptype == IKEV2_PAYLOAD_NOTIFY) {
	
			if (payload_notify_check(p) < 0) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

			np = (struct payload_notify *)p;
	
			switch(ntohs(np->n_type)) {
			case N_INVALID_SPI:
				goto out;

			case N_INVALID_MAJOR_VERSION:
				goto out;

			case N_INVALID_SYNTAX:
				break;
			default:
				break;
			}

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		}

		while (ptype == IKEV2_PAYLOAD_DELETE) {

			if ((delete = payload_delete_parse(p)) == NULL) {
				msg->notify = N_INVALID_SYNTAX;
				r1 |= MSGPARSE_SEND_NOTIFY;
				goto out;
			}

			msg->informational.deletes = 
				g_slist_append(msg->informational.deletes, 
						delete);

			if ((r2 = message_find_next_payload(&p, &ptype,
					hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}

		}

		if (ptype == IKEV2_PAYLOAD_CONFIG) {

#ifdef CFG_MODULE
			msg->informational.cfg = payload_cfg_parse(p);

			if (!msg->ike_auth.cfg) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}
#else /* CFG_MODULE */

			LOG_WARNING("Config request payload found! Ignoring!");

#endif /* CFG_MODULE */

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				msg->notify = r2 & 0xFFFF;
				goto out;
			}
		}

	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_WARNING("Unexpected payload an the end of the message");
		r1 |= MSGPARSE_SEND_NOTIFY;
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Parse INFORMATIONAL response message
 *
 * @param hdr
 * @param p
 * @param msg			Structure with received network data
 *
 * \return 0	Everything went OK
 *		   != 0	MSGPARSE_* flag combination that determines what has to
 *		be done. Any additional data is placed into appropriate
 *		msg fields.
 *
 * According to IKEv2 clarification document, INFORMATIONAL response message
 * has the following format:
 *
 *	[N+],
 *	[D+],
 *	[CP(CFG_REPLY)]
 */
gint message_parse_informational_r(struct ikev2_header *hdr, gchar *p, 
		struct message_msg *msg)
{
	guint8 ptype;
	struct payload_delete_data *delete;
	guint32 r1, r2;
	
	LOG_FUNC_START(1);

	/*
	 * Assume that error occured...
	 */
	r1 = MSGPARSE_TERMINATE_MSG;

	/*
	 * Sanity check of received response...
	 */

	/*
	 * Parsing of a informational response message...
	 */
#warning "This is redundant, to reinitialize unconditinally input argument"
	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;
	hdr = (struct ikev2_header *)p;

	if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
		r1 |= (r2 & 0xFFFF0000);
		goto out;
	}

	if (ptype == IKEV2_PAYLOAD_ENCRYPTED) {

		if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
			r1 |= (r2 & 0xFFFF0000);
			goto out;
		}

	} else {

		LOG_ERROR("Received unencrypted payload!");
		goto out;

	}

	while (ptype == IKEV2_PAYLOAD_NOTIFY 
			|| ptype == IKEV2_PAYLOAD_DELETE
			|| ptype == IKEV2_PAYLOAD_CONFIG) {
	
		while (ptype == IKEV2_PAYLOAD_NOTIFY) {
#warning "Processing of different Notifies in informational msgs not implemented!"

			if ((r2 = message_find_next_payload(&p, &ptype,
					hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}

		}

		while (ptype == IKEV2_PAYLOAD_DELETE) {

			if ((delete = payload_delete_parse(p)) == NULL)
				goto out;

			msg->informational.deletes = 
				g_slist_append(msg->informational.deletes,
						delete);

			if ((r2 = message_find_next_payload(&p, &ptype, hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}

		if (ptype == IKEV2_PAYLOAD_CONFIG) {

#ifdef CFG_CLIENT
			msg->informational.cfg = payload_cfg_parse(p);

			if (!msg->ike_auth.cfg) {
				r1 |= MSGPARSE_SEND_NOTIFY;
				msg->notify = N_INVALID_SYNTAX;
				goto out;
			}

#else /* CFG_CLIENT */

			LOG_ERROR("Config reply payload found! Ignoring!");
#endif /* CFG_CLIENT */
		
			if ((r2 = message_find_next_payload(&p, &ptype,
					hdr))) {
				r1 |= (r2 & 0xFFFF0000);
				goto out;
			}
		}
	}

	if (ptype != IKEV2_PAYLOAD_NONE) {
		LOG_ERROR("Unexpected payload an the end of the message");
		goto out;
	}

	/*
	 * No error occured...
	 */
	r1 = 0;

out:
	LOG_FUNC_END(1);
	return r1;
}

/**
 * Function to parse received message
 *
 * @param msg		Received message
 * @param session	Session to whom the message belongs
 *
 * \return	MSGPARSE_TERMINATE_MSG
 *		MSGPARSE_SEND_NOTIFY
 *
 * In case that received response message has been parsed without error, 
 * corresponding request is removed from a queue. Also, in case
 * request has been received, then, response outside of receive window
 * is released.
 */
gint message_parse(struct message_msg *msg, struct session *session)
{
	char *p, *cchksum;
	struct ikev2_header *hdr;
	struct sent_msg *sent;
	GSList *count;
	guint32 retval;
	gint8 ptype, ilen;
	guint32 len, lo;
	gboolean released;

	LOG_FUNC_START(1);
	LOG_DEBUG("msg_id=%u response=%s", msg->msg_id,
			msg->response ? "true" : "false");

	retval = MSGPARSE_TERMINATE_MSG | MSGPARSE_SEND_NOTIFY;

	p = (char *)(msg->buffer);
	if (*(guint32 *)p == 0)
		p += 4;

	hdr = (struct ikev2_header *)p;
	msg->notify = 0;

	/*
	 * Global checks valid for any message:
	 */

	/*
	 * Check version...
	 */
	if (hdr->mj_ver != IKEV2_MAJOR_VERSION) {
		LOG_ERROR("Invalid IKE major version (%d)",
				(int)(hdr->mj_ver));
		msg->notify = N_INVALID_MAJOR_VERSION;
		goto out;
	}

	/*
	 * Check sizes...
	 */
	if (msg->size < sizeof(struct ikev2_header)) {
		LOG_ERROR("Received message is to small (size=%u)",
				msg->size);
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	if (msg->size != ntohl(hdr->length)) {
		LOG_ERROR("Received message has incorrect length value"
			" (expected %u, declared %u)",
			msg->size, ntohl(hdr->length));
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * Check exchange type...
	 */
	if (hdr->exchg_type != IKEV2_EXT_IKE_SA_INIT
		&& hdr->exchg_type != IKEV2_EXT_IKE_AUTH
		&& hdr->exchg_type != IKEV2_EXT_CREATE_CHILD_SA
		&& hdr->exchg_type != IKEV2_EXT_INFORMATIONAL) {

		LOG_ERROR("Invalid exchange type value in"
				 " header (%u)", hdr->exchg_type);
		msg->notify = N_INVALID_SYNTAX;
		goto out;
	}

	/*
	 * This is a default notify error we are sending back.
	 */
	msg->notify = N_INVALID_MESSAGE_ID;
	if (!session) {

		if (msg->msg_id) {
			LOG_ERROR("Received message has invalid message"
					" id (%u)", msg->msg_id);
			goto out;
		}

	} else {

		LOG_DEBUG("msgid_lo=%u, msgid_hi=%u, wsize=%u",
				session->msgid_lo, session->msgid_hi,
				session->wsize);

		LOG_DEBUG("peer_msgid_lo=%u, peer_wsize=%u",
				session->peer_msgid_lo, session->peer_wsize);
	}

	/*
	 * Check to see if this is expected message id...
	 */
	if (msg->response) {

		g_mutex_lock(session->sent_req_mux);

		/*
		 * Check if the received response is inside expected window...
		 */
		if (msg->msg_id < session->msgid_lo
				|| session->msgid_hi <= msg->msg_id) {
			LOG_ERROR("Received message has message id (%u)"
					" outside of receive window",
					msg->msg_id);
			g_mutex_unlock(session->sent_req_mux);
			goto out;
		}

		g_mutex_unlock(session->sent_req_mux);
	}

	/*
	 * If this is a request message then check that it's expected...
	 */
	if (session && !msg->response) {

		/*
		 * Lock variables until we examine if this is valid request...
		 */
		g_mutex_lock(session->sent_resp_mux);
		retval = MSGPARSE_TERMINATE_MSG;

		/*
		 * For request, check if it's above expected window...
		 */
		if (session->peer_msgid_lo + session->peer_wsize
				<= msg->msg_id) {
			LOG_ERROR("Received message has message id (%u)"
					" outside of receive window",
					msg->msg_id);
			g_mutex_unlock(session->sent_resp_mux);
			goto out;
		}

		/*
		 * Check if it's below saved window of replies...
		 */
		if (msg->msg_id + session->peer_wsize <
				session->peer_msgid_hi) {
			LOG_ERROR("Received message has message id (%u)"
					" outside of receive window"
					" (peer_msgid_hi=%u, peer_wsize=%u)",
					msg->msg_id, session->peer_msgid_hi,
					session->peer_wsize);
			g_mutex_unlock(session->sent_resp_mux);
			goto out;
		}

		/*
		 * If received msg_id is in a saved window of replies...
		 */
		if (session->peer_msgid_hi <=
				msg->msg_id + session->peer_wsize &&
			msg->msg_id < session->peer_msgid_lo) {
			message_retransmit_response(session, msg->msg_id);
			g_mutex_unlock(session->sent_resp_mux);
			goto out;
		}

		/*
		 * We are inside the active window, so it might happen that we
		 * already received this request. If so, then just ignore it...
		 */
		for (count = session->sent_resp; count; count = count->next) {
			sent = count->data;

			if (sent->msg_id < msg->msg_id)
				continue;

			if (sent->msg_id > msg->msg_id)
				break;

			LOG_DEBUG("Received duplicate request with id %u",
					msg->msg_id);

			g_mutex_unlock(session->sent_resp_mux);
			goto out;
		}

		/*
		 * If we are here, then we didn't receive given request and
		 * thus, we should reserve place for response.
		 */
		if ((sent = sent_msg_new()) == NULL) {
			g_mutex_unlock(session->sent_resp_mux);
			goto out;
		}

		sent->msg_id = msg->msg_id;
		sent->response = TRUE;
		sent->packet_size = -1;

		session->sent_resp = g_slist_insert_sorted(session->sent_resp,
						sent, sent_cmp_msgid);

		g_mutex_unlock(session->sent_resp_mux);
	}

	/*
	 * Request/response is expected so we are proceeding on
	 * processing (parsing) it...
	 */
	if ((ptype = hdr->next) == IKEV2_PAYLOAD_ENCRYPTED) {

		p += sizeof(struct ikev2_header);

		g_assert(session != NULL);
		g_assert(session->integ_type != NULL);

		ilen = integ_chksumlen_get(session->integ_type);

		/*
		 * First, check the integrity of a message.
		 * TODO: Check return values!
		 */
		if (integ(session->integ_type,
				session_get_ike_type(session) == IKEV2_INITIATOR
						? session->sk_ar
						: session->sk_ai,
				session->integ_type->key_len,
				(char *)hdr,
				ntohl(hdr->length) - ilen,
				&cchksum) < 0)
			goto out;

		if (memcmp(cchksum,
				(char *)hdr + ntohl(hdr->length) - ilen,
				ilen)) {
			LOG_ERROR("Integrity check on message failed!");
			goto out;
		}

		g_free(cchksum);
		msg->integrity_ok = TRUE;

		((struct payload_generic_header *)p)->length =
				htons(ntohs(
				((struct payload_generic_header *)p)->length)
					- ilen);

		hdr->length = htonl(ntohl(hdr->length) - ilen);

		len = payload_encr_parse(p, session->encr_type,
				session_get_ike_type(session) == IKEV2_INITIATOR
						? session->sk_er
						: session->sk_ei);

		/*
		 * Put actuall length of encrypted payload header into
		 * appropriate field...
		 */
		((struct payload_generic_header *)p)->length = htons(len);

	}

	/*
	 * Parsing of message depending on exchange...
	 */
	switch (msg->exchg_type) {
	case IKEV2_EXT_IKE_SA_INIT:
		/**
		 * IKE_SA_INIT can't have encrypted payload, so we
		 * could actually have just one argument - msg..
		 */
		if (msg->response)
			retval = message_parse_ikesa_init_r(hdr, p, msg);
		else
			retval = message_parse_ikesa_init_i(hdr, p, msg);
		break;

	case IKEV2_EXT_IKE_AUTH:
		if (msg->response)
			retval = message_parse_ike_auth_r(hdr, p, msg);
		else
			retval = message_parse_ike_auth_i(hdr, p, msg);
		break;

	case IKEV2_EXT_CREATE_CHILD_SA:
		if (msg->response)
			retval = message_parse_create_child_sa_r(hdr, p, msg);
		else
			retval = message_parse_create_child_sa_i(hdr, p, msg);
		break;

	case IKEV2_EXT_INFORMATIONAL:
		if (msg->response)
			retval = message_parse_informational_r(hdr, p, msg);
		else
			retval = message_parse_informational_i(hdr, p, msg);
		break;

	default:
		LOG_ERROR("Unknown exchange type %u, ignoring message",
			msg->exchg_type);
		retval = MSGPARSE_TERMINATE_MSG;
		/*
		 * FIXME: Should we send invalid syntax notify?
		 */
		break;
	}

	/*
	 * For recieved response: if message was correctly parsed, remove
	 * messages in a queue and update request window.
	 */
	if (!retval && msg->response) {

		/*
		 * Remove request that triggered received response.
		 */
		LOG_DEBUG("Removing request that triggered received response");

		g_mutex_lock(session->sent_req_mux);
		lo = session->msgid_hi;
		count = session->sent_req;
		released = FALSE;

		while (count) {
			sent = count->data;
			count = count->next;

			if (sent->msg_id != msg->msg_id) {
				if (sent->msg_id < lo)
					lo = sent->msg_id;
				continue;
			}

			LOG_DEBUG("Removing request packet (id=%u)"
					" from the queue", sent->msg_id);

			session->sent_req = g_slist_remove(session->sent_req,
							sent);
			sent_msg_free(sent);

			if (released)
				LOG_BUG("sent structure remembered multiple"
						" times. Ignoring");

			released = TRUE;
		}

		if (!released)
			LOG_DEBUG("Didn't find request that matches received "
					"response id=%u", msg->msg_id);

		/*
		 * We are incrementing lower bound of send window only
		 * if the following conditions hold:
		 *
		 * 1) ID of received message is the oldest sent request
		 *    (this garantuees that we don't have "holes" in a
		 *    window)
		 *
		 * 2) It is not a message with repeated request, like,
		 *    e.g. in case we received cookie, or different
		 *    DH group.
		 *
		 * When update is approved, we set it to the first
		 * sent but unacknowledged ID.
		 */
		if (msg->exchg_type == IKEV2_EXT_IKE_SA_INIT) {
			if (msg->ike_sa_init.peer_dh_group > 0 &&
					!msg->ike_sa_init.cookie)
				if (session->msgid_lo == msg->msg_id)
					session->msgid_lo = lo;

			LOG_DEBUG("msg->peer_dh_group=%u,"
					"msg->cookie=%d",
					msg->ike_sa_init.peer_dh_group,
					msg->ike_sa_init.cookie);
		} else {
			if (session->msgid_lo == msg->msg_id)
				session->msgid_lo = lo;
		}

		LOG_DEBUG("session->msgid_lo=%u, session->msgid_hi=%u, "
				"msg->msg_id=%u, lo=%u",
				session->msgid_lo, session->msgid_hi,
				msg->msg_id, lo);

		g_mutex_unlock(session->sent_req_mux);

	}

	/*
	 * If there was an error in received request, then drop prepared
	 * sent_msg structure...
	 */
	if (!msg->response && session) {

		LOG_TRACE("Process queue of requests...");

		g_mutex_lock(session->sent_resp_mux);

		if (retval) {

			/*
			 * We a inside the active window, so it might happen
			 * that we already received this request. If so, then
			 * just ignore it...
			 */
			for (count = session->sent_resp; count;) {
				sent = count->data;
				count = count->next;

				if (sent->msg_id < msg->msg_id)
					continue;

				/*
				 * There _has_ to be entry so we don't check
				 * if msg_ids are equal...
				 */

				LOG_DEBUG("Removing response packet (id=%u)"
						" from a queue", sent->msg_id);

				session->sent_resp = g_slist_remove(
							session->sent_resp,
							sent);
				sent_msg_free(sent);
				
				count = session->sent_resp;
			}

		} else {

			/*
			 * Request is legal, so update highest recevied request
			 * seen so far and free any responses that fall now
			 * outside window...
			 */
			LOG_DEBUG("Update highest received message ID");

			session->peer_msgid_hi = MAX(session->peer_msgid_hi,
								msg->msg_id + 1);
			for (count = session->sent_resp; count;) {
				sent = count->data;

				/*
				 * TODO: This loop can probably be more
				 * optimized.
				 */
				if (sent->msg_id < (session->peer_msgid_hi
							- session->wsize)) {
					LOG_DEBUG("Removing response packet "
							"(id=%u) from a queue",
							sent->msg_id);
					session->sent_resp = g_slist_remove(
							session->sent_resp,
							sent);
					sent_msg_free(sent);
					count = session->sent_resp;
				} else
					count = count->next;
			}
		}

		g_mutex_unlock(session->sent_resp_mux);
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/*******************************************************************************
 * FUNCTIONS FOR SENDING MESSAGES TO NETWORK AND TAKING CARE OF TIMEOUTS...
 ******************************************************************************/

/**
 * Remove all packets in queues that belong to a given session.
 *
 */
void message_remove_session(struct session *session)
{
	struct sent_msg *sent;
	GSList *iterator;

	LOG_FUNC_START(1);

	LOG_TRACE("session=%p", session);

	g_mutex_lock(session->sent_resp_mux);
	iterator = session->sent_resp;
	while (iterator) {
		sent = iterator->data;
		iterator = iterator->next;

		LOG_TRACE("Removing response packet (id=%u) from a queue",
				sent->msg_id);

		session->sent_resp = g_slist_remove(session->sent_resp, sent);
		sent_msg_free(sent);
	}
	g_mutex_unlock(session->sent_resp_mux);

	g_mutex_lock(session->sent_req_mux);
	iterator = session->sent_req;
	while (iterator) {
		sent = iterator->data;
		iterator = iterator->next;

		LOG_TRACE("Removing request packet (id=%u) from a queue",
			sent->msg_id);

		session->sent_req = g_slist_remove(session->sent_req, sent);
		sent_msg_free(sent);
	}
	g_mutex_unlock(session->sent_req_mux);

	LOG_FUNC_END(1);
}

gboolean message_retransmit_response(struct session *session, guint32 msg_id)
{
	struct sent_msg *sent;
	GSList *iterator;
	gboolean ret = FALSE;

	LOG_FUNC_START(1);

	for (iterator = session->sent_resp; iterator;
					iterator = iterator->next) {
		sent = iterator->data;

		if (sent->msg_id != msg_id)
			continue;

		network_send_packet(&(message_data->network_data),
				sent->packet, sent->packet_size,
				sent->daddr);

		ret = TRUE;

		goto out;
	}

	LOG_ERROR("Didn't found message for retransmit with msg_id=%d", msg_id);

out:
	LOG_FUNC_END(1);

	return ret;
};

/**
 * This function is called by the timeout module. It is used to
 * retransmit message for which no response have been received
 * within given time frame.
 *
 * @param _sent
 *
 * return
 */
gpointer message_retransmit_request(gpointer _sent)
{
	struct session *session;
	struct sent_msg *sent = _sent;
	struct timeout_msg *to;

	LOG_FUNC_START(LOG_PRIORITY_TRACE);

	/*
	 * This is to assure that we didn't received response
	 * in the exact moment timeout triggered execution of
	 * this function!
	 */
	sent_msg_lock(sent);

	/*
	 * Since timeout has expired, remove it's reference
	 * because it's invalid now...
	 */
	sent->to = NULL;

	/*
	 * If we are the last user of this message then
	 * response has been received, so remove structure
	 * and terminate
	 */
	if (sent->refcnt == 1) {
		sent_msg_unlock(sent);
		sent_msg_free(sent);
		goto out;
	}

	LOG_ERROR("Timeout reached for request msgid=%u", sent->msg_id);

	/*
	 * If we are not allowed to try sending again (number of retries
	 * is zero), or sending didn't succeed, notify state machines
	 * about error.
	 */
	if (!sent->retries-- ||
			(network_send_packet(sent->ns, sent->packet,
				sent->packet_size, sent->daddr) !=
						sent->packet_size)) {

		LOG_TRACE("Releasing sent structure");

		session = sent->session;

		/*
 		 * Remove sent_msg from the list
 		 */
		g_mutex_lock(session->sent_req_mux);
		session->sent_req = g_slist_remove(session->sent_req, sent);
		g_mutex_unlock(session->sent_req_mux);

		/*
		 * Create and send informational message to a controller
		 * (state machine)
		 *
		 * NOTE/TODO: Maybe we can send send_msg messages to
		 * state machines?
		 */
		if ((to = timeout_msg_alloc())) {
			to->session = session;
			g_async_queue_push (message_data->upper_queue, to);
		}

		sent_msg_unlock(sent);
		sent_msg_free(sent);

	} else {

		/*
		 * Register new timeout with time adjusted by exponential
		 * value (exponential backoff)...
		 */
		sent->tv.tv_sec <<= 1;

		sent->to = timeout_register_thread(&sent->tv, 0, sent,
					message_retransmit_request);

		sent_msg_unlock(sent);
	}

out:
	LOG_FUNC_END(LOG_PRIORITY_TRACE);

	return NULL;
}

/**
 * Send packet and put it into request/response queue.
 *
 * @param ns		On which socket we should send packet. If NULL
 *			then default socket is used (send_sock).
 * @param session
 * @param data		Pointer to a message to send. Message is preceeded
 *			with four null octets in case we are doing NAT-T.
 * @param len
 * @param daddr
 *
 * \return
 */
int message_send_packet(struct network_socket *ns, struct session *session,
		char *buffer, gint len, struct netaddr *daddr)
{
	struct sent_msg *sent;
	gint retval;
	guint32 msg_id, next;
	gboolean response;
	GSList *count;
	gchar *data = buffer;

	LOG_FUNC_START(1);

	retval = -1;

	/*
	 * Maybe the following two should be made input arguments?
	 */
	response = ((struct ikev2_header *)(data + 4))->flags &
				IKEV2_FLAG_RESPONSE;
	msg_id = ntohl(((struct ikev2_header *)(data + 4))->msg_id);

	if (len > IKEV2_MESSAGE_MUST_MAX)
		LOG_ERROR("Sending message that exceeds maximum size");
	else if (len > IKEV2_MESSAGE_SHOULD_MAX)
		LOG_WARNING("Sending message that exceeds maximum "
				"recommended size");

	/*
	 * If ns argument is NULL then take socket from message_data,
	 * that is created explicitly for sending packets.
	 */
	if (!ns) {
		if (netaddr_get_family(daddr) == AF_INET) {

#ifdef NATT
			if (session->natt && msg_id)
				ns = message_data->natt_send_sock;
			else
#endif /* NATT */
				ns = message_data->send_sock;

		} else if (netaddr_get_family(daddr) == AF_INET6) {
			ns = message_data->send_sock6;
		} else {
			LOG_BUG("Unknown address family!");
			goto out;
		}
	}

#ifdef NATT
	if (!session->natt || !msg_id) {

		/**
		 * If NAT-T is off, or we are sending messages from the
		 * IKE_SA_INIT exchange, then skip encapsulation header
		 * in the message.
		 */
#endif /* NATT */
		data += 4;
		len -= 4;	
#ifdef NATT
	}
#endif /* NATT */

	retval = network_send_packet(ns, data, len, daddr);

	if (retval != len) {
		/*
		 * TODO: Maybe if it's non fatal error we should try later
		 * instead of quitting?
		 */
		g_free(buffer);
		retval = -1;
		goto out;
	}

	/*
	 * If this is a response, update peer's request window and free
	 * any saved responses that are now outside of a window...
	 */
	if (response) {
		g_mutex_lock(session->sent_resp_mux);

		/*
		 * This would be the next unreplied/expected request...
		 */
		next = msg_id + 1;

		count = session->sent_resp;
		while (count) {
			sent = count->data;
			count = count->next;

			if (next == sent->msg_id && !sent->packet) {
				next++;
				continue;
			}

			/*
			 * If we reach here than we came, either to
			 * unresponded request, or request that we
			 * didn't receive yet. So, break out of the
			 * loop.
			 */
			break;
		}

		if (msg_id == session->peer_msgid_lo) {
			LOG_DEBUG("Updating peer_msgid_lo to %u", next);
			session->peer_msgid_lo = next;
		}

		LOG_DEBUG("Sent response with msgid=%u", msg_id);
		g_mutex_unlock(session->sent_resp_mux);
	}

	/*
	 * If this is request and timeout values are not set then
	 * there is no further processing necessary for this
	 * packet.
	 */
	if (!response && (!session->cr->response_retries ||
			!session->cr->response_timeout))
		goto out;

	sent = sent_msg_new();

	sent->daddr = netaddr_dup(daddr);
	sent->ns = network_socket_dup_ro(ns);
	sent->packet = data;
	sent->packet_size = len;
	sent->response = response;
	sent->msg_id = msg_id;
	sent->session = session;

	/*
	 * Increase session reference counter
	 */
	session_ref(session);

	if (response) {
		LOG_DEBUG("Putting packet into sent response queue");

		g_mutex_lock(session->sent_resp_mux);

		session->sent_resp = g_slist_insert_sorted(session->sent_resp,
							sent, sent_cmp_msgid);

		LOG_DEBUG("Sent response with msgid=%u", msg_id);

		g_mutex_unlock(session->sent_resp_mux);

	} else {

		LOG_DEBUG("Putting packet into sent request queue");

		g_mutex_lock(session->sent_req_mux);

		session->sent_req = g_slist_insert_sorted(session->sent_req, 
							sent, sent_cmp_msgid);

		LOG_DEBUG("Sent request with msgid=%u", msg_id);

		g_mutex_unlock(session->sent_req_mux);

		sent->retries = session->cr->response_retries;
		sent->tv.tv_sec = session->cr->response_timeout / 1000;
		sent->tv.tv_usec = (session->cr->response_timeout 
					% 1000) * 1000;
		sent->to = timeout_register_thread(&sent->tv, 0, sent,
					message_retransmit_request);
		sent_msg_ref(sent);
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Handler which waits on a network_queue, retrieves a message from a
 * network layer, allocates memory for a message structure and extracts
 * some basic data from a network layer. Finally, it pushes message
 * structure to upper layers for further processing.
 */
gpointer message_main_thread(gpointer data)
{
	struct network_msg *msg;
	struct message_data *message_data;
	struct message_msg *mmsg;
	char *p;
	struct ikev2_header *hdr;

	LOG_FUNC_START(1);

	message_data = data;

	while (1) {
		msg = (struct network_msg *)g_async_queue_pop(
						message_data->network_queue);

		if (GPOINTER_TO_UINT(msg) == 1) {
			LOG_DEBUG("Got terminate message");
			break;
		}

		mmsg = message_msg_alloc();

		mmsg->srcaddr = network_msg_get_srcaddr(msg);
		mmsg->dstaddr = network_msg_get_dstaddr(msg);

		mmsg->size = network_msg_get_data(msg, &(mmsg->buffer));

		p = (char *)(mmsg->buffer);

		/*
		 * If the first four octets of the message are zero then
		 * it must be UDP encapsulated IKEv2 message so skip
		 * the marker.
		 */
		if (*(guint32 *)p == 0) {
			p += 4;
			mmsg->size -= 4;
		}

		hdr = (struct ikev2_header *)p;

		mmsg->i_spi = hdr->i_spi;
		mmsg->r_spi = hdr->r_spi;
		mmsg->exchg_type = hdr->exchg_type;
		mmsg->msg_id = ntohl(hdr->msg_id);
		mmsg->initiator = hdr->flags & IKEV2_FLAG_INITIATOR
					? TRUE
					: FALSE;
		mmsg->response = hdr->flags & IKEV2_FLAG_RESPONSE
					? TRUE
					: FALSE;

		msg->ns = network_socket_dup_ro(msg->ns);

		network_msg_free(msg);

		g_async_queue_push (message_data->upper_queue, mmsg);
	}

	LOG_FUNC_END(1);

	return NULL;
}

/*******************************************************************************
 * SUBSYSTEM INITIALIZATION AND DEINITIALIZATION FUNCTIONS
 ******************************************************************************/

/**
 * Register a socket with a given parameters
 *
 * @param cl	Parameter info for a socket
 *
 * \return 0 if registration was successful or -1 in case of an error
 */
gint message_register_socket(struct config_listen *cl)
{
	gint retval = -1;
	gint family;
	struct network_socket *ns;
	gboolean free_addr = FALSE;

#ifdef NATT
	int option;
#endif /* NATT */

	if (!cl->addr) {
		free_addr = TRUE;
		cl->addr = netaddr_new();
		netaddr_set_family(cl->addr, AF_UNSPEC);
	}

	family = cl->afamily != AF_UNSPEC ?
			cl->afamily : netaddr_get_family(cl->addr);

	if (family == AF_INET || family == AF_UNSPEC) {

		netaddr_set_family(cl->addr, AF_INET);

		netaddr_set_port(cl->addr, cl->port);

		ns = network_socket_register_queue(
				netaddr_get_family(cl->addr),
				SOCK_DGRAM, 0, cl->addr,
				NETWORK_F_DSTADDR,
				message_data->network_queue);

		if (!ns) {
			message_unload();
			goto out;
		}

		message_data->listen_socks = g_slist_append(
				message_data->listen_socks, ns);

		message_data->send_sock = ns;

#ifdef NATT
		if (cl->natt_port) {

			netaddr_set_port(cl->addr,
					cl->natt_port);

			ns = network_socket_register_queue(
				netaddr_get_family(cl->addr),
				SOCK_DGRAM, 0,
				cl->addr,
				NETWORK_F_DSTADDR,
				message_data->network_queue);

			if (!ns) {
				message_unload();
				goto out;
			}

			message_data->listen_socks =
				g_slist_append(
					message_data->listen_socks,
					ns);

			option = UDP_ENCAP_ESPINUDP;

			if (setsockopt(network_socket_get_sockfd(ns),
					SOL_UDP, UDP_ENCAP,
					&option, sizeof(option)) < 0) {
				LOG_PERROR(LOG_PRIORITY_ERROR);
				message_unload();
				goto out;
			}

			message_data->natt_send_sock = ns;
		}
#endif /* NATT */
	}

	if (family == AF_INET6 || family == AF_UNSPEC) {

		netaddr_set_family(cl->addr, AF_INET6);
		netaddr_set_port(cl->addr, cl->port);

		ns = network_socket_register_queue(
				netaddr_get_family(cl->addr),
				SOCK_DGRAM, 0, cl->addr,
				NETWORK_F_DSTADDR | NETWORK_F_IPV6ONLY,
				message_data->network_queue);

		message_data->listen_socks = g_slist_append(
				message_data->listen_socks, ns);
		message_data->send_sock6 = ns;
	}

	retval = 0;

out:
	if (free_addr) {
		netaddr_free(cl->addr);
		cl->addr = NULL;
	}

	return retval;
}

void message_unload(void)
{
	LOG_FUNC_START(1);

	if (message_data) {

		message_data->shutdown = TRUE;

		while (message_data->listen_socks) {
			network_socket_unregister(
					message_data->listen_socks->data);
			message_data->listen_socks = g_slist_remove(
					message_data->listen_socks,
					message_data->listen_socks->data);
		}

		if (message_data->secret_to)
			timeout_cancel(message_data->secret_to);

		/*
		 * Terminate and do join with main thread
		 */
		if (message_data->main_thread) {
			if (message_data->network_queue)
				g_async_queue_push(message_data->network_queue,
							GUINT_TO_POINTER(1));

			if (message_data->main_thread)
				g_thread_join(message_data->main_thread);
		}

		if (message_data->network_queue)
			g_async_queue_unref(message_data->network_queue);

		if (message_data->secret)
			g_free(message_data->secret);

		if (message_data->old_secret)
			g_free(message_data->old_secret);

		g_free(message_data);
		message_data = NULL;
	}

	LOG_FUNC_END(1);
}

gint message_init(GMainContext *context, GSList *listen_data,
		GAsyncQueue *upper_queue)
{
	struct config_listen *cl;
	gint retval = -1;
	GTimeVal tv;

	LOG_FUNC_START(1);

	message_data = g_malloc0(sizeof(struct message_data));

	message_data->network_queue = g_async_queue_new();

	/*
	 * In the following loop, while adding socket to a list of
	 * listening sockets, we assume that this is called in single
	 * thread so that no locking is necessary!
	 */
	while (listen_data) {
		cl = listen_data->data;
		listen_data = listen_data->next;

		if (message_register_socket(cl) < 0) {
			message_unload();
			goto out;
		}
	}

	g_async_queue_ref(upper_queue);
	message_data->upper_queue = upper_queue;

	/*
	 * Prepare data and for thread that generates secret at periodic
	 * intervals...
	 */
	message_data->secret = g_malloc(MESSAGE_SECRET_LEN);

	message_data->old_secret = g_malloc(MESSAGE_SECRET_LEN);

	if (!rand_bytes((guchar *)message_data->secret, MESSAGE_SECRET_LEN))
		goto out;

	if (!rand_bytes((guchar *)message_data->old_secret, MESSAGE_SECRET_LEN))
		goto out;

	/*
	 * Thread that processes events from network
	 */
	message_data->main_thread = g_thread_create(message_main_thread,
						message_data, TRUE, NULL);

	/*
	 * Register timeouts for generating secrets...
	 */
	tv.tv_sec = MESSAGE_SECRET_LIFETIME;
	tv.tv_usec = 0;
	message_data->secret_to = timeout_register_thread(&tv, TO_RECURRING,
					NULL, message_regenerate_secret_thread);

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}
