/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing timeout module
 *
 * The difference between this and timeout_test2 is that second timeout
 * event, with a smaller interval, is recurring and only prints something
 * on the screen. The first timeout teminates execution of a test.
 */

#define __TIMEOUT_TEST_C

#define LOGGERNAME	"timeout"

#include <stdio.h>
#include <glib.h>

#include "timeout.h"

gpointer wait_thread1(gpointer _queue)
{
	GTimeVal t1, t2, t;
	GAsyncQueue *queue = _queue;
	GMainLoop *ml;

	/*
	 * Wait for a message...
	 */
	printf ("Thread 1 waiting on a queue\n");
	g_get_current_time(&t1);
	ml = g_async_queue_pop(queue);
	g_get_current_time(&t2);

	t.tv_usec = t2.tv_usec - t1.tv_usec;
	t.tv_sec = t2.tv_sec - t1.tv_sec;
	if (t.tv_usec < 0) {
		t.tv_usec += 1000000;
		t.tv_sec--;
	}

	printf ("Thread 1 waited for %u secs %u usecs\n",
			(guint)t.tv_sec, (guint)t.tv_usec);

	/*
	 * Just terminate main loop...
	 */
	printf ("Terminating main loop\n");
	g_main_loop_quit(ml);

	return NULL;
}

gpointer wait_thread2(gpointer _queue)
{
	GTimeVal t1, t2, t;
	GAsyncQueue *queue = _queue;
	GMainLoop *ml;

	while(1) {

		/*
		 * Wait for a message...
		 */
		printf ("Thread 2 waiting on a queue\n");
		g_get_current_time(&t1);
		ml = g_async_queue_pop(queue);
		g_get_current_time(&t2);

		t.tv_usec = t2.tv_usec - t1.tv_usec;
		t.tv_sec = t2.tv_sec - t1.tv_sec;
		if (t.tv_usec < 0) {
			t.tv_usec += 1000000;
			t.tv_sec--;
		}

		printf ("Waited for %u secs %u usecs\n",
				(guint)t.tv_sec, (guint)t.tv_usec);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	GMainLoop *ml;
	GTimeVal mainrun;
	GAsyncQueue *queue1, *queue2;
	GThread *wt;
	timeout_t *t1, *t2;

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to timeout module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Queue on which we'll wait message that will terminate test
	 */
	queue1 = g_async_queue_new();

	/*
	 * Queue on which we'll wait message about 3 second interval change
	 */
	queue2 = g_async_queue_new();

	/*
	 * Start thread that will wait for a quit message
	 */
	printf ("Creating thread 1\n");
	wt = g_thread_create(wait_thread1, queue1, FALSE, NULL);

	/*
	 * Start thread that will wait for interval couting
	 */
	printf ("Creating thread 2\n");
	wt = g_thread_create(wait_thread2, queue2, FALSE, NULL);

	printf ("Initializing timeout module\n");
	timeout_init();

	/*
	 * This test will run for 10 seconds and then, we'll terminate
	 * main loop...
	 */
	printf ("Registering timeout in 10 seconds\n");
	mainrun.tv_sec = 10;
	mainrun.tv_usec = 0;
	t1 = timeout_register_queue(&mainrun, 0, ml, queue1);

	/*
	 * This test will run for 3 seconds and then, we'll terminate
	 * main loop...
	 */
	printf ("Registering recurring timeout in 3 seconds\n");
	mainrun.tv_sec = 3;
	mainrun.tv_usec = 0;
	t2 = timeout_register_queue(&mainrun, TO_RECURRING, ml, queue2);

	/*
	 * Start main loop...
	 */
	printf ("Starting main loop\n");
	g_main_loop_run(ml);

	/*
	 * Unload timeout module
	 */
	printf ("Unloading timeout module\n");
	timeout_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	printf ("Exiting\n");
	return 0;
}
