/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __TRANSFORMS_C

#define LOGGERNAME	"transforms"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "transforms.h"
#include "netlib.h"
#include "ikev2_consts.h"
#include "logging.h"
#include "config.h"
#include "crypto.h"

const char *ikev2_transform_types[] = {
	NULL,
	"IKEV2_TRANSFORM_ENCR",
	"IKEV2_TRANSFORM_PRF",
	"IKEV2_TRANSFORM_INTEGRITY",
	"IKEV2_TRANSFORM_DH_GROUP",
	"IKEV2_TRANSFORM_ESN"
};

const char *ikev2_encr_types[] = {
	"RESERVED",
	"ENCR_DES_IV64",
	"ENCR_DES",
	"ENCR_3DES",
	"ENCR_RC5",
	"ENCR_IDEA",
	"ENCR_CAST",
	"ENCR_BLOWFISH",
	"ENCR_3IDEA",
	"ENCR_DES_IV32",
	"RESERVED",
	"ENCR_NULL",
	"ENCR_AES_CBC",
	"ENCR_AES_CTR"
};

const char *ikev2_prf_types[] = {
	"RESERVED",
	"PRF_HMAC_MD5",
	"PRF_HMAC_SHA1",
	"PRF_HMAC_TIGER",
	"PRF_AES128_CBC"
};

const char *ikev2_auth_types[] = {
	"NONE",
	"AUTH_HMAC_MD5_96",
	"AUTH_HMAC_SHA1_96",
	"AUTH_DES_MAC",
	"AUTH_KPDK_MD5",
	"AUTH_AES_XCBC_96"
};

const char *ikev2_dh_groups[] = {
	"NONE",
	"MODP768",
	"MODP1024",
	"INVALID",
	"INVALID",
	"MODP1536",
	"INVALID",
	"INVALID",
	"INVALID",
	"INVALID",
	"INVALID",
	"INVALID",
	"INVALID",
	"INVALID",
	"MODP2048",
	"MODP3072",
	"MODP4096",
	"MODP6144",
	"MODP8192"
};

const transform_t transforms[] = {

	/*
	 * List of supported encryption functions
 	 */
	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_DES_IV64,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_DES_IV64",
		"des_iv64" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_DES,
		TRANSFORM_KERNEL | TRANSFORM_IKE,
		8, 8, 8, 8, 0,
		"ENCR_DES",
		"des",
		.f.encr = xcrypt_des },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_3DES,
		TRANSFORM_KERNEL | TRANSFORM_IKE,
		24, 24, 24, 8, 0,
		"ENCR_3DES",
		"3des" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_RC5,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_RC5",
		"rc5" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_IDEA,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_IDEA",
		"idea" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_CAST,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_CAST",
		"cast" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_BLOWFISH,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_BLOWFISH",
		"blowfish" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_3IDEA,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_3IDEA",
		"3idea" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_DES_IV32,
		TRANSFORM_KERNEL,
		0, 0, 0, 0, 0,
		"ENCR_DES_IV32",
		"des_iv32" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_NULL,
		TRANSFORM_KERNEL | TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"ENCR_NULL",
		"null" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CBC,
		TRANSFORM_KERNEL | TRANSFORM_IKE,
		16, 16, 16, 16, 0,
		"ENCR_AES128_CBC",
		"aes128_cbc" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CBC,
		TRANSFORM_IKE,
		24, 24, 24, 16, 0,
		"ENCR_AES192_CBC",
		"aes192_cbc" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CBC,
		TRANSFORM_IKE,
		32, 32, 32, 16, 0,
		"ENCR_AES256_CBC",
		"aes256_cbc" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CTR,
		TRANSFORM_KERNEL | TRANSFORM_IKE,
		16, 16, 16, 16, 0,
		"ENCR_AES128_CTR",
		"aes128_ctr" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CTR,
		TRANSFORM_IKE,
		24, 24, 24, 16, 0,
		"ENCR_AES192_CTR",
		"aes192_ctr" },

	{ IKEV2_TRANSFORM_ENCR,
		IKEV2_ENCR_AES_CTR,
		TRANSFORM_IKE,
		32, 32, 32, 16, 0,
		"ENCR_AES256_CTR",
		"aes256_ctr" },

	/*
	 * List of supported pseudo-random functions
	 */
	{ IKEV2_TRANSFORM_PRF,
		IKEV2_PRF_HMAC_MD5,
		TRANSFORM_IKE,
		16, 16, 16, 0, 16,
		"PRF_HMAC_MD5",
		"md5" },

	{ IKEV2_TRANSFORM_PRF,
		IKEV2_PRF_HMAC_SHA1,
		TRANSFORM_IKE,
		20, 20, 20, 0, 20,
		"PRF_HMAC_SHA1",
		"sha1" },

	{ IKEV2_TRANSFORM_PRF,
		IKEV2_PRF_AES128_CBC,
		TRANSFORM_IKE,
		16, 16, 16, 0, 16,
		"PRF_AES128_CBC",
		"aes128_cbc" },

	/*
	 * List of integrity (a.k.a. authentication functions)
	 *
	 * In the following data first number represents key
	 * length (in bytes) for a given algorithm, while last
	 * number is checksum/digest length produced by algorithm.
	 *
	 * Note that these are esentially same as the PRF
	 * functions but only 12 bytes is taken from the
	 * result.
	 */
	{ IKEV2_TRANSFORM_INTEGRITY,
		IKEV2_HMAC_MD5_96,
		TRANSFORM_IKE | TRANSFORM_KERNEL,
		16, 16, 16, 0, 16,
		"HMAC_MD5_96",
		"md5_96" },

	{ IKEV2_TRANSFORM_INTEGRITY,
		IKEV2_HMAC_SHA1_96,
		TRANSFORM_IKE | TRANSFORM_KERNEL,
		20, 20, 20, 0, 20,
		"HMAC_SHA1_96",
		"sha1_96" },

	{ IKEV2_TRANSFORM_INTEGRITY,
		IKEV2_AES_XCBC_96,
		TRANSFORM_IKE | TRANSFORM_KERNEL,
		16, 16, 16, 0, 20,
		"AES_XCBC_96",
		"aes_xcbc_96" },

	/*
	 * Supported DH groups. For data specific for each group see
	 * ikev2_consts.h
	 */
	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP768,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP768",
		"modp768" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP1024,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP1024",
		"modp1024" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP1536,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP1536",
		"modp1536" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP2048,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP2048",
		"modp2048" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP3072,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP3072",
		"modp3072" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP4096,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP4096",
		"modp4096" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP6144,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP6144",
		"modp6144" },

	{ IKEV2_TRANSFORM_DH_GROUP,
		IKEV2_DHG_MODP8192,
		TRANSFORM_IKE,
		0, 0, 0, 0, 0,
		"MODP8192",
		"modp8192" },

	{ 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL }
};

/**
 * Constructors/Destructors
 */

transform_t *transform_new(guint8 type, guint16 id)
{
	transform_t *transform = NULL;

	LOG_FUNC_START(3);

	transform = g_malloc0(sizeof(transform_t));

	transform->type = type;
	transform->id = id;

out:
	LOG_FUNC_END(3);

	return transform;
}

transform_t *transform_encr_new(guint16 id)
{
	return transform_new(IKEV2_TRANSFORM_ENCR, id);
}

transform_t *transform_prf_new(guint16 id)
{
	return transform_new(IKEV2_TRANSFORM_PRF, id);
}

transform_t *transform_auth_new(guint16 id)
{
	return transform_new(IKEV2_TRANSFORM_INTEGRITY, id);
}

transform_t *transform_dh_new(guint16 id)
{
	return transform_new(IKEV2_TRANSFORM_DH_GROUP, id);
}

transform_t *transform_esn_new(guint16 id)
{
	return transform_new(IKEV2_TRANSFORM_ESN, id);
}

transform_t *transform_dup(transform_t *transform)
{
	return g_memdup(transform, sizeof(transform_t));
}

void transform_free(transform_t *transform)
{
	LOG_FUNC_START(3);

	if (transform)
		g_free(transform);

	LOG_FUNC_END(3);
}

void transform_list_free(GSList *transforms)
{
	while (transforms) {
		transform_free(transforms->data);
		transforms = g_slist_remove(transforms, transforms->data);
	}
}

/**
 * Search list of transforms for a given transform name
 *
 * @param type	Type of transform
 * @param flag	Transform protocol flag (e.g. IKE or kernel)
 * @param name	Name of transform as specified in config file
 *
 * \return	Duplicated transform, or NULL if there is no transform under
 *		given name
 */
transform_t *transform_find_by_name(guint8 type, guint8 flag,
		const char *name)
{
	int i;

	for (i = 0; transforms[i].type; i++)
		if (transforms[i].type == type
				&& (transforms[i].flags & flag)
				&& !strcmp(name, transforms[i].conf_name))
			return g_memdup(&transforms[i], sizeof(transform_t));

	return NULL;
}

/**
 * Given a transform check if it's in a given list
 *
 * @param transform	Transform to search for
 * @param tl		List of transforms
 *
 * \return TRUE if the transform is in a list, FALSE otherwise
 */
gboolean transform_find(transform_t *transform, GSList *tl)
{
	while (tl) {

		if (transform_equal(transform, tl->data))
			return TRUE;
		tl = tl->next;
	}

	return FALSE;
}

/**
 * Getter/Setter methods
 */
guint8 transform_get_type(transform_t *transform)
{
	return transform->type;
}

void transform_set_type(transform_t *transform, guint8 type)
{
	g_assert(type && type <= IKEV2_TRANSFORM_MAX);

	transform->type = type;
}

guint16 transform_get_id(transform_t *transform)
{
	return transform->id;
}

void transform_set_id(transform_t *transform, guint16 id)
{
	transform->id = id;
}

guint16 transform_get_key_len(transform_t *transform)
{
	return transform->key_len;
}

void transform_set_key_len(transform_t *transform, guint16 key_len)
{
	transform->key_len = key_len;
}

guint16 transform_get_min_key_len(transform_t *transform)
{
	return transform->min_key_len;
}

void transform_set_min_key_len(transform_t *transform, guint16 min_key_len)
{
	transform->min_key_len = min_key_len;
}

guint16 transform_get_max_key_len(transform_t *transform)
{
	return transform->max_key_len;
}

void transform_set_max_key_len(transform_t *transform, guint16 max_key_len)
{
	transform->max_key_len = max_key_len;
}

guint16 transform_prf_get_key_len(transform_t *transform)
{
	guint8 i;

	g_assert(transform->type == IKEV2_TRANSFORM_PRF);

	i = 0;
	while (transforms[i].type) {
		if (transforms[i].type == IKEV2_TRANSFORM_PRF
				&& transforms[i].id == transform->id) {
			return transforms[i].key_len;
		}
		i++;
	}

	return transform->key_len;
}

guint16 transform_prf_get_digest_len(transform_t *transform)
{
	guint8 i;

	g_assert(transform->type == IKEV2_TRANSFORM_PRF);

	i = 0;
	while (transforms[i].type) {
		if (transforms[i].type == IKEV2_TRANSFORM_PRF
				&& transforms[i].id == transform->id) {
			return transforms[i].olen;
		}
		i++;
	}

	return 0;
}

/**
 * Other methods
 */

/**
 * Check if two transforms are equal!
 *
 * TODO: What to do with key sizes?
 */
gboolean transform_equal(transform_t *t1, transform_t *t2)
{
	return ((t1->type == t2->type)
		&& (t1->id == t2->id)
		&& (t1->key_len == 0 || t2->key_len == 0 || t1->key_len == t2->key_len));
}

/**
 * This function lists all available transforms to stdout
 */
void transform_list_all(void)
{
	int i;

	for (i = 0; transforms[i].type; i++) {
		switch(transforms[i].type) {
		case IKEV2_TRANSFORM_ENCR:
			printf ("encrypt");
			break;
		case IKEV2_TRANSFORM_PRF:
			printf ("prf");
			break;
		case IKEV2_TRANSFORM_INTEGRITY:
			printf ("integ");
			break;
		case IKEV2_TRANSFORM_DH_GROUP:
			printf ("dhg");
			break;
		case IKEV2_TRANSFORM_ESN:
			printf ("esn");
			break;
		}
		printf ("\tname=%s\tconf_name=%s\tkeylen=%u\tminkeylen=%u "
			"maxkeylen=%u\tivlen=%u\tolen=%u\n",
			transforms[i].name, transforms[i].conf_name,
			transforms[i].key_len, transforms[i].min_key_len,
			transforms[i].max_key_len, transforms[i].ivlen,
			transforms[i].olen);
	}

	return;
}

/**
 * DEBUG functions
 */
void transform_dump(char *loggername, transform_t *transform)
{
	switch (transform->type) {
	case IKEV2_TRANSFORM_ENCR:
		if (transform->id <= IKEV2_ENCR_MAX)
			ikev2_log (loggername, LOG_PRIORITY_DEBUG,
				"%s (key_len=%d)",
				ikev2_encr_types[transform->id],
				transform->key_len);
		else
			ikev2_log (loggername, LOG_PRIORITY_DEBUG,
				"UNKNOWN ENCRYPTION TRANSFORM(%d)",
					transform->id);
		break;

	case IKEV2_TRANSFORM_PRF:
		if (transform->id <= IKEV2_PRF_MAX)
			ikev2_log (loggername, LOG_PRIORITY_DEBUG, "%s",
					ikev2_prf_types[transform->id]);
		else
			ikev2_log (loggername, LOG_PRIORITY_DEBUG,
				"UNKNOWN PRF TRANSFORM(%d)",
					transform->id);
		break;

	case IKEV2_TRANSFORM_INTEGRITY:
		if (transform->id <= IKEV2_INTEG_MAX)
			ikev2_log (loggername, LOG_PRIORITY_DEBUG, "%s",
					ikev2_auth_types[transform->id]);
		else
			ikev2_log (loggername, LOG_PRIORITY_DEBUG,
				"UNKNOWN AUTH TRANSFORM(%d)",
					transform->id);
		break;

	case IKEV2_TRANSFORM_DH_GROUP:
		if (transform->id <= IKEV2_DHG_MAX)
			ikev2_log (loggername, LOG_PRIORITY_DEBUG, "%s",
					ikev2_dh_groups[transform->id]);
		else
			ikev2_log (loggername, LOG_PRIORITY_DEBUG,
				"UNKNOWN DH GROUP TRANSFORM(%d)",
					transform->id);
		break;

	case IKEV2_TRANSFORM_ESN:
		ikev2_log (loggername, LOG_PRIORITY_DEBUG, "ESN");
		break;
	}
}

/**
 * Dump a list of transforms
 */
void transform_list_dump(char *loggername, GSList *transforms)
{
	while (transforms) {
		transform_dump(loggername, transforms->data);
		transforms = transforms->next;
	}
}
