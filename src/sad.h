/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/ 

#ifndef __SAD_H
#define __SAD_H

/*
 * States transitioned during creation of a new CHILD SA
 */
#define SA_STATE_INIT		0

#define SA_STATE_CREATE		1
#define SA_STATE_CREATE_WAIT	2

#define SA_STATE_REKEY		3
#define SA_STATE_REKEY_WAIT	4

#define SA_STATE_MATURE		200

#define SA_STATE_DEAD		230

#define SA_STATE_REMOVE		255

/**
 * State data for SAD in ikev2
 */
struct sad_data {
	/**
	 * Queue for upper modules, i.e. state machines
	 */
	GAsyncQueue *sm_queue;

	/**
	 * Queue for receiving responses and requests from kernel
	 */
	GAsyncQueue *queue;

	/**
	 * Operation structure, with pointer to specific functions
	 * to perform different tasks on SAD.
	 */
	struct sad_op *sad_op;

	/**
	 * Thread that waits on a queue and processes all the
	 * received messages 
	 */
	GThread *thread;
};

/**
 * Structure that holds all the information relevant to security association
 * element in SAD
 *
 * This structure is used for both directions, i.e. two SAs are inserted into
 * kernel based on one single structure.
 *
 * TODO: Synchronize with RFC4301
 */
struct sad_item {
	/*
	 * Mutex for critical changes and tests in a structure
	 */
	GMutex *mutex;

	/*
	 * TRUE if this CSA is initiator. This variable changes the way
	 * sk_e? and sk_a? fields are treated.
	 */
	gboolean initiator;

	/**
	 * State in which this CHILD SA is...
	 */
	guint8 state;

	/**
	 * If we sent request message to a peer, then the peer has to respond
	 * with a message that has message_id as the one placed in the
	 * following field...
	 *
	 * If this field is zero, then no request has been sent...
	 */
	guint32 msg_id;

	/**
	 * SPI values for this CHILD SA
	 *
	 * spi is always value choosen by local node, while peer_spi is
	 * value chosen by peer...
	 */
	guint32 spi;
	guint32 peer_spi;
	guint8 spi_size;	/* This depends, if we are creating/rekeying
				 * CHILD SA in which case it's 4, or rekeying
				 * IKE SA, then it's 8.
				 */

	/**
	 * Traffic selectors for the SA
	 *
	 * Usually, here will be only single TS for local and remote
	 * peers. But, during TS negotiations we store here all the
	 * proposed TSs to the peer. If, by any chance, we negotiate
	 * more than one TSs, then multiple SA will be created, for
	 * each pair one.
	 */
	GSList *tsl;
	GSList *tsr;

	/**
	 * Endpoint addresses
	 */
	struct netaddr *src, *dst;

	/**
	 * Selected and applied proposals for this SA. If this is
	 * NULL then proposals are inherited from associated policy,
	 * but they are not applied.
	 */
	GSList *proposals;

	/**
	 * SA mode that is used, either tunnel (IPSEC_MODE_TUNNEL),
	 * or transport (IPSEC_MODE_TRANSPORT). If this field is
	 * zero, than mode is inherited from the associated policy.
	 */
	guint8 ipsec_mode;

	/**
	 * Transforms selected for this CHILD SA
	 */
	transform_t *encr;
	transform_t *auth;

	/**
	 * Shared keys for CHILD SA
	 *
	 * Unlike the specification that calls them i for initiator
	 * and r for remote, we call them l for local and r for
	 * remote.
	 */
	gchar *sk_el;
	gchar *sk_er;
	gchar *sk_al;
	gchar *sk_ar;

	/**
	 * When this CHILD SA has been transfered to REMOVE state
	 */
	guint32 remove_time;

	/**
	 * Policy item for the security association
	 */
	struct spd_item *spd;

	/**
	 * Data specific to PF_KEYv2 interface. This should be moved
	 * into private section of this structure shared by all
	 * implemented interfaces.
	 */

	/**
	 * If we sent some request to a kernel then this field is different
	 * from zero and holds a number of a request to which we expect
	 * response...
	 */
	guint32 seq;
	guint32 seq_in;

	/**
	 * Fields used for rekeyings of CHILD SA and/or IKE SA
	 */

	/**
	 * Nonce values
	 */
	BIGNUM *i_nonce;
	BIGNUM *r_nonce;

	/**
	 * New CHILD SA is kept here on initiator until rekeying
	 * is finished. For responder, this field is not necessary
	 * since there it's a one shot process.
	 */
	struct sad_item *nsi;

	/**
	 * Did we reached hard limit of SA?
	 */
	gboolean hard_limit;

	/**
	 * For rekeying SA or reauthenticating new IKE SA
	 */
	struct netaddr *rekey_dstaddr;

	/**
	 * DH group used during rekeying of IKE SA
	 */
	DH *dh;
	transform_t *prf_type;
	transform_t *dh_group;
};

/**
 * Structure used to register a protocol for SAD manipulation
 */
struct sad_op {
	/*
	 * Pointer to data specific to a subsystem used for communication
	 * with kernels SA database.
	 */
	void *priv_data;

	guint32 (*sad_getspi)(struct netaddr *, struct netaddr *,
			guint8, guint8, guint32, guint32);
	void (*sad_update)();
	int (*sad_add)(struct sad_item *, struct netaddr *, struct netaddr *,
			gboolean, gboolean);
	int (*sad_delete)(struct sad_item *, struct netaddr *,
			struct netaddr *);
	void (*sad_get)();
	void (*sad_acquire)();
	void (*sad_register)();
	void (*sad_expire)();
	void (*sad_flush)();
	void (*sad_dump)();

	/**
	 * Return read-only list of supported transforms by the kernel
	 */
	GSList *(*sad_transforms_get)();

	/*
	 * Function to unload subsystem
	 */
	void (*sad_shutdown)(struct sad_op *sad_op);
	void (*sad_unload)(struct sad_op *sad_op);
};

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
struct sad_item *sad_item_new();
struct sad_item *sad_item_clone(struct sad_item *);
void sad_item_free(struct sad_item *);
void sad_item_list_free(GSList *);

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/
void sad_item_set_msg_id(struct sad_item *, guint32);
guint32 sad_item_get_msg_id(struct sad_item *);
gboolean sad_item_get_initiator(struct sad_item *);
void sad_item_set_initiator(struct sad_item *, gboolean);
GSList *sad_item_get_tsl(struct sad_item *);
GSList *sad_item_get_tsr(struct sad_item *);
int sad_item_set_ts(struct sad_item *, GSList *, GSList *);
guint8 sad_item_get_spi_size(struct sad_item *);
void sad_item_set_spi_size(struct sad_item *, guint8);
guint64 sad_item_get_spi(struct sad_item *);
void sad_item_set_spi(struct sad_item *, guint64);
guint64 sad_item_get_peer_spi(struct sad_item *);
void sad_item_set_peer_spi(struct sad_item *, guint64);
guint8 sad_item_get_ipsec_mode(struct sad_item *);
void sad_item_set_ipsec_mode(struct sad_item *, guint8);
guint8 sad_item_get_protocol(struct sad_item *);
void sad_item_set_protocol(struct sad_item *, guint8);
guint8 sad_item_get_mode(struct sad_item *);
void sad_item_set_mode(struct sad_item *, guint8);
guint32 sad_item_get_reqid(struct sad_item *);
guint32 sad_item_get_reqid_in(struct sad_item *);
guint32 sad_item_get_reqid_fwd(struct sad_item *);
guint32 sad_item_get_pid(struct sad_item *);
guint32 sad_item_get_pid_in(struct sad_item *);
GSList *sad_item_get_proposals(struct sad_item *);
void sad_item_set_proposals(struct sad_item *, GSList *);
void sad_item_set_proposal(struct sad_item *, proposal_t *);
GSList *sad_item_get_encr_algs(struct sad_item *);
transform_t *sad_item_get_encr(struct sad_item *);
gchar *sad_item_get_sk_el(struct sad_item *);
gchar *sad_item_get_sk_er(struct sad_item *);
gchar *sad_item_get_sk_al(struct sad_item *);
gchar *sad_item_get_sk_ar(struct sad_item *);
GSList *sad_item_get_auth_algs(struct sad_item *);
transform_t *sad_item_get_auth(struct sad_item *);
guint32 sad_item_get_seq(struct sad_item *);
void sad_item_set_seq(struct sad_item *, guint32);
guint32 sad_item_get_seq_in(struct sad_item *);
void sad_item_set_seq_in(struct sad_item *, guint32);
guint8 sad_item_get_state(struct sad_item *);
void sad_item_set_state(struct sad_item *, guint8);
struct netaddr *sad_item_get_rekey_dstaddr(struct sad_item *);
void sad_item_set_rekey_dstaddr(struct sad_item *, struct netaddr *);
guint32 sad_item_get_remove_time(struct sad_item *);
void sad_item_set_remove_time(struct sad_item *, guint32);
BIGNUM *sad_item_get_i_nonce(struct sad_item *);
BIGNUM *sad_item_get_r_nonce(struct sad_item *);
DH *sad_item_get_dh(struct sad_item *);
transform_t *sad_item_get_dh_group(struct sad_item *);
transform_t *sad_item_get_dh_group(struct sad_item *si);
struct spd_item *sad_item_get_spd(struct sad_item *);
void sad_item_set_spd(struct sad_item *si, struct spd_item *);
void sad_item_remove_transient(struct sad_item *);

int sad_item_set_tsl(struct sad_item *, struct ts *);
int sad_item_set_tsr(struct sad_item *, struct ts *);

void sad_item_clear_tsl(struct sad_item *);
void sad_item_clear_tsr(struct sad_item *);

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SAD IN KERNEL
 ******************************************************************************/
int sad_getspi(struct sad_item *, struct netaddr *, struct netaddr *);
void sad_update();
int sad_add(struct sad_item *, struct netaddr *, struct netaddr *, gboolean,
		gboolean);
void sad_delete(struct sad_item *, struct netaddr *, struct netaddr *);
void sad_get();
void sad_acquire();
void sad_register();
void sad_expire();
void sad_flush();
void sad_dump();

/*******************************************************************************
 * MISC FUNCTIONS
 ******************************************************************************/
void sad_item_lock(struct sad_item *);
gboolean sad_item_try_lock(struct sad_item *);
void sad_item_unlock(struct sad_item *);
gboolean sad_item_wait_response(struct sad_item *);
int sad_item_compute_keys(struct sad_item *, char *,
		BIGNUM *, BIGNUM *, transform_t *, gboolean);

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/
void sad_item_dump(struct sad_item *);
void sad_item_list_dump(GSList *);

/*******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/
void sad_set_shutdown_state(void);
void sad_unload();
int sad_init(GMainContext *, GAsyncQueue *);

#endif /* __SPD_H */
