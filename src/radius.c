/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __RADIUS_C

#ifdef RADIUS_CLIENT

#define LOGGERNAME	"radius"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <glib.h>
#include <openssl/md5.h>
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <linux/ipsec.h>

#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/rand.h>


#include "ikev2.h"
#include "ikev2_consts.h"
#include "transforms.h"
#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "auth.h"
#include "cert.h"

#include "crypto.h"
#include "proposals.h"
#include "spd.h"
#include "supplicant.h"
#include "timeout.h"
#include "session.h"

#include "radius.h"
#include "config.h"
#include "aaa_wrapper.h"

/**
 * Data global to RADIUS subsystem
 */
struct radius_data *radius_data = NULL;

/***************************************************************************
 * RADIUS CONFIGURATION STRUCTURES
 ***************************************************************************/

/**
 * Allocate new radius_server structure
 */
struct radius_server *radius_server_new()
{
	struct radius_server *rs;

	rs = g_malloc0(sizeof(struct radius_server));

	return rs;
}
/**
 * Dump value of radius_server structure
 */
void radius_server_dump(struct radius_server *rs)
{
	char ip[64];

	LOG_DEBUG("IP address: %s", netaddr_ip2str(rs->addr, ip, 64));
}

/**
 * Allocate new radius_config structure
 *
 * \return Pointer to a new structure
 */
struct radius_config *radius_config_new()
{
	struct radius_config *cr;

	cr = g_malloc0(sizeof(struct radius_config));
	cr->refcnt = 1;

	return cr;
}

/**
 * Free radius_config_structure
 *
 * @param cr	Configuration structure to free
 */
void radius_config_free(struct radius_config *cr)
{
	if (cr && g_atomic_int_dec_and_test(&(cr->refcnt))) {
		g_free(cr);
	}
}

/**
 * Set ID for configuration structure
 *
 * @param cr	Radius configuration structure
 * @param id	ID to set
 *
 * After setting ID callee should not touch, free, or otherwise manipulate
 * ID parameter.
 */
void radius_config_set_id(struct radius_config *cr, gchar *id)
{
	cr->id = id;
}

/**
 * Add authentication server to radius configuration
 */
void radius_config_add_auth_server(struct radius_config *cr,
		struct radius_server *rs)
{
	cr->auth_servers = g_slist_append(cr->auth_servers, rs);
}

/**
 * Add accounting server to radius configuration
 */
void radius_config_add_acct_server(struct radius_config *cr,
		struct radius_server *rs)
{
	cr->acct_servers = g_slist_append(cr->acct_servers, rs);
}

/*
 * Search for a RADIUS configuration structure with a given ID
 *
 * @param list          List of RADIUS configuration structures
 * @param id            ID
 *
 * \return Pointer to a RADIUS configuration structure, or NULL if none
 * was found.
 *
 * This function searches by strict string compare between two IDs.
 */
struct radius_config *radius_config_find_by_id (GSList *list, char *id)
{
	struct radius_config *cr;

	LOG_FUNC_START(1);

	LOG_DEBUG("Searching RADIUS configuration with ID %s", id);

	cr = NULL;

	while (list) {
		LOG_TRACE("Checking RADIUS configuration with ID %s",
				((struct radius_config *)list->data)->id);

		if (!strcmp(((struct radius_config *)list->data)->id, id)) {
			cr = list->data;
			break;
		}
	
		list = list->next;
	}

	LOG_FUNC_END(1);

	return cr;
}

/**
 * Dump contents of radius structure
 *
 * @param cr	Radius configuration structure
 */
void radius_config_dump(struct radius_config *cr)
{
	GSList *iterator;

	LOG_DEBUG("Dumping radius_config_structure at line %u",
			cr->cfg_line);

	if (cr->id)
		LOG_DEBUG("id=%s", cr->id);

	LOG_DEBUG("nas_port = %u", cr->nas_port);
	LOG_DEBUG("timeout = %u", cr->timeout);
	LOG_DEBUG("retries = %u", cr->retries);
	LOG_DEBUG("interim_interval = %u", cr->interim_interval);

	LOG_DEBUG("Authentication servers");
	for (iterator = cr->auth_servers; iterator;
			iterator = iterator->next)
		radius_server_dump(iterator->data);

	LOG_DEBUG("Accouting servers");
	for (iterator = cr->acct_servers; iterator;
			iterator = iterator->next)
		radius_server_dump(iterator->data);

}

/***************************************************************************
 *                                  EAP                                    *
 ***************************************************************************/

/**
 * Get Code field
 *
 * @param packet	pointer to EAP packet
 *
 * \return	EAP packet Code or -1 if packet == NULL
 */
guint8 radius_eap_get_code (struct radius_eap_hdr *packet)
{
	return (packet != NULL) ? packet->code : -1;
}

/**
 * Get Length field
 *
 * @param packet	pointer to EAP packet
 *
 * \return	EAP packet Length or -1 if packet == NULL
 */
guint16 radius_eap_get_length (struct radius_eap_hdr *packet)
{
	return (packet != NULL) ?  g_ntohs(packet->length) : 0;
}

/**
 * Print content of EAP packet
 *
 * @param packet	Pointer to EAP packet
 * @param prefix	text to insert before data
 *
 */
void radius_eap_dump_packet (void *packet, gchar *prefix)
{
	struct radius_eap_hdr *phdr;
	struct radius_eap_data *ptype;
	int i;
	guint16 pack_len;
	gchar *tvalue, tval[128];
	gchar *eap_code[] = {"Request", "Response", "Success", "Failure"};
	gchar *eap_type[] = {"Identity", "Notification", "Nak",
				"MD5-Challenge", "TLS"};

	LOG_FUNC_START(1);

	if (packet == NULL) {
		LOG_BUG("NULL passed to dump_eap_packet");
		return;
	}

	phdr = (struct radius_eap_hdr*) packet;

	pack_len = g_ntohs(phdr->length);

	LOG_DEBUG("%sEAP packet:>>>", prefix);
	LOG_DEBUG("%s\tCode=%u (%s)", prefix, phdr->code, eap_code[phdr->code-1]);
	LOG_DEBUG("%s\tIdentifier=%u", prefix, phdr->identifier);
	LOG_DEBUG("%s\tLength=%d", prefix, pack_len);

	if (phdr->code == 1 || phdr->code == 2) { /* have Data field */
		ptype = (struct radius_eap_data*)(packet +
				sizeof(struct radius_eap_hdr));

		LOG_DEBUG("%s", prefix);

		if (ptype->type < 5)
			LOG_DEBUG("%s\tType=%u (%s)", prefix, ptype->type,
				eap_type[ptype->type-1]);
		else if (ptype->type == EAP_TYPE_TLS)
			LOG_DEBUG("%s\tType=%u (%s)", prefix, ptype->type,
				eap_type[4]);
		else
			LOG_DEBUG("%s\tType=%u", prefix, ptype->type);
	
		if (pack_len > sizeof(struct radius_eap_hdr) + sizeof(guint8)) {
			switch(ptype->type) {
			case EAP_TYPE_IDENTITY:
				for(i = 0; i < pack_len - 5; i++)
					tval[i] = *(i + ptype->type_data);
				tval[i] = 0;
				LOG_DEBUG ("%s\tType-Data=%s", prefix, tval);
				break;
			
			default:
				tvalue = radius_hextostring (ptype->type_data,
								pack_len - 5);
				LOG_DEBUG("%s\tType-Data=0x%s", prefix, tvalue);
				g_free(tvalue);
			
				break;
			}
		}
	}

	LOG_DEBUG("%sEAP packet:<<<", prefix);

	LOG_FUNC_END(1);
}


/***************************************************************************
 *                                  RADIUS                                 *
 ***************************************************************************/

#define NEXTATTR(A) ((void *)A) + (A)->length

/**
 * Create ready to send RADIUS packet containing EAP packet
 *
 * @param radius_challenge	Challenge packet received from radius. NULL if
 *				there was no packet before this request...
 * @param eap_packet		Pointer to EAP packet to encapsulate
 * @param username		Value for the User-Name attribute
 * @param username_len		Length of username param
 * @param identifier		identifier for radius packet
 * @param rs			RADIUS server & client data (ikev2 responder)
 *
 * \return	pointer to RADIUS packet
 *
 */
void *radius_create_access_request_packet (void *radius_challenge,
		void *eap_packet, gchar *username, guint username_len,
		guint8 identifier, struct radius_state *rs)
{
	int eap_curr_len, eap_len, rad_len = 0;
	guint32 nas_port;
	struct radius_hdr *packet = NULL;
	struct radius_attr_hdr *attr, *attr_state;
	void *eap_pos_start;
	void *digest;
	guint8 *tmp;
	char *nas_id, *key;
	int nas_id_len, key_len;

	LOG_FUNC_START(1);

	eap_len = radius_eap_get_length(eap_packet);

	/*
	 * Calculate RADIUS packet size ----->
	 */

	/*
	 * RADIUS packet header
	 */
	rad_len = sizeof(struct radius_hdr);

	/*
	 * User-name or Calling-Station-Id length (by RFC)
	 *
	 * Leonardo: User-name should be used, freeradius rejects if
	 * User-name is not the same as in EAP packet
	 *
	 * This needs to be further investigated, but pretty sure its like this
	 */

	/*
	 * Get User-name from session - passed to this function trough
	 * parameter 'username'
	 *
	 * Note: User-Name attribute DON'T include terminating NULL character
	 */
	if (!username) {
		/*
		 * User-Name must be sent, aborting
		 */
		goto out_err;
	}
	rad_len += username_len + 2;
	
	/*
	 * NAS-Identifier - from radius_config structure
	 * (needed)
	 * NAS-Identifier or NAS-IP-Address MUST be present by rfc (so far we
	 * neglected this other option, but ... see comments in radius.h)
	 */
	if ((nas_id = radius_get_client_nas_id(rs)) != NULL) {
		nas_id_len = strlen(nas_id);
		rad_len += nas_id_len + 2;
	} else {
		/*
		 * This is needed, RADIUS will reject communication if this is
		 * not provided. So breaking if this is the case.
		 */
		LOG_ERROR("NAS-Id not defined, aborting creation of RADIUS",
		 	" packet");
		goto out_err;
	}

	/*
	 * NAS-Port: size=6 - get it from client
	 * (not needed, but if present)
	 */
	if ((nas_port = radius_get_client_nas_port(rs)))
		rad_len += sizeof(nas_port) + 2;

	/*
	 * EAP-Message attribute(s)
	 */
	if (eap_len == 0) {
		/*
		 * generate EAP-Start packet attribute
		 */
		rad_len += 2;
	} else {
		/*
		 * if EAP packet greater than RADIUS_MAX_ATTR_LEN, need to
		 * create more attributes to put EAP packet in
		 * size = eap size + attribute headers
		 */
		rad_len += eap_len + 2 * ((eap_len + RADIUS_MAX_ATTR_LEN - 1) /
							RADIUS_MAX_ATTR_LEN);
	}

	/*
	 * Message-Authenticator: length = 18
	 */
	rad_len += 18;

	/*
	 * Check if there is State atribute in challenge, if it is then we
	 * have to replicate it in a new request...
	 */
	if (radius_challenge) {
		attr_state = radius_get_attribute(
				(struct radius_hdr *)radius_challenge,
				RADIUS_ATTR_STATE);
		if (attr_state)
			rad_len += attr_state->length;
	} else
		attr_state = NULL;

	/*
	 * Proxy-State attribute (for matching replys)
	 */
	rad_len += 2 + 2 * sizeof(void *) + 4;

	if (rad_len < RADIUS_MIN_PACKET_LEN || rad_len > RADIUS_MAX_PACKET_LEN)
	{
		LOG_ERROR("Radius packet will be too big");
		goto out;
	}

	/*
	 * RADIUS packet size calculated <-----
	 *
	 * Next, create RADIUS packet
	 */
	packet = g_malloc0(rad_len);

	/*
	 * Construct RADIUS packet header
	 */
	packet->code = RADIUS_CODE_ACCESS_REQUEST;
	packet->identifier = identifier;
	packet->length = g_htons(rad_len);
	radius_get_authenticator(packet->authenticator);

	/*
	 * RADIUS Attributes
	 */
	attr = (struct radius_attr_hdr *) (packet + 1);

	/*
	 * User-Name
	 */
	if (username != NULL) {
		attr->type = RADIUS_ATTR_USER_NAME;
		attr->length = username_len + 2;

		memcpy(attr + 1, username, username_len);

		attr = NEXTATTR(attr);
	}

	/*
	 * NAS-Identifier
	 */
	if (nas_id != NULL) {
		attr->type = RADIUS_ATTR_NAS_IDENTIFIER;
		attr->length = nas_id_len + 2;

		memcpy(attr + 1, nas_id, nas_id_len);

		attr = NEXTATTR(attr);
	}

	/*
	 * NAS-Port
	 */
	if (nas_port) {
		attr->type = RADIUS_ATTR_NAS_PORT;
		attr->length = 2 + sizeof(nas_port);
	
		/*
		 * tricky, value of this attribute is not (can't predict) aligned
		 * on 32-bit address
		 */
		tmp = (guint8 *)(attr + 1);
		tmp[0] = (guint8) ((nas_port & 0xff000000) >> 24);
		tmp[1] = (guint8) ((nas_port & 0x00ff0000) >> 16);
		tmp[2] = (guint8) ((nas_port & 0x0000ff00) >> 8);
		tmp[3] = (guint8) ((nas_port & 0x000000ff) >> 0);

		attr = NEXTATTR(attr);
	}


	/*
	 * EAP-Message attribute(s)
	 */
	if (eap_packet == NULL) {
		/*
		 * Create EAP-Start packet attribute (no data)
		 */
		attr->type = RADIUS_ATTR_EAP_MESSAGE;
		attr->length = 2;

		attr = NEXTATTR(attr);
	} else {
		/*
		 * if EAP packet is greater than 253 - create more attributes
		 */
		eap_pos_start = eap_packet;
			
		while(eap_len > 0) {
			eap_curr_len = (eap_len > RADIUS_MAX_ATTR_LEN) ?
				RADIUS_MAX_ATTR_LEN : eap_len;
			

			attr->type = RADIUS_ATTR_EAP_MESSAGE;
			attr->length = eap_curr_len + 2;

			memcpy(attr + 1, eap_pos_start, eap_curr_len);

			attr = NEXTATTR(attr);

			eap_pos_start += RADIUS_MAX_ATTR_LEN;
			eap_len -= RADIUS_MAX_ATTR_LEN;
		}
	}

	/*
	 * Add State attribute if it's defined...
	 */

	if (radius_challenge && attr_state) {
		memcpy(attr, attr_state, attr_state->length);

		attr = NEXTATTR(attr);
	}

	/*
	 * Add Proxy-State attribute
	 */

	attr->type = 33;
	attr->length = 2 + 2 * sizeof(void *) + 4;
	memcpy(attr + 1, rs->proxy_state, 2 * sizeof(void *) + 4);

	attr = NEXTATTR(attr);

	/*
	 * Message-Authenticator
	 */
	attr->type = RADIUS_ATTR_MESSAGE_AUTHENTICATOR;
	attr->length = 18;

	key = radius_get_server_secret(rs);
	if (key == NULL) {
		LOG_ERROR("RADIUS server secret not defined?!");
		goto out_err;
	}
	key_len = strlen(key);

	digest = g_malloc0(MD5_DIGEST_SIZE);

	prf_hmac_md5((guint8 *) key, key_len, (guint8 *) packet,
			rad_len, (caddr_t)digest);

	memcpy(attr + 1, digest, MD5_DIGEST_SIZE);

	g_free(digest);

	goto out;

out_err:
	if (packet != NULL)
		g_free(packet);
	packet = NULL;

out:
	LOG_FUNC_END(1);

	return (gchar *) packet;
}

/**
 * Generate authenticator
 *
 * @param authenticator - pointer where to copy value
 */
void radius_get_authenticator (guint8 *authenticator)
{
	/*
	 * generating random number every time - if problems with this FIXME
	 */
	if (authenticator != NULL)
		if (rand_bytes(authenticator, 16)!=1)
			LOG_BUG("'rand_bytes' returned -1, "
					"authenticator not generated");
}

/**
 * Get a secret for currently active radius server
 *
 * @param rs	pointer to 'struct radius_servers' - all data for servers
 *
 * \return	Pointer to shared secret
 *		NULL in case of an error
 *
 * Callee _must_ treat received value read-only, and _must_ not free it.
 */
char *radius_get_server_secret(struct radius_state *rs)
{
	char *secret = NULL;

	if (rs && rs->auth_server)
		secret = rs->auth_server->shared_secret;

	if (!secret)
		LOG_WARNING("Shared secret is not set!");

	return secret;
}

/**
 * Get NAS-Identifier
 *
 * @param rs	Pointer to 'radius_state' structure
 *
 * \return	Pointer to NAS-Identifier
 *		NULL in case of an error
 *
 * Callee _must_ treat received value read-only, and _must_ not free it.
 */
char *radius_get_client_nas_id (struct radius_state *rs)
{
	char *nas_id = NULL;

	if (rs && rs->cr)
		nas_id = rs->cr->nas_identifier;

	if (!nas_id)
		LOG_WARNING("Cant get NAS-Identifier");

	return nas_id;
}

/**
 * Get NAS-port
 *
 * @param rs	Pointer to 'struct radius_servers' - all data for servers
 *
 * \return	NAS-Port
 *		0 in case of an error
 */
guint32 radius_get_client_nas_port(struct radius_state *rs)
{
	guint32 nas_port = 0;

	if (rs && rs->cr)
		nas_port = rs->cr->nas_port;
	else
		LOG_NOTICE("Can't get NAS-Port");

	return nas_port;
}

/**
 * Get new Identifier for new session
 *
 * \return	new Identificator
 *
 * Note: need to be unique only for request/reply - random number should be
 *       quite good --- maybe still to check for duplicate in use? TODO
 *
 */
guint8 radius_get_new_identifier (void)
{
	guint8 rnd = 0;

	if (rand_bytes(&rnd, 1) != 1)
		LOG_BUG("rand_bytes return -1!");

	return rnd;
}

/**
 * Get radius packet length
 *
 * @param packet	RADIUS packet
 *
 * \return RADIUS packet length
 */
int radius_get_packet_len (struct radius_hdr *packet)
{
	return (packet != NULL)? g_ntohs(packet->length) : 0;
}

/**
 * Get RADIUS code
 *
 * @param packet	RADIUS packet
 *
 * \return RADIUS packet Code
 */
guint8 radius_get_packet_code (struct radius_hdr *packet)
{
	return (packet != NULL)? packet->code : 0;
}

/**
 * Get RADIUS packet identifier
 *
 * @param packet	RADIUS packet
 *
 * \return RADIUS packet identifier
 */
guint8 radius_get_packet_identifier (struct radius_hdr *packet)
{
	return (packet != NULL)? packet->identifier : 0;
}

/**
 * Get defined RADIUS attribute
 *
 * @param hdr	RADIUS packet header
 * @param type	Type of attribute to search
 *
 * \return	Pointer to attribute header (in RADIUS packet)
 */
struct radius_attr_hdr *radius_get_attribute(struct radius_hdr *hdr,
		guint8 type)
{
	struct radius_attr_hdr *attr = NULL;
	guint len;

	LOG_FUNC_START(1);

	if (hdr == NULL)
		goto out;

	attr = (struct radius_attr_hdr *)(hdr + 1);
	len = g_ntohs(hdr->length) - sizeof(struct radius_hdr);

	while(len > 0) {
		if (attr->type == type)
			goto out;

		len -= attr->length;
		attr = NEXTATTR(attr);
	}

	attr = NULL;

out:
	LOG_FUNC_END(1);

	return attr;
}

/**
 * Get EAP packet from RADIUS packet
 *
 * @param packet	RADIUS packet
 *
 * \return	Pointer to EAP packet data
 *		(making copy of data, because EAP can be within >1 attributes)
 *
 */
void *radius_get_eap_packet (struct radius_hdr *packet)
{
	struct radius_hdr *hdr;
	struct radius_attr_hdr *attr;
	guint len, eap_len = 0, eap_done;
	void *eap = NULL;

	LOG_FUNC_START(1);

	if (packet == NULL)
		return NULL;

	hdr = packet;

	attr = (struct radius_attr_hdr *) (hdr + 1);
	len = g_ntohs(hdr->length)-sizeof(struct radius_hdr);

	while(len > 0) {
		if (attr->type == RADIUS_ATTR_EAP_MESSAGE) {
			eap_len += attr->length - 2;
		}
		len -= attr->length;
		attr = NEXTATTR(attr);
	}

	if (eap_len == 0)
		goto out;

	eap = g_malloc0(eap_len);

	attr = (struct radius_attr_hdr *) (hdr + 1);
	len = g_ntohs(hdr->length)-sizeof(struct radius_hdr);

	eap_done = 0;

	while(len > 0 && eap_done < eap_len) {
		if (attr->type == RADIUS_ATTR_EAP_MESSAGE) {
			memcpy(eap + eap_done, attr + 1, attr->length -2);
			eap_done += attr->length - 2;
		}
		len -= attr->length;
		attr = NEXTATTR(attr);
	}

out:
	LOG_FUNC_END(1);
	return eap;
}

/**
 * Print content of RADIUS packet
 *
 * @param packet	Pointer to RADIUS packet
 * @param prefix	text to put in front of data
 *
 */
void radius_dump_packet (struct radius_hdr *packet, gchar *prefix)
{
	struct radius_hdr *hdr;
	struct radius_attr_hdr *attr;
	guint len, eap_done = 0;
	gchar *prefix2eap = NULL;
	void *eappack = NULL;
	guint8 *n;
	gchar *text;
	gchar *type, value[128];
	guint32 nas;

	LOG_FUNC_START(1);

	if (packet == NULL) {
		LOG_BUG("NULL passed to dump_radius_packet");
		return;
	}

	hdr = packet;

	LOG_DEBUG("%sRADIUS packet:>>>", prefix);
	LOG_DEBUG("%s\tCode=%u", prefix, hdr->code);
	LOG_DEBUG("%s\tIdentifier=%u", prefix, hdr->identifier);
	LOG_DEBUG("%s\tLength=%d", prefix, g_ntohs(hdr->length));

	text = radius_hextostring (hdr->authenticator, 16);
	LOG_DEBUG ("%s\tAuthenticator=0x%s", prefix, text);
	g_free(text);

	LOG_DEBUG ("%s", prefix);
	LOG_DEBUG ("%s\tAttributes:", prefix);
	LOG_DEBUG ("%s", prefix);

	attr = (struct radius_attr_hdr *) (hdr + 1);
	len = g_ntohs(hdr->length)-sizeof(struct radius_hdr);

	while (len>0) {
		type = NULL;
		value[0] = 0;

		switch(attr->type) {
		case RADIUS_ATTR_USER_NAME:
			type = "User-Name";
		
			strncpy (value, ((const char*) attr) + 2,
							attr->length - 2);
			value[attr->length - 2] = 0;
						
			break;
		
		case RADIUS_ATTR_NAS_IDENTIFIER:
			type = "NAS-Identifier";
		
			strncpy (value, ((const char*) attr) + 2,
							attr->length - 2);
			value[attr->length - 2] = 0;
						
			break;
		
		case RADIUS_ATTR_NAS_PORT:
			type = "NAS-Port";
		
			n = ((guint8 *) attr) + 2;
			/*
			 * "Is it simpler to use ntohl to initialize nas?"
			 *
			 * No, we cant assume this field is aligned ...
			 */
			nas = (n[0] << 24) + (n[1] << 16) + (n[2] << 8) + n[3];
			sprintf(value, "%d", nas);
			break;
		
		case RADIUS_ATTR_EAP_MESSAGE:
			if (!eap_done) {
				type = "EAP-Message";
				value[0] = 0;
			
				if (attr->length > 2) {
					prefix2eap = g_strjoin(NULL, prefix,
							"\t", NULL);
					eappack = radius_get_eap_packet(packet);
				}
			}
			break;
		
		case RADIUS_ATTR_MESSAGE_AUTHENTICATOR:
			type = "Message-Authenticator";
			value[0] = 0;
			break;

		case RADIUS_ATTR_STATE:
			type = "State";
			value[0] = 0;
			break;
		
		case RADIUS_ATTR_PROXY_STATE:
			type = "Proxy-State";
			value[0] = 0;
			break;
		
		case RADIUS_ATTR_SESSION_TIMEOUT:
			type = "Session-Timeout";
			n = ((guint8 *) attr) + 2;
			nas = (n[0] << 24) + (n[1] << 16) + (n[2] << 8) + n[3];
			sprintf(value, "%d", nas);
			break;
		
		default:
			break;
		}

		if (attr->type != RADIUS_ATTR_EAP_MESSAGE || eap_done == 0) {
			text = radius_hextostring (((void *) attr) + 2,
							attr->length - 2);

			if (type)
				LOG_DEBUG ("%s\tType=%u (%s)", prefix,
							attr->type, type);
			else
				LOG_DEBUG ("%s\tType=%u", prefix, attr->type);
		
			LOG_DEBUG ("%s\tLength=%d", prefix, attr->length);

		
			if (value[0] != 0) /* dump value */
				LOG_DEBUG ("%s\tValue=%s", prefix, value);
			
			else /* dump hex value */
				LOG_DEBUG ("%s\tValue=0x%s", prefix, text);
		
			if (attr->type == RADIUS_ATTR_EAP_MESSAGE &&
				attr->length > 2)
			{
				radius_eap_dump_packet(eappack, prefix2eap);
				g_free(eappack);
				g_free(prefix2eap);
				eap_done = 1;
			}
		
			g_free(text);

			LOG_DEBUG ("%s", prefix);
		}
	
		if (attr->length <= 0) {
			LOG_BUG ("Attribute size == 0, aborting");
			return;
		}
	
		len -= attr->length;
		attr = NEXTATTR(attr);
	}

	LOG_DEBUG("%sRADIUS packet:<<<", prefix);
	LOG_FUNC_END(1);
}

/**
 * Create hexadecimal ASCII representation of binary data
 *
 * @param h	pointer to binary data (source)
 * @param len	length fo 'h' (in bytes)
 *
 * \return Pointer to a hexadecimal representation of binary data
 *
 * Note: When no longer necessary, allocated memory should be
 * free'd using g_free function
 */
gchar *radius_hextostring (void *h, gint len)
{
	guint8 *x = h;
	gchar *s, hexa[] = "0123456789abcdef";
	int i;

	if (x == NULL || len <= 0) {
		return NULL;
	}
	
	s = g_malloc0 (2 * len * sizeof(gchar) + 1);
	if (s == NULL)
		return NULL;
	
	for(i = 0; i < len; i++) {
		s[i * 2] = hexa[ x[i] >> 4 ];
		s[i * 2 + 1] = hexa[ x[i] & 0x000f ];
	}

	s[i*2] = 0;

	return s;
}

/**
 * Create a new socket for radius usage
 *
 * \return Handle to be used for state machine manipulation, or NULL in
 *         case of an error
 */
struct radius_socket *radius_socket_new()
{
	struct radius_socket *rs = NULL;
	int protocol_family;

	LOG_FUNC_START(1);

	if (radius_data == NULL) {
		LOG_BUG("radius subsystem NOT initialized");
		goto out;
	}

	rs = g_malloc0(sizeof(struct radius_socket));
	rs->refcnt = 1;

	/*
	 * Create a new socket (UDP) for IPv6 that handle both IPv4 and IPv6
	 */
	protocol_family = AF_INET6;

	rs->socket = network_socket_register_queue(protocol_family,
				SOCK_DGRAM, 0, NULL, 0, radius_data->queue);

	if (rs->socket == NULL) {
		LOG_BUG("radius_socket not created");
		g_free(rs);
		rs = NULL;
	}

out:
	LOG_FUNC_END(1);

	return rs;
}

/**
 * Free socket used by radius
 *
 * @param rs		pointer to radius socket to be released
 */
void radius_socket_free (struct radius_socket *rs)
{
	LOG_FUNC_START(1);

	if(rs) {
		if (rs->socket)
			network_socket_unregister (rs->socket);
		g_free(rs);
	}

	LOG_FUNC_END(1);
}

/**
 * Verify received RADIUS packet (code, size, digest...)
 *
 * @param buffer	radius packet
 * @param rs		struct radius state
 *
 * \return 1 if validation is successful, 0 otherwise
 *
 * Note: If validation is succesfull mutex on radius state is locked!
 */
struct radius_state *radius_verify_packet (void *buffer, int size)
{
	struct radius_hdr *recv;
	guint8 *data;
	char *proxy_state;
	GSList *item;
	struct radius_state *rs;
	struct radius_hdr *sent;
	guint8 digest[16], recv_auth[16], recv_msg_auth[16];
	struct radius_attr_hdr *attr, *msg_auth;
	char *key;
	int key_len, slen, rlen, cpos, retval = 0;
	MD5_CTX	context;

	LOG_FUNC_START(1);

	recv = buffer;
	data = buffer;
	/*
	 * Check basic packet integrity of RADIUS packet
	 * - code field
	 * - size of packet
	 * - 'length' in packet
	 */
	if (	recv->code != RADIUS_CODE_ACCESS_CHALLENGE &&
		recv->code != RADIUS_CODE_ACCESS_ACCEPT &&
		recv->code != RADIUS_CODE_ACCESS_REJECT ) {
		LOG_ERROR("Received RADIUS packet don't have expected"
			" code (received code=%d)", recv->code);
		goto out;
	}
	if ( size < RADIUS_MIN_PACKET_LEN || size > RADIUS_MAX_PACKET_LEN ) {
		LOG_ERROR("Received RADIUS packet is too small or big:"
			" size=%d", size);
		goto out;
	}

	rlen = (data[2] << 8) + data[3];

	if ( rlen < RADIUS_MIN_PACKET_LEN ||
		rlen > RADIUS_MAX_PACKET_LEN ) {
		LOG_ERROR("'length' in received RADIUS packet is to"
			" small or big: size=%d", rlen);
		goto out;
	}
	if ( rlen > size ) {
		LOG_ERROR("'length' in received RADIUS packet is"
			" greater than packet size: packet_size=%d, "
			"RADIUS_packet->length=%d", size, rlen);
		goto out;
	}

	/*
	 * attributes integrity
	 */
	cpos = sizeof(struct radius_hdr);
	while ( cpos < rlen ) {
		attr = buffer + cpos;
		if ( attr->type < 1 || attr->type > 240 ) {
			LOG_ERROR("Unknown attribute in RADIUS packet");
			goto out;
		}
		if ( attr->length < 2 ) {
			LOG_ERROR("Attribute too small (in RADIUS "
					"packet)");
			goto out;
		}
		if ( cpos + attr->length > rlen ) {
			LOG_ERROR("Attribute goes over RADIUS packet"
					"boundary");
			goto out;
		}
		cpos += attr->length;
	}
	/*
	 * packet looks fine so far
	 */
	attr = radius_get_attribute(buffer,
			RADIUS_ATTR_PROXY_STATE);
	if (!attr){
		LOG_ERROR("No PROXY_STATE attribute in packet");
		goto out;
	}
	proxy_state = (char *) (attr + 1);

	memcpy(&rs, proxy_state, sizeof(void *));

	g_mutex_lock(radius_data->mutex);
	item = g_slist_find(radius_data->rsessions, rs);

	if (item) {
		rs = item->data;
		g_mutex_lock(rs->mutex);
	}
	else {
		rs = NULL;
	}

	g_mutex_unlock(radius_data->mutex);

	if (!rs) {
		LOG_ERROR("Received reply but couldn't match it with"
				" any request");
		goto out;
	}
	if (rs->rpacket || !rs->spacket) {
		LOG_ERROR("Received reply but one is already in processing "
				"(%p-%p), or spacket is NULL", rs, rs->rpacket);
		g_mutex_unlock(rs->mutex);
		goto out;
	}
	sent = rs->spacket;
	if (recv->identifier != sent->identifier) {
		LOG_WARNING("Wrong identifier in RADIUS packet");
		g_mutex_unlock(rs->mutex);
		goto out;
	}
	if (memcmp(rs->proxy_state, proxy_state, 2 * sizeof(void *) + 4)) {
		LOG_WARNING("PROXY_STATE differs in received packet");
		log_buffer(LOGGERNAME, rs->proxy_state, 2*sizeof(void *) + 4);
		log_buffer(LOGGERNAME, proxy_state, 2*sizeof(void *) + 4);
		g_mutex_unlock(rs->mutex);
		goto out;
	}

	/*
	 * Integrity OK, check hashes
	 */
	slen = ntohs(sent->length);
	key = radius_get_server_secret(rs);
	key_len = strlen(key);

	/*
	 * Check 'authenticator' field
	 */
	memcpy(recv_auth, recv->authenticator, 16);
	memcpy(recv->authenticator, sent->authenticator, 16);

	/*
	 * Response Authenticator =
	 * 	MD5(Code+ID+Length+RequestAuth+Attributes+Secret)
	 */
	MD5_Init(&context);
	MD5_Update(&context, recv, rlen);
	MD5_Update(&context, key, key_len);
	MD5_Final(digest, &context);

	if ( memcmp(digest, recv_auth, 16) != 0 ) {
		LOG_ERROR("'authenticator' validation failed");
		g_mutex_unlock(rs->mutex);
		goto out_restore;
	} else {
		LOG_DEBUG("'authenticator' validation successful");
		retval = 1;
	}

	/*
	 * Validate Message-Authenticator
	 */
	msg_auth = radius_get_attribute (recv,
			RADIUS_ATTR_MESSAGE_AUTHENTICATOR);
	if ( !msg_auth )
		goto out_restore;
	
	retval = 0;

	memcpy(recv_msg_auth, msg_auth + 1, 16);
	memset(msg_auth + 1, 0, 16);

	prf_hmac_md5((guint8 *) key, key_len, (guint8 *) recv,
			rlen, (caddr_t)digest);

	if ( memcmp(digest, recv_msg_auth, 16) ) {
		LOG_ERROR("'Message-Authenticator' validation failed");
		log_buffer(LOGGERNAME, (char *) recv_msg_auth, 16);
		log_buffer(LOGGERNAME, (char *) digest, 16);
		g_mutex_unlock(rs->mutex);
	} else {
		LOG_DEBUG("'Message-Authenticator' validation successful");
		retval = 1;
	}
	memcpy(msg_auth + 1, recv_msg_auth, 16);

out_restore:
	memcpy(recv->authenticator, recv_auth, 16);

out:
	if (!retval)
		rs = NULL;

	LOG_FUNC_END(1);

	return rs;
}

/**
 * Create RADIUS packet (if not provided) and send it to RADIUS server
 * (used from outside RADIUS subsystem)
 *
 * @param eappack	pointer to EAP packet to send, NULL if initiating EAP
 *			exchange)
 * @param _username	Username to be used (MUST NOT be NULL)
 * @param _username_len	length of username (username does not need to be null
 *                      terminated string)
 * @param radius	pointer to radius_state
 *
 * \return	1 if packet succesfully sent, 0 otherwise
 */
int radius_send_packet (void *eappack, gchar *_username, guint _username_len,
		struct radius_state *radius)
{
	struct radius_hdr *radpack, *newpack;
	struct radius_hdr *prevrpack;
	gchar *username = _username;
	guint username_len = _username_len;
	struct radius_attr_hdr *attr;
	int identifier = -1;
	int retval = AAA_ERROR;

	int packet_length;

	LOG_FUNC_START(1);

	if (radius == NULL) {
		LOG_BUG("NULL parameters for 'radius_state' in ",
				"'radius_send_packet' funtion");
		goto out;
	}

	if (radius_data == NULL) {
		LOG_BUG("radius subsystem not initialized");
		goto out;
	}

	radpack = radius->spacket;
	prevrpack = radius->rpacket;

	/*
	 * if radius->spacket != NULL && radius->rpacket != NULL
	 *    && eappack == NULL
	 * -> retransmition, use same socket and same packet that was sent before
	 *
	 * Otherwise, create a new packet
	 */
	if (radpack == NULL || eappack != NULL) {
		/*
		 * Create a new RADIUS packet
		 */

		/*
		 * Find socket where to send: had to have some identifiers free
		 */
		identifier = radius_get_new_identifier();

		/*
		 * Create new RADIUS packet
		 */
		if (username == NULL) {
			/*
			 * Extract username from previously sent packet
			 */
			attr = radius_get_attribute(radpack, RADIUS_ATTR_USER_NAME);
			if (attr == NULL) {
				LOG_ERROR("Username not provided!");
				goto out;
			}
			username_len = attr->length - 2;
			username = (gchar *) (attr + 1);
		}

		newpack = radius_create_access_request_packet (prevrpack,
				eappack, username, username_len, identifier,
				radius);

		if (newpack == NULL) {
			LOG_BUG("RADIUS packet not created");
			goto out;
		}

		if (radius->spacket)
			g_free(radius->spacket);

		radius->spacket = newpack;
	}
	/*
	 * else - resend radius->spacket
	 */

	if (radius->rpacket) {
		g_free(radius->rpacket);
		radius->rpacket = NULL;
	}

	/*
	 * Sent UDP packet through socket:radius->socket to RADIUS server at
	 * address radius->auth_server
	 */
	packet_length = radius_get_packet_len(radius->spacket);

	if (network_send_packet(radius->socket, radius->spacket,
			packet_length, radius->auth_server->addr) < 0) {
		LOG_BUG("network_send_packet returned neg. value");
		goto out;
	}

	radius->auth_timeout = timeout_register_thread(&radius->auth_timeval,
					0, radius, radius_auth_timeout);
	retval = AAA_CONT;

	if (radius->eap_packet) {
		g_free(radius->eap_packet);
		radius->eap_packet = NULL;
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Function called when radius server didn't respond in defined time
 */
gpointer radius_auth_timeout(gpointer data)
{
	struct radius_state *rs = (struct radius_state *) data;
	struct aaa_msg *msg;
	GSList *item;
	guint timeout;

	LOG_FUNC_START(1);

	/*
	 * Check if session still exists
	 */
	g_mutex_lock(radius_data->mutex);
	item = g_slist_find(radius_data->rsessions, rs);
                 
	if (!item) {
		g_mutex_unlock(radius_data->mutex);
		goto out;
	}

	g_mutex_lock(rs->mutex);
	g_mutex_unlock(radius_data->mutex);

	if ( rs->auth_retry_count ) {
		/*
		 * Re-sent radius packet
		 */
		rs->auth_retry_count--;

		/*
		 * Reset timeout - double response times
		 */
		timeout = rs->auth_timeval.tv_sec * 1000 +
				rs->auth_timeval.tv_usec / 1000;
		timeout <<= 1;
	
		rs->auth_timeval.tv_sec = timeout / 1000;
		rs->auth_timeval.tv_usec = (timeout % 1000) * 1000;

		if (radius_send_packet(NULL, NULL, 0, rs) == AAA_ERROR) {
			LOG_ERROR("Error sending RADIUS response");
		} else {
			LOG_WARNING("Resending RADIUS packet (%d retries left)",
				rs->auth_retry_count);
		}
	} else {
		/*
		 * RADIUS server not responding after retries,
		 * drop session and send reject
		 */
		LOG_ERROR("RADIUS server not responding, aborting");

		msg = aaa_msg_new();
		msg->type = AAA_NO_RESPONSE;
		msg->session = rs->session;
		g_async_queue_push (radius_data->upper_queue, msg);
	}
	g_mutex_unlock(rs->mutex);

out:
	LOG_FUNC_END(1);

	return NULL;
}

/**
 * radius subsystem main thread - thread that will wait for messages comming
 * from some RADIUS server as response for previos requests
 *
 * @param data		not used, got all needed in radius_data
 *
 * \return		NULL when exiting
 */
gpointer radius_main_thread (gpointer data)
{
	struct network_msg *net_msg;
	void *buffer;
	int size;
	struct radius_state *rs;
	struct aaa_msg *msg;

	LOG_FUNC_START(1);

	if (radius_data == NULL) {
		LOG_BUG("radius subsystem not initialized");
		goto out;
	}

	while (1) {
		LOG_DEBUG("Waiting for RADIUS messages");
	
		net_msg = (struct network_msg *) g_async_queue_pop(
						radius_data->queue);

		LOG_DEBUG("Got message form a queue: %p", net_msg);
	
		if (GPOINTER_TO_UINT(net_msg) == 1) {
			LOG_DEBUG("Got terminate message");
			break;
		}

		size = network_msg_get_data(net_msg, &buffer);
		network_msg_free(net_msg);

		rs = radius_verify_packet(buffer, size);
	
		if (!rs) {
			LOG_ERROR("RADIUS packet validation failed");
			g_free(buffer);
			continue;
		}

		rs->rpacket = buffer;
		rs->eap_packet = radius_get_eap_packet(rs->rpacket);
		LOG_DEBUG("EAP packet = %p", rs->eap_packet);

		timeout_cancel(rs->auth_timeout);
		rs->auth_timeout = NULL;
		rs->auth_retry_count = rs->cr->retries;
	
		g_mutex_unlock(rs->mutex);
	
		msg = aaa_msg_new();
		msg->session = rs->session;
		msg->type = AAA_MSG_READY;

		g_async_queue_push (radius_data->upper_queue, msg);
	}

out:
	LOG_FUNC_END(1);

	return NULL;
}

/***************************************************************************
 * INTERFACE TO AAA_WRAPPER
 ***************************************************************************/

/**
 * Get Session-Timeout attribute value
 *
 * @param rs	radius state (with saved RADIUS packet)
 *
 * \return	session-timeout value, if present
 * 		otherwise -1
 */
int radius_get_session_timeout_attribute_value(struct radius_state *rs)
{
	struct radius_hdr *hdr;
	int session_timeout = -1;
	struct radius_attr_hdr *attr;
	unsigned char *time;

	LOG_FUNC_START(1);

	if (!rs || !rs->rpacket)
		goto out;

	hdr = rs->rpacket;

	attr = radius_get_attribute(hdr, RADIUS_ATTR_SESSION_TIMEOUT);
	if ( attr ) {
		time = (unsigned char *)(attr + 1);
		session_timeout = time[0];
		session_timeout = (session_timeout << 8) + time[1];
		session_timeout = (session_timeout << 8) + time[2];
		session_timeout = (session_timeout << 8) + time[3];
		LOG_DEBUG("Session-Timeout (from RADIUS) = %d", session_timeout);
	}
out:
	LOG_FUNC_END(1);

	return session_timeout;
}

/**
 * Get MSK from last RADIUS packet, if exist
 * (Assuming MSK is in fields: MS-MPPE-Recv-Key and MS-MPPE-Send-Key, which is
 *  when TLS is used. If there could be something different ... FIXME )
 *
 * @param radius	pointer to radius_state struct. with RADIUS packet
 * @param key		pointer for returning pointer to key
 * @param key_len	pointer for returning key_length
 *
 * \return		TRUE if MSK is present, FALSE otherwise
 */
int radius_get_msk(struct radius_state *radius, gchar **key, size_t *key_len)
{
	/*
	 * FIXME - if MSK is not in MS_MPPE_SEND/RECV_KEY format? could it be?
	 */
	gboolean retval;

	struct radius_hdr *hdr;
	struct radius_attr_hdr *attr;
	guint8 *ptr, *c, *b, *p, *A, *R, *string;
	char *S;
	guint8 vendor_type1 = MS_MPPE_SEND_KEY;
	guint8 vendor_type2 = MS_MPPE_RECV_KEY;
	guint8 type = RADIUS_ATTR_VENDOR_SPECIFIC;
	guint len, str_len, j, i, S_len;

	LOG_FUNC_START(1);

	retval = FALSE;

	if (radius->msk) {
		*key = radius->msk;
		*key_len = radius->msk_len;
		retval = TRUE;
		goto out;
	}
	else {
		*key = NULL;
		*key_len = 0;
	}

	if (!radius || !(radius->rpacket)) {
		LOG_BUG("'radius' and radius->rpacket MUST NOT be null");
		goto out;
	}

	hdr = radius->rpacket;

	if (hdr == NULL)
		goto out;

	attr = (struct radius_attr_hdr *)(hdr + 1);
	len = g_ntohs(hdr->length) - sizeof(struct radius_hdr);

	while (len > 0) {

		if (attr->type == type) {
			/*
			 * We need MS-MPPE-Send/Recv-Key(s) (16/17)
			 * It's in MS Vendor-Specific attribute
			 * MS's Vendor-Id is 311 = 0x0137
			 *
			 * Vendor ID is in last 2 octets
			 * skip 'Type|Length|Vendor-Id' - first 32 bit
			 * and go to: Vendor-Id (cont)
			 */
			ptr = ((guchar *)attr) + 4;

			/*
			 * Have we got at least some headers
			 */
			if (attr->length > 8
					&& ptr[0] == 0x01 && ptr[1]==0x37
					&& (ptr[2] == vendor_type1 ||
						ptr[2] == vendor_type2)) {
				/*
				 * Decrypt MSK
				 * According to: RFC2548: MS-MPPE-Send-Key
				 */
				A = ptr + 4; /* salt */
				c = ptr + 6; /* crypted string */
				S = radius_get_server_secret(radius);
				S_len = strlen(S);
				//R = hdr->authenticator;
				R = ((struct radius_hdr *) radius->spacket)
					->authenticator; /* req. auth. */
			
				str_len = ptr[3] - 4;
				b = g_malloc0(16);
				p = g_malloc0(str_len);
			
				string = g_malloc0(S_len + 16 + 2 + 1);
				memcpy(string, S, S_len);
			
				/* 1ST round */
				memcpy(string + S_len, R, 16);
				memcpy(string + S_len + 16, A, 2);
				MD5 (string, S_len + 16 + 2, b);
				for (j = 0; j<16; j++)
					p[j] = b[j] ^ c[j];

				/* Rounds 2-'i' */
				for (i = 1; i < str_len/16; i++) {
					memcpy(string + S_len,
							c + (i-1)*16, 16);
					MD5 (string, S_len + 16, b);
					for (j = 0; j<16; j++)
						p[i*16+j] = b[j] ^ c[i*16+j];
				}
			
				/* 'plaintext' is in 'p' */
				*key = g_realloc(*key, *key_len + p[0]);
				memcpy(((gchar*)*key) + *key_len, p + 1, p[0]);
				*key_len += p[0];
			
				g_free(b);
				g_free(p);
				g_free(string);
			
				retval = TRUE;
			}
		}

		len -= attr->length;
		attr = NEXTATTR(attr);
	}


	if (retval) {
		LOG_DEBUG("MSK, len=%d", *key_len);
		log_buffer(LOGGERNAME, (char *)*key, *key_len);
	
		radius->msk = *key;
		radius->msk_len = *key_len;
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Get response (EAP packet) from last RADIUS packet
 *
 * @param rs		radius state
 * @param eap_packet	place for EAP packet
 *
 * \return 'code' of response
 *
 * Note: EAP packet is saved in 'rs' and should not be free outside
 * radius subsystem!
 */
int radius_get_response(struct radius_state *rs, void **eap_packet)
{
	int retval = AAA_ERROR;
	int radius_code;

	LOG_FUNC_START(1);

	if ( rs && eap_packet && rs->eap_packet ) {
		*eap_packet = rs->eap_packet;

		radius_code = radius_get_packet_code(rs->rpacket);
	
		switch ( radius_code ) {
		case RADIUS_CODE_ACCESS_CHALLENGE:
			retval = AAA_CONT;
			LOG_DEBUG("Received RADIUS code ACCESS_CHALLENGE");
			break;
		case RADIUS_CODE_ACCESS_ACCEPT:
			retval = AAA_ACCEPT;
			LOG_DEBUG("Received RADIUS code ACCESS_ACCEPT");
			break;
		case RADIUS_CODE_ACCESS_REJECT:
			LOG_DEBUG("Received RADIUS code ACCESS_REJECT (%d)",
					radius_code);
			retval = AAA_REJECT;
		default:
			LOG_DEBUG("Received unexpected RADIUS code: %d",
					radius_code);
		}
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Create new request and send it to RADISU server, including provided
 * EAP packet
 *
 * @param rs	radius state
 * @param eap	EAP packet to send
 *
 * \return 'code' of response
 */
int radius_request(struct radius_state *rs, void *eap)
{
	int retval = AAA_REJECT;

	LOG_FUNC_START(1);

	if (!rs || !eap)
		return retval;

	g_mutex_lock(rs->mutex);

	if (rs->proxy_state)
		g_free(rs->proxy_state);

	rs->proxy_state = g_malloc0(2 * sizeof(void *) + 4);
	memcpy(rs->proxy_state, &rs, sizeof(void *));
	memcpy(rs->proxy_state + sizeof(void *), &rs->session, sizeof(void *));
	rand_bytes(rs->proxy_state + 2 * sizeof(void *), 4);

	retval = radius_send_packet (eap, NULL, 0, rs);

	g_mutex_unlock(rs->mutex);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Create new session and send request to RADIUS server
 *
 * @param _session	pointer to session (in state machines)
 * @param eap		EAP packet to send
 * @param _uname	username (can be NULL if EAP packet contains it)
 * @param _uname_len	length of '_uname'
 * @param retval	pointer for status
 *
 * \return pointer to new radius state structure (if all was successful)
 */
struct radius_state *radius_new_client(void *_session, void *eap, char *_uname,
		int _uname_len, int *retval)
{
	struct radius_state *rs;
	struct radius_config *cr;
	struct session *session = _session;
	struct radius_eap_hdr *eap_pack;
	struct radius_eap_data *eap_data;
	char *uname = _uname;
	int uname_len = _uname_len;

	LOG_FUNC_START(1);

	*retval = AAA_ERROR;

	if (!radius_data || !radius_data->ikev2_data ||
		!radius_data->ikev2_data->config ||
		!radius_data->ikev2_data->config->radius_servers ||
		!session || !session->cp || !session->cp->radius_server )
		goto out;

	if (!eap && !uname) {
		LOG_ERROR("Neither EAP packet nor username are provided");
		goto out;
	}

	cr = radius_config_find_by_id(
			radius_data->ikev2_data->config->radius_servers,
			session->cp->radius_server);

	if (!cr)
		goto out;

	rs = g_malloc0(sizeof(struct radius_state));

	rs->mutex = g_mutex_new();
	g_mutex_lock(rs->mutex);

	rs->cr = cr;

	/*
	 * auth_server = first from list
	 */
	rs->auth_server = cr->auth_servers->data;

	rs->auth_timeval.tv_sec = cr->timeout / 1000;
	rs->auth_timeval.tv_usec = (cr->timeout % 1000) * 1000;
	rs->auth_retry_count = cr->retries;

	/*
	 * acct_server = first from list
	 */
	if (cr->acct_servers) {
		rs->acct_server = cr->acct_servers->data;

		rs->acct_timeval.tv_sec = cr->timeout / 1000;
		rs->acct_timeval.tv_usec = (cr->timeout % 1000) * 1000;
		rs->acct_retry_count = cr->retries;
	}
	rs->socket = ((struct radius_socket *)
			(radius_data->sockets->data))->socket;

	rs->session = session;

	rs->proxy_state = g_malloc0(2 * sizeof(void *) + 4);
	memcpy(rs->proxy_state, &rs, sizeof(void *));
	memcpy(rs->proxy_state + sizeof(void *), &rs->session, sizeof(void *));
	rand_bytes(rs->proxy_state + 2 * sizeof(void *), 4);

	/*
	 * Create EAP-Identity-Response packet (username is provided)
	 */
	eap_pack = eap;
	*retval = AAA_CONT;

	if (!eap) {
		eap_pack = g_malloc0(sizeof(struct radius_eap_hdr)
					+ uname_len + 1);
		eap_pack->code = EAP_CODE_RESPONSE;
		rand_bytes(&eap_pack->identifier, 1);
		eap_pack->length = g_htons(sizeof(struct radius_eap_hdr)
						+ uname_len + 1);
		eap_data = (struct radius_eap_data *) (eap_pack + 1);
		eap_data->type = EAP_TYPE_IDENTITY;
		memcpy(eap_data->type_data, uname, uname_len);
	} else {
		if (!uname) {
			eap_data = (struct radius_eap_data *) (eap_pack + 1);
			if (eap_pack->code == EAP_CODE_RESPONSE &&
					eap_data->type == EAP_TYPE_IDENTITY) {
				uname = (char *) eap_data->type_data;
				uname_len = g_ntohs(eap_pack->length) - 
					sizeof(struct radius_eap_hdr) - 1;
			} else {
				LOG_ERROR("username not provided");
				*retval = AAA_ERROR;
			}
		}
	}

	if (*retval != AAA_ERROR)
		*retval = radius_send_packet (eap_pack, uname, uname_len, rs);

	if (eap != eap_pack)
		g_free(eap_pack);

	if (*retval == AAA_ERROR) {
		g_mutex_unlock(rs->mutex);
		radius_state_free(rs);
		rs = NULL;
	} else {
		g_mutex_lock(radius_data->mutex);
		radius_data->rsessions =
				g_slist_append(radius_data->rsessions, rs);
		g_mutex_unlock(rs->mutex);
		g_mutex_unlock(radius_data->mutex);
	}
out:
	LOG_FUNC_END(1);

	return rs;
}

/**
 * Release one RADIUS state machine
 * (used from outside RADIUS subsystem)
 *
 * @param rs	pointer to radius state
 */
void radius_state_free(struct radius_state *rs)
{
	LOG_FUNC_START(1);

	if (rs && radius_data) {
		g_mutex_lock(radius_data->mutex);

		if (rs->mutex)
			g_mutex_lock(rs->mutex);
		
		if (rs->spacket) {
			g_free(rs->spacket);
		}

		if (rs->rpacket) {
			g_free(rs->rpacket);
		}

		if (rs->msk)
			g_free(rs->msk);

		if (rs->proxy_state)
			g_free(rs->proxy_state);

		if (rs->eap_packet)
			g_free(rs->eap_packet);

		if (rs->auth_timeout)
			timeout_cancel(rs->auth_timeout);
		
		if (rs->mutex) {
			g_mutex_unlock(rs->mutex);
			g_mutex_free(rs->mutex);
		}
		
		g_free(rs);
	
		radius_data->rsessions = g_slist_remove(
				radius_data->rsessions, rs);

		g_mutex_unlock(radius_data->mutex);
	}

	LOG_FUNC_END(1);
}

/***************************************************************************
 * GLOBAL INITIALIZATION AND DEINITIALIZATION FUNCTIONS
 ***************************************************************************/

/**
 * Uninitialize radius subsystem
 * (used from outside RADIUS subsystem)
 *
 */
void radius_unload()
{
	/*
	 * Close and free socket used by RADIUS subsystem
	 */
	GSList *iter;
	struct radius_socket *socket;
	struct radius_state *rs;

	LOG_FUNC_START(1);

	if (radius_data == NULL) {
		LOG_BUG("'radius_data' already NULL");
		goto out;
	}

	/*
	 * push 'terminate' message to queue - thread will exit
	 */
	g_async_queue_push(radius_data->queue, GINT_TO_POINTER(1));

	/*
	 * wait for thread to exit, before continuing ...
	 */
	(void) g_thread_join (radius_data->thread);

	iter = radius_data->sockets;
	while (iter != NULL) {
		/*
		 * Release socket
		 */
		socket = iter->data;
		radius_socket_free(socket);

		iter->data = NULL;
		iter = iter->next;
	}
	g_slist_free(radius_data->sockets);

	iter = radius_data->rsessions;
	while (iter != NULL) {
		/*
		 * Release radius sessions
		 */
		rs = iter->data;
		radius_state_free(rs);

		iter->data = NULL;
		iter = iter->next;
	}
	g_slist_free(radius_data->rsessions);

	if (radius_data->queue)
		g_async_queue_unref(radius_data->queue);

	g_free(radius_data);

	radius_data = NULL;

out:
	LOG_FUNC_END(1);
}

/**
 * Initialize radius subsystem
 * (used from outside RADIUS subsystem)
 *
 * @param upper_queue		pointer to main queue for incomming messages
 *				(handled by 'sm_main_thread')
 * @param data			pointer to ikev2_data, main ikev2 data structure
 *
 * \return 			pointer to radius_data - radius subsystem data
 *				NULL in case of an error
 */
int radius_init(GAsyncQueue *upper_queue, struct ikev2_data *data)
{
	int retval = -1;
	struct radius_socket *s;

	LOG_FUNC_START(1);

	if (radius_data != NULL) {
		LOG_BUG("'radius_data' NOT NULL!");
		goto out;
	}

	radius_data = g_malloc0(sizeof(struct radius_data));

	radius_data->ikev2_data = data;

	/*
	 * Create a queue and a thread to watch for
	 * packets received from RADIUS
	 */

	/*
	 * Queue on which we'll receive messages
	 */
	radius_data->queue = g_async_queue_new();
	radius_data->upper_queue = upper_queue;

	/*
	 * Create a socket for sending/receiving RADIUS packets
	 */
	s = radius_socket_new();
	radius_data->sockets = g_slist_append (radius_data->sockets, s);

	radius_data->rsessions = NULL;

	radius_data->mutex = g_mutex_new();

	/*
	 * Start thread that will wait for a message
	 */
	radius_data->thread = g_thread_create(radius_main_thread,
				NULL, TRUE, NULL);

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

#endif /* RADIUS_CLIENT */
