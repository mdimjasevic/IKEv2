/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <glib.h>

/* 
 * This small program is used to test how fast is implementation of
 * asynchronous queues in GLib library. It was written by Ana Kukec.
 *
 * To compile use:
 * gcc -o async_queues_time_test async_queues_time_test.c	\
 *		`pkg-config --cflags glib-2.0`			\
 * 		`pkg-config --libs glib-2.0`			\
 *		`pkg-config --libs gthread-2.0`
 */

gpointer main_thread(gpointer);
GAsyncQueue *queue = NULL;

int main()
{
	int data[5];
	int i;
	GTimeVal *ptr;

	g_thread_init (NULL);

	queue = g_async_queue_new();
	
	srandom(time(NULL));

	for (i=0; i<5; i++) {
		data[i]=i;
		g_thread_create(main_thread, data + i, FALSE, NULL);
	}

	while(1) {
		ptr = g_new(GTimeVal, 1);
		g_get_current_time(ptr);
		g_async_queue_push(queue, (gpointer)ptr);
		sleep(1);
	}

	return 0;
}

gpointer main_thread(gpointer data)
{
	int tno = *(int *)data;
	GTimeVal *qtime;
	GTimeVal *otime;

	while(1) {
		printf("thread number = %d\n", tno);
		qtime = g_async_queue_pop(queue);
		g_get_current_time(otime);

		printf("elapsed: %ld sec, %ld usec\n",
			((long)(otime->tv_sec - qtime->tv_sec)),
			((long)(otime->tv_usec - qtime->tv_usec)));
		g_free(qtime);

		/* vjerojatnost povratka u queue = 10% */
		if (1 + (int)(10.0*random()/(RAND_MAX + 1.0)) == 1) {
			qtime = g_new(GTimeVal, 1);
			g_get_current_time(qtime);
			g_async_queue_push(queue, (gpointer)qtime);
		}
		
		sleep(1);
	}

	return NULL;
}
