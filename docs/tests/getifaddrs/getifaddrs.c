/**
 * The purpose of this sample code is to list all active
 * interfaces on the system and their IP addresses.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <linux/if.h>

int main(int argc, char **argv)
{
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	struct ifaddrs *ifap, *ifapc;
	char addr[128], flags[128];

	if (getifaddrs(&ifap) < 0) {
		perror("getifaddrs");
		return 1;
	}

	for (ifapc = ifap; ifapc; ifapc = ifapc->ifa_next) {

		/*
		 * This is for interfaces that have no link layer
		 * address, e.g. ppp.
		 */
		if (!ifapc->ifa_addr)
			continue;

		switch (ifapc->ifa_addr->sa_family) {
		case AF_INET:
			sin = (struct sockaddr_in *)ifapc->ifa_addr;
			inet_ntop(AF_INET, &sin->sin_addr, addr, 128);
			break;

		case AF_INET6:
			sin6 = (struct sockaddr_in6 *)ifapc->ifa_addr;
			inet_ntop(AF_INET6, &sin6->sin6_addr, addr, 128);
			break;

		default:
			snprintf(addr, 128, "Unknown type %u",
					ifapc->ifa_addr->sa_family);
		}
		printf ("name=%-10s flags=%08X type=", ifapc->ifa_name,
				ifapc->ifa_flags);

		flags[0] = 0;
		if (ifapc->ifa_flags & IFF_UP)
			printf("UP ");
		if (ifapc->ifa_flags & IFF_BROADCAST)
			printf("BROADCAST ");
		if (ifapc->ifa_flags & IFF_LOOPBACK)
			printf("LOOPBACK ");
		if (ifapc->ifa_flags & IFF_POINTOPOINT)
			printf("POINTTOPOINT ");
		if (ifapc->ifa_flags & IFF_RUNNING)
			printf("RUNNING ");
		if (ifapc->ifa_flags & IFF_NOARP)
			printf("NOARP ");
		if (ifapc->ifa_flags & IFF_MULTICAST)
			printf("MULTICAST ");

		printf (" addr=%s\n", addr);
	}

	freeifaddrs(ifap);

	return 0;
}
