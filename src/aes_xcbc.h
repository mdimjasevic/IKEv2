/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __AES_XCBC_H
#define __AES_XCBC_H

void aes_xcbc_mac_128(guchar *, int, guchar *, int, char *);

void aes_xcbc_mac_96(guchar *, int, guchar *, int, char *);

#define aes_xcbc_prf_128 aes_xcbc_mac_128

#endif
       
