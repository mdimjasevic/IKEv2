/***************************************************************************
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

#define __CRYPTO_C 

#define LOGGERNAME	"crypto"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */

#include <openssl/rand.h>
#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/des.h>
#include <openssl/aes.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/conf.h>

#include <glib.h>

#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>

#include "ikev2_consts.h"
#include "netlib.h"
#include "transforms.h"
#include "logging.h"
#include "config.h"
#include "payload.h"
#include "aes_xcbc.h"
#include "crypto.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/**
 * Functions for random data...
 */

/**
 * Generate num bytes of random data and place it into the buffer
 * pointed to by buf pointer.
 */
int rand_bytes(unsigned char *buf, int num)
{
	LOG_FUNC_START(1);

	return RAND_bytes(buf, num);
}

int dh_generate_key(DH *dh)
{

	/*
	 * Deterministic private key for testing purposes. Do not remove...
	static const char *bn = "4A71EA3D3664AF5F9501D71E5E93A9191A41DD72C4BAAE"
				"3C8AC2ADEA220710819C6783B368E09F409D7A5DB31745"
				"836FA718723DED686434EE1C6BFC4571FF723938D34591"
				"97D6D06D0C53B7BEBDCFB42B2AEC2169EB5D6FD41C8FD0"
				"9030243C9CE97411202E21C0F06D51FA967EB104D37242"
				"C4A366C879A8AFE5F05665D8F4F3CBD366D77A97AFD19E"
				"CE4C43DD9FAC86D5F70C8EBD7575FDCCD77306C8B4762C"
				"F5E589884EAECA2E07DCB5CD04DC8075A135485D99899D"
				"D53DA7582211F9F72AF15A0C29F520D7AC07B66B2A61FC"
				"C44A7BA4B51D73827D91946E8BE5343CA666B26BEA8107"
				"F61265DCCF21E48CF9E503D4896B69F72BFC35FF57CA0C"
				"3D6430045DE152CBD97C7E0861A58CFE8E93F75CB53E9F"
				"A54D536AE9B085398EAF3F251FBE03BCFD8F3D279105A8"
				"3B923B2FC7391B35BACF37450BA4970C7E0A8282CC2587"
				"749AE1AB3F2EAE6D5FB0F6EA275F1B200E0A0B4F851314"
				"A9C59B6643620AB303FD5E81A682AB2AA83C9F818804A4"
				"22AB9AF32EE364F87C0B642040C32D8BB562852D4928E8"
				"C1147323B84B693D7D43DDB70C31C5B62C3988947C4D1F"
				"40D26A51E242A0AE86AB85CF5DEA14F03D6454F96CCA35"
				"33BDEDC225399613783EFFF2A777C624753D1EDC401A47"
				"2DFDA243F0B04AFEC53AE0E1B8FF27BCF14ACD5FCF072E"
				"6BF00A42F6DD6F5239691519FD97DD018D8D2C38900D53"
				"576E530F41B6";

	if (false_random) {
		if (!BN_hex2bn(&(dh->priv_key), bn))
			return 0;
	}
	 */

	return DH_generate_key(dh);
}

DH *dh_ke_generate(guint16 dh_group)
{
	DH *dh;
	char ig = DHGROUP_GENERATOR;

	char *buffer;
	int size;

	LOG_FUNC_START(1);

	dh = NULL;
	if (dh_group > IKEV2_DHG_MAX) {
		LOG_DEBUG("Invalid DH group");
		goto out;
	}

	if (modp[dh_group] == NULL) {
		LOG_DEBUG("Invalid DH group");
		goto out;
	}

	if ((dh = DH_new()) == NULL) {
		LOG_DEBUG("Error allocating DH group");
		goto out;
	}

	if (BN_hex2bn(&(dh->p), modp[dh_group]) != strlen(modp[dh_group])) {
		LOG_DEBUG("Error generating DH data");
		goto out_free;
	}

	if ((dh->g = BN_bin2bn((guchar *)&ig, sizeof(ig), NULL)) == NULL) {
		LOG_DEBUG("Error generating DH data");
		goto out_free;
	}

	if (dh_generate_key(dh) != 1) {
		LOG_DEBUG("Error generating DH data");
		goto out_free;
	}

	LOG_DEBUG("dh->priv_key");
	if ((buffer = g_malloc(BN_num_bytes(dh->priv_key))) != NULL) {
		size = BN_bn2bin(dh->priv_key, (guchar *)buffer);
		log_buffer(LOGGERNAME, buffer, size);
		g_free (buffer);
	}

	LOG_DEBUG("dh->pub_key");
	if ((buffer = g_malloc(BN_num_bytes(dh->pub_key))) != NULL) {
		size = BN_bn2bin(dh->pub_key, (guchar *)buffer);
		log_buffer(LOGGERNAME, buffer, size);
		g_free (buffer);
	}

out:
	LOG_FUNC_END(1);
	return dh;

out_free:
	DH_free(dh);
	dh = NULL;

	goto out;
}

/**
 * Function which calculates hmac_md5
 *
 * @param text		pointer to data stream
 * @param text_len	length of data stream
 * @param key		pointer to authentication key
 * @param key_len	length of authentication key
 * @param digest	caller digest to be filled in
 *
 */
void prf_hmac_md5(unsigned char *key, int key_len,
		unsigned char *text, int text_len,
		caddr_t digest)
{
	MD5_CTX context;
	unsigned char k_ipad[65];	/* inner padding -
					 * key XORd with ipad
					 */
	unsigned char k_opad[65];	/* outer padding -
					 * key XORd with opad
					 */
	unsigned char tk[16];
	int i;

	/* if key is longer than 64 bytes reset it to key=MD5(key) */
	if (key_len > 64) {

		MD5_CTX		 tctx;

		MD5_Init(&tctx);
		MD5_Update(&tctx, key, key_len);
		MD5_Final(tk, &tctx);

		key = tk;
		key_len = 16;
	}

	/*
	 * the HMAC_MD5 transform looks like:
	 *
	 * MD5(K XOR opad, MD5(K XOR ipad, text))
	 *
	 * where K is an n byte key
	 * ipad is the byte 0x36 repeated 64 times
	 * opad is the byte 0x5c repeated 64 times
	 * and text is the data being protected
	 */

	/* start out by storing key in pads */
	memset(k_ipad, 0, sizeof(k_ipad));
	memset(k_opad, 0, sizeof(k_opad));
	memcpy(k_ipad, key, key_len);
	memcpy(k_opad, key, key_len);

	/* XOR key with ipad and opad values */
	for (i=0; i<64; i++) {
		k_ipad[i] ^= 0x36;
		k_opad[i] ^= 0x5c;
	}
	/*
	 * perform inner MD5
	 */
	MD5_Init(&context);			/* init context for 1st
						 * pass
						 */
	MD5_Update(&context, k_ipad, 64);	/* start with inner pad */
	MD5_Update(&context, text, text_len);	/* then text of datagram */
	MD5_Final((guchar *)digest, &context);	/* finish up 1st pass */
	/*
	 * perform outer MD5
	 */
	MD5_Init(&context);			/* init context for 2nd
						 * pass
						 */
	MD5_Update(&context, k_opad, 64);	/* start with outer pad */
	MD5_Update(&context, digest, 16);	/* then results of 1st
						 * hash
						 */
	MD5_Final((guchar *)digest, &context);	/* finish up 2nd pass */
}


/**
 * Function: hmac_sha1
 *
 * @param text		pointer to data stream
 * @param text_len	length of data stream
 * @param key		pointer to authentication key
 * @param key_len	length of authentication key
 * @param digest	caller digest to be filled in
 */
void prf_hmac_sha1(unsigned char *key, int key_len,
		unsigned char *text, int text_len,
		caddr_t digest)
{
	SHA_CTX context;
	unsigned char k_ipad[65];	/* inner padding -
					 * key XORd with ipad
					 */
	unsigned char k_opad[65];	/* outer padding -
					 * key XORd with opad
					 */
	unsigned char tk[20];
	int i;

	LOG_FUNC_START(1);

	/* if key is longer than 64 bytes reset it to key=SHA1(key) */
	if (key_len > 64) {

		SHA_CTX		 tctx;

		SHA1_Init(&tctx);
		SHA1_Update(&tctx, key, key_len);
		SHA1_Final(tk, &tctx);

		key = tk;
		key_len = 20;
	}

	/*
	 * the HMAC_SHA1 transform looks like:
	 *
	 * SHA1(K XOR opad, SHA1(K XOR ipad, text))
	 *
	 * where K is an n byte key
	 * ipad is the byte 0x36 repeated 64 times
	 * opad is the byte 0x5c repeated 64 times
	 * and text is the data being protected
	 */

	/* start out by storing key in pads */
	memset(k_ipad, 0, sizeof(k_ipad));
	memset(k_opad, 0, sizeof(k_opad));
	memcpy(k_ipad, key, key_len);
	memcpy(k_opad, key, key_len);

	/* XOR key with ipad and opad values */
	for (i=0; i<64; i++) {
		k_ipad[i] ^= 0x36;
		k_opad[i] ^= 0x5c;
	}

	/*
	 * perform inner SHA1
	 */
	SHA1_Init(&context);			/* init context for 1st pass */
	SHA1_Update(&context, k_ipad, 64);	/* start with inner pad */
	SHA1_Update(&context, text, text_len);	/* then text of datagram */
	SHA1_Final((guchar *)digest, &context);	/* finish up 1st pass */
	/*
	 * perform outer SHA1
	 */
	SHA1_Init(&context);			/* init context for 2nd pass */
	SHA1_Update(&context, k_opad, 64);	/* start with outer pad */
	SHA1_Update(&context, digest, 20);	/* then results of 1st hash */
	SHA1_Final((guchar *)digest, &context);	/* finish up 2nd pass */

	LOG_FUNC_END(1);
}

/*
 * Dispatcher routine to calculate pseudo-random function.
 *
 * @param prf_type	Pseudo-random function to use for calculations.
 * @param text		Pointer to data stream
 * @param text_len	length of data stream
 * @param key		pointer to authentication key
 * @param key_len	length of authentication key
 * @param digest	caller digest to be filled in
 *
 * \return 0 if everything went OK, -1 in case of an error.
 *
 * This function should call handler in transform_t structure (which is
 * not currently defined :)) and not do switch/case.
 */
int prf(transform_t *prf_alg, unsigned char *key, int key_len,
		unsigned char *text, int text_len, char **digest)
{
	LOG_FUNC_START(1);

	switch(prf_alg->id) {
	case IKEV2_PRF_HMAC_MD5:
		LOG_TRACE("md5, keylen=%d", key_len);
		if ((*digest = g_malloc0(MD5_DIGEST_SIZE)) == NULL) {
			printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__,
					__LINE__);
			return -1;
		}
		prf_hmac_md5(key, key_len, text, text_len, *digest);
		return MD5_DIGEST_SIZE;
		break;

	case IKEV2_PRF_HMAC_SHA1:
		LOG_TRACE("sha1, keylen=%d", key_len);
		if ((*digest = g_malloc0(SHA1_DIGEST_SIZE)) == NULL) {
			printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__,
					__LINE__);
			return -1;
		}
		prf_hmac_sha1(key, key_len, text, text_len, *digest);
		return SHA1_DIGEST_SIZE;
		break;

	case IKEV2_PRF_AES128_CBC:
		LOG_TRACE("aes, keylen=%d", key_len);
		if ((*digest = g_malloc0(AES_BLOCK_SIZE)) == NULL) {
			printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__,
					__LINE__);
			return -1;
		}
		if (key_len < 16) {
			unsigned char buf[16];
			int i;
			for(i = 0; i < key_len; i++) buf[i] = key[i]; 
			for(i = key_len; i < 16; i++) buf[i] = 0; 
			aes_xcbc_prf_128(buf, 16, text, text_len, *digest);
		} else if (key_len > 16) {
			aes_xcbc_prf_128(key, 16, text, text_len, *digest);
		} else {
			aes_xcbc_prf_128(key, 16, text, text_len, *digest);
		}
		return AES_BLOCK_SIZE;
		break;

	case IKEV2_PRF_HMAC_TIGER:
	default:
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	LOG_FUNC_END(1);
}

/**
 * Implementation of pseudo random function for longer results (prf+)
 *
 * @param prf_alg	Type of pseudo random function (see IKEV2 specification)
 * @param key		Key for hmac
 * @param klen		Length of key
 * @param text		Data to be DONT_FORCE
 * @param tlen		Length of text
 * @param res		Result
 * @param rlen		Result length
 *
 * return -1 in case of an error, 0 in case of success
 */
int prf_plus(transform_t *prf_alg, char *key, guint32 klen, char *text,
		guint32 tlen, char **res, guint32 rlen)
{
	guint32 dummy, ds, nr, i;
	char *buf, *dbuf;

	LOG_FUNC_START(1);
	LOG_TRACE("keylen=%d", klen);

	if (!(ds = transform_prf_get_digest_len(prf_alg))) {
		*res = NULL;
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	/*
	 * Number of parts, i.e. how many time should prf function be called.
	 */
	nr = (rlen + ds - 1) / ds;

	if ((*res = g_malloc0(nr * ds)) == NULL) {
		/*
		 * TODO: Log error! Error recovery!
		 */
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	if ((buf = g_malloc0(ds + tlen + 1)) == NULL) {
		/*
		 * TODO: Log error! Error recovery!
		 */
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	memcpy(buf, text, tlen);
	buf[tlen] = 1;
	dummy = prf(prf_alg, (guchar *)key, klen, (guchar *)buf,
			tlen + 1, &dbuf);

	memcpy(*res, dbuf, ds);
	g_free(dbuf);

	for(i = 2; i <= nr; i++) {
		memcpy(buf, *res + (i - 2) * ds, ds);
		memcpy(buf + ds, text, tlen);
		buf[ds + tlen] = (guint8)i;
		dummy = prf(prf_alg, (guchar *)key, klen, (guchar *)buf,
				ds + tlen + 1, &dbuf);
		memcpy(*res + (i - 1) * ds, dbuf, ds);
		g_free(dbuf);
	}

	g_free(buf);

	LOG_FUNC_END(1);

	return 0;
}

guint16 prf_has_fixed_key_length(transform_t *transform)
{

	LOG_FUNC_START(1);

	switch(transform_get_id(transform)) {
	case IKEV2_PRF_HMAC_MD5:
	case IKEV2_PRF_HMAC_SHA1:
		return 0;

	case IKEV2_PRF_AES128_CBC:
		return 1;

	default:
		/*
		* This option is executed only when someone has forgotten to
		* adjust this switch statement - so in a warning will be issued. 
		*/
		LOG_DEBUG("Unknown encryption algorithm! Returned zero.");
		return 0;
	}

	LOG_FUNC_END(1);
}

/**
 * Function that returns length of digest for a given algorithm
 *
 * @param type	Algorithm type
 *
 * \return Digest length, or -1 if unsupported digest.
 */
guint32 prf_digest_length_get(guint32 type)
{

	LOG_FUNC_START(1);

	switch(type) {
	case IKEV2_PRF_HMAC_MD5:
		return MD5_DIGEST_SIZE;

	case IKEV2_PRF_HMAC_SHA1:
		return SHA1_DIGEST_SIZE;

	case IKEV2_PRF_AES128_CBC:
		return 12;

	default:
		/*
		* This option is executed only when someone has forgotten to
		* adjust this switch statement - so in a warning will be issued. 
		*/
		LOG_DEBUG("Unknown encryption algorithm! Returned zero.");
		return 0;
	}

	LOG_FUNC_END(1);
}

/**
 * Return the key len for given crypto algorithm
 *
 * @param transform	Type of crypto algo
 *
 * \return Key length or -1 in case of unsupported encryption algorithm
 *
 * TODO: This should move to transform.c
 */
guint32 encr_keylen_get(transform_t *transform)
{
	guint16 keylen;

	LOG_FUNC_START(1);

	/*
	 * Should this be assert? If transform is NULL than it's
	 * internal error to daemon...
	 */
	if (!transform) {
		LOG_DEBUG("Transform is NULL!");
		return 0;
	}

	g_assert(transform->type == IKEV2_TRANSFORM_ENCR);

	keylen = transform_get_key_len(transform);
	if(keylen) return keylen;

	switch(transform->id) {
	case IKEV2_ENCR_NULL:
		return 0;
	case IKEV2_ENCR_DES:
		/*
		 * Including parity bits!!
		 */
		return 8;

	case IKEV2_ENCR_3DES:
		/*
		 * Including parity bits!!
		 */
		return 24;

	case IKEV2_ENCR_AES_CBC:
		/*
		 * Use "default" value!!
		 */
		return 16;

	case IKEV2_ENCR_AES_CTR:
		/*
		 * Use "default" value!!
		 */
		return 16;

	default:
		/*
		* This option is executed only when someone has forgotten to adjust this switch
		* statement - so in a warning will be issued. 
		*/
		LOG_DEBUG("Unknown encryption algorithm! Returned zero.");
		return 0;
	}
}

/**
 * Return the block len of a given crypto algorithm
 *
 * @param encr_type		type of crypto algo
 *
 * \return
 */
guint32 encr_blocklen_get(transform_t *encr_type)
{

	LOG_FUNC_START(1);

	/*
	 * assert maybe? type is under assert!
	 */
	if (!encr_type) {
		LOG_DEBUG("Transform is NULL!");
		return 0;
	}

	g_assert(encr_type->type == IKEV2_TRANSFORM_ENCR);

	switch(encr_type->id) {
	case IKEV2_ENCR_NULL:
		return 1;
	case IKEV2_ENCR_DES:
	case IKEV2_ENCR_3DES:
		return 8;
	case IKEV2_ENCR_AES_CBC:
	case IKEV2_ENCR_AES_CTR:
		return AES_BLOCK_SIZE;
	default:
		/*
		* This option is executed only when someone has forgotten to adjust this switch
		* statement - so in a warning will be issued. 
		*/
		LOG_DEBUG("Unknown encryption algorithm!");
		return 0;
	}
}

/**
 * Build a valid key (e.g. parity bits for DES)
 *
 * @param encr_type	type of crypto algo
 * @param key		ptr to key
 */
void encr_build_key(transform_t *encr_type, char *key)
{

	LOG_FUNC_START(1);

	g_assert(encr_type->type == IKEV2_TRANSFORM_ENCR);

	switch(encr_type->id) {
	case IKEV2_ENCR_DES:
		DES_set_odd_parity((des_cblock *)key);
		break;

	case IKEV2_ENCR_3DES:
		DES_set_odd_parity((des_cblock *)key);
		DES_set_odd_parity((des_cblock *)(key + 8));
		DES_set_odd_parity((des_cblock *)(key + 16));
		break;

	case IKEV2_ENCR_NULL:
	case IKEV2_ENCR_AES_CBC:
	case IKEV2_ENCR_AES_CTR:
		/*
		* This is here to catch algorithms that dont need any key preparation.
		* This is different then "default" option, for "default" option is
		* executed only when someone has forgotten to adjust this switch
		* statement - so in that case warning will be issued. 
		*/
		break;

	default:
		LOG_DEBUG("Unknown encryption algorithm!");
		break;
	}
}

/**
 * Encrypt/decrypt data with 3DES in ECB mode
 *
 * @param data	Pointer to buffer with plain text to be encrypted, or
 *		cypertext to be decrypted.
 * @param dlen	Buffer length (has to be multiple of 8)
 * @param iv	Init vector
 * @param key	Pointer to a key (has to be 24 byte = 3 x 64bit)
 * @param enc	Encrypt/Decrypt
 *
 */
void xcrypt_3des(char *data, guint32 dlen, DES_cblock *iv, char *key, int enc)
{
	/*guint32 i;*/
	DES_key_schedule k1, k2, k3;
	DES_cblock liv;

	LOG_FUNC_START(1);

	LOG_DEBUG("data=%p, dlen=%d, iv=%p,key=%p, enc=%d\n",
			data, dlen, iv, key, enc);

	des_key_sched((des_cblock *)key, k1);
	des_key_sched((des_cblock *)(key + 8), k2);
	des_key_sched((des_cblock *)(key + 16), k3);

	LOG_DEBUG("initialization vector");
	log_data(LOGGERNAME, (char *)iv, sizeof(DES_cblock));

	/*
	 * Because DES_ede3_cbc_encrypt changes initialization
	 * vector, we are making a copy and then pass a copy to
	 * the function.
	 */
	memcpy(&liv, iv, sizeof(DES_cblock));

	DES_ede3_cbc_encrypt((guchar *)data, (guchar *)data, dlen, &k1, &k2,
			&k3, &liv, enc);

	LOG_FUNC_END(1);
}

/**
 * Encrypt/decrypt data with DES in ECB mode
 *
 * @param data	Pointer to buffer with plain text to be encrypted, or
 *		cypertext to be decrypted.
 * @param dlen	Buffer length (has to be multiple of 8)
 * @param iv	Init vector
 * @param key	Pointer to a key (has to be 8 byte = 64bit)
 * @param enc	Encrypt (if enc!=0)/Decrypt (if enc==0)
 */
void xcrypt_des(char *data, guint32 dlen, DES_cblock *iv, char *key, int enc)
{
	DES_key_schedule k1;
	DES_cblock liv;

	LOG_FUNC_START(1);

	LOG_DEBUG("data=%p, dlen=%d, iv=%p, key=%p, enc=%d\n",
			data, dlen, iv, key, enc);

	des_key_sched((des_cblock *)key, k1);

	LOG_DEBUG("initialization vector");
	log_data(LOGGERNAME, (char *)iv, sizeof(DES_cblock));

	/*
	 * DES_cbc_encrypt does not change initialization
	 * vector. However, for now, we are making a copy
	 * and then pass a copy to the function.
	 */
	memcpy(&liv, iv, sizeof(DES_cblock));

	DES_cbc_encrypt((guchar *)data, (guchar *)data, dlen, &k1, &liv, enc);

	LOG_FUNC_END(1);
}

/**
 * Encrypt/decrypt data with AES in CBC mode
 *
 * @param data	 Pointer to buffer with plain text to be encrypted, or
 *		 cypertext to be decrypted.
 * @param dlen	 Buffer length
 * @param iv	 Init vector
 * @param key	 Pointer to a key (has to be 8 byte = 64bit)
 * @param enc	 Encrypt (if enc!=0)/Decrypt (if enc==0)
 * @param keylen Length of key - in bits. Can be one of 128, 192, 256.
 */
void xcrypt_aes_cbc(char *data, guint32 dlen, unsigned char *iv,
		unsigned char *key, int enc, guint32 keylen)
{
	AES_KEY aes_key;
	unsigned char liv[AES_BLOCK_SIZE];

	LOG_FUNC_START(1);

	LOG_DEBUG("data=%p, dlen=%d, iv=%p, key=%p, enc=%d\n",
			data, dlen, iv, key, enc);

	g_assert( enc==AES_ENCRYPT || enc==AES_DECRYPT );

	if (enc == AES_ENCRYPT) {
		AES_set_encrypt_key(key, keylen, &aes_key);
	} else if (enc == AES_DECRYPT) {
		AES_set_decrypt_key(key, keylen, &aes_key);
	} else {
		LOG_BUG("Unknown transform requested (%u)", enc);
		return;
	}

	LOG_DEBUG("initialization vector");
	log_data(LOGGERNAME, (char *)iv, AES_BLOCK_SIZE);

	/*
	 * AES_cbc_encrypt changes initialization vector, so will make a copy.
	 */
	memcpy(&liv[0], iv, AES_BLOCK_SIZE);

	AES_cbc_encrypt((guchar *)data, (guchar *)data, dlen, &aes_key,
			&liv[0], enc);

	LOG_FUNC_END(1);
}

/**
 * Encrypt/decrypt data with AES in CTR mode
 *
 * @param data	 Pointer to buffer with plain text to be encrypted, or
 *		 cypertext to be decrypted.
 * @param dlen	 Buffer length
 * @param iv	 Init vector
 * @param key	 Pointer to a key (has to be 8 byte = 64bit)
 * @param enc	 Encrypt (if enc!=0)/Decrypt (if enc==0)
 * @param keylen Length of key - in bits. Can be one of 128, 192, 256.
 */
void xcrypt_aes_ctr128(char *data, guint32 dlen, unsigned char *iv,
		unsigned char *key, int enc, guint32 keylen)
{
	AES_KEY aes_key;
	unsigned char liv[AES_BLOCK_SIZE];
	unsigned char ecounter[16];
	unsigned int num = 0;

	LOG_FUNC_START(1);

	LOG_DEBUG("data=%p, dlen=%d, iv=%p, key=%p, enc=%d\n",
			data, dlen, iv, key, enc);

	LOG_TRACE("Initialization vector");
	log_data(LOGGERNAME, (char *)iv, AES_BLOCK_SIZE);

	LOG_TRACE("Key");
	log_data(LOGGERNAME, (char *)key, keylen / 8);

	g_assert (enc==AES_ENCRYPT || enc==AES_DECRYPT);

	AES_set_encrypt_key(key, keylen, &aes_key);

	/*
	 * AES_cbc_encrypt changes initialization
	 * vector, so will make a copy.
	 */
	memcpy(&liv[0], iv, AES_BLOCK_SIZE);

	memset(&ecounter[0], 0, 16);
	AES_ctr128_encrypt((guchar *)data, (guchar *)data, dlen, &aes_key,
				&liv[0], &ecounter[0], &num);

	LOG_FUNC_END(1);
}

/**
 * Dispatcher for encryption of  a block of data with given algorithm. Length
 * of data to be encrypted should be a multiple of block len
 *
 * @param data		Pointer to plain text data. Place where cypher text is
 *			placed on exit from function.
 * @param dlen		Number of octets of plain text data
 * @param encr_type	Type of crypto algorithm to use
 * @param iv		Initialization vector
 * @param key		Key for cipher
 * @param keylen	Keylen parameter (optional)
 *
 */
void crypto_encrypt(char *data, guint32 dlen, transform_t *encr_type, char *iv,
		char *key, guint32 keylen)
{

	LOG_FUNC_START(1);

	g_assert(encr_type->type == IKEV2_TRANSFORM_ENCR);

	LOG_DEBUG("DATA BUFFER BEFORE ENCRYPTION");
	log_buffer(LOGGERNAME, data, dlen);

	switch(encr_type->id) {
	case IKEV2_ENCR_NULL:
		/* Do nothing here... */
		break;

	case IKEV2_ENCR_AES_CBC:
		/* Assuming that keylen parameter above is in
		 * byte-s, will multiply it with 8
		 */
		xcrypt_aes_cbc(data, dlen, (unsigned char *)iv, (guchar *)key,
				AES_ENCRYPT, keylen*8);
		break;

	case IKEV2_ENCR_AES_CTR:
		/* Assuming that keylen parameter above is in
		 * byte-s, will multiply it with 8
		 */
		xcrypt_aes_ctr128(data, dlen, (unsigned char *)iv,
				(guchar *)key, AES_ENCRYPT, keylen * 8);
		break;

	case IKEV2_ENCR_DES:
		xcrypt_des(data, dlen, (DES_cblock *)iv, key, DES_ENCRYPT);
		break;

	case IKEV2_ENCR_3DES:
		xcrypt_3des(data, dlen, (DES_cblock *)iv, key, DES_ENCRYPT);
		break;

	default:
		/*
		 * TODO: Log an error! To report error back to callee?!
		 *
		 * This option is executed only when someone has forgotten to
		 * adjust this switch statement - so in a warning will be
		 * issued.
		 */
		LOG_DEBUG("Unknown encryption algorithm!");
		break;
	}

	LOG_DEBUG("DATA BUFFER AFTER ENCRYPTION");
	log_buffer(LOGGERNAME, data, dlen);

	LOG_FUNC_END(1);
}

/**
 * Decrypt a block of data (length should be a multiple of block len)
 *
 * @param data		Pointer to source buffer. Destionation buffer with
 *			decrypted cypher text.
 * @param dlen		Number of octets for data
 * @param encr_type	Type of crypto algorithm
 * @param iv
 * @param key		Key to use for deciphering
 * @param keylen	Keylen parameter (optional)
 *
 * TODO: Rearange parameters!! Introduce 'struct transform' instead of
 *	 separate arguments type and keylen.
 */
void crypto_decrypt(char *data, guint32 dlen, transform_t *encr_type, char *iv,
		char *key, guint32 keylen)
{

	LOG_FUNC_START(1);

	g_assert(encr_type->type == IKEV2_TRANSFORM_ENCR);

	LOG_DEBUG("DATA BUFFER BEFORE DECRYPTION");
	log_buffer(LOGGERNAME, data, dlen);

	switch(encr_type->id) {
	case IKEV2_ENCR_NULL:
		/* Do some hevy work here - like nothing... */
		break;

	case IKEV2_ENCR_AES_CBC:
		/* Assuming that keylen parameter above is in
		 * byte-s, will multiply it with 8
		 */
		xcrypt_aes_cbc(data, dlen, (unsigned char *)iv, (guchar *)key,
				AES_DECRYPT, keylen*8);
		break;

	case IKEV2_ENCR_AES_CTR:
		/* Assuming that keylen parameter above is in byte-s,
		 * will multiply it with 8
		 */
		xcrypt_aes_ctr128(data, dlen, (unsigned char *)iv,
				(guchar *)key, AES_DECRYPT, keylen * 8);
		break;

	case IKEV2_ENCR_DES:
		xcrypt_des(data, dlen, (DES_cblock *)iv, key, DES_DECRYPT);
		break;

	case IKEV2_ENCR_3DES:
		xcrypt_3des(data, dlen, (DES_cblock *)iv, key, DES_DECRYPT);
		break;

	default:
		/*
		 * TODO: Log an error. Not implemented transform. It should also
		 * notify callee!!!
		 */
		/*
		 * This option is executed only when someone has forgotten to
		 * adjust this switch statement - so in a warning will be issued. 
		 */
		LOG_DEBUG("Unknown encryption algorithm!");
		break;
	}

	LOG_DEBUG("DATA BUFFER AFTER DECRYPTION");
	log_buffer(LOGGERNAME, data, dlen);

	LOG_FUNC_END(1);
}

/**
 * Return the key length of a given integ algo
 *
 * @param transform	type of algo
 *
 * \return Size of integrity algorithm output
 *
 * TODO: This function is probably redundant since in each transform
 * there is a field key_len. So, this function should take an transform
 * structure as input and return key_len from that structure.
 */
guint32 integ_keylen_get(transform_t *transform)
{

	LOG_FUNC_START(1);

	g_assert(transform->type == IKEV2_TRANSFORM_INTEGRITY);

	switch (transform->id) {
	case IKEV2_HMAC_MD5_96:
		LOG_TRACE("md5 - 16");
		return 16;

	case IKEV2_HMAC_SHA1_96:
		LOG_TRACE("sha1 - 20");
		return 20;

	case IKEV2_AES_XCBC_96:
		LOG_TRACE("aes - 16");
		return 16;

	}

	LOG_TRACE("Invalid algorithm!");
	LOG_FUNC_END(1);

	return 0;
}

/**
 * Return the checksum length of a given integrity algorithm
 *
 * @param transform	Algoritym type
 */
guint32 integ_chksumlen_get(transform_t *transform)
{

	LOG_FUNC_START(1);
	g_assert(transform->type == IKEV2_TRANSFORM_INTEGRITY);

	switch(transform->id) {
	case IKEV2_HMAC_MD5_96:
		LOG_TRACE("md5 - 12");
		return 12;

	case IKEV2_HMAC_SHA1_96:
		LOG_TRACE("sha1 - 12");
		return 12;

	case IKEV2_AES_XCBC_96:
		LOG_TRACE("aes - 12");
		return 12;

	}

	return 0;
}

/**
 * Implementation of integrity algorithm
 *
 * @param integ_type	Type of integ algo (see IKEV2 specification)
 * @param key		Key for algorithm
 * @param klen		Length of key
 * @param text		Data to be integrity checked
 * @param tlen		Length of text
 * @param res		Result
 *
 * \return Length of result, or -1 in case of an error.
 *
 * TODO: Instead of integtype pass 'struct transform'.
 */
int integ(transform_t *integ_type, char *key, guint32 klen, char *text,
		guint32 tlen, char **res)
{
	char *buf;
	int rlen;

	LOG_FUNC_START(1);

	LOG_DEBUG("integ_type=%p, key=%p, klen=%d, text=%p, tlen=%d",
			integ_type, key, klen, text, tlen);

	g_assert(integ_type->type == IKEV2_TRANSFORM_INTEGRITY);

	LOG_DEBUG("integrity key");
	log_data(LOGGERNAME, key, klen);
	LOG_DEBUG("text to be integrity protected");
	log_buffer(LOGGERNAME, text, tlen);

	*res = NULL;
	rlen = -1;

	switch (integ_type->id) {
	case IKEV2_HMAC_MD5_96:
		if (klen != 16) {
			LOG_ERROR("Wrong key length");
			break;
		}

		if (!(buf = g_malloc0(MD5_DIGEST_SIZE))) {
			LOG_ERROR("Out of memory");
			break;
		}

		rlen = 12;
		prf_hmac_md5((guchar *)key, klen, (guchar *)text, tlen, buf);
		*res = g_memdup(buf, 12);
		g_free(buf);

		break;

	case IKEV2_HMAC_SHA1_96:
		if (klen != 20) {
			LOG_ERROR("Wrong key length");
			break;
		}

		if (!(buf = g_malloc0(SHA1_DIGEST_SIZE))) {
			LOG_ERROR("Out of memory");
			break;
		}
		rlen = 12;
		prf_hmac_sha1((guchar *)key, klen, (guchar *)text, tlen, buf);

		*res = g_memdup(buf, 12);
		g_free(buf);
		break;

	case IKEV2_AES_XCBC_96:
		if (klen != 16) {
			LOG_ERROR("Wrong key length");
			break;
		}

		if (!(buf = g_malloc0(12))) {
			LOG_ERROR("Out of memory");
			break;
		}
		rlen = 12;
		aes_xcbc_mac_96((guchar *)key, klen, (guchar *)text, tlen, buf);

		*res = g_memdup(buf, 12);
		g_free(buf);
		break;

	default:
		/*
		 * This is error situation, should it return -1?
		 */
		LOG_ERROR("Unknown transform!");
		break;
	}

	if (rlen > 0) {
		LOG_DEBUG("hmac");
		log_data(LOGGERNAME, *res, rlen);
	}

	LOG_FUNC_END(1);

	return rlen;
}

/*******************************************************************************
 * BASIC FUNCTIONS FOR INITIALIZING CRYPTO SUBSYSTEM
 ******************************************************************************/

void crypto_unload()
{
	LOG_FUNC_START(1);

	LOG_FUNC_END(1);
}

int crypto_init(const char *randdev)
{
	FILE *fin;
	char buffer[RAND_INIT_SIZE];
	size_t bytes;

	LOG_FUNC_START(1);

	if ((fin = fopen(randdev, "r")) == NULL) {
		/*
		 * TODO: Log error!
		 */
		LOG_ERROR("Unable to open radnom device %s", randdev);
		return -1;
	}

	bytes = fread(buffer, sizeof(char), RAND_INIT_SIZE, fin);

	fclose (fin);

	if (bytes != RAND_INIT_SIZE) {
		/*
		 * TODO: Log error!
		 */
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return -1;
	}

	RAND_seed(buffer, bytes);

	LOG_FUNC_END(1);

	return 0;
}
