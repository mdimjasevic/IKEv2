/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SESSION_H
#define __SESSION_H

/**
 * Session types
 */
#define IKEV2_INITIATOR		1
#define IKEV2_RESPONDER		2

/**
 * Session states
 */

#define IKE_SM_CREATED		0	/* Just created session structure */

/**
 * States for a top level initiator state machine...
 */
#define IKE_SMI_INIT		1	/* Send IKE SA INIT message */
#define IKE_SMI_INVALIDKE	2	/* Send IKE SA INIT with different KE */
#define IKE_SMI_COOKIE		3	/* Send IKE SA INIT with COOKIE */
#define IKE_SMI_AUTH		4	/* Send IKE AUTH */
#define IKE_SMI_AUTH_PEER	5	/* Received IKE AUTH resp. */
					/* If EAP is used go to IKE_SMI_EAP */
#define IKE_SMI_INSTALLCSA	6	/* calculate crypto material */
					/* and install CSAs into kernel */
#define IKE_SMI_EAP		7	/* State for EAP authentication */ 
#define IKE_SMI_AUTH_DL		8	/* Waiting for cert download */
#define IKE_SMI_INSTALLCSA_DL	9	/* Waiting for cert download */

/**
 * States for a top level responder state machine...
 */

/**
 * Waiting for IKE SA
 */
#define IKE_SMR_INIT			100

/**
 * Waiting for IKE AUTH
 */
#define IKE_SMR_AUTH			101

#define IKE_SMR_AUTH_RESPONSE		102

/*
 * First state if EAP is required
 */
#define IKE_SMR_EAP_INITIATOR_REQUEST	103

/**
 * Waiting for IKE AUTH EAP
 */
#define IKE_SMR_EAP_AAA_REQUEST		104

/**
 * If success message is received
 */
#define IKE_SMR_AUTH_FINALIZE		105

#define IKE_SMR_CFG_WAIT		109

#define IKE_SMR_AUTH_DL_PEER		110	/* Waiting for cert download */
#define IKE_SMR_AUTH_DL_HOST		111	/* Waiting for cert download */

/**
 * States common to both roles
 */
#define IKE_SM_MATURE		210	/* IKE SA can be used		  */
#define IKE_SM_REKEY		220 	/* IKE SA is in a process of	  */
					/* rekeying			  */
#define IKE_SM_REKEY_RESPONSE	221 	/* We received response to rekey  */
					/* request			  */
#define IKE_SM_REAUTH		231 	/* IKE SA is beeing replaced with */
					/* new IKE SA                     */
#define IKE_SM_DYING		240	/* Session has to be removed      */
#define IKE_SM_CRL_UPDATE	241	/* CRL needs updating             */

/**
 * Maximum number of half opened sessions comming from one IP address
 */
#define MAX_HALF_OPENED_SESSIONS	2

/**
 * Structure for holding local and remote addresses making pair of addresses
 * in a IP addresses state machine.
 */
struct pair_of_addresses {
	/**
	 * One of initiator's and one of responder's addresses.
	 *
	 * Those addresses also hold ports!
	 */
	struct netaddr *i_addr, *r_addr;

	/**
	 * State of address pair in IP Address Pair State Machine.
	 *
	 * Possible states are:
	 *
	 * ADDRESS_PAIR_INVALID			0
	 * ADDRESS_PAIR_VALID			1
	 * ADDRESS_PAIR_NEGOTIATING		2
	 * ADDRESS_PAIR_ACTIVE			3
	 * ADDRESS_PAIR_TRANSITING		4
	 * ADDRESS_PAIR_INEXISTING		5
	 *
	 * Default state is ADDRESS_PAIR_INVALID.
	 */
	guint8 state;

	/**
	 * These are the time fields for return routability check timeout.
	 */
	GTimeVal rrc_timeout;
	timeout_t *to;
};

/**
 * In the following structure all the fields marked as transient are
 * valid only throughout some particular lifetime of IKE SA, e.g.
 * during cration, rekeying, reautentification, etc.
 */
struct session {
	/**
	 * Mutex which has to be acquired before anything is changed
	 * in the structure
	 */
	GMutex *mutex;

	/**
	 * Reference counter counting of users of the session structure
	 */
	gint refcnt;

	/**
	 * ike_type defines if the particular structure belongs to initiator
	 * (IKEV2_INITIATOR) or to responder (IKEV2_RESPONDER)
	 */
	guint8 ike_type;

	/**
	 * Queue for messages for this IKE SA
	 *
	 * This queue has to be locked before putting message into it
	 * because it might happen that we push a message here, immediatelly
	 * after this queue is checked for any outstanding messages. The
	 * result is that the message will not be processed immediatelly,
	 * but will wait for the next message to trigger it's processing.
	 */
	GAsyncQueue *msg_queue;

	/**
	 * State of a session
	 */
	guint8 state;

	/**
	 * Do we need to remove session as soon as possible?
	 */
	gboolean shutdown;

	/**
	 * Is this session initiated on explicit user request?
	 * This is used in order to terminate ikev2 in case
	 * session establishment doesn't succeed.
	 */
	gboolean user_initiated;

	/**
	 * Data for responder to keep track of unfinished IKE SAs
	 */
	struct {
		GTimeVal tv;
		timeout_t *to;
		guint8 retries;
	} responder_wait;

	/**
	 * Pointer to configuration data for IKE SA exchange
	 */
	struct config_remote *cr;

	/**
	 * Pointer to configuration data for IKE AUTH exchange
	 */
	struct config_peer *cp;

	/**
	 * Pointer to configuration data for IKE AUTH exchange
	 */
	struct config_general *cg;

	/**
	 * Timestamp when this IKE SA has been created
	 */
	guint32 create_time;

	/**
	 * Maximum time that IKE SA of this session can be extended
	 * (in seconds relative to 'create_time')
	 */
	guint32 auth_limit_time;

	/**
	 * Timestamp when this IKE SA has last been rekeyed
	 *
	 * When rekeying of IKE SA starts, this field is set to zero
	 */
	guint32 rekey_time;

	/**
	 * Number of octets transfered through this IKE SA
	 */
	guint64 octets_sendrcv;

	/**
	 * Number of allocations this IKE SA had
	 */
	guint32 allocations;

	/**
	 * Last time (in Unix time) we received something from peer
	 */
	guint32 last_seen;

	/**
	 * List of all CHILD SAs that belong to this particular
	 * IKE SA
	 *
	 * Do not add or remove from this list unless session structure
	 * is locked!
	 */
	GSList *sad;

	/**
	 * SPI values for IKE SA and for a session. These are unique
	 * per session...
	 */
	guint64 i_spi;
	guint64 r_spi;

	/**
	 * Digest of a first message...
	 *
	 * Transient field...
	 */
	char *digest;

	/**
	 * Window size for incoming requests. Note that during
	 * IKE INIT and IKE AUTH those are always 1!
	 */

	/**
	 * List of sent_msg structures, sent but not yet acknowledged
	 * request messages. Mutex is for protecting modifications done
	 * on a list.
	 *
	 * TODO: With the following fields we are actually introducing 
	 * data from messaging subsystem into session structure. It is
	 * for reconsideration!
	 *
	 * One possibility is to keep sent structures in two lists. One
	 * in session structure, and the other in message structure.
	 *
	 * sent_req list has to be ordered by msg_id field in ascending
	 * order.
	 */
	GMutex *sent_req_mux;
	GSList *sent_req;

	/**
	 * The following variables are used to track allowable msg
	 * id's of request sent to peer, and received responses
	 * on local daemon....
	 *
	 * These are protected by sent_req_mux
	 */
	guint16 wsize;		/* Window size, initially it's set to 1 */
	guint32 msgid_hi;	/* First unsent.... */
	guint32 msgid_lo;	/* First sent and unacknowledged ... */

	/**
	 * List of sent_msg structures, sent but not yet acknowledged
	 * response messages. Mutex is for protecting modifications done on
	 * a list.
	 *
	 * sent_resp list has to be ordered by msg_id field in ascending
	 * order.
	 *
	 * TODO: Note that adding new element into list is not efficient
	 * since we have to traverse the whole list to acquire last element!
	 * Hopefully, this list will be short!
	 */
	GMutex *sent_resp_mux;
	GSList *sent_resp;

	/**
	 * peer_msgid_lo is the highest unsent reponse in any given moment
	 * These are also protected by sent_resp_mux.
	 */
	guint16 peer_wsize;	/* Initially one */
	guint32 peer_msgid_lo;	/* Lowest request received but not responded
				 * yet */
	guint32 peer_msgid_hi;	/* Highest request received so far, but not
				 * responded yet */

	/**
	 * Initiator's and responder's addresses for this session.
	 *
	 * Those addresses also hold ports!
	 */
	struct netaddr *i_addr, *r_addr;

	/**
	 * List of remote peer's IPv4 and IPv6 addresses
	 */
	GSList *peer_ipv4_addresses;
	GSList *peer_ipv6_addresses;

	/**
	 * List of pair of addresses, their states and timeouts in
	 * IP address state machine in MOBIKE.
	 * One pair consists of initiator's address and of respodnder's address.
	 */
	GSList *pair_of_addresses_list;
	/**
	 * Pointer to active address from pair_of_addresses_list list
	 * TODO: This variable should replace i_addr and r_addr variables.
	 */
	GSList *active_address_pair;

#ifdef SUPPLICANT
	/**
	 * Pointer to a structure with EAP states 
	 * used during EAP authentication 
	 */
	supplicant_t *sd;
#endif /* SUPPLICANT */ 

	/**
	 * Nonce values
	 *
	 * Transient field...
	 */
	BIGNUM *i_nonce;
	BIGNUM *r_nonce;

	/**
	 * Identity sent by peer...
	 */
	struct id *peer_id;

	/**
	 * DH key data.
	 *
	 * Transient field...
	 */
	DH *dh;
	BIGNUM *pub_key;
	unsigned char *dh_shared_key;
	int dh_shared_key_len;

	/**
	 * Selected proposal for this peer. Note that this field points
	 * to the same area of memory like tranform_t pointers.
	 */
	proposal_t *selected_proposal;

	/**
	 * Transforms selected for this IKE SA
	 */
	transform_t *prf_type;
	transform_t *encr_type;
	transform_t *integ_type;	/* Aka integrity type */
	transform_t *dh_group;

	/**
	 * Session keys. d is a master key, a stands for authentication, and
	 * e for encription. Keys denoted by i belong to initiator, while
	 * those denoted by r belong to responder.
	 */
	char *sk_d;
	char *sk_ai;
	char *sk_ar;
	char *sk_ei;
	char *sk_er;
	char *sk_pi;
	char *sk_pr;

	/**
	 * Authentication data. id and id_len are contained in config ikesa
	 * structure, and auth_data containts additional authentication data.
	 * That structure is incomplete until proposal negotiations...
	 *
	 * Transient field...
	 */
	struct auth_data *auth_data;

	/**
	 * List of certificates that should be sent to a peer in CERT payload
	 * List elements are of type struct cert_item
	 */
	GSList *certs_for_peer;

	/**
	 * List of peer's locally stored and verified certificates
	 * List elements are of type struct cert_item
	 */
	GSList *peer_local_certs;

	/**
	 * List of peer's verified certificates from CERT payloads
	 * List elements are of type struct cert
	 */
	GSList *peer_payload_certs;

	/**
	 * List of CA's used with successfuly verified certs in this session
	 * (subset of 'cg->ca_list' and 'cp->ca_list')
	 */
	GSList *used_cas;

	/**
	 * Closest time any of CRLs this session uses will need updating
	 */
	time_t next_crl_update;

	/**
	 * List of CA's whose CRL needs updating at 'next_crl_update'
	 * (subset of 'used_cas')
	 */
	GSList *crl_update_cas;
	
	/**
	 * If peer supports HTTP_CERT_LOOKUP_SUPPORTED
	 */
	gboolean cert_lookup_supported;

	/**
	 * Variables necessary for NAT-T implementation
	 *
	 * natt		If UDP encapsulation should be enabled. This also
	 *		enables sending on high port.
	 *
	 * In case of initiator, source and destination port are always
	 * equal to 4500. For responder, if initiator is behind a NAT
	 * then it's allowed to change port (and in that case change_peer_port
	 * is set to TRUE), otherwise, it's not!
	 *
	 * The variable natt is purposely left compiled in since we are
	 * using this variable as an input parameter to some functions, so
	 * it's much easier to do it this way that to clutter code with
	 * huge number of #ifdef's
	 */
	gboolean natt;
#ifdef NATT
	gboolean change_peer_port;

	/**
	 * When keepalive packets are sent, timeout is registered
	 * and it should be canceled when session is removed. This
	 * variable keeps record about that.
	 */
	gboolean natt_keepalive;
	struct timeout_t *keepalive_timeout;

	/*
	 * Pointer to hash list of source IP addresses.
	 * Used for NAT support.
	 */
	GSList *nat_src_hashes;

	/*
	 * Pointer to hash of destination IP address.
	 * Used for NAT support.
	 */
	gpointer *nat_dest_hashes;
#endif /* NATT */

	/*
	 * FIXME
	 */
	/**
	 * Variables needed for MOBIKE implementation
	 *
	 * mobike_supported		If MOBIKE is to be used in this session.
	 * If configuration data enables MOBIKE, this variable is set
	 * to true during initialization of session. When sending IKE_AUTH
	 * message MOBIKE_SUPPORTED notify payload is included if this
	 * variable is set to true.
	 * Upon receiving IKE_AUTH message, peer checks if other peer supports
	 * MOBIKE. If not, this variable is set to false.
	 */
#ifdef MOBIKE
	gboolean mobike_supported;

	/**
	 * Counter for IKE AUTH exchanges. It serves for determining if
	 * particular exchange contains SA payload, hence 1st IKE AUTH
	 * exchange.
	 */
	guint8 ike_auth_exchange_counter;

	/*
	 * Lists of local IP addresses initialized in network_init() function.
	 * Temporary pointers are needed for ADDITIONAL_*_ADDRESS notify
	 * payloads in IKE AUTH and INFORMATIONAL exchanges.
	 */
	GSList *local_ipv4_addresses;
	GSList *local_ipv6_addresses;

	GSList *local_ipv4_addresses_ptr;
	GSList *local_ipv6_addresses_ptr;

	/**
	 * FIXME
	 * If we are to switch to MOBIKE port (4500) to send IKE_AUTH and
	 * it turns out that remote peer does not support MOBIKE for this
	 * connection, we need to switch to ports set previously. That
	 * could be IKEV2_PORT_NATT (4500), default port (500) as well as
	 * any other set by any party
	 */
	 guint16 previous_port_i;
	 guint16 previous_port_r;
#endif /* MOBIKE */

#ifdef CFG_CLIENT
	/**
	 * Configuration state on initiator
	 *
	 * The value GUINT_TO_POINTER(1) in this field indicates
	 * an ERROR condition.
	 */
	struct cfg *icfg;
#endif /* CFG_CLIENT */

#ifdef CFG_MODULE
	/**
	 * Configuration state on responder.
	 *
	 * If the stored value is (1) then an error occured. If the
	 * value is NULL then the client didn't request configuration
	 * parameters.
	 *
	 * The value GUINT_TO_POINTER(1) in this field indicates
	 * an ERROR condition.
	 *
	 * This is a structure of cfg_data defined in cfg_module
	 */
	gpointer rcfg;

	/**
	 * Time after which lease expires and all the CHILD SAs
	 * should be deleted. It is the responsibility of Initiator
	 * to either renew lease before this time expires, or to
	 * request new configuration parameters after this time
	 * expires.
	 */
	guint32 lease_time;
#endif /* CFG_MODULE */

	/**
	 * Pointer to a session structure we are replacing during
	 * reauthenticifation
	 */
	struct session *session;

#ifdef AAA_CLIENT
	/**
	 * AAA data (responder only)
	 */
	void *aaa_data;
	void *eap_packet;
	int eap_packet_len;
#endif /* AAA_CLIENT */

	/*
	 * ID with request sent to rekey IKE SA
	 */
	guint32 msgid;

	/**
	 * Copy of IKE SA INIT messages for authentication purposes
	 *
	 * Transient field...
	 */
	void *ikesa_init_i;
	guint16 ikesa_init_i_len;
	void *ikesa_init_r;
	guint16 ikesa_init_r_len;
	
	/**
	 * Should the message union be preserved during state change
	 */
	gboolean preserve_message;

	/**
	 * Pointer to embedded message 
	 *
	 * Maybe it would be a good idea to make this field a stack so
	 * that processing of messages can be delayed for later. One good
	 * candidate is processing of CFG payloads and associated message
	 * that contains CHILD SA data. Namely, we can not assume authentication
	 * if finished because in EAP case it would be not! Current design
	 * forces us to do processing of the message immediatelly since
	 * it wan't be available later (meaning, after few more states SM
	 * pass). This complicates code with many #ifdef's and if's.
	 */
	union sm_msg *embedded_sm_msg;
};

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
struct session *session_new(void);
struct session *session_temporary_new(void);
void session_ref(struct session *);
gboolean session_free(struct session *);
void session_transient_free(struct session *);

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/
guint8 session_get_ike_type(struct session *);
void session_set_ike_type(struct session *, guint8);

guint8 session_get_state(struct session *);
void session_set_state(struct session *, guint8);

gboolean session_get_shutdown(struct session *);
void session_set_shutdown(struct session *, gboolean);

guint32 session_get_limit_time(struct session *);
void session_set_limit_time(struct session *, guint32);

guint16 session_get_limit_allocs(struct session *);
void session_set_limit_allocs(struct session *, guint16);

guint64 session_get_limit_octets(struct session *);
void session_set_limit_octets(struct session *, guint32);

guint16 session_get_wsize(struct session *);
void session_set_wsize(struct session *, guint16);

guint64 session_get_r_spi(struct session *);
void session_set_r_spi(struct session *, guint64);

guint64 session_get_i_spi(struct session *);
void session_set_i_spi(struct session *, guint64);

guint32 session_get_msgid_lo(struct session *);
void session_set_msgid_lo(struct session *, guint32);

guint32 session_get_msgid_hi(struct session *);
void session_set_msgid_hi(struct session *, guint32);

guint32 session_get_peer_msgid_lo(struct session *);
void session_set_peer_msgid_lo(struct session *, guint32);

guint32 session_get_peer_msgid_hi(struct session *);
void session_set_peer_msgid_hi(struct session *, guint32);

guint32 session_get_and_incr_msg_id(gboolean, struct session *);

guint8 session_get_auth_type(struct session *);

struct message_msg *session_get_message_msg(struct session *);
void session_set_message_msg(struct session *, struct message_msg *);

struct pfkey_msg *session_get_pfkey_msg(struct session *);
void session_set_pfkey_msg(struct session *, struct pfkey_msg *);

struct netaddr * session_get_i_addr(struct session *);
void session_set_i_addr(struct session *, struct netaddr *);

struct netaddr * session_get_r_addr(struct session *);
void session_set_r_addr(struct session *, struct netaddr *);

GSList *session_get_proposals(struct session *);

proposal_t *session_get_selected_proposal(struct session *);
void session_set_selected_proposal(struct session *, proposal_t *);

void session_set_user_initiated(struct session *, gboolean);
gboolean session_get_user_initiated(struct session *);

/*******************************************************************************
 * MISC METHODS
 ******************************************************************************/
void session_nat_hash_clear(struct session *);
void session_nat_hash_append(struct session *, guchar *);

struct sad_item *session_get_sad_item(struct session *);
void session_sad_item_remove(struct session *, struct sad_item *);
guint32 session_get_next_msgid(struct session *);

gboolean session_check_queue(struct session *);

void session_lock(struct session *);
gboolean session_try_lock(struct session *);
void session_unlock(struct session *);

gboolean session_msg_push(struct session *, gpointer);
gpointer session_msg_pop(struct session *);

void session_sad_item_add(struct session *, struct sad_item *);
void session_sad_item_remove(struct session *, struct sad_item *);
int session_check_msg_id(struct session *, struct message_msg *);
struct sad_item *session_sad_item_find_by_spi(struct session *, guint32);
struct sad_item *session_sad_item_find_by_peer_spi(struct session *, guint32);
struct sad_item *session_sad_item_find_by_spi(struct session *, guint32);
struct sad_item *session_sad_item_find_by_protocol_and_spi(struct session *,
			guint8, guint32);
struct sad_item *session_sad_item_find_by_protocol_and_o_spi(struct session *,
			guint8, guint32);
struct sad_item *session_sad_item_find_rekey_by_spi(struct session *, guint32);
struct sad_item *session_sad_item_find_by_msg_id(struct session *, guint32);
int session_crypto_material(struct session *);

gboolean session_use_radius(struct session *);

GSList *session_add_peer_address(struct session *, struct netaddr *);
#ifdef MOBIKE
void session_local_addresses_init(struct session *);
#endif /* MOBIKE */
/*******************************************************************************
 * DEBUG METHODS
 ******************************************************************************/
void _session_dump(struct session *);

#endif /* __SESSION_H */
