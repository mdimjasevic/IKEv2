/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __NETWORK_MSG_H
#define __NETWORK_MSG_H

/*
 * Maximum message size ready to receive from peer.
 */
#define NET_RECEIVE_BUFFER_SIZE	4096

/*
 * Structure placed into queue when new packet arrives for upper subsystems.
 */
struct network_msg {
	void *buffer;
	size_t size;

	/*
	 * Socket on which packet has been received
 	 */
	struct network_socket *ns;

	/*
	 * Source address from the received packet
	 */
	struct netaddr *srcaddr;

	/*
	 * Destination address from the received packet
	 */
	struct netaddr *dstaddr;
};

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
struct network_msg *network_msg_alloc(void);
void network_msg_free(struct network_msg *);
struct netaddr *network_msg_get_srcaddr(struct network_msg *);
struct netaddr *network_msg_get_dstaddr(struct network_msg *);
ssize_t network_msg_get_data(struct network_msg *, void **);

#endif /* __NETWORK_MSG_H */
