#include <stdlib.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#define BUFFER_SIZE	1024

main(int argc, char **argv)
{
	int sock_fd;
	char buffer[BUFFER_SIZE];

	if ((sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		perror("socket");
		exit (1);
	}

	read(sock_fd, buffer, BUFFER_SIZE);

	close (sock_fd);
}
