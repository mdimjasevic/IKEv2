/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing network module
 *
 * ARP listener implemented through thread processing
 */

#define __NETWORK_TEST_C

#define LOGGERNAME	"network"

#include <stdio.h>

#include <sys/types.h>
#include <netinet/ip.h>
#include <net/ethernet.h>

#include <netpacket/packet.h>
#include <netinet/ether.h>

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "config.h"

char *pkttype2str(guint16 pkttype)
{
	switch(pkttype) {
	case PACKET_HOST:
		return "PACKET_HOST";
	case PACKET_BROADCAST:
		return "PACKET_BROADCAST";
	case PACKET_MULTICAST:
		return "PACKET_MULTICAST";
	case PACKET_OTHERHOST:
		return "PACKET_OTHERHOST";
	case PACKET_OUTGOING:
		return "PACKET_OUTGOING";
	case PACKET_LOOPBACK:
		return "PACKET_LOOPBACK";
	case PACKET_FASTROUTE:
		return "PACKET_FASTROUTE";
	}

	return "UNKNOWN";
}

char *hatype2str(guint16 hatype)
{
	switch(hatype) {
	case ARPHRD_NETROM:
		return "ARPHRD_NETROM";
	case ARPHRD_ETHER:
		return "ARPHRD_ETHER";
	case ARPHRD_EETHER:
		return "ARPHRD_EETHER";
	case ARPHRD_AX25:
		return "ARPHRD_AX25";
	case ARPHRD_PRONET:
		return "ARPHRD_PRONET";
	case ARPHRD_CHAOS:
		return "ARPHRD_CHAOS";
	case ARPHRD_IEEE802:
		return "ARPHRD_IEEE802";
	case ARPHRD_ARCNET:
		return "ARPHRD_ARCNET";
	case ARPHRD_APPLETLK:
		return "ARPHRD_APPLETLK";
	case ARPHRD_DLCI:
		return "ARPHRD_DLCI";
	case ARPHRD_ATM:
		return "ARPHRD_ATM";
	case ARPHRD_METRICOM:
		return "ARPHRD_METRICOM";
	case ARPHRD_IEEE1394:
		return "ARPHRD_IEEE1394";
	case ARPHRD_EUI64:
		return "ARPHRD_EUI64";
	case ARPHRD_INFINIBAND:
		return "ARPHRD_INFINIBAND";
	case ARPHRD_SLIP:
		return "ARPHRD_SLIP";
	case ARPHRD_CSLIP:
		return "ARPHRD_CSLIP";
	case ARPHRD_SLIP6:
		return "ARPHRD_SLIP6";
	case ARPHRD_CSLIP6:
		return "ARPHRD_CSLIP6";
	case ARPHRD_RSRVD:
		return "ARPHRD_RSRVD";
	case ARPHRD_ADAPT:
		return "ARPHRD_ADAPT";
	case ARPHRD_ROSE:
		return "ARPHRD_ROSE";
	case ARPHRD_X25:
		return "ARPHRD_X25";
#ifdef ARPHRD_HWX25
	case ARPHRD_HWX25:
		return "ARPHRD_HWX25";
#endif
	case ARPHRD_PPP:
		return "ARPHRD_PPP";
	case ARPHRD_CISCO:
		return "ARPHRD_CISCO";
	case ARPHRD_LAPB:
		return "ARPHRD_LAPB";
	case ARPHRD_DDCMP:
		return "ARPHRD_DDCMP";
	case ARPHRD_RAWHDLC:
		return "ARPHRD_RAWHDLC";
	case ARPHRD_TUNNEL:
		return "ARPHRD_TUNNEL";
	case ARPHRD_TUNNEL6:
		return "ARPHRD_TUNNEL6";
	case ARPHRD_FRAD:
		return "ARPHRD_FRAD";
	case ARPHRD_SKIP:
		return "ARPHRD_SKIP";
	case ARPHRD_LOOPBACK:
		return "ARPHRD_LOOPBACK";
	case ARPHRD_LOCALTLK:
		return "ARPHRD_LOCALTLK";
	case ARPHRD_FDDI:
		return "ARPHRD_FDDI";
	case ARPHRD_BIF:
		return "ARPHRD_BIF";
	case ARPHRD_SIT:
		return "ARPHRD_SIT";
	case ARPHRD_IPDDP:
		return "ARPHRD_IPDDP";
	case ARPHRD_IPGRE:
		return "ARPHRD_IPGRE";
	case ARPHRD_PIMREG:
		return "ARPHRD_PIMREG";
	case ARPHRD_HIPPI:
		return "ARPHRD_HIPPI";
	case ARPHRD_ASH:
		return "ARPHRD_ASH";
	case ARPHRD_ECONET:
		return "ARPHRD_ECONET";
	case ARPHRD_IRDA:
		return "ARPHRD_IRDA";
	case ARPHRD_FCPP:
		return "ARPHRD_FCPP";
	case ARPHRD_FCAL:
		return "ARPHRD_FCAL";
	case ARPHRD_FCPL:
		return "ARPHRD_FCPL";
	case ARPHRD_FCFABRIC:
		return "ARPHRD_FCFABRIC";
	case ARPHRD_IEEE802_TR:
		return "ARPHRD_IEEE802_TR";
	case ARPHRD_IEEE80211:
		return "ARPHRD_IEEE80211";
#ifdef ARPHRD_IEEE80211_PRISM
	case ARPHRD_IEEE80211_PRISM:
		return "ARPHRD_IEEE80211_PRISM";
#endif
#ifdef ARPHRD_IEEE80211_RADIOTAP
	case ARPHRD_IEEE80211_RADIOTAP:
		return "ARPHRD_IEEE80211_RADIOTAP";
#endif
	}

	return "UNKNOWN";
}

gpointer func_cb(gpointer _network_msg)
{
	struct network_msg *msg = _network_msg;
	struct sockaddr_ll *sll;

	LOG_NOTICE("Octets received %u", msg->size);

	sll = (struct sockaddr_ll *)(msg->srcaddr);

	LOG_NOTICE("sll->sll_protocol = %u", ntohs(sll->sll_protocol));
	LOG_NOTICE("sll->sll_ifindex = %u", sll->sll_ifindex);
	LOG_NOTICE("sll->sll_hatype = %s", hatype2str(sll->sll_hatype));
	LOG_NOTICE("sll->sll_pkttype = %s", pkttype2str(sll->sll_pkttype));
	LOG_NOTICE("sll->sll_halen = %u", sll->sll_halen);

	if (sll->sll_halen && sll->sll_hatype == ARPHRD_ETHER)
		LOG_NOTICE("sll->sll_addr=%s", ether_ntoa(sll->sll_addr));

	network_msg_free(msg);

	return NULL;
}

int main(int argc, char **argv)
{
	GMainLoop *ml;
	network_t *ns;

	struct config *config;

	if (argc != 2) {
		LOG_ERROR("Usage: %s <logging.conf>", argv[0]);
		goto out;
	}

	LOG_NOTICE("Simple sniffer that captures IP packets at data link");
	LOG_NOTICE("layer and prints some basic info about received data.");
	LOG_NOTICE("Data itself is not printed.");

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to network module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Initialize logging subsystem
	 */
	if (config_init(argv[1], &config) < 0)
		goto out;

	if (logging_init(config->logging) < 0)
		goto out;

	LOG_NOTICE("Initializing network module");
	if (network_init(NULL) < 0)
		goto out;

	LOG_NOTICE("Registering listening socket");
	ns = network_socket_register_thread(AF_PACKET, SOCK_RAW,
			htons(ETH_P_IP), NULL, 0, func_cb);
	if (ns == NULL)
		goto out;

	/*
	 * Start main loop...
	 */
	LOG_NOTICE("Starting main loop");
	g_main_loop_run(ml);

	/*
	 * Unload network module
	 */
	LOG_NOTICE("Unloading network module");
	network_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	LOG_NOTICE("Exiting");

out:
	logging_unload();

	return 0;
}
