/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file contains function for manipulation with kernel's security
 * policy database.
 */

#define __SPD_C

#define LOGGERNAME	"spd"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#include <stdlib.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "auth.h"
#include "ts.h"
#include "transforms.h"
#include "proposals.h"
#include "spd.h"
#include "sad.h"

#include "config.h"

#ifdef PFKEY_LINUX
#include "pfkey_linux.h"
#endif /* PFKEY_LINUX */

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/**
 * Global structure with data for SPD subsystem
 */
struct spd_data *spd_data;

/*******************************************************************************
 * SPD_DATA structure functions
 ******************************************************************************/

void spd_data_kspd_si_add(struct spd_item *si)
{
	struct spd_item *si_clone;

	si_clone = spd_item_ref(si);

	g_mutex_lock(spd_data->kspd_mutex);
	spd_data->kspd = g_slist_append(spd_data->kspd, si_clone);
	g_mutex_unlock(spd_data->kspd_mutex);
}

void spd_data_kspd_si_remove(struct spd_item *si)
{
	g_mutex_lock(spd_data->kspd_mutex);
	spd_data->kspd = g_slist_remove(spd_data->kspd, si);
	spd_item_free(si);
	g_mutex_unlock(spd_data->kspd_mutex);
}

/*******************************************************************************
 * CONFIG CSA structure functions
 ******************************************************************************/

struct config_csa *config_csa_new()
{
	struct config_csa *config_csa;

	LOG_FUNC_START(2);

	if ((config_csa = g_malloc0(sizeof(struct config_csa))) != NULL)
		config_csa->refcnt = 1;
	else
		LOG_ERROR("Out of memory");

	LOG_FUNC_END(2);

	return config_csa;
}

struct config_csa *config_csa_dup_ro(struct config_csa *config_csa)
{
	LOG_FUNC_START(2);

	if (config_csa)
		g_atomic_int_inc(&config_csa->refcnt);
	else
		LOG_BUG("NULL pointer");

	LOG_FUNC_END(2);

	return config_csa;
}

void config_csa_free(struct config_csa *config_csa)
{
	LOG_FUNC_START(2);

	if (g_atomic_int_dec_and_test(&(config_csa->refcnt))) {

		if (config_csa->tsl)
			ts_list_free(config_csa->tsl);

		if (config_csa->tsr)
			ts_list_free(config_csa->tsr);

		if (config_csa->encr_algs)
			transform_list_free(config_csa->encr_algs);

		if (config_csa->auth_algs)
			transform_list_free(config_csa->auth_algs);

		g_free(config_csa);

	}

	LOG_FUNC_END(2);
}

guint32 config_csa_get_cfg_line(struct config_csa *config_csa)
{
	return config_csa->cfg_line;
}

GSList *config_csa_get_encr_transforms(struct config_csa *ccsa)
{
	return ccsa->encr_algs;
}

GSList *config_csa_get_auth_transforms(struct config_csa *ccsa)
{
	return ccsa->auth_algs;
}

void config_csa_dump(struct config_csa *ccsa)
{
	LOG_DEBUG("Dumping SAINFO at line %d", ccsa->cfg_line);

	LOG_DEBUG("Local traffic selector");
	ts_list_dump(LOGGERNAME, ccsa->tsl);

	LOG_DEBUG("Remote traffic selector");
	ts_list_dump(LOGGERNAME, ccsa->tsr);

	LOG_DEBUG("Encryption algorithms");
	transform_list_dump(LOGGERNAME, ccsa->encr_algs);

	LOG_DEBUG("Authentication algorithms");
	transform_list_dump(LOGGERNAME, ccsa->auth_algs);

	LOG_DEBUG("Soft limits");
	limits_dump(ccsa->soft_limits);
	LOG_DEBUG("Hard limits");
	limits_dump(ccsa->hard_limits);
}

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS FOR SPD_ITEM STRUCTURE
 ******************************************************************************/

/**
 * Allocate memory for spd_item structure
 *
 * \return Pointer to a new spd_item structure or NULL in case of an error.
 */
struct spd_item *spd_item_new(void)
{
	struct spd_item *si;

	LOG_FUNC_START(1);

	si = g_malloc0(sizeof(struct spd_item));
	si->refcnt = 1;

	LOG_FUNC_END(1);

	return si;
}

/**
 * Create new policy given a template
 *
 * @param si		Prototype policy to be used to create a new one
 * @param tsl		Local traffic selector of a new policy
 * @param tsr		Remote traffic selector of a new policy
 *
 * \return Pointer to a new policy
 *
 * This is used in cases when SA changes traffic selectors. In that
 * case we change policy of this SA.
 */
struct spd_item *spd_item_new_from_prototype(struct spd_item *si,
		GSList *tsl, GSList *tsr)
{
	struct spd_item *spd;

	LOG_FUNC_START(1);

	spd = spd_item_new();

	/*
	 * This policy is created by IKEv2 and is not in the kernel
	 */
	spd->owner = SP_OWNER_IKEV2;

	spd->mode = si->mode;

	/*
	 * Because this policy is bound to a specific SA, then we
	 * bind them together with UNIQUE level
	 */
//	spd->level = IPSEC_LEVEL_UNIQUE;

	spd->proto = si->proto;

	/*
	 * Copy given local and remote traffic policies
	 *
	 * TODO: Introduce reference couting into traffic policies
	 */
	while (tsl) {
		spd->tsl = g_slist_append(spd->tsl, ts_dup(tsl->data));
		tsl = tsl->next;
	}

	while (tsr) {
		spd->tsr = g_slist_append(spd->tsr, ts_dup(tsr->data));
		tsr = tsr->next;
	}

	/*
	 * We skip fields pid, pid_in, pid_fwd * since the new
	 * policy is not yet in the kernel and thus it doesn't
	 * have those parameters defined.
	 *
	 * On the other hand, we _must not_ skip reqid fields
	 * beacuse if kernel triggered on one of those policies
	 * than it expects those numbers in SAs.
	 */
	spd->reqid = si->reqid;
	spd->reqid_in = si->reqid_in;
	spd->reqid_fwd = si->reqid_fwd;

	spd->tunnel_saddr = netaddr_dup(si->tunnel_saddr);
	spd->tunnel_daddr = netaddr_dup(si->tunnel_daddr);

	spd->ccsa = config_csa_dup_ro(si->ccsa);

	LOG_FUNC_END(1);

	return spd;
}

/**
 * Increase reference counter for policy item
 *
 * @param si		Policy to be cloned
 *
 * \return Pointer to a new policy
 */
struct spd_item *spd_item_ref(struct spd_item *si)
{
	LOG_FUNC_START(1);

	g_atomic_int_inc(&si->refcnt);

	LOG_FUNC_END(1);

	return si;
}

/**
 * Duplicate policy
 *
 * @param si		Prototype policy to be used to create a new one
 *
 * \return Pointer to a new policy
 *
 * This is used when rekeying CHILD SA. Because there are actually
 * new SAs crated, then also policies for them are also newly
 * created.
 */
struct spd_item *spd_item_dup(struct spd_item *si)
{
	struct spd_item *spd;

	LOG_FUNC_START(1);

	spd = spd_item_new();

	/*
	 * This policy is created by IKEv2 and is not yet in the kernel
	 */
	spd->owner = SP_OWNER_IKEV2;

	spd->mode = si->mode;
	spd->level = si->level;
	spd->proto = si->proto;
	spd->dir = si->dir;

	spd->tsl = ts_list_dup(si->tsl);
	spd->tsr = ts_list_dup(si->tsr);

	/*
	 * We skip fields pid, pid_in, pid_fwd * since the new
	 * policy is not yet in the kernel and thus it doesn't
	 * have those parameters defined.
	 *
	 * On the other hand, we _must not_ skip reqid fields
	 * beacuse if kernel triggered on one of those policies
	 * than it expects those numbers in SAs.
	 */
	spd->reqid = si->reqid;
	spd->reqid_in = si->reqid_in;
	spd->reqid_fwd = si->reqid_fwd;

	spd->tunnel_saddr = netaddr_dup(si->tunnel_saddr);
	spd->tunnel_daddr = netaddr_dup(si->tunnel_daddr);

	spd->ccsa = config_csa_dup_ro(si->ccsa);

	LOG_FUNC_END(1);

	return spd;
}

/**
 * Free spd_item
 *
 * @param si	Item to be free'd
 *
 * This function decrements reference counter of the SPD item
 * and if this counter is zero only then the structure is
 * removed from the memory!
 */
void spd_item_free(struct spd_item *si)
{

	LOG_FUNC_START(1);

	if (si && g_atomic_int_dec_and_test(&si->refcnt)) {

		if (si->tsl)
			ts_list_free(si->tsl);

		if (si->tsr)
			ts_list_free(si->tsr);

		if (si->tunnel_saddr)
			netaddr_free(si->tunnel_saddr);

		if (si->tunnel_daddr)
			netaddr_free(si->tunnel_daddr);

		if (si->ccsa) {
			config_csa_free(si->ccsa);
		}

		g_free (si);
	}

	LOG_FUNC_END(1);
}

/**
 * Free a list of spd_item items.
 *
 * @param spd_list
 *
 */
void spd_item_list_free(GSList *spd_list)
{
	while (spd_list) {
		spd_item_free(spd_list->data);
		spd_list = g_slist_remove(spd_list, spd_list->data);
	}
}

/*******************************************************************************
 * GETTERS AND SETTERS FOR spd_item STRUCTURE
 ******************************************************************************/

GSList *spd_item_get_encr_algs(struct spd_item *si)
{
	if (si->ccsa)
		return si->ccsa->encr_algs;

	return NULL;
}

GSList *spd_item_get_auth_algs(struct spd_item *si)
{
	if (si->ccsa)
		return si->ccsa->auth_algs;

	return NULL;
}

guint8 spd_item_get_ipsec_level(struct spd_item *spd_item)
{
	if (spd_item) {

		if (spd_item->level)
			return spd_item->level;

		if (spd_item->ccsa)
			return spd_item->ccsa->ipsec_level;
	}

	return IPSEC_MODE_ANY;
}

void spd_item_set_ipsec_level(struct spd_item *spd_item, guint8 level)
{
	if (spd_item)
		spd_item->level = level;
}

guint8 spd_item_get_ipsec_mode(struct spd_item *spd_item)
{
	if (spd_item) {

		if (spd_item->mode)
			return spd_item->mode;

		if (spd_item->ccsa)
			return spd_item->ccsa->ipsec_mode;
	}

	return IPSEC_MODE_ANY;
}

void spd_item_set_ipsec_mode(struct spd_item *spd_item, guint8 mode)
{
	if (spd_item)
		spd_item->mode = mode;
}

guint8 spd_item_get_ipsec_proto(struct spd_item *spd_item)
{
	if (spd_item) {

		if (spd_item->proto)
			return spd_item->proto;

		if (spd_item->ccsa)
			return spd_item->ccsa->ipsec_proto;
	}

	return IPSEC_MODE_ANY;
}

void spd_item_set_ipsec_proto(struct spd_item *spd_item, guint8 proto)
{
	if (spd_item)
		spd_item->proto = proto;
}

guint32 spd_item_get_pid(struct spd_item *spd_item)
{
	return spd_item->pid;
}

guint32 spd_item_get_pid_in(struct spd_item *spd_item)
{
	return spd_item->pid_in;
}

guint32 spd_item_get_reqid(struct spd_item *spd_item)
{
	return spd_item->reqid;
}

guint32 spd_item_get_reqid_in(struct spd_item *spd_item)
{
	return spd_item->reqid_in;
}

guint32 spd_item_get_reqid_fwd(struct spd_item *spd_item)
{
	return spd_item->reqid_fwd;
}

/**
 * Return local traffic selector of the policy
 *
 * @param si
 *
 * \return  Traffic selectors or NULL in case of an error
 */
GSList *spd_item_get_tsl(struct spd_item *si)
{
	return si->tsl ? si->tsl : si->ccsa->tsl;
}

/**
 * Return remote traffic selector of the policy
 */
GSList *spd_item_get_tsr(struct spd_item *si)
{
	return si->tsr ? si->tsr : si->ccsa->tsr;
}

/**
 * Return local tunnel endpoint
 */
struct netaddr *spd_item_get_tunnel_saddr(struct spd_item *si)
{
	return si->tunnel_saddr ? si->tunnel_saddr : si->ccsa->tunnel_saddr;
}

/**
 * Return remote tunnel endpoint
 */
struct netaddr *spd_item_get_tunnel_daddr(struct spd_item *si)
{
	return si->tunnel_daddr ? si->tunnel_daddr : si->ccsa->tunnel_daddr;
}

struct limits *spd_item_get_hard_limits(struct spd_item *si)
{
	if (si->ccsa)
		return si->ccsa->hard_limits;

	return NULL;
}

struct limits *spd_item_get_soft_limits(struct spd_item *si)
{
	if (si->ccsa)
		return si->ccsa->soft_limits;

	return NULL;
}

/*******************************************************************************
 * FUNCTIONS FOR SPD MAINTENANCE
 ******************************************************************************/

void spd_update(struct spd_item *sp)
{
//	spd_data->spd_op->spd_update(sp);
}

/**
 * Add policy to the kernel's SPD
 *
 * \return	0  if operation succeeded
 *		-1 in case of an error
 *		1  policy was already in the kernel
 */
int spd_add(struct spd_item *si)
{
	int retval;

	LOG_FUNC_START(1);

	/*
	 * First check if this policy is already in the kernel.
	 * If it is then do nothing.
	 */
	if (si->pid) {
		LOG_BUG("Policy already in the kernel.");
		retval = 1;
		goto out;
	}

	retval = spd_data->spd_op->spd_add(si);

	/*
	 * If adding policy to the kernel succeeded, add it
	 * also to our internal list of policies.
	 */
	if (!retval)
		spd_data_kspd_si_add(si);

out:
	LOG_FUNC_END(1);

	return retval;
}

void spd_delete(struct spd_item *si)
{
	LOG_FUNC_START(1);

	if (si->pid && si->owner == SP_OWNER_IKEV2) {
		spd_data->spd_op->spd_delete(si);
		spd_data_kspd_si_remove(si);
	}

	LOG_FUNC_END(1);
}

struct spd_item *spd_get()
{
//	spd_data->spd_op->spd_get(sp);
	return NULL;
}

void spd_acquire()
{
//	spd_data->spd_op->spd_acquire(sp);
}

void spd_dump()
{
//	spd_data->spd_op->spd_dump(sp);
}

void spd_flush()
{
	spd_data->spd_op->spd_flush();
}

void spd_setidx()
{
//	spd_data->spd_op->spd_setidx(sp);
}

void spd_setexpire()
{
//	spd_data->spd_op->spd_setexpire(sp);
}

void spd_delete2()
{
//	spd_data->spd_op->spd_delete2(sp);
}

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/

/**
 * Dump single policy item into log file
 *
 * @param si
 *
 * \return
 */
void spd_item_dump(struct spd_item *si)
{

	if (si->mode <= IPSEC_MODE_TUNNEL)
		LOG_DEBUG("mode = %s", ipsec_mode_strs[si->mode]);
	else
		LOG_DEBUG("mode = UNKNOWN VALUE (%u)", si->mode);

	switch (si->proto) {
	case SADB_SATYPE_AH:
		LOG_DEBUG("protocol=SADB_SATYPE_AH");
		break;
	case SADB_SATYPE_ESP:
		LOG_DEBUG("protocol=SADB_SATYPE_ESP");
		break;
	default:
		LOG_DEBUG("protocol=UNKNOWN VALUE (%u)", si->mode);
		break;
	}

	if (si->dir < IPSEC_DIR_MAX)
		LOG_DEBUG("dir = %s", ipsec_dir_strs[si->dir]);
	else
		LOG_DEBUG("dir = UNKNOWN VALUE (%u)", si->dir);

	ts_list_dump(LOGGERNAME, si->tsl);
        ts_list_dump(LOGGERNAME, si->tsr);

	LOG_DEBUG("\tpid=%u pid_in=%u pid_fwd=%u\n",
			si->pid, si->pid_in, si->pid_fwd);
	LOG_DEBUG("\treqid=%u reqid_in=%u reqid_fwd=%u\n",
			si->reqid, si->reqid_in, si->reqid_fwd);

	netaddr_dump(LOGGERNAME, si->tunnel_saddr);
	netaddr_dump(LOGGERNAME, si->tunnel_daddr);
}

/**
 * Dump policy database into log file
 *
 * @param sl
 */
void spd_list_dump(GSList *sl)
{
	while (sl) {
		spd_item_dump(sl->data);
		sl = sl->next;
	}
}

/*******************************************************************************
 * MISCALLENOUS FUNCTIONS
 ******************************************************************************/

/**
 * Search through policy database for the item with a given ID
 */
struct spd_item *spd_find_by_pid(guint32 pid)
{
	GSList *c;
	struct spd_item *si;

	LOG_DEBUG("Searching for the policy with ID %u",
			pid);

	g_mutex_lock(spd_data->kspd_mutex);
	for (c = spd_data->kspd; c; c = c->next) {
		si = c->data;

		if (si->pid == pid || si->pid_in == pid || si->pid_fwd == pid)
			goto out;
	}

	si = NULL;

out:
	g_mutex_unlock(spd_data->kspd_mutex);

	return si;
}

/**
 * Search through policy database for the item with a given configuration
 * data
 */
struct spd_item *spd_find_by_ccsa(struct config_csa *ccsa)
{
	GSList *c;
	struct spd_item *si;

	g_mutex_lock(spd_data->kspd_mutex);
	for (c = spd_data->kspd; c; c = c->next) {
		si = c->data;

		if (si->ccsa == ccsa)
			goto out;
	}

	si = NULL;

out:
	g_mutex_unlock(spd_data->kspd_mutex);

	return si;
}

/**
 * Find a policy that matches given traffic selectors.
 *
 * @param tsl		Traffic selector for local peer
 * @param tsr		Traffic selector for remote peer
 * @param sainfos	Restrict a search for SPD by this list of
 *			configuration structures
 * @param tsl_r		Result of the local traffic selectors
 *			intersection. If NULL, the intersection
 *			is equal to tsl.
 * @param tsr_r		Result of the remote traffic selectors
 *			intersection. If NULL, the intersection
 *			is equal to tsr.
 *
 * \return	spd structure, or NULL if there is no
 *		appropriate structure.
 *
 * This function does three searches:
 *
 * 1. searches for the complete superset of (TSl, TSr)
 * 2. searches for the complete superset of (TSl[1], TSr[1])
 * 3. searches for the first, non-null, intersection with (TSl[1], TSr[1])
 *
 * If there is no non-null intersection in third step, this
 * function returns NULL.
 */
struct spd_item *spd_item_find_by_ts(GSList *tsl, GSList *tsr,
		GSList *sainfos, GSList **tsl_r, GSList **tsr_r)
{
	struct spd_item *si;
	GSList *c, *c1;
	gboolean found;

	LOG_FUNC_START(1);

	ts_list_dump(LOGGERNAME, tsl);
	ts_list_dump(LOGGERNAME, tsr);

	g_mutex_lock(spd_data->kspd_mutex);

	/*
	 * First round, search for complete superset of tsi and tsr
	 */
	for (c = spd_data->kspd; c; c = c->next) {

		si = c->data;

		if (ts_lists_are_subset(spd_item_get_tsl(si),
				spd_item_get_tsr(si), tsl, tsr)) {

			/*
			 * We found a complete match, but we should also
			 * check if this is a permited policy
			 */
			found = FALSE;
			for (c1 = sainfos; c1; c1 = c1->next)
				if (c1->data == si->ccsa) {
					LOG_DEBUG("Found complete superset");
					found = TRUE;
					break;
				}

			if (!found)
				continue;

			if (!ts_lists_are_subset(tsl, tsr, spd_item_get_tsl(si),
					spd_item_get_tsr(si))) {
				LOG_NOTICE("Narrowing traffic selectors");

				*tsl_r = ts_lists_intersect(
						spd_item_get_tsl(si),
						tsl);

				*tsr_r = ts_lists_intersect(
						spd_item_get_tsr(si),
						tsr);
			} else
				LOG_NOTICE("Traffic selectors are equal!");

			goto out;
		}
	}

	/*
	 * Second round, search for complete superset of tsl[1] and tsr[1]
	 * but only if there are more then one element in both tsl and
	 * tsr
	 */
	if (tsl->next && tsr->next)
		for (c = spd_data->kspd; c; c = c->next) {

			si = c->data;

			if (ts_lists_are_subset(spd_item_get_tsl(si),
					spd_item_get_tsr(si),
					tsl->data, tsr->data)) {

				/*
				 * We found a match, but we should also
				 * check if this is a permitted policy
				 */
				for (c1 = sainfos; c1; c1 = c1->next)
					if (c1->data == si->ccsa) {

						LOG_DEBUG("Found complete "
							"superset for tsl[1]"
							" and tsr[1]");

						*tsl_r = ts_lists_intersect(
							spd_item_get_tsl(si),
								tsl);

						*tsr_r = ts_lists_intersect(
							spd_item_get_tsr(si),
								tsr);

						goto out;
					}
			}

		}

	/*
	 * Third round, search for first non-null intersection between
	 * tsi,tsr and SAINFO selectors
	 */
	for (c = spd_data->kspd; c; c = c->next) {

		si = c->data;

		*tsl_r = ts_lists_intersect(spd_item_get_tsl(si), tsl);

		*tsr_r = ts_lists_intersect(spd_item_get_tsr(si), tsr);

		if (*tsl_r && *tsr_r) {

			/*
			 * We found a match, but we should also
			 * check if this is a permited policy
			 */
			for (c1 = sainfos; c1; c1 = c1->next)
				if (c1->data == si->ccsa) {

					LOG_DEBUG("Found non-null "
							"intersection");

					goto out;
				}
		}

		if (*tsl_r) {
			ts_list_free(*tsl_r);
			*tsl_r = NULL;
		}

		if (*tsr_r) {
			ts_list_free(*tsr_r);
			*tsr_r = NULL;
		}
	}

	si = NULL;

out:
	g_mutex_unlock(spd_data->kspd_mutex);

	LOG_FUNC_END(1);

	return si;
}


/*******************************************************************************
 * INTERNAL FUNCTIONS
 ******************************************************************************/

/**
 * Creates a list of SPDs from the configuration file
 *
 * @param peers
 * @param spdinstall	Should created policies be installed in the kernel
 * @param spdflush	If FLUSH should be performed on kernel's SPD
 *
 * \return
 *
 * This function generates SPD entries from the given peer list.
 */
void spd_subsys_generate(GSList *peers, gboolean spdinstall, gboolean spdflush)
{
	GSList *c;
	struct config_peer *cp;
	struct config_csa *ccsa;
	struct spd_item *si;

	LOG_FUNC_START(1);

	if (spdflush)
		spd_data->spd_op->spd_flush();

	/*
	 * Iterate over all peers we have in a configuration file
	 */
	while (peers) {

		cp = peers->data;
		peers = peers->next;

		/*
		 * For each peer, from it's sainfo sections,
		 * create SPD entries.
		 */
		for (c = cp->sainfo; c; c = c->next) {
	
			ccsa = c->data;

			si = spd_item_new();

			si->ccsa = ccsa;

			g_atomic_int_inc(&ccsa->refcnt);

			if (spdinstall && ccsa->status == CSA_INSTALL)
				if (!spd_data->spd_op->spd_add(si))
					si->owner = SP_OWNER_IKEV2;

			spd_data_kspd_si_add(si);
		}
	}

	LOG_FUNC_END(1);
}

/**
 * Syncronize IKEv2 configuration and SPD
 *
 * @param peers
 *
 * \return
 *
 * This function synchronizes policy database in kernel with the
 * one in IKEv2 daemon. In other words, for each SPD pair in kernel
 * this function searches appropriate items in configuration file
 * and then binds them together. If there is no appropriate entry
 * in configuration file for a SPD it will be ignored during ikev2
 * execution. It also attaches all the aditional data to each of
 * policy item.
 *
 * Maybe we should go twice through configuration file. In the
 * first run we search for exact match between policy and the
 * configuration value, and in the second run we search for the
 * subset?
 */
int spd_subsys_sync(GSList *peers)
{
	GSList *kspd, *c1, *c2;
	struct config_peer *cp;
	struct spd_item *si;
	struct config_csa *ccsa;
	gboolean found;
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	if (spd_data->spd_op->spd_dump(&spd_data->kspd) < 0)
		goto out;

	/*
	 * We iterate over kspd without holding lock since this
	 * function runs only during startup when there are no
	 * multiple threads.
	 */
	for (kspd = spd_data->kspd; kspd; kspd = kspd->next) {

		si = kspd->data;

		LOG_DEBUG("Kernel traffic selectors");
		ts_list_dump(LOGGERNAME, si->tsl);
		ts_list_dump(LOGGERNAME, si->tsr);

		found = FALSE;

		for (c1 = peers; c1; c1 = c1->next) {

			cp = c1->data;

			for (c2 = cp->sainfo; c2; c2 = c2->next) {

				ccsa = c2->data;

				LOG_DEBUG("Config traffic selectors");
				ts_list_dump(LOGGERNAME, ccsa->tsl);
				ts_list_dump(LOGGERNAME, ccsa->tsr);

				if (!ts_lists_are_subset(ccsa->tsl, ccsa->tsr,
						si->tsl, si->tsr))
					continue;

				found = TRUE;

				LOG_DEBUG("Policy ID %u matches SAINFO section"
						" in line %u",
						si->pid, ccsa->cfg_line);

				g_atomic_int_inc(&ccsa->refcnt);
				si->ccsa = ccsa;
			}
		}

		if (!found) {
			LOG_WARNING("No configuration data found for policy "
					"ID %u. It will be ignored!",
					si->pid);
		}
	}

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Thread that waits for responses and notifies from lower level interface
 * to SPD
 */
gpointer spd_thread(gpointer data)
{
	return NULL;
}

/*******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/

void spd_set_shutdown_state(void)
{
	if (spd_data->spd_op && spd_data->spd_op->spd_shutdown)
		spd_data->spd_op->spd_shutdown(spd_data->spd_op);
}

void spd_unload()
{
	struct spd_item *si;

	if (spd_data) {

		if (spd_data->kspd) {
			LOG_NOTICE("Removing policies from the kernel");
			while (spd_data->kspd) {

				si = spd_data->kspd->data;

				if (si->owner == SP_OWNER_IKEV2 && si->pid)
					spd_data->spd_op->spd_delete(si);

				spd_data->kspd = g_slist_remove(
							spd_data->kspd, si);
			}
		}

		if (spd_data->kspd_mutex)
			g_mutex_free(spd_data->kspd_mutex);

		if (spd_data->spd_op)
			spd_data->spd_op->spd_unload();

		g_free(spd_data);
	}
}

/**
 * Initialize IKEv2's connection to kernel's SPD
 *
 * @param context
 * @param sm_queue
 * @param peers
 * @param kernel_spd
 *
 * \return
 */
int spd_init(GMainContext *context, GAsyncQueue *sm_queue,
		GSList *peers, guint8 kernel_spd)
{
	gint retval;

	spd_data = g_malloc0(sizeof(struct spd_data));

	retval = -1;

#ifdef PFKEY_LINUX
	/*
	 * Initialize PFKEY subsystem and return it's handle...
	 */
	if ((spd_data->spd_op = pfkey_linux_spd_init(context,
			spd_data->queue)) == NULL) {
		spd_unload();
		goto out;
	}
#endif

#ifdef PFKEY_SOLARIS
	if ((spd_data->sad_op = pfkey_solaris_spd_init(context,
			spd_data->queue)) == NULL) {
		spd_unload();
		goto out;
	}
#endif

#ifdef NETLINK_LINUX
	if ((spd_data->spd_op = netlink_spd_init(context,
			spd_data->queue)) == NULL) {
		spd_unload();
		goto out;
	}
#endif

	spd_data->kspd_mutex = g_mutex_new();

	spd_data->thread = g_thread_create(spd_thread, NULL, FALSE, NULL);

	retval = 0;

	switch (kernel_spd) {
	case KERNEL_SPD_FLUSH:
		LOG_NOTICE("Flushing kernel's security policy database");
		spd_subsys_generate(peers, FALSE, TRUE);
		break;

	case KERNEL_SPD_ROSYNC:
	case KERNEL_SPD_SYNC:
		LOG_NOTICE("Reading kernel's SPD");
		spd_subsys_sync(peers);
		break;

	case KERNEL_SPD_GENERATE:
		LOG_NOTICE("Generating kernel's security policy database");
		spd_subsys_generate(peers, TRUE, FALSE);
		break;

	default:
		break;
	}

#ifdef PFKEY_LINUX
	/*
	 * Set PFKEY into running state, meaning that initialization
	 * finished. It is important so that internal function know
	 * that they, from now on, can relay on GLib's main loop!
	 */
	spd_data->spd_op->spd_running();
#endif

out:
        LOG_FUNC_END(1);

        return retval;
}
