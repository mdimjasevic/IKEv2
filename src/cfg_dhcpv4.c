/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef CFG_DHCPV4

#define LOGGERNAME	"cfg_dhcpv4"

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#include <net/if_arp.h>
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET_H
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET_H */

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "timeout.h"

#include "auth.h"
#include "cfg.h"
#include "cfg_module.h"
#include "cfg_dhcpv4.h"

/**
 * Magic cookie 
 */
char magic_cookie[] = { 0x63, 0x82, 0x53, 0x63};

/**
 * Global provider data for DHCPv4
 */
struct dhcpv4_provider_data *dpd = NULL;

gpointer dhcpv4_timeout_cb(gpointer);

/******************************************************************************
 * dhcpv4_cfg manipulation functions
 ******************************************************************************/

/**
 * Allocate new cfg structure
 */
struct dhcpv4_cfg *dhcpv4_cfg_new()
{
	struct dhcpv4_cfg *dc;

	dc = g_malloc0(sizeof(struct dhcpv4_cfg));
	dc->refcnt = 1;

	return dc;
}

/**
 * Increase reference counter of dhcpv4_cfg structure
 */
void dhcpv4_cfg_ref(struct dhcpv4_cfg *dc)
{
	g_atomic_int_inc(&(dc->refcnt));
}

/**
 * Free cfg structure
 */
void dhcpv4_cfg_free(struct dhcpv4_cfg *dc)
{
	if (dc && g_atomic_int_dec_and_test(&(dc->refcnt)))
		g_free(dc);
}

/**
 * Dump cfg structure
 */
void dhcpv4_cfg_dump(struct dhcpv4_cfg *dc)
{
	char ipaddr[INET6_ADDRSTRLEN];

	if (dc) {
	
		LOG_NOTICE("Timeout %u", dc->timeout);
		LOG_NOTICE("Retries %u", dc->retries);
		LOG_NOTICE("DHCP port %u", dc->port);

		LOG_NOTICE("DHCP server %s",
				inet_ntop(AF_INET, &dc->server,
					ipaddr, INET6_ADDRSTRLEN));

		LOG_NOTICE("DHCP relay server %s",
				inet_ntop(AF_INET, &dc->relay,
					ipaddr, INET6_ADDRSTRLEN));

	}
}

/******************************************************************************
 * dhcp_event manipulation functions
 ******************************************************************************/

/**
 * Allocate new event structure
 */
struct dhcpv4_event *dhcpv4_event_new()
{
	return g_malloc0(sizeof(struct dhcpv4_event));
}

/**
 * Release dhcpv4 structure
 */
void dhcpv4_event_free(struct dhcpv4_event *de)
{
	if (de) {

		switch(de->type) {
		case EVENT_PACKET_RECEIVED:
			if (de->dd)
				dhcpv4_data_free(de->dd);
			break;

		case EVENT_NONE:
		case EVENT_T1_EXPIRED:
		case EVENT_T2_EXPIRED:
		case EVENT_TIMEOUT:
		case EVENT_RELEASE:
			break;
		}

		g_free(de);
	}
}

/******************************************************************************
 * dhcp_data manipulation functions
 ******************************************************************************/

/**
 * Allocate new dhcpv4 structure
 */
struct dhcpv4_data *dhcpv4_data_new()
{
	return g_malloc0(sizeof(struct dhcpv4_data));
}

/**
 * Release dhcpv4 structure
 */
void dhcpv4_data_free(struct dhcpv4_data *dd)
{
	if (dd) {

		if (dd->message)
			g_free(dd->message);

		if (dd->dns)
			g_free(dd->dns);

		g_free(dd);
	}
}

/**
 * Dump dhcpv4_data structure
 */
void dhcpv4_data_dump(struct dhcpv4_data *dd)
{
	char ipaddr[16];
	int i;

	LOG_DEBUG("message type = %s\n",
		dd->bootp_type == BOOTPREQUEST ? "BOOTPREQUEST"
				: (dd->bootp_type == BOOTPREPLY
					? "BOOTPREPLY"
					: "UNKNOWN"));

	switch(dd->dhcp_type) {
	case DHCP_MSGTYPE_DISCOVER:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_DISCOVER\n");
		break;
	case DHCP_MSGTYPE_OFFER:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_OFFER\n");
		break;
	case DHCP_MSGTYPE_REQUEST:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_REQUEST\n");
		break;
	case DHCP_MSGTYPE_DECLINE:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_DECLINE\n");
		break;
	case DHCP_MSGTYPE_ACK:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_ACK\n");
		break;
	case DHCP_MSGTYPE_NAK:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_NAK\n");
		break;
	case DHCP_MSGTYPE_RELEASE:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_RELEASE\n");
		break;
	case DHCP_MSGTYPE_INFORM:
		LOG_DEBUG ("dhcp type = DHCP_MSGTYPE_INFORM\n");
		break;
	default:
		LOG_DEBUG ("dhcp type = UNKNOWN\n");
		break;
	}

	LOG_DEBUG ("xid=%u\n", dd->xid);

	if (dd->subnet)
		LOG_DEBUG ("subnet mask %s\n",
			inet_ntop(AF_INET, &dd->subnet, ipaddr, 16));

	if (dd->yiaddr)
		LOG_DEBUG ("assigned address %s\n",
			inet_ntop(AF_INET, &dd->yiaddr, ipaddr, 16));

	if (dd->router)
		LOG_DEBUG ("default router %s\n",
			inet_ntop(AF_INET, &dd->router, ipaddr, 16));

	for(i = 0; i < dd->dns_servers; i++)
		LOG_DEBUG ("dns %s\n",
			inet_ntop(AF_INET, &dd->dns[i], ipaddr, 16));

	if (dd->max_msg_size)
		LOG_DEBUG ("maximum dhcp message size %u\n", dd->max_msg_size);

	if (dd->message)
		LOG_DEBUG ("message %*s\n", dd->message_len, dd->message);

	if (dd->lease_time)
		LOG_DEBUG ("lease time %u\n", dd->lease_time);

	if (dd->serverid)
		LOG_DEBUG ("serverid %s\n",
			inet_ntop(AF_INET, &dd->serverid, ipaddr, 16));

	if (dd->renew_time)
		LOG_DEBUG ("lease time %u\n", dd->renew_time);
}

/******************************************************************************
 * Networking related functions
 ******************************************************************************/

/**
 * Parse received DHCPv4 packet
 *
 * @param dp		Pointer to received packet
 * @param packet_size	Size in octets of received packet
 * @param dd
 */
struct dhcpv4_data *dhcpv4_packet_parse(struct dhcpv4_packet *dp,
		int packet_size)
{
	guchar *p;
	struct dhcpv4_data *ddret = NULL;

	/*
	 * Check to see if minimal size is met...
	 */
	if (packet_size < sizeof(struct dhcpv4_packet)) {
		LOG_ERROR ("Packet total size too small");
		goto out;
	}

	ddret = dhcpv4_data_new();

	ddret->bootp_type = dp->op;
	ddret->xid = dp->xid;
	ddret->yiaddr = dp->yiaddr;

	p = (guchar *)dp + sizeof(struct dhcpv4_packet);
	while (((p < (guchar *)dp + packet_size) && (*p != DHCP_OPTION_END))
			&& ddret) {

		/*
		 * Check correct size of option
		 */
		if (p + *(p + 1) + 2 >= (guchar *)dp + packet_size) {
			dhcpv4_data_free(ddret);
			ddret = NULL;
			LOG_ERROR ("Inconsistant total size");
			break;
		}

		switch (*p) {
		case DHCP_OPTION_SUBNETMASK:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" SUBNETMASK");
				break;
			}

			ddret->subnet = *(guint32 *)(p + 2);
			break;

		case DHCP_OPTION_ROUTER:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" ROUTER");
				break;
			}

			ddret->router = *(guint32 *)(p + 2);
			break;

		case DHCP_OPTION_DNS:
			/*
			 * Check option's len
			 */
			if ((*(p + 1) & 3) || *(p + 1) < 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option DNS");
				break;
			}

			ddret->dns_servers = *(p + 1) >> 2;

			if ((ddret->dns = g_memdup(p + 2, ddret->dns_servers *
						sizeof(guint32))) == NULL) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				break;
			}

			break;

		case DHCP_OPTION_HOSTNAME:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" HOSTNAME");
				break;
			}

			break;

		case DHCP_OPTION_DOMAINNAME:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" DOMAIN NAME");
				break;
			}

			break;

		case DHCP_OPTION_BROADCAST:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" BROADCAST");
				break;
			}

			break;

		case DHCP_OPTION_REQUESTEDIP:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" REQUESTED IP");
				break;
			}

			/*
			 * Do nothing because this option is from client to server
			 */
			break;

		case DHCP_OPTION_LEASETIME:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" LEASE TIME");
				break;
			}

			ddret->lease_time = ntohl(*(guint32 *)(p + 2));
			break;

		case DHCP_OPTION_MSGTYPE:
			/*
			 * Check option's len
			 */
			if ((*(p + 1) != 1) ||
					(*(p + 2) != DHCP_MSGTYPE_DISCOVER
					&& *(p + 2) != DHCP_MSGTYPE_OFFER
					&& *(p + 2) != DHCP_MSGTYPE_REQUEST
					&& *(p + 2) != DHCP_MSGTYPE_DECLINE
					&& *(p + 2) != DHCP_MSGTYPE_ACK
					&& *(p + 2) != DHCP_MSGTYPE_NAK
					&& *(p + 2) != DHCP_MSGTYPE_RELEASE
					&& *(p + 2) != DHCP_MSGTYPE_INFORM)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" MSGTYPE");
				break;
			}

			ddret->dhcp_type = *(p + 2);
			break;

		case DHCP_OPTION_SERVERID:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option"
						" SERVERID");
				break;
			}

			ddret->serverid = *(guint32 *)(p + 2);
			break;

		case DHCP_OPTION_PARAMREQLIST:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option "
						"PARAM REQ LIST");
				break;
			}

			break;

		case DHCP_OPTION_MESSAGE:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in "
						"option MESSAGE");
				break;
			}

			ddret->message_len = *(p + 1);
			ddret->message = g_strndup((gchar *)(p + 2), *(p + 1));
			break;

		case DHCP_OPTION_MAXMSGSIZE:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 2) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option MAX "
						"MESSAGE SIZE\n");
				break;
			}

			ddret->max_msg_size = ntohs(*((guint16 *)(p + 2)));
			break;

		case DHCP_OPTION_RENEWALTIME:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option "
						"RENEWALTIME");
				break;
			}

			ddret->renew_time = ntohl(*(guint32 *)(p + 2));
			break;

		case DHCP_OPTION_REBINDINGTIME:
			/*
			 * Check option's len
			 */
			if (*(p + 1) != 4) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("Inconsistant size in option "
						"REBINDINGTIME");
				break;
			}

			break;

		case DHCP_OPTION_VENDORCLASSID:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("\tIncorrect length %u", *p);
				break;
			}

			LOG_DEBUG ("\tVendor class id (len=%u) %*s", *(p + 1),
					*(p + 1), p + 2);

			break;

		case DHCP_OPTION_CLIENTID:
			/*
			 * Check option's len
			 */
			if (!*(p + 1)) {
				dhcpv4_data_free(ddret);
				ddret = NULL;
				LOG_ERROR ("\tIncorrect length %u", *p);
				break;
			}

			LOG_DEBUG ("\tClient id (len=%u) %*s", *(p + 1),
					*(p + 1), p + 2);

			break;

		default:
			LOG_ERROR ("Ignoring unknown option type %u", *p);
		}

		p++;
		p += *p;
		p++;
	}

out:
	return ddret;
}

/**
 * Send DHCPv4 request
 *
 * @param cfg_data
 */
void dhcpv4_send_msg(struct cfg_data *cfg_data)
{
	struct netaddr *na;
	struct dhcpv4_event *de;
	struct dhcpv4_client *dc;
	char ip_buf[INET6_ADDRSTRLEN];
	GTimeVal tv;

	dc = cfg_data->provider4_data;
	na = netaddr_new_from_inaddr(dhcpv4_client_get_server(dc));

	LOG_DEBUG("Using DHCP server with the IP address %s ",
			netaddr_ip2str(na, ip_buf, INET6_ADDRSTRLEN));

	/*
	 * Send didn't succeed because of insufficient resources
	 */
	if (!na)
		return;

	netaddr_set_port(na, dhcpv4_client_get_port(dc));
	network_send_packet(dpd->ns, dc->msg, dc->msg_size, na);

	/*
	 * Should we start timeout?
	 *
	 * We don't expect response in FINISHED or ERROR states, so
	 * don't register timeout in such cases.
	 */
	if (dc->dhcpv4_cfg->timeout &&
		dc->state != DHCLNT_STATE_FINISHED &&
		dc->state != DHCLNT_STATE_ERROR) {

		if ((de = dhcpv4_event_new()) != NULL) {

			de->type = EVENT_TIMEOUT;
			de->cfg_data = cfg_data;

			tv.tv_sec = dc->dhcpv4_cfg->timeout;
			tv.tv_usec = 0;

			/*
			 * Register timeout
			 */
			dc->to = timeout_register_thread(&tv, 0, de,
						dhcpv4_timeout_cb);

			if (dc->to == NULL) {
				/*
				 * Raise an error, timeout
				 * registration failed
				 */
			}
		}
	}

	netaddr_free(na);
}

/**
 * Create DHCPv4 request to a DHCP packet
 *
 * @param dc
 * @param msg_type
 *
 * \return 0 if everything was OK, or -1 in case of an error
 */
gint dhcpv4_create_request(struct cfg_data *cfg_data, gint msg_type)
{
	GSList *c;
	guchar *p;
	gint retval = -1;
	struct dhcpv4_packet *dp;
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	dc->msg = dp = g_malloc0(DHCP_MAXIMUM_PACKET_SIZE);

	dp->op = BOOTPREQUEST;
	dp->htype = ARPHRD_ETHER;

	dp->xid = dc->xid;

	/**
	 * Search for IPv4 address to put it into appropriate
	 * field of DHCP request packet.
	 */
	for (c = cfg_data->cfg->netaddrs; c; c = c->next)
		if (netaddr_get_family(c->data) == AF_INET) {
			dp->yiaddr = *(guint32 *)netaddr_get_ipaddr(c->data);
			break;
		}

	/*
	 * ciaddr	4  Client IP address; only filled in if client is in
	 *		   BOUND, RENEW or REBINDING state and can respond
	 *		   to ARP requests.
	 */
	if (dc->state == DHCLNT_STATE_BOUND
			|| dc->state == DHCLNT_STATE_RENEWING
			|| dc->state == DHCLNT_STATE_REBINDING)
		for (c = cfg_data->cfg->netaddrs; c; c = c->next)
			if (netaddr_get_family(c->data) == AF_INET) {
				dp->ciaddr = *(guint32 *)
						netaddr_get_ipaddr(c->data);
				break;
			}

	memcpy(&dp->giaddr, &dc->dhcpv4_cfg->relay, 4);

	memcpy(&dp->magic_cookie, magic_cookie, 4);

	dc->msg_size = sizeof(struct dhcpv4_packet);
	p = (guchar *)dp + dc->msg_size;

	/**
	 * Add Message Type Option
	 */
	*p++ = DHCP_OPTION_MSGTYPE;
	*p++ = 1;
	*p++ = msg_type;
	dc->msg_size += 3;

	if (dc->serverid) {
		*p++ = DHCP_OPTION_SERVERID;
		*p++ = 4;
		*(guint32 *)p = dc->serverid;
		p += 4;
		dc->msg_size += 6;
	}

	/*
	 * If we are in initial state then construct a list of
	 * parameters we are requesting from a DHCP server.
	 */
	if (dc->state == DHCLNT_STATE_INIT) {

		*p = DHCP_OPTION_PARAMREQLIST;
		*(p + 1) = 0;

#if 0
		if (cfg_data->data->flags & OPTION_F4_ADDR) {
			*(p + *(p + 1) + 2) = DHCP_OPTION_ADDR;
			(*(p + 1))++;
		}
#endif

		if (cfg_data->cfg->flags & OPTION_F4_DNS) {
			*(p + *(p + 1) + 2) = DHCP_OPTION_DNS;
			(*(p + 1))++;
		}

#if 0
		if (cfg_data->data->flags & OPTION_F4_DHCP) {
			*(p + *(p + 1) + 2) = DHCP_OPTION_DHCP;
			(*(p + 1))++;
		}
#endif

		if (cfg_data->cfg->flags & OPTION_F4_NBNS) {
			*(p + *(p + 1) + 2) = DHCP_OPTION_NBNS;
			(*(p + 1))++;
		}

		if (*(p + 1)) {
			dc->msg_size += 2 + *(p + 1);
			p += *(p + 1) + 2;
		}
	}

	/*
	 * We are relyaing on a fact that dp->yiaddr is already
	 * initialized so we don't have to search through a list
	 * again.
	 */
	if (dp->yiaddr && msg_type != DHCP_MSGTYPE_RELEASE) {
		*p++ = DHCP_OPTION_REQUESTEDIP;
		*p++ = 4;
		*(guint32 *)p = dp->yiaddr;
		p += 4;
		dc->msg_size += 6;
	}

	if (cfg_data->cid) {
		*p++ = DHCP_OPTION_CLIENTID;
		*p++ = id_get_size(cfg_data->cid);
		memcpy(p, id_get_ptr(cfg_data->cid),
				id_get_size(cfg_data->cid));
		p += id_get_size(cfg_data->cid);
		dc->msg_size += id_get_size(cfg_data->cid) + 2;
	}

	*p++ = DHCP_OPTION_END;

	retval = 0;

	return retval;
}

/******************************************************************************
 * dhcpv4_client manipulation functions
 ******************************************************************************/

/**
 * Initialize state for a single client
 *
 * @param cfg_data	CFG state with data that should be obtained
 *
 * \return 0 if initialization has been successful, -1 otherwise
 */
gint dhcpv4_client_init(struct cfg_data *cfg_data,
		struct cfg_config_provider *ccp)
{
	struct dhcpv4_client *dc;
	gint retval;

	LOG_FUNC_START(1);

	retval = -1;

	/*
	 * Initialize new client
	 */
	dc = g_malloc0(sizeof(struct dhcpv4_client));

	cfg_data->provider4_data = dc;

	dc->dhcpv4_cfg = ccp->data;
	dhcpv4_cfg_ref(dc->dhcpv4_cfg);

	/*
	 * Initialize mutex...
	 */
	dc->mutex = g_mutex_new();

	/*
	 * Add to a client list handled by this module
	 */
	g_static_rw_lock_writer_lock(&dpd->clients_lock);
	dpd->clients = g_slist_prepend(dpd->clients, cfg_data);
	g_static_rw_lock_writer_unlock(&dpd->clients_lock);

	retval = 0;

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Release dhcpv4_client structure
 */
void dhcpv4_client_free(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	LOG_FUNC_START(1);

	dc = cfg_data->provider4_data;

	if (dc) {

		if (dc->dhcpv4_cfg)
			dhcpv4_cfg_free(dc->dhcpv4_cfg);

		if (dc->msg)
			g_free(dc->msg);

		if (dc->to)
			timeout_cancel(dc->to);

		g_mutex_free(dc->mutex);

		g_free(dc);
	}

	LOG_FUNC_END(1);
}

/**
 * Reset client state and release all the memory.
 */
void dhcpv4_client_reset(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	LOG_FUNC_START(1);

	dc = cfg_data->provider4_data;

	if (dc) {

		dc->state = DHCLNT_STATE_INIT;

		if (dc->dhcpv4_cfg)
			dhcpv4_cfg_free(dc->dhcpv4_cfg);

		if (dc->msg)
			g_free(dc->msg);

		if (dc->to) {
			timeout_cancel(dc->to);
			dc->to = NULL;
		}

	}

	LOG_FUNC_END(1);
}

/**
 * Merge data from response into cfg_data structure
 *
 * @param cfg_data
 * @param dd
 *
 * This is private function to merge data received in DHCP response
 * into cfg_data structure.
 */
void dhcpv4_merge_dhcp_response(struct cfg_data *cfg_data,
		struct dhcpv4_data *dd)
{
	GSList *c;
	struct dhcpv4_client *dc;
	struct netaddr *na;
	int i;

	dc = cfg_data->provider4_data;

	/*
	 * If we got IP address, release any existing one, and
	 * add this one to the list.
	 */
	if (dd->yiaddr) {

		for (c = cfg_data->cfg->netaddrs; c; c = c->next)
			if (netaddr_get_family(c->data) == AF_INET) {
				netaddr_free(c->data);
				cfg_data->cfg->netaddrs = g_slist_remove(
						cfg_data->cfg->netaddrs,
						c->data);
			}

		na = netaddr_new_from_inaddr(
					(struct in_addr *)&(dd->yiaddr));
		cfg_data->cfg->netaddrs = g_slist_append(
						cfg_data->cfg->netaddrs, na);
	}

	dc->serverid = dd->serverid;
	cfg_data->cfg->netmask32 = dd->subnet;

	if (dd->dns_servers) {

		/*
		 * Release current list of DNS servers
		 */
		while (cfg_data->cfg->dns) {
			netaddr_free(cfg_data->cfg->dns->data);
			cfg_data->cfg->dns = g_slist_remove(
						cfg_data->cfg->dns,
						cfg_data->cfg->dns->data);
		}

		for(i = 0; i < dd->dns_servers; i++) {
			na = netaddr_new_from_inaddr(
					(struct in_addr *)&(dd->dns[i]));
			cfg_data->cfg->dns = g_slist_append(
						cfg_data->cfg->dns, na);
		}
		dd->dns = NULL;
	}

	cfg_data->cfg->expiry4 = dd->renew_time;
}

/**
 * Advance client's state accoring to event
 *
 * @param dc
 * @param event
 *
 * \return	0  Everything was OK
 *		-1 Client state machine got into error state
 */
int dhcpv4_client_step(struct cfg_data *cfg_data, struct dhcpv4_event *event)
{
	int retval;
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	if (!dc)
		return 0;

	retval = -1;

	switch(dc->state) {
	case DHCLNT_STATE_INIT:
		LOG_DEBUG ("DHCLNT_STATE_INIT\n");

		/*
		 * Send DHCPDISCOVER and transition in DHCLNT_STATE_SELECTING
		 */
		dc->retries = 0;
		dc->state = DHCLNT_STATE_SELECTING;

		dc->xid = dpd->xid++;

		dhcpv4_create_request(cfg_data, DHCP_MSGTYPE_DISCOVER);

		retval = 0;
		break;

	case DHCLNT_STATE_SELECTING:
		LOG_DEBUG ("DHCLNT_STATE_SELECTING\n");

		switch (event->type) {
		case EVENT_RELEASE:

			LOG_DEBUG("EVENT_RELEASE");

			/*
			 * Release has been requested. Remove all timeouts and
			 * so transit into FINISHED state.
			 */
			if (dc->to) {
				timeout_cancel(dc->to);
				dc->to = NULL;
			}

			dc->state = DHCLNT_STATE_FINISHED;
			break;

		case EVENT_PACKET_RECEIVED:

			LOG_DEBUG("EVENT_PACKET_RECEIVED");

			/*
			 * We expect only DHCPOFFER message in this state
			 */
			if (event->dd->dhcp_type != DHCP_MSGTYPE_OFFER) {
				LOG_ERROR ("We didn't receive DHCPOFFER");
				break;
			}

			dhcpv4_merge_dhcp_response(cfg_data, event->dd);

			/*
			 * Prepare DHCPREQUEST message
			 */
			dc->retries = 0;

			/*
			 * Transition into REQUESTING state
			 */
			dc->state = DHCLNT_STATE_REQUESTING;

			if (dc->msg) {
				g_free(dc->msg);
				dc->msg = NULL;
				dc->msg_size = 0;
			}

			dc->xid = dpd->xid++;
			dhcpv4_create_request(cfg_data, DHCP_MSGTYPE_REQUEST);

			break;

		case EVENT_TIMEOUT:

			LOG_DEBUG("EVENT_TIMEOUT");

			/*
			 * Timeout occured. Notify user and transition
			 * into ERROR state.
			 */
			dc->state = DHCLNT_STATE_ERROR;

			/*
			 * Send notification
			 */
			if (dc->queue && dc->data)
				g_async_queue_push(dc->queue, dc->data);

			break;

		default:
			break;
		}
		break;

	case DHCLNT_STATE_REQUESTING:
		LOG_DEBUG ("DHCLNT_STATE_REQUESTING\n");

		switch (event->type) {
		case EVENT_RELEASE:
			/*
			 * Release has been requested. Remove all timeouts and
			 * so transit into FINISHED state.
			 */
			if (dc->to) {
				timeout_cancel(dc->to);
				dc->to = NULL;
			}

			dc->state = DHCLNT_STATE_FINISHED;
			break;

		case EVENT_PACKET_RECEIVED:

			/*
			 * If we received NAK then start all over again
			 */
			if (event->dd->dhcp_type == DHCP_MSGTYPE_NAK) {
				dhcpv4_client_reset(cfg_data);
				dhcpv4_client_step(cfg_data, NULL);
				break;
			}

			/*
			 * We expect DHCPACK message here
			 */
			if (event->dd->dhcp_type != DHCP_MSGTYPE_ACK) {
				LOG_ERROR ("We didn't receive DHCPOFFER");
				break;
			}

			/*
			 * Transition into BOUND state
			 */
			dc->state = DHCLNT_STATE_BOUND;

			if (dc->msg) {
				g_free(dc->msg);
				dc->msg = NULL;
				dc->msg_size = 0;
			}

			/*
			 * Send notification
			 */
			if (dc->queue && dc->data)
				g_async_queue_push(dc->queue, dc->data);

			break;

		case EVENT_TIMEOUT:
			/*
			 * Timeout occured. Notify user and transition
			 * into ERROR state.
			 */
			dc->state = DHCLNT_STATE_ERROR;

			/*
			 * Send notification
			 */
			if (dc->queue && dc->data)
				g_async_queue_push(dc->queue, dc->data);

			break;

		default:
			break;
		}
		break;

	case DHCLNT_STATE_BOUND:
		LOG_DEBUG ("DHCLNT_STATE_BOUND\n");

		switch (event->type) {
		case EVENT_T1_EXPIRED:
			/*
			 * T1 expired and we have to renew lease
			 */
			dc->retries = 0;
			dc->state = DHCLNT_STATE_RENEWING;
			break;

		case EVENT_RELEASE:
			/*
			 * We were requested to release resources
			 */
			dc->xid = dpd->xid++;
			dhcpv4_create_request(cfg_data, DHCP_MSGTYPE_RELEASE);

			dc->state = DHCLNT_STATE_FINISHED;
			break;

		default:
			break;
		}

		break;

	case DHCLNT_STATE_RENEWING:
		LOG_DEBUG ("DHCLNT_STATE_RENEWING\n");

		switch (event->type) {
		case EVENT_T1_EXPIRED:
			/*
			 * T1 expired and we have to renew lease
			 */
			dc->retries = 0;
			dc->state = DHCLNT_STATE_RENEWING;
			break;

		case EVENT_T2_EXPIRED:
			/*
			 * T2 expired and we have to renew lease
			 */
			dc->state = DHCLNT_STATE_RENEWING;
			break;

		case EVENT_PACKET_RECEIVED:

			/*
			 * We were requested to release resources
			 */
			switch(event->dd->dhcp_type) {
			case DHCP_MSGTYPE_NAK:
				break;
			case DHCP_MSGTYPE_ACK:
				break;
			}

			dc->retries = 0;
			dc->state = DHCLNT_STATE_FINISHED;
			break;

		case EVENT_RELEASE:
			/*
			 * We were requested to release resources
			 */
			dc->xid = dpd->xid++;
			dhcpv4_create_request(cfg_data, DHCP_MSGTYPE_RELEASE);

			dc->state = DHCLNT_STATE_FINISHED;
			break;

		default:
			break;
		}

		break;

	case DHCLNT_STATE_REBINDING:
		LOG_DEBUG ("DHCLNT_STATE_REBINDING\n");

		switch(event->type) {
		case EVENT_RELEASE:
			/*
			 * We were requested to release resources
			 */
			dc->xid = dpd->xid++;
			dhcpv4_create_request(cfg_data, DHCP_MSGTYPE_RELEASE);

			dc->state = DHCLNT_STATE_FINISHED;
			break;

		default:
			break;
		}

		break;

	case DHCLNT_STATE_ERROR:
		LOG_DEBUG ("DHCLNT_STATE_ERROR\n");

		break;

	case DHCLNT_STATE_FINISHED:
		LOG_DEBUG ("DHCLNT_STATE_FINISHED\n");

		break;
	}

	return retval;
}

void *dhcpv4_client_get_msg(struct dhcpv4_client *dc)
{
	return dc->msg;
}

int dhcpv4_client_get_msg_size(struct dhcpv4_client *dc)
{
	return dc->msg_size;
}

struct in_addr *dhcpv4_client_get_server(struct dhcpv4_client *dc)
{
	return &dc->dhcpv4_cfg->server;
}

guint16 dhcpv4_client_get_port(struct dhcpv4_client *dc)
{
	return dc->dhcpv4_cfg->port;
}

void *dhcpv4_client_get_dhcrelay_hwaddr(struct dhcpv4_client *dc)
{
	return dc->dhcpv4_cfg->dhcrelay_hwaddr;
}

/**
 * Is the client in state BOUND?
 *
 * @param dc
 *
 * \return TRUE if it is, FALSE otherwise
 */
gboolean dhcpv4_client_is_bound(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	return (dc->state == DHCLNT_STATE_BOUND);
}

/**
 * Is the client in state ERROR?
 *
 * @param cfg_data
 *
 * \return TRUE if it is, FALSE otherwise
 */
gboolean dhcpv4_client_is_error(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	return (dc->state == DHCLNT_STATE_ERROR);
}

/**
 * Return current client's state
 *
 * @param cfg_data
 *
 * \return TRUE if it is, FALSE otherwise
 */
gchar *dhcpv4_client_get_state(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	switch (dc->state) {
	case DHCLNT_STATE_INIT:
		return "DHCLNT_STATE_INIT";
	case DHCLNT_STATE_SELECTING:
		return "DHCLNT_STATE_SELECTING";
	case DHCLNT_STATE_REQUESTING:
		return "DHCLNT_STATE_REQUESTING";
	case DHCLNT_STATE_BOUND:
		return "DHCLNT_STATE_BOUND";
	case DHCLNT_STATE_RENEWING:
		return "DHCLNT_STATE_RENEWING";
	case DHCLNT_STATE_REBINDING:
		return "DHCLNT_STATE_REBINDING";
	case DHCLNT_STATE_FINISHED:
		return "DHCLNT_STATE_FINISHED";
	case DHCLNT_STATE_ERROR:
		return "DHCLNT_STATE_ERROR";
	default:
		return "UNKNOWN";
	}
}

/**
 * Dump current state into a log file
 *
 * @param dc
 *
 * \return TRUE if it is, FALSE otherwise
 */
void dhcpv4_client_dump(struct cfg_data *cfg_data)
{
	char ipaddr[40];
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	LOG_NOTICE("State %s", dhcpv4_client_get_state(cfg_data));
	LOG_NOTICE("Retries %u", dc->retries);
	LOG_NOTICE("Server ID %s", inet_ntop(AF_INET, &dc->serverid,
						ipaddr, 40));
}

/**
 * Request parameters for a client
 *
 * @param cfg_data	Client for whom request should be made
 * @param queue		In case request is asynchronous, queue on which
 *			message	about completition will be sent.
 * @param data		Data to be sent on a queue
 *
 * \return	-1 if an error occured
 *		0 if request has been satisfied
 *		1 if request is in progress
 */
int dhcpv4_client_request(struct cfg_data *cfg_data,
		GAsyncQueue *queue, gpointer data)
{
	struct dhcpv4_client *dc;
	gint retval = 0;

	LOG_FUNC_START(1);

	dc = cfg_data->provider4_data;

	/*
	 * Make a first step, i.e. request to a server
	 */
	dhcpv4_client_step(cfg_data, NULL);

	if (dhcpv4_client_has_request(cfg_data))
		dhcpv4_send_msg(cfg_data);

	if (dc->state != DHCLNT_STATE_FINISHED
			&& dc->state != DHCLNT_STATE_ERROR) {
		dc->queue = queue;
		dc->data = data;

		retval = 1;
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Initiate RELEASE of acquired resources
 *
 * @param dc	Client whose resources have to be released
 *
 * \return	-1 if an error occured
 *		0 if request has been satisfied
 *		1 if request is in progress
 *
 * Note that in case we are not in bound state, this function
 * will behave as CANCEL function, i.e. it will terminate
 * client (but not free resources!).
 */
int dhcpv4_client_release(struct cfg_data *cfg_data, GAsyncQueue *queue,
		gpointer data)
{
	struct dhcpv4_event de;
	struct dhcpv4_client *dc; 

	dc = cfg_data->provider4_data;

	de.type = EVENT_RELEASE;
	dhcpv4_client_step(cfg_data, &de);

	if (dhcpv4_client_has_request(cfg_data))
		dhcpv4_send_msg(cfg_data);

	if (dc->state == DHCLNT_STATE_FINISHED
			|| dc->state == DHCLNT_STATE_ERROR)
		return 0;

	/*
	 * If the queue and/or data are NULL than the caller is not
	 * interested in the return value
	 */
	if (queue == NULL || data == NULL)
		return 0;

	dc->queue = queue;
	dc->data = data;

	return 1;
}

/**
 * Is there a request that should be sent?
 *
 * @param cfg_data
 *
 * \return TRUE if there is, FALSE otherwise
 */
gboolean dhcpv4_client_has_request(struct cfg_data *cfg_data)
{
	struct dhcpv4_client *dc;

	dc = cfg_data->provider4_data;

	return (dc->msg != NULL && dc->msg_size > 0);
}

/**
 * Callback function to process received network traffic on a private socket
 *
 * @param data
 *
 * Process received packet
 */
gpointer dhcpv4_network_cb(gpointer data)
{
	GSList *c;
	struct dhcpv4_event *de;
	struct dhcpv4_client *dc;
	struct network_msg *msg = data;

	de = dhcpv4_event_new();

	de->type = EVENT_PACKET_RECEIVED;

	if ((de->dd = dhcpv4_packet_parse(msg->buffer, msg->size)) != NULL) {

		g_static_rw_lock_reader_lock(&dpd->clients_lock);
		for (c = dpd->clients; c; c = c->next) {
			de->cfg_data = c->data;
			dc = de->cfg_data->provider4_data;

			if (dc->xid == de->dd->xid) {

				if (dc->to) {
					timeout_cancel(dc->to);
					dc->to = NULL;
				}

				break;
			}
		}
		g_static_rw_lock_reader_unlock(&dpd->clients_lock);

		if (!c || de->dd->bootp_type != BOOTPREPLY) {
			dhcpv4_event_free(de);
			de = NULL;
		}

		network_msg_free(msg);

		/*
		 * If error has been detected stop further processing
		 */
		if (!de)
			return NULL;

		dhcpv4_client_step(de->cfg_data, de);

		/*
		 * Is there anything we should send?
		 */
		if (dhcpv4_client_has_request(de->cfg_data))
			dhcpv4_send_msg(de->cfg_data);

	}

	return NULL;
}

/**
 * Callback function to process timeouts
 *
 * @param data
 *
 * Process received packet
 */
gpointer dhcpv4_timeout_cb(gpointer data)
{
	struct dhcpv4_event *de = data;
	struct dhcpv4_client *dc;

	dc = de->cfg_data->provider4_data;

	/*
	 * Lock structure
	 */
	g_mutex_lock(dc->mutex);

	/*
	 * Check that the problem was resloved
	 * while we waited on a a lock
	 */
	if (!dc->to)
		goto out;

	/*
	 * Otherwise, problem was not resolved so check the
	 * number of retries and if we didn't reach the
	 * maximum then retry.
	 */
	if (++dc->retries <= dc->dhcpv4_cfg->retries) {
		dhcpv4_send_msg(de->cfg_data);
		goto out;
	}

	/*
	 * We reached maxium so client has to be notified about
	 * timeout condition.
	 */
	dhcpv4_client_step(de->cfg_data, de);

out:
	g_mutex_unlock(dc->mutex);

	return NULL;
}


/******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/

/**
 * Unload DHCPv4 provider
 *
 * @param ccp	Configuration and state data for a single instance
 */
void cfg_dhcpv4_instance_free(struct cfg_config_provider *ccp)
{
	dhcpv4_cfg_free(ccp->data);
	ccp->data = NULL;

	return;
}

/**
 * Initialize instance of a DHCPv4 provider
 *
 * @param ccp	Configuration and state data for a single instance
 *
 * \return 0 if successful, -1 in case of an error
 *
 * Single instance uses single DHCP server.
 */
gint cfg_dhcpv4_instance_init(struct cfg_config_provider *ccp)
{
	GSList *c;
	struct cfg_av *ca;
	struct dhcpv4_cfg *dc;
	gint retval = -1;

	LOG_FUNC_START(1);

	ccp->data = dc = dhcpv4_cfg_new();

	/*
	 * Parse attribute-value list and generate configuration
	 */
	for (c = ccp->av; c; c = c->next) {
		ca = c->data;

		if (!strcasecmp(DHCPV4_ATTR_SERVER, ca->attribute)) {
			if (dc->server.s_addr)
				LOG_WARNING("Duplicate server specification "
						"in line %u", ca->line);

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Expecting single value for server"
						"in line %u. Ignoring line.",
						ca->line);
				continue;
			}

			if (inet_pton(AF_INET, ca->string,
					&dc->server) < 0)
				LOG_PERROR(LOG_PRIORITY_ERROR);

		} else if (!strcasecmp(DHCPV4_ATTR_RELAY, ca->attribute)) {

			if (dc->relay.s_addr)
				LOG_ERROR("Duplicate relay specification "
						"in line %u", ca->line);

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Expecting single value for relay"
						"in line %u. Ignoring line.",
						ca->line);
				continue;
			}

			if (inet_pton(AF_INET, ca->string,
					&dc->relay) < 0)
				LOG_PERROR(LOG_PRIORITY_ERROR);

		} else if (!strcasecmp(DHCPV4_ATTR_PORT, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Expecting single value for timeout"
						"in line %u. Ignoring line.",
						ca->line);
				continue;
			}

			dc->port = atoi(ca->string);

		} else if (!strcasecmp(DHCPV4_ATTR_TIMEOUT, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Expecting single value for timeout"
						"in line %u. Ignoring line.",
						ca->line);
				continue;
			}

			dc->timeout = atoi(ca->string);

		} else if (!strcasecmp(DHCPV4_ATTR_RETRY, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Expecting single value for retry"
						"count in line %u. Ignoring "
						"line.", ca->line);
				continue;
			}

			dc->retries = atoi(ca->string);

		} else {
			LOG_ERROR("Unrecognized attribute %s in line %u",
					ca->attribute, ca->line);
		}
	}

	/**
	 * Check if port was given...
	 */
	if (!dc->port)
		dc->port = DHCP_SERVER_PORT;

	/**
	 * Do we have the server's address...
	 */
	if (!dc->server.s_addr) {
		LOG_ERROR("No DHCP server was specified");
		dhcpv4_cfg_free(ccp->data);
		ccp->data = NULL;
		goto out;
	}

	/**
	 * Do we have a relay's IP address...
	 */
	if (!dc->relay.s_addr) {
		LOG_ERROR("No relay address was specified");
		dhcpv4_cfg_free(ccp->data);
		ccp->data = NULL;
		goto out;
	}

	ccp->provider = provider_new();

	ccp->provider->pid = "dhcp";
	ccp->provider->flags = OPTION_F4_ADDR
				| OPTION_F4_NETMASK
				| OPTION_F4_DNS
				| OPTION_F4_DHCP
				| OPTION_F4_NBNS;

	ccp->provider->init = dhcpv4_client_init;
	ccp->provider->free = dhcpv4_client_free;
	ccp->provider->reset = dhcpv4_client_reset;
	ccp->provider->isbound = dhcpv4_client_is_bound;
	ccp->provider->iserror = dhcpv4_client_is_error;
	ccp->provider->request = dhcpv4_client_request;
	ccp->provider->release = dhcpv4_client_release;
	ccp->provider->state = dhcpv4_client_get_state;
	ccp->provider->dump = dhcpv4_client_dump;

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Unload DHCPv4 provider
 *
 * @param cfg	Configuration data for provider
 */
void cfg_dhcpv4_unload()
{
	if (dpd) {

		if (dpd->ns)
			network_socket_free(dpd->ns);

		g_static_rw_lock_free(&dpd->clients_lock);

		g_free(dpd);
	}

	return;
}

/**
 * Global initialization of DHCPv4 provider
 *
 * \return 0 if successful, -1 in case of an error
 */
gint cfg_dhcpv4_init()
{
	struct netaddr *na;
	gint retval = -1;

	LOG_FUNC_START(1);

	retval = -1;

	dpd = g_malloc0(sizeof(struct dhcpv4_provider_data));

	/*
	 * Initialize client state list
	 */
	dpd->clients = NULL;

	g_static_rw_lock_init(&dpd->clients_lock);

	/*
	 * Initialize network data
	 */
	na = netaddr_new();

	netaddr_set_family(na, AF_INET);
	netaddr_set_port(na, DHCP_SERVER_PORT);

	if ((dpd->ns = network_socket_register_thread(AF_INET,
			SOCK_DGRAM, 0, na, 0,
			dhcpv4_network_cb)) == NULL) {
		cfg_dhcpv4_unload();
		goto out;
	}

	/*
	 * Initialize XID value
	 */
	dpd->xid = 128;

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

#endif /* CFG_DHCPV4 */
