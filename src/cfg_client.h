/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __CFG_CLIENT_H
#define __CFG_CLIENT_H

#define IKEV2_CLIENT_ENV_ADDR4		"IKEV2_IPV4_ADDR"
#define IKEV2_CLIENT_ENV_NETMASK4	"IKEV2_IPV4_NETMASK"
#define IKEV2_CLIENT_ENV_ADDR6		"IKEV2_IPV6_ADDR"
#define IKEV2_CLIENT_ENV_EXPIRES4	"IKEV2_IPV4_EXPIRES"
#define IKEV2_CLIENT_ENV_EXPIRES6	"IKEV2_IPV6_EXPIRES"
#define IKEV2_CLIENT_ENV_DHCP4		"IKEV2_IPV4_DHCP"
#define IKEV2_CLIENT_ENV_DHCP6		"IKEV2_IPV6_DHCP"
#define IKEV2_CLIENT_ENV_DNS4		"IKEV2_IPV4_DNS"
#define IKEV2_CLIENT_ENV_DNS6		"IKEV2_IPV6_DNS"
#define IKEV2_CLIENT_ENV_NBNS		"IKEV2_IPV4_NBNS"
#define IKEV2_CLIENT_ENV_SUBNETS4	"IKEV2_VPN_SUBNETS4"
#define IKEV2_CLIENT_ENV_SUBNETS6	"IKEV2_VPN_SUBNETS6"
#define IKEV2_CLIENT_ENV_ID		"IKEV2_CLIENT_ID"
#define IKEV2_CLIENT_ENV_IF		"IKEV2_INTERFACE"
#define IKEV2_CLIENT_ENV_VPN_IP4_ADDR	"IKEV2_VPN_IPV4_ADDR"
#define IKEV2_CLIENT_ENV_VPN_IP6_ADDR	"IKEV2_VPN_IPV6_ADDR"

/*******************************************************************************
 * Public interface
 ******************************************************************************/
int cfg_client_exec_script(gchar **, struct cfg *, struct id *,
		struct netaddr *, gchar *);

#endif /* __CFG_CLIENT_H */
