/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/**
 * \file cfg.c
 *
 * \brief Server side CFG payload processing routines
 *
 * First, note on some terminology. By "provider" we mean speciallized
 * module that contacts source of configuration data. For example, DHCPv4
 * has a provider in files cfg_dhcpv4.[ch] that request configuration
 * parameters from DHCPv4 server. By "configuration module" we assume
 * this module. It role is to isolate the rest of IKEv2 from the specifics
 * of each possible provider.
 *
 * This subsystem is designed with the following assumptions:
 *
 * - it does not do failover
 * - each provider will be used only once
 * - when creating new proxy, adding providers is stopped as soon as all
 *   the configuration parameters are covered
 */

#define __CFG_MODULE_C

#ifdef CFG_MODULE

#define LOGGERNAME	"cfg_module"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <wait.h>

#include <glib.h>
#include <gmodule.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "timeout.h"
#include "auth.h"

#include "cfg.h"
#include "cfg_module.h"

#ifdef CFG_DHCPV4
#include "cfg_dhcpv4.h"
#endif

#ifdef CFG_DHCPV6
#include "cfg_dhcpv6.h"
#endif

#ifdef CFG_PRIVATE
#include "cfg_private.h"
#endif

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#define MAX_ENVLINE_LEN		256
#define MAX_IPBUF_SIZE		80

/**
 * Thread for receiving asynchronous messages from providers
 */
GThread *thread = NULL;

/**
 * Queue on which thread waits for messages
 */
GAsyncQueue *cfg_queue = NULL;

/**
 * Queue on which thread sends messages to state machines
 */
GAsyncQueue *sm_queue = NULL;

/**
 * Lock to protect provider list during configuration reloads
 */
GMutex *ccp_lock;

/**
 * List of all available configuration providers
 */
GSList *cfg_config_providers = NULL;

/**
 * List of all available providers for dynamic data
 */
// GSList *providers = NULL;

/**
 * List of all the loaded modules
 */
GSList *modules = NULL;

/*******************************************************************************
 * config related functions
 ******************************************************************************/

struct cfg_config_provider *cfg_config_provider_new()
{
	struct cfg_config_provider *ccp;

	ccp = g_malloc0(sizeof(struct cfg_config_provider));
	ccp->refcnt = 1;

	return ccp;
}

void cfg_config_provider_ref(struct cfg_config_provider *ccp)
{
	g_atomic_int_inc(&ccp->refcnt);
}

void cfg_config_provider_free(struct cfg_config_provider *ccp)
{
	if (ccp && g_atomic_int_dec_and_test(&ccp->refcnt)) {

		if (ccp->id)
			g_free(ccp->id);

		if (ccp->provider) {

#ifdef CFG_DHCPV4
			if (ccp->provider_type == CFG_PROTO_DHCPV4)
				cfg_dhcpv4_instance_free(ccp);
#endif /* CFG_DHCPV4 */

#ifdef CFG_PRIVATE
			if (ccp->provider_type == CFG_PROTO_PRIVATE)
				cfg_private_instance_free(ccp);
#endif /* CFG_PRIVATE */

#ifdef CFG_DHCPV6
			if (ccp->provider_type == CFG_PROTO_DHCPV6)
				cfg_dhcpv6_instance_free(ccp);
#endif /* CFG_DHCPV6 */

		}

		while(ccp->av) {
			cfg_av_free(ccp->av->data);
			ccp->av = g_slist_remove(ccp->av, ccp->av->data);
		}
	}
}

void cfg_config_provider_dump(struct cfg_config_provider *ccp)
{
	GSList *c;

	if (ccp) {
		LOG_NOTICE("id %s", ccp->id);
#ifdef CFG_DHCPV4
//		if (ccp->type == CFG_PROTO_DHCPV4)
//			LOG_DEBUG("type CFG_PROTO_DHCPV4");
#endif
#ifdef CFG_DHCPV6
//		if (ccp->type == CFG_PROTO_DHCPV6)
//			LOG_DEBUG("type CFG_PROTO_DHCPV6");
#endif
#ifdef CFG_FILE
//		if (ccp->type == CFG_PROTO_FILE)
//			LOG_DEBUG("type CFG_PROTO_FILE");
#endif

		for(c = ccp->av; c; c = c->next)
			cfg_av_dump(c->data);
	}
}

struct cfg_av *cfg_av_new()
{
	return g_malloc0(sizeof(struct cfg_av));
}

void cfg_av_free(struct cfg_av *ca)
{
	if (ca) {

		if (ca->attribute)
			g_free(ca->attribute);

		switch(ca->val_type) {
		case CFG_AV_TYPE_SINGLE:
			g_free(ca->string);
			break;

		case CFG_AV_TYPE_RANGE:
			g_free(ca->lower);
			g_free(ca->upper);
			break;

		case CFG_AV_TYPE_LIST:
			while (ca->list) {
				g_free(ca->list->data);
				ca->list = g_slist_remove(ca->list,
							ca->list->data);
			}
			break;
		}

		g_free(ca);
	}
}

void cfg_av_dump(struct cfg_av *ca)
{
	GSList *c;

	if (ca) {

		switch(ca->val_type) {
		case CFG_AV_TYPE_SINGLE:
			LOG_DEBUG("av: %s = %s", ca->attribute, ca->string);
			break;

		case CFG_AV_TYPE_RANGE:
			LOG_DEBUG("av: %s = %s - %s", ca->attribute,
					ca->lower, ca->upper);
			break;

		case CFG_AV_TYPE_LIST:
			LOG_DEBUG("av: %s (list)");
			for (c = ca->list; c; c = c->next)
				LOG_DEBUG("%s", ca->list->data);
			break;
		}
	}
}

/*******************************************************************************
 * struct cfg_msg related functions
 ******************************************************************************/
struct cfg_msg *cfg_msg_new()
{
	struct cfg_msg *cm;

	cm = g_malloc0(sizeof(struct cfg_msg));

	cm->msg_type = CFG_MSG;

	return cm;
}

void cfg_msg_free(struct cfg_msg *cm)
{
	if (cm && g_atomic_int_dec_and_test(&(cm->refcnt))) {
		g_free(cm);
	}
}

/*******************************************************************************
 * struct provider related functions
 ******************************************************************************/

struct provider *provider_new()
{
	return g_malloc0(sizeof(struct provider));
}

void provider_free(struct provider *provider)
{
	if (g_atomic_int_dec_and_test(&provider->refcnt)) {
//		provider->release(provider);
		g_free(provider);
	}
}

/*******************************************************************************
 * struct cfg_data related functions
 ******************************************************************************/

void cfg_data_free(struct cfg_data *cd)
{
	if (cd) {

		while(cd->ccpl) {
			cd->ccpl = g_slist_remove(cd->ccpl,
						cd->ccpl->data);
		}

		cfg_free(cd->cfg);

		id_free(cd->cid);
	}
}

/*******************************************************************************
 * Thread that receives messages from providers
 ******************************************************************************/

/**
 *
 */
gpointer cfg_main_thread(gpointer data)
{
	struct cfg_msg *msg;

	LOG_FUNC_START(1);

	while(1) {

		/**
		 * Wait for a message
		 */
		msg = g_async_queue_pop(cfg_queue);

		LOG_TRACE("Message received");

		if (GPOINTER_TO_UINT(msg) == 1) {
			LOG_DEBUG("Got terminate message");
			break;
		}

		/**
		 * Check if we received all the responses
		 */

		/**
		 * Send message to state machines
		 */
		g_async_queue_push(sm_queue, msg);
	}

	LOG_FUNC_END(1);

	return NULL;
}

/*******************************************************************************
 * Public client interface
 ******************************************************************************/

/**
 * Initialize provider for a single peer configuration data
 *
 * @param providers	List of provider ID's to use to obtain
 *			configuration data
 * @param cfg		Request data received from a peer
 * @param cid		Client ID
 *
 * \return CFG handle for subsequent requests, or NULL in case of an error
 */
gpointer cfg_proxy_new(GSList *provider, struct cfg *cfg, struct id *cid)
{
	GSList *c;
	struct cfg_data *cd;
	struct cfg_config_provider *ccp;
	guint32 flags;

	LOG_FUNC_START(1);

	cd = g_malloc0(sizeof(struct cfg_data));

	cd->cid = g_memdup(cid, sizeof(struct id));

	cd->cfg = cfg_refcnt_inc(cfg);

	flags = cfg->flags;

	/*
	 * Search through providers until we cover all the requests
	 * parameters, or until there are no more providers to try.
	 */
	g_mutex_lock(ccp_lock);
	while (provider && flags) {

		for (c = cfg_config_providers; c && flags; c = c->next) {

			ccp = c->data;

			if (!strcmp(provider->data, ccp->id)) {

				/*
				 * If current provider doesn't offer any of the
				 * requested parameters, then skip it.
				 */
				if (!(ccp->flags & flags))
					continue;

				if (ccp->provider->init(cd, ccp) < 0) {
					LOG_ERROR("Failed to initialize "
							"provider %s\n",
							provider->data);
					cfg_proxy_free(cd);
					cd = NULL;
					g_mutex_unlock(ccp_lock);
					goto out;
				}

				LOG_NOTICE("Using configuration %s", ccp->id);
				cd->ccpl = g_slist_append(cd->ccpl, ccp);
				cfg_config_provider_ref(ccp);
				flags &= ~(ccp->flags & ccp->flags);

				break;
			}

		}

		if (c == NULL) {
			LOG_WARNING("Could not find provider ID %s",
					provider->data);
			cfg_proxy_free(cd);
			cd = NULL;
			g_mutex_unlock(ccp_lock);
			goto out;
		}

		provider = provider->next;
	}
	g_mutex_unlock(ccp_lock);

	/*
	 * Check if all the requested parameters can be provided...
	 */
	if (flags) {
		LOG_WARNING("Not all requested parameters will be provided!");
		LOG_DEBUG("Parameters requested %u, not covered are %u",
				 cfg->flags, flags);
	}

out:
	LOG_FUNC_END(1);

	return cd;
}

/**
 * Obtain configuration data as specified in configuration structure
 *
 * @param cd
 * @param cookie	Generic pointer that will be used by state machines
 *			to corellate message with session
 *
 * \return	-1 An error occured and the request can not be satisified
 *		 0 Configuration data has been acquired
 *		 1 Request has to be repeated later
 */
int cfg_proxy_request(struct cfg_data *cd, gpointer session)
{
	GSList *c;
	guint32 flags;
	gint ret, retval;
	struct cfg_config_provider *ccp;
	struct cfg_msg *msg;

	LOG_FUNC_START(1);

	msg = cfg_msg_new();

	msg->session = session;

	retval = 0;

	flags = cd->cfg->flags;
	for (c = cd->ccpl; c && flags; c = c->next) {
		ccp = c->data;

		ret = ccp->provider->request(cd, cfg_queue, msg);

		/*
		 * If error occured, rollback all the previous requests
		 */
		if (ret < 0) {
			cfg_proxy_release(cd);

			retval = -1;
			break;
		}

		/*
		 * If any of the providers returns that request should
		 * be waited for, then the total result is that callee
		 * should also wait until last provider returns result.
		 */
		if (ret > 0)
			retval = 1;

		/*
		 * Update flags
		 */
		flags &= ~ccp->flags;
	}

	if (flags)
		LOG_WARNING("Not all parameters were requested!");

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Release configuration data
 *
 * @param cd
 */
void cfg_proxy_release(struct cfg_data *cd)
{
	GSList *c;
	struct cfg_config_provider *ccp;
	gint retval;

	LOG_FUNC_START(1);

	for (c = cd->ccpl; c; c = c->next) {
		ccp = c->data;
		ccp->provider->release(cd, NULL, NULL);
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Free configuration structure
 */
void cfg_proxy_free(struct cfg_data *cd)
{
	GSList *c;
	struct cfg_config_provider *ccp;

	LOG_FUNC_START(1);

	if (cd) {

		/**
		 * Check all the providers, and the ones not used any more
		 * should be free'd.
		 */
		g_mutex_lock(ccp_lock);
		while (cd->ccpl) {
			ccp = cd->ccpl->data;
			ccp->provider->free(cd);
			cfg_config_provider_free(ccp);
			cd->ccpl = cd->ccpl->next;
		}
		g_mutex_unlock(ccp_lock);

		if (cd->cfg)
			cfg_free(cd->cfg);

		if (cd->cid)
			id_free(cd->cid);

		g_free(cd);
	}

	LOG_FUNC_END(1);
}

/**
 * Get current state
 */
gint cfg_proxy_get_state(struct cfg_data *cd)
{
	return 0;
}

/**
 * Get configuration structure
 */
struct cfg *cfg_proxy_get_cfg(struct cfg_data *cfg_data)
{
	return cfg_data->cfg;
}

/**
 * Get assigned network addrsses
 */
GSList *cfg_proxy_get_netaddrs(struct cfg_data *cd)
{
	return cfg_get_netaddrs(cd->cfg);
}

/**
 * Check if error occured
 *
 * \return TRUE if error occured, FALSE if not
 */
gboolean cfg_proxy_is_error(struct cfg_data *cd)
{
	GSList *c;
	struct cfg_config_provider *ccp;

	/*
	 * In order that everyting is OK, each provider should
	 * return error free state. The first that reports
	 * error, we immediately return error condition.
	 */
	for (c = cd->ccpl; c; c = c->next) {
		ccp = c->data;
		if (!ccp->provider->iserror(cd))
			return FALSE;
	}

	return TRUE;
}

/**
 * Dump configuration data
 */
void cfg_proxy_dump(struct cfg_data *cd)
{
	struct cfg_config_provider *ccp;
	GSList *c;

	LOG_DEBUG("Dumping cfg_data structure on address %p", cd);
	id_dump(LOGGERNAME, cd->cid);

	cfg_dump(cd->cfg);

	for (c = cd->ccpl; c; c = c->next) {
		ccp = c->data;
		ccp->provider->dump(cd);
	}
}

/**
 * Retrieve client's ID
 *
 * Data returned should not be manipulated in any way, but treated as a
 * read-only data.
 */
struct id *cfg_proxy_get_cid(gpointer cfg_data)
{
	return ((struct cfg_data *)cfg_data)->cid;
}

/*******************************************************************************
 * Running external scripts to adjust environment for a new client
 ******************************************************************************/

/**
 * Put variable into environemnt
 *
 * \return This function does not return
 *
 * In case of an error, exit() is called since we assume that we run
 * from fork()-ed process.
 */
static void newenv(char *c)
{
	LOG_DEBUG("New env item '%s'", c);

	if (putenv(g_strdup(c)) < 0) {
		LOG_PERROR(LOG_PRIORITY_FATAL);
		exit(EXIT_FAILURE);
	}
}

/**
 * Execute script to update network parameters received from responder
 *
 * @param argv		Script to execute, along with additional parameters
 * @param cfg		Configuration data
 * @param clntaddr	Client's public address
 * @param interface	Interface connecting us to the client
 *
 * \return 0 if execution was successful, -1 otherwise
 */
int cfg_module_exec_script(gchar **argv, struct cfg *cfg,
		struct netaddr *clntaddr, gchar *interface,
		struct netaddr *vpngwaddr)
{
	GSList *c;
	pid_t pid;
	char buf[MAX_ENVLINE_LEN];
	char ipbuf[MAX_IPBUF_SIZE];
	pid_t status;
	gint retval;

	retval = -1;

	pid = fork();

	if (pid < 0) {
		LOG_PERROR(LOG_PRIORITY_ERROR);
		goto out;
	}

	if (!pid) {

		/*
		 * Child process should prepare environment and
		 * execute startup script.
		 */
		strcpy(buf, IKEV2_MODULE_ENV_IF);
		strcat(buf, "=");
		strcat(buf, interface);
		newenv(buf);

		if (clntaddr) {
			if (netaddr_get_family(clntaddr) == AF_INET)
				strcpy(buf, IKEV2_MODULE_ENV_RW_IP4_ADDR);
			else
				strcpy(buf, IKEV2_MODULE_ENV_RW_IP6_ADDR);

			strcat(buf, "=");
			netaddr_ip2str(clntaddr, ipbuf, MAX_IPBUF_SIZE);
			strcat(buf, ipbuf);
			newenv(buf);
		}

		if (vpngwaddr) {
			if (netaddr_get_family(vpngwaddr) == AF_INET)
				strcpy(buf, IKEV2_MODULE_ENV_VPNGW_IPV4_ADDR);
			else
				strcpy(buf, IKEV2_MODULE_ENV_VPNGW_IPV6_ADDR);

			strcat(buf, "=");
			netaddr_ip2str(vpngwaddr, ipbuf, MAX_IPBUF_SIZE);
			strcat(buf, ipbuf);
			newenv(buf);
		}

		if (cfg->flags & OPTION_F4_ADDR) {

			/*
			 * If there are IPv4 addresses put them into
			 * environemnt variable
			 */
			strcpy(buf, IKEV2_MODULE_ENV_ADDR4);
			strcat(buf, "=");

			for (c = cfg->netaddrs; c; c = c->next) {

				if (netaddr_get_family(c->data) == AF_INET) {
					netaddr_ip2str(c->data, ipbuf,
							MAX_IPBUF_SIZE);
					strcat(buf, ipbuf);
					strcat(buf, " ");
				}
			}

			if (strlen(buf) > sizeof(IKEV2_MODULE_ENV_ADDR4) + 1)
				newenv(buf);
		}

		if (cfg->flags & OPTION_F6_ADDR) {

			/*
			 * If there are IPv4 addresses put them into
			 * environemnt variable
			 */
			strcpy(buf, IKEV2_MODULE_ENV_ADDR6);
			strcat(buf, "=");

			for (c = cfg->netaddrs; c; c = c->next) {

				if (netaddr_get_family(c->data) == AF_INET6) {
					netaddr_ip2str(c->data, ipbuf,
							MAX_IPBUF_SIZE);
					strcat(buf, ipbuf);
					strcat(buf, " ");
				}
			}

			if (strlen(buf) > sizeof(IKEV2_MODULE_ENV_ADDR6) + 1)
				newenv(buf);
		}

		execv(argv[0], argv);

		/*
		 * If we reached this part then there was error in the
		 * previous function.
		 */
		LOG_PERROR(LOG_PRIORITY_ERROR);
		exit(1);

	}

	/*
	 * Parent process should wait for the child process
	 * and monitor return value...
	 *
	 * There is potential DoS here. If the script never
	 * returns then the IKEv2 will block. Furthermore,
	 * if the script needs more time to complete, then
	 * it's maybe better to make this asynchronous?
	 */
	wait(&status);

	/**
	 * Check that script exited via exit call and that it
	 * returned zero. This signals successful script run.
	 */
	if (!WIFEXITED(status))
		LOG_ERROR("Script %s prematurely ended!", argv[0]);
	else if (WEXITSTATUS(status))
		LOG_ERROR("Script %s returned exit code %d",
				argv[0], WEXITSTATUS(status));
	else
		retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/*******************************************************************************
 * Module and providers initialization and deinitialization functions
 ******************************************************************************/

void cfg_unload()
{
}

/**
 * Initialize CFG module and all compiled-in modules
 *
 * @param cfg		List of configuration provider sections from the
 *			configuration file.
 * @param queue		State machine's queue. All the notifications will
 *			be sent on this queue.
 *
 * \return 0 if initalization was successful, -1 in case of an error
 */
int cfg_init(GSList *cfg, GAsyncQueue *queue)
{
	GSList *c;
	struct cfg_config_provider *ccp;

	if ((ccp_lock = g_mutex_new()) == NULL)
		return -1;

	cfg_config_providers = cfg;

	cfg_queue = g_async_queue_new();

	sm_queue = queue;

	/**
	 * Spawn a thread that will wait for responsed from providers
	 */
	thread = g_thread_create(cfg_main_thread, NULL, TRUE, NULL);

	/**
	 * Global initialization of configuration providers
	 */
#ifdef CFG_DHCPV4
	if (cfg_dhcpv4_init() < 0) {
		cfg_unload();
		return -1;
	}
#endif /* CFG_DHCPV4 */

#ifdef CFG_PRIVATE
	if (cfg_private_init() < 0) {
		cfg_unload();
		return -1;
	}
#endif /* CFG_PRIVATE */

#ifdef CFG_DHCPV6
	if (cfg_dhcpv6_init() < 0) {
		cfg_unload();
		return -1;
	}
#endif /* CFG_DHCPV6 */

	/**
	 * Configuration of all the instances defined in the
	 * configuration file in cfg { ... } configuration section.
	 */
	for (c = cfg; c; c = c->next) {
		ccp = c->data;

		switch(ccp->provider_type) {
#ifdef CFG_DHCPV4
		case CFG_PROTO_DHCPV4:
			if (cfg_dhcpv4_instance_init(ccp) < 0) {
				cfg_unload();
				return -1;
			}
			break;
#endif /* CFG_DHCPV4 */

#ifdef CFG_DHCPV6
		case CFG_PROTO_DHCPV6:
			if (cfg_dhcpv6_instance_init(ccp) < 0) {
				cfg_unload();
				return -1;
			}
			break;
#endif /* CFG_DHCPV6 */

#ifdef CFG_PRIVATE
		case CFG_PROTO_PRIVATE:
			if (cfg_private_instance_init(ccp) < 0) {
				cfg_unload();
				return -1;
			}
			break;
#endif /* CFG_PRIVATE */

		default:
			LOG_ERROR("No provider compiled in for cfg id %s",
					ccp->id);
			cfg_unload();
			return -1;
		}
	}

	return 0;
}

#endif /* CFG_MODULE */
