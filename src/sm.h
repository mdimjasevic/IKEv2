/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SM_H
#define __SM_H

/*
 * This module also sends messages to queue for processing. The following
 * constant defines number for those messages so that they can be
 * differentiated from the rest.
 */
#define SM_SESSION_MSG			4

#define CONNECT_MSG			7

/*
 * How fast (in ms) do we check for different timeouts of IKE SA
 */
#define IKESA_CHECKPOINT_INTERVAL	100

/*
 * Messages to state machine...
 */
#define MSG_IKE_DPD		1	/* Initiate dead peer detection */
#define MSG_IKE_REKEY		2	/* Start rekeying IKE SA */
#define MSG_IKE_REAUTH		3	/* Start rekeying IKE SA */
#define MSG_IKE_TIMEOUT		4	/* Timeout expired on a request */
#define MSG_IKE_TERMINATE	5	/* Terminate the session */
#define MSG_IKE_REMOVE		6	/* Remove session from memory */
#define MSG_IKE_LEASE_RENEW	7	/* Lease time has expired */
#define MSG_IKE_CRL_UPDATE	11	/* CRL needs updating */

#define MSG_SA_TRANSFER		8	/* Transfer to another session */
#define MSG_SA_DELETE		9	/* Initiate delete exchange */
#define MSG_SA_CLEAN		10	/* Remove dead CHILD SA structures */

/*
 * Messages sent by perodic timer in SM module
 */
struct sm_session_msg {
	/*
	 * For this structure the value of the following field is
	 * always SM_MSG.
	 */
	guint8 msg_type;

        /*
	 * Task that has to be done on session
	 */
	guint8 message;
};

/*
 * Message pushed by the main function to initiate connection
 * to a remote peer
 */
struct connect_msg {
	guint8 msg_type;

	struct id *id;
};

/*
 * Union of all the known messages...
 */
union sm_msg {
	guint8 msg_type;
	struct pfkey_msg pfkey_msg;
	struct message_msg message_msg;
	struct sig_msg sig_msg;
	struct timeout_msg timeout_msg;
	struct sm_session_msg sm_session_msg;
	struct aaa_msg aaa_msg;
	struct connect_msg connect_msg;
	struct cert_msg cert_msg;
#ifdef CFG_MODULE
	struct cfg_msg cfg_msg;
#endif /* CFG_MODULE */
};

/*******************************************************************************
 * SM_SESSION_MSG STRUCTURE
 ******************************************************************************/
struct sm_session_msg *sm_session_msg_alloc(guint8);
void sm_session_msg_free(struct sm_session_msg *);

/*******************************************************************************
 * FUNCTIONS
 ******************************************************************************/

void sm_signal_handler(struct ikev2_data *, struct sig_msg *);

gint sm_process_informational(struct session *, struct message_msg *);
gint sm_process_informational_finish(struct session *, struct message_msg *);

void sm_session_activate(struct session *);
void sm_session_remove(struct session *);
gint sm_csa_remove(struct sad_item *, struct session *, struct ikev2_data *);

gint sm_child_sa_create_i(struct session *, struct sad_item *,
		union sm_msg *);
gint sm_child_sa_create_r(struct session *, struct sad_item *,
		struct message_msg *);
gint sm_child_sa_rekey_i(struct session *, struct sad_item *,
		union sm_msg *);
gint sm_child_sa_rekey_r(struct session *, struct sad_item *,
		struct message_msg *);
gint sm_child_sa_delete_i(struct session *, struct sad_item *);
gint sm_ike_sa_rekey_i(struct session *, struct message_msg *);
gint sm_ike_sa_rekey_r(struct session *, struct message_msg *);

gint sm_process_delete(struct session *, struct ikev2_data *,
		struct message_msg *);
gint sm_process_informational(struct session *, struct message_msg *);

void sm_ike_thread(gpointer, gpointer);
gint sm_ike_i_thread(struct session *, union sm_msg *);
gint sm_ike_r_thread(struct session *, union sm_msg *);

gpointer sm_ikesa_responder_wait_timeout(gpointer);
gboolean sm_ikesa_check_thread(gpointer);

/*******************************************************************************
 * INITIALIZATION AND REMOVAL
 ******************************************************************************/

void sm_unload(struct ikev2_data *);
int sm_init(struct ikev2_data *, GAsyncQueue *);

#endif /* __SM_H */
