/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __CERT_MSG_C

#define LOGGERNAME	"cert_msg"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <linux/ipsec.h>
#include <sys/time.h>

#include <openssl/rand.h>
#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "proposals.h"
#include "ts.h"
#include "auth.h"
//#include "config.h"
#include "network.h"
#include "payload.h"
#include "sad.h"
#include "cert_msg.h"
#include "crypto.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE CERT_MSG STRUCTURE
 ******************************************************************************/

/**
 * Function to allocate memory for cert message.
 *
 * \return Pointer to a new message structure or NULL in case of an error.
 */
struct cert_msg *cert_msg_alloc(void)
{
	struct cert_msg *msg;

	LOG_FUNC_START(1);

	msg = g_malloc0(sizeof(struct cert_msg));

	msg->msg_type = CERT_MSG;

	LOG_FUNC_END(1);

	return msg;
}

/**
 * Function to free memory occupied by cert message.
 *
 * @param msg	Pointer to a message structure
 */
void cert_msg_free(struct cert_msg *msg)
{
	LOG_FUNC_START(1);

	if (msg)
		g_free (msg);

	LOG_FUNC_END(1);
}
