#!/bin/sh

ip addr flush dev eth1
ip addr add 10.0.0.2/24 dev eth1
ip link set dev eth1 up
