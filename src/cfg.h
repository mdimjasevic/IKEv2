/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __CFG_H
#define __CFG_H

/*
 * Flags for supported options
 */
#define OPTION_F4_ADDR		(1<<0)
#define OPTION_F4_NETMASK	(1<<1)
#define OPTION_F4_DNS		(1<<2)
#define OPTION_F4_DHCP		(1<<3)
#define OPTION_F4_NBNS		(1<<4)
#define OPTION_F4_SUBNETS	(1<<5)

#define OPTION_F6_ADDR		(1<<6)
#define OPTION_F6_DNS		(1<<7)
#define OPTION_F6_DHCP		(1<<8)
#define OPTION_F6_NBNS		(1<<9)
#define OPTION_F6_SUBNETS	(1<<10)

#define OPTION_F_APPVER		(1<<11)
#define OPTION_F_SUPPATTR	(1<<12)
#define OPTION_F_EXPIRY		(1<<13)

/**
 * All the dynamically assigned configuration data
 */
struct cfg {

	/**
	 * Reference coutner
	 */
	gint refcnt;

	/**
	 * Is this originaly request (TRUE), or is it received
	 * response (FALSE)
	 */
	gboolean request;

	/**
	 * Parameters that should be obtained from the provider
	 *
	 * INITIATOR: After we receive response from the responder
	 * we clear this field. When rerequesting data we place here
	 * requested parameters!
	 */
	guint32 flags;

	/**
	 * Supported paramterers and/or obtained parameters
	 */
	guint32 supported;

	/**
	 * List of IPv4 and/or IPv6 addresses leased to a peer
	 */
	GSList *netaddrs;

	/**
	 * IPv4 netmask. If 0 then there is no IPv4 netmask
	 *
	 * This is a union that has 32 bits. The reason for definig
	 * it this way is that gcc doesn't allow us to assign guint32
	 * to struct in_addr though both are 32-bit in size.
	 */
	union {
		struct in_addr netmask;
		guint32 netmask32;
	};

	/**
	 * List of IPv4 and/or IPv6 DNS servers. They are
	 * distinguished by IP address type (i.e. IPv4 address
	 * is for DNSv4 servers, while IPv6 address is for
	 * DNSv6 servers).
	 */
	GSList *dns;

	/**
	 * List of IPv4 and/or IPv6 DHCP servers. They are
	 * distinguished by IP address type (i.e. IPv4 address
	 * is for DHCPv4 servers, while IPv6 address is for
	 * DHCPv6 servers).
	 */
	GSList *dhcp;

	/**
	 * List of WINS servers. Only valid for IPv4 addresses
	 */
	GSList *nbns;

	/**
	 * Lease expiry in seconds (if 0 no expiry is defined) for
	 * IPv4 leased addresses.
 	 */
	guint32 expiry4;

	/**
	 * Lease expiry in seconds (if 0 no expiry is defined) for
	 * IPv6 leased addresses.
 	 */
	guint32 expiry6;

	/**
	 * List of IPv4 and/or IPv6 subnets protected by this
	 * server.
	 *
	 * TODO: Maybe protected subnets are not specific to particular
	 * configuration, i.e. they have to be inside remote configuration
	 * directive!
	 */
	GSList *subnets;

	/**
	 * Application data length
	 */
	gint appid_len;

	/**
	 * Application data
	 */
	gpointer appid;
};

/*******************************************************************************
 * struct cfg related functions
 ******************************************************************************/
struct cfg *cfg_new();
struct cfg *cfg_refcnt_inc(struct cfg *);
void cfg_free(struct cfg *);
void cfg_dump(struct cfg *);

GSList *cfg_get_netaddrs(void *);
gboolean cfg_is_request(void *);

#endif /* __CFG_H */
