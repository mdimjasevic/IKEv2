/*
spdadd 197.100.1.0/24 192.168.11.0/24 icmp
        -P in ipsec
        esp/tunnel/213.149.47.226-161.53.65.229/require;
 */ 
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <linux/ipsec.h>
#include <unistd.h>
#include <errno.h>

struct net {
	struct sockaddr_in sin;
	int prefix;
};

void *my_malloc(size_t size)
{
	void *retval;

	if ((retval = malloc(size)) != NULL)
		memset(retval, 0, size);

	return retval;
}

int _pfkey_send(int sockfd, struct sadb_msg *hdr, void **ext_hdrs)
{
	struct iovec *iovec;
	int niovec, i, retval;

	retval = -1;

	if (!(iovec = my_malloc(sizeof(struct iovec) * (SADB_EXT_MAX + 1))))
		goto out;

	niovec = 0;

	iovec[niovec].iov_base = hdr;
	iovec[niovec++].iov_len = sizeof(struct sadb_msg);

	for (i = 0; i < SADB_EXT_MAX; i++) {

		if (ext_hdrs[i] == NULL)
			continue;

		iovec[niovec].iov_base = ext_hdrs[i];
		iovec[niovec++].iov_len =
			((struct sadb_ext *)ext_hdrs[i])->sadb_ext_len << 3;
	}

	if (writev(sockfd, iovec, niovec) < 0)
                goto out_free;

        retval = 0;

out_free:
	free(iovec);

out:
	return retval;
}

#define MAX_PFKEY_RECV_SIZE	4096

ssize_t _pfkey_recv(int sockfd, void **buffer)
{
	int len;

	len = -1;
        if ((*buffer = my_malloc(MAX_PFKEY_RECV_SIZE)) == NULL)
                goto out;

	if ((len = read(sockfd, *buffer, MAX_PFKEY_RECV_SIZE)) < 0) {
                len = -1;
		free (*buffer);
		printf("errno=%d\n", ((struct sadb_msg *)(*buffer))->sadb_msg_errno);
        }

out:
	return len;
}

int policy_set(int spid, int protoid, int direction, struct net *lnet,
		struct net *rnet, struct sockaddr_in *lip, struct sockaddr_in *rip)
{
	int sockfd;
	int retval = -1;

	void *ext_hdrs[SADB_EXT_MAX];

	struct sadb_msg hdr;
	struct sadb_address *srcaddress, *dstaddress;
	struct sadb_x_policy *pol;
	struct sadb_x_ipsecrequest *ipsecrequest;

	struct sockaddr_in *sin;

	void *buffer;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	if ((sockfd = socket(PF_KEY, SOCK_RAW, PF_KEY_V2)) < 0)
		return -1;

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDADD;
	hdr.sadb_msg_satype = SADB_SATYPE_UNSPEC;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) >> 3;
	hdr.sadb_msg_seq = 0;
	hdr.sadb_msg_pid = getpid();

	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1] =
				my_malloc(sizeof(struct sadb_address)
					+ sizeof(struct sockaddr_in));
	if (srcaddress == NULL)
		return -2;

	srcaddress->sadb_address_len = (sizeof(struct sadb_address) +
						sizeof(struct sockaddr_in)) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;
	srcaddress->sadb_address_prefixlen = lnet->prefix;
	srcaddress->sadb_address_proto = 1;
	memcpy((char *)(srcaddress + 1), lnet, sizeof(struct sockaddr_in));
	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1] =
				my_malloc(sizeof(struct sadb_address)
					+ sizeof(struct sockaddr_in));
	if (dstaddress == NULL)
		return -3;

	dstaddress->sadb_address_len = (sizeof(struct sadb_address) +
						sizeof(struct sockaddr_in)) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;
	dstaddress->sadb_address_prefixlen = rnet->prefix;
	dstaddress->sadb_address_proto = 1;
	memcpy((char *)(dstaddress + 1), rnet, sizeof(struct sockaddr_in));
	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	pol = ext_hdrs[SADB_X_EXT_POLICY-1] =
			my_malloc(sizeof(struct sadb_x_policy) +
				sizeof(struct sadb_x_ipsecrequest) +
					sizeof(struct sockaddr_in) * 2);
	if (pol == NULL)
		return -4;

	pol->sadb_x_policy_len = (sizeof(struct sadb_x_policy) +
				sizeof(struct sadb_x_ipsecrequest) +
					+ sizeof(struct sockaddr_in) * 2)/ 8;
	pol->sadb_x_policy_exttype = SADB_X_EXT_POLICY;
	pol->sadb_x_policy_type = IPSEC_POLICY_IPSEC;
	pol->sadb_x_policy_dir = direction;
	pol->sadb_x_policy_id = spid;
	pol->sadb_x_policy_priority = 0x80000000;

	ipsecrequest = (struct sadb_x_ipsecrequest *)(pol + 1);
	ipsecrequest->sadb_x_ipsecrequest_len =
				(sizeof(struct sadb_x_ipsecrequest) 
					+ sizeof(struct sockaddr_in) * 2);
	ipsecrequest->sadb_x_ipsecrequest_proto = protoid;
	ipsecrequest->sadb_x_ipsecrequest_mode = IPSEC_MODE_TUNNEL;
	ipsecrequest->sadb_x_ipsecrequest_level = IPSEC_LEVEL_REQUIRE;
	ipsecrequest->sadb_x_ipsecrequest_reqid = 0;

	sin = (struct sockaddr_in *)(ipsecrequest + 1);
	memcpy(sin, lip, sizeof(struct sockaddr_in));
	memcpy(sin + 1, rip, sizeof(struct sockaddr_in));

	hdr.sadb_msg_len += pol->sadb_x_policy_len;

	if (_pfkey_send(sockfd, &hdr, ext_hdrs) < 0)
		return -5;

	if (_pfkey_recv(sockfd, &buffer) < 0)
		return -6;

	close (sockfd);

	return retval;
}

struct net *parse_net(const char *anet)
{
	struct net *retval;
	char *p;

	if ((p = strchr(anet, '/')) == NULL)
		return NULL;

	if ((retval = my_malloc(sizeof(struct net))) == NULL)
		return NULL;

	*p = 0;

	retval->sin.sin_family = AF_INET;
	if (inet_pton(AF_INET, anet, &retval->sin.sin_addr) <= 0) {
		free(retval);
		return NULL;
	}

	retval->prefix = atoi(p + 1);

	*p = '/';

	return retval;
}

struct sockaddr_in *parse_addr(const char *addr)
{
	struct sockaddr_in *retval;

	if ((retval = my_malloc(sizeof(struct sockaddr_in))) == NULL)
		return NULL;

	retval->sin_family = AF_INET;
	if (inet_pton(AF_INET, addr, &retval->sin_addr) <= 0) {
		free(retval);
		return NULL;
	}

	return retval;
}

main(int argc, char **argv)
{
	int spid, protoid, direction;
	struct net *leftnet, *rightnet;
	struct sockaddr_in *lrip, *rrip;

	if (argc != 8) {
		fprintf (stderr, "Usage: %s [arguments]\n", argv[0]);
		fprintf (stderr, "Arguments are:\n");
		fprintf (stderr, "	<policy_id>		Policy ID\n");
		fprintf (stderr, "	<proto_id>		Should policy use ESP(50) or AH(51)\n");
		fprintf (stderr, "	<direction>		Inbound (1) or outbound (2)\n");
		fprintf (stderr, "	<left_net>		Left network\n");
		fprintf (stderr, "	<right_net>		Right network\n");
		fprintf (stderr, "	<letf_router_ip>	Left router IP address\n");
		fprintf (stderr, "	<right_router_ip>	Left router IP address\n");
		return -1;
	}

	spid = atoi(argv[1]);
	protoid = atoi(argv[2]);
	direction = atoi(argv[3]);

	if ((leftnet = parse_net(argv[4])) == NULL) {
		fprintf (stderr, "Error in left network\n");
		return -1;
	}

	if ((rightnet = parse_net(argv[5])) == NULL) {
		fprintf (stderr, "Error in right network\n");
		return -1;
	}

	if ((lrip = parse_addr(argv[6])) == NULL) {
		fprintf (stderr, "Error in left ip address\n");
		return -1;
	}

	if ((rrip = parse_addr(argv[7])) == NULL) {
		fprintf (stderr, "Error in right ip address\n");
		return -1;
	}

	if ((spid = policy_set(spid, protoid, direction, leftnet, rightnet, lrip, rrip)) < 0)
		return spid;

	printf ("%d\n", spid);
}

