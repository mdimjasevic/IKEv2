#include <stdio.h>

#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

void bin2hex(const unsigned char *buf, int size)
{
	int i;
	unsigned char ch;

	for (i = 0; i < size; i++) {

		if (!(i % 16))
			printf ("\n%04X ", i);

		ch = buf[i] >> 4;
		printf ("%c", ch > 9 ? (ch - 10 + 'A') : ch + '0');

		ch = buf[i] & 0x0F;
		printf ("%c", ch > 9 ? (ch - 10 + 'A') : ch + '0');
	}

	printf ("\n");
}

int main(int argc, char **argv)
{
	FILE *infile, *keyfile;
	EVP_PKEY *pkey;
	char *buffer, *data, *rsa_sign;
	char sha_res[200];
	int file_size, rsa_sign_len;
	X509 *cert;

	if (argc != 4) {
		fprintf (stderr, "Usage: %s <cert file> <signed file> <signature file>\n",
				argv[0]);
		exit (1);
	}

	if ((keyfile = fopen(argv[1], "r")) == NULL) {
		fprintf (stderr, "Could not open certificate file %s\n", argv[1]);
		exit (1);
	}

	if ((cert = PEM_read_X509(keyfile, NULL, NULL, NULL)) == NULL) {
		ERR_print_errors_fp (stderr);
		exit (1);
	}

	X509_free(cert);

	fclose (keyfile);

	exit(1);

	if ((rsa_sign = malloc(RSA_size(pkey->pkey.rsa))) == NULL) {
		fprintf (stderr, "Could not allocate memory\n");
		exit (1);
	}

	if ((infile = fopen(argv[2], "r")) == NULL) {
		fprintf (stderr, "Could not open data file %s\n", argv[2]);
		exit (1);
	}

	if (fseek(infile, 0, SEEK_END) < 0) {
		perror("fseek");
		exit (1);
	}

	file_size = ftell(infile);

	if (fseek(infile, 0, SEEK_SET) < 0) {
		perror("fseek");
		exit (1);
	}

	if ((data = malloc(file_size)) == NULL) {
		fprintf (stderr, "Could not allocate memory to load file\n");
		exit (1);
	}

	if (fread(data, 1, file_size, infile) != file_size) {
		fprintf (stderr, "Could not load file into memory\n");
		exit (1);
	}

	fclose(infile);

	SHA1(data, file_size, sha_res);
	free(data);

	if (RSA_sign(NID_sha1, sha_res, 20, rsa_sign, &rsa_sign_len,
		pkey->pkey.rsa) != 1) {
		fprintf (stderr, "Could not sign data\n");
		exit (1);
	}

	printf ("Dumping buffer of size %d\n", rsa_sign_len);
	bin2hex(rsa_sign, rsa_sign_len);
}
