#include <stdio.h>
#include <string.h>

#include <openssl/rand.h>
#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/des.h>
#include <openssl/aes.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/conf.h>

#include <glib.h>

char *crypto_string_to_derasn1dn(char *string, int len)
{
        X509_NAME *name;
        char *der_asn1dn = NULL;
        char *buf, *field, *bytes;
        int i, der_len;
        gboolean field_pos;
        unsigned char *ptr = NULL;
        unsigned char **der = &ptr;

        if ((buf = g_malloc(len)) == NULL) {
                fprintf(stderr, "Can't allocate memory!\n");
                return NULL;
        }

        memcpy(buf, string, len);

	if ((name = X509_NAME_new()) == NULL) {
		fprintf(stderr, "Error creating new X509 name!\n");
		return NULL;
	}

        i = 0;
        field = NULL;
        bytes = NULL;
        field_pos = TRUE;

        while (i < len) {

                if (field_pos && (field == NULL)) {

                        if ((buf[i] == ' ') || ((i == 0) && (buf[i] == '/')))
                                field = &buf[i + 1];
                        else
                                field = &buf[i];
                }

                if (field_pos) {

                        if (buf[i] == '=') {
                                buf[i] = '\0';
                                field_pos = FALSE;
                        }
                }

                if (!field_pos && (bytes == NULL)) {
                                bytes = &buf[i + 1];
                }

                if (!field_pos) {
                        if ((buf[i] == ',') || (buf[i] == '/')) {
                                buf[i] = '\0';
                                field_pos = TRUE;
				fprintf (stderr, "Adding field %s value %s\n", field, bytes);
                                if (!X509_NAME_add_entry_by_txt(name, field,
						MBSTRING_ASC, bytes, -1, -1, 0)) {
                                        fprintf(stderr, "Unsuccessfull adding new field to X509_NAME\n");
                                        return NULL;
                                }

                                field = NULL;
                                bytes = NULL;
                        }
                }
                i++;
        }

        g_free(buf);

        /* Convert X509_NAME structure to DER format */

        der_len = i2d_X509_NAME(name, der);

        if ((der_asn1dn = g_malloc(der_len + 1)) == NULL) {
                fprintf(stderr, "Can't allocate memory!\n");
                return NULL;
        }

        memcpy(der_asn1dn, *der, der_len);
        der_asn1dn[der_len] = '\0';

        X509_NAME_free(name);

        return der_asn1dn;
}

int main(int argc, char **argv)
{
	char *p;

	if (argc != 2) {
		fprintf (stderr, "Usage: %s <dn>\n", argv[0]);
		fprintf (stderr, "Example: %s \"C=HR, O=Disorganized Organization, CN=Joe Bloggs\"\n", argv[0]);
		exit(1);
	}

	if (p = crypto_string_to_derasn1dn(argv[1], strlen(argv[1])))
		printf ("Result: %s\n", p);
}
