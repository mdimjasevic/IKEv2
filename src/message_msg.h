/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/ 

#ifndef __MESSAGE_MSG_H
#define __MESSAGE_MSG_H

/**
 * Discriminator ID of messages sent throuth async queue to upper layers.
 */
#define MESSAGE_MSG	1

/**
 * Size of the message_msg digest
 */
#define MESSAGE_MSG_DIGEST_SIZE	16

/*
 * These are used for calculating digest of received message
 */
#define DIGEST_SECRET		"0123456789012345"
#define DIGEST_SECRET_LEN	(sizeof("0123456789012345") - 1)

/**
 * Structure with populated from data in IKE SA INIT request or response
 */
struct ike_sa_init {

	/**
	 * Errors
	 */
	guint no_proposal_chosen:1;
	guint invalid_syntax:1;
	guint invalid_major_version:1;

	/**
	 * If responder wants another DH group to be used
	 */
	guint16 dh_group;

	/**
	 * Traffic selectors from the packet
	 */
	GSList *tsi;
	GSList *tsr;

	/*
	 * Cookie
	 *
	 * The usage of this field depends on the exact
	 * situation. There are three cases:
	 *
	 * 1. There was cookie included in IKE SA INIT request.
	 *    Then this filed points inside structure where
	 *    cookie starts.
	 *
	 * 2. The response should be sent back and that response
	 *    has to include cookie value given in this filed
	 *    with it's appropriate len
	 *
	 * 3. The request has to be reated with the cookie
	 *    pointed to by this field.
	 *
	 * Note that this pointer (cookie) is _not_ freed with
	 * g_free since it's in received message, so by freeing
	 * that structure we are making this pointer invalid....
	 */

	/**
	 * Cookies that should be placed in request.
	 */
	gchar *cookie;

	/**
	 * Number of bytes in cookie field
	 */
	guint8 cookie_len;

	/**
	 * Expected cookies that should be placed in request.
	 */
	gchar *expected_cookie;

	/**
	 * Proposals in SA field
	 */
	GSList *proposals;

	/**
	 * Public DH key from peer...
	 */
	BIGNUM *peer_pub_key;

	/**
	 * DH group used by peer...
	 */
	gint16 peer_dh_group;

	/**
	 * Nonce sent by peer...
	 */
	BIGNUM *peer_nonce;

	/**
	 * Source IP addresses for NAT detection
	 */
	GSList *nat_sourceip;

	/**
	 * Destination IP addresses for NAT detection
	 */
	gpointer nat_destip;

	/**
	 * Certificate related data
	 */
	GSList *certreq_list;

	/**
	 * Does initiator support http certificate lookup?
	 */
	gboolean http_cert_lookup;
};

/**
 * Structure with populated from data in IKE SA AUTH request or response
 */
struct ike_auth {

	/**
	 * Has initiator requested transport mode?
	 */
	gint transport_mode:1;

	/**
	 * Is this message with Invalid IKE SPI?
	 */
	gint invalid_ike_spi:1;

	/**
	 * Did we receive INVALID_SYNTAX?
	 */
	gint invalid_syntax:1;

	/**
	 * Did we receive NO_PROPOSAL_CHOSEN?
	 */
	gint no_proposal_chosen:1;

	/**
	 * Did we receive SINGLE_PAIR_REQUIRED?
	 */
	gint single_pair_required:1;

	/**
	 * Did we receive TS_UNACCEPTABLE?
	 */
	gint ts_unacceptable:1;

	/**
	 *  Notify N_ESP_TFC_PADDING_NOT_SUPPORTED
	 */
	gint no_tfc:1;

	/**
	 * TRUE if we received notify AUTHENTICATION_FAILED
	 */
	guint authentication_failed:1;

	/**
	 * TRUE if we received notify INTERNAL_ADDRESS_FAILURE
	 */
	guint internal_address_failure:1;

#ifdef MOBIKE
	/**
	 * TRUE if we received N_MOBIKE_SUPPORTED notify payload
	 */
	guint peer_supports_mobike:1;
#endif /* MOBIKE */

	/**
	 * Initiator's ID
	 */
	struct id *i_id;

	/**
	 * Requested responder's ID
	 */
	struct id *r_id;

	/**
	 * Is this initial contact from peer?
	 */
	gboolean initial_contact;

	/**
	 * Does initiator support http certificate lookup?
	 */
	gboolean http_cert_lookup;

	/**
	 * This is unused for now...
	 */
	gboolean non_first_fragment;

	/**
	 * Configuration payload data
	 */
#if defined(CFG_MODULE) || defined(CFG_CLIENT)
	void *cfg;
#endif /* defined(CFG_MODULE) || defined(CFG_CLIENT) */

	/**
	 * Proposals offered by initiator
	 */
	GSList *proposals;

	/**
	 * Traffic selectors from the packet
	 */
	GSList *tsi;
	GSList *tsr;

	/**
	 * Autentification related data
	 */
	guint8 auth_type;
	void *auth_payload;
	guint16 auth_payload_len;

	/**
	 * EAP related data
	 */
	void *eap;
	gint eap_len;

	/**
	 * Certificate related data
	 */
	GSList *cert_list;
	GSList *certreq_list;

	/**
	 * Lists of remote peer's IPv4 and IPv6 addresses
	 */
#ifdef MOBIKE
	GSList *peer_ipv4_addresses;
	GSList *peer_ipv6_addresses;
#endif /* MOBIKE */
};

/**
 * Structure with populated from data in INFORMATIONAL exchange
 */
struct informational {
	/**
	 * Errors...
	 */
	gint invalid_syntax:1;

	/**
	 * List of payload_delete_data structures...
	 */  
       	GSList *deletes;

#if defined(CFG_MODULE) || defined(CFG_CLIENT)
	/**
	 * Configuration payload data
	 */
	void *cfg;
#endif /* defined(CFG_MODULE) || defined(CFG_CLIENT) */

	/**
	 * Notify to be sent back to peer
	 */
	gint32 notify;
	union {
		guint8 notify_data8;
		guint16 notify_data16;
		guint32 notify_data32;
	};
};

/**
 * Structure with populated from data in CREATE CHILD SA exchange
 */
struct create_child_sa {

	/**
	 * Errors...
	 */
	gint no_tfc:1;

	gint invalid_syntax:1;

	gint no_proposal_chosen:1;

	gint ts_unacceptable:1;

	gint no_additional_sas:1;

	/**
	 * Does initiator requested transport mode?
	 */
	gint transport_mode:1;

	/**
	 * Non-error notifies...
	 */
	gboolean non_first_fragment:1;

	/**
	 * Rekey notify data sent by peer
	 */
	gboolean rekey;
	guint32 rekey_spi;

	guint8 mode;

	/**
	 * Proposals offered by initiator
	 */
	GSList *proposals;

	/**
	 * Nonce sent by peer...
	 */
	BIGNUM *peer_nonce;

	/**
	 * Public DH key from peer...
	 */
	BIGNUM *peer_pub_key;

	/**
	 * DH group used by peer...
	 */
	gint16 peer_dh_group;

	/**
	 * Traffic selectors from the packet
	 */
	GSList *tsi;
	GSList *tsr;
};

/**
 * Structure for communicating data from message subsystem to state machines.
 *
 * @todo This structure should be used also when sending request and responses
 */
struct message_msg {

	/**
	 * Different message types could be sent to state machine subsystem
	 * and the following field holds discriminator to exact type. For
	 * pfkey_msg type is set to PFKEY_MSG.
	 */
	guint8 msg_type;

	/**
	 * These two are copies from networking subsystem so they souldn't be
	 * modified!
	 */
	struct netaddr *srcaddr;
	struct netaddr *dstaddr;

	/**
	 * Socket on which we received message
	 *
	 * Note that it's interesting question whether we should send on the
	 * socket we received message, or it's completly irrelevant. If it's
	 * irrelevant, then this could be simplified...
	 */
	void *ns;

	/**
	 * Data from the header of message
	 */
	guint64 i_spi;
	guint64 r_spi;
	guint32 msg_id;
	guint8 exchg_type;

	/**
	 * TRUE if the message was sent by IKE SA initiator
	 */
	gboolean initiator;

	/**
	 * TRUE if the message is response
	 */
	gboolean response;

	/*
	 * TRUE if message integrity has passed
	 */
	gboolean integrity_ok;

	/**
	 * The complete received message from network layer
	 */
	ssize_t size;
	void *buffer;

	/**
	 * Digest data for searching sessions by messages
	 */
	gpointer digest;

	/**
	 * Notify to be sent back to peer
	 *
	 * We assume that if this field is non-zero then it's an
	 * error notify and no other payload should be placed in
	 * the response message!
	 */
	gint32 notify;
	union {
		guint8 notify_data8;
		guint16 notify_data16;
		guint32 notify_data32;
	};

	union {
		struct ike_sa_init ike_sa_init;
		struct ike_auth ike_auth;
		struct informational informational;
		struct create_child_sa create_child_sa;
	};
};

/*
 * Functions that work with 'struct message_msg'
 */
gpointer message_msg_dup_digest(struct message_msg *);
struct message_msg *message_msg_alloc(void);
void message_msg_free(struct message_msg *);

#endif /* __MESSAGE_MSG_H */
