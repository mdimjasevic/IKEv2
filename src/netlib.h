/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __NETLIB_H
#define __NETLIB_H

/**
 * Return lower 64 bits of IPv6 address
 *
 * @param p	Pointer to IPv6 address in network byte order
 *
 * The IPv6 address is stored in the memory as follows
 *
 * aa bb cc dd ee ff gg hh ii jj kk ll mm nn oo pp
 *
 * with 'aa' being the most significant octet, and pp least
 * significant octet.
 *
 * TODO: This has to be optimized for big endian machines!
 */
#define IPV6_GET_LO64(p)	((guint64) \
				(((guint64)(*((guint8 *)(p) +  8)) << 56) \
				| ((guint64)(*((guint8 *)(p) +  9)) << 48) \
				| ((guint64)(*((guint8 *)(p) + 10)) << 40) \
				| ((guint64)(*((guint8 *)(p) + 11)) << 32) \
				| ((guint64)(*((guint8 *)(p) + 12)) << 24) \
				| ((guint64)(*((guint8 *)(p) + 13)) << 16) \
				| ((guint64)(*((guint8 *)(p) + 14)) << 8) \
				| ((guint64)(*((guint8 *)(p) + 15)))))

/**
 * Return higher 64 bits of IPv6 address
 */
#define IPV6_GET_HI64(p)	((guint64) \
				((((guint64)(*(guint8 *)(p))) << 56) \
				| ((guint64)(*((guint8 *)(p) + 1)) << 48) \
				| ((guint64)(*((guint8 *)(p) + 2)) << 40) \
				| ((guint64)(*((guint8 *)(p) + 3)) << 32) \
				| ((guint64)(*((guint8 *)(p) + 4)) << 24) \
				| ((guint64)(*((guint8 *)(p) + 5)) << 16) \
				| ((guint64)(*((guint8 *)(p) + 6)) << 8) \
				| ((guint64)(*((guint8 *)(p) + 7)))))

/*******************************************************************************
 * Structures related to window management functions
 ******************************************************************************/

#define MAX_WIN_SIZE		128

struct nl_win {
	/*
	 * Window size, initially it's set to 1
	 */
	guint16 wsize;

	/*
	 * First unsent....
	 */
	guint32 hi;

	/*
	 * First sent and unacknowledged ...
	 */
	guint32 lo;

	/*
	 * Bitfield used to mark already received responses/requests
	 */
	guint32 bitfield[(MAX_WIN_SIZE + 31) >> 5];
};

/*******************************************************************************
 * Network address functions
 ******************************************************************************/

#ifdef __NETLIB_C

typedef struct netaddr {
	/**
	 * It is important that this union be the first one
	 */
	union {
		struct sockaddr sa;
		struct sockaddr_in sin;
		struct sockaddr_in6 sin6;
		struct sockaddr_ll sll;
	};

	guint8 prefix;

	/**
	 * Interface name from configuration file
	 */
        gchar *ifname;

	gint refcnt;
} netaddr_t;

/*
 * TODO
 * This can easily be calculated, just take -1 and shift
 * it left by (32 - the netmask size)...
 */
const guint32 ipv4_netmasks[33] = {
	0x00000000, 0x80000000, 0xC0000000, 0xE0000000,
	0xF0000000, 0xF8000000, 0xFC000000, 0xFE000000,
	0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
	0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000,
	0xFFFF0000, 0xFFFF8000, 0xFFFFC000, 0xFFFFE000,
	0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
	0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0,
	0xFFFFFFF0, 0xFFFFFFF8, 0xFFFFFFFC, 0xFFFFFFFE,
	0xFFFFFFFF,
};

#else /* _NETLIB_C */

typedef void netaddr_t;

#endif /* _NETLIB_C */

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
struct netaddr *netaddr_new();
struct netaddr *netaddr_dup_ro(struct netaddr *);
struct netaddr *netaddr_dup(struct netaddr *);
struct netaddr *netaddr_new_from_inaddr(struct in_addr *);
struct netaddr *netaddr_new_from_in6addr(struct in6_addr *);
struct netaddr *netaddr_new_from_str(int, const char *);
void netaddr_free(struct netaddr *);

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/
guint8 netaddr_get_prefix(struct netaddr *);
void netaddr_set_prefix(struct netaddr *, guint8);

gchar *netaddr_get_ifname(struct netaddr *);
void netaddr_set_ifname(struct netaddr *, gchar *);

/*******************************************************************************
 * NETWORK ADDRESS COMPARING FUNCTIONS
 ******************************************************************************/
gint netaddr_cmp_ip2ip(struct netaddr *, struct netaddr *);
gint netaddr_cmp_net2ip(struct netaddr *, struct netaddr *);

/*******************************************************************************
 * MISC AND INTERNAL FUNCTIONS
 ******************************************************************************/
guint32 netaddr_get_netmask(guint8);

gint netaddr_size(void);
gint netaddr_get_ipaddr_size(struct netaddr *);
void *netaddr_get_addr(struct netaddr *);
gint netaddr_get_addr_size(struct netaddr *);
gint netaddr_get_sa_size(struct netaddr *);
struct sockaddr *netaddr_get_sa(struct netaddr *);
gint netaddr_get_family(struct netaddr *);
void netaddr_set_family(struct netaddr *, gint);
gint netaddr_same_family(struct netaddr *, struct netaddr *);
char *netaddr2str(struct netaddr *, char *, socklen_t);
const char *netaddr_ip2str(struct netaddr *, char *, socklen_t);
const char *netaddr_net2str(struct netaddr *, char *, socklen_t);
struct netaddr *netaddr_calc_netaddr(struct netaddr *, struct netaddr *);
guint16 netaddr_get_port(struct netaddr *);
void *netaddr_get_ipaddr(struct netaddr *);
void netaddr_set_port(struct netaddr *, guint16);
void netaddr_free_list(GSList *);

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/

void netaddr_dump(char *, struct netaddr *);
void netaddr_dump_net(char *, struct netaddr *);
void netaddr_dump_sockaddr(char *, struct sockaddr *);

#endif /* __NETLIB_H */
