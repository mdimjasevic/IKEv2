/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/**
 * \file session.c
 *
 * \brief Functions to manipulate session structure
 *
 * Each peer with whom IKEv2 daemon communicates is represented with
 * session structure. This structure contains all the data necessary
 * for the communication.
 */

#define __SESSION_C

#define LOGGERNAME	"session"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "timeout.h"
#include "transforms.h"
#include "ts.h"
#include "pfkey_msg.h"
#include "auth.h"
#include "pfkey_linux.h"
#include "proposals.h"
#include "config.h"
#include "network.h"
#include "auth.h"
#include "crypto.h"
#include "sad.h"
#include "supplicant.h"
#include "session.h"
#include "message_msg.h"
#include "message.h"

#include "aaa_wrapper.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * CONSTRUTORS AND DESTRUCTORS
 ******************************************************************************/

/**
 * Allocate new session structure
 */
struct session *session_new(void)
{
	struct session *session;
	struct sad_item *si;

	LOG_FUNC_START(2);

	session = g_malloc0(sizeof(struct session));

	session->refcnt = 1;

	si = sad_item_new();

	session->sad = g_slist_append(NULL, si);

	session->auth_data = g_malloc0(sizeof(struct auth_data));

	session->mutex = g_mutex_new();

	session->msg_queue = g_async_queue_new();

#ifdef NATT
	/*
	 * By default we are not doing NAT-T
	 */
	session->natt = FALSE;
#endif /* NATT */

	session->sent_req_mux = g_mutex_new();
	session->sent_resp_mux = g_mutex_new();

	session->msgid = 0xFFFFFFFF;

	session->wsize = 1;
	session->peer_wsize = 1;

	session->next_crl_update = (time_t) -1;

	session->used_cas = NULL;

	session->crl_update_cas = NULL;

	session->preserve_message = FALSE;

	session->embedded_sm_msg = NULL;

#ifdef MOBIKE
	session->ike_auth_exchange_counter = 0;

	session_local_addresses_init(session);
#endif /* MOBIKE */

	LOG_FUNC_END(2);

	return session;
}

/**
 * Allocate new temporary session structure
 */
struct session *session_temporary_new(void)
{
	struct session *session;

	session = g_malloc0(sizeof(struct session));
	session->refcnt = 1;

	return session;
}

/**
 * Increase reference counter of a session
 *
 * @param session
 *
 * This function should be called with lock held on session
 */
void session_ref(struct session *session)
{
	LOG_FUNC_START(1);
	LOG_TRACE("session=%p refcnt=%d", session, session->refcnt);
	g_atomic_int_inc(&(session->refcnt));
	LOG_FUNC_END(1);
}

/**
 * Free session structure
 *
 * @param session
 *
 * \return TRUE if session has been free'd, FALSE if reference
 *		counter is still non-zero
 *
 * This function frees session structure if it's reference coutner,
 * after decrementing, reaches zero. Otherwise, it only removes
 * lock from the session.
 */
gboolean session_free(struct session *session)
{
	gboolean retval = TRUE;

	LOG_FUNC_START(2);

	if (session && g_atomic_int_dec_and_test(&(session->refcnt))) {

		LOG_DEBUG("Freeing session");

		session_transient_free(session);

		if (session->msg_queue)
			g_async_queue_unref(session->msg_queue);

		sad_item_list_free(session->sad);

		if (session->cr)
			config_remote_free(session->cr);

		if (session->cp)
			config_peer_free(session->cp);

		if (session->cg)
			config_general_free(session->cg);

		if (session->selected_proposal)
			proposal_free(session->selected_proposal);

		if (session->sk_d)
			g_free(session->sk_d);

		if (session->sk_ai)
			g_free(session->sk_ai);

		if (session->sk_ar)
			g_free(session->sk_ar);

		if (session->sk_ei)
			g_free(session->sk_ei);

		if (session->sk_er)
			g_free(session->sk_er);

		if (session->sk_pi)
			g_free(session->sk_pi);

		if (session->sk_pr)
			g_free(session->sk_pr);

		if (session->digest)
			g_free(session->digest);

		if (session->certs_for_peer)
			cert_item_list_free(session->certs_for_peer);

		if (session->peer_local_certs)
			cert_item_list_free(session->peer_local_certs);

		if (session->auth_data) {

			if (session->auth_data->public_key)
				g_free(session->auth_data->public_key);
		}

#ifdef AAA_CLIENT
		if (session->aaa_data &&
				GPOINTER_TO_INT(session->aaa_data) != 1)
			aaa_free(session);
#endif /* AAA_CLIENT */

		if (session->mutex) {
			session->mutex = NULL;
//			g_mutex_unlock(session->mutex);
//			g_mutex_free(session->mutex);
		}

		if (session->sent_req_mux)
			g_mutex_free(session->sent_req_mux);

		if (session->sent_resp_mux)
			g_mutex_free(session->sent_resp_mux);

		if (session)
		g_free (session);

	} else {

		LOG_DEBUG("Session not freed (refcnt = %d)",
				session->refcnt);
		retval = FALSE;
	}

out:
	LOG_FUNC_END(2);

	return retval;
}

/**
 * This function removes all the transient data from session structure.
 *
 * Transient data is necessary only during certain phases of session
 * lifetime, e.g. during IKE SA establishment, rekeying, ...
 */
void session_transient_free(struct session *session)
{
	LOG_FUNC_START(2);

	if (session) {

		if (session->i_nonce) {
			BN_free(session->i_nonce);
			session->i_nonce = NULL;
		}

		if (session->r_nonce) {
			BN_free(session->r_nonce);
			session->r_nonce = NULL;
		}

		if (session->dh) {
			DH_free(session->dh);
			session->dh = NULL;
		}

		if (session->pub_key) {
			BN_free(session->pub_key);
			session->pub_key = NULL;
		}

		if (session->dh_shared_key) {
			g_free(session->dh_shared_key);
			session->dh_shared_key = NULL;
		}

		if (session->ikesa_init_i) {
			g_free(session->ikesa_init_i);
			session->ikesa_init_i = NULL;
		}

		if (session->ikesa_init_r) {
			g_free(session->ikesa_init_r);
			session->ikesa_init_r = NULL;
		}
	}

	LOG_FUNC_END(1);
}

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/

#ifdef NATT
void session_nat_hash_clear(struct session *session)
{
	while (session->nat_src_hashes) {
		g_free(session->nat_src_hashes->data);
		session->nat_src_hashes = g_slist_remove(
					session->nat_src_hashes,
					session->nat_src_hashes->data);
	}
}

/**
 * Add new source address digest 
 *
 * @param session
 * @param digest
 *
 * After calling this function digest should not be free'd!
 */
void session_nat_hash_append(struct session *session, guchar *digest)
{
	session->nat_src_hashes = g_slist_append(session->nat_src_hashes,
						digest);
}
#endif /* NATT */

guint64 session_get_i_spi(struct session *session)
{
	return session->i_spi;
}

void session_set_i_spi(struct session *session, guint64 i_spi)
{
	session->i_spi = i_spi;
}

guint64 session_get_r_spi(struct session *session)
{
	return session->r_spi;
}

void session_set_r_spi(struct session *session, guint64 r_spi)
{
	session->r_spi = r_spi;
}

guint16 session_get_wsize(struct session *session)
{
	return session->wsize;
}

void session_set_wsize(struct session *session, guint16 wsize)
{
	session->wsize = wsize;
}

guint32 session_get_msgid_lo(struct session *session)
{
	LOG_DEBUG("session->msgid_lo=%d", session->msgid_lo);
	return session->msgid_lo;
}

void session_set_msgid_lo(struct session *session, guint32 msgid_lo)
{
	session->msgid_lo = msgid_lo;
}

guint32 session_get_msgid_hi(struct session *session)
{
	return session->msgid_hi;
}

void session_set_msgid_hi(struct session *session, guint32 msgid_hi)
{
	session->msgid_hi = msgid_hi;
}

guint32 session_get_peer_msgid_lo(struct session *session)
{
	return session->peer_msgid_lo;
}

void session_set_peer_msgid_lo(struct session *session, guint32 msgid_lo)
{
	session->peer_msgid_lo = msgid_lo;
}

guint8 session_get_auth_type(struct session *session)
{
	guint8 auth_method;

	auth_method = session->cp->auth_method & 0x000000FF;

	if (!auth_method)
		LOG_ERROR("No authentication method defined!");

	return auth_method;
}

void session_set_auth_type(struct session *session, guint8 auth_type)
{
#warning "Incomplete implementation of this function"
}

/**
 * Get next available message id
 *
 * @param session
 *
 * \return	0	if no msg_id's a currently available, otherwise,
 *			valid msg_id number
 *
 * Note that because of return value that indicates error, this function
 * cannot be called for IKE_SA_INIT request message.
 *
 * TODO: This should be made atomic because of concurrent calls!
 */
guint32 session_get_next_msgid(struct session *session)
{
	guint32 msg_id = 0;

	g_mutex_lock(session->sent_req_mux);

	LOG_DEBUG("Increment msgid_hi");
	LOG_DEBUG("msgid_hi=%u msgid_lo=%u wsize=%u",
			session->msgid_hi, session->msgid_lo, session->wsize);

	if (session->msgid_hi - session->msgid_lo < session->wsize)
		msg_id = session->msgid_hi++;

	g_mutex_unlock(session->sent_req_mux);

	return msg_id;
}

guint8 session_get_ike_type(struct session *session)
{
	return session->ike_type;
}

void session_set_ike_type(struct session *session, guint8 ike_type)
{
	session->ike_type = ike_type;
}

guint8 session_get_state(struct session *session)
{
	return session->state;
}

void session_set_state(struct session *session, guint8 state)
{
	LOG_DEBUG("new state=%d", state);
	session->state = state;
}

gboolean session_get_shutdown(struct session *session)
{
	return session->shutdown;
}

void session_set_shutdown(struct session *session, gboolean state)
{
	session->shutdown = state;
}

GSList *session_get_proposals(struct session *session)
{
	return session->cr->proposals;
}

/**
 * Get selected proposal for session
 */
proposal_t *session_get_selected_proposal(struct session *session)
{
	return session->selected_proposal;
}

/**
 * Set selected proposal for session
 */
void session_set_selected_proposal(struct session *session,
		proposal_t *proposal)
{
	session->selected_proposal = proposal;
}

void session_set_i_addr(struct session *session, struct netaddr *i_addr)
{
	session->i_addr = netaddr_dup(i_addr);
}

struct netaddr *session_get_i_addr(struct session *session)
{
	return session->i_addr;
}

void session_set_r_addr(struct session *session, struct netaddr *r_addr)
{
	session->r_addr = netaddr_dup(r_addr);
}

struct netaddr *session_get_r_addr(struct session *session)
{
	return session->r_addr;
}

void session_set_user_initiated(struct session *session, gboolean flag)
{
	session->user_initiated = flag;
}

/**
 * Return TRUE if the session was initiated manually by the user
 *
 * @param session
 *
 * \return TRUE if it is, otherwise FALSE
 */
gboolean session_get_user_initiated(struct session *session)
{
	return session->user_initiated;
}

/*******************************************************************************
 * MISC FUNCTIONS
 ******************************************************************************/

/**
 * Try to lock session
 *
 * @param session
 *
 * \return TRUE if session successfuly locked, FALSE if it was alredy locked
 */
gboolean session_try_lock(struct session *session)
{
	LOG_DEBUG("session=%p", session);
	return g_mutex_trylock(session->mutex);
}

/**
 * Lock session structure
 *
 * @param session
 */
void session_lock(struct session *session)
{
	LOG_FUNC_START(2);

	LOG_DEBUG("session=%p", session);
	if (session)
		g_mutex_lock(session->mutex);
	LOG_FUNC_END(2);
}

/**
 * Unlock session
 *
 * @param session
 */
void session_unlock(struct session *session)
{
	LOG_FUNC_START(2);
	LOG_DEBUG("session=%p", session);
	if (session)
		g_mutex_unlock(session->mutex);
	LOG_FUNC_END(2);
}

/**
 * Check if there are any messages in wait queue and transfer them
 * to work queue.
 *
 * @param session
 *
 * \return TRUE if there are elements in queue, otherwise FALSE
 */
gboolean session_check_queue(struct session *session)
{
	gint ret;

	LOG_FUNC_START(1);

	ret = FALSE;

	if (session) {
		g_async_queue_lock(session->msg_queue);
		ret = g_async_queue_length_unlocked(session->msg_queue);
		g_async_queue_unlock(session->msg_queue);
	}

	LOG_DEBUG("Number of requests in the queue is %u", ret);

	LOG_FUNC_END(1);

	return ret != 0;
}

/**
 * Push message into queue for processing
 *
 * @param session
 * @param msg
 * 
 * \return TRUE if queue was empty, FALSE otherwise
 */
gboolean session_msg_push(struct session *session, gpointer msg)
{
	gint ret;

	g_async_queue_lock(session->msg_queue);
	ret = g_async_queue_length_unlocked(session->msg_queue);
	g_async_queue_push_unlocked(session->msg_queue, msg);
	g_async_queue_unlock(session->msg_queue);

	LOG_DEBUG("Number of requests in the queue is %u", ret);

	return ret == 0;
}

/**
 * Pop a message from a queue
 */
gpointer session_msg_pop(struct session *session)
{
	return g_async_queue_pop(session->msg_queue);
}

/**
 * Add given CHILD SA to a list of CHILD SAs
 *
 * @param session
 * @param si
 *
 * This function and the one for removal of sad_item implement
 * ordering, meaning each newly added sad_item will be the "last"
 * one in some list. This is important in case another data structure
 * is used since during reautentication process we relay on ordering
 * to know if we finished transfering of all sad_items.
 */
void session_sad_item_add(struct session *session, struct sad_item *si)
{
	session->sad = g_slist_append(session->sad, si);
}

/**
 * Remove given CHILD SA from a list of CHILD SAs
 *
 * @param session
 * @param si
 *
 * See the note for the session_sad_item_add function.
 */
void session_sad_item_remove(struct session *session, struct sad_item *si)
{
	session->sad = g_slist_remove(session->sad, si);
}

/**
 * Return first (and only) CHILD SA.
 *
 * \return Pointer to a child SA structure or NULL in case of an error
 *
 * This function returns pointer to CHILD SA. It assumes that there is
 * only one (which is returned) CHILD SA which is correct during IKA SA
 * INIT exchanges. In case there are more CHILD SA elements, NULL is
 * returned and error is logged.
 *
 * TODO: Introduce protocol parameter!
 */
struct sad_item *session_get_sad_item(struct session *session)
{
	if (!(session->sad))
		return NULL;

	if (session->sad->next)
		LOG_BUG("To many CHILD SA structures in session");

	return session->sad->data;
}

/**
 * Find CHILD SA by SPI value
 *
 * @param session	Session with all CHILD SAs
 * @param spi		SPI value for which to search.
 *
 * \return Pointer to CHILD SA structure, or NULL if not found
 */
struct sad_item *session_sad_item_find_by_spi(struct session *session,
		guint32 spi)
{
	struct sad_item *si;
	GSList *sad;

	LOG_FUNC_START(1);
	LOG_TRACE("spi=%08x", spi);

	sad = session->sad;

	while (sad) {
		si = sad->data;

		if (sad_item_get_spi(si) == spi)
			goto out;

		sad = sad->next;
	}

	si = NULL;

out:
	LOG_FUNC_END(1);

	return si;
}

/**
 * Find CHILD SA by peer's SPI value
 *
 * @param session	Session with all CHILD SAs
 * @param peer_spi	SPI value for which to search.
 *
 * \return Pointer to CHILD SA structure, or NULL if not found
 */
struct sad_item *session_sad_item_find_by_peer_spi(struct session *session,
		guint32 peer_spi)
{
	struct sad_item *si;
	GSList *sad;

	LOG_FUNC_START(1);
	LOG_TRACE("peer_spi=%08x", peer_spi);

	sad = session->sad;

	while (sad) {
		si = sad->data;

		if (sad_item_get_peer_spi(si) == peer_spi)
			goto out;

		sad = sad->next;
	}

	si = NULL;

out:
	LOG_FUNC_END(1);

	return si;
}

/**
 * Find CHILD SA by SPI value & protocol
 *
 * @param session	Session with all CHILD SAs
 * @param protocol
 * @param spi		SPI value for which to search.
 *
 * \return Pointer to CHILD SA structure, or NULL if not found
 */
struct sad_item *session_sad_item_find_by_protocol_and_spi(
			struct session *session,
			guint8 protocol, guint32 spi)
{
	struct sad_item *si;
	GSList *sad;

	LOG_FUNC_START(1);
	LOG_TRACE("protocol=%d, spi=%08x", protocol, spi);

	sad = session->sad;

	while (sad) {
		si = sad->data;
		if ((sad_item_get_spi(si) == spi
				|| sad_item_get_peer_spi(si) == spi) 
				&& (sad_item_get_protocol(si) == protocol))
			goto out;

		sad = sad->next;
	}

	si = NULL;

out:
	LOG_FUNC_END(1);

	return si;
}

/**
 * Given msg_id find CHILD SA that is expects response to a request with
 * given ID.
 *
 * @param session	Session with all CHILD SAs
 * @param msg_id	Message ID to search for
 *
 * \return Pointer to CHILD SA structure, or NULL if not found
 */
struct sad_item *session_sad_item_find_by_msg_id(struct session *session,
		guint32 msg_id)
{
	struct sad_item *si;
	GSList *sad;

	LOG_FUNC_START(1);
	LOG_TRACE("msg_id=%lX", msg_id);

	sad = session->sad;

	while (sad) {
		si = sad->data;
		sad = sad->next;

		if (!sad_item_wait_response(si))
			continue;

		if (sad_item_get_msg_id(si) == msg_id)
			goto out;
	}

	si = NULL;

out:
	LOG_FUNC_END(1);

	return si;
}

/**
 * Compute all needed session keys
 *
 * @param session	Session for which to compute keys. All the
 *			necessary date have to be in place.
 *
 * \return 0 everything was OK, -1 an error occured.
 *
 * TODO: Analyse if this function belongs to message or session subsystem.
 *       (i believe since it uses data only from session that this is
 *        right place)
 */
int session_crypto_material(struct session *session)
{
	unsigned char *nonces, *buff;
	char *skeyseed, *key, *p;
	gint16 nli, nlr, kslen, elen, ilen, plen, keylen;

	LOG_FUNC_START(1);

	/*
	 * First, compute shared DH key.
	 */
	session->dh_shared_key = g_malloc(DH_size(session->dh));

	session->dh_shared_key_len = DH_compute_key(session->dh_shared_key,
					session->pub_key, session->dh);
	if (session->dh_shared_key_len < 0) {
		/*
		 * If this happens that it's an internal error!
		 */
		LOG_ERROR("Illegal DH shared key len");
		return -1;
	}

	/*
	 * Then, compute session keys.
	 */
	nli = BN_num_bytes(session->i_nonce);
	nlr = BN_num_bytes(session->r_nonce);

	/*
	 * TODO: This check must be placed somewhere BEFORE, to enable sending of
	 *	 Notification with error message.
	 */
	if (!(plen = transform_get_key_len(session->prf_type))) {
		LOG_TRACE("keysize for prf function set in transform is 0."
			"Will try default.");

		plen = transform_prf_get_key_len(session->prf_type);
	}

	LOG_TRACE("prf_id=%d, plen: %d",
			transform_get_id(session->prf_type), plen);

	if (nli < plen/2 || nlr < plen/2) {
		LOG_ERROR("Sum of nonces length (%u) is to small, expected %u",
				nli+nlr, plen);
		return -1;
	}

	/*
	 * 16 = len of 2x SPI
	 */
	nonces = g_malloc(nli + nlr + 16);

	BN_bn2bin(session->i_nonce, nonces);
	BN_bn2bin(session->r_nonce, nonces + nli);

	/* TODO: the following correction is for fixed key sizes and
	 * will not work for odd length key size. What to do in such cases?
	 */
	if (nli+nlr > plen && prf_has_fixed_key_length(session->prf_type)) {

		/* Construct half key from i_nonce, half key from r_nonce. */

		buff = g_malloc(plen);

		memcpy(buff, nonces, plen/2);
		memcpy(buff + (plen/2), nonces+nli, plen/2);

		kslen = prf(session->prf_type, buff, plen,
					session->dh_shared_key,
					session->dh_shared_key_len,
					&skeyseed);

		g_free(buff);

	} else {

		kslen = prf(session->prf_type, nonces, nli + nlr,
			session->dh_shared_key, session->dh_shared_key_len,
						&skeyseed);

	}

	LOG_DEBUG("Nonce data");
	log_buffer(LOGGERNAME, (char *)nonces, nli + nlr);

	LOG_DEBUG("session->dh_shared_key");
	log_buffer(LOGGERNAME, (gchar *)session->dh_shared_key,
					session->dh_shared_key_len);

	if (kslen < 0) {
		LOG_ERROR("Negative key length");
		return -1;
	}

	LOG_DEBUG("skeyseed: ");
	log_data(LOGGERNAME, skeyseed, kslen);

	/*
	 * TODO: The following two key lengths should be written in
	 * transform. Change input parameters to *_keylen_get into
	 * transform_t, and let those functions behave as
	 * getters.
	 */
	elen = encr_keylen_get(session->encr_type);
	ilen = integ_keylen_get(session->integ_type);
	keylen = 2 * elen + 2 * ilen + 3 * plen;

	*((guint64 *)(nonces + nli + nlr)) = session->i_spi;
	*((guint64 *)(nonces + nli + nlr + 8)) = session->r_spi;

	if (prf_plus(session->prf_type, skeyseed, kslen, (gchar *)nonces,
			nli + nlr + 16, &key, keylen) < 0) {
		LOG_ERROR("Error calculating prf function");
		return -1;
	}

	g_free(nonces);
      
	p = key;
	session->sk_d = g_memdup(p, plen);
	p += plen;

	LOG_DEBUG("session->sk_d: ");
	log_data(LOGGERNAME, session->sk_d, plen);

	session->sk_ai = g_memdup(p, ilen);
	p += ilen;

	LOG_DEBUG("session->sk_ai: ");
	log_data(LOGGERNAME, session->sk_ai, ilen);

	session->sk_ar = g_memdup(p, ilen);
	p += ilen;

	LOG_DEBUG("session->sk_ar: ");
	log_data(LOGGERNAME, session->sk_ar, ilen);

	session->sk_ei = g_memdup(p, elen);

	encr_build_key(session->encr_type, session->sk_ei);
	p += elen;

	LOG_DEBUG("session->sk_ei: ");
	log_data(LOGGERNAME, session->sk_ei, elen);

	session->sk_er = g_memdup(p, elen);
	encr_build_key(session->encr_type, session->sk_er);
	p += elen;

	LOG_DEBUG("session->sk_er: ");
	log_data(LOGGERNAME, session->sk_er, elen);

	session->sk_pi = g_memdup(p, plen);
	p += plen;

	LOG_DEBUG("session->sk_pi: ");
	log_data(LOGGERNAME, session->sk_pi, plen);

	session->sk_pr = g_memdup(p, plen);

	LOG_DEBUG("session->sk_pr: ");
	log_data(LOGGERNAME, session->sk_pr, plen);

	g_free(key);

	LOG_FUNC_END(1);

	return 0;
}

/**
 * Dump human readable representation of session...
 */
void _session_dump(struct session *session)
{

	printf ("Dumping session structure\n");
	printf ("\tike_type=");
	switch (session->ike_type) {
	case IKEV2_INITIATOR:
		printf ("IKEV2_INITATOR\n");
		break;

	case IKEV2_RESPONDER:
		printf ("IKEV2_RESPONDER\n");
		break;

	default:
		printf ("UNKNOWN\n");
		break;
	}

	printf ("\ti_spi=%llx\n", session->i_spi);
	printf ("\tr_spi=%llx\n", session->r_spi);

	printf ("\tmsg_id_lo=%08x\n", session_get_msgid_lo(session));
	printf ("\tmsg_id_hi=%08x\n", session_get_msgid_hi(session));

	printf ("session->srcaddr=");
	netaddr_dump(LOGGERNAME, session->i_addr);

	printf ("session->dstaddr=");
	netaddr_dump(LOGGERNAME, session->r_addr);

}

/**
 * Dump machine readable representation of session...
 */
void _session_dump_binary(struct session *session)
{
}

/**
 *
 */
gboolean session_use_radius(struct session *session)
{
#ifdef AAA_CLIENT
	if (session && session->cp && session->cp->radius_server)
		return TRUE;
	else
#endif
		return FALSE;
}

#ifdef MOBIKE
/**
 * Initializes lists of local IPv4 and IPv6 addresses
 *
 * @param session		session to which addresses should be assigned
 */
void session_local_addresses_init(struct session *session) {

	struct network_address_iterator_t *network_data_iterator = NULL;
	netaddr_t *addr;

	LOG_FUNC_START(1);

	session->local_ipv4_addresses = NULL;
	session->local_ipv6_addresses = NULL;

	/* Fetch list of IPv4 addresses */

	network_data_iterator = network_address_iterator_new(AF_INET, NULL);

	addr = network_address_iterator_next(network_data_iterator);

	while(addr) {
		session->local_ipv4_addresses =
				g_slist_append(session->local_ipv4_addresses, addr);
		addr = network_address_iterator_next(network_data_iterator);
	}

	network_address_iterator_free(network_data_iterator);

	/* Fetch list of IPv6 addresses */

	network_data_iterator = network_address_iterator_new(AF_INET6, NULL);

	addr = NULL;
	addr = network_address_iterator_next(network_data_iterator);

	while(addr) {
		session->local_ipv6_addresses =
				g_slist_append(session->local_ipv6_addresses, addr);
		addr = network_address_iterator_next(network_data_iterator);
	}

	network_address_iterator_free(network_data_iterator);

	LOG_FUNC_END(1);
}
#endif /* MOBIKE */
