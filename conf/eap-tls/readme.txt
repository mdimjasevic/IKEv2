Network configuration:

    +-----------+           +-----------+           +-----------+
    |           |  vmnet2   |           |  vmnet3   |           |
    | Initiator |-----------| Responder |-----------|    AAA    |
    |           | .2     .1 |           | .1     .2 |           |
    +-----------+           +-----------+           +-----------+
                 eth1   eth1             eth2   eth1
                 10.0.0.0/24            172.16.0.0/24


ikev2 - for Initiator - compiled with: configure --enable-eap
ikev2 - for Responder - compiled with: configure --enable-radius-client

++++
In logfile of responder:
"Peer authenticated successfuly"

In logfile of initiator:
"EAP authentication completed successfuly"
