/**
 * The code in this file demonstrates how to list all the interfaces
 * in the system as well as how to obtain their addresses.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE	10240
#define MAX_ADDR_LEN	64

void rt_dump_addrmsg(struct nlmsghdr *nlh)
{
	int len, nlhlen;
	char addr[MAX_ADDR_LEN];
	struct ifaddrmsg *ifaddrmsg;
	struct rtattr *rta;
	struct ifa_cacheinfo *ifc;

	printf ("nlmsghdr->nlmsg_len=%u\n", nlh->nlmsg_len);
	printf ("nlmsghdr->nlmsg_type= ");
        switch(nlh->nlmsg_type) {
	case RTM_NEWADDR:
		printf ("RTM_NEWADDR\n");
		break;
	case RTM_DELADDR:
		printf ("RTM_DELADDR\n");
		break;
	case RTM_GETADDR:
		printf ("RTM_GETADDR\n");
		break;
	default:
		printf ("unknown\n");
		return;
	}

	printf ("nlmsghdr->nlmsg_flags=");
	if (nlh->nlmsg_flags) {
		if (nlh->nlmsg_flags & NLM_F_REQUEST)
			printf ("NLM_F_REQUEST ");
		if (nlh->nlmsg_flags & NLM_F_MULTI)
			printf ("NLM_F_MULTI ");
		if (nlh->nlmsg_flags & NLM_F_ACK)
			printf ("NLM_F_ACK ");
		if (nlh->nlmsg_flags & NLM_F_ECHO)
			printf ("NLM_F_ECHO ");
		if (nlh->nlmsg_flags & NLM_F_ROOT)
			printf ("NLM_F_ROOT ");
		if (nlh->nlmsg_flags & NLM_F_MATCH)
			printf ("NLM_F_MATCH ");
		if (nlh->nlmsg_flags & NLM_F_ATOMIC)
			printf ("NLM_F_ATOMIC ");
		if (nlh->nlmsg_flags & NLM_F_DUMP)
			printf ("NLM_F_DUMP ");
		if (nlh->nlmsg_flags & NLM_F_REPLACE)
			printf ("NLM_F_REPLACE ");
		if (nlh->nlmsg_flags & NLM_F_EXCL)
			printf ("NLM_F_EXCL ");
		if (nlh->nlmsg_flags & NLM_F_CREATE)
			printf ("NLM_F_CREATE ");
		if (nlh->nlmsg_flags & NLM_F_APPEND)
			printf ("NLM_F_APPEND");
	} else
		printf ("(none)");
	printf ("\n");

	printf ("nlmsghdr->nlmsg_seq=%u\n", nlh->nlmsg_seq);
	printf ("nlmsghdr->nlmsg_pid=%u\n", nlh->nlmsg_pid);

	len = nlh->nlmsg_len - NLMSG_LENGTH(0);

	ifaddrmsg = NLMSG_DATA(nlh);

	printf ("\tifaddrmsg->ifa_family=%u\n", ifaddrmsg->ifa_family);
	printf ("\tifaddrmsg->ifa_prefixlen=%u\n", ifaddrmsg->ifa_prefixlen);
	printf ("\tifaddrmsg->ifa_flags=%08X\n", ifaddrmsg->ifa_flags);
	printf ("\tifaddrmsg->ifa_scope=");
	switch (ifaddrmsg->ifa_scope) {
	case RT_SCOPE_UNIVERSE:
		printf ("RT_SCOPE_UNIVERSE\n");
		break;
	case RT_SCOPE_SITE:
		printf ("RT_SCOPE_SITE\n");
		break;
	case RT_SCOPE_LINK:
		printf ("RT_SCOPE_LINK\n");
		break;
	case RT_SCOPE_HOST:
		printf ("RT_SCOPE_HOST\n");
		break;
	default:
		printf ("unknown\n");
		break;
	}

	printf ("\tifaddrmsg->ifa_index=%08X\n", ifaddrmsg->ifa_index);

	rta = (struct rtattr *)(ifaddrmsg + 1);

	while (RTA_OK(rta, len)) {
		switch(rta->rta_type) {
		case IFA_UNSPEC:
			printf("IFA_UNSPEC\n");
			break;
		case IFA_ADDRESS:
			printf ("\t\tAddress: ");
			switch (ifaddrmsg->ifa_family) {
			case AF_INET:
				inet_ntop(AF_INET, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			case AF_INET6:
				inet_ntop(AF_INET6, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			default:
				printf ("error!\n");
			}
			break;
		case IFA_LOCAL:
			printf ("\t\tLocal address: ");
			switch (ifaddrmsg->ifa_family) {
			case AF_INET:
				inet_ntop(AF_INET, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			case AF_INET6:
				inet_ntop(AF_INET6, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			default:
				printf ("error!\n");
			}
			break;
		case IFA_LABEL:
			printf ("\t\t%s\n", (char *)RTA_DATA(rta));
			break;
		case IFA_BROADCAST:
			printf ("\t\tBroadcast address: ");
			switch (ifaddrmsg->ifa_family) {
			case AF_INET:
				inet_ntop(AF_INET, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			case AF_INET6:
				inet_ntop(AF_INET6, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			default:
				printf ("error!\n");
			}
			break;
		case IFA_ANYCAST:
			printf ("\t\tAnycast address: ");
			switch (ifaddrmsg->ifa_family) {
			case AF_INET:
				inet_ntop(AF_INET, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			case AF_INET6:
				inet_ntop(AF_INET6, RTA_DATA(rta), addr, MAX_ADDR_LEN);
				printf ("%s\n", addr);
				break;
			default:
				printf ("error!\n");
			}
			break;
		case IFA_CACHEINFO:
			ifc = RTA_DATA(rta);
			printf ("\t\tprefered lifetime=%u\n", ifc->ifa_prefered);
			printf ("\t\tvalid lifetime=%u\n", ifc->ifa_valid);
			break;
		}
		rta = RTA_NEXT(rta, len);
	}
}
