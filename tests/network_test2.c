/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing network module
 *
 * Test thread version of network registration function
 */

#define __NETWORK_TEST_C

#define LOGGERNAME	"network"

#include <stdio.h>

#include <sys/types.h>
#include <netinet/ip.h>

#include <linux/if_ether.h>

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "config.h"
#include "network_msg.h"

gpointer func_cb(gpointer _network_msg)
{
	struct network_msg *msg = _network_msg;
	char buf[64];

	LOG_NOTICE("Octets received %u", msg->size);

	LOG_NOTICE("Source address %s",
			netaddr_ip2str(msg->srcaddr, buf, 64));

	network_msg_free(msg);

	return NULL;
}

int main(int argc, char **argv)
{
	GMainLoop *ml;
	network_t *ns;
	netaddr_t *na;

	struct config *config;

	LOG_NOTICE("This test program creates UDP socket, binds it to a port");
	LOG_NOTICE("12345 and waits for some traffic. It prints data about");
	LOG_NOTICE("what it has received.");

	if (argc != 2) {
		LOG_ERROR("Usage: %s <logging.conf>", argv[0]);
		goto out;
	}

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to network module
	 */
	ml = g_main_loop_new(NULL, FALSE);

        /*
         * Initialize logging subsystem
         */
	if (config_init(argv[1], &config) < 0)
		goto out;

	if (logging_init(config->logging) < 0)
		goto out;

	LOG_NOTICE("Initializing network module");
	if (network_init(NULL) < 0)
		goto out;

	/*
	 * Prepare address on which to bind...
	 */
	if ((na = netaddr_new()) == NULL)
		goto out;

	netaddr_set_family(na, AF_INET);
	netaddr_set_port(na, 12345);

	LOG_NOTICE("Registering listening socket");
	ns = network_socket_register_thread(AF_INET, SOCK_DGRAM, 0,
			na, 0, func_cb);
	if (ns == NULL)
		goto out;

	/*
	 * Start main loop...
	 */
	LOG_NOTICE("Starting main loop");
	g_main_loop_run(ml);

	/*
	 * Unload network module
	 */
	LOG_NOTICE("Unloading network module");
	network_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	LOG_NOTICE("Exiting");

out:
	logging_unload();

	return 0;
}
