/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/ 

#ifndef __MESSAGE_H
#define __MESSAGE_H

/*
 * Discriminator ID of messages sent throuth async queue to upper layers.
 */
#define MESSAGE_MSG	1
#define TIMEOUT_MSG	3

/*
 * Maximum message size that can be received....
 */
#define TB_SIZE		4096

/*
 * Nonce len. TODO: This should actually be configuration parameter...
 */
#define NONCE_LEN	128

/*
 * How offten we regenerate secrets (in s)...
 */
#define MESSAGE_SECRET_LIFETIME		3

/*
 * Define how long secret we have for generating secrets
 */
#define MESSAGE_SECRET_LEN	16
#define MESSAGE_COOKIE_LEN	16

/*
 * Flags returned during message parsing.
 *
 * This have to be in the higher 16 bits of 32-bit word!
 */
#define MSGPARSE_RESPONSE_NOTIFY	(1 << 16)
#define MSGPARSE_SEND_NOTIFY		(1 << 17)
#define MSGPARSE_TERMINATE_MSG		(1 << 18)
#define MSGPARSE_TERMINATE_SESSION	(1 << 19)
#define MSGPARSE_RETRANSMIT_RESPONSE	(1 << 20)

#ifdef __MESSAGE_C

/*
 * Constants used during message parsing
 */
#define	MSG_PAYLOAD_SA		(1 << 0)
#define	MSG_PAYLOAD_KE		(1 << 2)
#define	MSG_PAYLOAD_IDI		(1 << 3)
#define	MSG_PAYLOAD_IDR		(1 << 4)
#define	MSG_PAYLOAD_CERT	(1 << 5)
#define	MSG_PAYLOAD_CERTREQ	(1 << 6)
#define	MSG_PAYLOAD_AUTH	(1 << 7)
#define	MSG_PAYLOAD_NONCE	(1 << 8)
#define	MSG_PAYLOAD_NOTIFY	(1 << 9)
#define	MSG_PAYLOAD_DELETE	(1 << 10)
#define	MSG_PAYLOAD_VENDORID	(1 << 11)
#define	MSG_PAYLOAD_TSI		(1 << 12)
#define	MSG_PAYLOAD_TSR		(1 << 13)
#define	MSG_PAYLOAD_ENCRYPTED	(1 << 14)
#define	MSG_PAYLOAD_CONFIG	(1 << 15)
#define	MSG_PAYLOAD_EAP		(1 << 16)

/*
 * The list of allowed payloads in messages
 */
guint32 valid_messages[] = {
	0,
	1,
	2,
	3
};

#endif /* __MESSAGE_C */

/*
 * Structure used for keeping data about sent, but not yet acknowledged
 * messages...
 */
struct sent_msg {

	/*
	 * Lock to avoid race between timeout and received response
	 */
	GMutex *lock;

	/*
	 * Reference counting for structure
	 */
	gint refcnt;

	/*
	 * Session to which this request belongs...
	 */
	struct session *session;

	/*
	 * Address to which packet has been sent
	 */
	struct netaddr *daddr;

	/*
	 * Socket on which packet has been sent
	 */
	struct network_socket *ns;

	/*
	 * Message ID
	 *
	 * TODO: Is it enough to keep only msg_id or we have to differentiate
	 * and thus introduce separate values for i_msg_id and r_msg_id?
	 */
	guint32 msg_id;

	/*
	 * Is this a response message
	 */
	gboolean response;

	/*
	 * Number of retries left
	 */
	guint8 retries;

	/*
	 * Last used timeout value
	 */
	GTimeVal tv;

	/*
	 * Timeout handler for this packet
	 */
	timeout_t *to;

	/*
	 * Pointer to network packet and it's size
	 */
	void *packet;
	guint16 packet_size;
};

/*
 * Message sent to state machine subsystem when retry count for some
 * goes down...
 */
struct timeout_msg {
	/*
	 * Discriminator field so that state machine subsystem can
	 * differentiate between different messages sent to it.
	 */
	guint8 msg_type;

	struct session *session;
};

/*
 * Structure used to check validity of received messages
 */
struct payload_sm {
	struct ikev2_header *hdr;
	gint8 state;
};

/*
 * Authentication data...
 */
#warning "This has to be modified to efficiently include all authentication types"
struct auth_data {
	guint8 auth_type;

	char *key;
	guint key_len;

	/* For RSA_sign(): */
	char *priv_key_file;
	int priv_key_file_len;

	/* For RSA_verify(): */
	unsigned char *public_key;
	int public_key_len;

	/* FIXME: Remove this. */
	guint8 cert_type;
};

/*
 * Structure with information global to all sessions.
 */
struct message_data {
	/*
	 * Is this subsystem in a process of shutting down?
	 */
	gboolean shutdown;

	/*
	 * Sources of different threades, and different threads, 
	 * to be able to terminate them properly...
	 */
	GThread *main_thread;

	/*
	 * Timeout that will periodically trigger secret refresh.
	 */
	timeout_t *secret_to;

	/*
	 * Queue for sending asynchronous messages to upper layers.
	 */
	GAsyncQueue *upper_queue;

	/*
	 * Queue for communication from network subsystem to messaging
	 * subsystem.
	 */
	GAsyncQueue *network_queue;

	/*
	 * Private data for networking subsystem.
	 */
	network_t *network_data;

	/*
	 * The following three fields are used for generating cookies...
	 * secret is currently used secret for generating cookies, while
	 * secret_version is it's version. old_secret is previously used
	 * secret that't used for some time after it is replaced as active.
	 */
	char *secret;
	char *old_secret;

	/*
	 * Data used for sending and receiving network traffic...
	 *
	 * NOTE: send_sock pointer is only a copy from listen_socks that is
	 * used for sending initial packets (i.e. we don't know which source
	 * address will be used for a packet). In other words, when freeing
	 * this structure, send_sock is not explicitly freed.
	 */
	struct network_socket *send_sock;

#ifdef NATT
	struct network_socket *send_sock_natt;
#endif

	/**
	 * Socket used to send data when NAT traversal
	 * is in progress.
	 */
	struct network_socket *natt_send_sock;

	/* 
	 * send_sock6 is an IPv6 socket
	 */
	struct network_socket *send_sock6;
	GSList *listen_socks;
};

/*
 * Structure for NOTIFY data
 */

struct notify {
	guint16 type;
	union {
		/*
		 * UNSUPPORTED_CRITICAL_PAYLOAD
		 */
		char ch;

		/*
		 * NAT_DETECTION_SOURCE_IP
		 * NAT_DETECTION_DESTINATION_IP
		 */
		char sha1[SHA1_DIGEST_SIZE];

		/*
		 * INVALID_KE_PAYLOAD
		 */
		guint16 dh_group;

		/*
		 * IPCOMP_SUPPORTED
		 */
		guint16 cpi;

		/*
		 * INVALID_MESSAGE_ID
		 * SET_WINDOW_SIZE
		 */
		guint32 data;

		/*
		 * COOKIE
		 * INVALID_SELECTORS
		 */
		struct {
			gint len;
			void *data;
		};
	};
};

/*******************************************************************************
 * Functions for manipulating cookies
 ******************************************************************************/
void _message_calculate_cookie(struct message_msg *, char *, char *);
void message_calculate_cookie(struct message_msg *);
gboolean message_check_cookie(struct message_msg *);

/*******************************************************************************
 * Authentication functions
 ******************************************************************************/
int auth_data_calculate(transform_t *, struct auth_data *, gpointer *, int,
		char *, int, BIGNUM *, char *, char **);
int auth_data_verify(transform_t *, struct auth_data *, struct id *,
                char *, int, BIGNUM *, char *, char *, int);

/*******************************************************************************
 * Function that work with 'struct timeout_msg'
 ******************************************************************************/
struct timeout_msg *timeout_msg_alloc(void);
void timeout_msg_free(struct timeout_msg *);

/*******************************************************************************
 * Function that work with 'struct sent_msg'
 ******************************************************************************/
struct sent_msg *sent_msg_new(void);
void sent_msg_free(struct sent_msg *);
void sent_msg_lock(struct sent_msg *);
void sent_msg_unlock(struct sent_msg *);
void sent_msg_ref(struct sent_msg *);
gboolean sent_msg_unref_and_test(struct sent_msg *);
gint sent_cmp_msgid(gconstpointer, gconstpointer);

/*******************************************************************************
 * Function for constructing and parsing network messages...
 ******************************************************************************/
int message_send_cookie(struct message_msg *);
int message_send_keepalive_packet(struct session *);
int message_send_ike_sa_init_i(struct session *, struct message_msg *);
int message_send_ike_sa_init_r(struct session *, struct message_msg *);
int message_send_ike_auth_i(struct session *);
int message_send_ike_auth_r(struct session *, struct message_msg *);
int message_send_ike_eap_i(struct session *);
int message_send_ike_eap_r(struct session *, struct message_msg *);
int message_send_ike_auth_final_i(struct session *);
int message_send_ike_auth_final_r(struct session *, struct message_msg *);
int message_send_child_sa_i(struct session *, struct sad_item *);
int message_send_child_sa_r(struct session *, struct sad_item *,
		struct message_msg *);
int message_send_ike_sa_rekey_i(struct session *);
int message_send_ike_sa_rekey_r(struct session *, struct session *,
		struct message_msg *);
int message_send_delete(struct session *, struct message_msg *, GSList *);
int message_send_notify(struct session *, struct message_msg *);
int message_send_error_notify(struct session *, struct message_msg *);
int message_send_empty_notify(struct session *);
int message_send_informational_renew_i(struct session *);
int message_send_informational_r(struct session *, struct message_msg *);

#ifdef __MESSAGE_C
/*
 * Functions for processing timeouts and related data structures. These
 * are internal to message subsystem and are not called from outside...
 */
int message_send_packet(struct network_socket *, struct session *, char *,
		gint, struct netaddr *);
#endif /* __MESSAGE_C */

/*******************************************************************************
 * SUBSYSTEM GLOBAL FUNCTIONS
 ******************************************************************************/
void message_retransmit(struct session *);
int message_parse_quick(struct message_msg *);
gboolean message_check_validity(struct session *, struct message_msg *);
gint message_parse(struct message_msg *, struct session *session);

void message_remove_timeout(struct session *, struct message_msg *);
void message_remove_timeout_by_msgid(struct session *, guint32);
void message_remove_session(struct session *);
gboolean message_retransmit_response(struct session *, guint32);

/*******************************************************************************
 * SUBSYSTEM GLOBAL FUNCTIONS
 ******************************************************************************/
void message_unload(void);
int message_init(GMainContext *, GSList *, GAsyncQueue *);

#endif /* __MESSAGE_H */
