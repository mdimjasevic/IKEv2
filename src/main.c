/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define LOGGERNAME	"main"

#include <execinfo.h>
#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <signal.h>
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <linux/ipsec.h>
#include <getopt.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "netlib.h"
#include "logging.h"
#include "timeout.h"
#include "transforms.h"
#include "proposals.h"
#include "ts.h"
#include "pfkey_msg.h"
#include "sad.h"
#include "spd.h"
#include "config.h"
#include "network.h"
#include "auth.h"
#include "cert.h"
#include "cert_msg.h"
#include "crypto.h"
#include "supplicant.h"
#include "session.h"
#include "message_msg.h"
#include "message.h"
#include "ikev2.h"
#include "sig.h"
#include "aaa_wrapper.h"
#include "cfg.h"
#include "cfg_module.h"
#include "sm.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * Main queue. It is defined as global variable through which
 * signal handling routines can send information about received
 * signals.
 */
static GAsyncQueue *queue;

/*
 * This function prints stack trace. It should actually cause
 * core dump....
 */
void print_trace (void)
{
	void *array[10];
	size_t size;
	char **strings;
	size_t i;

	size = backtrace (array, 10);
	strings = backtrace_symbols (array, size);

	printf ("Obtained %zd stack frames.\n", size);

	for (i = 0; i < size; i++)
		printf ("%s\n", strings[i]);
 
	free (strings);

	exit (1);
}

static void sighndlr(int signum)
{
	struct sig_msg *msg;

	if (signum == SIGSEGV)
		print_trace();

	msg = g_malloc0(sizeof(struct sig_msg));

	msg->msg_type = SIG_MSG;
	msg->signum = signum;

	g_async_queue_push(queue, msg);
}

void print_usage(char *progname)
{
	printf("Usage: %s [options]\n", progname);
	printf("Options are\n");
	printf("\t-4|--ipv4-only    	Listen only on IPv4 sockets\n");
	printf("\t-6|--ipv6-only    	Listen only on IPv6 sockets\n");
	printf("\t-b|--background		Run daemon in background\n");
	printf("\t-c|--config <file>	Configuration file to read\n");
	printf("\t-h|--help		Print this help\n");
	printf("\t-i|--remoteid <id>	Connect to responder with a given ID\n");
	printf("\t-l|--listen [<if>]:[<ip>]\n");
	printf("\t			Listen on specific interface(if) and address(ip)\n");
	printf("\t-n|--natt-port port	NAT-T listen on specific port\n");
	printf("\t-o|--port port		Listen on specific port\n");
	printf("\t-p|--pidfile <file>	File where to write process's pid\n");
	printf("\t-s|--per-socket-policy	Install per socket policy\n");
	printf("\t-t|--list-transforms	List all available transforms\n");

	exit (1);
}

int main(int argc, char **argv)
{
	gboolean ipv4_only = FALSE;
	gboolean ipv6_only = FALSE;

	gint port = IKEV2_PORT;
	gint natt_port = IKEV2_PORT_NATT;

	char ipnum[sizeof(struct in6_addr)];
	struct config_listen *cl;

	GSList *listen_data = NULL;
	gchar *r;

	struct ikev2_data data;
	gboolean per_socket_policy = FALSE;
	gboolean background = FALSE;
	struct id *id = NULL;
	struct connect_msg *connect_msg;

	struct sigaction sa;

	char *config_file = "ikev2.conf";

	char *pid_file = NULL;
	FILE *fpidfile;

	g_thread_init(NULL);

	/*
	 * Parse command line options
	 */
	while (1) {
		int option_index = 0;
		int c;
		static struct option long_options[] = {
			{ "ipv4-only", 0, 0, '4' },
			{ "ipv6-only", 0, 0, '6' },
			{ "background", 0, 0, 'b' },
			{ "config", 1, 0, 'c' },
			{ "help", 0, 0, 'h' },
			{ "remoteid", 1, 0, 'i' },
			{ "listen", 1, 0, 'l' },
			{ "natt-port", 1, 0, 'n' },
			{ "port", 1, 0, 'o' },
			{ "pidfile", 1, 0, 'p' },
			{ "per-socket-policy", 0, 0, 's' },
			{ "list-transforms", 1, 0, 't' },
			{ 0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "46bc:hi:l:n:o:p:st", long_options,
				&option_index);
		if (c == -1)
			break;

		switch (c) {

		case '4':
			/*
			 * Check if we already have -6 option specified? If so,
			 * terminate with an error message.
			 */
			if (ipv6_only) {
				LOG_ERROR("-4 and -6 options are mutually "
						"exclusive!");
				goto out_pid;
			}

			ipv4_only = TRUE;
			break;

		case '6':
			/*
			 * Check if we already have -4 option specified? If so,
			 * terminate with an error message.
			 */
			if (ipv4_only) {
				LOG_ERROR("-4 and -6 options are mutually "
						"exclusive!");
				goto out_pid;
			}

			ipv6_only = TRUE;
			break;

		case 'b':
			background = TRUE;
			break;

		case 'c':
			config_file = optarg;
			break;

		case 'n':
			natt_port = atol(optarg);
			if (natt_port < 0 || natt_port > 65535) {
				LOG_ERROR("Port value is in the range "
						"0 - 65535 inclusive");
				goto out_pid;
			}
			break;

		case 'o':
			port = atol(optarg);
			if (port < 0 || port > 65535) {
				LOG_ERROR("Port value is in the range "
						"0 - 65535 inclusive");
				goto out_pid;
			}
			break;

		case 'p':
			pid_file = optarg;
			break;

		case 'l':

			/*
			 * We have the following cases to parse
			 *	interface:ipv4
			 *	interface:ipv6
			 *	ipv4
			 *	ipv6
			 *	interface
			 */
			cl = config_listen_new();
			cl->port = port;
			cl->natt_port = natt_port;

			if (ipv4_only)
				cl->afamily = AF_INET;

			if (ipv6_only)
				cl->afamily = AF_INET6;

			listen_data = g_slist_append(listen_data, cl);

			if (inet_pton(AF_INET, optarg, ipnum) > 0) {
				cl->addr = netaddr_new_from_inaddr(
						(struct in_addr *)ipnum);
				break;
			}

			if (inet_pton(AF_INET6, optarg, ipnum) > 0) {
				cl->addr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
				break;
			}

			r = strrchr(optarg, ':');

			if (!r) {
				/*
				 * We assume that only an iterface was specified
				 */
				cl->addr = netaddr_new();
				netaddr_set_ifname(cl->addr, optarg);
				break;
			}

			if (inet_pton(AF_INET, r, ipnum) > 0) {
				cl->addr = netaddr_new_from_inaddr(
						(struct in_addr *)ipnum);
				*r = '0';
				netaddr_set_ifname(cl->addr, optarg);
				*r = ':';
				break;
			}

			if (inet_pton(AF_INET6, r, ipnum) > 0) {
				cl->addr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
				*r = '0';
				netaddr_set_ifname(cl->addr, optarg);
				*r = ':';
				break;
			}

			LOG_ERROR("Error parsing argument %s", optarg);
			goto out_pid;

			break;

		case 't':
			transform_list_all();
			exit(1);

		case 's':
			per_socket_policy = TRUE;
			break;

		case 'i':
			if (!(id = id_new_from_str(optarg))) {
				LOG_ERROR("Unsupported ID '%s' in command "
						"line argument", optarg);
				goto out_pid;
			}
			break;

		case 'h':
		case '?':
		default:
			print_usage(argv[0]);
			break;
		}
	}

	memset(&data, 0, sizeof(struct ikev2_data));

	data.shutdown = FALSE;

	g_static_rw_lock_init(&data.config_lock);
	g_static_rw_lock_init(&data.session_lock);

	sa.sa_handler = (void *)&sighndlr;
	sigemptyset(&sa.sa_mask);;
	sa.sa_flags = 0;
	sigaction(SIGTERM, &sa, NULL);
	sigaction(SIGUSR1, &sa, NULL);
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGHUP, &sa, NULL);

	data.mainloop = g_main_new(FALSE);

	queue = g_async_queue_new();

	if (config_init(config_file, &(data.config)) < 0)
		goto out_pid;

	if (logging_init(data.config->logging) < 0)
		goto out_pid;

	/*
	 * Initialize timer subsystem
 	 */
	if (timeout_init() < 0)
		goto out_pid;

	if (pid_file) {
		if ((data.config)->cg->pidfile) {
			LOG_NOTICE("PID filename given in "
					"command line has priority over "
					"configuration file value");
			g_free((data.config)->cg->pidfile);
		}
		(data.config)->cg->pidfile = pid_file;
	} else {
		if ((data.config)->cg->pidfile == NULL)
			(data.config)->cg->pidfile = "ikev2.pid";
	}

	if ((fpidfile = fopen((data.config)->cg->pidfile, "w")) == NULL) {
		LOG_ERROR("Could not write pid file %s\n",
				(data.config)->cg->pidfile);
		goto out;
	}

	fprintf (fpidfile, "%d\n", getpid());
	fclose (fpidfile);

	if (crypto_init(config_general_get_random_device(data.config->cg)) < 0)
		goto out_pid;

	cert_init(queue);

	/*
	 * Initialize networking subsystem
 	 */
	if (network_init(g_main_loop_get_context(data.mainloop)) < 0)
		goto out_pid;

	/**
	 * Initialize connection to SAD
	 */
	if (sad_init(g_main_loop_get_context(data.mainloop), queue) < 0)
		goto out_pid;

	/**
	 * Initialize connection to SPD
	 */
	if (spd_init(g_main_loop_get_context(data.mainloop), queue,
			(data.config)->peer,
			(data.config)->cg->kernel_spd) < 0)
		goto out_pid;

	/**
	 * Initialize message subsystem. Before initializing message
	 * subsystem we prepare necessary data structures that describe
	 * on which interfaces/addresses will ikev2 listen for incoming
	 * requests.
	 */
	if (listen_data) {
		/*
		 * Parameters on the command line were given so they
		 * have priority over any parameters specified in the
		 * configuration file.
		 */
		config_listen_list_free((data.config)->cg->listen_data);
		(data.config)->cg->listen_data = listen_data;

	} else if (!((data.config)->cg->listen_data) ||
			ipv4_only || ipv6_only) {
		/*
		 * We handle two cases here. In both cases no specific
		 * addresses or interfaces were given on the command
		 * line.
		 *
		 * The first case is when the user specified either
		 * -4 or -6 on command line then we ignore configuration
		 * file and bind to any of the given address family.
		 *
		 * The other case is that user didn't specified anything
		 * in command line and in the configuration file!
		 */

		config_listen_list_free((data.config)->cg->listen_data);

		cl = config_listen_new();
		cl->port = port;
		cl->natt_port = natt_port;

		if (ipv6_only)
			cl->afamily = AF_INET6;

		if (ipv4_only)
			cl->afamily = AF_INET;

		(data.config)->cg->listen_data = g_slist_append(NULL, cl);

	}

	if (message_init(g_main_loop_get_context(data.mainloop),
			(data.config)->cg->listen_data, queue) < 0)
		goto out_pid;

#ifdef CFG_MODULE
	if (cfg_init((data.config)->providers, queue) < 0)
		goto out_pid;
#endif /* CFG_MODULE */

#ifdef AAA_CLIENT
	/*
	 * AAA subsystem initialization
	 */
	if (aaa_init(queue, &data) < 0 )
		goto out_pid;
#endif /* AAA_CLIENT */
	
	sm_init(&data, queue);

	if (background) {
		/* First fork: allow shell to return and to do a setsid() */
 		switch(fork()) {
 		case 0:
			/* Child process */
			LOG_NOTICE("Continuing in background");
			break;
		case -1:
			fprintf(stderr, "Unsuccessful #1 forking.");
			exit(1);
			break;
		default:
			/* If we got a child PID, we can exit #1 parent 
			 * process */
 			fprintf (stderr, "Successfuly forked\n");
			/* TODO: exit with out_fork label */
			exit(0);
			break;
		}

		if (setsid() < 0) {
			LOG_ERROR("setsid() did not succeeded.");	
			exit(0);
		}

		/* Second fork: prevents from reacquiring a controlling 
		 * terminal. */
		switch(fork()) {
		case 0:
			/* Child process */
			break;
		case -1:
			perror("fork");
			LOG_ERROR("Unsuccessful #2 forking.");
			exit(1);
 		default:
			LOG_NOTICE("Continuing in background");
			/* If we got a child PID, we can exit #2 parent 
			 * process. */
			/* TODO: exit with out_fork label */
			exit(0);
 		}

		/* Close out the standard file desriptors. */
		close(0);
		close(1);
		close(2);
		
 	}

	if (id) {
		/*
		 * If IKEv2 was requested to initiate the connection to a
		 * remote peer push the message to the STATE machines.
		 */
		connect_msg = g_malloc0(sizeof(struct connect_msg));
		connect_msg->msg_type = CONNECT_MSG;
		connect_msg->id = id;

		g_async_queue_push(queue, connect_msg);
	}

	g_main_loop_run(data.mainloop);

	sm_unload(&data);

#ifdef AAA_CLIENT
	aaa_unload();
#endif /* AAA_CLIENT */

#ifdef CFG_MODULE
	cfg_unload();
#endif /* CFG_MODULE */

	message_unload();

	spd_unload();

	crypto_unload();

	cert_unload();

	g_async_queue_unref(queue);

out_pid:
	/*
	 * We call logging unload in any case since there could be log
	 * records that should be shown to user.
	 */
	logging_unload();

	unlink(pid_file);

out:
	return 0;
}
