/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __LOGGING_H
#define __LOGGING_H

#define LOG_LINE_LENGTH		1024

/*
 * Data for syslog
 */
#define IKEV2_SYSLOG_IDENT	"ikev2"
#define IKEV2_SYSLOG_OPTIONS	(LOG_PID)

#define LOG_F_TIMESTAMP		(1 << 0)
#define LOG_F_LINE		(1 << 1)
#define LOG_F_FUNCTION		(1 << 2)
#define LOG_F_SEVERITY		(1 << 3)
#define LOG_F_SUBSYSTEM		(1 << 4)

#define LOGGING_SUBSYSTEM_ANY	"ALL"

/*******************************************************************************
 * CONFIGURATION STRUCTURES FOR LOGGING SUBSYSTEM
 ******************************************************************************/

/**
 * Configuration log item. One is created per file/syslog/stderr. It
 * contains data common for different subsystems.
 */
typedef struct config_log_item {
	/**
	 * List of subsystems that are beeing loged by data in this
	 * structure.
	 */
	GSList *subsystem;

	/**
	 * Log level. Anything above this value will not be logged
	 * (TODO: Check this!)
	 */
	gint level;

	/**
	 * Flags that determine what will be logged. Possibilities are:
	 *
	 *	LOG_F_TIMESTAMP		Timestamp when event occured
	 *	LOG_F_LINE		Line in which error occured
	 *	LOG_F_FUNCTION		Function in which error occured
	 *	LOG_F_SEVERITY		Severity of error
	 *	LOG_F_SUBSYSTEM		Subsystem in which error occured
	 */
	gint flags;

	/**
	 * If filename == NULL then logging is done via syslog.
	 * For stderr this filed has to contain string "stderr". Otherwise
	 * it is treated as a filename. In both cases this value has to be
	 * free'd before freeing this structure.
	 */
	char *filename;
	FILE *fileio;
} config_log_item_t;

/**
 * Configuration data for logging subsystem
 */
typedef struct config_log {
	/**
	 * Facility for syslog. If no syslog logging is used then this
	 * variable holds value -1.
	 */
	int facility;

	/**
	 * List of config_log_item structures...
	 */
	GSList *log_items;
} config_log_t;

/*******************************************************************************
 * RUNTIME STRUCTURES AND DATA
 ******************************************************************************/

/*
 * Maximum logging level in case messages are dumped to stdout when
 * before logging subsystem is initialized. This shoudn't be to low
 * so that administrator can see what went wrong.
 */
#define LOG_MAX_STDERR		LOG_PRIORITY_ERROR

#ifndef LOGGERNAME
#define LOGGERNAME "root"
#endif /* LOGGERNAME */

/**
 * Predefined Levels of priorities.
 * Copied form /usr/include/log4C/priority.h
 */
typedef enum {
	LOG_PRIORITY_FATAL    = 000,
	LOG_PRIORITY_ALERT    = 100,
	LOG_PRIORITY_CRIT     = 200,
	LOG_PRIORITY_ERROR    = 300,
	LOG_PRIORITY_WARN     = 400,
	LOG_PRIORITY_NOTICE   = 500,
	LOG_PRIORITY_INFO     = 600,
	LOG_PRIORITY_DEBUG    = 700,
	LOG_PRIORITY_TRACE    = 800,
	LOG_PRIORITY_NOTSET   = 900,
	LOG_PRIORITY_UNKNOWN  = 1000
} log_priority_level_t;

/**
 * Structure to temporarily hold logging records until logging is
 * fully initialized.
 */
struct logging_item {
	/**
	 * Buffer that points to a message
	 */
	char *buffer;	

	/**
	 * Subsystem that initiated log
	 */
	char *logger_name;

	/**
	 * Severity level of log item
	 */
	int level;
};

#ifndef DISABLE_LOGGING

#define LOG_BUG(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_DEBUG, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_NOTICE(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_NOTICE, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_ERROR(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_ERROR, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_WARNING(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_WARN, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_DEBUG(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_DEBUG, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_TRACE(format, ...)		\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_TRACE, "%-s:%4d: " format, \
		__FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG(level, format, ...)		\
		ikev2_log(LOGGERNAME, level, "%-s: %d: " format, __FUNCTION__, \
		__LINE__, ##__VA_ARGS__)

#define LOG_PERROR(level)		\
		ikev2_log_perror(LOGGERNAME, level, __FUNCTION__,  __LINE__)

#define LOG_ERRNO(level, errno)		\
		ikev2_log_errno(LOGGERNAME, level, __FUNCTION__,  __LINE__, errno)

#define LOG_FUNC_START(level)			\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_DEBUG + 50 + level,	\
				"Entering %-s:%4d  ", __FUNCTION__, __LINE__)

#define LOG_FUNC_END(level)			\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_DEBUG + 50 + level,	\
				"Leaving %-s:%4d  ", __FUNCTION__, __LINE__)

#else

#define LOG_BUG(format, ...)
#define LOG_NOTICE(format, ...)
#define LOG_WARNING(format, ...)
#define LOG_DEBUG(format, ...)
#define LOG_TRACE(format, ...)

#define LOG(level, format, ...)
#define LOG_PERROR(level)
#define LOG_FUNC_START(level)
#define LOG_FUNC_END(level)

#endif /* DISABLE_LOGGING */

/*******************************************************************************
 * CONFIGURATIONS STRUCTURES MANIPULATION FUNCTIONS
 ******************************************************************************/
config_log_t *config_log_new(void);
void config_log_free(config_log_t *);
void config_log_add_config_log_item(config_log_t *, config_log_item_t *);

config_log_item_t *config_log_item_new(void);
void config_log_item_free(config_log_item_t *);
void config_log_item_add_subsystem(config_log_item_t *, gchar *);

/*******************************************************************************
 * LOGGING FUNCTIONS
 ******************************************************************************/

void ikev2_log_str(char *, int, char *);
void ikev2_log(char *, int, const char *, ...);
void ikev2_log_perror(char *, int, const char *, int);
void ikev2_log_errno(char *, int, const char *, int, int);
void log_data(char *, char *, ssize_t);
void log_buffer(char *, char *, ssize_t);

/*******************************************************************************
 * SUBSYSTEM GLOBAL FUNCTIONS
 ******************************************************************************/

void logging_unload(void);
gint logging_init(struct config_log *);

#endif /* __LOGGING_H */
