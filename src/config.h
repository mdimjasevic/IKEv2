/**************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CONFIG_H
#define __CONFIG_H

#define	NATT_OFF		0
#define	NATT_ON			1
#define	NATT_PASSIVE		2

#define	MOBIKE_OFF		0
#define	MOBIKE_ON		1
#define	MOBIKE_PASSIVE	2

#define RRC_ON			0
#define RRC_OFF			1

#define COOKIE2_ON		0
#define COOKIE2_OFF		1

#define	PFS_OFF			0
#define	PFS_ON			1
#define	PFS_PASSIVE		2

#define CSA_INSTALL		1
#define CSA_ACTIVE		2

/**
 * Constants that define how IKEv2 will function, as responder only,
 * initiator only, or both.
 */
enum {
	IKEV2_MODE_UNSPEC,
	IKEV2_MODE_INITIATOR,
	IKEV2_MODE_RESPONDER
};

/**
 * Constants that define should IKEv2 do sanity check of configuration
 * file and how to behave in case it found errors on inconsitiences.
 */
enum {
	SANITY_CHK_OFF,
	SANITY_CHK_WARNING,
	SANITY_CHK_ERROR
};


/**
 * Constants that define how IKEv2 behaves with respect to in
 * kernel SPD.
 */
enum {
	KERNEL_SPD_UNDEFINED,
	KERNEL_SPD_FLUSH,
	KERNEL_SPD_ROSYNC,
	KERNEL_SPD_SYNC,
	KERNEL_SPD_GENERATE
};

/**
 * Section where option can occur
 */
enum {
	OPTION_SECTION_GENERAL	= 0,
	OPTION_SECTION_REMOTE,
	OPTION_SECTION_PEER
};

/**
 * All the supported options
 */
enum {
	/*
	 * Verify ID used by ikev2 with the ID in the certificate
	 */
	OPTION_NO_ID_VERIFY	= 1,

	/*
	 * Verify ID used by ikev2 with the ID used in the EAP
	 */
	OPTION_VERIFY_EAPID	= 2,

	/*
	 * Do not use HASH_AND_URL
	 */
	OPTION_NOHASHANDURL	= 4,

	/*
	 * Do the sanity check over configuration file
	 */
	OPTION_SANITYCHECK	= 8
};

/**
 * Options supported by the ikev2
 */
struct config_option {
	/**
	 * Option's name in the configuration file
	 */
	char *name;

	/**
	 * Section in which option can be specified. One of the
	 * OPTION_SECTION_GENERAL, OPTION_SECTION_REMOTE, or
	 * OPTION_SECTION_PEER
	 */
	guint16 section;

	/**
	 * Option's exact value to be set in the bit field
	 */
	guint16 value;
};

#ifdef __CONFIG_C
struct config_option config_options[] = {
	{ "noidverify", OPTION_SECTION_PEER, OPTION_NO_ID_VERIFY },
	{ "verifyeapid", OPTION_SECTION_PEER, OPTION_VERIFY_EAPID },
	{ "nohashandurl", OPTION_SECTION_PEER, OPTION_NOHASHANDURL },
	{ "sanitycheck", OPTION_SECTION_GENERAL, OPTION_SANITYCHECK },
	{ NULL, 0, 0 },
};
#else /* __CONFIG_C */
extern struct config_option config_options[];
#endif /* __CONFIG_C */

/**
 * Interface or address data for ikev2 to listen to
 *
 * Care should be taken that family of address stored in addr
 * field is consistent with the value of field afamily!
 */
struct config_listen {

	/**
	 * Address to listen to
	 */
	netaddr_t *addr;

	/**
	 * Port value
	 */
	guint16 port;

	/**
	 * NAT-T port value
	 */
	guint16 natt_port;

	/**
	 * Which protocol family IPv4 (AF_INET), IPv6 (AF_INET6),
	 * or both (AF_UNSPEC).
	 *
	 * Note that in case of IPv6, natt_port value is ignored!
	 */
	gint afamily;
};

/**
 * General configuration section
 *
 * All the parameters bound to IKEv2 protocol should be put here.
 * Note that per-peer and per-SA parameters are placed into separate
 * structure.
 */
struct config_general {

	/**
	 * Number of users that particular instance of this structure
	 * has. The structure should not be free'd until this counter
	 * drops to zero.
	 */
	gint refcnt;

	/**
	 * Random device to initialize crypto system
	 */
	gchar *random_device;

	/**
	 * Number of half opened connections after which we assume
	 * DOS attacke and start to send COOKIE notifications
	 */
	guint32 dos_treshold;

	/**
	 * Number of worker threads in sm pool. This parameter has
	 * to be determined based on expected load.
	 */
	guint16 sm_threads;

	/**
	 * Name and path of a pid file
	 */
	char *pidfile;

	/**
	 * Mode in which IKEv2 daemon functions. It can strictly
	 * behave as initiator, responder, or it can take both
	 * roles.
	 */
	guint8 mode;

	/**
	 * Maximum number of IKE SAs allowed from a single IP address.
	 * This is also form of a DOS attack defence, but NATs create
	 * problems here (actually, NATs create problems everywhere! :)).
	 */
	guint32 ikesa_max;

	/**
	 * Maximum number of half opened IKE SAs from the single IP address
	 */
	guint32 ikesa_max_halfopened;

	/**
	 * Localy stored list of CAs
	 */
	GSList *ca_list;

	/**
	 * Localy stored list of certs
	 */
	GSList *cert_list;

	/**
	 * File with a shared keys...
	 */
	char *psk_file;

	/**
	 * List of shared keys with identifiers.
	 *
	 * @todo Probably this List shoud go somewhere else
	 */
	GSList *psk;

	/**
	 * Parameter that determines how IKEv2 daemon behaves with respect
	 * to in-kernel SPD.
	 */
	guint8 kernel_spd;

	/**
	 * Addresses and interfaces on which ikev2 should listen prior to
	 * start. In case this attribut has NULL value, then ikev2 will
	 * listen on all addresses.
	 */
	GSList *listen_data;

	/**
	 * Options specified in the general section
	 */
	guint32 options;
};

/**
 * Configuration structure for each remote peer that we can connect to.
 * The data in this structure is used only for the IKE SA INIT exchange.
 */
struct config_remote {

	/**
	 * This is for debugging purposes, we mark where REMOTE section
	 * starts and when this REMOTE is selected we write it's line
	 * number...
	 */
	guint32 cfg_line;

	/**
	 * Traffic selectors used to select remote structure.
	 */
	GSList *ts_list;

	/**
	 * Number of users that particular instance of this structure
	 * has. The structure should not be free'd until this counter
	 * drops to zero.
	 */
	gint refcnt;

	/*
	 * List of acceptable/offered proposals from/to a peer....
	 */
	GSList *proposals;

	/**
	 * Size of a nonce data to send to peer...
	 *
	 * According to IKEv2 RFC this value is between X and Y, inclusive
	 */
	guint16 nonce_size;

	/**
	 * How long should IKEv2 wait for response from a peer. This
	 * value is given in milliseconds.
	 */
	guint32 response_timeout;

	/**
	 * How many times should we try to contant peer before declaring
	 * declaring it unreachable.
	 */
	guint16 response_retries;

#ifdef NATT
	/**
	 * Should we include NATT support? The posslible values are NATT_ON,
	 * NATT_OFF, and NATT_PASSIVE. If it's NATT_ON, and we are initiator,
	 * then by default we try to detect NAT.
	 */
	guint8 natt;

	/**
	 * How frequently we send NATT keepalives. This field is only used
	 * if we are behind a NAT box.
	 */
	guint32 natt_keepalive;

	/**
	 * NATT port to use instead of 4500 as prescribed by IKEv2 RFC.
	 */
	guint16 natt_port;
#endif /* NATT */

	/**
	 * This field is used by initiator to contact responder if it's not
	 * on IKE standard port. Zero means default port.
	 */
	guint16 remote_port;

	/**
	 * ID that Initiator will use to determine which responder to
	 * contact and with what parameters.
	 */
	struct id *rid;

	/**
	 * Options specified in the remote section
	 */
	guint32 options;

	/**
	 * List of proposals that we should use for particular peer.
	 */
#warning "Analyze this field and the proposals field above"
	GSList *remote_proposals;
	/*
	 * FIXME
	 * MOBIKE support structure part
	 */
#ifdef MOBIKE
	/**
	 * Should we include MOBIKE support? The posslible values are MOBIKE_ON,
	 * MOBIKE_OFF, and MOBIKE_PASSIVE. If it's MOBIKE_ON, and we are initiator,
	 * then by default we announce MOBIKE support in IKE AUTH exchange.
	 */
	guint8 mobike;
	/**
	 * Is return routability check required? By default, it is. Possible values
	 * are RRC_ON and RRC_OFF. So default is RRC_ON.
	 */
	guint8 rrc;
	/**
	 * Is COOKIE2 payload required? By default, it is. Possible values
	 * are COOKIE2_ON and COOKIE2_OFF. So default is COOKIE2_ON.
	 */
	guint8 cookie2;
	/**
	 * How long should IKEv2 wait for return routability check. This
	 * value is given in milliseconds.
	 */
	guint32 rrc_timeout;
#endif /* MOBIKE */
};

/**
 * Configuration structure valid for a certain peer
 */
struct config_peer {

	/**
	 * Number of users that particular instance of this structure
	 * has. The structure should not be free'd until this counter
	 * drops to zero.
	 */
	gint refcnt;

	/**
	 * Line in configuration file where peer section is defined
	 */
	guint16 cfg_line;

	/**
	 * Time, in seconds, after which CSA is considered to be in
	 * half closed state and forcefully removed...
	 */
	guint32 sa_halfclosed_wait;

	/**
	 * List of IDs expected from remote peer
	 *
	 * If this field is NULL that it's a special ANY section that
	 * catches any ID!
	 */
	GSList *rids;

	/**
	 * Identification used by this peer.
	 */
	struct id *lid;

	/**
	 * How do we authenticate to a peer
	 *
	 * Supported authentication methods are stored in octets
	 * of 32-bit words, i.e. we support up to 4 authentication
	 * methods. First method is stored in lowes significant
	 * octet ([0]). If the value of some octet is 0, it means
	 * NULL value. Currenty, we take only first authentication
	 * method.
	 */
	guint32 auth_method;

	/**
	 * What authentication methods do we expect/support from
	 * the peer.
	 *
	 * Supported authentication methods are stored in octets
	 * of 32-bit words, i.e. we support up to 4 authentication
	 * methods. First method is stored in lowes significant
	 * octet ([0]). If the value of some octet is 0, it means
	 * NULL value.
	 */
	guint32 peer_auth_method;

	/**
	 * PSK file with secrets
	 */
	char *psk_file;

#ifdef SUPPLICANT
	/**
	 * wpa_supplicant configuration file to authenticate this peer
	 */
	char *wpa_conf;
#endif /* SUPPLICANT */

#ifdef RADIUS_CLIENT
	/**
	 * Radius server's ID that will be used for authenticating this
	 * peer.
	 */
	char *radius_server;
#endif /* RADIUS_CLIENT */

#ifdef CFG_MODULE
	/**
	 * Subnets protected by the VPN gateway. List of netaddr structures
	 */
	GSList *subnets;

	/**
	 * ID list of providers to use by this peer
	 */
	GSList *providers;

	/**
	 * Script to be run when responder gives a new set of
	 * configuration parameters. Parameters given to the
	 * initiator will be placed in the environment
	 */
	gint gw_up_argc;
	gchar **gw_up_argv;

	/**
	 * Script to be run when removing IKE SA.
	 */
	gint gw_down_argc;
	gchar **gw_down_argv;
#endif /* CFG_MODULE */

#ifdef CFG_CLIENT
	/**
	 * Parameters that should be retrieved from responder. If this
	 * field is zero then no parameters should be requested.
	 */
	guint32 flags;

	/**
	 * Script to be run when initiator receives a new IP address
	 * and the parameters to pass to the script. Parameters obtained
	 * from the responder will be placed in the environment
	 */
	gint up_argc;
	gchar **up_argv;

	/**
	 * Script to be run when removing IKE SA.
	 */
	gint down_argc;
	gchar **down_argv;
#endif /* CFG_CLIENT */

	/*
	 * Received peer's certificates will be validated against this 
	 * CA certificates.
	 */
	GSList *ca_list;

	/*
	 * Locally stored peer's certificates. 
	 */
	GSList *cert_list;

	/**
	 * Options specified in the remote section
	 */
	guint32 options;

	/**
	 * Maximum idle time for session before dead peer detection process
	 * is started...
	 * The value is given in the seconds
	 */
	guint32 ike_max_idle;

	/**
	 * Maximum time this IKE SA can be extended without reautentification
	 */
	struct limits *auth_limits;

	/**
	 * In case the proposals have no specific limits than these are
	 * applied as defaults...
	 */
	struct limits *rekey_limits;

	/**
	 * List of config SA structures
	 */
	GSList *sainfo;
	/*
	 * FIXME
	 * MOBIKE support structure part
	 */
#ifdef MOBIKE
	/**
	 * Should we include MOBIKE support? The posslible values are MOBIKE_ON,
	 * MOBIKE_OFF, and MOBIKE_PASSIVE. If it's MOBIKE_ON, and we are initiator,
	 * then by default we announce MOBIKE support in IKE AUTH exchange.
	 */
	guint8 mobike;
	/**
	 * Is return routability check required? By default, it is. Possible values
	 * are RRC_ON and RRC_OFF. So default is RRC_ON.
	 */
	guint8 rrc;
	/**
	 * Is COOKIE2 payload required? By default, it is. Possible values
	 * are COOKIE2_ON and COOKIE2_OFF. So default is COOKIE2_ON.
	 */
	guint8 cookie2;
	/**
	 * How long should IKEv2 wait for return routability check. This
	 * value is given in milliseconds.
	 */
	guint32 rrc_timeout;
#endif /* MOBIKE */
};

/**
 * Configuration structure that holds every single configuration parameter
 * for the IKEv2 daemon. This structure is filled in parser.y.
 */
struct config {
	/*
	 * Name of a file with configuration
	 */
	char *config_file;

	/**
	 * General configuration data for IKEv2 daemon
	 */
	struct config_general *cg;

#ifdef RADIUS_CLIENT
	/**
	 * List of radius servers and their backups
	 */
	GSList *radius_servers;
#endif /* RADIUS_CLIENT */

	/**
	 * List of providers for dynamic data
	 */
	GSList *providers;

	/**
	 * Configuration data for cfg providers (e.g. DHCP)
	 */
	struct config_cfg *cc;

	/**
	 * List of config_remote structures with data specific for
	 * each peer during IKE SA INIT phase. This list is
	 * searched by IP network address...
	 */
	GSList *remote;

	/**
	 * List of config_peer structures with data specific for
	 * each peer during IKE SA AUTH phase. This list is
	 * searched by ID...
	 */
	GSList *peer;

	/*
	 * List of config_csa structures with data specific for
	 * each peer during CREATE_CHILD_SA phase. This list is
	 * searched by IP network address...
	 */
	GSList *csa;

	/*
	 * Directory with certificates. 
	 */
	char *cert_path;

	/**
	 * Logging directives
	 */
	config_log_t *logging;
};

/*******************************************************************************
 * CONFIG_LISTEN structure functions
 ******************************************************************************/
struct config_listen *config_listen_new(void);

/*******************************************************************************
 * CONFIG_GENERAL structure functions
 ******************************************************************************/
struct config_general *config_general_new(void);
void config_general_free(struct config_general *);
void config_general_set_random_device(struct config_general *, char *);
const char *config_general_get_random_device(struct config_general *);
void config_general_set_sm_threads(struct config_general *, int);
int config_general_get_sm_threads(struct config_general *);
void config_general_dump(struct config_general *);
void config_general_add_ca(struct config_general *, struct ca_item *);

/*******************************************************************************
 * CONFIG_REMOTE structure functions
 ******************************************************************************/
struct config_remote *config_remote_new();
void config_remote_free(struct config_remote *);
void config_remote_add_proposal(struct config_remote *, struct proposal *);
void config_remote_dump(struct config_remote *);

struct config_remote *config_remote_find_by_addr(struct config *, netaddr_t *);
struct config_remote *config_remote_find_by_id(struct config *, struct id *);

/*******************************************************************************
 * CONFIG_PEER structure functions
 ******************************************************************************/
struct config_peer *config_peer_new();
void config_peer_free(struct config_peer *);
void config_peer_add_sainfo(struct config_peer *, struct config_csa *);
void config_peer_dump(struct config_peer *);
struct config_peer *config_peer_find_by_id(GSList *, struct id *);
struct config_peer *config_peer_find_by_ccsa(GSList *, struct config_csa *);

/*******************************************************************************
 * CONFIG structure functions
 ******************************************************************************/
struct config *config_new(void);
void config_free(struct config *);
void config_add_radius(struct config *, struct radius_config *);
#ifdef CFG_MODULE
void config_add_provider(struct config *, struct cfg_config_provider *);
#endif /* CFG_MODULE */
void config_add_remote(struct config *, struct config_remote *);
void config_add_peer(struct config *, struct config_peer *);
void config_dump(struct config *);

struct radius_config *radius_config_find_by_id (GSList *, char *);

struct config *config_read_cfg_file(const char *);
struct config *config_reread_cfg_file(struct config *);

int conf_fgets(char *, int);

int config_init(const char *, struct config **);

#endif /* __CONFIG_H */
