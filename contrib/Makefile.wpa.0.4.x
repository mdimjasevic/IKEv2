# Name of final library
LIBNAME=libsupplicant.so

# Major and minor versions of library
MJVER=1
MINVER=0

# CPP, C and linker flags
CPPFLAGS=-I../hostapd
CFLAGS=-ggdb -fPIC -c -Wall
LDFLAGS=-ggdb -shared -Wl,-soname,$(LIBNAME).$(MJVER)

# Destination directories for installation
DSTLIBDIR=/usr/local/lib
DSTINCDIR=/usr/local/include/wpa

# Include files to install
INCLUDES=common.h eap.h crypto.h wpa_supplicant.h defs.h eap_defs.h config.h config_ssid.h

# Object files to put into library
OBJS=md5.o eap.o crypto.o eap_tls.o eap_tls_common.o tls_openssl.o config.o config_file.o base64.o eap_tlv.o sha1.o rc4.o aes_wrap.o

# Defines

# If I'm taking generic EAP, why should I define this?
CPPFLAGS += -DIEEE8021X_EAPOL

# Also, why would use define
CPPFLAGS += -DEAP_TLS_FUNCS

# Also, why would use define
# This was added here so that macros wpa_msg at al. are properly
# defined!
#CPPFLAGS += -DCONFIG_NO_STDOUT_DEBUG

# EAP Methods that should be included in libraries...
CPPFLAGS += -DEAP_TLS

CPPFLAGS += -DEAP_TTLS
OBJS += eap_ttls.o ms_funcs.o

CPPFLAGS += -DEAP_PEAP
OBJS += eap_peap.o

CPPFLAGS += -DEAP_PSK
OBJS += eap_psk.o eap_psk_common.o

CPPFLAGS += -DEAP_SIM
OBJS += eap_sim.o eap_sim_common.o

CPPFLAGS += -DEAP_MD5
OBJS += eap_md5.o

# Targets

all: $(LIBNAME)

$(LIBNAME): $(OBJS)
	gcc -o $(LIBNAME).$(MJVER).$(MINVER) $(OBJS) $(LDFLAGS)

%.o:	%.c
	gcc $(CPPFLAGS) $(CFLAGS) -o $*.o $*.c

install:
	mkdir -p $(DSTINCDIR)
	cp $(LIBNAME).$(MJVER).$(MINVER) $(DSTLIBDIR)/
	cp $(INCLUDES) $(DSTINCDIR)
	( cd $(DSTLIBDIR) && rm -f $(LIBNAME).$(MJVER) && ln -s $(LIBNAME).$(MJVER).$(MINVER) $(LIBNAME).$(MJVER) )
	( cd $(DSTLIBDIR) && rm -f $(LIBNAME) && ln -s $(LIBNAME).$(MJVER).$(MINVER) $(LIBNAME) )

clean:
	rm -f $(OBJS) $(LIBNAME).$(MJVER).$(MINVER)
