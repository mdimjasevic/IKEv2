/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef SUPPLICANT

#define __SUPPLICANT_C

#define LOGGERNAME	"supplicant"

#include <stdio.h>
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <ctype.h>

#include <wpa/common.h>
#include <wpa/config.h>
#include <wpa/eap.h>

#include <glib.h>

#include "logging.h"
#include "transforms.h"
#include "crypto.h"
#include "supplicant.h"

/***************************************************************************
 * Functions necessary to integrate wpa_supplicant
 ***************************************************************************/

gint wpalog2ikelog(gint level)
{
	switch (level) {
	case MSG_MSGDUMP:
		return LOG_PRIORITY_TRACE;
	case MSG_DEBUG:
		return LOG_PRIORITY_DEBUG;
	case MSG_INFO:
		return LOG_PRIORITY_INFO;
	case MSG_WARNING:
		return LOG_PRIORITY_WARN;
	default:
		return LOG_PRIORITY_ERROR;
	}
}

/**
 * wpa_printf - conditional printf
 *
 * @level: priority level (MSG_*) of the message
 * @fmt: printf format string, followed by optional arguments
 *
 * This function is used to print conditional debugging and error messages. The
 * output may be directed to stdout, stderr, and/or syslog based on
 * configuration.
 *
 * Note: New line '\n' is added to the end of the text when printing to stdout.
 *
 * In IKEv2 this function is only relay to ikev2_log function...
 */
void wpa_printf(int level, char *fmt, ...)
{
	va_list ap;
	char buffer[LOG_LINE_LENGTH];
	gint log_level;

	va_start(ap, fmt);
	vsnprintf (buffer, LOG_LINE_LENGTH, fmt, ap);
	va_end(ap);

	log_level = wpalog2ikelog(level);

	ikev2_log_str(LOGGERNAME, log_level, buffer);
}

static void _wpa_hexdump(int level, const char *title, const u8 *buf,
                         size_t len, int show)
{
	gint log_level;

	log_level = wpalog2ikelog(level);

        LOG_DEBUG("%s - hexdump(len=%lu):", title, (unsigned long) len);

	log_buffer(LOGGERNAME, (gchar *)buf, len);
}

void wpa_msg(void *wpa_s, int level, char *fmt, ...)
{
	va_list ap;
	char buffer[LOG_LINE_LENGTH];
	gint log_level;

	va_start(ap, fmt);
	vsnprintf (buffer, LOG_LINE_LENGTH, fmt, ap);
	va_end(ap);

	switch (level) {
	case MSG_MSGDUMP:
		log_level = LOG_PRIORITY_TRACE;
		break;
	case MSG_DEBUG:
		log_level = LOG_PRIORITY_DEBUG;
		break;
	case MSG_INFO:
		log_level = LOG_PRIORITY_INFO;
		break;
	case MSG_WARNING:
		log_level = LOG_PRIORITY_WARN;
		break;
	default:
		log_level = LOG_PRIORITY_ERROR;
		break;
	}

	ikev2_log_str(LOGGERNAME, log_level, buffer);
}

/**
 * Convert SSID into text representation
 *
 * Note: Since we do not use SSID this function always returns NULL
 * string.
 */
const char * wpa_ssid_txt(u8 *ssid, size_t ssid_len)
{
	static char buf = 0;

	return &buf;
}

int hostapd_get_rand(u8 *buf, size_t len)
{
	if (!rand_bytes(buf, len))
		return -1;

	return 0;
}

/**
 * hwaddr_aton - Convert ASCII string to MAC address
 * @txt: MAC address as a string (e.g., "00:11:22:33:44:55")
 * @addr: Buffer for the MAC address (ETH_ALEN = 6 bytes)
 * Returns: 0 on success, -1 on failure (e.g., string not a MAC address)
 */
int hwaddr_aton(const char *txt, u8 *addr)
{
	LOG_BUG("Called function without implementation");

	return -1;
}

/******************************************************************************
 * Functions taken from wpa_supplicant 0.4.10 unmodified
 ******************************************************************************/

/**
 * inc_byte_array - Increment arbitrary length byte array by one
 * @counter: Pointer to byte array
 * @len: Length of the counter in bytes
 *
 * This function increments the last byte of the counter by one and continues
 * rolling over to more significant bytes if the byte was incremented from
 * 0xff to 0x00.
 */
void inc_byte_array(u8 *counter, size_t len)
{
	int pos = len - 1;
	while (pos >= 0) {
		counter[pos]++;
		if (counter[pos] != 0)
			break;
		pos--;
	}
}

static int hex2num(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

static int hex2byte(const char *hex)
{
	int a, b;
	a = hex2num(*hex++);
	if (a < 0)
		return -1;
	b = hex2num(*hex++);
	if (b < 0)
		return -1;
	return (a << 4) | b;
}

/**
 * hexstr2bin - Convert ASCII hex string into binary data
 * @hex: ASCII hex string (e.g., "01ab")
 * @buf: Buffer for the binary data
 * @len: Length of the text to convert in bytes (of buf); hex will be double
 * this size
 * Returns: 0 on success, -1 on failure (invalid hex string)
 */
int hexstr2bin(const char *hex, u8 *buf, size_t len)
{
	int i, a;
	const char *ipos = hex;
	u8 *opos = buf;

	for (i = 0; i < len; i++) {
		a = hex2byte(ipos);
		if (a < 0)
			return -1;
		*opos++ = a;
		ipos += 2;
	}
	return 0;
}

void wpa_hexdump(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump(level, title, buf, len, 1);
}


void wpa_hexdump_key(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump(level, title, buf, len, FALSE);
}

static void _wpa_hexdump_ascii(int level, const char *title, const u8 *buf,
                               size_t len, int show)
{
	int i, llen;
	const u8 *pos = buf;
	const int line_len = 16;
	char *string;

	if (!show) {
		LOG_DEBUG("%s - hexdump_ascii(len=%lu): [REMOVED]",
			title, (unsigned long) len);
		return;
	}
	if (buf == NULL) {
		LOG_DEBUG("%s - hexdump_ascii(len=%lu): [NULL]",
			title, (unsigned long) len);
		return;
	}
	LOG_DEBUG("%s - hexdump_ascii(len=%lu):", title, (unsigned long) len);

	string = g_malloc(len+1);
	for (i = 0; i < len; i++) {
		if (isprint(buf[i]))
			string[i] = buf[i];
		else
			string[i] = '_';
	}
	string[len] = '\0';

	LOG_DEBUG("%s", string);
	g_free(string);
	log_buffer(LOGGERNAME, buf, (unsigned long) len);
}

void wpa_hexdump_ascii(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump_ascii(level, title, buf, len, 1);
}


void wpa_hexdump_ascii_key(int level, const char *title, const u8 *buf,
		size_t len)
{
	_wpa_hexdump_ascii(level, title, buf, len, FALSE);
}

/***************************************************************************
 * CALLBACKS for necessary to implement EAP state machine
 ***************************************************************************/

/*
 * Callback functions for supplicant
 */
struct wpa_ssid * get_config(void *ctx)
{
	struct supplicant_data *sd = ctx;

	LOG_FUNC_START(1);

	return sd->wpa_conf->ssid;
}

Boolean get_bool(void *ctx, enum eapol_bool_var variable)
{
	struct supplicant_data *sd = ctx;

	/*
	 * Check if ctx a.k.a. sd is NULL!
	 */
	switch (variable) {
	case EAPOL_eapSuccess:
		LOG_TRACE("called get_bool for variable EAPOL_eapSuccess (%u)",
				sd->EAPOL_eapSuccess);
		return sd->EAPOL_eapSuccess;

	case EAPOL_eapRestart:
		LOG_TRACE("called get_bool for variable EAPOL_eapRestart (%u)",
				sd->EAPOL_eapRestart);
		return sd->EAPOL_eapRestart;

	case EAPOL_eapFail:
		LOG_TRACE("called get_bool for variable EAPOL_eapFail (%u)",
				sd->EAPOL_eapFail);
		return sd->EAPOL_eapFail;

	case EAPOL_eapResp:
		LOG_TRACE("called get_bool for variable EAPOL_eapResp (%u)",
				sd->EAPOL_eapResp);
		return sd->EAPOL_eapResp;

	case EAPOL_eapNoResp:
		LOG_TRACE("called get_bool for variable EAPOL_eapNoResp (%u)",
				sd->EAPOL_eapNoResp);
                return sd->EAPOL_eapNoResp;

	case EAPOL_eapReq:
		LOG_TRACE("called get_bool for variable EAPOL_eapReq (%u)",
				sd->EAPOL_eapReq);
		return sd->EAPOL_eapReq;

	case EAPOL_portEnabled:
		LOG_TRACE("called get_bool for variable EAPOL_portEnabled (%u)",
				sd->EAPOL_portEnabled);
                return sd->EAPOL_portEnabled;

	case EAPOL_altAccept:
		LOG_TRACE("called get_bool for variable EAPOL_altAccept (%u)",
				sd->EAPOL_altAccept);
		return sd->EAPOL_altAccept;

	case EAPOL_altReject:
		LOG_TRACE("called get_bool for variable EAPOL_altReject (%u)",
				sd->EAPOL_altReject);
		return sd->EAPOL_altReject;

	default:
		LOG_ERROR("EAP state machines requested unkown variable (%u)",
				variable);
		return 0;
	}
}

void set_bool(void *ctx, enum eapol_bool_var variable, Boolean value)
{
	struct supplicant_data *sd = ctx;

	/*
	 * Check if ctx a.k.a. sd is NULL!
	 */

        switch (variable) {
        case EAPOL_eapSuccess:
		LOG_TRACE("called set_bool for variable EAPOL_eapSuccess (%u)",
				value);
                sd->EAPOL_eapSuccess = value;
                break;

        case EAPOL_eapRestart:
		LOG_TRACE("called set_bool for variable EAPOL_eapRestart (%u)",
				value);
		sd->EAPOL_eapRestart = value;
		break;

	case EAPOL_eapFail:
		LOG_TRACE("called set_bool for variable EAPOL_eapFail (%u)",
				value);
		sd->EAPOL_eapFail = value;
		break;

	case EAPOL_eapResp:
		LOG_TRACE("called set_bool for variable EAPOL_eapResp (%u)",
				value);
		sd->EAPOL_eapResp = value;
		break;

	case EAPOL_eapNoResp:
		LOG_TRACE("called set_bool for variable EAPOL_eapNoResp (%u)",
				value);
		sd->EAPOL_eapNoResp = value;
		break;

	case EAPOL_eapReq:
		LOG_TRACE("called set_bool for variable EAPOL_eapReq (%u)",
				value);
		sd->EAPOL_eapReq = value;
		break;

	case EAPOL_portEnabled:
		LOG_TRACE("called set_bool for variable EAPOL_portEnabled (%u)",
				value);
		sd->EAPOL_portEnabled = value;
		break;

	case EAPOL_altAccept:
		LOG_TRACE("called set_bool for variable EAPOL_altAccept (%u)",
				value);
		sd->EAPOL_altAccept = value;
		break;

	case EAPOL_altReject:
		LOG_TRACE("called set_bool for variable EAPOL_altReject (%u)",
				value);
		sd->EAPOL_altReject = value;
		break;
	default:
		LOG_ERROR ("Unknown variable");
		break;
        }
}

unsigned int get_int(void *ctx, enum eapol_int_var variable)
{
	struct supplicant_data *sd = ctx;

	switch (variable) {
	case EAPOL_idleWhile:
		LOG_TRACE("called get_int for variable EAPOL_idleWhile (%u)",
				sd->EAPOL_idleWhile);
		return sd->EAPOL_idleWhile;

	default:
		LOG_ERROR ("Unknown variable");
		return 0;
	}
}

void set_int(void *ctx, enum eapol_int_var variable, unsigned int value)
{
	struct supplicant_data *sd = ctx;

	switch (variable) {
	case EAPOL_idleWhile:
		LOG_TRACE("called set_int for variable EAPOL_idleWhile (%u)",
				value);
		sd->EAPOL_idleWhile = value;
		break;

	default:
		LOG_ERROR ("Unknown variable");
		break;
	}
}

void set_config_blob(void *ctx, struct wpa_config_blob *blob)
{
	struct supplicant_data *sd = ctx;

	LOG_FUNC_START(1);

	if (sd)
		sd->blob = blob;

	LOG_FUNC_END(1);
}

const struct wpa_config_blob *get_config_blob(void *ctx, const char *name)
{
	struct supplicant_data *sd = ctx;

	LOG_FUNC_START(1);

	if (sd)
		return sd->blob;

	LOG_FUNC_END(1);

	return NULL;
}

static u8 *get_eapReqData(void *ctx, size_t *len)
{
	struct supplicant_data *sd = ctx;

	LOG_FUNC_START(1);

	*len = sd->eapData_len;

	LOG_FUNC_END(1);

	return sd->eapData;
}

void set_eapReqData(struct supplicant_data *sd, void *eapReqData, size_t len)
{
	LOG_FUNC_START(1);

	if (sd) {
		sd->eapData = eapReqData;
		sd->eapData_len = len;
	}

	LOG_FUNC_END(1);
}

static struct eapol_callbacks eap_cb =
{
	get_config,
	get_bool,
	set_bool,
	get_int,
	set_int,
	get_eapReqData, // eapol_sm_get_eapReqData,
	set_config_blob,
	NULL, // eapol_sm_get_config_blob,
};

/***************************************************************************
 * Functions to be called by IKEv2
 ***************************************************************************/

/**
 * Get response EAP state machines
 *
 * @param sd
 *
 * \return	TRUE if everything was OK, FALSE in case of an error
 */
gboolean supplicant_get_response(struct supplicant_data *sd)
{
	gboolean ret;

	LOG_FUNC_START(1);

	ret = FALSE;

	if (!get_bool(sd, EAPOL_eapResp)) {
                LOG_ERROR("No response from supplicant!");
                goto out;
        }

	sd->eapData = eap_get_eapRespData(sd->eap_sm, &sd->eapData_len);

	if (!sd->eapData) {
		LOG_ERROR("Received error response. No EAP packet");
		goto out;
	}

	set_bool(sd, EAPOL_eapResp, FALSE);

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Pass EAP request received via IKEv2 message to EAP state machines
 *
 * @param sd
 * @param eap_req	Pointer to received EAP request
 * @param size		Size of received EAP request
 *
 * \return
 */
gint supplicant_process_request(struct supplicant_data *sd, void *eap_req,
		gint size)
{
	LOG_FUNC_START(1);

	set_eapReqData(sd, eap_req, size);
	set_bool(sd, EAPOL_eapReq, TRUE);
	eap_sm_step(sd->eap_sm);

#warning "Error check from EAP state machines should be done?"
	LOG_FUNC_END(1);
	return 0;
}

void *supplicant_get_eap_data (struct supplicant_data *sd)
{
	return sd->eapData;
}

size_t supplicant_get_eap_data_len (struct supplicant_data *sd)
{
	return sd->eapData_len;
}

/**
 * Get MSK value generated during EAP authentication
 *
 * @param sd
 * @param msk
 * @param msk_len
 *
 * \return TRUE if there is MSK, otherwise FALSE
 */
gboolean supplicant_msk_get(struct supplicant_data *sd, char **msk,
		guint *msk_len)
{
	if (eap_key_available(sd->eap_sm)) {
		*msk = (gchar *)eap_get_eapKeyData(sd->eap_sm, (size_t *)msk_len);	
		return TRUE;
	}

	return FALSE;
}


/**
 * Create new supplicant state machine
 *
 * @param conf_file	Configuration file for EAP state machines
 *
 * \return Handle to EAP state machines, or NULL in case of an error
 */
struct supplicant_data *supplicant_new(const char *conf_file)
{
	struct supplicant_data *sd;
	struct eap_config eap_conf;

	LOG_FUNC_START(1);

	sd = g_malloc0(sizeof(struct supplicant_data));

	if ((sd->wpa_conf = wpa_config_read(conf_file)) == NULL) {
		g_free(sd);
		sd = NULL;
		LOG_ERROR("Error reading configuration file!");
		goto out;
	}

	if ((sd->eap_sm = eap_sm_init(sd, &eap_cb, NULL, &eap_conf)) == NULL) {
		LOG_ERROR("Error reading configuration file!");
		wpa_config_free(sd->wpa_conf);
		g_free(sd);
		sd = NULL;
	}

	/*
	 * According to eap.c comment, if we want to activate EAP state
	 * machine, we have to enable port!
	 *
	 * Should this be here, or, should there be separate function
	 * for enabling EAP? It actually depends on needs...
	 */
	set_bool(sd, EAPOL_portEnabled, TRUE);

out:
	LOG_FUNC_END(1);

	return sd;
}

/**
 * Free a supplicant state machine
 *
 * @param sd	Supplicant state machine to free
 */
void supplicant_free(struct supplicant_data *sd)
{
	LOG_FUNC_START(1);

	if (sd->eap_sm)
		eap_sm_deinit(sd->eap_sm);

	if (sd->wpa_conf)
		wpa_config_free(sd->wpa_conf);

	if (sd->blob)
		g_assert_not_reached();

//	if (sd->eapData && sd->eapData_len)
//		g_free(sd->eapData);

	g_free(sd);

	LOG_FUNC_END(1);
}

/**
 * Checks response from supplicant
 *
 * @param sd
 * 
 * \return If response is success then TRUE, otherwise, return NULL
 */
gboolean supplicant_is_success(struct supplicant_data *sd)
{
	return get_bool(sd, EAPOL_eapSuccess);
}

/**
 *Checks response from supplicant
 *
 * @param sd
 *
 * \return If response is failure then TRUE, otherwise, return NULL
 */
gboolean supplicant_is_failure(struct supplicant_data *sd)
{
	return get_bool(sd, EAPOL_eapFail);
}

/***************************************************************************
 * SUBSYSTEM INITIALIZATION AND DEINITIALIZATION FUNCTIONS
 ***************************************************************************/

/**
 * Deinitialize supplicant code
 */
void supplicant_uninit(void)
{
}

/**
 * Initialize supplicant code
 *
 * \return 0 if initialization was successful, -1 otherwise
 */
int supplicant_init(void)
{
	int ret = 0;

	LOG_FUNC_START(1);

#if 0
	if ((ret = eap_peer_register_methods())) {
		wpa_printf(MSG_ERROR, "Failed to register EAP methods");
		if (ret == -2)
			wpa_printf(MSG_ERROR, "Two or more EAP methods used "
				"the same EAP type.");

	}
#endif

	LOG_FUNC_END(1);

	return ret;
}

#endif /* SUPPLICANT */
