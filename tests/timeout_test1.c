/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing timeout module
 */

#define __TIMEOUT_TEST_C

#define LOGGERNAME	"timeout"

#include <stdio.h>
#include <glib.h>

#include "timeout.h"

gpointer wait_thread(gpointer _queue)
{
        GTimeVal t1, t2, t;
	GAsyncQueue *queue = _queue;
	GMainLoop *ml;

	/*
	 * Wait for a message...
	 */
	printf ("Waiting on a queue\n");
	g_get_current_time(&t1);
	ml = g_async_queue_pop(queue);
	g_get_current_time(&t2);

	t.tv_usec = t2.tv_usec - t1.tv_usec;
	t.tv_sec = t2.tv_sec - t1.tv_sec;
	if (t.tv_usec < 0) {
		t.tv_usec += 1000000;
		t.tv_sec--;
	}

	printf ("Waited for %u secs %u usecs\n",
			(guint)t.tv_sec, (guint)t.tv_usec);

	/*
	 * Terminate main loop...
	 */
	printf ("Terminating main thread.\n");
	g_main_loop_quit(ml);

	return NULL;
}

int main(int argc, char **argv)
{
	GMainLoop *ml;
	GTimeVal mainrun;
	GAsyncQueue *queue;
	GThread *wt;

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to timeout module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Queue on which we'll receive message
	 */
	queue = g_async_queue_new();

	/*
	 * Start thread that will wait for a message
	 */
	wt = g_thread_create(wait_thread, queue, FALSE, NULL);

	printf ("Initializing timeout module\n");
	timeout_init();

	/*
	 * This test will run for 3 seconds and then, we'll terminate
	 * main loop...
	 */
	printf ("Registering timeout in 3 seconds\n");
	mainrun.tv_sec = 3;
	mainrun.tv_usec = 0;
	timeout_register_queue(&mainrun, 0, ml, queue);

	/*
	 * Start main loop...
	 */
	printf ("Starting main loop\n");
	g_main_loop_run(ml);

	/*
	 * Unload timeout module
	 */
	printf ("Unloading timeout module\n");
	timeout_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	printf ("Exiting\n");
	return 0;
}
