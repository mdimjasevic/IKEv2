/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __TIMEOUT_C

#define LOGGERNAME	"timeout"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#include <glib.h>

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#include "logging.h"
#include "timeout.h"

/*
 * Data specific to this subsystem
 */
struct timeout_data *timeout_data;

/*******************************************************************************
 * INTERNAL HELPER FUNCTIONS
 ******************************************************************************/

gint timeout_compare_timeouts(gconstpointer a, gconstpointer b)
{
	const struct timeout_item *ti1 = a;
	const struct timeout_item *ti2 = b;

	if (ti1->expire.tv_sec == ti2->expire.tv_sec)
		return ti1->expire.tv_usec - ti2->expire.tv_usec;

	return ti1->expire.tv_sec - ti2->expire.tv_sec;
}

/**
 * For a given timeout_item calculate next occurrence and put it
 * into expire filed.
 */
void timeout_calc_next_occurence(struct timeout_item *ti)
{
	/*
	 * Calculate absolute time when this timer has to trigger
	 */

	g_get_current_time(&ti->expire);

	ti->expire.tv_usec += ti->recurring.tv_usec;
	ti->expire.tv_sec += ti->recurring.tv_usec / 1000000;
	ti->expire.tv_usec %= 1000000;
	ti->expire.tv_sec += ti->recurring.tv_sec;
}

/**
 * Add new timeout and recalculate existing timeouts
 */
void timeout_add_ti(struct timeout_item *ti)
{
	g_static_rw_lock_writer_lock(&(timeout_data->to_lock));

	timeout_data->to_list = g_slist_insert_sorted(timeout_data->to_list,
						ti, timeout_compare_timeouts);

	timeout_reschedule_timeout();
	g_static_rw_lock_writer_unlock(&(timeout_data->to_lock));
}

/**
 * Activate given timeout
 */
void timeout_activate_timeout(struct timeout_item *ti)
{
	if (ti->flags & TO_NOTIFY_QUEUE)
		g_async_queue_push(ti->notify.queue, ti->data);
	else
		g_thread_create(ti->notify.func, ti->data, FALSE, NULL);
}

/**
 * Schedule next timeout
 *
 * We assume that lock is held while this function is called.
 */
void timeout_reschedule_timeout(void)
{
	struct timeout_item *ti;
	GTimeVal curr, to;

	/*
	 * If there are no timeouts in a list then just exit...
	 */
	if (!timeout_data->to_list)
		return;

	ti = timeout_data->to_list->data;

	/*
	 * If there is no pending timeout, or, the element on the head of the
	 * list sooner expires, then reschedule timeout.
 	 */
	if (!timeout_data->next ||
			timeout_compare_timeouts(timeout_data->next, ti) > 0) {

		/*
		 * Calculate timeout for new source
		 */
		g_get_current_time(&curr);

		to.tv_sec = ti->expire.tv_sec - curr.tv_sec;
		if (to.tv_sec < 0) {
			LOG_ERROR("Timeout expired during rescheduling");
			return;
		}

		to.tv_usec = ti->expire.tv_usec - curr.tv_usec;
		if (to.tv_usec < 0) {
			to.tv_usec = 1000000 + to.tv_usec;
			to.tv_sec--;
			if (to.tv_sec < 0) {
				LOG_ERROR("Timeout expired during rescheduling");
				return;
			}
		}

		/*
		 * Commit new timeout...
		 */
		if (timeout_data->next)
			g_source_remove(timeout_data->next_src);

		timeout_data->next_src = g_timeout_add(
					to.tv_usec / 1000 + to.tv_sec * 1000,
					timeout_thread, NULL);

		timeout_data->next = ti;

	}
}

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS FOR timeout_item STRUCTURE
 ******************************************************************************/

struct timeout_item *timeout_item_new(void)
{
	struct timeout_item *ti;

	ti = g_malloc0(sizeof(struct timeout_item));

	return ti;
}

void timeout_item_free(struct timeout_item *ti)
{
	if (ti) {
		if ((ti->flags & TO_NOTIFY_QUEUE) && ti->notify.queue)
			g_async_queue_unref(ti->notify.queue);

		g_free(ti);
	}
}

/*******************************************************************************
 * MANIPULATION FUNCTIONS
 ******************************************************************************/

/**
 * Register timeout that has to be reported via asynchronous queue
 *
 * For parameters look the timeout_register function.
 */
timeout_t *timeout_register_queue(GTimeVal *tv, guint32 flags, gpointer data,
		GAsyncQueue *queue)
{
	flags |= TO_NOTIFY_QUEUE;
	return timeout_register(tv, flags, data, queue);
}

/**
 * Register timeout that has to start new thread
 *
 * For parameters look the timeout_register function.
 */
timeout_t *timeout_register_thread(GTimeVal *tv, guint32 flags, gpointer data,
		GThreadFunc func)
{
	return timeout_register(tv, flags, data, func);
}

/*
 * Register new timeout
 *
 * @param msecs		Number of miliseconds until timeout expires
 * @param queue		Queue on which timeout message will be sent
 * @param flags		Flags that determine type and behavior of timeout:
 *
 *			TO_NOTIFY_QUEUE
 *				When timeout expires send data into
 *				queue "queue". Otherwise, start a new
 *				thread.
 *
 *			TO_ABSOLUTE
 *				Given time is absolute point in time
 *				when timeout has to expire. Otherwise
 *				it's interval.
 *
 *			TO_RECURRING
 *				Timeout recurres until explicitely
 *				revoked by callee. Otherwise, it's not
 *				recurring.
 *
 * @param data		Data that will be passed in message
 * @param notify	Pointer to a queue or a function that should be called
 *
 * \return	Timeout ID, or NULL in case an error occured
 */
timeout_t *timeout_register(GTimeVal *tv, guint32 flags, gpointer data,
		gpointer notify)
{
	timeout_t *ti;

	if ((flags & TO_ABSOLUTE) && (flags & TO_RECURRING)) {
		LOG_BUG("Timeout can't be absolute and recuring "
				"in the same time!");
		ti = NULL;
		goto out;
	}

	if ((ti = timeout_item_new()) == NULL)
		goto out;

	ti->data = data;

	if (flags & TO_ABSOLUTE) {
		ti->expire.tv_usec = tv->tv_usec;
		ti->expire.tv_sec = tv->tv_sec;
	} else {

		ti->recurring.tv_sec = tv->tv_sec;
		ti->recurring.tv_usec = tv->tv_usec;

		if (flags & TO_RECURRING)
			ti->flags |= TO_RECURRING;

		timeout_calc_next_occurence(ti);
	}

	if (flags & TO_NOTIFY_QUEUE) {
		ti->flags |= TO_NOTIFY_QUEUE;
		g_async_queue_ref(notify);
		ti->notify.queue = notify;
	} else
		ti->notify.func = notify;

	timeout_add_ti(ti);

out:
	return ti;
}

/**
 * Cancel timeout
 *
 * @param tid	ID of timeout to be canceled
 *
 * \return
 */
void timeout_cancel(timeout_t *tid)
{
	g_static_rw_lock_reader_lock(&(timeout_data->to_lock));

	timeout_data->to_list = g_slist_remove(timeout_data->to_list, tid);
	timeout_item_free(tid);
	if (tid == timeout_data->next) {
		timeout_data->next = NULL;
		g_source_remove(timeout_data->next_src);
		timeout_reschedule_timeout();
	}

	g_static_rw_lock_reader_unlock(&(timeout_data->to_lock));
}

/*******************************************************************************
 * THREAD THAT CHECKS FOR TIMEOUTS
 ******************************************************************************/

gboolean timeout_thread(gpointer data)
{
	GTimeVal curr;
	struct timeout_item *ti;

	/*
	 * Flag if we activated any timeout. Used to chase bugs with regard
	 * to rounding errors...
	 */
	gboolean removed = TRUE;

	g_static_rw_lock_reader_lock(&(timeout_data->to_lock));

	/*
	 * It could happen that the source has been removed while we
	 * were waiting on a lock...
	 */
	if (!timeout_data->next)
		goto out;

	removed = FALSE;

	/*
	 * Remove source that triggered our exection...
	 */
	g_source_remove(timeout_data->next_src);

	/*
	 * Get current time
	 */
	g_get_current_time(&curr);

	/*
	 * Round microseconds
	 */
	curr.tv_usec += 999;
	if (curr.tv_usec > 999999) {
		curr.tv_usec -= 1000000;
		curr.tv_sec++;
	}

	while (timeout_data->to_list) {
		ti = timeout_data->to_list->data;

		if (ti->expire.tv_sec > curr.tv_sec)
			break;

		if (ti->expire.tv_sec == curr.tv_sec &&
				ti->expire.tv_usec > curr.tv_usec)
			break;

		timeout_data->to_list = g_slist_remove(timeout_data->to_list,
							ti);

		if (timeout_data->next == ti)
			timeout_data->next = NULL;

		removed = TRUE;

		timeout_activate_timeout(ti);

		if (ti->flags & TO_RECURRING) {
			timeout_calc_next_occurence(ti);

			timeout_data->to_list = g_slist_insert_sorted(
						timeout_data->to_list, ti,
						timeout_compare_timeouts);
		} else
			timeout_item_free(ti);
	}

	if (!removed) {
		/*
		 * We didn't remove/activate any timeout, so force
		 * rescheduling...
		 */
		if (timeout_data->next) {
			g_source_remove(timeout_data->next_src);
			timeout_data->next = NULL;
		}

		/*
		printf ("No timeout removed!\n");
		printf ("curr.tv_sec=%u curr.tv_usec=%u\n",
				(guint)curr.tv_sec, (guint)curr.tv_usec);
		printf ("timeout_data->next->expire.tv_sec=%u "
			"timeout_data->next->expire.tv_usec=%u\n",
				(guint)timeout_data->next->expire.tv_sec,
				(guint)timeout_data->next->expire.tv_usec);
		*/

	}

	timeout_reschedule_timeout();

out:
	g_static_rw_lock_reader_unlock(&(timeout_data->to_lock));

	return FALSE;
}

/*******************************************************************************
 * MODULE LOAD AND UNLOAD FUNCTIONS
 ******************************************************************************/

void timeout_unload(void)
{
	LOG_FUNC_START(1);

	if (timeout_data) {

		g_static_rw_lock_writer_lock(&timeout_data->to_lock);

		if (timeout_data->check_thread)
			g_source_remove(timeout_data->check_thread);

		if (timeout_data->next)
			g_source_remove(timeout_data->next_src);

		g_static_rw_lock_writer_unlock(&timeout_data->to_lock);

		g_static_rw_lock_free(&timeout_data->to_lock);

		g_free(timeout_data);
	}

	LOG_FUNC_END(1);
}

int timeout_init(void)
{
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	timeout_data = g_malloc0(sizeof(struct timeout_data));

	g_static_rw_lock_init(&timeout_data->to_lock);

	retval = 0;

out:

	LOG_FUNC_END(1);

	return retval;
}
