#!/bin/bash

# This is a sample script on how to configure roadwarrior
# with dynamically assigned parameters.

if [ -z "$1" ]; then
	echo "Usage: $0 <up|down>"
	exit 1
fi

case "$1" in
up)
	echo "ADDING CONFIGURATION DATA"

	echo "Client ID $IKEV2_CLIENT_ID"
	echo "IPv4 address $IKEV2_IPV4_ADDR"
	echo "IPv4 netmask $IKEV2_IPV4_NETMASK"
	echo "DHCPv4 server(s) $IKEV2_IPV4_DHCP"
	echo "DNS server(s) $IKEV2_IPV4_DNS"
	echo "WINS server(s) $IKEV2_IPV4_NBNS"
	echo "IPv6 address $IKEV2_IPV6_ADDR"
	echo "DHCPv6 server(s) $IKEV2_IPV6_DHCP"
	echo "DNS server(s) $IKEV2_IPV6_DNS"
	echo "Application on remote peer $IKEV2_APP_VER"
	echo "Interface $IKEV2_INTERFACE"

	# Seems like this is not necessary since Linux is not using
	# via after the encapsulation, i.e. anything can be placed
	# in here.
	echo "VPN gateway IPv4 address $IKEV2_VPN_IPV4_ADDR"

	echo "VPN gateway IPv6 address $IKEV2_VPN_IPV6_ADDR"
	echo "VPN gateway protected IPv4 subnets $IKEV2_VPN_SUBNETS4"
	echo "VPN gateway protected IPv6 subnets $IKEV2_VPN_SUBNETS6"

	for i in $IKEV2_IPV4_ADDR
	do
		/sbin/ip addr add $i dev $IKEV2_INTERFACE
	done

	for i in $IKEV2_VPN_SUBNETS4
	do
		/sbin/ip route add $i via $IKEV2_VPN_IPV4_ADDR \
				src $IKEV2_IPV4_ADDR
	done

	exit 0
	;;

down)

	echo "REMOVING CONFIGURATION DATA"
	echo "Client ID $IKEV2_CLIENT_ID"
	echo "IPv4 address $IKEV2_IPV4_ADDR"
	echo "IPv4 netmask $IKEV2_IPV4_NETMASK"
	echo "DHCPv4 server(s) $IKEV2_IPV4_DHCP"
	echo "DNS server(s) $IKEV2_IPV4_DNS"
	echo "WINS server(s) $IKEV2_IPV4_NBNS"
	echo "IPv6 address $IKEV2_IPV6_ADDR"
	echo "DHCPv6 server(s) $IKEV2_IPV6_DHCP"
	echo "DNS server(s) $IKEV2_IPV6_DNS"
	echo "Application on remote peer $IKEV2_APP_VER"
	echo "Interface $IKEV2_INTERFACE"

	# Seems like this is not necessary since Linux is not using
	# via after the encapsulation, i.e. anything can be placed
	# in here.
	echo "VPN gateway IPv4 address $IKEV2_VPN_IPV4_ADDR"

	echo "VPN gateway IPv6 address $IKEV2_VPN_IPV6_ADDR"
	echo "VPN gateway protected IPv4 subnets $IKEV2_VPN_SUBNETS4"
	echo "VPN gateway protected IPv6 subnets $IKEV2_VPN_SUBNETS6"

	for i in $IKEV2_VPN_SUBNETS4
	do
		/sbin/ip route del $i via $IKEV2_VPN_IPV4_ADDR \
				src $IKEV2_IPV4_ADDR
	done

	for i in $IKEV2_IPV4_ADDR
	do
		/sbin/ip addr del ${i}/32 dev $IKEV2_INTERFACE
	done

	exit 0
	;;
*)
	echo "Unrecognized parameter"
	exit 1;
esac
