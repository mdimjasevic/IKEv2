/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __PROPOSAL_H
#define __PROPOSAL_H

typedef struct proposal {

	/**
	 * Protocol for CHILD SA. Either ESP or AH.
	 */
	guint8 protocol_id;

	/**
	 * SPI value and SPI size in octets
	 */
	guint8 spi_size;
	guint64 spi;

	/**
	 * List of transforms offered in a proposal
	 */
	GSList *transforms;

} proposal_t;

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/
proposal_t *proposal_new();
void proposal_free(proposal_t *);
void proposal_list_add_proposal(GSList **, proposal_t *);
void proposal_list_free(GSList *);

GSList *proposal_list_create(GSList *, GSList *, guint8, guint8, guint64);
struct proposal *proposal_get(GSList *, GSList *, GSList *);

void proposal_clear(proposal_t *);

void proposal_transform_append(proposal_t *, transform_t *);

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/
guint8 proposal_get_protocol(proposal_t *);
void proposal_set_protocol(proposal_t *, guint8);
guint8 proposal_get_spi_size(proposal_t *);
void proposal_set_spi_size(proposal_t *, guint8);
guint64 proposal_get_spi(proposal_t *);
void proposal_set_spi(proposal_t *, guint64);

guint32 proposal_get_soft_allocations(proposal_t *);
void proposal_set_soft_allocations(proposal_t *, guint32);
guint32 proposal_get_hard_allocations(proposal_t *);
void proposal_set_hard_allocations(proposal_t *, guint32);
guint64 proposal_get_soft_octets(proposal_t *);
void proposal_set_soft_octets(proposal_t *, guint64);
guint64 proposal_get_hard_octets(proposal_t *);
void proposal_set_hard_octets(proposal_t *, guint64);
guint32 proposal_get_soft_addtime(proposal_t *);
void proposal_set_soft_addtime(proposal_t *, guint32);
guint32 proposal_get_hard_addtime(proposal_t *);
void proposal_set_hard_addtime(proposal_t *, guint32);
guint32 proposal_get_soft_time(proposal_t *);
void proposal_set_soft_time(proposal_t *, guint32);
guint32 proposal_get_hard_time(proposal_t *);
void proposal_set_hard_time(proposal_t *, guint32);

/*******************************************************************************
 * OTHER METHODS
 ******************************************************************************/
proposal_t *proposal_intersect(GSList *, GSList *, guint8);
GSList *proposal_intersect_l(GSList *, GSList *, guint8);
GSList *proposal_duplicate(GSList *, guint8, guint8, guint64);
void proposal_list_set_spi(GSList *, guint8, guint64);
transform_t *proposal_list_find_transform(GSList *, guint8, guint8);
transform_t *proposal_find_transform(proposal_t *, guint8, guint8);

GSList *proposal_remove_dh_group(GSList *, guint16);
transform_t *proposal_find_dh_group(GSList *, guint16);

/*******************************************************************************
 * DEBUG METHODS
 ******************************************************************************/
void proposal_dump_proposal(char *, proposal_t *);
void proposal_dump_proposal_list(char *, GSList *);

#endif /* __PROPOSAL_H */
