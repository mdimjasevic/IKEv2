/***************************************************************************
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

#define __CERT_C 

#define LOGGERNAME	"cert"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif /* HAVE_SYS_STAT_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */

#include <openssl/rand.h>
#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/md5.h>
#include <openssl/sha.h>
#include <openssl/des.h>
#include <openssl/aes.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/conf.h>

#include <glib.h>

#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "auth.h"
#include "config.h"
#include "cert.h"
#include "payload.h"
#include "transforms.h"
#include "aes_xcbc.h"
#include "crypto.h"
#include "message_msg.h"
#include "cert_msg.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * Structure with private data for cert module
 */
struct cert_data *cert_data = NULL;

/*******************************************************************************
 * DL_REQUEST STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/

/**
 * Allocate new dl_request structure
 */
struct dl_request *dl_request_new(int items)
{
	struct dl_request *dl_req, *ret = NULL;
	struct dl_item *dl_item;
	int i;

	LOG_FUNC_START(1);

	dl_req = g_malloc0(sizeof(struct dl_request));

	if (items < 1) {
		LOG_BUG("Invalid number of download items!");
		dl_req->items = 1;
	} else
		dl_req->items = items;

	/*
	 * Add 'items' number of download items
	 */
	dl_req->dl_items = NULL;
	for (i = 0; i < items; i++) {
		dl_item = g_malloc0 (sizeof(struct dl_item));

		dl_req->dl_items = g_slist_append (dl_req->dl_items, dl_item);
	}

	ret = dl_req;

	LOG_FUNC_END(1);

	return ret;
}

/**
 * Free dl_request structure
 */
void dl_request_free(struct dl_request *dl_req)
{
	struct dl_item *dl_item;

	LOG_FUNC_START(1);

	if (dl_req)
	{
		/*
		 * Iterate through list, delete list elements
		 * and elements data
		 */
		while (dl_req->dl_items) {
			dl_item = dl_req->dl_items->data;
			dl_req->dl_items = g_slist_remove(dl_req->dl_items,
					dl_item);
			dl_item_free(dl_item);
		}

		g_free(dl_req);
	}

	LOG_FUNC_END(1);
}

/**
 * Free dl_item structure
 */
void dl_item_free(struct dl_item *dl_item)
{
	LOG_FUNC_START(1);

	if (dl_item) {

		if (dl_item->url)
			g_free(dl_item->url);

		if (dl_item->dl_file)
			g_free(dl_item->dl_file);

		g_free(dl_item);
	}

	LOG_FUNC_END(1);
}

/*******************************************************************************
 * CERT SPECIFIC CONFIG_PEER STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/

/**
 * Allocate new cert_item structure
 */
struct cert_item *cert_item_new()
{
	struct cert_item *cert_item;

	cert_item = g_malloc0(sizeof(struct cert_item));

	cert_item->file = NULL;
	cert_item->priv_key_file = NULL;
	cert_item->url = NULL;

	return cert_item;
}

/**
 * Free cert_item structure
 */
void cert_item_free(struct cert_item *cert_item)
{
	LOG_FUNC_START(1);

	if (cert_item) {

		if (cert_item->file)
			g_free(cert_item->file);	 

		if (cert_item->priv_key_file)
			g_free(cert_item->priv_key_file);	 

		if (cert_item->url)
			g_free(cert_item->url);

		if (cert_item->peers_id)
			id_free(cert_item->peers_id);

		g_free(cert_item);
	}

	LOG_FUNC_END(1);
}

/**
 * Free list of cert_item structures...
 */
void cert_item_list_free(GSList *cert_items)
{
	while (cert_items) {
		cert_item_free(cert_items->data);
		cert_items = g_slist_remove(cert_items, cert_items->data);
	}
}

/**
 * Returns the list of cert_item items that match the given encoding.
 *
 * @param encoding The encoding type in received CERTREQ payload
 * @param cert_items
 *
 * Returns the list of cert_item items of the list cert_items of the
 * requested type (encoding).
 */
GSList *cert_find_certs_by_type(int encoding, GSList *cert_items)
{
	GSList *appr_certs = NULL;
	struct cert_item *cert_item;

	LOG_FUNC_START(1);

	while (cert_items) {

		cert_item = (struct cert_item *)cert_items->data;
		if (cert_item->type == encoding) {
			/**
			 * cert_items is actually config_general->cert_items
			 */

			LOG_DEBUG("Added CERT item to appropriate certs");
			appr_certs = g_slist_append(appr_certs, cert_item);
		}
		else
			LOG_DEBUG("CERT encoding mismatch, skipped CERT item");

		cert_items = cert_items->next;
	}

	LOG_FUNC_END(1);

	return appr_certs;
}

/*******************************************************************************
 * CA STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/

/**
 * Allocate new CA structure
 */
struct ca_item *ca_item_new()
{
        struct ca_item *ca_item;

	ca_item = g_malloc0(sizeof(struct ca_item));

	ca_item->file = NULL;
	ca_item->crl_file = NULL;
	ca_item->url = NULL;
	ca_item->crl_url = NULL;

	return ca_item;
}

/**
 * Free CA structure
 */
void ca_item_free(struct ca_item *ca_item)
{
	struct session_certificate *sc;
	if (ca_item) {
		if (ca_item->file)
			g_free(ca_item->file);

		if (ca_item->url)
			g_free(ca_item->url);

		if (ca_item->crl_url)
			g_free(ca_item->crl_url);

		if (ca_item->crl_file)
			g_free(ca_item->crl_file);

		if (ca_item->session_certs) 
			while (ca_item->session_certs) {

				sc = ca_item->session_certs->data;
				if (sc->certificate->certificate)
					g_free(sc->certificate->certificate);
				if (sc->certificate->pub_key)
					g_free(sc->certificate->pub_key);
				g_free(sc->certificate);
				g_free(sc);
				
				ca_item->session_certs = 
					g_slist_remove(ca_item->session_certs,
						ca_item->session_certs->data);
			}
	
		g_free(ca_item);
	}
}

/**
 * Free list of CA structures
 */
void ca_item_list_free(GSList *cil)
{
	while (cil) {
		ca_item_free(cil->data);
		cil = cil->next;
	}
}

/**
 * Free list of CA items without deleting the elements
 */
void cert_ca_items_remove(GSList *ca_items) 
{
	LOG_FUNC_START(2);

	while (ca_items)
		ca_items = g_slist_remove(ca_items, ca_items->data);
	
	LOG_FUNC_END(2);
}


/**
 * Sort CA certificates into two lists: CA certificates of type X509 
 * Certificate - Signature and CA certificates of type Hash and URL
 * of the X509 Certificate.
 *
 * @param ca_items	List of items that stores data about the configured 
 *			CA certificates.
 * @param sig_ca_items	Items from the ca_items list that have encoding
 *			IKEV2_SIG
 * @param hash_ca_items	Items from the ca_items list that have encoding
 *			IKEV2_HASH_AND_URL
 *
 * \return	Returns TRUE on success. 
 */
gboolean config_find_ca_by_type(GSList *ca_items, GSList **sig_ca_items, 
			GSList **hash_ca_items) 
{
	struct ca_item *ca_item;
	GSList *t_ca_items;

	LOG_FUNC_START(1);

	ca_item = NULL;

	if ((t_ca_items = ca_items) == NULL)
		return FALSE;

	while (t_ca_items) {
		ca_item = t_ca_items->data;

		/**
		 * ca_item's are actually part of the
		 * config_general->ca_items list (memory will be cleaned there)
		 */
		if (ca_item->type == IKEV2_CERT_X509_SIGNATURE)
			*sig_ca_items = 
				g_slist_append(*sig_ca_items, ca_item);	
		else if (ca_item->type == IKEV2_CERT_HASH_AND_URL)
			*hash_ca_items = 
				g_slist_append(*hash_ca_items, ca_item);
		else {
			/**
			 * Log the unsupported CERTREQ Encoding type. But this
			 * should be discovered in parser.
			 */
			LOG_BUG("Unsupported CA encoding!");
			return FALSE;
		}

		t_ca_items = t_ca_items->next;
	}

	LOG_FUNC_END(1);

	return TRUE;
}

/**
 * Find localy stored certificates for given ID
 *
 * @param peers_cert_items	List of peers' certificates stored locally.
 * @param id			Identity received in ID payload that has to be
 * 				matched to the identity in peer's certificate.
 *
 * \return
 */
GSList *config_find_pci_by_id(GSList *peers_cert_items, struct id *id)
{
	struct cert_item *cert_item;
	GSList *t_cert_items, *cert_items_by_id;
	struct id *peers_id;

	LOG_FUNC_START(1);

	peers_id = NULL;
	cert_item = NULL;
	cert_items_by_id = NULL;

	if ((t_cert_items = peers_cert_items) == NULL)
		goto out;

	while (t_cert_items) {
		cert_item = t_cert_items->data;

		peers_id = cert_item->peers_id;

		if (peers_id == NULL) {

			LOG_ERROR("There is no identity associated to the "
					"peer's certificate (id = %s)", id->id);

		} else {

			if (!memcmp(peers_id->id, id->id, id->len)) {

			cert_items_by_id = g_slist_append( cert_items_by_id,
					cert_item);

			LOG_NOTICE("Found CA certificate associated to the " 
					"received identity (%s)", id->id);
			}
		}
	}

out:
	LOG_FUNC_END(1);

	return cert_items_by_id;
}

/*******************************************************************************
 * FUNCTIONS FOR MANIPULATING CERTIFICATES
 ******************************************************************************/

/**
 * Convert DER_ASN1_DN into string representation
 *
 * @param derasn1dn	DER encoded ASN1_DN value
 *
 * \return	ASN1_DN in string format
 *
 * Returned string should be freed with a call to g_free
 */
char *crypto_derasn1dn_to_string(gpointer derasn1dn) 
{
	X509_NAME *name;

	gchar str[DER_ASN1_DN_MAX_STR_LEN];

	if (derasn1dn) {
		name = d2i_X509_NAME(NULL, &derasn1dn, strlen(derasn1dn));

		X509_NAME_oneline(name, str, DER_ASN1_DN_MAX_STR_LEN);
	} else
		strcpy(str, "<none>");

	return g_strdup(str);
}

/** 
 * Convert ASN1_DN string into DER format
 *
 * @param string	Identity from the IKEV2 ID payload of type
 * 			IKEV2_IDT_DER_ASN1_DN.
 *
 * \return		String converted to DER_ASN1_DN format. NULL in
 *			case of an error.
 */
gpointer crypto_string_to_derasn1dn(char *string) 
{
	X509_NAME *name;
	gpointer der_asn1dn = NULL;
	char *buf, *field, *value;
	int i, der_len;
	unsigned char *der = NULL;

	LOG_FUNC_START(1);

	buf = g_strdup(string);

	name = X509_NAME_new();
	if (name == NULL) {
		LOG_ERROR("Error with X509_NAME_new.");
		goto out;
	}

	/*
	 * Pointer to a field name (O, OU, C, CN, ...)
	 */
	field = NULL;

	/*
	 * Pointer to a field's value, i.e. the string after equal
	 */
	value = NULL;

	/*
	 * Scan the whole ID string
	 */
	i = 0;
	while (buf[i]) {

		/*
		 * We have three state machine.
		 *
		 * In the first state (field == NULL && value == NULL)
		 * we have not encountered neither field nor value. We
		 * are in this state as long as we read spaces.
		 *
		 * In the second state (field != NULL && value == NULL)
		 * we are reading field's name. This state lasts until
		 * we encounter equal sign.
		 *
		 * Finally, in the third state we iterate until we
		 * find either comma or slash, or end of a string,
		 * meaning we come to the end of the value. In case
		 * comma/slash is encountered we just add the new
		 * X509_NAME and reset state machine.
		 *
		 * End of the line is a special case. It is handled
		 * outside of the loop.
		 */
		if (!field && !value) {

			if (buf[i] == '/')
				field = buf + i + 1;
			else if (buf[i] != ' ' && buf[i] != '\t')
				field = buf + i;

		} else if (field && !value) {

			if (buf[i] == '=') {
				buf[i] = 0;
				value = buf + i + 1;
			}

		} else {

			if (buf[i] == ',' || buf[i] == '/') {

				buf[i] = 0;

				if (!X509_NAME_add_entry_by_txt(name, field,
						MBSTRING_ASC, 
						(unsigned char*)value,
						-1, -1, 0)) {

					LOG_ERROR("%s can not be added to"
							"X509_NAME", field);
					goto out;
				}

				field = value = NULL;
			}
		}

		i++;
	}

	if (value) {

		if (!X509_NAME_add_entry_by_txt(name, field,
				MBSTRING_ASC, (unsigned char*)value, 
				-1, -1, 0)) {

			LOG_ERROR("%s can not be added to"
					"X509_NAME", field);
			goto out;
		}
	}

	/*
 	 * Convert X509_NAME structure to DER format
 	 */
	der_len = i2d_X509_NAME(name, &der);

	der_asn1dn = g_malloc(der_len + 1);

	memcpy(der_asn1dn, der, der_len);
	*(gchar *)(der_asn1dn + der_len) = '\0';

out:
	g_free(buf);

	X509_NAME_free(name);

	LOG_FUNC_END(1);

	return der_asn1dn;
}

void crypto_derasn1dn_free(gpointer derasn1dn)
{
	g_free(derasn1dn);
}

/**
 * Compare two derasn1dn structure for strict equality
 *
 * @param derasn1dn1
 * @param derasn1dn2
 *
 * \return TRUE if equal, FALSE otherwise
 */
gboolean crypto_derasn1dn_cmp(gpointer derasn1dn1, gpointer derasn1dn2)
{
	gboolean result;
	X509_NAME *id1 = NULL, *id2 = NULL;
#if 0
	char data1[DER_ASN1_DN_MAX_STR_LEN], data2[DER_ASN1_DN_MAX_STR_LEN];
#endif

	LOG_FUNC_START(1);

	result = FALSE;

	if (!d2i_X509_NAME(&id1, &derasn1dn1, strlen(derasn1dn1))) {
		LOG_ERROR("Error in ID value");
		goto out;
	}

	if (!d2i_X509_NAME(&id2, &derasn1dn2, strlen(derasn1dn2))) {
		LOG_ERROR("Error in ID value");
		goto out;
	}

#if 0
	X509_NAME_oneline(id2, data2, DER_ASN1_DN_MAX_STR_LEN);
	X509_NAME_oneline(id1, data1, DER_ASN1_DN_MAX_STR_LEN);
#endif

	result = !X509_NAME_cmp(id1, id2);

	X509_NAME_free(id1);
	X509_NAME_free(id2);

out:
	LOG_FUNC_END(1);

	return result;
}

/**
 * Read elements of 'subjectaltname'
 */
char *crypto_printf_ext_val_prn(guint8 id_type, STACK_OF(CONF_VALUE) *val)
{
	int i;
	char *ext_result;
	CONF_VALUE *nval;

	ext_result = NULL;

	if (!val)
		goto out;

	if(!sk_CONF_VALUE_num(val)) {
		if (!sk_CONF_VALUE_num(val)) {
			/* FIXME:
			 * LOG_ERROR: printf("<EMPTY>\n");
			 */
			goto out;
		}
	}

	for(i = 0; i < sk_CONF_VALUE_num(val); i++) {

		/* FIXME: LOG_ERROR:
		if (i > 0)
			printf(", ");
		*/

		nval = sk_CONF_VALUE_value(val, i);

		/* FIXME: 
		if (!nval->name)
			printf("nval->value = %s\n", nval->value);
		else if (!nval->value)
			printf("nval->name = %s\n", nval->name);
		else {
			printf("nval->name = %s\n", nval->name);
			printf("nval->value = %s\n", nval->value);
		}
		*/

		if (id_type == IKEV2_IDT_FQDN) {

			if (!strcmp(nval->name, "DNS")) {

				ext_result = nval->value;
				goto out;

			} else {
			/* LOG_ERROR: IKEV2_IDT_FQDN id type requests DNS
			 * subjectAltName extension which does not exist in
			 * received certificate. */
			}

		} else if (id_type == IKEV2_IDT_RFC822_ADDR) {

			if (!strcmp(nval->name, "RFC822")) {
			}

		} else if (id_type == IKEV2_IDT_IPV4_ADDR) {

			if (!strcmp(nval->name, "IP")) {
			}
		}

	}

out:

	return ext_result;
}

/**
 * Reads subjectAltName extension from the certificate in file.
 *
 * @param id_type    	Type of the subjectAltName extension.
 * @param filename 	Path to certificate file
 *
 * \return          	subjectAltName extension from the certificate.
 */
char *cert_get_subjectaltname(guint8 id_type, char *filename)
{
	FILE *fp;
	X509 *x509;
	char *ext_result;

	LOG_FUNC_START(1);

	ext_result = NULL;

	/* Open cert file */
	if ((fp = fopen(filename, "r")) == NULL) {
		LOG_ERROR("Unable to open certificate file (%s)",
				filename);
		goto out;
	}

	/* Load cert from file */
	x509 = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);

	if (x509 == NULL) {
		LOG_ERROR("Error reading certificate file (%s)",
				filename);
		goto out;
	}

	ext_result = cert_get_cert_subjectaltname(id_type, x509);

out:
	LOG_FUNC_END(1);

	return ext_result;
}

/**
 * Reads subjectAltName extension from the certificate.
 *
 * @param id_type    	Type of the subjectAltName extension.
 * @param x509		Certificate.
 *
 * \return          	subjectAltName extension from the certificate.
 */
char *cert_get_cert_subjectaltname(guint8 id_type, X509 *x509)
{
	X509_EXTENSION *x509_extension;
	const char *extension;
	int i;
	unsigned char *data;
	CONF_VALUE *nval;
	X509V3_EXT_METHOD *meth;
	char *value, *ext_result;
	char *test_str;

	LOG_FUNC_START(1);
	
	ext_result = NULL;

	for (i = 0; i <  X509_get_ext_count(x509); i++) {

		x509_extension = X509_get_ext(x509, i);

		extension =
			OBJ_nid2sn(OBJ_obj2nid(
				X509_EXTENSION_get_object(x509_extension)));

		if (strcmp(extension, "subjectAltName") == 0) {

			if (!(meth = X509V3_EXT_get(x509_extension))) {
				/* FIXME: Log! */
				break;
			}

			data = x509_extension->value->data;

			if (meth->it) 
				test_str = ASN1_item_d2i(NULL, &data,
					x509_extension->value->length,
					ASN1_ITEM_ptr(meth->it));
			else
				test_str = 
					meth->d2i(NULL, &data, 
						x509_extension->value->length);

			if (!test_str) {
				/* LOG_ERROR: Unknown extension */
				goto out;
			}

			if(meth->i2s) { 

				/* FIXME:
				 * LOG_ERROR: printf ("i2s\n"); 
				 */
				if ((value = meth->i2s(meth, test_str))) { 
				/* FIXME: LOG_ERROR: printf("%s", value); */
				}

			} else if (meth->i2v) { 

				if ((nval = meth->i2v(meth, test_str, NULL)))
					/* id_type is ID payload's type. */
					ext_result = 
						crypto_printf_ext_val_prn(
							id_type, nval);

				if (ext_result != NULL)
					goto out;

			} else if (meth->i2r) {
				/* FIXME:
				 * LOG_ERROR: printf ("Not programmed\n");
				 */
			}
		}
	}

out:
	LOG_FUNC_END(1);

	return ext_result;
}

/**
 * Reads subjectName (DN field) from the certificate.
 *
 * @param filename	Filename with the certificate.
 *
 * \return	subjectName (DN field) from the certificate.
 */
char *cert_get_cert_subjectname(char *filename) 
{
	FILE *fp;
	X509 *x509 = NULL;
	char data[256];
	char *derasn1dn = NULL;

	LOG_FUNC_START(1);

	/* Open cert file */
	if ((fp = fopen(filename, "r")) == NULL) {
		LOG_ERROR("Unable to open certificate file (%s)",
				filename);
		goto out;
	}

	/* Load cert from file */
	x509 = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);

	if (x509 == NULL) {
		LOG_ERROR("Error reading certificate file (%s)",
				filename);
		goto out;
	}

	X509_NAME_oneline(X509_get_subject_name(x509), data, 256);
	LOG_NOTICE("subject = %s\n", data);

	derasn1dn = crypto_string_to_derasn1dn(data);

out:
	if (x509)
		X509_free(x509);

	LOG_FUNC_END(1);

	return derasn1dn;
}

/**
 * Given certificate extract the public key
 *
 * @param type		Certificate type
 * @param data		Pointer to certificate
 * @param dlen		Length of certificate data
 * @param public_key	Public key taken from certificate
 *
 * \return Length of DER encoded public key, -1 in case of an error
 */
gint32 crypto_cert_get_pubkey(guint8 type, unsigned char *data, guint16 dlen,
		unsigned char **public_key)
{
	X509 *x509;
	EVP_PKEY *pubkey;
	gint32 len;

	LOG_FUNC_START(1);

	*public_key = NULL;
	len = -1;

	if ((type =! IKEV2_CERT_X509_SIGNATURE) ||
			(type =! IKEV2_CERT_HASH_AND_URL)) {

		LOG_ERROR("Unsupported certificate type");
		goto out;

	}

	/* Convert cert to internal format */
	x509 = d2i_X509(NULL, &data, dlen);
	if (x509 == NULL) {
		LOG_ERROR("Error reading certificate from memory");
		goto out;
	}

	/* Get public key */
	pubkey = X509_get_pubkey(x509);
	X509_free(x509);

	if (pubkey == NULL) {
		LOG_ERROR("Error getting public key from memory");
		goto out;
	}

	/* Convert pubkey to DER encoding */
	len = i2d_PublicKey(pubkey, public_key);
	EVP_PKEY_free(pubkey);

out:

	LOG_FUNC_END(1);

	return len;
}

/**
 * Load a certificate from a file and convert it to DER encoding
 *
 * @param type		Certificate type
 * @param filename	File name containing certificate
 * @param cert		Result
 *
 * \return Length in byte of cert, -1 in case of an error
 */
gint32 cert_load_data(int type, char *filename, unsigned char **cert)
{
	FILE *fp;
	X509 *x509;
	gint32 len;

	*cert = NULL;
	len = -1;

	LOG_FUNC_START(1);

	if (filename == NULL)
		return -1;

	if ((type =! IKEV2_CERT_X509_SIGNATURE) ||
			(type =! IKEV2_CERT_HASH_AND_URL)) {

		LOG_ERROR("Unsupported certificate type");
		goto out;
	}

	/* Open cert file */
	if ((fp = fopen(filename, "r")) == NULL) {
		LOG_ERROR("Unable to open certificate file (%s)",
				filename);
		goto out;
	}

	/* Load cert from file */
	x509 = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);

	if (x509 == NULL) {
		LOG_ERROR("Error reading certificate file (%s)",
				filename);
		goto out;
	}

	/* Convert cert to DER encoding */
	len = i2d_X509(x509, cert);
	X509_free(x509);

	LOG_FUNC_END(1);

out:

	return len;
}

/**
 * Check if identity from ID payload matches the corresponding PKIX attribute
 *
 * @param cert_items	Received certificates
 * @param certs			Locally stored peer's certificates
 * @param id			Identity from ID payload			
 *
 * \return true for matched IDs and false for for unmatched IDs 
 *
 * Check if identity from ID payload matches the corresponding PKIX attribute:
 * 		IP*_ADDR 		to SubjAltName iPAddress
 * 		FQDN	 		to SubjAltName dNSName
 * 		RFC822_ADDR		to SubjAltName rfc822Name
 * 		DN       		to SubjectName
 */
gboolean crypto_verify_id(GSList *cert_items, GSList *certs, struct id *id)
{
	X509 *x509;
	char data[256];
	char *cert_id = NULL;
	struct cert_item *cert_item;
	struct cert *cert;
	GSList *t_cert_items;
	GSList *t_certs;
	gboolean cmp_result;
	unsigned char *pointer;

	LOG_FUNC_START(1);

	cmp_result = FALSE;

	if (cert_items != NULL) {
		LOG_DEBUG("cert_items: local cert files");
		t_cert_items = cert_items;

		while (t_cert_items != NULL) {

			cert_item = t_cert_items->data;

			switch (id->type) {

			case IKEV2_IDT_DER_ASN1_DN:
				/*
				 * Cert_id is in DER format, as well as id.
				 */
				LOG_DEBUG("case IKEV2_IDT_DER_ASN1_DN");
				cert_id = cert_get_cert_subjectname(
						cert_item->file);
				break;

			default:

				LOG_DEBUG("default: SubjectAltName");
				cert_id = cert_get_subjectaltname(
						id->type, cert_item->file);

				if (cert_id == NULL) {
					LOG_ERROR("SubjectAltName not found "
							"in certificate.");
					goto out;
				}

				break;
			}

			if ((id->id == NULL) && (cert_id == NULL))
				goto out;

			cmp_result = crypto_cmp_id(id->type, cert_id,
						id->id, id->len);

			if (cmp_result == FALSE)
				goto out;

			t_cert_items = t_cert_items->next;
		}

	} else if (certs != NULL) {

		LOG_DEBUG("certs: from payload");
		t_certs = certs;

		while (t_certs != NULL) {

			cert = t_certs->data;

			LOG_DEBUG("Invoking d2i_X509()");
			x509 = NULL;
			/* d2i_X509 'may' change the second argument
			 * so we have to use a temp pointer! */
			pointer = (unsigned char *) cert->certificate;
			x509 = d2i_X509(NULL,
				&pointer,
				cert->certificate_len);

			if (!x509) {
				LOG_ERROR("Certificate error");
				goto out;
			}

			switch (id->type) {
			case IKEV2_IDT_DER_ASN1_DN:
				LOG_DEBUG("case IKEV2_IDT_DER_ASN1_DN:");
				X509_NAME_oneline(X509_get_subject_name(x509),
						data, 256);
				LOG_DEBUG("subject = %s\n", data);

				cert_id = crypto_string_to_derasn1dn(data);
				if (cert_id == NULL)
					goto out;

				LOG_DEBUG("Found certificate identity: %s",
					cert_id);
				cmp_result = crypto_cmp_id(id->type, cert_id,
						id->derasn1dn, id->len);
				break;

			default:
				LOG_DEBUG("default:");

				/* read subjectaltname from 'X509 *x509' */
				cert_id = cert_get_cert_subjectaltname(
					id->type, x509);

				if (cert_id == NULL)
					goto out;

				LOG_DEBUG("Found certificate identity: %s",
					cert_id);
				cmp_result = crypto_cmp_id(id->type, cert_id,
						id->id, id->len);
				break;

			}

			if (cmp_result == TRUE)
				goto out;

			t_certs = t_certs->next;
		}

	} else {
		LOG_BUG("Empty certificate lists!");
	}

out:
	if (cmp_result == FALSE)
		LOG_ERROR("Certificate identity does not match received" 
				"IKEv2 ID payload identity.");
	else 
		LOG_DEBUG("Certificate identity matched to the" 
				"IKEv2 ID payload identity!");

	LOG_FUNC_END(1);

	return cmp_result;
}

/**
 * Compare ID's based on their type
 *
 * \return	TRUE if ID's are the same, FALSE otherwise
 */
gboolean crypto_cmp_id(int id_type, char *cert_id, char *id, int id_len)
{
	gboolean cmp_result;

	LOG_FUNC_START(1);

	cmp_result = FALSE;

	switch (id_type) {

		case IKEV2_IDT_DER_ASN1_DN:

			LOG_DEBUG("case IKEV2_IDT_DER_ASN1_DN");
			cmp_result = crypto_cmp_subjectnames(
				(unsigned char*)cert_id, 
				(unsigned char*)id, id_len);

			break;

		default:

			LOG_DEBUG("default: bitwise comparison");
			if (!memcmp(cert_id, id, id_len)) {
				cmp_result = TRUE;
				goto out;
			}
	}

out:

	LOG_FUNC_END(1);

	return cmp_result;
}

/**
 *  Compare two subject names
 *
 *  FIXME: This function is candidate to be replaced by
 *  crypto_derasn1dn_cmp
 */
gboolean crypto_cmp_subjectnames(unsigned char *cert_id, 
		unsigned char *id, int id_len) 
{
	X509_NAME *i_cert_id = NULL, *i_id = NULL;
	char data1[DER_ASN1_DN_MAX_STR_LEN], data2[DER_ASN1_DN_MAX_STR_LEN];

	gboolean cmp_result = FALSE;

	LOG_FUNC_START(1);

	if (!d2i_X509_NAME(&i_cert_id, &cert_id, strlen((char*)cert_id))) {
		LOG_ERROR("Error with d2i_X509_NAME for cert_id\n");
		goto out;
	}

	X509_NAME_oneline(i_cert_id, data1, DER_ASN1_DN_MAX_STR_LEN);

	if (!d2i_X509_NAME(&i_id, &id, id_len)) {
		LOG_ERROR("Error with d2i_X509_NAME for id\n");
		goto out;
	}

	X509_NAME_oneline(i_id, data2, DER_ASN1_DN_MAX_STR_LEN);
	LOG_DEBUG("Certificate identity = %s, ID payload identity = %s ",
			data1, data2);

	if (X509_NAME_cmp(i_cert_id, i_id) == 0)
		cmp_result = TRUE;

	X509_NAME_free(i_cert_id);
	X509_NAME_free(i_id);

out:
	LOG_FUNC_END(1);

	return cmp_result;
}

/**
 * Read public key from a certificate file
 *
 * @param ca_file	File with the CA certificate in PEM format.
 * @param public_key	Public key from the CA certificate.
 * 
 * \return Length of the public key.
 */
gint32 crypto_read_pubkey_from_file(char *ca_file, unsigned char **public_key)
{
	X509 *x509;
	FILE *fp;
	EVP_PKEY *pubkey = NULL;
	gint32 len;

	LOG_FUNC_START(1);

	*public_key = NULL;
	len = -1;

	if (ca_file == NULL)
		goto out;

	if ((fp = fopen(ca_file, "r")) == NULL) {
		LOG_ERROR("Unable to open certificate file (%s)", ca_file);
		goto out;
	}

	/**
	 * Read Trust Anchor certificate and the public key from
	 * the certificate.
	 */
	x509 = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);

	/*
	 * If loading didn't succedded then skip further processing
	 */
	if (x509 == NULL) {
		LOG_ERROR("Loading of trust anchor certificate and public key "
			"didn't succedded!");
		goto out;
	}

	pubkey = X509_get_pubkey(x509);
	X509_free(x509);

	if (pubkey == NULL) {
		LOG_ERROR("Error with pubkey calculation");
		goto out;
	}

	/**
	 * Convert pubkey to DER encoding 
	 */
#if 0
	len = i2d_PublicKey(pubkey, NULL);
	*public_key = g_malloc(len);
#endif

	len = i2d_PublicKey(pubkey, public_key);
	EVP_PKEY_free(pubkey);

out:
	LOG_FUNC_END(1);

	return len;
}


/**
 * Compare public keys in a list of certificates
 *
 * @param payload_certs	List of certs from CERT payloads
 * @param local_certs	List of certs from local file store
 * 
 * \return	TRUE if all public keys are the same, FALSE otherwise
 */
gboolean cert_check_public_keys(GSList *payload_certs, GSList *local_certs)
{
	gboolean retval = FALSE;
	char *cert_file;
	unsigned char *next_key, *prev_key;
	int next_len, prev_len;

	LOG_FUNC_START(1);

	prev_len = 0;
	prev_key = next_key = NULL;

	while (payload_certs) {

		next_key = 
			((struct cert *)payload_certs->data)->pub_key;
		next_len = 
			((struct cert *)payload_certs->data)->pub_key_len;

		if (prev_len) {

			if (prev_len != next_len) {
				goto out;
			}
			if (memcmp(prev_key, next_key, prev_len)) {
					goto out;
			}
		}

		prev_key = next_key;
		prev_len = next_len;
		payload_certs = payload_certs->next;
	}

	prev_len = 0;
	prev_key = next_key = NULL;

	while (local_certs) {

		cert_file = ((struct cert_item*)
			local_certs->data)->file;

		next_len = 
			crypto_read_pubkey_from_file(cert_file, &(next_key));

		if (prev_len) {

			if (prev_len != next_len) {
				goto out;
			}
			if (memcmp(prev_key, next_key, prev_len)) {
					goto out;
			}
		}

		if (prev_key)
			g_free(prev_key);

		prev_key = next_key;
		prev_len = next_len;
		local_certs = local_certs->next;
	}

	retval = TRUE;

out:
	if (prev_key)
		g_free(prev_key);

	if (next_key && next_key != prev_key)
		g_free(next_key);

	LOG_FUNC_END(1);

	return retval;
}


/**
 * Convert CA public keys into concatenated list of SHA-1 hashes.
 *
 * @param ca_items list of CA-related data
 * @param hash_list list of SHA-1 hashes
 *
 * \return Length in bytes of hash list, -1 in case of an error
 */
int crypto_create_hash_list(GSList *ca_items, char **hash_list) 
{
	GSList *t_ca_items;
	struct ca_item *ca_item;
	unsigned char *public_key, md[SHA1_DIGEST_SIZE];
	gint32 len, total_hash_len;

	LOG_FUNC_START(1);

	len = 0;
	total_hash_len = 0;
	*hash_list = NULL;

	if ((t_ca_items = ca_items) == NULL)
		return -1;

	while (t_ca_items != NULL) {

		ca_item = (struct ca_item*)t_ca_items->data;

		if ((len = crypto_read_pubkey_from_file(ca_item->file, 
						&public_key)) <= 0)	{
			return -1;
		}

		/**
		 * Create SHA-1 hash of DER-encoded public key and add
		 * SHA-1 hash to the list of the concatenated hashed CA's
		 * public keys.
		 * */

		SHA1(public_key, len, md);

		*hash_list = g_realloc(*hash_list, total_hash_len +
				SHA1_DIGEST_SIZE);

		if (*hash_list == NULL) {
			LOG_ERROR("Out of memory");
			return -1;
		}

		memcpy(*hash_list + total_hash_len, md, SHA1_DIGEST_SIZE);
		total_hash_len += SHA1_DIGEST_SIZE;

		/**
		 * Move to the next Trust Anchor.
		 */
		t_ca_items = t_ca_items->next;
	}

	LOG_FUNC_END(1);

	return total_hash_len;
}

/**
 * Compare local CAs' and CERTREQ CAs' hashes
 *
 * @param certreq	List with the items that store data extracted from
 *						each CERTREQ payload.
 * @param ca_items	List of the configured CA items data.
 *
 * \return List of the data related to the locally configured CA's that
 * peer also have configured.
 *
 * Compares hashes of the CA certificates received in the CERTREQ payload
 * and locally configured CA certificates to determine which end-entity 
 * certificates to send in the CERT payload.
 */
GSList *crypto_check_hash_list(struct certreq *certreq, GSList *ca_items)
{
	GSList *trusted_cas = NULL;
	struct ca_item *ca_item;
	unsigned char *public_key, md[SHA1_DIGEST_SIZE], *hash_list;
	guint16 len, list_len;

	LOG_FUNC_START(1);

	if ((certreq == NULL) || (ca_items == NULL))
		goto out;

	/**
	 * Check the configured CAs public keys. We are searching if we have
	 * configured the same CA's as those received in the CERTREQ. If not,
	 * we will act as we have received the Empty CERTREQ.
	 */
	while (ca_items != NULL) {

		ca_item = ca_items->data;
		LOG_DEBUG("Cheching CA item against CERTREQ items...");

		/**
		 * Read the public key of the configured CA and make the SHA-1
		 * hash. Compare that hash with the hash extracted from the
		 * CERTREQ (received_hash_list).
		 */
		if ((len = crypto_read_pubkey_from_file(ca_item->file,
						&public_key)) <= 0 ) {

			LOG_ERROR("Unable to read CA public key! "
			"(file: %s)", ca_item->file);
			goto out;
		}

		SHA1(public_key, len, md);

		list_len = certreq->hash_list_len;
		hash_list = certreq->hash_list;

		while (list_len) {
			if (!memcmp(md, hash_list, SHA1_DIGEST_SIZE)) {

				/*
				 * With this CA peer should be able to verify
				 * the peer's certificate; add it to the
				 * trusted_cas list.
				 */
				LOG_DEBUG("Added CA item to peer trusted CA's");
				trusted_cas = g_slist_append(trusted_cas,
					ca_item);
			}

			hash_list += SHA1_DIGEST_SIZE;
			list_len -= SHA1_DIGEST_SIZE;
		}

		ca_items = ca_items->next;
	}

out:
	LOG_FUNC_END(1);

	return trusted_cas;
}

/**
 * Callback function for the X509_verify_cert(), needed by OpenSSL.
 */
int verify_callback(int ok, X509_STORE_CTX *store)
{
	char data[256];
	int depth, err;
	X509 *x509;

	LOG_FUNC_START(1);

	if(!ok) {
		x509 = X509_STORE_CTX_get_current_cert(store);
		depth = X509_STORE_CTX_get_error_depth(store);
		err = X509_STORE_CTX_get_error(store);

		X509_NAME_oneline(X509_get_issuer_name(x509), data, 256);
		LOG_DEBUG("issuer = %s\n", data);

		X509_NAME_oneline(X509_get_subject_name(x509), data, 256);
		LOG_DEBUG("subject = %s\n", data);
		LOG_DEBUG("err %i:%s\n", err,
				X509_verify_cert_error_string(err));
	}

	LOG_FUNC_END(1);

	return ok;
}

/**
 * Perform verification of a single certificate with given context.
 *
 * @param ca_file	File with the CA certificate(s) in PEM format.
 * @param cert		Pointer to certificate data.
 * @param cert_len	Length of the certificate_data.
 * @param crl_file	File with the CRL.
 *
 * \return 0 if certificate verified succesfully, 1 otherwise
 */
int cert_verify(const char *ca_file, unsigned char *cert,
		guint16 cert_len, const char *crl_file)
{
	X509 *x509;
	X509_LOOKUP *lookup = NULL;
	X509_STORE *store = NULL;
	X509_STORE_CTX *ctx;
	int ret = 1, use_crl = 1;
	const char *ca_dir = NULL;
	
	LOG_FUNC_START(1);

	if ((cert == NULL) || !cert_len)
		goto out;

	/**
	 * Transform binary certificate data into X509 structure 
	 */
	x509 = d2i_X509(NULL, (const unsigned char **) &cert, cert_len);
	if (!x509) {
		LOG_ERROR("Certificate error");
		goto out;
	}

	OpenSSL_add_all_algorithms();

	store = X509_STORE_new();
	X509_STORE_set_verify_cb_func(store, verify_callback);

	/**
	 * Load the CA certificates and CRLs.
	 */
	if (X509_STORE_load_locations(store, ca_file, ca_dir) != 1) {
		/* Log error: Error loading the CA file or directory. */
	}
	if (X509_STORE_set_default_paths(store) != 1) {
		/* Log error: Error loading the system-wide CA certificates. */
	}
	if (!(lookup = X509_STORE_add_lookup(store, X509_LOOKUP_file()))) {
		/* Log error: Error creating X509 lookup object. */
	}

	if(crl_file == NULL)
		use_crl = 0;
	else LOG_DEBUG("Using CRL");

	if (use_crl && 
		(X509_load_crl_file(
			lookup, crl_file, 
			X509_FILETYPE_PEM) != 1)) {
		LOG_ERROR("Unable to load CRL! Verification unsuccessful!");
		goto out;
	}

	/**
	 * CRL verification is possible only in OpenSSL v >=0.9.7
	 */
#if (OPENSSL_VERSION_NUMBER > 0x00907000L)
	/* set the flags of the store so that CRLs are consulted */
	if(use_crl)
		X509_STORE_set_flags(store, X509_V_FLAG_CRL_CHECK |
			X509_V_FLAG_CRL_CHECK_ALL);
#endif

	/* create a verification context and initialize it */
	if (!(ctx = X509_STORE_CTX_new())) {
		/* Log error: Error creating X509_STORE_CTX object") */
	}
#if (OPENSSL_VERSION_NUMBER > 0x00907000L)
	if (X509_STORE_CTX_init(ctx, store, x509, NULL) != 1) {
		/* Log error: Error initializing verification context. */
	}
#else
	X509_STORE_CTX_init(ctx, store, x509, NULL);
#endif

	/**
	 * Verify the certificate. If certificated has been verified correctly
	 * the ret is 0.
	 */
	if (X509_verify_cert(ctx) != 1) {
		LOG_ERROR("Unsuccessfull certificate verification.");
		ret = 1;
	} else {
		LOG_DEBUG("Successfull certificate verification!");
		ret = 0;
	}

	X509_STORE_CTX_cleanup(ctx);

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Try to verify each certificate with each CA item
 *
 * @param cert_items	list of (local host) certicifates
 * @param ca_items	list of CA's
 * @param session	pointer to current session
 *
 * \return The list of successfully verified certificates.
 *
 * Interface between cert_find_appropriate_certs() and cert_verify(). We
 * are verifing each certificate against each configured CA certificate
 * (from flat files) and CRL.
 */
GSList *cert_prepare_cert_item_verify(GSList *cert_items, GSList *ca_items, 
				      gpointer session)
{
	unsigned char *cert;
	int cert_len;
	GSList *appr_certs, *t_ca_items;

	struct cert_item *cert_item;
	struct cert *certificate;
	struct ca_item *ca_item;
	struct session_certificate *session_cert;

	LOG_FUNC_START(1);

	appr_certs = NULL;

	while (cert_items != NULL) {
		LOG_DEBUG("Checking CERT item against CA items...");
		cert_item = (struct cert_item *)cert_items->data;

		t_ca_items = ca_items;
		while (t_ca_items != NULL) {

			ca_item = (struct ca_item *)t_ca_items->data;
			cert_len = cert_load_data(cert_item->type,
						cert_item->file, &cert);

			certificate = g_malloc0(sizeof(struct cert));
			certificate->certificate = cert;
			certificate->certificate_len = cert_len;

			if (!cert_verify(ca_item->file, cert, cert_len,
						ca_item->crl_file)) {
				appr_certs = g_slist_append(appr_certs, cert_item);

				/*
				 * Add this certificate and session pointer to CA
				 * if session is NULL, this has already been done
				 */
				if (session) {
					session_cert = g_malloc0(sizeof(struct session_certificate));
					session_cert->session = session;
					session_cert->certificate = certificate;
					ca_item->session_certs = 
						g_slist_append(ca_item->session_certs, session_cert);
				}
			}
			/* 
			 * TODO: certs are stored in CA structure 
			 * and freed when CA is deleted - optimize and free
			 * when session is deleted.
			 */

			t_ca_items = t_ca_items->next;
		}
		cert_items = cert_items->next;
	}


	LOG_FUNC_END(1);

	return appr_certs;
}

/**
 * Try to verify each certificate with each CA item
 *
 * @param cert_items	list of certificates from payload
 * @param ca_items	list of CA's
 * @param session	pointer to current session
 *
 * \return The list of successfully verified certificates.
 *
 * Similar to cert_prepare_cert_item_verify(), but this function takes as
 * argument certificate data extracted from the IKE payload and not from
 * the configuration file as cert_prepare_cert_item_verify() does.
 */
GSList *cert_prepare_cert_verify(GSList *cert_list, GSList *ca_items, 
				 gpointer session)
{
	GSList *verified_certs, *c;
	struct cert *cert;
	struct ca_item *ca_item;
	struct session_certificate *session_cert;

	LOG_FUNC_START(1);

	verified_certs = NULL;

	if (!cert_list)
		goto out;

	while (ca_items) {

		c = cert_list;

		while (c) {

			ca_item = (struct ca_item *)ca_items->data;
			cert = (struct cert *)c->data;

			if (!cert_verify(ca_item->file, cert->certificate,
					cert->certificate_len,
					ca_item->crl_file)) {

				/*
				 * Add certificate to the list of succesfully 
				 * verified certificates. 
				 */
				verified_certs = g_slist_append(verified_certs, cert);

				/*
				 * Add this certificate and session pointer to CA
				 * if session is NULL, this has already been done
				 */
				if (session) {
					session_cert = g_malloc0(sizeof(struct session_certificate));
					session_cert->session = session;
					session_cert->certificate = cert;
					ca_item->session_certs = 
						g_slist_append(ca_item->session_certs, session_cert);
				}
			}

			c = c->next;
		}

		ca_items = ca_items->next;
	}

out:
	LOG_FUNC_END(1);

	return verified_certs; 
}

/**
 * Build certificate list according to data in CERTREQ
 *
 * @param certreq_list	list of CERTREQ payloads
 * @param cert_items	our certificates
 * @param ca_items	trusted CA's
 * @param cert_lookup_supported	does peer support HASH&URL certs
 * @param session	pointer to current session
 *
 * \return List of certificates that will be directly transformed into
 * CERT payloads. Also, with private key from first of them, auth_data
 * will be signed.
 *
 * There are three scenarios when finding CERTs to send to peer. 
 * Either we have received:
 *	1. CERTREQ with Unsupported Certificate Encoding;
 *		we will send all valid certificates. [pki4ipsec draft]
 *	2. CERTREQ with Empty CA field; we will send all valid certificates of
 *		the requested type we posses.
 *	3. CERTREQ with non-Empty CA field; we will search if we have
 *		certificates that we can validate with CA certificates that are
 *		matched to CA-hashes received in the CERTREQ. 
 */
GSList *cert_find_appropriate_certs(GSList *certreq_list, GSList *cert_items,
				    GSList *ca_items, 
				    gboolean cert_lookup_supported, 
				    gpointer session)
{
	struct certreq *certreq;
	GSList *appr_certs = NULL, *t_appr_certs = NULL;
	GSList *appr_certs_tmp = NULL, *trusted_cas = NULL;
	GSList *t_ca_items, *t_cert_items;

	LOG_FUNC_START(1);

	if (cert_items == NULL) {
		LOG_DEBUG("Have no certificates!");
		goto out;
	}

	if ((t_ca_items = ca_items) == NULL) {
		LOG_DEBUG("Empty CA list!");
		goto out;
	}

	if (certreq_list == NULL) {
		LOG_DEBUG("Empty CERTREQ list!");
		goto out;
	}

	//ca_items = config_find_ca_by_id(t_ca_items, config_ikesa->id);

	/**
	 * certreq_list is actually the list of the CERTREQ(s). Each item
	 * of the certreq_list has the concatenated list of the SHA-1 hashes
	 * of the peer CA's public keys stored in the hash_list.
	 */
	while (certreq_list != NULL) {

		certreq = (struct certreq *)certreq_list->data;
		if (certreq == NULL) {
			LOG_DEBUG("No data in CERTREQ!");
			goto out;
		}

		/**
		 * Upon receipt of an unsupported CERTREQ a host will respond
		 * with all valid certificates it posesses [pki4ipsec draft].
		 */
		if ((certreq->encoding != IKEV2_CERT_X509_SIGNATURE)
			&& (certreq->encoding != IKEV2_CERT_HASH_AND_URL)) {

			LOG_DEBUG("Received CERTREQ with unsupported cert "
				"encoding - sending all valid certificates");
			t_cert_items = cert_items;
			while (t_cert_items) {
				t_appr_certs = g_slist_append(
						t_appr_certs, 
						t_cert_items->data);
				t_cert_items = t_cert_items->next;
			}

		} else {

			/**
			 * Check if we have received Notify 
			 * HTTP_CERT_LOOKUP_SUPPORTED.
			 */
			if (cert_lookup_supported) {
				LOG_DEBUG("Finding certs of "
					"HASH_AND_URL type");
				t_appr_certs = cert_find_certs_by_type(
						IKEV2_CERT_HASH_AND_URL,
						cert_items);
			}

			if (t_appr_certs == NULL)
				t_appr_certs = cert_find_certs_by_type(
						certreq->encoding, 
						cert_items);

			if (t_appr_certs == NULL) {
				LOG_DEBUG("No certificates that match "
					"CERTREQ certificate encoding!");
			}
		}

		trusted_cas = crypto_check_hash_list(certreq, t_ca_items);

		if (trusted_cas == NULL && certreq->hash_list_len != 0) {
		 	LOG_DEBUG("No CA's matched to CERTREQ!");
			appr_certs = NULL;
			goto out;
		}

		if (certreq->hash_list_len != 0) {

			LOG_DEBUG("Building cert list according to "
				"trusted CA items");
			appr_certs_tmp = cert_prepare_cert_item_verify(
					t_appr_certs,
					trusted_cas,
					session);

		} else {

			/* certreq has 0-length hash list (this is used for
			 * interoperability testing): send all certificates
			 */
			LOG_DEBUG("Building cert list according to "
				"all CA items");
			appr_certs_tmp = cert_prepare_cert_item_verify(
					t_appr_certs,
					t_ca_items,
					session);
		}

		if (appr_certs != NULL) {
			appr_certs = g_slist_concat(appr_certs, 
					appr_certs_tmp);
		} else {
			appr_certs = appr_certs_tmp;
		}

		certreq_list = certreq_list->next;
	}

out:
	LOG_FUNC_END(1);

	return appr_certs;
}


/**
 * Returns sublist of CAs that are used in given session
 *
 * @param ca_list	list of input CAs
 * @param session	pointer to session
 *
 * \return	sublist of CAs that were used for certificate verification
 * \return	in given session
 */
GSList *cert_find_used_cas(GSList *ca_list, gpointer session)
{
	GSList *used_cas = NULL;
	GSList *session_certs;
	struct ca_item *ca_item;
	struct session_certificate *session_cert;

	LOG_FUNC_START(1);

	while (ca_list) {

		ca_item = ca_list->data;
		session_certs = ca_item->session_certs;

		while (session_certs) {

			session_cert = session_certs->data;
			if (session_cert->session == session) {
				used_cas = g_slist_append(used_cas, ca_item);
				break; /* next ca_item */
			}
			session_certs = session_certs->next;
		}

		ca_list = ca_list->next;
	}

	LOG_FUNC_END(1);

	return used_cas;
}


/**
 * Re-verify certs signed by given CA used in given session
 *
 * @param ca_item	CA used in session
 * @param session	pointer to session
 *
 * \return	TRUE if all certs used in 'session' still verify with 'ca_item'
 * \return	FALSE otherwise
 *
 * Called from 'sm.c' after download of new CRL associated with CA
 */
gboolean cert_verify_ca(struct ca_item *ca_item, gpointer session)
{
	struct cert *cert;
	struct session_certificate *session_cert;
	GSList *session_certs;
	gboolean retval = FALSE;

	LOG_FUNC_START(1);

	if (!ca_item)
		goto out;

	session_certs = ca_item->session_certs;
	for (; session_certs; session_certs = session_certs->next) {

		session_cert = session_certs->data;
		if (session_cert->session != session)
			continue;
		cert = (struct cert *) session_cert->certificate;

		if (cert_verify(ca_item->file, cert->certificate,
				cert->certificate_len,
				ca_item->crl_file)) {
			retval = FALSE;
			goto out;
		}
	}

	retval = TRUE;

out:
	LOG_FUNC_END(1);

	return retval;
}


/**
 * Load CRL from file into X509_CRL object
 *
 * @param infile	CRL file
 *
 * \return CLR object if OK, NULL if operation failed
 */
X509_CRL *cert_load_crl(const char *infile)
{
	X509_CRL *crl = NULL;
	BIO *in = NULL;

	LOG_FUNC_START(1);

	in = BIO_new(BIO_s_file());
	if (in == NULL) {
		LOG_ERROR("Can't create BIO object");
		goto out;
	}

	if (BIO_read_filename(in,infile) <= 0) {
		LOG_ERROR("Can't load BIO object from file");
		goto out;
	}

	/* try to read CRL in either PEM or DER format */
	if ((crl = PEM_read_bio_X509_CRL(in,NULL,NULL,NULL)) == NULL &&
		(crl = d2i_X509_CRL_bio(in,NULL)) == NULL ) {
		LOG_ERROR("Unable to load CRL!");
		goto out;
	}

out:
	BIO_free(in);
	LOG_FUNC_END(1);
	return crl;
}

/**
 * Check if CRL is still valid (current time < nextUpdate)
 *
 * @param crl_file	CRL file
 *
 * \return	1 if OK, 0 if expired or if CRL not present,
 * \return	-1 if nextUpdate field not present
 */
int cert_validate_crl(const char *crl_file)
{
	int retval = 0;
	X509_CRL *crl = NULL;

	LOG_FUNC_START(1);

	if((crl = cert_load_crl(crl_file)) == NULL) {
		retval = 0;
		goto out;
	}

	/*
	* Check if there is nextUpdate field, then check if it is not overdue
	*/
	if (X509_CRL_get_nextUpdate(crl))
		if(X509_cmp_current_time(crl->crl->nextUpdate) < 0)
			retval = 0;
		else
			retval = 1;
	else
		retval = -1;

out:
	X509_CRL_free(crl);
	LOG_FUNC_END(1);
	return retval;
}

/**
 * Returns CRL nextUpdate field (as new object)
 *
 * @param crl_file	CRL file
 *
 * \return	CRL nextUpdate
 * \return	NULL if nextUpdate field not present
 */
ASN1_TIME *cert_get_crl_update(const char *crl_file)
{
	ASN1_TIME *next_update = NULL;
	X509_CRL *crl = NULL;

	LOG_FUNC_START(1);

	if((crl = cert_load_crl(crl_file)) != NULL) {

		/*
		 * Check if there is nextUpdate field
		 */
		if (X509_CRL_get_nextUpdate(crl))
			next_update =
				M_ASN1_TIME_dup(X509_CRL_get_nextUpdate(crl));

		X509_CRL_free(crl);
	}

	LOG_FUNC_END(1);

	return next_update;
}

/*
 * Returns the number of days since 1.1.1. to the date in ASN1_TIME structure
 * 
 * @param t	ASN1_TIME date & time
 *
 * \return	number of days for ASN1_TIME date value
 * \return	-1 if error
 *
 * Internal: called from cert_asn1_time_diff_seconds()
 * Update: Ignores centuries data (assumes year>=2000)
 */
int cert_asn1_time_to_days(ASN1_TIME *t)
{
	ASN1_TIME *gt;
	int year, month, day, days;
	int i;
	unsigned char* data;
	static int months[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

	LOG_FUNC_START(1);

	year = month = day = days = 0;
	gt = t;

	if(t->type == V_ASN1_GENERALIZEDTIME)
		data = gt->data + 2;
	else
		data = gt->data;

	/*
	 * get year, month and day from ASN1_GENERALIZEDTIME
	 */
	year = 200;
	for(i = 0; i < 2; i++)
	{	year *= 10;
		year += (data[i] - '0');
	}
	month = (data[2] - '0') * 10 + (data[3] - '0');
	day = (data[4] - '0') * 10 + (data[5] - '0');

	/*
	 * how many days before 'year', before 'month' and in current month
	 */
	days = (year - 1) * 365;
	days = days + (year - 1)/4 - (year - 1)/100 + (year - 1)/400;

	for(i = 0; i < month-1; i++)
		days += months[i];
	if(month > 2 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)))
		days++;

	days += day - 1;

	LOG_FUNC_END(1);
	return days;
}

/*
 * Returns the difference between two ASN1_TIME structures in seconds
 *
 * @param t1,t2	ASN1_TIME structures
 *
 * \return	difference in seconds (t1 - t2), 1 if times are same
 * \return	0 if error
 *
 * Returns the difference between two ASN1_TIME structures in seconds
 * (ASN1_UTCTIME or ASN1_GENERALIZEDTIME)
 * Internal: called from cert_sa_limit_adjust()
 */
int cert_asn1_time_diff_seconds(ASN1_TIME *t1, ASN1_TIME *t2)
{
	int days, hour_offset[3], seconds;
	int td1[3], td2[3];
	int i;

	LOG_FUNC_START(1);

	if(t1->type == V_ASN1_GENERALIZEDTIME)
		hour_offset[1] = 8;
	else
		hour_offset[1] = 6;

	if(t2->type == V_ASN1_GENERALIZEDTIME)
		hour_offset[2] = 8;
	else
		hour_offset[2] = 6;

	if( (days = cert_asn1_time_to_days(t1)) < 0)
	{	seconds = 0;
		goto out;
	}
	if( (i = cert_asn1_time_to_days(t2)) < 0)
	{	seconds = 0;
		goto out;
	}
	days -= i;

	for(i = 0; i < 3; i++)
	{	td1[i] = (t1->data[hour_offset[1] + i*2] - '0') * 10 
			+ (t1->data[hour_offset[1] + i*2 + 1] - '0');
		td2[i] = (t2->data[hour_offset[2] + i*2] - '0') * 10 
			+ (t2->data[hour_offset[2] + i*2 + 1] - '0');
	}

	seconds = (td1[0] - td2[0]) * 3600;
	seconds += (td1[1] - td2[1]) * 60;
	seconds += td1[2] - td2[2];

	seconds += days * 24 * 3600;

	if(seconds == 0)
		seconds++;

out:
	LOG_FUNC_END(1);
	return seconds;
}

/**
 * Get certificate validity (notAfter field) from a locally stored cert (file)
 *
 * @param filename	filename of cert data
 *
 * \return notAfter field as ASN1_TIME structure (NULL on error)
 *
 * Internal: called from cert_sa_limit_adjust()
 */
ASN1_TIME *cert_get_cert_validity(char *filename) 
{
	FILE *fp;
	X509 *x509;
	ASN1_TIME *not_after;

	LOG_FUNC_START(1);

	/* Open cert file */
	if ((fp = fopen(filename, "r")) == NULL) {
		LOG_ERROR("Unable to open certificate file (%s)",
				filename);
		return NULL;
	}

	/* Load cert from file */
	x509 = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);

	if (x509 == NULL) {
		LOG_ERROR("Error reading certificate file (%s)",
				filename);
		return NULL;
	}

	not_after = M_ASN1_TIME_dup(x509->cert_info->validity->notAfter);

	X509_free(x509);

	LOG_FUNC_END(1);

	return not_after;
}

/**
 * Limit SA lifetime with certificate(s) validity time
 *
 * @param sa_start	time_t of SA start
 * @param hardlimit	SA hardlimit time
 * @param cert_items	list of locally stored certs (cert_item struct)
 * @param certs	list of certs from IKE payloads (cert struct)
 *
 * \return	new hardlimit time limited to 'notAfter' or
 * \return	unchanged if comes before
 *
 * Determine if SA hardlimit time comes after the 'notAfter' field of 
 * the certificate the SA was established with. Return adjusted hardlimit
 * time (set to 'notAfter' value) if necessary.
 * Called from sm.c after SA establishment
 */
guint32 cert_sa_limit_adjust(guint32 sa_start, guint32 hardlimit, 
		GSList *cert_items, GSList *certs)
{
	ASN1_TIME *sa, *not_after;
	X509 *x509;
	guint32 new_limit;
	GSList *t_cert_items, *t_certs;
	struct cert *cert;
	struct cert_item *cert_item;
	time_t sa_limit;
	int offset;
	unsigned char *pointer;

	LOG_FUNC_START(1);

	new_limit = hardlimit;
	if ((sa = ASN1_TIME_new()) == NULL) {
		LOG_ERROR("Failed to initialize ASN1_TIME structure");
		goto out;
	}

	t_cert_items = cert_items;
	t_certs = certs;
	sa_limit = sa_start + hardlimit;

	/* 
	 * iterate over all the certificates in both cert lists
	 */
	while (t_cert_items != NULL) {
		/* certificate data in cert_item struct */
		cert_item = t_cert_items->data;
		not_after = cert_get_cert_validity(cert_item->file);

		/*
		* compare sa_limit (SA hardlimit) with 'notAfter' cert field
		* (doesnt check 'notAfter' field as cert is already 
		* verified for SA)
		*/
		LOG_DEBUG("SA_limit: %s", asctime(localtime(&sa_limit)));
		LOG_DEBUG("notAfter: %s", not_after->data);
		if (X509_cmp_time(not_after, &sa_limit) < 0) {
			LOG_DEBUG("Limiting SA hardlimit to CERT notAfter");
			offset = sa_start - time(NULL);
			X509_gmtime_adj(sa, offset);
			new_limit = cert_asn1_time_diff_seconds(not_after, sa);
			sa_limit = sa_start + new_limit;
			LOG_DEBUG("new SA_limit: %s", 
				asctime(localtime(&sa_limit)));
		}

		M_ASN1_TIME_free(not_after);
		t_cert_items = t_cert_items->next;
	}

	while (t_certs != NULL) {
		/* certificate data in cert struct */
		cert = t_certs->data;
		pointer = (unsigned char *) cert->certificate;
		x509 = d2i_X509(NULL, &pointer, 
				cert->certificate_len);
		if (!x509) {
			LOG_ERROR("Error converting certificate "
				"from binary to X509");
			goto out;
		}
		not_after = x509->cert_info->validity->notAfter;

		/* how to get SA limit in the form of ASN1_time */
		/*
		 * X509_gmtime_adj(sa, sa_start + hardlimit - time(NULL));
		 */

		/*
		* compare sa_limit (SA hardlimit) with 'notAfter' cert field
		* (doesnt check 'notAfter' field as cert is already 
		* verified for SA)
		*/
		LOG_DEBUG("SA_limit: %s", asctime(localtime(&sa_limit)));
		LOG_DEBUG("notAfter: %s", not_after->data);

		if (X509_cmp_time(not_after, &sa_limit) < 0) {
			LOG_DEBUG("Limiting SA hardlimit to CERT notAfter");
			offset = sa_start - time(NULL);
			X509_gmtime_adj(sa, offset);
			new_limit = cert_asn1_time_diff_seconds(not_after, sa);
			sa_limit = sa_start + new_limit;
			LOG_DEBUG("new SA_limit: %s", 
				asctime(localtime(&sa_limit)));
		}

		t_certs = t_certs->next;
	}

	ASN1_TIME_free(sa);

out:
	LOG_FUNC_END(1);
	return new_limit;
}

/**
 * Iterate through a ca_item list and find if any CRL's need to be downloaded
 *
 * @param dl_req	current dl_request
 * @param ca_list	list of ca_items to iterate through
 *
 * \return updated dl_request structure
 *
 * Internal: called from cert_check_auth_material() and cert_update_crl()
 */
struct dl_request *cert_update_crl_dl_request(struct dl_request *dl_req, 
		GSList *ca_list)
{
	struct ca_item *ca_item;
	struct stat crl_file_data;
	struct dl_item *dl_item;
	ASN1_TIME *asn_now;
	int status, seconds;
	time_t now, seconds_elapsed;
	gboolean download;

	LOG_FUNC_START(1);

	if ((asn_now = ASN1_TIME_new()) == NULL) {
		LOG_ERROR("Failed to initialize ASN1_TIME structure");
		goto out;
	}

	/*
	 * define 'now' in ASN1_TIME format
	 */
	X509_gmtime_adj(asn_now, 0);

	while (ca_list) {

		ca_item = ca_list->data;
		ca_list = ca_list->next;

		if (ca_item->crl_url == NULL)
			continue;

		download = FALSE;

		/*
		 * First check if CRL is stored localy
		 */
		status = stat(ca_item->crl_file, &crl_file_data);
		if (status == 0) {

			ca_item->crl_timestamp = crl_file_data.st_mtime;
			ca_item->crl_update_time = (time_t) -1;

			/*
			 * if check_interval is defined, see how old the file is
			 */
			if (ca_item->check_interval > 0) {

				now = time(NULL);
				seconds_elapsed = (now - ca_item->crl_timestamp);
				if (seconds_elapsed > (time_t)ca_item->check_interval)
					download = TRUE;

				/*
				 * Also define CRLs next update time
				 */
				ca_item->crl_update_time = 
						ca_item->crl_timestamp
						+ (time_t)ca_item->check_interval;

			} else {	/* else check if nextUpdate is overdue */

				/* get nextUpdate if not defined */
				if (ca_item->crl_next_update == NULL)
					ca_item->crl_next_update = 
						cert_get_crl_update(ca_item->crl_file);
				/* if crl_next_update STILL undefined, CRL lacks it */
				if (ca_item->crl_next_update == NULL)
					continue;

				/* finally, check if overdue */
				if (X509_cmp_current_time(
						ca_item->crl_next_update) < 0)
					download = TRUE;
				else { /* convert nextUpdate to time_t */
					seconds = cert_asn1_time_diff_seconds(
						ca_item->crl_next_update,
						asn_now);
					now = time(NULL);
					ca_item->crl_update_time = now + (time_t) seconds;
				}

			}
		} else	/* assume the file doesn't exist */
			download = TRUE;

		/* form new dl request or add new dl item to existing request */
		if (download == TRUE) {

			if (dl_req == NULL) {

				LOG_DEBUG("Forming new download request");
				dl_req = dl_request_new(1);
				dl_item = dl_req->dl_items->data;

			} else {

				LOG_DEBUG("Adding new dl_item to dl request");
				dl_item = g_malloc0(sizeof(struct dl_item));
				dl_req->dl_items = g_slist_append(
							dl_req->dl_items,
							dl_item);
				dl_req->items++;
			}

			/* fill the dl_item with data */
			dl_item->memory = FALSE;
			dl_item->url = g_strdup(ca_item->crl_url);
			dl_item->dl_file = g_strdup(ca_item->crl_file);
			LOG_DEBUG("URL: %s", dl_item->url);
			LOG_DEBUG("File: %s", dl_item->dl_file);
		}
	} /* while */

out:
	ASN1_TIME_free(asn_now);
	LOG_FUNC_END(1);

	return dl_req;
}

/**
 * Find nearest time_t of CRL updates
 *
 * @param ca_list	list of ca_items to iterate through
 * @param current minimal time_t value (if (time_t)-1 then ignore)
 *
 * \return minimal time_t or (time_t) -1 if no update time defined
 *
 * Internal: called from cert_check_auth_material() and cert_update_crl()
 */
time_t cert_find_nearest_time(GSList *ca_list, time_t current)
{
	struct ca_item *ca_item;
	time_t next_crl_update;

	LOG_FUNC_START(1);

	next_crl_update = current;
	while (ca_list)
	{	ca_item = ca_list->data;
		ca_list = ca_list->next;
		if (ca_item->crl_update_time > 0)
			if (next_crl_update == (time_t) -1 
				|| ca_item->crl_update_time < next_crl_update)
				next_crl_update = ca_item->crl_update_time;
	}

	LOG_FUNC_END(1);

	return next_crl_update;
}

/**
 * Check if any auth material needs to be downloaded prior to verification
 *
 * @param general_ca_list	list of CA items in general conf. section
 * @param peer_ca_list		list of CA items in peer conf. section
 * @param session		IKE session that tries to authenticate
 * @param sm_msg		message that the session received
 * @param next_crl_update	the nearest time when any of CLRs will
 * 				need update
 *
 * \return 1 if no download needed, 0 if anything needs to be downloaded
 *
 * Called from sm.c when compiling CERT payload or verifying peer certificates
 * The outcomes:
 *	- something needs to be downloaded; form a dl_request and return
 *	(in this case the session must hold and reenter the same state after a 
 *	response is received from the download thread)
 *	- all material present; return appropriate
 *	(in this case also define time_t variable in session structue that 
 *	determines the closest future time at which any of the CRLs will need 
 *	to be updated!)
 *
 * Parameters 'session' and 'sm_msg' are needed to restore session context
 * after the download is finished.
 */
int cert_check_auth_material(GSList *general_ca_list, GSList *peer_ca_list, 
		gpointer session, gpointer sm_msg, time_t *next_crl_update)
{
	struct dl_request *new_dl_req;
	int retval = 1;

	LOG_FUNC_START(1);

	new_dl_req = NULL;

	/* form a dl_request with data from both CA lists */
	new_dl_req = 
		cert_update_crl_dl_request(new_dl_req, general_ca_list);
	new_dl_req = 
		cert_update_crl_dl_request(new_dl_req, peer_ca_list);

	/* TODO: check also if any certificate needs downloading */

	/* designate and send dl_request */
	if (new_dl_req != NULL) {
		LOG_DEBUG("Sending a download request to dl queue");
		new_dl_req->session = session;
		new_dl_req->sm_msg = sm_msg;
		g_async_queue_push(cert_data->download_queue, 
			(gpointer) new_dl_req);
		retval = 0;

	/* determine first occurence of any CRL update */
	} else {
		*next_crl_update = 
			cert_find_nearest_time(peer_ca_list, *next_crl_update);
		*next_crl_update = 
			cert_find_nearest_time(general_ca_list, 
			*next_crl_update);
	}

	LOG_FUNC_END(1);
	return retval;
}

/**
 * Check if a CRL of any CA the session is using needs updating
 * 
 * @param general_ca_list	list of CA items in general conf. section
 * @param peer_ca_list	list of CA items in peer conf. section
 * @param session	IKE session that tries to update CRL
 * @param next_crl_update	the nearest time when any of CLRs will update
 *
 * \return 1 if no download needed, 0 if anything needs to be downloaded
 *
 * This function checks if a CRL of any CA the session is using needs updating
 * - if CRL needs updating, initiate download
 * (in this case the session must hold and reenter the same state after a 
 * response is received from the download thread)
 * - if download is finished, return appropriate
 * Called from sm.c when a session needs to update CRL data
 */
int cert_update_crl(GSList *general_ca_list, GSList *peer_ca_list, 
		    gpointer session, gpointer sm_msg, time_t *next_crl_update)
{
	struct dl_request *new_dl_req;
	int retval = 1;

	LOG_FUNC_START(1);

	new_dl_req = NULL;

	/* form a dl_request with data from both CA lists */
	new_dl_req = 
		cert_update_crl_dl_request(new_dl_req, general_ca_list);
	new_dl_req = 
		cert_update_crl_dl_request(new_dl_req, peer_ca_list);

	/* designate and send dl_request */
	if (new_dl_req != NULL) {
		new_dl_req->session = session;
		new_dl_req->sm_msg = sm_msg;
		g_async_queue_push(cert_data->download_queue, 
			(gpointer) new_dl_req);
		retval = 0;

	/* determine first occurence of any CRL update */
	} else {
		*next_crl_update = 
			cert_find_nearest_time(peer_ca_list, (time_t) -1);
		*next_crl_update = 
			cert_find_nearest_time(general_ca_list, 
			*next_crl_update);
	}

	LOG_FUNC_END(1);
	return retval;
}


/*******************************************************************************
 * DOWNLOAD FUNCTIONS
 ******************************************************************************/

/**
 * Process a single dl_request
 *
 * @param dl_req	new dl_request
 * @param active_downloads_data	pointer to active_downloads_data 
 * @param multi_handle	CURL multi handle to add single handles to
 *
 * \return	the number of successfuly initiated handles (new downloads)
 *
 * Identify all dl items in this request and define internal download
 * data for each dl item;
 * Also find if any existing dl_request item matches the URL and filename
 * of any item in this request (that is if there are multiple requests for the
 * same document); if so, no new dl_data will be created, but
 * a pointer to this new request will be placed in existing dl_item's related
 * requests list so when it finishes it can decrease the number of dl_items
 * in this dl_request;
 * The function may add to the internal active_downloads_data list!
 * Internal:
 */
int cert_process_dl_request(struct dl_request *dl_req, 
		GSList **active_downloads_data,
		CURLM *multi_handle)
{
	struct dl_item *dl_item;
	struct dl_data *dl_data, *existing_dl_data;
	GSList *dl_items, *active_downloads;
	int new_downloads = 0;
	gboolean related_request;

	LOG_FUNC_START(1);

	/*
	 * We will count single downloads
	 */
	dl_req->items = 0;

	/*
	 * Assume, then change if any fails
	 */
	dl_req->success = TRUE;
	dl_items = dl_req->dl_items;
	active_downloads = *active_downloads_data;

	/*
	 * Initialize download data for each item in the request
	 */
	while(dl_items) {

		dl_item = dl_items->data;
		dl_items = dl_items->next;

		LOG_DEBUG("Processing dl_item:");
		LOG_DEBUG("URL = %s\n", dl_item->url);
		LOG_DEBUG("file = %s\n", dl_item->dl_file);

		/*
		 * First find if there is an active download
		 * of the same document
		 */
		related_request = FALSE;
		while(active_downloads) {

			LOG_DEBUG("Checking existing downloads...");
			existing_dl_data = active_downloads->data;
			active_downloads = active_downloads->next;

			/*
			 * Check only file downloads
			 */
			if (existing_dl_data->item->memory)
				continue;

			/* check if URLs AND local files are the same */
			if (g_ascii_strcasecmp(dl_item->url, 
					existing_dl_data->item->url) == 0 && 
					g_ascii_strcasecmp(dl_item->dl_file, 
					existing_dl_data->item->dl_file) == 0) {

				/*
				 * Add pointer to the new request
				 * and continue outer loop
				 */
				LOG_DEBUG("This is a related (existing) dl_item!");
				related_request = TRUE;

				/*
				 * Does this item belong to the same dl
				 * request?
				 */
				if (existing_dl_data->request == dl_req)
					break;

				existing_dl_data->related_requests = 
					g_slist_append(
						existing_dl_data->related_requests,
						dl_req);

				dl_req->items++;
				break;
			}
		}
		if (related_request == TRUE)
			continue;

		/* this is a new unique dl item, so process it */
		LOG_DEBUG("This is a new dl_item");
		dl_data = g_malloc0(sizeof(struct dl_data));
		*active_downloads_data = 
				g_slist_append(*active_downloads_data, dl_data);
		active_downloads = *active_downloads_data;

		/*
		 * which dl request and dl_item this dl_data object belongs to
		 */
		dl_data->request = dl_req;
		dl_data->item = dl_item;

		if (cert_dl_init_handle(dl_item, dl_data)) {
			/*
			 * This download failed to init, but will try others
			 */
			LOG_ERROR("Cant initialize download handle!");
			dl_data->success = FALSE;
			dl_data->active = FALSE;

			/*
			 * One item fails, whole request failed
			 */
			dl_data->request->success = FALSE;

		} else {

			dl_req->items++;
			curl_easy_setopt(dl_data->handle, CURLOPT_TIMEOUT, 
					dl_req->timeout);
			/*
			 * include only initialized items in multi handle
			 */
			curl_multi_add_handle(multi_handle, dl_data->handle);
			new_downloads++;
		}
	}

	LOG_FUNC_END(1);

	return new_downloads;
}

/**
 * Perform internal logic after a single download has finished
 *
 * @param msg			CURL message after finished download
 * @param active_downloads_data	Internal list of download data
 *
 * \return	updated active_downloads_data list
 *
 * After a single download is finished, do the following:
 * - identify which dl request this dl belongs to and decrease num. of items
 * - decrease num. of items of any related download requests (if any)
 * - identify which dl item this download belongs to and mark succes code
 * - store contents in memory if memory download
 * - remove internal dl data from active_downloads_data list
 * Internal:
 */
GSList *cert_dl_finished(CURLMsg *msg, GSList *active_downloads_data)
{
	struct dl_data *dl_data;
	struct dl_request *dl_req;
	GSList *related_requests, *t_dl_data;

	LOG_FUNC_START(1);

	/*
	 * find the dl_data struct this handle matches 
	 */
	t_dl_data = active_downloads_data;
	do {
		dl_data = t_dl_data->data;
		t_dl_data = t_dl_data->next;
	} while (dl_data->handle != msg->easy_handle);

	dl_data->request->items--;

	/* 
	 * if download failed, the whole request failes! 
	 */
	if (msg->data.result != CURLE_OK) {
		dl_data->request->success = FALSE;
	}

	/* 
	 * also decrease number of dl items for any subsequent requests that 
	 * requested the same document; and copy the success code (only if 
	 * related request still holds TRUE)
	 */
	related_requests = dl_data->related_requests;
	while (related_requests) {
		dl_req = related_requests->data;
		dl_req->items--;
		if (dl_req->success == TRUE)
			dl_req->success = dl_data->request->success;
		related_requests = related_requests->next;
	}

	/* 
	 * arrange destination pointer and data size 
	 */
	if (dl_data->request->success == TRUE) {
		if (dl_data->item->memory) {
			dl_data->data[dl_data->size] = 0;
			*(dl_data->item->size) = dl_data->size;
			*(dl_data->item->dl_mem) = (void *) dl_data->data;
		} else
			fclose(dl_data->fp);
	}

	/* 
	 * free this dl_data 
	 */
	related_requests = dl_data->related_requests;
	while (related_requests)
		related_requests = g_slist_remove(related_requests,
				related_requests->data);

	g_free(dl_data);
	active_downloads_data = g_slist_remove(active_downloads_data, dl_data);

	LOG_FUNC_END(1);
	return active_downloads_data;
}

/**
 * Main download thread function - waits for and processes download requests
 *
 * @param data	pointer to cert_data object
 *
 * Requests must be formed in dl_request structure and sent in the cert module
 * download queue;
 * Responses are sent to sm module through the main queue 
 * in a dl_response struct;
 */
gpointer cert_dl_thread(gpointer data)
{
	struct cert_data *cert_data;
	struct dl_request *dl_req;
	struct dl_data *dl_data;
	GAsyncQueue *dl_queue;
	GTimeVal gtime;
	GSList *dl_reqs, *active_downloads_data, *t_dl_reqs;
	CURLM *multi_handle;
	CURLMsg *msg;
	int active_downloads, new_active, msgs, new_downloads;

	LOG_FUNC_START(1);

	cert_data = data;
	dl_queue = cert_data->download_queue;
	/* 
	 * list of received dl requests 
	 */
	dl_reqs = NULL;
	/* 
	 * list of internal download data 
	 */
	active_downloads_data = NULL;

	if ((multi_handle = curl_multi_init()) == NULL) {
		LOG_ERROR("Cant initialize download handle!");
		goto out;
	}

	while (TRUE) {

		dl_req = (struct dl_request *)g_async_queue_pop(dl_queue);

		/*
		 * received terminate message
		 */
		if (GPOINTER_TO_UINT(dl_req) == 1)
			goto out;
		
		if (dl_req->items == 0) {
			g_free(dl_req);
			continue;
		}
		
		LOG_DEBUG("Received new download request!");
		
		active_downloads = 0;

		/* 
		 * Process this dl_request.
		 * Here only a single request exists, but we may receive more
		 * during the download of this one
		 */
		if (dl_req->dl_items == NULL) {
			LOG_BUG("Invalid download request!");
			dl_request_free(dl_req);
			continue;
		}

		active_downloads = cert_process_dl_request(dl_req, 
				&active_downloads_data, multi_handle);

		if (active_downloads == 0) {
			/*
			 * Valid request but with no successfuly initiated
			 * handles; still have to send a (negative) response
			 */
			cert_send_dl_response(dl_req, cert_data->main_queue);

			/*
			 * Remove this dl_request
			 */
			dl_request_free(dl_req);
		} else
 			dl_reqs = g_slist_append(dl_reqs, dl_req);

		/* 
		 * Perform download in this loop 
		 */
		while (active_downloads) {

			LOG_DEBUG("Downloading...");

			while (CURLM_CALL_MULTI_PERFORM ==
					curl_multi_perform(multi_handle,
							&new_active));

			LOG_DEBUG("Checking download results...");

			/*
			 * If there are finished downloads, collect data
			 */
			if (new_active != active_downloads) {

				LOG_DEBUG("Downloads finished: %d",
						active_downloads-new_active);

				while ((msg = curl_multi_info_read(
						multi_handle, &msgs)) != NULL) {
					if(msg->msg == CURLMSG_DONE) {

						/*
						 * Identify and store data
						 * about finished download
						 */
						active_downloads_data = 
							cert_dl_finished(msg, 
							active_downloads_data);

						/* 
						 * Remove single download handle
						 */
						curl_multi_remove_handle(
							multi_handle, 
							msg->easy_handle);
						curl_easy_cleanup(
							msg->easy_handle);
					} else
						LOG_BUG("Undefined libcurl message");
				}
			}

			active_downloads = new_active;

			LOG_DEBUG("Checking finished requests...");

			/* 
			 * If all download items for a request are finished,
			 * send ack 
			 */
			t_dl_reqs = dl_reqs;
			do {
				dl_req = t_dl_reqs->data;
				t_dl_reqs = t_dl_reqs->next;

				/*
				 * All active downloads finished
				 */
				if (dl_req->items == 0) {

					cert_send_dl_response(dl_req,
						cert_data->main_queue);
					/*
					 * Mark this dl_request as processed
					 */
					dl_req->items = -1;
					dl_request_free(dl_req);
					dl_reqs = g_slist_remove(dl_reqs, dl_req);
				}

			} while (t_dl_reqs != NULL);

			/*
			 * if there are no downloads, break download loop
			 */
			if (active_downloads == 0)
				break;

			/* else, wait max 50 ms on the download queue */
			g_get_current_time(&gtime);
			g_time_val_add(&gtime, 50000);
			dl_req = (struct dl_request *) 
				g_async_queue_timed_pop(dl_queue, &gtime);

			/* 
			 * If a new request arrived, process it;
			 * otherwise continue
			 */
			if (dl_req == NULL)
				continue;

			if (GPOINTER_TO_UINT(dl_req) == 1) /* abandon ship... */
				goto out;

			if (dl_req->dl_items == NULL) {
				LOG_BUG("Invalid download request!");
				dl_request_free(dl_req);
				continue;
			}

			dl_reqs = g_slist_append(dl_reqs, dl_req);

			new_downloads = cert_process_dl_request(dl_req, 
				&active_downloads_data, multi_handle);

			active_downloads += new_downloads;
		} /* end of download loop */
		/* 
		 * No downloads or download requests left, 
		 * OK to continue main loop 
		 */
	}

out:
	/*
	 * clean all memory items
	 */
	while (active_downloads_data != NULL) {
		dl_data = active_downloads_data->data;
		curl_multi_remove_handle(multi_handle, dl_data->handle);
		curl_easy_cleanup(dl_data->handle);
		g_free(dl_data);
		active_downloads_data = g_slist_remove(active_downloads_data,
						dl_data);
	}

	curl_multi_cleanup(multi_handle);
	while (dl_reqs != NULL) {
		dl_request_free(dl_reqs->data);
		dl_reqs = g_slist_remove(dl_reqs, dl_reqs->data);
	}

	LOG_FUNC_END(1);

	return NULL;
}

/**
 * Send download response to (main) queue
 *
 * @param dl_req	processed dl request
 * @param queue	pointer to application main queue
 *
 * Internal: called by the download thread after all downloads of a request 
 * are finished
 */
void cert_send_dl_response(struct dl_request *dl_req, GAsyncQueue *queue)
{
	struct cert_msg *cert_msg;

	LOG_FUNC_START(1);

	cert_msg = cert_msg_alloc();

	cert_msg->session = dl_req->session;
	cert_msg->success = dl_req->success;
	cert_msg->sm_msg  = dl_req->sm_msg;

	LOG_DEBUG("Sending download response (CERT_MSG)");

	g_async_queue_push(cert_data->main_queue, (gpointer) cert_msg);

	LOG_FUNC_END(1);
	return;
}

/**
 * Download callback function needed by libcurl
 */
size_t writecallback_cb(void *data, size_t size, size_t nmemb, void *ptr)
{
	int retval = -1;
	gpointer mem;
	size_t datasize = size * nmemb;
	struct dl_data *downloaded = (struct dl_data *) ptr;

	LOG_FUNC_START(1);

	mem = g_try_realloc(downloaded->data, downloaded->size + datasize + 1);

	if (mem != NULL) {

		downloaded->data = (char *) mem;	/* if the data was moved */
		memcpy(&downloaded->data[downloaded->size], data, datasize);
		downloaded->size += datasize;
		retval = (int) datasize;

	} else {

		/*
		 * If memory allocation fails, tell libcurl
		 * so it can abort
		 */
		retval = -1;
		g_free(downloaded->data);
		LOG_ERROR("libcurl callback: Can't allocate memory!");
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Initialize single download handle with data from dl_item 
 * 
 * @param dl_item	struct with download request item
 * @param dl_data	struct with internal download data
 *
 * \return	0 if OK, -1 if error
 *
 * Initialize single download handle with data from dl_item and
 * fill the internal dl_data structure.
 * Internal: called by download thread function when processing a request
 */
int cert_dl_init_handle(struct dl_item *dl_item, struct dl_data *dl_data)
{
	CURL *handle;
	int retval;
	FILE *fp;

	LOG_FUNC_START(1);

	handle = curl_easy_init();
	if (handle == NULL) {
		retval = -1;
		goto out;
	}

	curl_easy_setopt(handle, CURLOPT_NOSIGNAL, 1);

	curl_easy_setopt(handle, CURLOPT_URL, dl_item->url);

	curl_easy_setopt(handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	if (dl_item->memory == TRUE) {
		dl_data->data = NULL;
		dl_data->size = 0;

		/* define callback function  */
		curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writecallback_cb);

		/* define pointer for downloaded data */
		curl_easy_setopt(handle, CURLOPT_WRITEDATA, (void *) dl_data);

	} else {	/* define file pointer */

		if ((fp = fopen(dl_item->dl_file,"w+")) == NULL) {
			retval = -1;
			goto out;
		}

		curl_easy_setopt(handle, CURLOPT_WRITEDATA, fp);
		dl_data->fp = fp;
	}

	dl_data->handle = (void *) handle;
	dl_data->active = TRUE;

	retval = 0;

out:
	if (retval == -1 && handle)
		curl_easy_cleanup(handle);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Initialize curl 
 * \return 0 if OK, -1 otherwise
 */
int cert_libcurl_init(void)
{
	int retval = 0;
	LOG_FUNC_START(1);

	if (curl_global_init(CURL_GLOBAL_ALL))
		retval = -1;

	LOG_FUNC_END(1);

	return retval;
}

void cert_libcurl_unload(void)
{
	LOG_FUNC_START(1);

	curl_global_cleanup();

	LOG_FUNC_END(1);
}

/**
 * Download from URL into memory
 *
 * @param url
 * @param ptr	address of pointer to memory buffer
 *
 * \return size of downloaded data or -1 if operation failed
 */
int cert_dltomem(char *url, void *ptr)
{
	CURL *curl_handle;
	CURLcode opcode;
	void **ptrptr;
	int retval = 0;

	struct dl_data downloaded;

	LOG_FUNC_START(1);

	downloaded.data = NULL;
	downloaded.size = 0;

	curl_handle = curl_easy_init();
	if (curl_handle == NULL) {
		LOG_ERROR("Error initializing curl handle");
		retval = -1;
		goto out;
	}

	curl_easy_setopt(curl_handle, CURLOPT_URL, url);

	/* define callback function  */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, writecallback_cb);

	/* define pointer for downloaded data */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) &downloaded);

	/*
	 * Some servers don't like requests that are made without
	 * a user-agent field, so we provide one
	 */
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	opcode = curl_easy_perform(curl_handle);
	curl_easy_cleanup(curl_handle);

	if (opcode != 0) {	/* if operation failed */
		LOG_ERROR("Error performing libcurl operation");
		retval = -1;
		goto out;
	}

	/*
	 * Zero termination
	 */
	downloaded.data[downloaded.size] = 0;
	ptrptr = (void **) ptr;

	/*
	 * Point to downloaded data
	 */
	*ptrptr = (void *) downloaded.data;
	retval = downloaded.size;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Download from URL into file
 *
 * @param url
 * @param fp	file pointer
 * @param timeout	maximum download duration
 *
 * \return 0 if OK, -1 if operation failed or timeouted
 */
int cert_dltofile(char *url, FILE *fp, long timeout)
{
	CURL *curl_handle;
	CURLcode opcode;
	int retval = 0;

	LOG_FUNC_START(1);

	curl_handle = curl_easy_init();
	if (curl_handle == NULL) {
		LOG_ERROR("Error initializing curl handle");
		retval = -1;
		goto out;
	}

	if (timeout > 0)
		curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, timeout);

	curl_easy_setopt(curl_handle, CURLOPT_NOSIGNAL, 1);

	curl_easy_setopt(curl_handle, CURLOPT_URL, url);

	/* define file pointer */
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, fp);

	/* some servers don't like requests that are made without a user-agent
	   field, so we provide one */
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	opcode = curl_easy_perform(curl_handle);
	curl_easy_cleanup(curl_handle);

	if (opcode != 0) {	/* if operation failed */
		LOG_ERROR("Error performing libcurl operation");
		retval = -1;
		goto out;
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/*******************************************************************************
 * BASIC FUNCTIONS FOR INITIALIZING CERT SUBSYSTEM
 ******************************************************************************/

void cert_unload()
{
	LOG_FUNC_START(1);

	/*
	 * Terminate download thread and queue
	 */
	if (cert_data->download_thread) {
		if (cert_data->download_queue)
			g_async_queue_push(cert_data->download_queue,
					GUINT_TO_POINTER(1));

		if (cert_data->download_thread)
			g_thread_join(cert_data->download_thread);
	}

	if (cert_data->download_queue)
		g_async_queue_unref(cert_data->download_queue);

	/* should we dereference main_queue? message_unload() does not... */
	g_async_queue_unref(cert_data->main_queue);

	cert_libcurl_unload();
	g_free(cert_data);

	LOG_FUNC_END(1);
}

int cert_init(GAsyncQueue *main_queue)
{
	gint retval = -1;
	struct dl_request *dl_req;

	LOG_FUNC_START(1);

	dl_req = g_malloc0(sizeof(struct dl_request));
	dl_req->items = 0;

	if (cert_libcurl_init() != 0) {
		LOG_ERROR("Error initializing libcurl!");
		goto out;
	}

	cert_data = g_malloc0(sizeof(struct cert_data));

	/*
	 * Reference the main queue to sm module
	 */
	g_async_queue_ref(main_queue);
	cert_data->main_queue = main_queue;

	/*
	 * Create download queue and thread
	 */
	cert_data->download_queue = g_async_queue_new();
	cert_data->download_thread = 
			g_thread_create(cert_dl_thread, cert_data, TRUE, NULL);

	g_async_queue_push(cert_data->download_queue, (gpointer)dl_req);

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}
