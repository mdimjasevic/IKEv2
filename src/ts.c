/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __TS_C

#define LOGGERNAME	"ts"

/**
 * Maximal expected ASCII representation of TS. Used for
 * dumping TSs into a log file.
 */
#define TS_MAX_LEN	512

/**
 * \file ts.c
 *
 */

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <netdb.h>

#include <openssl/rand.h>
#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "ts.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/

/**
 * Allocate new ts structure
 */
struct ts *ts_new()
{
	return g_malloc0(sizeof(struct ts));
}

/**
 * Free ts structure
 */
void ts_free(struct ts *ts)
{
	LOG_FUNC_START(2);

	if (ts) {
		if (ts->saddr)
			netaddr_free(ts->saddr);

		if (ts->eaddr)
			netaddr_free(ts->eaddr);

		g_free (ts);
	}

	LOG_FUNC_END(2);
}

/**
 * Duplicate traffic selector
 */
struct ts *ts_dup(struct ts *ts)
{
	struct ts *tsd;

	tsd = ts_new();

	tsd->ip_proto_id = ts->ip_proto_id;
	tsd->saddr = netaddr_dup(ts->saddr);
	tsd->eaddr = netaddr_dup(ts->eaddr);

	return tsd;
}

/**
 * Duplicate a list of traffic selectors
 */
GSList *ts_list_dup(GSList *list)
{
	GSList *newlist = NULL;

	while (list) {
		newlist = g_slist_append(newlist, ts_dup(list->data));
		list = list->next;
	}

	return newlist;
}

/**
 * Free a list of ts structures
 */
void ts_list_free(GSList *ts)
{
	LOG_FUNC_START(2);
	LOG_TRACE("ts = %p", ts);

	while (ts) {
		ts_free(ts->data);
		ts = g_slist_remove(ts, ts->data);
	}

	LOG_FUNC_END(2);
}

/**
 * Create new ts structure and populate it with a data
 */
struct ts *ts_new_and_set(guint8 protocol, guint16 sport, guint16 dport,
			struct netaddr *saddr, struct netaddr *eaddr)
{
	struct ts *ts;

	if ((ts = ts_new()) == NULL)
		return NULL;

	ts->ip_proto_id = protocol;

	ts->saddr = netaddr_dup(saddr);
	ts->eaddr = netaddr_dup(eaddr);

	netaddr_set_port(ts->saddr, sport);
	netaddr_set_port(ts->eaddr, dport);

	return ts;
}

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/

void ts_set_saddr(struct ts *ts, struct netaddr *addr)
{
	if (ts->saddr)
		netaddr_free(ts->saddr);

	ts->saddr = netaddr_dup(addr);
}

void ts_set_saddr_from_inaddr(struct ts *ts, struct in_addr *addr)
{
	if (ts->saddr)
		netaddr_free(ts->saddr);

	ts->saddr = netaddr_new_from_inaddr(addr);
}

void ts_set_saddr_from_netaddr(struct ts *ts, struct in_addr *addr, int netmask)
{
	guint32 a4;

	if (ts->saddr)
		netaddr_free(ts->saddr);

	if (netmask) {
		a4 = ntohl(*((guint32 *)addr));
		a4 &= ~0 << (32 - netmask);
		a4 = htonl(a4);
	} else
		a4 = 0;

	ts->saddr = netaddr_new_from_inaddr((struct in_addr *)&a4);
}

void ts_set_saddr_from_in6addr(struct ts *ts, struct in6_addr *addr)
{
	if (ts->saddr)
		netaddr_free(ts->saddr);

	ts->saddr = netaddr_new_from_in6addr(addr);
}

void ts_set_saddr_from_netaddr6(struct ts *ts, struct in6_addr *addr,
		int netaddr)
{
	guint i;
	gint mask;
	struct in6_addr in6_addr;
	guint8 s6_addr1[16];

	mask = i = 0;

	for (i=0; i<16; i++)
		s6_addr1[15-i] = addr->s6_addr[i];

	mask = 128 - netaddr;
	for (i=0; i<16; i++) {
                if (mask > 0){

			if ((mask / 8) > 0)
				s6_addr1[i] &= ~0 << 8;
			else
				s6_addr1[i] &= ~0 << mask;

			mask -= 8;
		}
		in6_addr.s6_addr[15-i] = s6_addr1[i];
	};

	ts->saddr = netaddr_new_from_in6addr((struct in6_addr *) &in6_addr);
}

/**
 * Set end address in TS from netaddr
 *
 * @param ts
 * @param addr
 *
 * This function takes a read-only copy of the address so the caller
 * can free address upun return from this function.
 */
void ts_set_eaddr(struct ts *ts, struct netaddr *addr)
{
	if (ts->eaddr)
		netaddr_free(ts->eaddr);

	ts->eaddr = netaddr_dup(addr);
}

/**
 * Set end address in TS from inaddr
 *
 * @param ts
 * @param addr
 *
 * This function takes a complete copy of the address so the caller
 * can free address upun return from this function.
 */
void ts_set_eaddr_from_inaddr(struct ts *ts, struct in_addr *addr)
{
	if (ts->eaddr)
		netaddr_free(ts->eaddr);

	ts->eaddr = netaddr_new_from_inaddr(addr);
}

void ts_set_eaddr_from_netaddr(struct ts *ts, struct in_addr *addr, int netaddr)
{
	guint32 a4;

	if (ts->eaddr)
		netaddr_free(ts->eaddr);

	if (netaddr) {
		a4 = ntohl(*((guint32 *)addr));
		a4 |= ~(~0 << (32 - netaddr));
		a4 = htonl(a4);
	} else
		a4 = 0xFFFFFFFF;

	ts->eaddr = netaddr_new_from_inaddr((struct in_addr *)&a4);
}

/**
 * Set end address in TS from in6addr
 *
 * @param ts
 * @param addr
 *
 * This function takes a complete copy of the address so the caller
 * can free address upun return from this function.
 */
void ts_set_eaddr_from_in6addr(struct ts *ts, struct in6_addr *addr)
{
	if (ts->eaddr)
		netaddr_free(ts->eaddr);

	ts->eaddr = netaddr_new_from_in6addr(addr);
}

void ts_set_eaddr_from_netaddr6(struct ts *ts, struct in6_addr *addr,
		int netaddr)
{
	guint i;
        gint mask;
        struct in6_addr in6_addr;
        guint8 s6_addr1[16];

	i = mask = 0;

        for (i=0; i<16; i++)
                s6_addr1[15-i] = addr->s6_addr[i];

        mask = 128 - netaddr;
        for (i=0; i<16; i++) {
                if (mask > 0){
			if ((mask / 8) > 0)
				s6_addr1[i] |= ~(~0 << 8);
			else
				s6_addr1[i] |= ~(~0 << mask);

			mask -= 8;
                }
                in6_addr.s6_addr[15-i] = s6_addr1[i];
        };

        ts->eaddr = netaddr_new_from_in6addr((struct in6_addr *) &in6_addr);
}

void ts_set_sport(struct ts *ts, guint16 port)
{
	netaddr_set_port(ts->saddr, port);
}

guint16 ts_get_sport(struct ts *ts)
{
	return netaddr_get_port(ts->saddr);
}

void ts_set_eport(struct ts *ts, guint16 port)
{
	netaddr_set_port(ts->eaddr, port);
}

guint16 ts_get_eport(struct ts *ts)
{
	return netaddr_get_port(ts->eaddr);
}

struct netaddr *ts_get_saddr(struct ts *ts)
{
	return ts->saddr;
}

struct netaddr *ts_get_eaddr(struct ts *ts)
{
	return ts->eaddr;
}

void ts_set_ip_proto_id(struct ts *ts, guint8 ip_proto_id)
{
	ts->ip_proto_id = ip_proto_id;
}

guint8 ts_get_ip_proto_id(struct ts *ts)
{
	return ts->ip_proto_id;
}

/*******************************************************************************
 * COMPARISON METHODS
 ******************************************************************************/

/**
 * Compare ts1 and ts2
 *
 * @param ts1
 * @param ts2
 *
 * \return TRUE if ts1 < ts2, otherwise FALSE
 *
 * This function treats ts1 and ts2 as 4-tuples in the following order:
 *	(startIP, startPORT, destinationIP, destinationPORT)
 *
 * iff (destinationIP1 < startIP2) or (destinationPORT1 < startPORT2) then
 * ts1 < ts2 and in that case this function returns TRUE, otherwise, it
 * returns FALSE.
 */
gboolean ts_lt(struct ts *ts1, struct ts *ts2)
{
	return FALSE;
}

/**
 * Compare ts1 and ts2
 *
 * @param ts1
 * @param ts2
 *
 * \return TRUE if ts1 > ts2, otherwise FALSE
 *
 * This function treats ts1 and ts2 as 4-tuples in the following order:
 *	(startIP, startPORT, destinationIP, destinationPORT)
 *
 * iff (destinationIP2 < startIP1) or (destinationPORT2 < startPORT1) then
 * ts1 < ts2 and in that case this function returns TRUE, otherwise, it
 * returns FALSE.
 */
gboolean ts_gt(struct ts *ts1, struct ts *ts2)
{
	return FALSE;
}

/**
 * Check if two traffic selectors are equal.
 *
 * @param ts1		Frist traffic selector
 * @param ts2		Second traffic selector. This one can
 *			have "jocker" in the IP field
 *
 * \return TRUE if TSs are equal, FALSE otherwise
 *
 * Two TSs are treated equal by this function if they have same
 * IP rages in addresses, and they have same IP protocols. Note
 * that during IP protocol checking it is allowed for the second
 * protocol to have zero, meaning ANY IP protocol!
 */
gboolean ts_is_equal(struct ts *ts1, struct ts *ts2)
{
	if (ts1->ip_proto_id != ts2->ip_proto_id
			&& ts2->ip_proto_id)
		return FALSE;

	if (netaddr_cmp_ip2ip(ts1->saddr, ts2->saddr))
		return FALSE;

	if (netaddr_cmp_ip2ip(ts1->eaddr, ts2->eaddr))
		return FALSE;

	return TRUE;
}

/**
 *  Check that ts1 is a subset of the ts2
 *
 *  @param ts1
 *  @param ts2
 *
 *  \return TRUE if ts1 is subset of ts2, otherwise FALSE
 */
gboolean ts_is_subset(struct ts *ts1, struct ts *ts2)
{
	/*
	 * Comparison can not succees if the traffic selectors
	 * have different IP protocols, or if ts2 doesn't have
	 * any IP protocol specified.
	 */
	if (ts1->ip_proto_id != ts2->ip_proto_id
			&& ts2->ip_proto_id)
		return FALSE;

	/*
 	 * The first condition that ts1 is a subset of ts2 is
 	 *	start_address(ts1) >= start_address(ts2)
 	 */
	if (netaddr_cmp_ip2ip(ts1->saddr, ts2->saddr) < 0)
		return FALSE;

	/*
 	 * The second condition that ts1 is a subset of ts2 is
 	 *	end_address(ts1) <= end_address(ts2)
 	 */
	if (netaddr_cmp_ip2ip(ts1->eaddr, ts2->eaddr) > 0)
		return FALSE;

	return TRUE;
}

/*******************************************************************************
 * OTHER METHODS
 ******************************************************************************/

/**
 * Get traffic selector type
 *
 * @param ts
 *
 * \return IKEV2_TST_IPV4_ADDR_RANGE or IKEV2_TST_IPV6_ADDR_RANGE
 */
guint8 ts_get_ts_type(struct ts *ts)
{
	return (netaddr_get_family(ts->saddr) == AF_INET)
			? IKEV2_TST_IPV4_ADDR_RANGE
			: IKEV2_TST_IPV6_ADDR_RANGE;
}

/**
 * Return pointer to SA structure inside start address of trafic selector
 *
 * @param ts
 *
 * \return Pointer to a structure
 */
void *ts_get_saddr_sa(struct ts *ts)
{
	return netaddr_get_ipaddr(ts->saddr);
}

/**
 * Return pointer to SA structure inside end address of trafic selector
 *
 * @param ts
 *
 * \return Pointer to a structure
 */
void *ts_get_eaddr_sa(struct ts *ts)
{
	return netaddr_get_ipaddr(ts->eaddr);
}

/**
 * Find and return intersection between two TS lists
 *
 * @param tsl1
 * @param tsl2
 *
 * \return New ts list
 *
 * Note that we are not distinguishing between error condition and emtpy
 * intersection when NULL is returned. Also, returned list has to be free'd
 * after usage with ts_list_free().
 */
GSList *ts_lists_intersect(GSList *tsl1, GSList *tsl2)
{
	GSList *tsc, *res = NULL;
	struct ts *ts, *ts1, *ts2;
	struct netaddr *n1, *n2;
	guint16 n1_port, n2_port;

	/*
	 * Iterate over each element of tsl1
	 */
	while (tsl1) {

		ts1 = tsl1->data;
		tsl1 = tsl1->next;

		/*
		 * In the following loop we are searching for new traffic
		 * selectors that fullfill the following condition:
		 *
		 * ts->saddr=MAX(ts1->saddr, ts2->saddr)
		 * ts->daddr=MIN(ts1->daddr, ts2->daddr)
		 *
		 * Finally, we take new ts into a list if
		 * ts->saddr <= ts->daddr
		 */
		for (tsc = tsl2; tsc; tsc = tsc->next) {

			ts2 = tsc->data;

			/*
			 * Skip comparison if those are not the same
			 * IP versions...
			 */
			if (ts_sa_family(ts1) != ts_sa_family(ts2))
				continue;

			/*
			 * Check protocols...
			 *
			 * We allow wildcards in either one of the protocols
			 */
			if (ts_get_ip_proto_id(ts1) &&
				ts_get_ip_proto_id(ts2) &&
				(ts_get_ip_proto_id(ts1)
						!= ts_get_ip_proto_id(ts2)))
				continue;

			n1 = netaddr_cmp_ip2ip(ts1->saddr, ts2->saddr) < 0
						?  ts2->saddr : ts1->saddr;
			n1_port = MAX(netaddr_get_port(ts1->saddr),
						netaddr_get_port(ts2->saddr));

			LOG_DEBUG("Set start port to %u", n1_port);

			n2 = netaddr_cmp_ip2ip(ts1->eaddr, ts2->eaddr) < 0
						?  ts1->eaddr : ts2->eaddr;
			n2_port = MIN(netaddr_get_port(ts1->eaddr),
						netaddr_get_port(ts2->eaddr));
			LOG_DEBUG("Set end port to %u", n2_port);

			/*
			 * Check that port ranges are OK, i.e. lower port is
			 * indeed lower than upper port. If not, then
			 * continue...
			 */
			if (n1_port > n2_port)
				continue;

			/*
			 * Check that IP range is ok, that is, that start
			 * IP address is lower than end IP address.
			 */
			if (netaddr_cmp_ip2ip(n1, n2) > 0)
				continue;

			ts = ts_new();

			/*
			 * This is a little "trick", we should take
			 * some concrete number if one is defined in
			 * either ts1 or ts2, or 0 if both of them are
			 * zero...
			 */
			ts->ip_proto_id = MAX(ts_get_ip_proto_id(ts1),
						ts_get_ip_proto_id(ts2));

			ts->saddr = netaddr_dup(n1);
			ts->eaddr = netaddr_dup(n2);

			netaddr_set_port(ts->saddr, n1_port);
			netaddr_set_port(ts->eaddr, n2_port);

			res = g_slist_append(res, ts);
		}
	}

	return res;
}

/**
 * Find and return intersection between TS and TS list...
 *
 * @param ts
 * @param tsl
 *
 * \return New ts list
 *
 * Note that we are not distinguishing between error condition and emtpy
 * intersection when NULL is returned. Also, returned list has to be free'd
 * after usage with ts_list_free().
 */
GSList *ts_list_intersect(struct ts *ts, GSList *tsl)
{
	GSList *ts1, *ret;

	ts1 = g_slist_append(NULL, ts);

	ret = ts_lists_intersect(ts1, tsl);

	ts1 = g_slist_remove(ts1, ts);

	return ret;
}

/**
 * Return address family of given traffic selector
 *
 * @param ts
 *
 * return AF_INET or AF_INET6
 */
sa_family_t ts_sa_family(struct ts *ts)
{
	return netaddr_get_family(ts->saddr);
}

/**
 * Check if tsl2 and tsr2 are subset of tsl1 and tsr1
 *
 * @param tsl1
 * @param tsr1
 * @param tsl2
 * @param tsr2
 *
 * \return	TRUE if tsi2 and tsr2 are subset of tsi1 and tsr1, otherwise
 *		return FALSE
 *
 * This function has a bug for the more complex cases. Take for example
 * the following two traffic selectors:
 *
 *	TS1a = (192.168.0.1 - 192.168.0.10, any, any, any)
 *	TS1b = (192.168.0.10 - 192.168.0.20, any, any, any)
 *
 * If we check if the following traffic selector is the subset of the
 * previous two:
 *
 *	TS2 = (192.168.0.1 - 192.168.0.20, any, any, any)
 *
 * This function will return FALSE, though actually, result has to be TRUE.
 *
 * But for the moment we assume that the previos case would not appear
 * because TS1 will not be given fragmented.
 */
gboolean ts_lists_are_subset(GSList *tsl1, GSList *tsr1,
		GSList *tsl2, GSList *tsr2)
{
	gboolean match;
	struct ts *ts1, *ts2;
	GSList *tsc;

	LOG_FUNC_START(1);

	ts_list_dump(LOGGERNAME, tsl1);
	ts_list_dump(LOGGERNAME, tsr1);

	ts_list_dump(LOGGERNAME, tsl2);
	ts_list_dump(LOGGERNAME, tsr2);

	/**
	 * First, check that tsl2 is a subset of tsi1. In other words
	 * we iterate over all elements of tsl2 and we check that each
	 * one is covered by some element in tsi1.
	 *
	 * This is O(N^2) complexity, but for now we assume that the
	 * cardinality of N is rather small, and in most cases, it will be 1.
	 */
	match = FALSE;
	while (tsl2) {
		ts2 = tsl2->data;
		tsl2 = tsl2->next;

		if (netaddr_cmp_ip2ip(ts2->saddr, ts2->eaddr) > 0)
			continue;

		for (tsc = tsl1; tsc; tsc = tsc->next) {
			ts1 = tsc->data;

			if (ts_get_ip_proto_id(ts1) &&
				(ts_get_ip_proto_id(ts1)
						!= ts_get_ip_proto_id(ts2)))
				continue;

			if (!netaddr_same_family(ts1->saddr, ts2->saddr))
				continue;

#warning "Is this necesasry? To check for the valid range?"
			if (netaddr_cmp_ip2ip(ts1->saddr, ts1->eaddr) > 0)
				continue;

			/*
			 * If ts1->saddr > ts2->saddr then ts1 doesn't
			 * ecompass ts2 and there is no point of checking
			 * any further.
			 */
			if (netaddr_cmp_ip2ip(ts1->saddr, ts2->saddr) > 0)
				continue;

			/*
			 * If ts2->eaddr > ts2->eaddr then ts1 doesn't
			 * ecompass ts2 and there is no point of checking
			 * any further.
			 */
			if (netaddr_cmp_ip2ip(ts2->eaddr, ts1->eaddr) > 0)
				continue;

			match = TRUE;

			break;
		}

		/*
		 * If we didn't found match for the current element in the
		 * previous loop, then stop the execution and return FALSE
		 * value.
		 */
		if (!match)
			break;
	}

	/*
	 * If we found some element in tsl2 that is not covered by
	 * any element in tsl1 then we go out.
	 */
	if (!match)
		goto out;

	/*
	 * Now, we have to check tsr1 and tsr2. We start by assuming
	 * that there is no match.
	 */
	match = FALSE;
	while (tsr2) {
		ts2 = tsr2->data;
		tsr2 = tsr2->next;

		for (tsc = tsr1; tsc; tsc = tsc->next) {
			ts1 = tsc->data;

			if (ts_get_ip_proto_id(ts1) && 
				(ts_get_ip_proto_id(ts1)
						!= ts_get_ip_proto_id(ts2)))
				continue;

			if (!netaddr_same_family(ts1->saddr, ts1->eaddr))
				continue;

			if (netaddr_cmp_ip2ip(ts1->saddr, ts1->eaddr) > 0)
				continue;

			if (netaddr_cmp_ip2ip(ts2->saddr, ts2->eaddr) > 0)
				continue;

			if (netaddr_cmp_ip2ip(ts1->saddr, ts2->saddr) > 0)
				continue;

			if (netaddr_cmp_ip2ip(ts2->eaddr, ts1->eaddr) > 0)
				continue;

			match = TRUE;

			break;
		}

		if (!match)
			break;
	}

out:
	LOG_FUNC_END(1);

	return match;
}

/**
 * Check if two traffic selector sets are equal
 *
 * @param tsi1
 * @param tsr1
 * @param tsi2
 * @param tsr2
 *
 * \return	TRUE if they are equal, otherwise return FALSE
 */
gboolean ts_lists_are_equal(GSList *tsi1, GSList *tsr1,
		GSList *tsi2, GSList *tsr2)
{
	gboolean ret = FALSE;

	LOG_FUNC_START(1);

	/*
	 * Two traffic selector sets are equal if they are each other's
	 * subset...
	 */
	if (!ts_lists_are_subset(tsi1, tsr1, tsi2, tsr2))
		goto out;

	ret = ts_lists_are_subset(tsi2, tsr2, tsi1, tsr1);

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Check if tsl and tsr are complete superset of tsll and tsrl
 *
 * @param tsl
 * @param tsr
 * @param tsll
 * @param tsrl
 *
 * \return TRUE if condition holds, otherwise FALSE
 */
gboolean ts_list_is_subset(struct ts *tsl, struct ts *tsr, GSList *tsll,
		GSList *tsrl)
{
	gboolean ret = FALSE;
	struct ts *ts;

	LOG_FUNC_START(1);

	/*
	 * First iteration over local address
	 */
	while (tsll) {

		ts = tsll->data;

		/*
		 * Check that tsl->saddr <= ts->saddr
		 */
		if (netaddr_cmp_ip2ip(tsl->saddr, ts->saddr) > 0)
			goto out;

		/*
		 * Check that tsl->eaddr >= ts->eaddr
		 */
		if (netaddr_cmp_ip2ip(tsl->eaddr, ts->eaddr) < 0)
			goto out;

		/*
		 * Check that tsl->sport <= ts->sport
		 */
		if (netaddr_get_port(tsl->saddr) >
				netaddr_get_port(ts->saddr))
			goto out;

		/*
		 * Check that tsl->eport >= ts->eport
		 */
		if (netaddr_get_port(tsl->eaddr) <
				netaddr_get_port(ts->eaddr))
			goto out;

		tsll = tsll->next;
	}

	/*
	 * Second iteration over remote address
	 */
	while (tsrl) {

		ts = tsrl->data;

		/*
		 * Check that tsr->saddr <= ts->saddr
		 */
		if (netaddr_cmp_ip2ip(tsr->saddr, ts->saddr) > 0)
			goto out;

		/*
		 * Check that tsr->eaddr >= ts->eaddr
		 */
		if (netaddr_cmp_ip2ip(tsr->eaddr, ts->eaddr) < 0)
			goto out;

		/*
		 * Check that tsr->sport <= ts->sport
		 */
		if (netaddr_get_port(tsr->saddr) >
				netaddr_get_port(ts->saddr))
			goto out;

		/*
		 * Check that tsr->eport >= ts->eport
		 */
		if (netaddr_get_port(tsr->eaddr) <
				netaddr_get_port(ts->eaddr))
			goto out;

		tsrl = tsrl->next;
	}

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Check if tsl1 and tsr1 are complete superset of tsl2 and tsr2
 *
 * @param tsl1
 * @param tsr1
 * @param tsl2
 * @param tsr2
 *
 * \return TRUE if condition holds, otherwise FALSE
 */
gboolean ts_pair_is_subset(struct ts *tsl1, struct ts *tsr1, struct ts *tsl2,
		struct ts *tsr2)
{
	gboolean ret = FALSE;

	LOG_FUNC_START(1);

	/*
	 * First check local address
	 */

	/*
	 * Check that tsl1->saddr <= tsl2->saddr
	 */
	if (netaddr_cmp_ip2ip(tsl1->saddr, tsl2->saddr) > 0)
		goto out;

	/*
	 * Check that tsl1->eaddr >= tsl2->eaddr
	 */
	if (netaddr_cmp_ip2ip(tsl1->eaddr, tsl2->eaddr) < 0)
		goto out;

	/*
	 * Check that tsl1->sport <= tsl2->sport
	 */
	if (netaddr_get_port(tsl1->saddr) > netaddr_get_port(tsl2->saddr))
		goto out;

	/*
	 * Check that tsl1->eport >= tsl2->eport
	 */
	if (netaddr_get_port(tsl1->eaddr) < netaddr_get_port(tsl2->eaddr))
		goto out;

	/*
	 * Second check for remote address
	 */

	/*
	 * Check that tsr1->saddr <= tsr2->saddr
	 */
	if (netaddr_cmp_ip2ip(tsr1->saddr, tsr2->saddr) > 0)
		goto out;

	/*
	 * Check that tsr1->eaddr >= tsr2->eaddr
	 */
	if (netaddr_cmp_ip2ip(tsr1->eaddr, tsr2->eaddr) < 0)
		goto out;

	/*
	 * Check that tsr1->sport <= tsr2->sport
	 */
	if (netaddr_get_port(tsr1->saddr) > netaddr_get_port(tsr2->saddr))
		goto out;

	/*
	 * Check that tsr1->eport >= tsr2->eport
	 */
	if (netaddr_get_port(tsr2->eaddr) < netaddr_get_port(tsr2->eaddr))
		goto out;

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Convert a traffic selector into network mask.
 *
 * @param ts
 *
 * \return Network mask
 *
 * This function doesn't try to be clever. So in case you hand it
 * something that is not representable in a single network address
 * you'll get incorrect return value. I.e. if you hand something
 * like 1.2.3.0 - 1.2.3.255, it will return 1.2.3.0/24, but if you
 * hand it 1.2.3.1 - 1.2.3.66 it will return 1.2.3.0/25 what's
 * obviously incorrect. For those cases use ts_ts2netaddr_list.
 */
struct netaddr *ts_ts2netaddr(struct ts *ts)
{
	struct netaddr *na;
	guint32 addr;
	int i;

	switch (netaddr_get_family(ts->saddr)) {
	case AF_INET:
		addr = htonl(*(guint32 *)netaddr_get_ipaddr(ts->eaddr)) -
			htonl(*(guint32 *)netaddr_get_ipaddr(ts->saddr));

		i = 32;
		while (addr) {
			addr >>= 1;
			i--;
		}

		addr = ntohl(addr);
		na = netaddr_dup(ts->saddr);

		netaddr_set_prefix(na, i);
		break;

	case AF_INET6:
		g_assert_not_reached();
		break;

	default:
		g_assert_not_reached();
	}

	return na;
}

/*******************************************************************************
 * DEBUG METHODS
 ******************************************************************************/

void ts_dump(char *loggername, struct ts *ts)
{
	char ts_ascii[TS_MAX_LEN];
	char a1[20];
	struct protoent *proto;

	strcpy(ts_ascii, "(");
	netaddr_net2str(ts->saddr, a1, 20);
	strcat(ts_ascii, a1);
	strcat(ts_ascii, " - ");
	netaddr_net2str(ts->eaddr, a1, 20);
	strcat(ts_ascii, a1);
	strcat(ts_ascii, ", ");

	if (ts->ip_proto_id) {
		proto = getprotobynumber(ts->ip_proto_id);

		if (proto) {
			strcat(ts_ascii, proto->p_name);

			switch(ts->ip_proto_id) {
			case IPPROTO_TCP:
			case IPPROTO_UDP:
				sprintf(ts_ascii + strlen(ts_ascii), ", %u - %u)",
					ts_get_sport(ts), ts_get_eport(ts));
				break;
			case IPPROTO_ICMP:
				sprintf(ts_ascii + strlen(ts_ascii), ", %u - %u)",
					ts_get_sport(ts), ts_get_eport(ts));
				break;
#ifdef IPPROTO_MOBILE
			case IPPROTO_MOBILE:
				g_assert_not_reached();
				break;
#endif /* IPPROTO_MOBILE */
			case IPPROTO_SCTP:
				g_assert_not_reached();
				break;
			default:
				strcat(ts_ascii, ", n/a, n/a)");
			};
		} else
			strcat(ts_ascii, ", any, any)");
	} else
		strcat(ts_ascii, "any, any, any)");

	ikev2_log (loggername, LOG_PRIORITY_DEBUG, ts_ascii);
}

void ts_list_dump(char *loggername, GSList *ts)
{
	while (ts) {
		ts_dump(loggername, ts->data);
		ts = ts->next;
	}
}
