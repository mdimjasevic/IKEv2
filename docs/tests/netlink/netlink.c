#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

main(int argc, char **argv)
{
	int fd, status;
	char buf[8192];
	struct iovec iov;
	struct sockaddr_nl local;
	struct sockaddr_nl nladdr;
	struct msghdr msg = {
		(void*)&nladdr, sizeof(nladdr),
		&iov,   1,
		NULL,   0,
		0
	};

	if ((fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_XFRM)) < 0) {
		perror("socket");
		exit(1);
	}

	memset(&local, 0, sizeof(local));
	local.nl_family = AF_NETLINK;
	local.nl_groups = ~0;

	if (bind(fd, (struct sockaddr*)&local, sizeof(local)) < 0) {
                perror("bind");
                return -1;
        }

	iov.iov_base = buf;

	memset(&nladdr, 0, sizeof(nladdr));
	nladdr.nl_family = AF_NETLINK;
	nladdr.nl_pid = 0;
	nladdr.nl_groups = 0;

	while (1) {

		iov.iov_len = sizeof(buf);
                status = recvmsg(fd, &msg, 0);

                if (status < 0) {
			perror("OVERRUN");
			continue;
                }

		printf ("Received message\n");
		
	}
}
