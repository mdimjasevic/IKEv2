/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __PFKEY_C

#define LOGGERNAME	"pfkey"

#include <stdio.h>
#include <stdint.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>

/*
 * For UDP_ENCAP_ESPINUDP
 */
#include <linux/udp.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "ts.h"
#include "transforms.h"
#include "proposals.h"
#include "auth.h"
#include "spd.h"
#include "sad.h"
#include "config.h"
#include "crypto.h"
#include "pfkey_msg.h"
#include "pfkey_linux.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

const char *sadb_ext_strs[] = {
	"SADB_EXT_RESERVED",
	"SADB_EXT_SA",
	"SADB_EXT_LIFETIME_CURRENT",
	"SADB_EXT_LIFETIME_HARD",
	"SADB_EXT_LIFETIME_SOFT",
	"SADB_EXT_ADDRESS_SRC",
	"SADB_EXT_ADDRESS_DST",
	"SADB_EXT_ADDRESS_PROXY",
	"SADB_EXT_KEY_AUTH",
	"SADB_EXT_KEY_ENCRYPT",
	"SADB_EXT_IDENTITY_SRC",
	"SADB_EXT_IDENTITY_DST",
	"SADB_EXT_SENSITIVITY",
	"SADB_EXT_PROPOSAL",
	"SADB_EXT_SUPPORTED_AUTH",
	"SADB_EXT_SUPPORTED_ENCRYPT",
	"SADB_EXT_SPIRANGE",
	"SADB_X_EXT_KMPRIVATE",
	"SADB_X_EXT_POLICY",
	"SADB_X_EXT_SA2",
	"SADB_X_EXT_NAT_T_TYPE",
	"SADB_X_EXT_NAT_T_SPORT",
	"SADB_X_EXT_NAT_T_DPORT",
	"SADB_X_EXT_NAT_T_OA"
};

const char *ipsec_policy_strs[] = {
	"IPSEC_POLICY_DISCARD",
	"IPSEC_POLICY_NONE",
	"IPSEC_POLICY_IPSEC",
	"IPSEC_POLICY_ENTRUST",
	"IPSEC_POLICY_BYPASS"
};

const char *ipsec_level_strs[] = {
	"IPSEC_LEVEL_DEFAULT",
	"IPSEC_LEVEL_USE",
	"IPSEC_LEVEL_REQUIRE",
	"IPSEC_LEVEL_UNIQUE"
};

/*
 * Internal data for subsystem
 */
struct pfkey_data *pfkey_data;

/*******************************************************************************
 * INTERNAL PFKEY FUNCTIONS
 ******************************************************************************/

/**
 * Get next available sequence number for a pfkey message to kernel
 *
 * @param seq
 *
 * \return
 *
 * Note that if seq is non-zero, seq is returned and global value updated.
 * Otherwise, new value is returned.
 *
 * TODO: This function reads and updates a global structure so it might
 * be necessary to introduce locking.
 */
guint32 pfkey_get_seq(guint32 seq)
{
	if (!seq)
		seq = g_atomic_int_exchange_and_add(&pfkey_data->pfkey_seq, 1);

	return seq;
}

/**
 * Parses the PFKEY message from the kernel into separate extension headers.
 *
 * This function takes PFKEY message received from the kernel and parses
 * all the extension headers into separate structures. Note: This function
 * is taken from the kernel sources.
 *
 * @param hdr
 * @param ext_hdrs
 *
 * \return
 */
int _pfkey_parse_exthdrs(struct sadb_msg *hdr, void **ext_hdrs)
{
	char *p = (char *) hdr;
	int len = hdr->sadb_msg_len << 3;

	LOG_FUNC_START(1);

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	len -= sizeof(struct sadb_msg);
	p += sizeof(struct sadb_msg);
	while (len > 0) {
		struct sadb_ext *ehdr = (struct sadb_ext *) p;
		guint16 ext_type;
		int ext_len;

		ext_len  = ehdr->sadb_ext_len;
		ext_len *= sizeof(guint64);
		ext_type = ehdr->sadb_ext_type;
		if (ext_len < sizeof(guint64) ||
		    ext_len > len ||
		    ext_type == SADB_EXT_RESERVED)
			return -EINVAL;

		if (ext_type <= SADB_EXT_MAX) {
			int min = (int) sadb_ext_min_len[ext_type];
			if (ext_len < min)
				return -EINVAL;
			if (ext_hdrs[ext_type-1] != NULL)
				return -EINVAL;
			ext_hdrs[ext_type-1] = p;
		}
		p   += ext_len;
		len -= ext_len;
	}

	LOG_FUNC_END(1);

	return 0;
}

/**
 * Parse address extension and return the address and protocol
 *
 * @param addr
 * @param ae	Pointer to an extension
 *
 * \return	-1 in case of an error, 0 if everything was ok.
 *
 * NOTE: Linux's PFKEY implementation seems not to conform to RFC2367. Namely,
 * protocol field (ae->sadb_address_proto), and port fields of addresses are
 * always zero!
 */
gint16 _pfkey_parse_address_ext(struct netaddr **addr, struct sadb_address *ae)
{
	struct sockaddr *sa;
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	int ret = -1;

	LOG_FUNC_START(1);

	sa = (struct sockaddr *)(ae + 1);
	if (sa->sa_family != AF_INET && sa->sa_family != AF_INET6) {
		LOG_ERROR("Unsupported address family");
		goto out;
	}

	if (sa->sa_family == AF_INET) {
		sin = (struct sockaddr_in *)sa;
		*addr = netaddr_new_from_inaddr(&sin->sin_addr);
	} else {
		sin6 = (struct sockaddr_in6 *)sa;
		*addr = netaddr_new_from_in6addr(&sin6->sin6_addr);
	}

	netaddr_set_prefix(*addr, ae->sadb_address_prefixlen);

	ret = 0;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Parse SA extension
 *
 * @param sa
 *
 * \return Just SPI for now (until there is a need for more...)
 */
guint32 _pfkey_parse_sa_ext(struct sadb_sa *sa)
{
	return sa->sadb_sa_spi;
}

/**
 * Parse address extensions and return appropriate traffic selector
 *
 * @param ae	Pointer to source address extension
 *
 * \return	Traffic selector, or NULL in case of an error
 *
 */
struct ts *pfkey_parse_address_ext(struct sadb_address *ae)
{
	struct sockaddr *sa;
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	struct ts *ts = NULL;

	LOG_FUNC_START(1);

	sa = (struct sockaddr *)(ae + 1);
	if (sa->sa_family != AF_INET && sa->sa_family != AF_INET6) {
		LOG_ERROR("Unknown address family in source address extenstion");
		goto out;
	}

	ts = ts_new();

	if (sa->sa_family == AF_INET) {
		sin = (struct sockaddr_in *)sa;
		ts_set_saddr_from_netaddr(ts, &sin->sin_addr,
					ae->sadb_address_prefixlen);
		ts_set_eaddr_from_netaddr(ts, &sin->sin_addr,
					ae->sadb_address_prefixlen);

		ts_set_sport(ts, ntohs(sin->sin_port));
		if (sin->sin_port)
			ts_set_eport(ts, ntohs(sin->sin_port));
		else
			ts_set_eport(ts, 0xFFFF);
	} else {
		sin6 = (struct sockaddr_in6 *)sa;
		ts_set_saddr_from_netaddr6(ts, &sin6->sin6_addr,
					ae->sadb_address_prefixlen);
		ts_set_eaddr_from_netaddr6(ts, &sin6->sin6_addr,
					ae->sadb_address_prefixlen);

		ts_set_sport(ts, ntohs(sin6->sin6_port));
		if (sin6->sin6_port)
			ts_set_eport(ts, ntohs(sin6->sin6_port));
		else
			ts_set_eport(ts, 0xFFFF);
	}

	if (ae->sadb_address_proto == 255)
		ts_set_ip_proto_id(ts, IKEV2_PROTOCOL_UNSPEC);
	else
		ts_set_ip_proto_id(ts, ae->sadb_address_proto);

out:
	LOG_FUNC_END(1);

	return ts;
}

/**
 * Parse a proposal extension, pack the result in the corresponding
 * structures defined in transforms.h and return it
 *
 * @param prop		ptr to extension
 *
 * \return		ptr to proposal structure containig all algorithms
 *			and transforms
 */
GSList *_pfkey_parse_proposal_ext(struct sadb_prop *prop)
{
	struct sadb_comb *comb = (struct sadb_comb *)(prop + 1);
	GSList *proposals;
	proposal_t *proposal;
	transform_t *transform;

	LOG_FUNC_START(1);

	proposals = NULL;

	while ((char *)comb < (char *)prop + (prop->sadb_prop_len << 3)) {

		LOG_TRACE("sadb_comb_auth=%d, sadb_comb_encrypt=%d ",
				comb->sadb_comb_auth, comb->sadb_comb_encrypt);
		LOG_TRACE("sadb_comb_auth_minbits=%d,"
				" sadb_comb_auth_maxbits=%d, ",
				comb->sadb_comb_auth_minbits,
				comb->sadb_comb_auth_maxbits);
		LOG_TRACE("sadb_comb_encrypt_minbits=%d, "
				"sadb_comb_encrypt_maxbits=%d, ",
				comb->sadb_comb_encrypt_minbits,
				comb->sadb_comb_encrypt_maxbits);
		LOG_TRACE("sadb_comb_soft_allocations=%d, "
				"sadb_comb_hard_allocations=%d, ",
				comb->sadb_comb_soft_allocations,
				comb->sadb_comb_hard_allocations);
		LOG_TRACE("sadb_comb_soft_bytes=%lld, "
				"sadb_comb_hard_bytes=%lld, ",
				comb->sadb_comb_soft_bytes,
				comb->sadb_comb_hard_bytes);
		LOG_TRACE("sadb_comb_soft_addtime=%lld, "
				"sadb_comb_hard_addtime=%lld, ",
				comb->sadb_comb_soft_addtime,
				comb->sadb_comb_hard_addtime);
		LOG_TRACE("sadb_comb_soft_usetime=%lld, "
				"sadb_comb_hard_usetime=%lld,\n",
				comb->sadb_comb_soft_usetime,
				comb->sadb_comb_hard_usetime);

		if ((proposal = proposal_new()) == NULL) {
			proposal_list_free(proposals);
			return NULL;
		}

		proposal_list_add_proposal(&proposals, proposal);

		if (comb->sadb_comb_auth) {
			transform = transform_auth_new(comb->sadb_comb_auth);
			transform_set_min_key_len(transform,
						comb->sadb_comb_auth_minbits);
			transform_set_max_key_len(transform,
						comb->sadb_comb_auth_maxbits);
			proposal_transform_append(proposal, transform);
		}

		if (comb->sadb_comb_encrypt) {
			transform = transform_encr_new(comb->sadb_comb_encrypt);
			transform_set_min_key_len(transform,
					comb->sadb_comb_encrypt_minbits);
			transform_set_max_key_len(transform,
					comb->sadb_comb_encrypt_maxbits);
			proposal_transform_append(proposal, transform);
		}

		comb++;
	}

	LOG_FUNC_END(1);

	return proposals;
}

/**
 * Parse a supported algorithms extension
 *
 * @param auth		ptr to authentication extension
 * @param encr		ptr to encryption extension
 *
 * \return		returns a list of transform structures
 */
GSList *pfkey_parse_supported_ext(struct sadb_supported *auth,
		struct sadb_supported *encr)
{
	struct sadb_alg *alg;
	GSList *tl;
	transform_t *transform;

	LOG_FUNC_START(1);

	tl = NULL;

	alg = (struct sadb_alg *)(auth + 1);
	while ((char *)alg < (char *)auth + (auth->sadb_supported_len << 3)) {

		transform = transform_auth_new(alg->sadb_alg_id);
		transform_set_min_key_len(transform,
					alg->sadb_alg_ivlen);
		transform_set_min_key_len(transform,
					alg->sadb_alg_minbits);
		transform_set_max_key_len(transform,
					alg->sadb_alg_maxbits);

		tl = g_slist_append(tl, transform);

		alg++;
	}

	alg = (struct sadb_alg *)(encr + 1);
	while ((char *)alg < (char *)encr + (encr->sadb_supported_len << 3)) {

		transform = transform_encr_new(alg->sadb_alg_id);
		transform_set_min_key_len(transform, alg->sadb_alg_ivlen);
		transform_set_min_key_len(transform, alg->sadb_alg_minbits);
		transform_set_max_key_len(transform, alg->sadb_alg_maxbits);

		tl = g_slist_append(tl, transform);

		alg++;
	}

	LOG_FUNC_END(1);

	return tl;
}

/**
 * Sends PFKEY data along with all defined extension headers to kernel.
 *
 * This function sends header message along with all defined extension headers.
 * Defined extension headers are all those which have non-NULL pointer.
 *
 * @param hdr
 * @param ext_hdrs
 *
 * \return -1 on error, otherwise 0
 */
int _pfkey_send(struct sadb_msg *hdr, void **ext_hdrs)
{
	struct iovec *iovec;
	int niovec, i, retval;

	LOG_FUNC_START(1);

	retval = -1;

	iovec = g_malloc0(sizeof(struct iovec) * (SADB_EXT_MAX + 1));

	niovec = 0;

	iovec[niovec].iov_base = hdr;
	iovec[niovec++].iov_len = sizeof(struct sadb_msg);

	for (i = 0; i < SADB_EXT_MAX; i++) {

		if (ext_hdrs[i] == NULL)
			continue;

		iovec[niovec].iov_base = ext_hdrs[i];
		iovec[niovec++].iov_len =
			((struct sadb_ext *)ext_hdrs[i])->sadb_ext_len << 3;
	}

	if (writev(pfkey_data->pfkeyfd, iovec, niovec) < 0) {
		LOG_PERROR(1);
		goto out_free;
	}

	retval = 0;

out_free:
	g_free(iovec);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Receives data from PFKEY. Dynamically allocates buffer space which
 * callee has to free!
 *
 * \return	buffer	Pointer to a buffer, returns number of read characters
 *		buffer == NULL, len <0	error condition
 */
ssize_t _pfkey_recv(void **buffer)
{
	int len;
	void *p;

	LOG_FUNC_START(1);

	*buffer = NULL;

	len = -1;
	p = g_malloc(MAX_PFKEY_RECV_SIZE);

	if ((len = read(pfkey_data->pfkeyfd, p, MAX_PFKEY_RECV_SIZE)) < 0) {
		len = -1;
		LOG_ERROR("Error reading from pfkey socket");
	} else {
		*buffer = g_memdup(p, len);

		if (((struct sadb_msg *)p)->sadb_msg_errno)
			LOG_DEBUG("errno=%d",
				((struct sadb_msg *)p)->sadb_msg_errno);
	}

	g_free (p);

	LOG_FUNC_END(1);

	return len;
}

/**
 * Update an IPsec SA
 *
 * @param sadb_op
 * @param protocol
 * @param mode
 * @param seq
 * @param spi
 * @param reqid
 * @param integ_type
 * @param sk_a
 * @param encr_type
 * @param sk_e
 * @param src
 * @param dst
 * @param hard_limits
 * @param soft_limits
 * @param natt
 *
 * \return 0 in case operation was successful, otherwise error code
 *
 * \todo{Problem with return values. PFKEY could return positive
 * value that actually means an error occured.}
 */
int pfkey_addupdate(int sadb_op, guint8 protocol, guint8 mode, guint32 seq,
		guint32 spi, guint32 reqid, transform_t *integ_type, char *sk_a,
		transform_t *encr_type, char *sk_e, struct netaddr *src,
		struct netaddr *dst, struct limits *soft_limits,
		struct limits *hard_limits, gboolean natt)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr;
	struct sadb_sa *sa;
	struct sadb_x_sa2 *sa2;
	struct sadb_lifetime *ltsoft, *lthard;
	struct sadb_address *srcaddress, *dstaddress;
	struct sadb_key *akey, *ekey;
	struct sadb_x_nat_t_type *nat_type;
	struct sadb_x_nat_t_port *nat_port;
	guint8 address_size, i, ilen, elen;
	struct pfkey_msg *msg;

	LOG_FUNC_START(1);

	char buf_src[INET6_ADDRSTRLEN], buf_dst[INET6_ADDRSTRLEN];
	LOG_DEBUG("protocol=%d, mode=%d, seq=%d, spi=%08x, "
			"reqid=%d, integ_type=%p, sk_a=%p, "
			"encr_type=%p, sk_e=%p, src=%s, dst=%s)",
			protocol, mode, seq, spi, reqid, integ_type,
			sk_a, encr_type, sk_e,
			netaddr2str(src, buf_src, INET6_ADDRSTRLEN),
			netaddr2str(dst, buf_dst, INET6_ADDRSTRLEN));

	g_assert((protocol == IKEV2_PROTOCOL_ESP)
			|| (protocol == IKEV2_PROTOCOL_AH));

#ifdef DEBUG_PFKEY_MODULE
_debug_transform_dump("", integ_type);
_debug_transform_dump("", encr_type);
_debug_dump_data(sk_a, integ_keylen_get(integ_type));
_debug_dump_data(sk_e, encr_keylen_get(encr_type));
#endif

	memset(ext_hdrs, 0, SADB_EXT_MAX * sizeof(void *));

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = sadb_op;
	hdr.sadb_msg_satype = (protocol == IKEV2_PROTOCOL_ESP)
					? SADB_SATYPE_ESP : SADB_SATYPE_AH;
	hdr.sadb_msg_seq = pfkey_get_seq(seq);
	hdr.sadb_msg_pid = getpid();
	hdr.sadb_msg_len = sizeof(struct sadb_msg) >> 3;

	sa = ext_hdrs[SADB_EXT_SA-1] = g_malloc0(sizeof(struct sadb_sa));

	sa->sadb_sa_len = sizeof(struct sadb_sa) / 8;
	sa->sadb_sa_exttype = SADB_EXT_SA;
	sa->sadb_sa_spi = spi;
	sa->sadb_sa_auth = integ_type ? transform_get_id(integ_type) + 1 : 0;
	sa->sadb_sa_encrypt = encr_type ? transform_get_id(encr_type) : 0;

        hdr.sadb_msg_len += sa->sadb_sa_len;

	lthard = ext_hdrs[SADB_EXT_LIFETIME_HARD-1] =
				g_malloc0(sizeof(struct sadb_lifetime));

	lthard->sadb_lifetime_len = sizeof(struct sadb_lifetime) / 8;
	lthard->sadb_lifetime_exttype = SADB_EXT_LIFETIME_HARD;
	if (hard_limits) {
		lthard->sadb_lifetime_addtime = hard_limits->time;
		lthard->sadb_lifetime_bytes = hard_limits->octets;
	}
	hdr.sadb_msg_len += lthard->sadb_lifetime_len;

	ltsoft = ext_hdrs[SADB_EXT_LIFETIME_SOFT-1] =
				g_malloc0(sizeof(struct sadb_lifetime));

	ltsoft->sadb_lifetime_len = sizeof(struct sadb_lifetime) / 8;
	ltsoft->sadb_lifetime_exttype = SADB_EXT_LIFETIME_SOFT;
	if (soft_limits) {
		ltsoft->sadb_lifetime_addtime = soft_limits->time;
		ltsoft->sadb_lifetime_bytes = soft_limits->octets;
	}
	hdr.sadb_msg_len += ltsoft->sadb_lifetime_len;

	address_size = netaddr_get_sa_size(src);

	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1] =
				g_malloc0(sizeof(struct sadb_address)
							+ address_size);

	srcaddress->sadb_address_len = (sizeof(struct sadb_address) +
							address_size + 7) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;
	memcpy((char *)(srcaddress + 1), src, address_size);
	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1] =
				g_malloc0(sizeof(struct sadb_address)
							+ address_size);

	dstaddress->sadb_address_len = (sizeof(struct sadb_address) +
							address_size + 7) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;
	memcpy((char *)(dstaddress + 1), dst, address_size);
	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	/*
	 * TODO: Here, pointer is checked against NULL, but above is not!!!
 	 */
	if (integ_type) {
		ilen = integ_keylen_get(integ_type);
		akey = ext_hdrs[SADB_EXT_KEY_AUTH-1] =
			g_malloc0(sizeof(struct sadb_key) +
				 ((ilen + 7) & ~7));

		akey->sadb_key_len = (sizeof(struct sadb_key) + ilen + 7) / 8;
		akey->sadb_key_exttype = SADB_EXT_KEY_AUTH;
		akey->sadb_key_bits = ilen * 8;
		memcpy(akey + 1, sk_a, ilen);

		hdr.sadb_msg_len += akey->sadb_key_len;
	}

	/*
	 * TODO: 
 	 */
	if (encr_type) {
		elen = encr_keylen_get(encr_type);

		ekey = ext_hdrs[SADB_EXT_KEY_ENCRYPT-1] =
				g_malloc0(sizeof(struct sadb_key) +
				 ((elen + 7) & ~7));

		ekey->sadb_key_len = (sizeof(struct sadb_key) + elen + 7) / 8;
		ekey->sadb_key_exttype = SADB_EXT_KEY_ENCRYPT;
		ekey->sadb_key_bits = elen * 8;
		memcpy(ekey + 1,sk_e, elen);

		hdr.sadb_msg_len += ekey->sadb_key_len;
	}

	if (mode == IPSEC_MODE_TUNNEL || reqid) {
		sa2 = ext_hdrs[SADB_X_EXT_SA2 - 1] =
				g_malloc0(sizeof(struct sadb_x_sa2));

		sa2->sadb_x_sa2_len = sizeof(struct sadb_x_sa2) / 8;
		sa2->sadb_x_sa2_exttype = SADB_X_EXT_SA2;
		sa2->sadb_x_sa2_mode = mode;
		sa2->sadb_x_sa2_reqid = reqid;

		hdr.sadb_msg_len += sa2->sadb_x_sa2_len;
	}

	/**
	 * Check if NAT-T should be enabled
	 */
	if (natt) {

		/*
		 * Define NAT-T encapsulation method
		 */
		nat_type = ext_hdrs[SADB_X_EXT_NAT_T_TYPE-1] =
				g_malloc0(sizeof(struct sadb_x_nat_t_type));

		nat_type->sadb_x_nat_t_type_len =
				sizeof(struct sadb_x_nat_t_type) / 8;
		nat_type->sadb_x_nat_t_type_exttype = SADB_X_EXT_NAT_T_TYPE;
		nat_type->sadb_x_nat_t_type_type = UDP_ENCAP_ESPINUDP;

		hdr.sadb_msg_len += nat_type->sadb_x_nat_t_type_len;

		/*
		 * Set source port
		 */
		nat_port = ext_hdrs[SADB_X_EXT_NAT_T_SPORT-1] =
				g_malloc0(sizeof(struct sadb_x_nat_t_port));

		nat_port->sadb_x_nat_t_port_len =
				sizeof(struct sadb_x_nat_t_port) / 8;
		nat_port->sadb_x_nat_t_port_exttype = SADB_X_EXT_NAT_T_SPORT;
		nat_port->sadb_x_nat_t_port_port =
				htons(netaddr_get_port(src));

		hdr.sadb_msg_len += nat_port->sadb_x_nat_t_port_len;

		/*
		 * Set destination port
		 */
		nat_port = ext_hdrs[SADB_X_EXT_NAT_T_DPORT-1] =
				g_malloc0(sizeof(struct sadb_x_nat_t_port));

		nat_port->sadb_x_nat_t_port_len =
				sizeof(struct sadb_x_nat_t_port) / 8;
		nat_port->sadb_x_nat_t_port_exttype = SADB_X_EXT_NAT_T_DPORT;
		nat_port->sadb_x_nat_t_port_port =
				htons(netaddr_get_port(dst));

		hdr.sadb_msg_len += nat_port->sadb_x_nat_t_port_len;

	}

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		g_mutex_unlock(pfkey_data->mlqueue);
		LOG_ERROR("Error sending request to kernel via PF_KEYv2");
		return -1;
	}

	msg = g_async_queue_pop(pfkey_data->lqueue);
	g_mutex_unlock(pfkey_data->mlqueue);
	errno = msg->errno_val;

	if (errno)
		LOG_ERRNO(LOG_PRIORITY_ERROR, errno);

	pfkey_msg_free(msg);

	for (i = 0; i < SADB_EXT_MAX; i++)
		if (ext_hdrs[i])
			g_free(ext_hdrs[i]);

	LOG_FUNC_END(1);

	return errno;
}

/*******************************************************************************
 * SIMPLE PFKEY FUNCTIONS
 ******************************************************************************/

/**
 * Register to the PF_KEY interface
 *
 * @param protocol	protocol to register (e.g. AH)
 * @param get_proposals
 *
 * \return		Supported proposals, or NULL. If proposals is TRUE
 *			than NULL means an error occured, otherwise
 *			it might or might not mean an error!
 *
 * Note that this function is called _before_ main loop is started so return
 * information from kernel have to be read directly in this function, we
 * can not rely on ikev2_pfkey_data_pending_cb.
 */
GSList *pfkey_register(guint8 protocol, gboolean get_transforms)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr, *retmsg;
	GSList *transforms = NULL;

	LOG_FUNC_START(1);

	g_assert(protocol == IKEV2_PROTOCOL_UNSPEC
			|| protocol == IKEV2_PROTOCOL_ESP
			|| protocol == IKEV2_PROTOCOL_AH);

	LOG_DEBUG("(protocol=%d)", protocol);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_REGISTER;
	hdr.sadb_msg_satype = (protocol == IKEV2_PROTOCOL_ESP)
				?  SADB_SATYPE_ESP :
					((protocol == IKEV2_PROTOCOL_AH)
						? SADB_SATYPE_AH
						: SADB_SATYPE_UNSPEC);
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;
	hdr.sadb_msg_seq = pfkey_get_seq(0);
	hdr.sadb_msg_pid = getpid();

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_send(&hdr, ext_hdrs) < 0 || _pfkey_recv((void *)&retmsg) < 0)
		goto out;

	if (_pfkey_parse_exthdrs(retmsg, ext_hdrs) < 0) {
		g_free(retmsg);
		goto out;
	}

	if (get_transforms)
		transforms = pfkey_parse_supported_ext(
				ext_hdrs[SADB_EXT_SUPPORTED_AUTH - 1],
				ext_hdrs[SADB_EXT_SUPPORTED_ENCRYPT - 1]);

	g_free(retmsg);

out:
	LOG_FUNC_END(1);

	return transforms;
}

/**
 * Flush kernel's policy database (SPD)
 *
 * \return 0 everything went ok, -1 an error occured
 */
int pfkey_spdflush()
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr, *retmsg;
	int retval = -1;

	LOG_FUNC_START(1);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDFLUSH;
	hdr.sadb_msg_satype = SADB_SATYPE_UNSPEC;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;
	hdr.sadb_msg_seq = pfkey_get_seq(0);
	hdr.sadb_msg_pid = getpid();

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_send(&hdr, ext_hdrs) < 0 || _pfkey_recv((void *)&retmsg) < 0)
		goto out;

	retval = _pfkey_parse_exthdrs(retmsg, ext_hdrs);

	g_free(retmsg);

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Flush kernel's security association database (SAD)
 *
 * @param protocol	protocol to flush (e.g. AH)
 *
 * \return 0 everything went ok, -1 an error occured
 */
int pfkey_flush(guint8 protocol)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr, *retmsg;
	int retval = -1;

	LOG_FUNC_START(1);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_FLUSH;
	hdr.sadb_msg_satype = protocol;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;
	hdr.sadb_msg_seq = pfkey_get_seq(0);
	hdr.sadb_msg_pid = getpid();

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_send(&hdr, ext_hdrs) < 0 || _pfkey_recv((void *)&retmsg) < 0)
		goto out;

	retval = _pfkey_parse_exthdrs(retmsg, ext_hdrs);

	g_free(retmsg);

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Get a new SPI from the PF_KEY interface
 *
 * @param srcaddr       source address of SA
 * @param dstaddr       destination address of SA
 * @param protocol
 * @param seq
 *
 * \return SPI (32 bit for ESP and AH). 0 in case of an error!
 */
guint32 pfkey_getspi(struct netaddr *srcaddr, struct netaddr *dstaddr,
		guint8 protocol, guint8 mode, guint32 reqid, guint32 seq)
{
	struct sadb_msg hdr;
	struct sadb_address *srcaddress;
	struct sadb_address *dstaddress;
	struct sadb_spirange spirange;
	struct sadb_x_sa2 *sa2;
	struct pfkey_msg *msg;
	int socklen;
	guint32 spi;
	void *ext_hdrs[SADB_EXT_MAX];

	LOG_FUNC_START(1);

	char srcbuf[INET6_ADDRSTRLEN], dstbuf[INET6_ADDRSTRLEN];
	LOG_DEBUG("srcaddr=%s, dstaddr=%s, protocol=%d,"
			" mode=%d, reqid=%d, seq=%d",
			netaddr2str(srcaddr, srcbuf, INET6_ADDRSTRLEN),
			netaddr2str(dstaddr, dstbuf, INET6_ADDRSTRLEN),
			protocol, mode, reqid, seq);

	g_assert(protocol == IKEV2_PROTOCOL_AH
			|| protocol == IKEV2_PROTOCOL_ESP);

	socklen = netaddr_get_sa_size(srcaddr);

	srcaddress = g_malloc0(sizeof(struct sadb_address) + socklen);

	dstaddress = g_malloc0(sizeof(struct sadb_address) + socklen);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_GETSPI;
	hdr.sadb_msg_satype = (protocol == IKEV2_PROTOCOL_ESP)
					? SADB_SATYPE_ESP : SADB_SATYPE_AH;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) >> 3;
	hdr.sadb_msg_seq = pfkey_get_seq(seq);
	hdr.sadb_msg_pid = getpid();

	srcaddress->sadb_address_len = (sizeof(struct sadb_address)
							+ socklen + 7) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;
	srcaddress->sadb_address_prefixlen = netaddr_get_prefix(srcaddr);
	memcpy(srcaddress + 1, srcaddr, socklen);
	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress->sadb_address_len = (sizeof(struct sadb_address)
							+ socklen + 7) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;
	dstaddress->sadb_address_prefixlen = netaddr_get_prefix(dstaddr);
	memcpy(dstaddress + 1, dstaddr, socklen);
	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	memset(&spirange, 0, sizeof(struct sadb_spirange));
	spirange.sadb_spirange_len = sizeof(struct sadb_spirange) / 8;
	spirange.sadb_spirange_exttype = SADB_EXT_SPIRANGE;
	spirange.sadb_spirange_min = 1;
	spirange.sadb_spirange_max = 0xFFFFFFFF;
	hdr.sadb_msg_len += spirange.sadb_spirange_len;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);
	ext_hdrs[SADB_EXT_ADDRESS_SRC - 1] = srcaddress;
	ext_hdrs[SADB_EXT_ADDRESS_DST - 1] = dstaddress;
	ext_hdrs[SADB_EXT_SPIRANGE - 1] = &spirange;

	if (mode == IPSEC_MODE_TUNNEL || reqid) {
		sa2 = ext_hdrs[SADB_X_EXT_SA2 - 1] =
				g_malloc0(sizeof(struct sadb_x_sa2));

		sa2->sadb_x_sa2_len = sizeof(struct sadb_x_sa2) / 8;
		sa2->sadb_x_sa2_exttype = SADB_X_EXT_SA2;
		sa2->sadb_x_sa2_mode = mode;
		sa2->sadb_x_sa2_reqid = reqid;

		hdr.sadb_msg_len += sa2->sadb_x_sa2_len;
	}

#warning "Potential memory leak in sa2"
	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		g_free (srcaddress);
		g_free (dstaddress);
		g_mutex_unlock(pfkey_data->mlqueue);
		printf ("ERROR: %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
		return 0;
	}

	msg = g_async_queue_pop(pfkey_data->lqueue);
	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERROR("errno=%u", msg->errno_val);
		spi = 0;
	} else
		spi = msg->getspi_spi;

	g_free(msg);

	g_free (srcaddress);
	g_free (dstaddress);

	LOG_FUNC_END(1);

	return spi;
}

/**
 * Add an IPsec SA
 *
 * @param protocol
 * @param mode
 * @param seq
 * @param spi
 * @param reqid
 * @param integ_type
 * @param sk_a
 * @param encr_type
 * @param sk_e
 * @param src
 * @param dst
 * @param soft_limits
 * @param hard_limits
 * @param natt
 *
 * \return 0 in case operation was successful, otherwise error code
 *
 * \todo{Problem with return values. PFKEY could return positive
 * value that actually means an error occured.}
 */
int pfkey_update(guint8 protocol, guint8 mode, guint32 seq, guint32 spi,
		guint32 reqid, transform_t *integ_type, char *sk_a,
		transform_t *encr_type, char *sk_e,
		struct netaddr *src, struct netaddr *dst,
		struct limits *soft_limits, struct limits *hard_limits,
		gboolean natt)
{

	LOG_FUNC_START(1);

	return pfkey_addupdate(SADB_UPDATE, protocol, mode, seq,
		spi, reqid, integ_type, sk_a, encr_type, sk_e,
		src, dst, soft_limits, hard_limits, natt);

	LOG_FUNC_END(1);
}

/**
 * Add an IPsec SA pair
 *
 * @param si		SA that should be installed in the kernel
 * @param laddr		Local address
 * @param raddr		Remote address
 * @param spdmodify	Should this function modify SPD?
 * @param natt		Should NAT-T be enabled?
 *
 * \return 0 if operation was successful, non-zero value in case of an error
 *
 * \todo{Problem with return values. PFKEY could return positive
 * value that actually means an error occured.}
 */
int pfkey_add(struct sad_item *si, struct netaddr *laddr,
		struct netaddr *raddr, gboolean spdmodify,
		gboolean natt)
{
	int r1;
	struct spd_item *spd;

	LOG_FUNC_START(1);

	r1 = -1;

	/*
	 * Check if we should modify policy
	 */
	if (spdmodify && !(sad_item_get_spd(si)->pid)) {

		/*
		 * We should modify policy, so, create a new SPD entry
		 * that will inherit specific parameters from the
		 * negotiated SAD 
		 */
		spd = sad_item_get_spd(si);
		if (si->tsl)
			sad_item_set_spd(si,
					spd_item_new_from_prototype(spd,
						si->tsl, si->tsr));
		else
			sad_item_set_spd(si, spd_item_dup(spd));

		spd_item_free(spd);
		spd = sad_item_get_spd(si);

		spd_item_set_ipsec_mode(spd, sad_item_get_ipsec_mode(si));

		if (spd->mode == IPSEC_MODE_TUNNEL &&
				!spd->tunnel_saddr && !spd->tunnel_daddr) {
			spd->tunnel_saddr = netaddr_dup(laddr);
			spd->tunnel_daddr = netaddr_dup(raddr);
		}

		/*
		 * We can not directly call pfkey_spdadd since spd_add
		 * checks if the addition is necessary at all and also
		 * adds the new policy into a list of existing policies.
		 */
		if (spd_add(spd) < 0)
			goto out;
	}

	/*
	 * Reserve sequence numbers for the requests to kernel
	 */
	if (!sad_item_get_seq(si))
		sad_item_set_seq(si, pfkey_get_seq(0));

	if (!sad_item_get_seq_in(si))
		sad_item_set_seq_in(si, pfkey_get_seq(0));

	r1 = pfkey_addupdate(SADB_ADD,
			sad_item_get_protocol(si),
			sad_item_get_ipsec_mode(si),
			sad_item_get_seq(si),
			sad_item_get_peer_spi(si),
			sad_item_get_reqid(si),
			sad_item_get_auth(si),
			sad_item_get_sk_al(si),
			sad_item_get_encr(si),
			sad_item_get_sk_el(si),
			laddr, raddr,
			spd_item_get_soft_limits(si->spd),
			spd_item_get_hard_limits(si->spd),
			natt);

	if (!r1) {

		r1 = pfkey_addupdate(SADB_UPDATE,
				sad_item_get_protocol(si),
				sad_item_get_ipsec_mode(si),
				sad_item_get_seq_in(si),
				sad_item_get_spi(si),
				sad_item_get_reqid_in(si),
				sad_item_get_auth(si),
				sad_item_get_sk_ar(si),
				sad_item_get_encr(si),
				sad_item_get_sk_er(si),
				raddr, laddr,
				spd_item_get_soft_limits(si->spd),
				spd_item_get_hard_limits(si->spd),
				natt);

		if (!r1 && spd->level == IPSEC_LEVEL_UNIQUE) {

			r1 = pfkey_addupdate(SADB_UPDATE,
					sad_item_get_protocol(si),
					sad_item_get_ipsec_mode(si),
					sad_item_get_seq_in(si),
					sad_item_get_spi(si),
					sad_item_get_reqid_fwd(si),
					sad_item_get_auth(si),
					sad_item_get_sk_ar(si),
					sad_item_get_encr(si),
					sad_item_get_sk_er(si),
					raddr, laddr,
					spd_item_get_soft_limits(si->spd),
					spd_item_get_hard_limits(si->spd),
					natt);

			if (r1) {
				sad_item_set_seq(si, pfkey_get_seq(0));

				pfkey_delete_single_sa(
						sad_item_get_peer_spi(si),
						sad_item_get_protocol(si),
						raddr, laddr,
						sad_item_get_seq(si));
			}
		}

		if (r1) {
			sad_item_set_seq(si, pfkey_get_seq(0));

			pfkey_delete_single_sa(sad_item_get_peer_spi(si),
					sad_item_get_protocol(si),
					laddr, raddr,
					sad_item_get_seq(si));
		}
	}

	if (r1)
		spd_delete(spd);

out:
	LOG_FUNC_END(1);

	return r1;
}

/**
 * Retrieve given policy from kernel
 *
 * @param seq
 * @param id
 *
 * \return NULL if some error occured, pointer to pfkey_msg otherwise...
 */
struct pfkey_msg *pfkey_spdget(guint32 seq, guint32 id)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr;
	struct sadb_x_policy *policy;
	struct pfkey_msg *msg;

	LOG_FUNC_START(1);

	msg = NULL;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDGET;
	hdr.sadb_msg_satype = SADB_SATYPE_UNSPEC;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) >> 3;
	hdr.sadb_msg_seq = pfkey_get_seq(seq);
	hdr.sadb_msg_pid = getpid();

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1] =
				g_malloc0(sizeof(struct sadb_x_policy));

	policy->sadb_x_policy_len = sizeof(struct sadb_x_policy) / 8;
	policy->sadb_x_policy_exttype = SADB_X_EXT_POLICY;
	policy->sadb_x_policy_id = id;

	hdr.sadb_msg_len += policy->sadb_x_policy_len;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		g_free (policy);
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	g_free (policy);

	msg = g_async_queue_pop(pfkey_data->lqueue);
	g_mutex_unlock(pfkey_data->mlqueue);
	LOG_ERROR("errno=%u", msg->errno_val);

out:
	LOG_FUNC_END(1);

	return msg;
}

/**
 * Delete SA from the SAD
 *
 * @param spi		SPI from SA to delete
 * @param protocol
 * @param src		Source address
 * @param dst		Destination address
 * @param seq
 *
 * \return
 */
int pfkey_delete_single_sa(guint32 spi, guint8 protocol,
		struct netaddr *src, struct netaddr *dst,
		guint32 seq)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr;
	struct sadb_sa *sa;
	struct sadb_address *srcaddress, *dstaddress;
	int address_size, i;
	struct pfkey_msg *msg;

	LOG_FUNC_START(1);

	char buf_src[20], buf_dst[20];
	LOG_TRACE("seq=%d, spi=%08x, src=%s, dst=%s)\n",
		seq, spi,
		netaddr_ip2str(src, buf_src, 20),
		netaddr_ip2str(dst, buf_dst, 20));

	g_assert((protocol == IKEV2_PROTOCOL_ESP)
			|| (protocol == IKEV2_PROTOCOL_AH));

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_DELETE;
	hdr.sadb_msg_satype = (protocol == IKEV2_PROTOCOL_ESP)
					? SADB_SATYPE_ESP
					: SADB_SATYPE_AH;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;
	hdr.sadb_msg_seq = pfkey_get_seq(seq);
	hdr.sadb_msg_pid = getpid();

	sa = ext_hdrs[SADB_EXT_SA-1] = g_malloc0(sizeof(struct sadb_sa));

	sa->sadb_sa_len = sizeof(struct sadb_sa) / 8;
	sa->sadb_sa_exttype = SADB_EXT_SA;
	sa->sadb_sa_spi = spi;
	hdr.sadb_msg_len += sa->sadb_sa_len;

	/*
	 * FIXME: It is assumption that both ends of the SA have to
	 * be the same address family.
	 */
	address_size = (netaddr_get_family(src) == AF_INET)
				? sizeof(struct sockaddr_in)
				: sizeof(struct sockaddr_in6);

	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1] =
					g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	srcaddress->sadb_address_len = (sizeof(struct sadb_address) +
							address_size + 7) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;

	memcpy((char *)(srcaddress + 1), src, address_size);
	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1] =
					g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	dstaddress->sadb_address_len = (sizeof(struct sadb_address) +
							address_size + 7) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;

	memcpy((char *)(dstaddress + 1), dst, address_size);
	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		g_mutex_unlock(pfkey_data->mlqueue);
		LOG_ERROR("Error removing CHILD SA from kernel");
		return -1;
	}

	msg = g_async_queue_pop(pfkey_data->lqueue);
	g_mutex_unlock(pfkey_data->mlqueue);
	errno = msg->errno_val;

	if (errno)
		LOG_ERRNO(LOG_PRIORITY_ERROR, errno);

        pfkey_msg_free(msg);

	for (i = 0; i < SADB_EXT_MAX; i++)
		if (ext_hdrs[i])
			g_free(ext_hdrs[i]);

	LOG_FUNC_END(1);

	return errno;
}

/**
 * Delete SA pair from the SAD
 *
 * @param si		SAD item structure
 * @param laddr		Local address
 * @param raddr		Remote address
 *
 * \return 0 deletition was successful
 */
int pfkey_delete(struct sad_item *si, struct netaddr *laddr,
		struct netaddr *raddr)
{
	/*
	 * Check if we should remove policy.  We can not call directly
	 * pfkey_spddelete since spd_delete checks if the removal is
	 * necessary at all.
	 */
	spd_delete(sad_item_get_spd(si));

	/*
	 * Reserve sequence numbers for the requests to kernel
	 */
	if (!sad_item_get_seq(si))
		sad_item_set_seq(si, pfkey_get_seq(0));

	if (!sad_item_get_seq_in(si))
		sad_item_set_seq_in(si, pfkey_get_seq(0));

	pfkey_delete_single_sa(sad_item_get_spi(si),
		sad_item_get_protocol(si),
			raddr, laddr,
			sad_item_get_seq_in(si));

	pfkey_delete_single_sa(sad_item_get_peer_spi(si),
			sad_item_get_protocol(si),
			laddr, raddr,
			sad_item_get_seq(si));

	return 0;
}

/**
 * Add new policy to kernel policy database (SPD)
 *
 * @param si	Policy to be installed into the kernel
 *
 * Note that even though this function assumess that traffic selectors
 * in the policy might be ranges and thus tries to convert them into
 * network mask this functionality is not complete. The problem is
 * that each policy gets it's own ID, and we don't have place to save
 * this ID!
 *
 * \return
 */
int pfkey_spdadd(struct spd_item *si)
{
	void *ext_hdrs[SADB_EXT_MAX];
	pid_t my_pid = getpid();
	struct pfkey_msg *msg;
	struct sadb_msg hdr;
	struct sadb_address *srcaddress, *dstaddress;
	struct sadb_x_policy *policy;
	struct sadb_x_ipsecrequest *ipsecrequest;
	int address_size, size, retval, i;
	struct netaddr *src, *dst;
	struct ts *tsl, *tsr;

	LOG_FUNC_START(1);

	retval = -1;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);
	memset(&hdr, 0, sizeof(struct sadb_msg));

	/*
	 * Some sanity checks before we do anything
	 */
	if (spd_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL &&
			(!spd_item_get_tunnel_saddr(si) ||
			!spd_item_get_tunnel_daddr(si))) {
		LOG_ERROR("Tunnel mode requested without specifying tunnel"
				" endpoints. Ignoring policy!");
		goto out;
	}

	/**
	 * We assume here that there is only _single_ ts per
	 * ts list! Otherwise, be terminate application since it's
	 * a bug!
	 */
	g_assert(spd_item_get_tsl(si)->next == NULL);
	g_assert(spd_item_get_tsr(si)->next == NULL);

	tsl = spd_item_get_tsl(si)->data;
	tsr = spd_item_get_tsr(si)->data;

	src = ts_ts2netaddr(tsl);
	dst = ts_ts2netaddr(tsr);

	/*
	 * Prepare data for the outbound policy to be added
	 */
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDADD;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;

	switch(spd_item_get_ipsec_proto(si)) {
	case IKEV2_PROTOCOL_ESP:
		hdr.sadb_msg_satype = SADB_SATYPE_ESP;
		break;
	case IKEV2_PROTOCOL_AH:
		hdr.sadb_msg_satype = SADB_SATYPE_AH;
		break;
	default:
		LOG_ERROR("Unknown protocol specification");
		goto out;
	}

	hdr.sadb_msg_pid = my_pid;
	hdr.sadb_msg_seq = pfkey_get_seq(0);

	address_size = netaddr_get_sa_size(src);

	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1] =
				g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	srcaddress->sadb_address_len = (sizeof(struct sadb_address) +
						address_size + 7) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;

	srcaddress->sadb_address_proto =
			ts_get_ip_proto_id(tsl);
	if (!srcaddress->sadb_address_proto)
		srcaddress->sadb_address_proto = IPSEC_PROTO_ANY;

	memcpy((char *)(srcaddress + 1), netaddr_get_sa(src), address_size);

	srcaddress->sadb_address_prefixlen = netaddr_get_prefix(src);

	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1] =
				g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	dstaddress->sadb_address_len = (sizeof(struct sadb_address) +
						address_size + 7) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;

	dstaddress->sadb_address_proto = ts_get_ip_proto_id(tsr);
	if (!dstaddress->sadb_address_proto)
		dstaddress->sadb_address_proto = IPSEC_PROTO_ANY;

	memcpy((char *)(dstaddress + 1), netaddr_get_sa(dst), address_size);

	dstaddress->sadb_address_prefixlen = netaddr_get_prefix(dst);

	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	netaddr_free(src);
	netaddr_free(dst);

	size = sizeof(struct sadb_x_policy) +
				sizeof(struct sadb_x_ipsecrequest);
	if (spd_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL)
		size += 2 * netaddr_get_sa_size(si->tunnel_saddr);

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1] = g_malloc0(size);

	policy->sadb_x_policy_len = (size + 7) / 8;
	policy->sadb_x_policy_exttype = SADB_X_EXT_POLICY;
	policy->sadb_x_policy_dir = IPSEC_DIR_OUTBOUND;
	policy->sadb_x_policy_type = IPSEC_POLICY_IPSEC;
	policy->sadb_x_policy_priority = 0x80000000;

	ipsecrequest = (struct sadb_x_ipsecrequest *)(policy + 1);

	ipsecrequest->sadb_x_ipsecrequest_len =
					sizeof(struct sadb_x_ipsecrequest);
	ipsecrequest->sadb_x_ipsecrequest_level = spd_item_get_ipsec_level(si);
	ipsecrequest->sadb_x_ipsecrequest_mode = spd_item_get_ipsec_mode(si);

	switch(spd_item_get_ipsec_proto(si)) {
	case IKEV2_PROTOCOL_ESP:
		ipsecrequest->sadb_x_ipsecrequest_proto = IPPROTO_ESP;
		break;
	case IKEV2_PROTOCOL_AH:
		ipsecrequest->sadb_x_ipsecrequest_proto = IPPROTO_AH;
		break;
	default:
		LOG_ERROR("Unknown protocol specification. Using ESP.");
		ipsecrequest->sadb_x_ipsecrequest_proto = IPPROTO_ESP;
	}

	if (spd_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL
			&& si->tunnel_saddr && si->tunnel_daddr) {

		memcpy((char *)(ipsecrequest + 1),
				netaddr_get_sa(si->tunnel_saddr),
				netaddr_get_sa_size(si->tunnel_saddr));

		memcpy((char *)(ipsecrequest + 1) +
					netaddr_get_sa_size(si->tunnel_saddr),
				netaddr_get_sa(si->tunnel_daddr),
				netaddr_get_sa_size(si->tunnel_daddr));

		ipsecrequest->sadb_x_ipsecrequest_len +=
				2 * netaddr_get_sa_size(si->tunnel_saddr);
	}

	hdr.sadb_msg_len += policy->sadb_x_policy_len;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error inserting policy into the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		goto out;
	}

	si->pid = msg->spd.policy_id;
	si->reqid = msg->spd.reqid;

	pfkey_msg_free(msg);

	/*
	 * Prepare data for the inbound policy to be added.
	 */
	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1];
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;
	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1];
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;
	ext_hdrs[SADB_EXT_ADDRESS_SRC-1] = srcaddress;
	ext_hdrs[SADB_EXT_ADDRESS_DST-1] = dstaddress;
	hdr.sadb_msg_seq = pfkey_get_seq(0);

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	policy->sadb_x_policy_dir = IPSEC_DIR_INBOUND;

	if (spd_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {

		memcpy((char *)(ipsecrequest + 1),
				netaddr_get_sa(si->tunnel_daddr),
				netaddr_get_sa_size(si->tunnel_daddr));

		memcpy((char *)(ipsecrequest + 1) +
					netaddr_get_sa_size(si->tunnel_daddr),
				netaddr_get_sa(si->tunnel_saddr),
				netaddr_get_sa_size(si->tunnel_saddr));
	}

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error inserting policy into the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		// pfkey_spddelete();
		goto out;
	}

	si->pid_in = msg->spd.policy_id;
	si->reqid_in = msg->spd.reqid;

	pfkey_msg_free(msg);

	/*
	 * Add fwd policy on the kernel. Maybe it's not necessary to
	 * add this each time pfkey_spdadd is called, but for now it's
	 * as is...
	 */
	policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	policy->sadb_x_policy_dir = IPSEC_DIR_FWD;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error inserting policy into the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		goto out;
	}

	si->pid_fwd = msg->spd.policy_id;
	si->reqid_fwd = msg->spd.reqid;

	pfkey_msg_free(msg);

	retval = 0;

out:
	for (i = 0; i < SADB_EXT_MAX; i++)
                if (ext_hdrs[i])
                        g_free(ext_hdrs[i]);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Remove existing policy from the kernel
 *
 * @param si
 *
 * \return 0 if removal was successful, -1 otherwise
 */
int pfkey_spddelete(struct spd_item *si)
{
	void *ext_hdrs[SADB_EXT_MAX];
	pid_t my_pid = getpid();
	struct pfkey_msg *msg;
	struct sadb_msg hdr;
	struct sadb_address *srcaddress, *dstaddress;
	struct sadb_x_policy *policy;
	int address_size, retval, i;
	struct netaddr *src, *dst;
	struct ts *tsl, *tsr;

	LOG_FUNC_START(1);

	retval = -1;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);
	memset(&hdr, 0, sizeof(struct sadb_msg));

	/**
	 * We assume here that there is only _single_ ts per
	 * ts list! Otherwise, be terminate application since it's
	 * a bug!
	 */
	g_assert(spd_item_get_tsl(si)->next == NULL);
	g_assert(spd_item_get_tsr(si)->next == NULL);

	tsl = spd_item_get_tsl(si)->data;
	tsr = spd_item_get_tsr(si)->data;

	src = ts_ts2netaddr(tsl);
	dst = ts_ts2netaddr(tsr);

	/*
	 * Prepare header
	 */
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDDELETE;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;

	switch(spd_item_get_ipsec_proto(si)) {
	case IKEV2_PROTOCOL_ESP:
		hdr.sadb_msg_satype = SADB_SATYPE_ESP;
		break;
	case IKEV2_PROTOCOL_AH:
		hdr.sadb_msg_satype = SADB_SATYPE_AH;
		break;
	default:
		LOG_ERROR("Unknown protocol specification");
		goto out;
	}

	hdr.sadb_msg_pid = my_pid;
	hdr.sadb_msg_seq = pfkey_get_seq(0);

	address_size = netaddr_get_sa_size(src);

	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1] =
				g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	srcaddress->sadb_address_len = (sizeof(struct sadb_address) +
						address_size + 7) / 8;
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;

	srcaddress->sadb_address_proto = ts_get_ip_proto_id(tsl);
	if (!srcaddress->sadb_address_proto)
		srcaddress->sadb_address_proto = IPSEC_PROTO_ANY;

	memcpy((char *)(srcaddress + 1), netaddr_get_sa(src), address_size);

	srcaddress->sadb_address_prefixlen = netaddr_get_prefix(src);

	hdr.sadb_msg_len += srcaddress->sadb_address_len;

	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1] =
				g_malloc0(sizeof(struct sadb_address)
						+ address_size);

	dstaddress->sadb_address_len = (sizeof(struct sadb_address) +
						address_size + 7) / 8;
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;

	dstaddress->sadb_address_proto = ts_get_ip_proto_id(tsr);
	if (!dstaddress->sadb_address_proto)
		dstaddress->sadb_address_proto = IPSEC_PROTO_ANY;

	memcpy((char *)(dstaddress + 1), netaddr_get_sa(dst), address_size);

	dstaddress->sadb_address_prefixlen = netaddr_get_prefix(dst);

	hdr.sadb_msg_len += dstaddress->sadb_address_len;

	netaddr_free(src);
	netaddr_free(dst);

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1] =
				g_malloc0(sizeof(struct sadb_x_policy));

	policy->sadb_x_policy_len = (sizeof(struct sadb_x_policy) + 7) / 8;
	policy->sadb_x_policy_exttype = SADB_X_EXT_POLICY;
	policy->sadb_x_policy_dir = IPSEC_DIR_OUTBOUND;
	policy->sadb_x_policy_type = IPSEC_POLICY_IPSEC;

	hdr.sadb_msg_len += policy->sadb_x_policy_len;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error removing policy from the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		goto out;
	}

	pfkey_msg_free(msg);

	/*
	 * Prepare data for the inbound policy to be deleted.
	 */
	dstaddress = ext_hdrs[SADB_EXT_ADDRESS_SRC-1];
	dstaddress->sadb_address_exttype = SADB_EXT_ADDRESS_DST;
	srcaddress = ext_hdrs[SADB_EXT_ADDRESS_DST-1];
	srcaddress->sadb_address_exttype = SADB_EXT_ADDRESS_SRC;
	ext_hdrs[SADB_EXT_ADDRESS_SRC-1] = srcaddress;
	ext_hdrs[SADB_EXT_ADDRESS_DST-1] = dstaddress;
	hdr.sadb_msg_seq = pfkey_get_seq(0);

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	policy->sadb_x_policy_dir = IPSEC_DIR_INBOUND;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error removing policy from the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		goto out;
	}

	pfkey_msg_free(msg);

	/*
	 * Prepare data for the fwd policy to be deleted.
	 */
	hdr.sadb_msg_seq = pfkey_get_seq(0);

	policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	policy->sadb_x_policy_dir = IPSEC_DIR_FWD;

	g_mutex_lock(pfkey_data->mlqueue);
	if (_pfkey_send(&hdr, ext_hdrs) < 0) {
		LOG_ERROR("Error removing policy from the kernel");
		g_mutex_unlock(pfkey_data->mlqueue);
		goto out;
	}

	if (pfkey_data->state == PFKEY_STATE_INIT)
		ikev2_pfkey_data_pending_cb(NULL, 0, NULL);

	msg = g_async_queue_pop(pfkey_data->lqueue);

	g_mutex_unlock(pfkey_data->mlqueue);

	if (msg->errno_val) {
		LOG_ERRNO(LOG_PRIORITY_ERROR, msg->errno_val);
		pfkey_msg_free(msg);
		goto out;
	}

	pfkey_msg_free(msg);

	retval = 0;

out:
	for (i = 0; i < SADB_EXT_MAX; i++)
                if (ext_hdrs[i])
                        g_free(ext_hdrs[i]);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Return complete policy database from the kernel...
 *
 * \return 0 if dump was successfull. In that case *spds contains a
 *		normalized list of all SPD entries. -1 is returned
 *		in case of an error.
 *
 * Note that this function is called _before_ main loop is started so
 * return information from kernel has to be read directly in this
 * function, we do not rely on ikev2_pfkey_data_pending_cb.
 */
gint pfkey_spddump(GSList **spds)
{
	GSList *kspd, *c;
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_msg hdr, *hdr2 = NULL;
	struct pfkey_msg *msg = NULL;
	struct spd_item *spd1, *spd2;
	int seq;
	pid_t my_pid = getpid();

	LOG_FUNC_START(1);

	spd1 = spd2 = NULL;
	*spds = NULL;
	kspd = NULL;

	memset(&hdr, 0, sizeof(struct sadb_msg));
	hdr.sadb_msg_version = PF_KEY_V2;
	hdr.sadb_msg_type = SADB_X_SPDDUMP;
	hdr.sadb_msg_satype = SADB_SATYPE_UNSPEC;
	hdr.sadb_msg_len = sizeof(struct sadb_msg) / 8;
	hdr.sadb_msg_seq = pfkey_get_seq(0);
	hdr.sadb_msg_pid = my_pid;

	memset(ext_hdrs, 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_send(&hdr, ext_hdrs) < 0)
		goto out;

	do {
		seq = 1;

		if (_pfkey_recv((void *)&hdr2) < 0)
			goto out_err;

		/*
		 * Check errno value in received message
		 */
		switch (hdr2->sadb_msg_errno) {
		case 0:
			break;

		case ENOENT:
			LOG_NOTICE("No SPD entries in kernel!");
			g_free(hdr2);
			goto out;

		default:
			LOG_ERROR("Unkown error received from kernel!");
			goto out_err;
		}

		if (hdr2->sadb_msg_type != SADB_X_SPDDUMP
			|| hdr2->sadb_msg_pid != my_pid)
			continue;

		if (hdr2->sadb_msg_errno)
			goto out_err;

		if ((msg = pfkey_msg_new()) == NULL)
			goto out_err;

		seq = hdr2->sadb_msg_seq;

		if (!pfkey_parse_sadb_x_spdget(hdr2, msg))
			goto out_err;

		g_free(hdr2);

		if (!(spd1 = spd_item_new()))
			goto out_err;

		spd1->tsl = g_slist_append(NULL, msg->spd.tsi);
		msg->spd.tsi = NULL;
		spd1->tsr = g_slist_append(NULL, msg->spd.tsr);
		msg->spd.tsr = NULL;

		spd_item_set_ipsec_mode(spd1, msg->spd.mode);
		spd_item_set_ipsec_proto(spd1, msg->spd.protocol);
		spd1->pid = msg->spd.policy_id;
		spd1->dir = msg->spd.dir;
		spd1->reqid = msg->spd.reqid;
		spd1->tunnel_saddr = msg->spd.tunnel_saddr;
		msg->spd.tunnel_saddr = NULL;
		spd1->tunnel_daddr = msg->spd.tunnel_daddr;
		msg->spd.tunnel_daddr = NULL;

		pfkey_msg_free(msg);
		msg = NULL;

		switch (spd1->dir) {
		case IPSEC_DIR_FWD:
		case IPSEC_DIR_INBOUND:
			kspd = g_slist_append(kspd, spd1);
			break;

		case IPSEC_DIR_OUTBOUND:
			g_atomic_int_inc(&spd1->refcnt);
			*spds = g_slist_append(*spds, spd1);
			break;

		default:
			LOG_ERROR("Unknown policy direction %u "
					"in policy with id=%u",
					spd1->dir, spd1->pid);
			spd_item_free(spd1);
			break;
		}

	} while (seq);

	/*
	 * Compress the list so that single entry contains data for in,
	 * out and fwd policies.
	 */
	while (kspd) {
		spd1 = kspd->data;
		kspd = g_slist_remove(kspd, spd1);

		LOG_DEBUG("Searching matching policy ID for policy %u",
				spd1->pid);

		for (c = *spds; c; c = c->next) {
			spd2 = c->data;

			LOG_DEBUG("Checking policy ID %u", spd2->pid);

			if (spd1->mode != spd2->mode)
				continue;

			if (!ts_lists_are_equal(spd1->tsl, spd1->tsr,
					spd2->tsr, spd2->tsl))
				continue;

			LOG_DEBUG("Match found");

			if (spd1->dir == IPSEC_DIR_INBOUND) {

				if (spd2->pid_in) {
					LOG_ERROR("Policy id %u"
						" has at least two equivalent "
						"in policies (%u and %u)",
						spd2->pid, spd2->pid_in,
						spd1->pid);
				} else {
					spd2->pid_in = spd1->pid;
					spd2->reqid_in = spd1->reqid;
				}

			} else {

				if (spd2->pid_fwd) {
					LOG_ERROR("Policy id %u"
						" has at least two equivalent "
						"fwd policies (%u and %u)",
						spd2->pid, spd2->pid_fwd,
						spd1->pid);
				} else {
					spd2->pid_fwd = spd1->pid;
					spd2->reqid_fwd = spd1->reqid;
				}
			}

			spd_item_free(spd1);

			break;
		}
	}

out:
	LOG_FUNC_END(1);

	return 0;

out_err:
	if (hdr2)
		g_free(hdr2);

	pfkey_msg_free(msg);

	spd_item_list_free(kspd);
	spd_item_list_free(*spds);

	*spds = NULL;

	LOG_FUNC_END(1);

	return -1;
}

/*******************************************************************************
 * MISC
 ******************************************************************************/

/**
 * Return proposal list of supported transforms by kernel
 *
 * \return List of proposals
 *
 */
GSList *pfkey_supported_transforms_get(void)
{
	return pfkey_data->transforms;
}

/*******************************************************************************
 * PFKEY CALLBACK FUNCTIONS
 ******************************************************************************/

/**
 * Parse kernel's SADB_ACQUIRE message
 *
 * @param hdr   Pointer to a message received from a kernel
 * @param msg
 *
 * \return
 */
gboolean pfkey_parse_sadb_acquire(struct sadb_msg *hdr, struct pfkey_msg *msg)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_x_policy *policy;
	int ret = FALSE;

	LOG_FUNC_START(1);

	memset(&ext_hdrs[0], 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_parse_exthdrs(hdr, ext_hdrs) < 0)
		goto out;

	/*
	 * Check for the presence of all the required extensions...
	 */
	if (ext_hdrs[SADB_EXT_ADDRESS_SRC - 1] == NULL
		|| ext_hdrs[SADB_EXT_ADDRESS_DST - 1] == NULL
		|| ext_hdrs[SADB_EXT_PROPOSAL - 1] == NULL) {
		LOG_DEBUG("Not all extensions present");
		goto out;
	}

	/*
	 * Check for the presence of optional extensions and warn if they
	 * exist since we are not using them for now...
	 */
	if (ext_hdrs[SADB_EXT_ADDRESS_PROXY - 1])
		LOG_DEBUG("Unhandled SADB_EXT_ADDRESS_PROXY extension");

	if (ext_hdrs[SADB_EXT_IDENTITY_SRC - 1])
		LOG_DEBUG("Unhandled SADB_EXT_IDENTITY_SRC extension");

	if (ext_hdrs[SADB_EXT_IDENTITY_DST - 1])
		LOG_DEBUG("Unhandled SADB_EXT_IDENTITY_DST extension");

	if (ext_hdrs[SADB_EXT_SENSITIVITY - 1])
		LOG_DEBUG("Unhandled SADB_EXT_SENSITIVITY extension");

	/*
	 * Parse source and destination addresses and create traffic
	 * selector from them....
	 *
	 * TODO: Maybe we should send error to a kernel...
	 */
	if (_pfkey_parse_address_ext(&msg->acquire.saddr,
			ext_hdrs[SADB_EXT_ADDRESS_SRC - 1]) < 0)
		goto out;

	if (_pfkey_parse_address_ext(&msg->acquire.daddr,
			ext_hdrs[SADB_EXT_ADDRESS_DST - 1]) < 0)
		goto out;

	/*
	 * Search for policy that triggered this ACQUIRE message...
	 */
	policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	msg->acquire.pid = policy->sadb_x_policy_id;

	pfkey_msg_dump(msg);

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * pfkey_expire
 *
 * @param hdr   Pointer to a message received from a kernel
 * @param msg
 *
 * \return
 */
int pfkey_parse_sadb_expire(struct sadb_msg *hdr, struct pfkey_msg *msg)
{
	int ret = -1;
	void *ext_hdrs[SADB_EXT_MAX];

	LOG_FUNC_START(1);

	memset(&ext_hdrs[0], 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_parse_exthdrs(hdr, ext_hdrs) < 0) {
		LOG_DEBUG("Error parsing PFKEY message from kernel");
		goto out;
	}

	/*
	 * Check for the existance of all the required extensions...
	 */
	if (ext_hdrs[SADB_EXT_SA - 1] == NULL
		|| ext_hdrs[SADB_EXT_ADDRESS_SRC - 1] == NULL
		|| ext_hdrs[SADB_EXT_ADDRESS_DST - 1] == NULL
		|| ext_hdrs[SADB_EXT_LIFETIME_CURRENT - 1] == NULL
		|| (ext_hdrs[SADB_EXT_LIFETIME_SOFT - 1] == NULL
			&& ext_hdrs[SADB_EXT_LIFETIME_HARD - 1] == NULL)) {
		LOG_DEBUG("Error parsing PFKEY message from kernel");
		goto out;
	}

	/*
	 * Get SPI value from SA extention
	 */
	msg->expire.spi = _pfkey_parse_sa_ext(ext_hdrs[SADB_EXT_SA - 1]);
	LOG_DEBUG("Expire message for SA with SPI=%08X", msg->expire.spi);

	/*
	 * Parse source and destination addresses
	 */
	if (_pfkey_parse_address_ext(&msg->expire.saddr,
				ext_hdrs[SADB_EXT_ADDRESS_SRC - 1]) < 0)
		goto out;

	if (_pfkey_parse_address_ext(&msg->expire.daddr,
				ext_hdrs[SADB_EXT_ADDRESS_DST - 1]) < 0)
		goto out;

	switch (hdr->sadb_msg_satype) {
	case SADB_SATYPE_AH:
		msg->expire.protocol = IKEV2_PROTOCOL_AH;
		break;
	case SADB_SATYPE_ESP:
		msg->expire.protocol = IKEV2_PROTOCOL_ESP;
		break;
	default:
		LOG_ERROR("Unknown SA TYPE (%d) in PFKEY message",
				hdr->sadb_msg_satype);
		goto out;
	}

	msg->expire.hard_limit = FALSE;

	if (ext_hdrs[SADB_EXT_LIFETIME_HARD - 1] != NULL) {
		LOG_TRACE("Hard limit expired");
		msg->expire.hard_limit = TRUE;
	}

	ret = 0;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Parse kernel's SADB_X_SPDADD message
 *
 * @param hdr   Pointer to a message received from a kernel
 * @param msg
 *
 * \return
 *
 * In this case we are only interested in policy ID and reqID
 */
gboolean pfkey_parse_sadb_x_spdadd(struct sadb_msg *hdr, struct pfkey_msg *msg)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_x_policy *sadb_x_policy;
	struct sadb_x_ipsecrequest *sadb_x_ipsecrequest;
	int ret = FALSE;

	LOG_FUNC_START(1);

	memset(&ext_hdrs[0], 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_parse_exthdrs(hdr, ext_hdrs) < 0)
		goto out;

	pfkey_dump(hdr, ext_hdrs);

	sadb_x_policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	msg->spd.policy_id = sadb_x_policy->sadb_x_policy_id;

	sadb_x_ipsecrequest = (struct sadb_x_ipsecrequest *)(sadb_x_policy + 1);
	msg->spd.reqid = sadb_x_ipsecrequest->sadb_x_ipsecrequest_reqid;

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Parse kernel's SADB_X_SPDDELETE message
 *
 * @param hdr   Pointer to a message received from a kernel
 * @param msg
 *
 * \return
 *
 * In this case we are only interested in policy ID and reqID
 */
gboolean pfkey_parse_sadb_x_spddelete(struct sadb_msg *hdr, struct pfkey_msg *msg)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_x_policy *sadb_x_policy;
	struct sadb_x_ipsecrequest *sadb_x_ipsecrequest;
	int ret = FALSE;

	LOG_FUNC_START(1);

	memset(&ext_hdrs[0], 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_parse_exthdrs(hdr, ext_hdrs) < 0)
		goto out;

	pfkey_dump(hdr, ext_hdrs);

	sadb_x_policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	msg->spd.policy_id = sadb_x_policy->sadb_x_policy_id;

	sadb_x_ipsecrequest = (struct sadb_x_ipsecrequest *)(sadb_x_policy + 1);
	msg->spd.reqid = sadb_x_ipsecrequest->sadb_x_ipsecrequest_reqid;

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Parse kernel's SADB_X_SPDDUMP/SADB_X_SPDGET message
 *
 * @param hdr   Pointer to a message received from a kernel
 * @param msg
 *
 * \return
 */
gboolean pfkey_parse_sadb_x_spdget(struct sadb_msg *hdr, struct pfkey_msg *msg)
{
	void *ext_hdrs[SADB_EXT_MAX];
	struct sadb_x_policy *sadb_x_policy;
	struct sadb_x_ipsecrequest *sadb_x_ipsecrequest;
	int ret = FALSE;

	struct sockaddr *sa;
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;

	LOG_FUNC_START(1);

	memset(&ext_hdrs[0], 0, sizeof(void *) * SADB_EXT_MAX);

	if (_pfkey_parse_exthdrs(hdr, ext_hdrs) < 0)
		goto out;

	pfkey_dump(hdr, ext_hdrs);

	sadb_x_policy = ext_hdrs[SADB_X_EXT_POLICY - 1];
	msg->spd.policy_id = sadb_x_policy->sadb_x_policy_id;
	msg->spd.dir = sadb_x_policy->sadb_x_policy_dir;

	/*
	 * Parse source and destination addresses and create traffic
	 * selector from them....
	 */
	msg->spd.tsi = pfkey_parse_address_ext(
				ext_hdrs[SADB_EXT_ADDRESS_SRC - 1]);

	if (!msg->spd.tsi)
		goto out;

	msg->spd.tsr = pfkey_parse_address_ext(
				ext_hdrs[SADB_EXT_ADDRESS_DST - 1]);

	/*
	 * TODO: Maybe we should send error to a kernel...
	 * BUG:  We should certainly free alocated memory!!
	 */
	if (!msg->spd.tsr)
		goto out;

	/*
	 * Correct port numbers in newly created traffic selectors...
	 */
	if (!ts_get_eport(msg->spd.tsi))
		ts_set_eport(msg->spd.tsi, 65535);

	if (!ts_get_eport(msg->spd.tsr))
		ts_set_eport(msg->spd.tsr, 65535);

	switch(sadb_x_policy->sadb_x_policy_type) {
	case IPSEC_POLICY_DISCARD:
	case IPSEC_POLICY_NONE:
		g_assert_not_reached();
		break;

	case IPSEC_POLICY_IPSEC:
		sadb_x_ipsecrequest = (struct sadb_x_ipsecrequest *)
						(sadb_x_policy + 1);
		msg->spd.mode = sadb_x_ipsecrequest->sadb_x_ipsecrequest_mode;
		msg->spd.reqid = sadb_x_ipsecrequest->sadb_x_ipsecrequest_reqid;

		switch (sadb_x_ipsecrequest->sadb_x_ipsecrequest_proto) {
		case IPPROTO_AH:
			msg->spd.protocol = IKEV2_PROTOCOL_AH;
			break;
		case IPPROTO_ESP:
			msg->spd.protocol = IKEV2_PROTOCOL_ESP;
			break;
#if 0
		case IPPROTO_IPCOMP:
			break;
#endif
		default:
			LOG_ERROR("Unknown IPSEC PROTOCOL (%d) in PFKEY "
					"message", sadb_x_ipsecrequest->sadb_x_ipsecrequest_proto);
			goto out;
		}

		if (msg->spd.mode == IPSEC_MODE_TUNNEL) {
			sa = (struct sockaddr *)(sadb_x_ipsecrequest + 1);
			if (sa->sa_family == AF_INET) {
				sin = (struct sockaddr_in *)sa;
				msg->spd.tunnel_saddr =
					netaddr_new_from_inaddr(&sin->sin_addr);
				sin++;
				msg->spd.tunnel_daddr =
					netaddr_new_from_inaddr(&sin->sin_addr);
			} else {
				sin6 = (struct sockaddr_in6 *)sa;
				msg->spd.tunnel_saddr =
					netaddr_new_from_in6addr(&sin6->sin6_addr);
				sin6++;
				msg->spd.tunnel_daddr =
					netaddr_new_from_in6addr(&sin6->sin6_addr);
			}
		}
		break;

	case IPSEC_POLICY_ENTRUST:
	case IPSEC_POLICY_BYPASS:
	default:
		g_assert_not_reached();
		break;
	}

	ret = TRUE;

out:
	LOG_FUNC_END(1);

	return ret;
}

/**
 * Function that callects data from socket when they are available
 *
 * @param source
 * @param condition
 * @param data
 *
 * TODO: Fix situations when asynchronous SADB_DELETE is received, while
 *       the other SADB_DELETE is expected. Also, analize all similar
 *	 situations...
 */
gboolean ikev2_pfkey_data_pending_cb(GIOChannel *source,
			GIOCondition condition, gpointer data)
{
	struct sadb_msg *hdr;
	struct pfkey_msg *msg;

	LOG_FUNC_START(1);

	if (_pfkey_recv((void *)&hdr) <= 0)
		return TRUE;

	LOG_DEBUG("Received %s (pid=%d, seq=%d)",
			pfkey_msg2str(hdr->sadb_msg_type),
			hdr->sadb_msg_pid, hdr->sadb_msg_seq);

	msg = pfkey_msg_new();

	msg->action = hdr->sadb_msg_type;
	msg->errno_val = hdr->sadb_msg_errno;
	msg->seq = hdr->sadb_msg_seq;
	msg->pid = hdr->sadb_msg_pid;

	switch (hdr->sadb_msg_type) {
	case SADB_GETSPI:
		/*
		 * Find out if lock is held on a local PFKEY queue. If
		 * so, then it means some function in this module
		 * awaits for response from the kernel...
		 */
		if (g_mutex_trylock(pfkey_data->mlqueue)) {
			g_mutex_unlock(pfkey_data->mlqueue);
			pfkey_msg_free(msg);
			break;
		}

		/*
		 * Additional sanity check: is the response for a
		 * message sent by this process ID? (TODO: Do all the
		 * threads have the same value for getpid()?)
		 */
		if (hdr->sadb_msg_pid != getpid()) {
			pfkey_msg_free(msg);
			break;
		}

		msg->getspi_spi = _pfkey_parse_sa_ext(
					(struct sadb_sa *)(hdr + 1));
		g_async_queue_push(pfkey_data->lqueue, msg);
		break;

	case SADB_UPDATE:
	case SADB_ADD:
	case SADB_DELETE:
		/*
		 * Find out if lock is held on a local PFKEY queue. If
		 * so, then it means some function in this module
		 * awaits for response from the kernel...
		 */
		if (g_mutex_trylock(pfkey_data->mlqueue)) {
			g_mutex_unlock(pfkey_data->mlqueue);
			pfkey_msg_free(msg);
			break;
		}

		/*
		 * Additional sanity check: is the response for a
		 * message sent by this process ID? (TODO: Do all the
		 * threads have the same value for getpid()?)
		 */
		if (hdr->sadb_msg_pid != getpid()) {
			pfkey_msg_free(msg);
			break;
		}

		g_async_queue_push(pfkey_data->lqueue, msg);
		break;

	case SADB_ACQUIRE:
		/*
		 * Do not send ACQUIRE messages if we are closing daemon...
		 */
		if (pfkey_data->state == PFKEY_STATE_SHUTDOWN) {
			pfkey_msg_free(msg);
			break;
		}

		if (pfkey_parse_sadb_acquire(hdr, msg))
			g_async_queue_push (pfkey_data->queue, msg);
		else
			pfkey_msg_free(msg);
		break;

	case SADB_EXPIRE:
		if (!pfkey_parse_sadb_expire(hdr, msg)) {
			g_async_queue_push (pfkey_data->queue, msg);
		} else {
			LOG_DEBUG("Dumping message because of errors");
			pfkey_msg_free(msg);
		}
		break;

	case SADB_GET:
	case SADB_REGISTER:
	case SADB_FLUSH:
	case SADB_DUMP:
	case SADB_X_SPDUPDATE:
	case SADB_X_SPDFLUSH:
	case SADB_X_SPDEXPIRE:
	case SADB_X_NAT_T_NEW_MAPPING:
		LOG_ERROR("Ignoring PFKEY message %u\n", hdr->sadb_msg_type);
		break;

	case SADB_X_SPDADD:
		if (hdr->sadb_msg_pid != getpid()) {
			pfkey_msg_free(msg);
			break;
		}

		if (g_mutex_trylock(pfkey_data->mlqueue)) {
			g_mutex_unlock(pfkey_data->mlqueue);
			pfkey_msg_free(msg);
			break;
		}

		if (msg->errno_val) {
			g_async_queue_push(pfkey_data->lqueue, msg);
		} else if (pfkey_parse_sadb_x_spdadd(hdr, msg)) {
			g_async_queue_push(pfkey_data->lqueue, msg);
		} else {
			pfkey_msg_free(msg);
		}

		break;

	case SADB_X_SPDDELETE2:
	case SADB_X_SPDDELETE:

		if (hdr->sadb_msg_pid != getpid()) {
			pfkey_msg_free(msg);
			break;
		}

		if (g_mutex_trylock(pfkey_data->mlqueue)) {
			g_mutex_unlock(pfkey_data->mlqueue);
			pfkey_msg_free(msg);
			break;
		}

		g_async_queue_push(pfkey_data->lqueue, msg);

		break;

	case SADB_X_SPDGET:
	case SADB_X_SPDDUMP:
		if (hdr->sadb_msg_pid != getpid()) {
			pfkey_msg_free(msg);
			break;
		}

		if (g_mutex_trylock(pfkey_data->mlqueue)) {
			g_mutex_unlock(pfkey_data->mlqueue);
			pfkey_msg_free(msg);
			break;
		}

		if (msg->errno_val) {
			g_async_queue_push(pfkey_data->lqueue, msg);
		} else if (pfkey_parse_sadb_x_spdget(hdr, msg)) {
			g_async_queue_push(pfkey_data->lqueue, msg);
		} else {
			pfkey_msg_free(msg);
		}

		break;

	default:
		LOG_BUG("Unhandled PFKEY message!");
		pfkey_msg_free(msg);
		break;
	}

	g_free(hdr);

	LOG_FUNC_END(1);

	return TRUE;
}

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/

int pfkey_dump(struct sadb_msg *hdr, void **ext_hdrs)
{
	struct sockaddr *sa;
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	struct sadb_address *sadb_address;
	struct sadb_x_policy *sadb_x_policy;
	struct sadb_x_ipsecrequest *sadb_x_ipsecrequest;
	struct sadb_x_sa2 *sadb_x_sa2;

	if (ext_hdrs[SADB_EXT_ADDRESS_SRC-1]) {

		sadb_address = ext_hdrs[SADB_EXT_ADDRESS_SRC-1];

		LOG_DEBUG("SADB_EXT_ADDRESS_SRC dump");
		LOG_DEBUG("sadb_address_len=%d",
				sadb_address->sadb_address_len);
		LOG_DEBUG("sadb_address_exttype=%d",
				sadb_address->sadb_address_exttype);
		LOG_DEBUG("sadb_address_proto=%d",
				sadb_address->sadb_address_proto);
		LOG_DEBUG("sadb_address_prefixlen=%d",
				sadb_address->sadb_address_prefixlen);
		netaddr_dump_sockaddr(LOGGERNAME,
				(struct sockaddr *)(sadb_address + 1));
	}

	if (ext_hdrs[SADB_EXT_ADDRESS_DST-1]) {

		sadb_address = ext_hdrs[SADB_EXT_ADDRESS_DST-1];

		LOG_DEBUG("SADB_EXT_ADDRESS_DST dump\n");
		LOG_DEBUG("sadb_address_len=%d",
				sadb_address->sadb_address_len);
		LOG_DEBUG("sadb_address_exttype=%d",
				sadb_address->sadb_address_exttype);
		LOG_DEBUG("sadb_address_proto=%d",
				sadb_address->sadb_address_proto);
		LOG_DEBUG("sadb_address_prefixlen=%d",
				sadb_address->sadb_address_prefixlen);
		netaddr_dump_sockaddr(LOGGERNAME,
				(struct sockaddr *)(sadb_address + 1));
	}

	if (ext_hdrs[SADB_EXT_ADDRESS_PROXY-1]) {

		sadb_address = ext_hdrs[SADB_EXT_ADDRESS_PROXY-1];

		LOG_DEBUG("SADB_EXT_ADDRESS_PROXY dump");
		LOG_DEBUG("sadb_address_len=%d",
				sadb_address->sadb_address_len);
		LOG_DEBUG("sadb_address_exttype=%d",
				sadb_address->sadb_address_exttype);
		LOG_DEBUG("sadb_address_proto=%d",
				sadb_address->sadb_address_proto);
		LOG_DEBUG("sadb_address_prefixlen=%d",
				sadb_address->sadb_address_prefixlen);
		netaddr_dump_sockaddr(LOGGERNAME,
				(struct sockaddr *)(sadb_address + 1));
	}

	if (ext_hdrs[SADB_X_EXT_POLICY-1]) {

		sadb_x_policy = ext_hdrs[SADB_X_EXT_POLICY-1];

		LOG_DEBUG("SADB_X_EXT_POLICY dump");
		LOG_DEBUG("sadb_x_policy_len=%d",
			sadb_x_policy->sadb_x_policy_len);
		LOG_DEBUG("sadb_x_policy_exttype=%d",
			sadb_x_policy->sadb_x_policy_exttype);
		LOG_DEBUG("sadb_x_policy_type=%s(%d)",
			ipsec_policy_strs[sadb_x_policy->sadb_x_policy_type],
			sadb_x_policy->sadb_x_policy_type);
		LOG_DEBUG("sadb_x_policy_dir=%s(%d)",
			ipsec_dir_strs[sadb_x_policy->sadb_x_policy_dir],
			sadb_x_policy->sadb_x_policy_dir);
		LOG_DEBUG("sadb_x_policy_id=%d",
			sadb_x_policy->sadb_x_policy_id);
		LOG_DEBUG("sadb_x_policy_priority=%d",
			sadb_x_policy->sadb_x_policy_priority);

		sadb_x_ipsecrequest =
			(struct sadb_x_ipsecrequest *)(sadb_x_policy + 1);
		while ((char *)sadb_x_ipsecrequest < ((char *)sadb_x_policy +
					sadb_x_policy->sadb_x_policy_len * 8)) {

			LOG_DEBUG("SADB_X_EXT_IPSECREQUEST dump");
			LOG_DEBUG("sadb_x_ipsecrequest_len=%d",
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_len);
			LOG_DEBUG("sadb_x_ipsecrequest_proto=%d",
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_proto);
			LOG_DEBUG("sadb_x_ipsecrequest_mode=%d",
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_mode);
			LOG_DEBUG("sadb_x_ipsecrequest_level=%d",
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_level);
			LOG_DEBUG("sadb_x_ipsecrequest_reqid=%d",
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_reqid);

			if (sadb_x_ipsecrequest->sadb_x_ipsecrequest_mode ==
							IPSEC_MODE_TUNNEL) {
				sa = (struct sockaddr *)
						(sadb_x_ipsecrequest + 1);
				if (sa->sa_family == AF_INET) {
					sin = (struct sockaddr_in *)sa;
					netaddr_dump_sockaddr(LOGGERNAME,
						(struct sockaddr *)sin);
					netaddr_dump_sockaddr(LOGGERNAME,
						(struct sockaddr *)(sin + 1));
				} else {
					sin6 = (struct sockaddr_in6 *)sa;
					netaddr_dump_sockaddr(LOGGERNAME,
						(struct sockaddr *)sin6);
					netaddr_dump_sockaddr(LOGGERNAME,
						(struct sockaddr *)(sin6 + 1));
				}
			}
			sadb_x_ipsecrequest = (struct sadb_x_ipsecrequest *)(
					(char *)sadb_x_ipsecrequest +
				sadb_x_ipsecrequest->sadb_x_ipsecrequest_len * 8);
		}

	}

	if (ext_hdrs[SADB_X_EXT_SA2-1]) {

		sadb_x_sa2 = ext_hdrs[SADB_X_EXT_SA2-1];

		LOG_DEBUG("SADB_X_SA2 dump");

	}

	return 0;
}

const char *pfkey_msg2str(guint8 msg_type)
{
	if (msg_type > SADB_MAX)
		return "UNKNOWN SADB MSG TYPE!!!";

	return pfkey_message_types[msg_type];
}

/*******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/

void pfkey_set_shutdown_state()
{
	LOG_FUNC_START(1);

	pfkey_data->state = PFKEY_STATE_INIT;

	LOG_FUNC_END(1);
}

void pfkey_set_running_state()
{
	LOG_FUNC_START(1);

	pfkey_data->state = PFKEY_STATE_RUNNING;

	LOG_FUNC_END(1);
}

void pfkey_unload()
{
	LOG_FUNC_START(1);

	g_source_destroy(pfkey_data->pfkey_cb);

	g_io_channel_shutdown(pfkey_data->pfkeyfd_io_channel, FALSE, NULL);
	close(pfkey_data->pfkeyfd);

	g_async_queue_unref(pfkey_data->lqueue);
	g_mutex_free(pfkey_data->mlqueue);

	LOG_FUNC_END(1);
}

struct spd_op pfkey_spd_op = {
	.priv_data	= NULL,

//	.spd_getspi	= pfkey_getspi,
//	.spd_update	= pfkey_spdupdate,
	.spd_add	= pfkey_spdadd,
	.spd_delete	= pfkey_spddelete,
//	.spd_get	= pfkey_spdget,
//	.spd_acquire	= pfkey_spdacquire,
//	.spd_register	= pfkey_spdregister,
//	.spd_expire	= pfkey_spdexpire,
	.spd_flush	= pfkey_spdflush,
	.spd_dump	= pfkey_spddump,

	.spd_shutdown	= pfkey_set_shutdown_state,
	.spd_running	= pfkey_set_running_state,
	.spd_unload	= pfkey_unload
};

struct sad_op pfkey_sad_op = {
	.priv_data	= NULL,

	.sad_getspi	= pfkey_getspi,
	.sad_update	= pfkey_update,
	.sad_add	= pfkey_add,
	.sad_delete	= pfkey_delete,
//	.sad_get	= pfkey_get,
//	.sad_acquire	= pfkey_acquire,
//	.sad_register	= pfkey_register,
//	.sad_expire	= pfkey_expire,
//	.sad_flush	= pfkey_flush,
//	.sad_dump	= pfkey_dump,

	.sad_shutdown	= NULL,
	.sad_unload	= pfkey_unload
};

/**
 * Initialize IKEv2's subsystem for communication with Linux's SAD's
 * 
 *
 * Note 1
 *	This function runs before glib2's main loop so callback is not yet
 *	working and any called function that sends data through PFKEY
 *	interface has to received data by itself, i.e. responses will _not_
 *	come through callback mechanism.
 *
 * Note 3
 *	This function must be called _before_ pfkey_linux_spd_init
 *
 * Note 2
 *	This is new function for initializin pfkey subsystem.
 */
struct sad_op *pfkey_linux_sad_init(GMainContext *context, GAsyncQueue *queue)
{
	struct sad_op *retval = NULL;
	guint source_id;

	LOG_FUNC_START(1);

	pfkey_data = g_malloc0(sizeof(struct pfkey_data));

	pfkey_data->state = PFKEY_STATE_INIT;

	if ((pfkey_data->pfkeyfd = socket(PF_KEY, SOCK_RAW, PF_KEY_V2)) < 0) {
		LOG_PERROR(LOG_PRIORITY_FATAL);
		g_free(pfkey_data);
		goto out;
	}

	pfkey_data->mlqueue = g_mutex_new();

	/*
	 * TODO: Is queue necessary here because only one message at a
	 *	 time is expected...
	 */
	pfkey_data->lqueue = g_async_queue_new();

	pfkey_data->pfkeyfd_io_channel =
			g_io_channel_unix_new(pfkey_data->pfkeyfd);

        source_id = g_io_add_watch (pfkey_data->pfkeyfd_io_channel,
                                        G_IO_IN|G_IO_PRI,
                                        (GIOFunc) ikev2_pfkey_data_pending_cb,
                                        pfkey_data);

	pfkey_data->pfkey_cb =
			g_main_context_find_source_by_id(context, source_id);

	pfkey_data->transforms = pfkey_register(IKEV2_PROTOCOL_ESP, TRUE);

	if (pfkey_data->transforms == NULL) {
		LOG_ERROR("Error registering PFKEY protocols");
		goto out;
	}

	pfkey_register(IKEV2_PROTOCOL_AH, FALSE);

	pfkey_data->queue = queue;

	retval = &pfkey_sad_op;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Initialize IKEv2's subsystem for communication with Linux's SPD's
 * 
 *
 * Note 1
 *	This function runs before glib2's main loop so callback is not yet
 *	working and any called function that sends data through PFKEY
 *	interface has to received data by itself, i.e. responses will _not_
 *	come through callback mechanism.
 *
 * Note 3
 *	This function must be called _after_ pfkey_linux_sad_init
 *
 * Note 2
 *	This is new function for initializin pfkey subsystem.
 */
struct spd_op *pfkey_linux_spd_init(GMainContext *context, GAsyncQueue *queue)
{
	struct spd_op *retval = NULL;

	LOG_FUNC_START(1);

	retval = &pfkey_spd_op;

	LOG_FUNC_END(1);

	return retval;
}
