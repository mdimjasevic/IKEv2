/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __IKEV2_H
#define __IKEV2_H

struct ikev2_data {

	/**
	 * Configuration data and appropriate lock. When anything is
	 */
	GStaticRWLock config_lock;
	struct config *config;

	/**
	 * Main application loop data
	 */
	GMainLoop *mainloop;

	/**
	 * Are we in a process of terminating IKEv2 daemon...
	 */
	gboolean shutdown;

	/**
	 * RW lock that has to be acquired before anything in done with
	 * session list. If only reading is required, then reader lock
	 * is taken. If writting is neccessary then writer lock has to
	 * be taken. There could be multiple readers in parallel, but
	 * only one writer without any readers...
	 */
	GStaticRWLock session_lock;
	GSList *sessions;

	/**
	 * Additional data specific to state machines
	 */
	GAsyncQueue *queue;
	GThreadPool *thread_pool;

	/**
	 * Counter of half-open connections. When this counter exceeds
	 * value dos_treshold (from configuration file), IKEv2 daemon
	 * will modify its behavior and start to request cookies for
	 * each received IKE_SA_INIT request.
	 */
	guint16 half_opened_sessions;

	/**
	 * The following three fields are used for generating cookies...
	 * secret is currently used secret for generating cookies, while
	 * secret_version is it's version. old_secret is previously used
	 * secret that't used for some time after it is replaced as active.
	 */
	char *secret;
	char *old_secret;

	/**
	 * This parameter is used in Initiator. When initiator establishes
	 * connection with responder it has to know which config_peer
	 * structure/section to use. This parameter is populated from
	 * command line and identifies ID to use.
	 */
	struct id *rid;
};

#endif /* __IKEV2_H */
