/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __CFG_MODULE_H
#define __CFG_MODULE_H

#ifdef CFG_MODULE

#define CFG_MSG		6

#define IKEV2_MODULE_ENV_ADDR4			"IKEV2_IPV4_ADDR"
#define IKEV2_MODULE_ENV_ADDR6			"IKEV2_IPV6_ADDR"
#define IKEV2_MODULE_ENV_IF			"IKEV2_INTERFACE"
#define IKEV2_MODULE_ENV_VPNGW_IPV4_ADDR	"IKEV2_VPNGW_IPV4_ADDR"
#define IKEV2_MODULE_ENV_VPNGW_IPV6_ADDR	"IKEV2_VPNGW_IPV6_ADDR"
#define IKEV2_MODULE_ENV_RW_IP4_ADDR		"IKEV2_RW_IPV4_ADDR"
#define IKEV2_MODULE_ENV_RW_IP6_ADDR		"IKEV2_RW_IPV6_ADDR"

/**
 * Type that can be found in cfg_av structure
 */
enum {
	CFG_AV_TYPE_NONE,
	CFG_AV_TYPE_SINGLE,
	CFG_AV_TYPE_RANGE,
	CFG_AV_TYPE_LIST
};

/**
 * Provider types supported by ikev2
 */
enum {
	CFG_PROTO_NONE,
	CFG_PROTO_DHCPV4,
	CFG_PROTO_PRIVATE,
	CFG_PROTO_DHCPV6
};

/**
 * Structure to be exchanged between state machines and cfg module
 */
struct cfg_msg {

	/**
	 * Message type, discriminator value
	 */
	guint8 msg_type;

	/**
	 * Number of concurrent copies of the message.
	 *
	 * This field is used to count number of expected responses.
	 */
	gint refcnt;

	/**
	 * Session on whose behalf this message has been sent. It
	 * is not used inside CFG module.
	 */
	gpointer session;
};

struct cfg_data;
struct cfg_config_provider;

/**
 * Info about provider
 *
 * This structure consists of a global section, avaiable to everyone,
 * and a provider specific section for provider to keep it's specific
 * data.
 *
 * Currently, ikev2 knows for only two providers, DHCPv4 and private
 * one. The plan is to add DHCPv6 support.
 */
struct provider {

	/**
	 * Reference counter on number of users
	 */
	gint refcnt;

	/**
	 * ID of the provider
	 */
	gchar *pid;

	/**
	 * Supported parameters by the provider
	 */
	guint32 flags;

	/**
	 * Initialization function. Creates a new client
	 */
	gint (*init)(struct cfg_data *, struct cfg_config_provider *);

	/**
	 * Resets a client to a initial state
	 */
	void (*reset)(struct cfg_data *);

	/**
	 * Check if client is in a bound state
	 */
	int (*isbound)(struct cfg_data *);

	/**
	 * Check if client is in error state
	 */
	int (*iserror)(struct cfg_data *);

	/**
	 * Initiate request for configuration data
	 *
	 * Parameters are as follows:
	 * cfg_data			Configuration data requested by
	 * 				a client
	 * GAsyncQueue			In case the request is asynchronous
	 * 				the notification about completition
	 * 				of request will be sent on this
	 * 				queue.
	 * gpointer			Data that should be sent on the
	 * 				queue as the notification.
	 */
	int (*request)(struct cfg_data *, GAsyncQueue *, gpointer);

	/**
	 * Initiate release of any configuration data.
	 */
	int (*release)(struct cfg_data *, GAsyncQueue *, gpointer);

	/**
	 * Release all the resources that client has. Note that
	 * this function does not necessariliy initiate release
	 * to a server!
	 */
	void (*free)(struct cfg_data *);

	/**
	 * Initiate release of any configuration data
	 */
	gchar *(*state)(struct cfg_data *);

	/**
	 * Dump provider private data
	 */
	void (*dump)(struct cfg_data *);

};

/**
 * Data exchanged between module and it's users
 */
struct cfg_data {

	/**
	 * Prioritized list of providers.
	 *
	 * This is a list of 'struct cfg_config_provider' structures
 	 */
	GSList *ccpl;

	/**
	 * Dynamically assigned/requested configuration data
	 */
	struct cfg *cfg;

	/**
	 * Client ID
	 */
	struct id *cid;

#ifdef CFG_PRIVATE
	/**
	 * State if the client uses private (file) provider
	 */
	void *private_data;
#endif /* CFG_PRIVATE */

#ifdef CFG_DHCPV4
	/**
	 * State if the client uses DHCP4 provider
	 */
	void *provider4_data;
#endif /* CFG_DHCPV4 */
};

/**
 * Attribute-value pair structure
 */
struct cfg_av {
	/**
	 * Attribute name
	 */
	gchar *attribute;

	/**
	 * Value' type, to discriminate union
	 */
	guint8 val_type;

	/**
	 * Attribute's value
	 */
	union {
		/**
		 * Single value
		 */
		gchar *string;

		/**
		 * A list (of strings) values
		 */
		GSList *list;

		/**
		 * Range value (lower - upper)
		 */
		struct {
			gchar *lower;
			gchar *upper;
		};
	};

	/**
	 * Line in configuration file from where attr-value pair has been read
	 */
	guint line;
};

/**
 * Configuration structure per module/provider
 */
struct cfg_config_provider {

	/**
	 * Reference couter on number of users for this structure
	 */
	gint refcnt;

	/**
	 * Configuration ID, i.e. free form string identifying
	 * this structure
 	 */
	gchar *id;

	/**
	 * Provider ID, i.e. private, DHCPv4, DHCPv6
 	 */
	gint provider_type;

	/**
	 * Supported configuration parameters by this peer
 	 */
	guint32 flags;

	/**
	 * Actuall provider bound to this configuration
	 */
	struct provider *provider;

	/**
	 * Data shared by all the instances of this configuration
	 *
	 * This structure is populated from attribute value pairs
	 * read from the configuration file.
	 */
	void *data;

	/**
	 * List of attribute value pairs
	 */
	GSList *av;
};

/*******************************************************************************
 * cfg_msg related functions
 ******************************************************************************/
void cfg_msg_free(struct cfg_msg *);

/*******************************************************************************
 * cfg_config related functions
 ******************************************************************************/
struct cfg_av *cfg_av_new();
void cfg_av_free(struct cfg_av *);
void cfg_av_dump(struct cfg_av *);
struct cfg_config_provider *cfg_config_provider_new();
void cfg_config_provider_ref(struct cfg_config_provider *);
void cfg_config_provider_free(struct cfg_config_provider *);
void cfg_config_provider_dump(struct cfg_config_provider *);
struct cfg_config *cfg_config_new();
void cfg_config_free(struct cfg_config *);
void cfg_config_dump(struct cfg_config *);

/*******************************************************************************
 * provider related functions
 ******************************************************************************/
struct provider *provider_new();

/*******************************************************************************
 * Public interface
 ******************************************************************************/
gpointer cfg_proxy_new(GSList *, struct cfg *, struct id *);
int cfg_proxy_request(struct cfg_data *, gpointer);
void cfg_proxy_release(struct cfg_data *);
void cfg_proxy_free(struct cfg_data *);
struct cfg *cfg_proxy_get_cfg(struct cfg_data *);
int cfg_data_get(struct cfg_data *);
int cfg_data_cancel(struct cfg_data *);
int cfg_data_release(struct cfg_data *);
void cfg_data_dump(struct cfg_data *);
gboolean cfg_proxy_is_error(struct cfg_data *);
void cfg_proxy_dump(struct cfg_data *);
GSList *cfg_proxy_get_netaddrs(struct cfg_data *);

int cfg_state_get_cid(gpointer, gpointer *, gint *);

/*******************************************************************************
 * Running external scripts to adjust environment for a new client
 ******************************************************************************/
int cfg_module_exec_script(gchar **, struct cfg *, struct netaddr *,
		gchar *, struct netaddr *);

/*******************************************************************************
 * Module initialization and deinitialization functions
 ******************************************************************************/
void cfg_unload();
int cfg_init(GSList *, GAsyncQueue *);

#endif /* CFG_MODULE */

#endif /* __CFG_H */
