/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CERT_H
#define __CERT_H

/*
 * Maximum length of string representation of DER_ASN1_DN
 */
#define DER_ASN1_DN_MAX_STR_LEN		512

/**
 * Structure with global data
 */
struct cert_data {
	/*
	 * Download thread
	 */
	GThread *download_thread;

	/*
	 * Queue for download requests
	 */
	GAsyncQueue *download_queue;

	/*
	 * Main queue to state machine
	 */
	GAsyncQueue *main_queue;
};

struct cert {
	guint8 encoding;
	unsigned char *pub_key;
	guint16 pub_key_len;
	unsigned char *certificate;
	guint16 certificate_len;
};

struct session_certificate {
	/**
	 * Pointer to session that uses a CA for verification
	 */
	gpointer session;

	/**
	 * Certificate verified with a CA
	 */
	struct cert *certificate;
};

/**
 * Certificate authority structure
 */
struct ca_item {
	/**
	 * CA type.
	 *
	 * One of IKEV2_CERT_HASH_AND_URL or IKEV2_CERT_X509_SIGNATURE
	 */
	guint8 type;

	/**
	 * Pointer to a localy stored CA.
	 *
	 * If this field is NULL then url field has to point to a place
	 * from where CA can be retrieved.
	 */
	char *file;

	/**
	 * URL where CA can be retrieved.
	 */
	char *url;

	/**
	 * Pointer to a CRL. This can be local (file:///), or somewhare on
	 * the Internet
	 */
	char *crl_url;

	/**
	 * How frequently to check CRL of this CA. The value is stored in
	 * seconds.
	 */
	guint32 check_interval;

	/**
	 * Path to a local file to hold downloaded CRL
	 */
	char *crl_file;

	/**
	 * Dynamic data: CRL's nextUpdate value
	 */
	ASN1_TIME *crl_next_update;

	/**
	 * Dynamic data: timestamp of local CRL file
	 */
	time_t crl_timestamp;

	/**
	 * Dynamic data: time when CRL will need updating
	 */
	time_t crl_update_time;

	/**
	 * List of sessions and certificates this CA is used to verify
	 * (elements of type 'struct session_certificates')
	 */
	GSList *session_certs;
};

/**
 * Certificate item defined in configuration file
 */
struct cert_item {

	/**
	 * Certificate type, either IKEV2_CERT_HASH_AND_URL or
	 * IKEV2_CERT_X509_SIGNATURE
	 */
	guint8 type;

	/**
	 * File with certificate
	 */
	char *file;

	/**
	 * File with private key
	 */
	char *priv_key_file;

	/**
	 * URL, if certificate is available on WEB/FTP/...
	 */
	char *url;

	struct id *id;
	struct id *peers_id;
};

struct certreq {
	guint8 encoding;
	unsigned char *hash_list;
	guint16 hash_list_len;
};

/**
 * Main download request structure; may include more single requests (items)
 * Create with dl_request_new(items)
 */
struct dl_request {
	/*
	 * Number of single download items; must be set in struct creation
	 */
	long items;
	GSList *dl_items;
	/*
	 * Download success; used by download thread
	 * (need not to be defined)
	 */
	gboolean success;
	/*
	 * Timeout for all items in seconds; 0 for system limit
	 */
	long timeout;
	/*
	 * Pointer to session that sent download request
	 */
	gpointer session;
	/*
	 * Pointer to the message that this session received
	 * (to preserve the context)
	 */
	gpointer sm_msg;
};

/**
 * Single download item structure (single document to download)
 */
struct dl_item {
	char *url;
	/*
	 * Download to memory (TRUE) or local file (FALSE)
	 */
	gboolean memory;
	/*
	 * Path and name of local file to save into
	 */
	char *dl_file;
	/*
	 * Pointer to and size of downloaded data
	 * Will get defined after download in download thread
	 */
	void **dl_mem;
	size_t *size;
};

/**
 * Internal download data storage structure
 * Used in the download thread for each download item (single document)
 */
 struct dl_data {
 	FILE *fp;
 	char *data;
 	size_t size;
 	void *handle;
 	gboolean active;
  	gboolean success;
  	struct dl_request *request;
	GSList *related_requests;
	struct dl_item *item;
};

/**
 * Download response structure
 */
 struct dl_response {
	gboolean success;
	gpointer session;
};

/*******************************************************************************
 * DL_REQUEST AND DL_ITEM STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/
struct dl_request *dl_request_new(int);
void dl_request_free(struct dl_request *);
void dl_item_free(struct dl_item *);

/*******************************************************************************
 * CA STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/
struct ca_item *ca_item_new();
void ca_item_free(struct ca_item *);
void ca_item_list_free(GSList *);
void ca_item_dump(struct ca_item *);

/*******************************************************************************
 * DER_ASN1_DN manipulation functions
 ******************************************************************************/
char *crypto_derasn1dn_to_string(gpointer);
gpointer crypto_string_to_derasn1dn(char *);
void crypto_derasn1dn_free(gpointer);
gboolean crypto_derasn1dn_cmp(gpointer, gpointer);

/*******************************************************************************
 * CERT STRUCTURE MANIPULATION FUNCTIONS
 ******************************************************************************/

/*
 * Certificate function
 */
gint32 crypto_cert_get_pubkey(guint8, unsigned char *, guint16,
		unsigned char **);
gint32 cert_load_data(int, char *, unsigned char **);
int crypto_create_hash_list(GSList *, char **);
gint32 crypto_read_pubkey_from_file(char *, unsigned char **);
GSList *crypto_check_hash_list(struct certreq *, GSList *);
int verify_callback(int, X509_STORE_CTX *);
int cert_verify(const char *, unsigned char *, guint16, const char *);
GSList *cert_prepare_cert_item_verify(GSList *, GSList *, gpointer);
GSList *cert_prepare_cert_verify(GSList *, GSList *, gpointer);
void cert_ca_items_remove(GSList *);
X509_CRL *cert_load_crl(const char *);
int cert_validate_crl(const char *);
ASN1_TIME *cert_get_crl_update(const char *);

char *cert_get_cert_subjectname(char *);
gboolean crypto_verify_id(GSList *, GSList *, struct id *);
gboolean crypto_cmp_id(int, char *, char *, int);
gboolean crypto_cmp_subjectnames(unsigned char *, unsigned char *, int);
char *cert_get_cert_subjectaltname(guint8, X509 *);
char *cert_get_subjectaltname(guint8, char *);
int cert_asn1_time_to_days(ASN1_TIME *);
int cert_asn1_time_diff_seconds(ASN1_TIME *, ASN1_TIME *);
ASN1_TIME *cert_get_cert_validity(char *);
guint32 cert_sa_limit_adjust(guint32, guint32, GSList *, GSList *);
struct dl_request *cert_update_crl_dl_request(struct dl_request *, GSList *);
int cert_check_auth_material(GSList *, GSList *, gpointer, gpointer, time_t *);
GSList *cert_find_appropriate_certs(GSList *, GSList *, GSList *, gboolean, gpointer);
GSList *cert_find_used_cas(GSList *, gpointer);
gboolean cert_verify_ca(struct ca_item *, gpointer);
gboolean cert_check_public_keys(GSList *, GSList *);

/*
 * Download functions
 */
int cert_libcurl_init(void);
void cert_libcurl_unload(void);
int cert_dltomem(char *, void *);
int cert_dltofile(char *, FILE *, long);
void cert_send_dl_response(struct dl_request *, GAsyncQueue *);
int cert_dl_init_handle(struct dl_item *, struct dl_data *);

void cert_unload(void);
int cert_init(GAsyncQueue *);

#endif /* __CERT_H */
