/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef CFG_PRIVATE

#define LOGGERNAME	"cfg_private"

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#include <net/if_arp.h>
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */

#include <glib.h>

#include "logging.h"
#include "netlib.h"

#include "auth.h"
#include "cfg.h"
#include "cfg_module.h"
#include "cfg_private.h"

/******************************************************************************
 * private_client manipulation functions
 ******************************************************************************/

/**
 * Allocate new private_client structure
 */
struct private_client *private_client_new()
{
	struct private_client *pc;

	pc = g_malloc0(sizeof(struct private_client));
	pc->iserror = FALSE;

	return pc;
}

/******************************************************************************
 * private_cfg manipulation functions
 ******************************************************************************/

/**
 * Allocate new private_cfg structure
 */
struct private_cfg *private_cfg_new()
{
	struct private_cfg *pcfg;

	pcfg = g_malloc0(sizeof(struct private_cfg));

	pcfg->refcnt = 1;
	pcfg->mutex = g_mutex_new();

	return pcfg;
}

/**
 * Reference cfg structure
 *
 * This function has to be called with a lock held on structure
 */
void private_cfg_ref(struct private_cfg *pc)
{
	g_atomic_int_inc(&(pc->refcnt));
}

/**
 * Free cfg structure
 *
 * This function has to be called with a lock held on structure
 */
void private_cfg_free(struct private_cfg *pc)
{
	if (pc && g_atomic_int_dec_and_test(&(pc->refcnt))) {

		if (pc->ipv4_pool_start)
			netaddr_free(pc->ipv4_pool_start);

		if (pc->ipv4_pool_end)
			netaddr_free(pc->ipv4_pool_end);

		while (pc->ipv4_nbns) {
			netaddr_free(pc->ipv4_nbns->data);
			pc->ipv4_nbns = g_slist_remove(pc->ipv4_nbns,
					pc->ipv4_nbns->data);
		}

		while (pc->ipv4_dhcp) {
			netaddr_free(pc->ipv4_dhcp->data);
			pc->ipv4_dhcp = g_slist_remove(pc->ipv4_dhcp,
					pc->ipv4_dhcp->data);
		}

		while (pc->ipv4_dns) {
			netaddr_free(pc->ipv4_dns->data);
			pc->ipv4_dns = g_slist_remove(pc->ipv4_dns,
					pc->ipv4_dns->data);
		}

		if (pc->ipv6_pool_start)
			netaddr_free(pc->ipv6_pool_start);

		if (pc->ipv6_pool_end)
			netaddr_free(pc->ipv6_pool_end);

		while (pc->ipv6_dhcp) {
			netaddr_free(pc->ipv6_dhcp->data);
			pc->ipv6_dhcp = g_slist_remove(pc->ipv6_dhcp,
					pc->ipv6_dhcp->data);
		}

		while (pc->ipv6_dns) {
			netaddr_free(pc->ipv6_dns->data);
			pc->ipv6_dns = g_slist_remove(pc->ipv6_dns,
					pc->ipv6_dns->data);
		}

		g_mutex_free(pc->mutex);

		g_free(pc);
	} else {
		LOG_DEBUG("private_cfg not free'd (refcnt=%d)",
				pc->refcnt);
	}
}

/******************************************************************************
 * private_client manipulation functions
 ******************************************************************************/

/**
 * Initialize state for a single client
 *
 * @param cfg_data	CFG state with data that should be obtained
 * @param ccp		Provider's instance data
 *
 * \return 0 if initialization has been successful, -1 otherwise
 */
gint private_client_init(struct cfg_data *cfg_data,
		struct cfg_config_provider *ccp)
{
	struct private_client *pc;

	LOG_FUNC_START(1);

	/*
	 * Initialize new client
	 */
	pc = g_malloc0(sizeof(struct private_client));

	cfg_data->private_data = pc;

	pc->pcfg = ccp->data;
	private_cfg_ref(pc->pcfg);

	LOG_FUNC_END(1);

	return 0;
}

/**
 * Release private_client structure
 */
void private_client_free(struct cfg_data *cd)
{
	struct private_client *pc;

	LOG_FUNC_START(1);

	pc = cd->private_data;

	if (pc) {

		if (private_client_is_bound(cd))
			private_client_release(cd, NULL, NULL);

		if (pc->pcfg)
			private_cfg_free(pc->pcfg);

		cd->private_data = NULL;

		g_free(pc);
	}

	LOG_FUNC_END(1);
}

/**
 * Reset client state and release all the memory.
 */
void private_client_reset(struct cfg_data *cd)
{
	struct private_client *pc;

	LOG_FUNC_START(1);

	pc = cd->private_data;

	if (private_client_is_bound(cd))
		private_client_release(cd, NULL, NULL);

	pc->iserror = FALSE;

	LOG_FUNC_END(1);
}

/**
 * Is the client in state BOUND?
 *
 * @param cfg_data
 *
 * \return TRUE if it is, FALSE otherwise
 */
gboolean private_client_is_bound(struct cfg_data *cfg_data)
{
	struct private_client *pc;

	LOG_FUNC_START(1);

	pc = cfg_data->private_data;

	LOG_FUNC_END(1);

	return pc->isbound;
}

/**
 * Is the client in state ERROR?
 *
 * @param cfg_data
 *
 * \return TRUE if it is, FALSE otherwise
 */
gboolean private_client_is_error(struct cfg_data *cfg_data)
{
	struct private_client *pc;

	LOG_FUNC_START(1);

	pc = cfg_data->private_data;

	LOG_FUNC_END(1);

	return pc->iserror;
}

/**
 * Dump current state into a log file
 *
 * @param cfg_data
 *
 * \return TRUE if it is, FALSE otherwise
 */
void private_client_dump(struct cfg_data *cfg_data)
{
	GSList *c;
	guint32 addr;
	gchar ipstr[INET6_ADDRSTRLEN];

	struct private_client *pc;

	pc = cfg_data->private_data;

	LOG_DEBUG("List of assigned IPv4 addresses");
	for (c = pc->addrs4; c; c = c->next) {
		addr = GPOINTER_TO_UINT(c->data);
		LOG_DEBUG("%s", inet_ntop(AF_INET, &addr,
				ipstr, INET6_ADDRSTRLEN));
	}

//	LOG_DEBUG("List of assigned IPv6 addresses");
//	for (c = pc->addrs6; c; c = c->next)
//		LOG_DEBUG("%s", inet_ntop(AF_INET6, c->data,
//				ipstr, INET6_ADDRSTRLEN));

}

/**
 * Search for given IPv4 address and try to renew it
 *
 * @param pcfg		Configuration data and state used by private provider
 * @param cid		Client ID for whom the address should be reallocated
 * @param addr		Address to reallocate, if NULL new address is allocated
 *
 * \return TRUE if successful, FALSE otherwise
 */
gboolean ipv4_address_alloc(struct cfg_data *cd, struct netaddr *addr)
{
	GSList *c;
	guint32 ipaddr, ipstart, ipend;
	guint16 offset;
	guint8 bit;
	struct private_client *pc;
	struct private_cfg *pcfg;
	struct netaddr *na;
	gchar ipstr[INET6_ADDRSTRLEN];
	gboolean retval;

	LOG_FUNC_START(1);

	retval = TRUE;

	pc = cd->private_data;
	pcfg = pc->pcfg;

	/*
 	 * Lock the structure until we are finished.
 	 */
	g_mutex_lock(pc->pcfg->mutex);

	/*
	 * Convert IPv4 address range into 32 bit numbers for
	 * easier manipulation.
	 */
	ipstart = ntohl(*(guint32 *)netaddr_get_ipaddr(pcfg->ipv4_pool_start));
	ipend = ntohl(*(guint32 *)netaddr_get_ipaddr(pcfg->ipv4_pool_end));

	if (addr) {

		/*
		 * Check that requested IPv4 address is within
		 * valid range.
		 */
		ipaddr = ntohl(*(guint32 *)netaddr_get_ipaddr(addr));

		if (ipaddr < ipstart || ipaddr > ipend) {
			LOG_WARNING("Client requested IPv4 address outside of "
					"allowed IPv4 range. Ignoring!");
			retval = FALSE;
			goto out;
		}

		/*
		 * Check if the given address is allocated. If not, we don't
		 * have to search for it.
		 */
		offset = (ipaddr - ipstart) >> 3;
		bit = 1 << ((ipaddr - ipstart) & 0x07);

		/*
		 * If the address is free, then reserve it
		 */
		if  (!(pcfg->ipv4_bitmap[offset] & bit)) {

			/*
			 * Add the newly allocated address to a client
			 */
			pc->addrs4 = g_slist_append(pc->addrs4,
					GUINT_TO_POINTER(htonl(ipaddr)));

			pcfg->ipv4_bitmap[offset] |= bit;

			LOG_NOTICE("Granted client's requested for ip address"
					" %s", netaddr_ip2str(addr, ipstr,
							INET6_ADDRSTRLEN));

			goto out;
		}

		/*
		 * Iterate over allocated list of IPv4 addresses
		 * checking if this address is allocated to the
		 * client
		 */
		for(c = pc->addrs4; c; c = c->next) {

			if (GPOINTER_TO_UINT(c->data) == htonl(ipaddr)) {

				LOG_NOTICE("Granted client's renewal request "
						" for ip address %s",
						netaddr_ip2str(addr, ipstr,
							INET6_ADDRSTRLEN));

				goto out;
			}
		}

		/*
		 * Address is allocated but to some other client, so log
		 * an warning and return from the function.
		 */
		LOG_WARNING("Client tried to renew address %s allocated to "
				"someone else!",
				netaddr_ip2str(addr, ipstr,
					INET6_ADDRSTRLEN));
		retval = FALSE;
		goto out;
	}

	/*
	 * If we came here, then no specific address has been requested
	 * and thus, search for the first available one.
	 */
	g_assert(addr == NULL);

	for (ipaddr = ipstart; ipaddr <= ipend; ipaddr++) {


		/*
		 * Check if the given address is allocated. If not, we don't
		 * have to search for it.
		 */
		offset = (ipaddr - ipstart) >> 3;
		bit = 1 << ((ipaddr - ipstart) & 0x07);

		/*
		 * If the address is free, then reserve it
		 */
		if  (!(pcfg->ipv4_bitmap[offset] & bit)) {

			pcfg->ipv4_bitmap[offset] |= bit;

			/*
			 * Add the newly allocated address to a client
			 */
			ipaddr = htonl(ipaddr);

			pc->addrs4 = g_slist_append(pc->addrs4,
					GUINT_TO_POINTER(ipaddr));

			na = netaddr_new_from_inaddr((struct in_addr *)&ipaddr);

			cd->cfg->netaddrs = g_slist_append(
						cd->cfg->netaddrs, na);

			LOG_NOTICE("Client's was given address %s",
					netaddr_ip2str(na, ipstr,
						INET6_ADDRSTRLEN));

			goto out;
		}
	}

	LOG_ERROR("No available IPv4 addresses in the pool!");
	retval = FALSE;

out:
	/*
 	 * Lock the structure until we are finished.
 	 */
	g_mutex_unlock(pcfg->mutex);

	return FALSE;
}

/**
 * Private function that searches for a IPv6 address in a specified range
 *
 * @param pcs		List of active clients.
 * @param nastart	Start address to search for free IPv6 address
 * @param naend		End address to search for free IPv6 address
 *
 * \return New private_client structure, NULL if no address has been found
 *
 * If address is found, private_client structure is reserved, filled in
 * with assigned address, and this structure is placed inside list of
 * active clients on the appropriate place.
 *
 * IPv6 is a problematic since it requires 128 bits and largest integer
 * value offered by compiler has 64 bits. So, we break address into
 * lower and higher 64 bits. Note that it highly probable that all
 * address will have only lower 64 bits different, and maybe even less
 * than that.
 */
static struct private_client *ipv6_address_allocate(GSList *pcs,
		struct netaddr *nastart, struct netaddr *naend)
{
#if 0
	GSList *c;
	guint64 ipaddr, ipend, ipcurr;
	guint64 ipaddr_hi, ipend_hi, ipcurr_hi;
	struct private_client *pc, *pcc;

	ipaddr_hi = IPV6_GET_HI64(netaddr_get_ipaddr(nastart));
	ipend_hi = IPV6_GET_HI64(netaddr_get_ipaddr(naend));

	ipaddr = IPV6_GET_LO64(netaddr_get_ipaddr(nastart));
	ipend = IPV6_GET_LO64(netaddr_get_ipaddr(naend));

	for (c = pcs; c && ((ipaddr_hi < ipend_hi) ||
			(ipaddr_hi == ipend_hi && ipaddr < ipend));
			c = c->next) {
		pcc = c->data;

		ipcurr = IPV6_GET_LO64(netaddr_get_ipaddr(pcc->na));
		ipcurr_hi = IPV6_GET_HI64(netaddr_get_ipaddr(pcc->na));

		if (ipaddr == ipcurr && ipaddr_hi == ipcurr_hi) {
			ipaddr++;
			if (!ipaddr)
				ipaddr_hi++;

			continue;
		}

		if ((ipaddr_hi < ipcurr_hi) || (ipaddr_hi == ipcurr_hi
				&& ipaddr < ipcurr)) {
			pc = private_client_new();
			g_assert_not_reached();
			pc->na = netaddr_new_from_in6addr(
					(struct in6_addr *)&ipaddr);
			return pc;
		}
	}

	/*
	 * Special case. We came to the end of the list
	 * and there is no reserved IPv4 address, so, we can
	 * reserve it now!
	 */
	if (!c && ((ipaddr_hi < ipend_hi) ||
			(ipaddr_hi == ipend_hi && ipaddr < ipend))) {
		pc = private_client_new();
		g_assert_not_reached();
		pc->na = netaddr_new_from_inaddr((struct in_addr *)&ipaddr);
		return pc;
	}

	/*
	 * No free address has been found!
	 */
#endif
	return NULL;
}

/**
 * Request parameters for a client
 *
 * @param ccp		Provider's private configuration data
 * @param cd		Client for whom request should be made
 * @param queue		In case request is asynchronous, queue on which
 *			message	about completition will be sent. This
 *			provider doesn't use this parameter!
 * @param data		Data to be sent on a queue This provider doesn't
 *			use this parameter!
 *
 * \return	-1 if an error occured
 *		0 if request has been satisfied
 *		1 if request is in progress
 */
int private_client_request(struct cfg_data *cd, GAsyncQueue *queue,
		gpointer data)
{
	GSList *c;
	struct private_client *pc;
	struct netaddr *na;
	gint noips;
	gint retval;

	LOG_FUNC_START(1);

	retval = -1;

	pc = cd->private_data;

	/*
	 * If the client requested IPv4 address(es), try to
	 * reserve/renew them.
	 *
	 * Note that client has an option to request specific IP
	 * address by stating some specific IPv4 address.
	 */
	if (cd->cfg->flags & OPTION_F4_ADDR) {

		/**
		 * First, check if client requested some specific IPv4
		 * address(es) and in that case we'll try to use it.
		 */
		if (!cd->cfg->netaddrs)
			noips = 1;
		else
			noips = 0;

		for (c = cd->cfg->netaddrs; c; c = c->next)

			/*
			 * Is IPv4 address specified?
			 */
			if (netaddr_get_family(c->data) == AF_INET) {
				na = c->data;

				/*
				 * Client searched for specific IP
				 * addresses, try to renew it.
				 */
				if (ipv4_address_alloc(cd, na))
					continue;

				/*
				 * We were not able to renew the
				 * requested address. So, remove it
				 * from the list and increment number
				 * of requested addresses.
				 */
				cd->cfg->netaddrs = g_slist_remove(
						cd->cfg->netaddrs, na);

				netaddr_free(na);

				noips++;

			} else
				/*
				 * Mark that the client requested address
				 */
				noips++;

		/*
		 * Additional IPv4 address allocation
		 */
		while (noips--) {

			/*
			 * Find and allocate free IPv4 address
			 */
			if (!ipv4_address_alloc(cd, NULL)) {

				/*
				 * If there are no more free IP addresses
				 * terminate further processing
				 */
				LOG_WARNING("Client requested %d more"
						" IPv4 addresses which are"
						" not available.", noips);
				break;
			}
		}
	}

	/*
	 * If the client requested IPv6 address, reserve one.
	 *
	 * Note that client has an option to request specific IP address.
	 *
	 * There is a specific quirk in this case. Namely, client can
	 * specify only Interface ID, and we pass it a complete address
	 * by prepending network mask. TODO: This is not yet implemented!
	 */
	if (cd->cfg->flags & OPTION_F6_ADDR) {
		g_assert_not_reached();
	}

	/*
	 * Cleanup all the other resources. We allow only IP address
	 * to be suggested by the client!
	 */
	while (cd->cfg->dns) {
		netaddr_free(cd->cfg->dns->data);
		cd->cfg->dns = g_slist_remove(cd->cfg->dns,
					cd->cfg->dns->data);
	}

	while (cd->cfg->dhcp) {
		netaddr_free(cd->cfg->dhcp->data);
		cd->cfg->dhcp = g_slist_remove(cd->cfg->dhcp,
					cd->cfg->dhcp->data);
	}

	while (cd->cfg->nbns) {
		netaddr_free(cd->cfg->nbns->data);
		cd->cfg->nbns = g_slist_remove(cd->cfg->nbns,
					cd->cfg->nbns->data);
	}

	if (cd->cfg->flags & OPTION_F4_NETMASK)
		cd->cfg->netmask = pc->pcfg->ipv4_netmask;

	if (cd->cfg->flags & OPTION_F4_DNS) {
		for (c = pc->pcfg->ipv4_dns; c; c = c->next)
			cd->cfg->dns = g_slist_append(cd->cfg->dns,
						netaddr_dup_ro(c->data));
	}

	if (cd->cfg->flags & OPTION_F4_DHCP) {
		for (c = pc->pcfg->ipv4_dhcp; c; c = c->next)
			cd->cfg->dhcp = g_slist_append(cd->cfg->dhcp,
						netaddr_dup_ro(c->data));
	}

	if (cd->cfg->flags & OPTION_F4_NBNS) {
		for (c = pc->pcfg->ipv4_nbns; c; c = c->next)
			cd->cfg->nbns = g_slist_append(cd->cfg->nbns,
						netaddr_dup_ro(c->data));
	}

	if (cd->cfg->flags & OPTION_F6_DNS) {
		for (c = pc->pcfg->ipv6_dns; c; c = c->next)
			cd->cfg->dns = g_slist_append(cd->cfg->dns,
						netaddr_dup_ro(c->data));
	}

	if (cd->cfg->flags & OPTION_F6_DHCP) {
		for (c = pc->pcfg->ipv6_dhcp; c; c = c->next)
			cd->cfg->dhcp = g_slist_append(cd->cfg->dhcp,
						netaddr_dup_ro(c->data));
	}

	retval = 0;

	/*
	 * Mark that client is now in bound state
	 */
	pc->isbound = TRUE;

	LOG_FUNC_END(1);

	return retval;
}

void ipv4_address_free(struct cfg_data *cd, struct netaddr *addr)
{
	struct private_client *pc;
	struct private_cfg *pcfg;
	guint32 ipstart, ipend, ipaddr;
	char ipstr[INET6_ADDRSTRLEN];
	guint16 offset;
	guint8 bit;

	pc = cd->private_data;
	pcfg = pc->pcfg;

	if (!addr)
		return;

	/*
	 * Convert IPv4 address range into 32 bit numbers for
	 * easier manipulation.
	 */
	ipstart = ntohl(*(guint32 *)netaddr_get_ipaddr(pcfg->ipv4_pool_start));
	ipend = ntohl(*(guint32 *)netaddr_get_ipaddr(pcfg->ipv4_pool_end));

	/*
	 * Check that requested IPv4 address is within
	 * valid range.
	 */
	ipaddr = ntohl(*(guint32 *)netaddr_get_ipaddr(addr));

	if (ipaddr < ipstart || ipaddr > ipend) {
		LOG_WARNING("Client requested release of the IPv4 address "
				"outside of the allowed IPv4 range. Ignoring!");
			goto out;
	}

	/*
	 * Check if the given address is allocated. If not, we don't
	 * have anything to release.
	 */
	offset = (ipaddr - ipstart) >> 3;
	bit = 1 << ((ipaddr - ipstart) & 0x07);

	/*
	 * If the address is free, then reserve it
	 */
	if  (pcfg->ipv4_bitmap[offset] & bit) {

		/*
		 * Remove allocated address from the client
		 */
		pc->addrs4 = g_slist_remove(pc->addrs4,
				GUINT_TO_POINTER(htonl(ipaddr)));

		pcfg->ipv4_bitmap[offset] &= ~bit;

		LOG_NOTICE("Released IPv4 address %s",
				netaddr_ip2str(addr, ipstr,
					INET6_ADDRSTRLEN));
	}

out:
	return;
}

/**
 * Initiate RELEASE of acquired resources
 *
 * @param cd		Client whose resources have to be released
 * @param queue
 * @param data
 *
 * \return	-1 if an error occured
 *		0 if request has been satisfied
 *		1 if request is in progress
 *
 * This function only releases private data, it doesn't release
 * configuration data stored in cd->cfg structure!
 */
int private_client_release(struct cfg_data *cd, GAsyncQueue *queue,
		gpointer data)
{
	struct private_client *pc;
	GSList *c;

	LOG_FUNC_START(1);

	pc = cd->private_data;

	/*
	 * Free all IPv4 addresses taken by client
	 */
	for (c = cd->cfg->netaddrs; c; c = c->next) {

		/*
		 * Is IPv4 address specified?
		 */
		if (netaddr_get_family(cd->cfg->netaddrs->data) == AF_INET)
			ipv4_address_free(cd, cd->cfg->netaddrs->data);

		/*
		 * Is IPv4 address specified?
		 */
		if (netaddr_get_family(cd->cfg->netaddrs->data) == AF_INET6)
//			ipv6_address_free(cd, cd->cfg->netaddrs->data);
			;

	}

	/*
	 * Mark that client is not any more in bound state
	 */
	pc->isbound = FALSE;

	LOG_FUNC_END(1);

	return 0;
}

/******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/

/**
 * Unload private provider
 */
void cfg_private_instance_free(struct cfg_config_provider *ccp)
{
	private_cfg_free(ccp->data);

	return;
}

/**
 * Initialize instance of the private provider
 *
 * @param ccp	Configuration data for an instance of a provider
 *
 * \return Configuration structure, or NULL in case of an error
 *
 * Instances are differentiated by the configuration data!
 */
gint cfg_private_instance_init(struct cfg_config_provider *ccp)
{
	GSList *c, *avl;
	struct netaddr *na;
	struct private_cfg *pc;
	struct cfg_av *ca;
	gint retval;
	guint32 ipstart, ipend;

	LOG_FUNC_START(1);

	retval = -1;

	ccp->provider = provider_new();
	pc = ccp->data = private_cfg_new();

	/*
	 * Parse attribute-value list and generate configuration file
	 */
	for (avl = ccp->av; avl; avl = avl->next) {
		ca = avl->data;

		if (!strcasecmp(PRIVATE_ATTR_IPV4_POOL, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_RANGE) {
				LOG_ERROR("Configuration directive %s "
						"expects a range in line %u",
						ca->attribute, ca->line);
				goto out;
			}

			pc->ipv4_pool_start = netaddr_new_from_str(AF_INET,
						ca->lower);

			if (pc->ipv4_pool_start == NULL) {
				LOG_ERROR("Unrecognized IP address %s in line "
						"%u", ca->lower, ca->line);
				goto out;
			}

			pc->ipv4_pool_end = netaddr_new_from_str(AF_INET,
						ca->upper);

			if (pc->ipv4_pool_end == NULL) {
				LOG_ERROR("Unrecognized IP address %s in line "
						"%u", ca->upper, ca->line);
				goto out;
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV4_NETMAKS,
				ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_SINGLE) {
				LOG_ERROR("Configuration directive %s "
						"expects a single value in line"
						" %u", ca->attribute, ca->line);
				goto out;
			}

			if (inet_pton(AF_INET, ca->string,
					&pc->ipv4_netmask) < 0) {

				LOG_ERROR("Unrecognized IP address %s in line "
						"%u", ca->upper, ca->line);
				goto out;
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV4_NBNS, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_LIST) {
				LOG_ERROR("Configuration directive %s "
						"expects a list of values in "
						"line %u", ca->attribute,
						ca->line);
				goto out;
			}

			for (c = ca->list; c; c = c->next) {

				na = netaddr_new_from_str(AF_INET, c->data);

				if (!na) {
					LOG_ERROR("Unrecognized IPv4 address %s"
						" in line %u", c->data,
						ca->line);
					continue;
				}

				pc->ipv4_nbns = g_slist_append(pc->ipv4_nbns,
							na);
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV4_DHCP, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_LIST) {
				LOG_ERROR("Configuration directive %s "
						"expects a list of values in "
						"line %u", ca->attribute,
						ca->line);
				goto out;
			}

			for (c = ca->list; c; c = c->next) {

				na = netaddr_new_from_str(AF_INET, c->data);

				if (!na) {
					LOG_ERROR("Unrecognized IPv4 address %s"
						" in line %u", c->data,
						ca->line);
					continue;
				}

				pc->ipv4_dhcp = g_slist_append(pc->ipv4_dhcp,
							na);
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV4_DNS, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_LIST) {
				LOG_ERROR("Configuration directive %s "
						"expects a list of values in "
						"line %u", ca->attribute,
						ca->line);
				goto out;
			}

			for (c = ca->list; c; c = c->next) {

				na = netaddr_new_from_str(AF_INET, c->data);

				if (!na) {
					LOG_ERROR("Unrecognized IPv4 address %s"
						" in line %u", c->data,
						ca->line);
					continue;
				}

				pc->ipv4_dns = g_slist_append(pc->ipv4_dns, na);
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV6_POOL, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_RANGE) {
				LOG_ERROR("Configuration directive %s "
						"expects a range in line %u",
						ca->attribute, ca->line);
				goto out;
			}

			pc->ipv6_pool_start = netaddr_new_from_str(AF_INET6,
						ca->lower);

			if (pc->ipv6_pool_start == NULL) {
				LOG_ERROR("Unrecognized IP address %s in line "
						"%u", ca->lower, ca->line);
				goto out;
			}

			pc->ipv6_pool_end = netaddr_new_from_str(AF_INET6,
						ca->upper);

			if (pc->ipv6_pool_end == NULL) {
				LOG_ERROR("Unrecognized IP address %s in line "
						"%u", ca->upper, ca->line);
				goto out;
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV6_DHCP, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_LIST) {
				LOG_ERROR("Configuration directive %s "
						"expects a list of values in "
						"line %u", ca->attribute,
						ca->line);
				continue;
			}

			for (c = ca->list; c; c = c->next) {

				na = netaddr_new_from_str(AF_INET6, c->data);

				if (!na) {
					LOG_ERROR("Unrecognized IPv6 address %s"
						" in line %u", c->data,
						ca->line);
					continue;
				}

				pc->ipv6_dhcp = g_slist_append(pc->ipv6_dhcp,
						na);
			}

		} else if (!strcasecmp(PRIVATE_ATTR_IPV6_DNS, ca->attribute)) {

			if (ca->val_type != CFG_AV_TYPE_LIST) {
				LOG_ERROR("Configuration directive %s "
						"expects a list of values in "
						"line %u", ca->attribute,
						ca->line);
				goto out;
			}

			for (c = ca->list; c; c = c->next) {

				na = netaddr_new_from_str(AF_INET6, c->data);

				if (!na) {
					LOG_ERROR("Unrecognized IPv6 address %s"
						" in line %u", c->data,
						ca->line);
					continue;
				}

				pc->ipv6_dns = g_slist_append(pc->ipv6_dns,
						na);
			}

		} else {
			LOG_WARNING("Ignoring unrecognized attribute %s in line"
					" %u", ca->attribute, ca->line);
		}
	}

	/*
	 * If IPv4 range was specified, then allocate memory for a
	 * bitmap to hold allocated IPv4 addresses.
	 */
	if (pc->ipv4_pool_start && pc->ipv4_pool_end) {
		ipstart = ntohl(*(guint32 *)netaddr_get_ipaddr(
					pc->ipv4_pool_start));
		ipend = ntohl(*(guint32 *)netaddr_get_ipaddr(
					pc->ipv4_pool_end));

		pc->ipv4_bitmap = g_malloc0((ipend - ipstart + 8) >> 3);
	}

	ccp->provider->pid = "private";

	ccp->provider->init = private_client_init;
	ccp->provider->free = private_client_free;
	ccp->provider->reset = private_client_reset;
	ccp->provider->isbound = private_client_is_bound;
	ccp->provider->iserror = private_client_is_error;
	ccp->provider->request = private_client_request;
	ccp->provider->release = private_client_release;
//	ccp->provider->state = private_client_get_state;
	ccp->provider->dump = private_client_dump;

	/*
	 * By default, we support all of these options, but
	 * the exact ones are determined during configuration
	 * directives parsing phase.
	 */
	ccp->provider->flags = OPTION_F4_ADDR
				| OPTION_F4_NETMASK
				| OPTION_F4_DNS
				| OPTION_F4_DHCP
				| OPTION_F4_NBNS
				| OPTION_F6_ADDR
				| OPTION_F6_DNS
				| OPTION_F6_DHCP;

	retval = 0;

out:
	LOG_FUNC_END(1);

	return 0;
}

/**
 * Global deinitalization of private provider
 *
 * For private provider this function is a dummy!
 */
void cfg_private_unload()
{
	return;
}

/**
 * Global initialization of private provider
 *
 * \return 0 in case of success, -1 in case of an error
 *
 * For private provider this function is a dummy!
 */
gint cfg_private_init(struct cfg_config_provider *ccp)
{
	return 0;
}

#endif /* CFG_PRIVATE */
