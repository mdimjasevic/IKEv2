/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __CFG_C

#if defined(CFG_MODULE) || defined(CFG_CLIENT)

#define LOGGERNAME	"cfg"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "auth.h"

#include "cfg.h"

/*******************************************************************************
 * struct cfg related functions
 ******************************************************************************/

/**
 * Allocate a new cfg structure
 */
struct cfg *cfg_new()
{
	struct cfg *cfg;

	cfg = g_malloc0(sizeof(struct cfg));
	cfg->refcnt = 1;

	return cfg;
}

/**
 * Increase reference counter of a given cfg structure
 */
struct cfg *cfg_refcnt_inc(struct cfg *cfg)
{
	g_atomic_int_inc(&(cfg->refcnt));
	return cfg;
}

void cfg_free(struct cfg *cfg)
{
	if (cfg && g_atomic_int_dec_and_test(&(cfg->refcnt))) {

		while (cfg->netaddrs) {
			netaddr_free(cfg->netaddrs->data);
			cfg->netaddrs = g_slist_remove(cfg->netaddrs,
						cfg->netaddrs->data);
		}

		while (cfg->dns) {
			netaddr_free(cfg->dns->data);
			cfg->dns = g_slist_remove(cfg->dns, cfg->dns->data);
		}

		while (cfg->dhcp) {
			netaddr_free(cfg->dhcp->data);
			cfg->dhcp = g_slist_remove(cfg->dhcp, cfg->dhcp->data);
		}

		while (cfg->nbns) {
			netaddr_free(cfg->nbns->data);
			cfg->dhcp = g_slist_remove(cfg->dhcp, cfg->nbns->data);
		}

		while (cfg->subnets) {
			netaddr_free(cfg->subnets->data);
			cfg->subnets = g_slist_remove(cfg->dhcp,
						cfg->subnets->data);
		}

		if (cfg->appid)
			g_free(cfg->appid);
	}
}

void cfg_dump(struct cfg *cfg)
{
	char ipaddr[45], line[250];
	GSList *c;

	LOG_DEBUG("flags set to %08X", cfg->flags);

	line[0] = 0;

	if (cfg->flags & OPTION_F4_ADDR)
		strcat(line, "ADDRESS ");

	if (cfg->flags & OPTION_F4_DNS)
		strcat(line, "DNS ");

	if (cfg->flags & OPTION_F4_DHCP)
		strcat(line, "DHCP ");

	if (cfg->flags & OPTION_F4_NBNS)
		strcat(line, "NBNS ");

	if (cfg->flags & OPTION_F4_SUBNETS)
		strcat(line, "SUBNETS ");

	if (!strlen(line))
		strcat(line, "<none>");

	LOG_DEBUG("IPv4 parameters: %s", line);

	line[0] = 0;

	if (cfg->flags & OPTION_F6_ADDR)
		strcat(line, "ADDRESS ");

	if (cfg->flags & OPTION_F6_DNS)
		strcat(line, "DNS ");

	if (cfg->flags & OPTION_F6_DHCP)
		strcat(line, "DHCP ");

	if (cfg->flags & OPTION_F6_SUBNETS)
		strcat(line, "SUBNETS ");

	if (!strlen(line))
		strcat(line, "<none>");

	LOG_DEBUG("IPv6 parameters: %s", line);

	line[0] = 0;

	if (cfg->supported & OPTION_F4_ADDR)
		strcat(line, "ADDRESS ");

	if (cfg->supported & OPTION_F4_DNS)
		strcat(line, "DNS ");

	if (cfg->supported & OPTION_F4_DHCP)
		strcat(line, "DHCP ");

	if (cfg->supported & OPTION_F4_NBNS)
		strcat(line, "NBNS ");

	if (cfg->supported & OPTION_F4_SUBNETS)
		strcat(line, "SUBNETS ");

	if (!strlen(line))
		strcat(line, "n/a");

	LOG_DEBUG("Supported IPv4 parameters: %s", line);

	line[0] = 0;

	if (cfg->supported & OPTION_F6_ADDR)
		strcat(line, "ADDRESS ");

	if (cfg->supported & OPTION_F6_DNS)
		strcat(line, "DNS ");

	if (cfg->supported & OPTION_F6_DHCP)
		strcat(line, "DHCP ");

	if (cfg->supported & OPTION_F6_SUBNETS)
		strcat(line, "SUBNETS ");

	if (!strlen(line))
		strcat(line, "n/a");

	LOG_DEBUG("Supported IPv6 parameters: %s", line);

	for(c = cfg->netaddrs; c; c = c->next)
		LOG_DEBUG("Leased address %s",
			netaddr_ip2str(c->data, ipaddr, 45));

	LOG_DEBUG("Lease time for IPv4 addresses: %u", cfg->expiry4);

	LOG_DEBUG("Lease time for IPv6 addresses: %u", cfg->expiry6);

	for(c = cfg->dns; c; c = c->next)
		LOG_DEBUG("DNS server address %s",
			netaddr_ip2str(c->data, ipaddr, 45));

	for(c = cfg->nbns; c; c = c->next)
		LOG_DEBUG("WINS server address %s",
			netaddr_ip2str(c->data, ipaddr, 45));

	for(c = cfg->subnets; c; c = c->next)
		LOG_DEBUG("Protected subnet %s",
			netaddr_net2str(c->data, ipaddr, 45));
}

GSList *cfg_get_netaddrs(void *cfg_struct)
{
	return ((struct cfg *)cfg_struct)->netaddrs;
}

gboolean cfg_is_request(void *cfg_struct)
{
	return ((struct cfg *)cfg_struct)->request;
}

#endif /* defined(CFG_MODULE) || defined(CFG_CLIENT) */
