/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __AUTH_H
#define __AUTH_H

#define ID_ANY		"any"

/**
 * ID related flags
 */
enum {
	ID_F_ANY = 1 << 0,

	/*
	 * If the ID contains globchar value
	 */
	ID_F_GLOBCHAR = 1 << 1
};

/**
 * Identifier structure.
 */
struct id {

	/**
	 * Identifier type
	 */
	guint8 type;

	/**
	 * Flags
	 */
	guint8 flags;

	union {
		/**
		 * The following field contains IP ranges. It is valid
		 * only when IP addresses are used as identifiers.
		 */
		struct ts *ts;

		/**
		 * The following structure is used for FQDN, RFC822
		 * types of IDs.
		 */
		struct {
			/**
			 * Identifier length
			 */
			guint16 len;

			/**
			 * Pointer to ID string. It is not NULL terminated!
			 */
			char *id;
		};

		/**
		 * The following structure is used for DN type of
		 * IDs.
		 *
		 * This is binary data, but it IS NULL terminated!
		 */
		gpointer derasn1dn;
	};
};

/**
 * PSK Item
 */
struct psk_item {
	/*
	 * ID
	 */
	struct id *id;

	/*
	 * Secret related to ID
	 */
	char *secret;
};

/**
 * Limits structure
 */
struct limits {
	/**
	 * Time limit, in seconds
	 */
	guint32 time;

	/**
	 * Allocation limit
	 */
	guint32 allocs;

	/**
	 * Octets limit, in bytes...
	 */
	guint64 octets;
};

/*******************************************************************************
 * ID structure functions
 ******************************************************************************/
struct id *id_new();
struct id *id_new_from_str(gchar *);
struct id *id_new_from_str_glob(gchar *);
struct id *id_bin2id(guint8, gint, gpointer);
void id_free(struct id *);
gint id_get_size(struct id *);
gpointer id_get_ptr(struct id *);
void id_list_free(GSList *);
gchar *id_id2str(struct id *);
void id_dump(char *, struct id *);
void id_list_dump(char *, GSList *);
void id_set_str_id(struct id *, char *);
gboolean id_is_subset(struct id *, struct id *);
gboolean id_is_equal(struct id *, struct id *);

/*******************************************************************************
 * PSK structure functions
 ******************************************************************************/
GSList *psk_read_from_file(const char *);
struct psk_item *psk_item_new(void);
void psk_item_free(struct psk_item *);
void psk_item_list_free(GSList *);
void psk_item_set_id(struct psk_item *, struct id *);
struct id *psk_item_get_id(struct psk_item *);
char *psk_secret_find_by_id(GSList *, struct id *);
struct psk_item *psk_find_by_id(GSList *, struct id *);

/*******************************************************************************
 * LIMITS structure functions
 ******************************************************************************/
struct limits *limits_new();
void limits_free(struct limits *);
void limits_dump(struct limits *);

#endif /* __AUTH_H */
