#!/bin/sh

ip addr flush dev eth1
ip addr add 10.0.0.1/24 dev eth1
ip link set dev eth1 up

ip addr flush dev eth2
ip addr add 172.16.0.1/24 dev eth2
ip link set dev eth2 up
