/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing network module
 *
 * Test queue version of network registration function
 */

#define __NETWORK_TEST_C

#define LOGGERNAME	"network_test1"

#include <stdio.h>

#include <sys/types.h>
#include <netinet/ip.h>

#include <linux/if_ether.h>

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "config.h"

gpointer wait_thread(gpointer _queue)
{
	GTimeVal t, t1, t2;
	GAsyncQueue *queue = _queue;
	struct network_msg *n_msg;
	char buf[64];

	while (1) {

		/*
		 * Wait for a message...
		 */
		LOG_NOTICE("Waiting on a queue");
		g_get_current_time(&t1);
		n_msg = g_async_queue_pop(queue);
		g_get_current_time(&t2);

		t.tv_usec = t2.tv_usec - t1.tv_usec;
		t.tv_sec = t2.tv_sec - t1.tv_sec;
		if (t.tv_usec < 0) {
			t.tv_usec += 1000000;
			t.tv_sec--;
		}

		LOG_NOTICE("Waited for %u secs %u usecs",
				(guint)t.tv_sec, (guint)t.tv_usec);

		LOG_NOTICE("Octets received %u", n_msg->size);

		LOG_NOTICE("Source address %s",
				netaddr_ip2str(n_msg->srcaddr, buf, 64));

		network_msg_free(n_msg);
	}

	return NULL;
}

int main(int argc, char **argv)
{
	GMainLoop *ml;
	GAsyncQueue *queue;
	GThread *wt;
	network_t *ns;
	netaddr_t *na;

	struct config *config;

	LOG_NOTICE("This test program creates UDP socket, binds it to a port");
	LOG_NOTICE("12345 and waits for some traffic. It prints data about");
	LOG_NOTICE("what it has received.");

	if (argc != 2) {
		LOG_ERROR("Usage: %s <conf file>", argv[0]);
		goto out;
	}

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to network module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Queue on which we'll receive message
	 */
	if ((queue = g_async_queue_new()) == NULL) {
		LOG_ERROR("Error creatig queue");
		goto out;
	}

	/*
	 * Start thread that will wait for a message
	 */
	wt = g_thread_create(wait_thread, queue, FALSE, NULL);

	/*
	 * Initialize logging subsystem
	 */
	if (config_init(argv[1], &config) < 0)
		goto out;
 
	if (logging_init(config->logging) < 0)
		goto out;

	LOG_NOTICE("Initializing network module");
	if (network_init(NULL) < 0)
		goto out;

	/*
	 * This test will run for 3 seconds and then, we'll terminate
	 * main loop...
	 */
	if ((na = netaddr_new()) == NULL)
		goto out;

	netaddr_set_family(na, AF_INET);
	netaddr_set_port(na, 12345);

	LOG_NOTICE("Registering listening socket");
	ns = network_socket_register_queue(AF_INET, SOCK_DGRAM, 0,
			na, 0, queue);
	if (ns == NULL)
		goto out;

	/*
	 * Start main loop...
	 */
	LOG_NOTICE("Starting main loop");
	g_main_loop_run(ml);

	/*
	 * Unload network module
	 */
	LOG_NOTICE("Unloading network module");
	network_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	LOG_NOTICE("Exiting");

out:
	logging_unload();

	return 0;
}
