/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __CONFIG_C

#define LOGGERNAME	"config"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "proposals.h"
#include "ts.h"
#include "auth.h"
#include "cert.h"
#include "radius.h"
#include "cfg.h"
#include "cfg_module.h"
#include "proposals.h"
#include "spd.h"
#include "config.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#define MAXLEN		512 

FILE *config_file_in;

/*******************************************************************************
 * CONFIG_LISTEN structure function
 ******************************************************************************/

/**
 * Allocate new config_listen structure
 *
 * \return Pointer to a new structure
 */
struct config_listen *config_listen_new(void)
{
	return g_malloc0(sizeof(struct config_listen));
}

/**
 * Free config_listen structure
 */
void config_listen_free(struct config_listen *cl)
{
	if (cl) {

		if (cl->addr)
			netaddr_free(cl->addr);

		g_free(cl);
	}
}

/**
 * Free a list of config_listen structures
 */
void config_listen_list_free(GSList *l)
{
	while (l) {
		config_listen_free(l->data);
		l = g_slist_remove(l, l->data);
	}
}

/*******************************************************************************
 * CONFIG_GENERAL structure functions
 ******************************************************************************/

/**
 * Allocate a new config_general structure
 *
 * \return Pointer to a new structure
 */
struct config_general *config_general_new(void)
{
	struct config_general *cg;

	cg = g_malloc0(sizeof(struct config_general));
	cg->refcnt = 1;
	
	cg->ca_list = NULL;
	cg->cert_list = NULL;

	return cg;
}

/**
 * Free config_general structure
 *
 * @param cg	Structure to be free'd
 */
void config_general_free(struct config_general *cg)
{
	if (cg && g_atomic_int_dec_and_test(&(cg->refcnt))) {

		if (cg->random_device)
			g_free(cg->random_device);

		if (cg->ca_list)
			ca_item_list_free(cg->ca_list);

		if (cg->cert_list)
			cert_item_list_free(cg->cert_list);
		
		g_free(cg);
	}
}

/**
 * Set random device name.
 *
 * @param cg	Configuration structure
 * @param rd	Device name
 *
 * Note, after returning, this function takes care of character string
 * pointed to by parameter rd.
 */
void config_general_set_random_device(struct config_general *cg, char *rd)
{
	cg->random_device = rd;
}

/**
 * Get file name of random device
 *
 * @param cg	Configuration structure
 *
 * \return Pointer to read-only copy of random device file name
 */
const char *config_general_get_random_device(struct config_general *cg)
{
	return cg->random_device;
}

/**
 * Set number of threads in state machine thread pool
 *
 * @param cg		Configuration structure
 * @param threads	Number of threads to use
 */
void config_general_set_sm_threads(struct config_general *cg, int threads)
{
	cg->sm_threads = threads;
}

/**
 * Return number of configured threads in state machine thread pool
 *
 * @param cg		Configuration structure
 *
 * \return Number of configured threads
 */
int config_general_get_sm_threads(struct config_general *cg)
{
	return cg->sm_threads;
}

/**
 * Dump contents of config_general structure into log file
 */
void config_general_dump(struct config_general *cg)
{
	gint i;

	LOG_DEBUG("Dumping general section");

	if (cg->random_device)
		LOG_DEBUG("random_device=%s", cg->random_device);
	else
		LOG_DEBUG("random_device=NULL");

	LOG_DEBUG("dos_treshold=%u", cg->dos_treshold);
	LOG_DEBUG("sm_threads=%d", cg->sm_threads);
	LOG_DEBUG("ikesa_max=%d", cg->ikesa_max);
	LOG_DEBUG("ikesa_max_halfopened=%d", cg->ikesa_max_halfopened);

	i = 0;
	while (config_options[i].name) {

		if (config_options[i].section == OPTION_SECTION_GENERAL &&
				cg->options & config_options[i].value)
			LOG_DEBUG("Turned on option %s",
					config_options[i].name);

		i++;
	}

}

/**
 * Add new CA
 */
void config_general_add_ca(struct config_general *cg, struct ca_item *ca_item)
{
	LOG_DEBUG("Added new CA item");
	cg->ca_list = g_slist_append(cg->ca_list, ca_item);
}

/**
 * Add new cert 
 */
void config_general_add_cert(struct config_general *cg, 
		struct cert_item *cert_item)
{
	LOG_DEBUG("Added new CERT item, url = %s", cert_item->url);
	cg->cert_list = g_slist_append(cg->cert_list, cert_item);
}

/*******************************************************************************
 * CONFIG REMOTE structure functions
 ******************************************************************************/

struct config_remote *config_remote_new()
{
	struct config_remote *cr;

	LOG_FUNC_START(1);

	cr = g_malloc0(sizeof(struct config_remote));

#ifdef MOBIKE
	/*
	 * Initialize mobike variable to -1 in order to avoid ambiguity
	 * with MOBIKE_OFF (which equals to zero)
	 */
	 cr->mobike = -1;
#endif /* MOBIKE */

	/*
	 * This newly created structure has initially refcnt set to 1
	 * since it's put into config structure.
	 */
	g_atomic_int_inc(&(cr->refcnt));

	LOG_FUNC_END(1);

	return cr;
}

void config_remote_free(struct config_remote *cr)
{
	LOG_FUNC_START(1);

	if (cr && g_atomic_int_dec_and_test(&(cr->refcnt))) {

		if (cr->proposals)
			proposal_list_free(cr->proposals);

		if (cr->ts_list)
			ts_list_free(cr->ts_list);

#warning "These have to be moved into config_peer"
#if 0
		if (cr->id)
			id_free(config_ikesa->id);
#endif

		g_free(cr);

	}

	LOG_FUNC_END(1);
}

/**
 * Add new proposal to a remote structure
 *
 * @param cr
 * @param proposal
 */
void config_remote_add_proposal(struct config_remote *cr,
		struct proposal *proposal)
{
	cr->proposals = g_slist_append(cr->proposals, proposal);
}

/**
 * Search a list of config_remote structures by IP address
 *
 * @param config
 * @param addr
 *
 * \return Pointer to config_remote structure or NULL if none was found
 */
struct config_remote *config_remote_find_by_addr(struct config *config,
		netaddr_t *addr)
{
	struct config_remote *cr = NULL;
	struct ts *ts;
	GSList *iterator, *tsc;
	gint v1, v2;
	char buffer[40];

	LOG_FUNC_START(1);

	LOG_DEBUG("Searching for address %s",
				netaddr_ip2str(addr, buffer, 40));

	iterator = config->remote;
	while (iterator) {
		cr = iterator->data;
		iterator = iterator->next;

		tsc = cr->ts_list;
		while (tsc) {
			ts = tsc->data;
			tsc = tsc->next;

			if (!netaddr_same_family(addr, ts->saddr))
				continue;

			v1 = netaddr_cmp_ip2ip(addr, ts->saddr);
			v2 = netaddr_cmp_ip2ip(addr, ts->eaddr);

			if (v1 >= 0 && v2 <= 0)
				goto out;
		}
	}

	cr = NULL;

out:
	LOG_FUNC_END(1);

	return cr;
}

/**
 * Search a list of config_remote structures by ID
 *
 * @param config
 * @param addr
 *
 * \return Pointer to config_remote structure or NULL if none was found
 */
struct config_remote *config_remote_find_by_id(struct config *config,
		struct id *id)
{
	struct config_remote *cr = NULL;
	GSList *iterator;

	LOG_FUNC_START(1);

	id_dump(LOGGERNAME, id);

	iterator = config->remote;
	while (iterator) {
		cr = iterator->data;
		iterator = iterator->next;

		id_dump(LOGGERNAME, cr->rid);
		if (id_is_subset(id, cr->rid))
			goto out;
	}

	cr = NULL;

out:
	LOG_FUNC_END(1);

	return cr;
}

void config_remote_dump(struct config_remote *cr)
{
	gint i;

	LOG_DEBUG("Dumping config_remote structure at line %u",
			cr->cfg_line);

	LOG_DEBUG("nonce_size=%d", cr->nonce_size);

	LOG_DEBUG("response_timeout=%d", cr->response_timeout);
	LOG_DEBUG("response_retries=%d", cr->response_retries);

	i = 0;
	while (config_options[i].name) {

		if (config_options[i].section == OPTION_SECTION_REMOTE &&
				cr->options & config_options[i].value)
			LOG_DEBUG("Turned on option %s",
					config_options[i].name);

		i++;
	}

	ts_list_dump(LOGGERNAME, cr->ts_list);

	proposal_dump_proposal_list(LOGGERNAME, cr->proposals);
}

/*******************************************************************************
 * CONFIG_PEER structure functions
 ******************************************************************************/

/**
 * Allocate new config_peer structure
 *
 * \return
 */
struct config_peer *config_peer_new()
{
	struct config_peer *cp;

	cp = g_malloc0(sizeof(struct config_peer));

#ifdef MOBIKE
	/*
	 * Initialize mobike variable to -1 in order to avoid ambiguity
	 * with MOBIKE_OFF (which equals to zero)
	 */
	 cp->mobike = -1;
#endif /* MOBIKE */

	cp->refcnt = 1;
	
	cp->ca_list = NULL;
	cp->cert_list = NULL;

	return cp;
}

/**
 * Add new CA
 */
void config_peer_add_ca(struct config_peer *cp, struct ca_item *ca_item)
{
	LOG_DEBUG("Added new peer CA item");
	cp->ca_list = g_slist_append(cp->ca_list, ca_item);
}

/**
 * Add new cert 
 */
void config_peer_add_cert(struct config_peer *cp, 
		struct cert_item *cert_item)
{
	LOG_DEBUG("Added new peer CERT item");
	cp->cert_list = g_slist_append(cp->cert_list, cert_item);
}


/**
 * Free config_peer structure
 *
 * @param cp
 */
void config_peer_free(struct config_peer *cp)
{

	if (cp && g_atomic_int_dec_and_test(&(cp->refcnt))) {

		if (cp->rids)
			id_list_free(cp->rids);

		if (cp->psk_file)
			g_free(cp->psk_file);

#ifdef RADIUS_CLIENT
		if (cp->radius_server)
			g_free(cp->radius_server);
#endif
		if (cp->ca_list)
			ca_item_list_free(cp->ca_list);

		if (cp->cert_list)
			cert_item_list_free(cp->cert_list);

#ifdef CFG_MODULE
		while (cp->subnets) {
			netaddr_free(cp->subnets->data);
			cp->subnets = g_slist_remove(cp->subnets,
						cp->subnets->data);
		}

		while (cp->providers) {
			g_free(cp->providers->data);
			cp->providers = g_slist_remove(cp->providers,
						cp->providers->data);
		}
#endif /* CFG_MODULE */

#ifdef CFG_CLIENT
		if (cp->up_argv)
			g_strfreev(cp->up_argv);

		if (cp->down_argv)
			g_strfreev(cp->down_argv);
#endif /* CFG_CLIENT */

		if (cp->auth_limits)
			limits_free(cp->auth_limits);

		if (cp->rekey_limits)
			limits_free(cp->rekey_limits);

		if (cp->sainfo)
			;

		g_free(cp);

	}
}

/**
 * Add new SA info section
 *
 * @param cp
 * @param csa
 */
void config_peer_add_sainfo(struct config_peer *cp, struct config_csa *csa)
{
	cp->sainfo = g_slist_append(cp->sainfo, csa);
}

/**
 * Find config_peer structure given peer's ID
 *
 * @param cpl	List of config_peer structures
 * @param id	ID to search for. If NULL first config_peer structure
 *		will be returned.
 *
 * \return Pointer to config_peer structure, or NULL in case an error
 * occured, or no structure has been found.
 */
struct config_peer *config_peer_find_by_id(GSList *cpl, struct id *id)
{
	struct config_peer *cp = NULL;
	GSList *iterator;
	struct id *rid;

	LOG_FUNC_START(1);

	if (!cpl)
		goto out;

	if (!id) {
		cp = cpl->data;
		goto out;
	}

	while (cpl) {
		cp = cpl->data;
		cpl = cpl->next;

		for(iterator = cp->rids; iterator; iterator = iterator->next) {
			rid = iterator->data;

			id_dump(LOGGERNAME, rid);

			if (id_is_subset(id, rid))
				goto out;
		}
	}

	LOG_ERROR("ID not found");
	cp = NULL;

out:
	LOG_FUNC_END(1);

	return cp;
}

/**
 * Find config_peer structure given sainfo
 *
 * @param cpl	List of config_peer structures
 * @param ccsa	SAINFO section to search for
 *
 * \return	Pointer to config_peer structure, or NULL in case an error
 *		occured, or no structure has been found.
 */
struct config_peer *config_peer_find_by_ccsa(GSList *cpl,
		struct config_csa *ccsa)
{
	struct config_peer *cp = NULL;
	GSList *iterator;

	LOG_FUNC_START(1);

	if (!cpl || !ccsa)
		goto out;

	while (cpl) {
		cp = cpl->data;
		cpl = cpl->next;

		for (iterator = cp->sainfo; iterator;
				iterator = iterator->next)
			if (iterator->data == ccsa)
				goto out;
	}

	LOG_ERROR("Appropriate sainfo section was not found");
	cp = NULL;

out:
	LOG_FUNC_END(1);

	return cp;
}

/**
 * Dump config_peer structure
 *
 * @param cp
 */
void config_peer_dump(struct config_peer *cp)
{
	gint i;
	GSList *iterator;
#if CFG_MODULE
	gchar ipaddr[46];
#endif /* CFG_MODULE */

	LOG_DEBUG("Dumping PEER at line %d", cp->cfg_line);

	LOG_DEBUG("sa_halfclosed_wait=%u", cp->sa_halfclosed_wait);
	LOG_DEBUG("Remote ID");
	if (cp->rids)
		id_list_dump(LOGGERNAME, cp->rids);
	else
		LOG_DEBUG("ANY");

	LOG_DEBUG("Local ID");
	id_dump(LOGGERNAME, cp->lid);

	LOG_DEBUG("ike_max_idle=%u", cp->ike_max_idle);

	LOG_DEBUG("PSK file %s", cp->psk_file);

#ifdef SUPPLICANT
	LOG_DEBUG("WPA conf file %s", cp->wpa_conf);
#endif /* SUPPLICANT */

#ifdef RADIUS_CLIENT
	LOG_DEBUG("Radius server ID %s", cp->radius_server);
#endif

	i = 0;
	while (config_options[i].name) {

		if (config_options[i].section == OPTION_SECTION_PEER &&
				cp->options & config_options[i].value)
			LOG_DEBUG("Turned on option %s",
					config_options[i].name);

		i++;
	}

#if CFG_MODULE
	if (cp->subnets) {
		for (iterator = cp->subnets; iterator;
				iterator = iterator->next) {
			netaddr_net2str(iterator->data, ipaddr, 46);
			LOG_DEBUG("subnet %s", ipaddr);
		}
	} else
		LOG_DEBUG("No subnets defined!");

	if (cp->providers)
		for (iterator = cp->providers; iterator;
				iterator = iterator->next)
			LOG_DEBUG("Provider ID %s", iterator->data);
	else
		LOG_DEBUG("No providers defined!");
#endif /* CFG_MODULE */

#ifdef CFG_CLIENT
	LOG_DEBUG("Cfg flags 0x%04X", cp->flags);

	if (cp->up_argc)
		LOG_DEBUG("Script for activating IP address: %s",
				cp->up_argv[0]);

	if (cp->down_argc)
		LOG_DEBUG("Script for activating IP address: %s",
				cp->down_argv[0]);
#endif /* CFG_CLIENT */

	LOG_DEBUG("Authentication limits");
	limits_dump(cp->auth_limits);
	LOG_DEBUG("Rekeying limits");
	limits_dump(cp->rekey_limits);

	for (iterator = cp->sainfo; iterator; iterator = iterator->next)
		config_csa_dump(iterator->data);
}

/*******************************************************************************
 * CONFIG structure functions
 ******************************************************************************/

struct config *config_new(void)
{
	struct config *config;

	LOG_FUNC_START(1);

	config = g_malloc0(sizeof(struct config));

	config->cert_path = NULL;

	LOG_FUNC_END(1);

	return config;
}

void config_free(struct config *config)
{
	if (config->config_file)
		g_free(config->config_file);

	if (config->cg)
		config_general_free(config->cg);

	while (config->remote) {
		config_remote_free(config->remote->data);
		config->remote = g_slist_remove(config->remote,
						config->remote->data);
	}

	while (config->csa) {
		config_csa_free(config->csa->data);
		config->csa = g_slist_remove(config->csa,
						config->csa->data);
	}

	g_free(config);
}

#ifdef RADIUS_CLIENT
/**
 * Add new radius section
 *
 * @param config
 * @param cr
 */
void config_add_radius(struct config *config, struct  radius_config *cr)
{
	config->radius_servers = g_slist_append(config->radius_servers, cr);
}
#endif

/**
 * Add new provider
 *
 * @param config
 * @param ccp
 */
void config_add_provider(struct config *config, struct cfg_config_provider *ccp)
{
	config->providers = g_slist_append(config->providers, ccp);
}

/**
 * Add new remote section
 *
 * @param config
 * @param cr
 */
void config_add_remote(struct config *config, struct  config_remote *cr)
{
	config->remote = g_slist_append(config->remote, cr);
}

/**
 * Add new peer section
 *
 * @param config
 * @param cp
 */
void config_add_peer(struct config *config, struct  config_peer *cp)
{
	config->peer = g_slist_append(config->peer, cp);
}

int conf_fgets(char *buffer, int size)
{
	if (fgets(buffer, size, config_file_in) == NULL)
		return 0;

	return (strlen(buffer));
}

struct config *config_read_cfg_file(const char *config_file)
{
	int retval;
	struct config *config = NULL;

	LOG_FUNC_START(1);

	config_file_in = fopen(config_file, "r");
	if (config_file_in == NULL) {
		LOG_ERROR("Could not open configuration file %s", config_file);
		goto out;
	}

	if ((config = config_new()) == NULL) {
		fclose(config_file_in);
		goto out;
	}

	if ((config->cg = config_general_new()) == NULL) {
		fclose(config_file_in);
		config_free(config);
		config = NULL;
		goto out;
	}

	/*
	 * Preset some default values...
	 */
	config->cg->ikesa_max = 1;
	config->cg->ikesa_max_halfopened = 2;

	retval = yyparse(config);

	fclose(config_file_in);

	if (retval) {
		config_free(config);
		config = NULL;
		goto out;
	}

	if (config->cg->psk_file) {
		if ((config->cg->psk = psk_read_from_file(config->cg->psk_file))
				== NULL) {
			config_free(config);
			config = NULL;
			goto out;
		}
	}

	config->config_file = strdup(config_file);

	config_dump(config);

out:
	LOG_FUNC_END(1);

	return config;
}

struct config *config_reread_cfg_file(struct config *config)
{
	return config_read_cfg_file(config->config_file);
}

int config_init(const char *config_file, struct config **config)
{
	LOG_FUNC_START(1);

	LOG_NOTICE("Reading configuration file %s", config_file);

	if (!(*config = config_read_cfg_file (config_file)))
		return -1;

	return 0;

}

/*******************************************************************************
 * DEBUG functions
 ******************************************************************************/

/**
 * Dump configuration structure
 *
 * @param config	Configuration structure to be dumped
 *
 * @todo Rewrite this function to output proper configuration file to feed
 *	 back to daemon.
 */
void config_dump(struct config *config)
{
	GSList *iterator;

	LOG_DEBUG("Dumping data from configuration file");

	if (config == NULL) {
		LOG_DEBUG("No data read from configuration file!");
		return;
	}

	LOG_DEBUG("config_file=%s", config->config_file);

	config_general_dump(config->cg);

#ifdef RADIUS_CLIENT
	for (iterator = config->radius_servers; iterator;
			iterator = iterator->next)
		radius_config_dump(iterator->data);
#endif

	if (config->cg->psk_file)
		LOG_DEBUG("psk_file=%s", config->cg->psk_file);
	else
		LOG_DEBUG("psk_file=NULL");

	iterator = config->remote;
	while(iterator) {
		config_remote_dump(iterator->data);
		iterator = iterator->next;
	}

	iterator = config->peer;
	while(iterator) {
		config_peer_dump(iterator->data);
		iterator = iterator->next;
	}
}

