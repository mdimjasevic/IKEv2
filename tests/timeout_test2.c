/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing timeout module
 *
 * The difference between this and timeout_test1 is that in this case we
 * have two timeouts registered in the following order:
 *
 * - we register timeout of 10
 * - then immediately we register timeout of 3 seconds
 *
 * the second timeout should terminate program.
 */

#define __TIMEOUT_TEST_C

#define LOGGERNAME	"timeout"

#include <stdio.h>
#include <glib.h>

#include "timeout.h"

gpointer wait_thread(gpointer _queue)
{
	GAsyncQueue *queue = _queue;
	GMainLoop *ml;

	/*
	 * Wait for a message...
	 */
	printf ("Waiting on a queue\n");
	ml = g_async_queue_pop(queue);

	/*
	 * Just terminate main loop...
	 */
	printf ("Received message. Terminating main loop.\n");
	g_main_loop_quit(ml);

	return NULL;
}

int main(int argc, char **argv)
{
	GTimeVal tv1, tv2, tv;
	GMainLoop *ml;
	GTimeVal mainrun;
	GAsyncQueue *queue;
	GThread *wt;
	timeout_t *t1, *t2;

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to timeout module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Queue on which we'll receive message
	 */
	queue = g_async_queue_new();

	/*
	 * Start thread that will wait for a message
	 */
	printf ("Creating waiting thread\n");
	wt = g_thread_create(wait_thread, queue, FALSE, NULL);

	printf ("Initializing timeout module\n");
	timeout_init();

	/*
	 * This test will run for 10 seconds and then, we'll terminate
	 * main loop...
	 */
	printf ("Registering timeout in 10 seconds\n");
	mainrun.tv_sec = 10;
	mainrun.tv_usec = 0;
	t1 = timeout_register_queue(&mainrun, 0, ml, queue);

	/*
	 * This test will run for 3 seconds and then, we'll terminate
	 * main loop...
	 */
	printf ("Registering timeout in 3 seconds\n");
	mainrun.tv_sec = 3;
	mainrun.tv_usec = 0;
	t2 = timeout_register_queue(&mainrun, 0, ml, queue);

	/*
	 * Start main loop...
	 */
	printf ("Entering main loop\n");
	g_get_current_time(&tv1);
	g_main_loop_run(ml);
	g_get_current_time(&tv2);

	tv.tv_usec = tv2.tv_usec - tv1.tv_usec;
	tv.tv_sec = tv2.tv_sec - tv1.tv_sec;
	if (tv.tv_usec < 0) {
		tv.tv_usec += 1000000;
		tv.tv_sec--;
	}

	printf ("Main loop ran for %u secs %u usecs\n",
			(guint)tv.tv_sec, (guint)tv.tv_usec);

	/*
	 * Unload timeout module
	 */
	printf ("Unloading timeout module\n");
	timeout_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	printf ("Exiting\n");
	return 0;
}
