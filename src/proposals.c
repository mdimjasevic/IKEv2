/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __PROPOSALS_C

#define LOGGERNAME	"proposals"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "transforms.h"
#include "proposals.h"

const char *ikev2_protocols[] = {
	NULL,
	"IKEV2_PROTOCOL_IKE",
	"IKEV2_PROTOCOL_AH",
	"IKEV2_PROTOCOL_ESP",
};

/*******************************************************************************
 * LIMITS structure functions
 ******************************************************************************/

/**
 * Allocate and return new proposal
 */
proposal_t *proposal_new()
{
	proposal_t *proposal;

	LOG_FUNC_START(2);

	proposal = g_malloc0(sizeof(proposal_t));

	LOG_FUNC_END(2);

	return proposal;
}

/**
 * Free proposal structure
 */
void proposal_free(proposal_t *proposal)
{
	LOG_FUNC_START(2);

	if (proposal) {
		while (proposal->transforms) {
			transform_free(proposal->transforms->data);
			proposal->transforms = g_slist_remove(
						proposal->transforms,
						proposal->transforms->data);
		}

		g_free(proposal);
	}

	LOG_FUNC_END(2);
}

/**
 * Add new proposal to a list of proposals
 */
void proposal_list_add_proposal(GSList **proposals, proposal_t *proposal)
{
	*proposals = g_slist_append(*proposals, proposal);
}

/**
 * Free a list of proposal structures
 */
void proposal_list_free(GSList *proposals)
{
	LOG_FUNC_START(2);

	while (proposals) {
		proposal_free(proposals->data);
		proposals = g_slist_remove(proposals, proposals->data);
	}

	LOG_FUNC_END(2);
}

void proposal_clear(proposal_t *proposal)
{
	transform_t *t;
	proposal->protocol_id = 0;
	proposal->spi_size = 0;
	proposal->spi = 0;

	while (proposal->transforms) {
		t = proposal->transforms->data;
		proposal->transforms = g_slist_remove(proposal->transforms,
						proposal->transforms->data);
		transform_free(t);
	}

	proposal->transforms = NULL;
}

/*******************************************************************************
 * GETTER/SETTER FUNCTIONS
 ******************************************************************************/

guint8 proposal_get_protocol(proposal_t *proposal)
{
	return proposal->protocol_id;
}

void proposal_set_protocol(proposal_t *proposal, guint8 protocol_id)
{
	g_assert((protocol_id == IKEV2_PROTOCOL_IKE)
			|| (protocol_id == IKEV2_PROTOCOL_ESP)
			|| (protocol_id == IKEV2_PROTOCOL_AH));

	proposal->protocol_id = protocol_id;
}

guint8 proposal_get_spi_size(proposal_t *proposal)
{
	return proposal->spi_size;
}

void proposal_set_spi_size(proposal_t *proposal, guint8 spi_size)
{
	proposal->spi_size = spi_size;
}

guint64 proposal_get_size(proposal_t *proposal)
{
	return proposal->spi;
}

void proposal_set_spi(proposal_t *proposal, guint64 spi)
{
	proposal->spi = spi;
}

guint64 proposal_get_spi(proposal_t *proposal)
{
	return proposal->spi;
}

/**
 * Other methods
 */

void proposal_transform_append(proposal_t *proposal,
	transform_t *transform)
{
	proposal->transforms = g_slist_append(proposal->transforms, transform);
}

/**
 * Determine if given proposal is valid
 */
gboolean proposal_is_valid(proposal_t *proposal)
{
	GSList *ct;
	transform_t *t;
	guint8 transform_counters[IKEV2_TRANSFORM_MAX];

	g_assert((proposal->protocol_id == IKEV2_PROTOCOL_IKE)
			|| (proposal->protocol_id == IKEV2_PROTOCOL_ESP)
			|| (proposal->protocol_id == IKEV2_PROTOCOL_AH));

	memset(transform_counters, 0, sizeof(guint8) * IKEV2_TRANSFORM_MAX);

	for (ct = proposal->transforms; ct; ct = ct->next) {
		t = ct->data;

		g_assert(t->type <= IKEV2_TRANSFORM_MAX);

		transform_counters[t->type]++;
	}

	switch (proposal->protocol_id) {
	case IKEV2_PROTOCOL_IKE:
		return ((transform_counters[IKEV2_TRANSFORM_ENCR] > 0)
			 && (transform_counters[IKEV2_TRANSFORM_PRF] > 0)
			 && (transform_counters[IKEV2_TRANSFORM_INTEGRITY] > 0)
			 && (transform_counters[IKEV2_TRANSFORM_DH_GROUP] > 0));
	case IKEV2_PROTOCOL_ESP:
		return ((transform_counters[IKEV2_TRANSFORM_ENCR] > 0)
			 && (transform_counters[IKEV2_TRANSFORM_PRF] == 0));
	case IKEV2_PROTOCOL_AH:
		return ((transform_counters[IKEV2_TRANSFORM_INTEGRITY] > 0)
			 && (transform_counters[IKEV2_TRANSFORM_ENCR] == 0)
			 && (transform_counters[IKEV2_TRANSFORM_PRF] == 0));
	}

	return FALSE;
}

/**
 * Intersect two proposals. The first proposal (proposal1) is of higher
 * priority meaning that protocols in that list have priority and are,
 * one by one, searched for in second proposal. Exactly one valid proposal
 * is returned!
 *
 * @param proposal1
 * @param proposal2
 * @param protocol
 *
 * \return Intersection proposal or NULL in case no proposal matches...
 *
 * TODO TODO TODO TODO
 * LINES ON THIS PLACE CAN EXCLUSIVLY BE LONGER THAN 80 COLUMNS BUT IT MEANS
 * THIS IS HIGH ON TODO LIST TO BE REWRITTEN!!!
 *
 * WARNING
 * This function has interesting side efect during negotiations. Namely,
 * because proposal1 has higher priority if initiator, after receiving
 * response from sender, puts his proposals first then result will have
 * SPI from initiator resulting in duplicate SPI, same value for initiator
 * and responder. This will be put into kernel and ofcourse nothing will
 * work!
 *
 * BUG: This function returns proposal that might include more than one
 * transform of the same type!
 */
proposal_t *proposal_intersect(GSList *proposal1, GSList *proposal2,
		guint8 protocol)
{
	GSList *cp2, *ct1, *ct2;
	proposal_t *p1, *p2, *proposal;
	transform_t *t1, *t2, *tt;
	guint8 trf_mask1, trf_mask2, trf_mask;

	LOG_FUNC_START(1);

	if (proposal1 == NULL) {
		LOG_BUG("First proposal is NULL");
		return NULL;
	}

	if (proposal2 == NULL) {
		LOG_BUG("Second proposal is NULL");
		return NULL;
	}

	if (protocol != IKEV2_PROTOCOL_IKE && protocol != IKEV2_PROTOCOL_ESP
			&& protocol != IKEV2_PROTOCOL_AH) {
		LOG_BUG("Protocol is not one of IKE, ESP or AH");
		return NULL;
	}

	proposal = g_malloc0(sizeof(proposal_t));

	/*
	 * Iterate over all available proposals in the first set...
	 */
	while (proposal1) {
		p1 = proposal1->data;

		proposal1 = proposal1->next;

		/*
		 * If this proposal is not for requested protocol skip further
		 * processing and continue with next one...
		 */
		if (p1->protocol_id != protocol
			&& p1->protocol_id != IKEV2_PROTOCOL_UNSPEC)
			continue;

		/*
		 * Then for each proposal in the first set iterate over all
		 * proposals in the second set...
		 */
		for (cp2 = proposal2; cp2; cp2 = cp2->next) {
			p2 = cp2->data;

			/*
			 * Check if current proposal matches the requested
			 * protocol
			 */
			if (p2->protocol_id != protocol
				&& p2->protocol_id != IKEV2_PROTOCOL_UNSPEC)
				continue;

			proposal_set_protocol(proposal, protocol);

			if (proposal_get_spi(p1) && proposal_get_spi_size(p1)) {
				proposal_set_spi(proposal, proposal_get_spi(p1));
				proposal_set_spi_size(proposal, proposal_get_spi_size(p1));
			} else if (proposal_get_spi(p2) && proposal_get_spi_size(p2)) {
				proposal_set_spi(proposal, proposal_get_spi(p2));
				proposal_set_spi_size(proposal, proposal_get_spi_size(p2));
			} else {
				proposal_set_spi(proposal, 0);
				proposal_set_spi_size(proposal, 0);
			}

			trf_mask1 = trf_mask2 = trf_mask = 0;

			for (ct1 = p1->transforms; ct1; ct1 = ct1->next) {
				t1 = ct1->data;

				trf_mask1 |= (1 << transform_get_type(t1));

				for (ct2 = p2->transforms; ct2; ct2 = ct2->next) {
					t2 = ct2->data;

					trf_mask2 |= (1 << transform_get_type(t2));

					if (transform_equal(t1, t2)) {
						tt = transform_dup(t1);
						if (!transform_get_key_len(tt) && transform_get_key_len(t2))
							transform_set_key_len(tt, transform_get_key_len(t2));

						proposal_transform_append(proposal, tt);
						trf_mask |= (1 << transform_get_type(t2));
					}
				}
			}

#ifdef DEBUG_PROPOSAL_INTERSECT
_debug_proposal_dump("STEP: ", proposal);
#endif
			if (proposal_is_valid(proposal) && (trf_mask == trf_mask1) && (trf_mask == trf_mask2))
				goto out;

			proposal_clear(proposal);
		}
	}

	proposal_free(proposal);
	proposal = NULL;

out:
	LOG_FUNC_END(1);

	return proposal;
}

/**
 * Intersect two proposals and return complete list of valid proposals. The
 * example for use of this function is when kernel requests SA and sends a
 * a list of valid transforms. That list has to be intersected with list
 * specified in configuration file, and finally, that list is sent to the
 * other side.
 *
 * The first proposal is of higher priority meaning that protocols in that
 * list have priority and are, one by one, searched for in second proposal.
 *
 * @param proposal1
 * @param proposal2
 * @param protocol
 *
 * \return List of common proposals
 */
GSList *proposal_intersect_l(GSList *proposal1, GSList *proposal2,
                guint8 protocol)
{
	GSList *result, *cp2, *ct1, *ct2;
	proposal_t *p1, *p2, *proposal;
	transform_t *t1, *t2;
	guint8 trf_mask1, trf_mask2, trf_mask;

	LOG_FUNC_START(1);

	if (proposal1 == NULL) {
		LOG_BUG("proposal1 == NULL");
		return NULL;
	}

	if (proposal2 == NULL) {
		LOG_BUG("proposal2 == NULL");
		return NULL;
	}

	if (protocol != IKEV2_PROTOCOL_IKE && protocol != IKEV2_PROTOCOL_ESP
			&& protocol != IKEV2_PROTOCOL_AH) {
		LOG_BUG("Protocol is not one of IKE, ESP or AH");
		return NULL;
	}

	result = NULL;

	proposal = g_malloc0(sizeof(proposal_t));

	proposal_set_protocol(proposal, protocol);

	while (proposal1) {
		p1 = proposal1->data;

		proposal1 = proposal1->next;

		if (p1->protocol_id != protocol
			&& p1->protocol_id != IKEV2_PROTOCOL_UNSPEC)
			continue;

		for (cp2 = proposal2; cp2; cp2 = cp2->next) {
			p2 = cp2->data;

			if (p1->protocol_id != p2->protocol_id
				&& p2->protocol_id != IKEV2_PROTOCOL_UNSPEC)
				continue;

			proposal_set_protocol(proposal, protocol);

			if (proposal_get_spi(p1) && proposal_get_spi_size(p1)) {
				proposal_set_spi(proposal, proposal_get_spi(p2));
				proposal_set_spi_size(proposal, proposal_get_spi_size(p2));
			} else if (proposal_get_spi(p2) && proposal_get_spi_size(p2)) {
				proposal_set_spi(proposal, proposal_get_spi(p2));
				proposal_set_spi_size(proposal, proposal_get_spi_size(p2));
			} else {
				proposal_set_spi(proposal, 0);
				proposal_set_spi_size(proposal, 0);
			}

			trf_mask1 = trf_mask2 = trf_mask = 0;

			for (ct1 = p1->transforms; ct1; ct1 = ct1->next) {
				t1 = ct1->data;

				trf_mask1 |= (1 << transform_get_type(t1));

				for (ct2 = p2->transforms; ct2;
							ct2 = ct2->next) {
					t2 = ct2->data;

					trf_mask2 |= (1 <<
							transform_get_type(t2));

					if (transform_equal(t1, t2)) {
						proposal_transform_append(
							proposal,
							transform_dup(t1));
						trf_mask |=
							(1 <<
							transform_get_type(t2));

					}
				}
			}

#ifdef DEBUG_PROPOSAL_INTERSECT
proposal_dump_proposal("STEP: ", proposal);
#endif
			if (proposal_is_valid(proposal)
				&& (trf_mask == trf_mask1)
				&& (trf_mask == trf_mask2)) {
				result = g_slist_append(result, proposal);
				proposal = g_malloc0(sizeof(proposal_t));

			} else
				proposal_clear(proposal);
		}
	}

	proposal_free(proposal);

	LOG_FUNC_END(1);

	return result;
}

/**
 * Create proposal list based on given parameters
 *
 * @param encr_algs
 * @param auth_algs
 * @param protocol_id	Either IKEV2_PROTOCOL_ESP or IKEV2_PROTOCOL_AH
 * @param spi_size
 * @param spi
 *
 * \return List of proposals. Transforms are duplicated.
 */
GSList *proposal_list_create(GSList *encr_algs, GSList *auth_algs,
		guint8 protocol_id, guint8 spi_size, guint64 spi)
{
	GSList *ret = NULL, *e, *a;
	struct proposal *proposal;

	LOG_FUNC_START(1);

	for (a = auth_algs; a; a = a->next) {

		/*
		 * If the protocol is AH then prepare authentication
		 * only proposals.
		 */
		if (protocol_id == IKEV2_PROTOCOL_AH) {
			proposal = proposal_new();

			proposal->protocol_id = protocol_id;
			proposal->spi_size = spi_size;
			proposal->spi = spi;

			proposal->transforms = g_slist_append(NULL,
						transform_dup(a->data));

			ret = g_slist_append(ret, proposal);

		} else {

			/*
			 * Otherwise, prepare proposals with both
			 * encryption and authentication algorithms
			 */
			for (e = encr_algs; e; e = e->next) {
				proposal = proposal_new();

				proposal->protocol_id = protocol_id;
				proposal->spi_size = spi_size;
				proposal->spi = spi;

				proposal->transforms = g_slist_append(NULL,
							transform_dup(e->data));
				proposal->transforms = g_slist_append(
							proposal->transforms,
							transform_dup(a->data));

				ret = g_slist_append(ret, proposal);
			}
		}

	}

	LOG_DEBUG("List of prepared proposals");
	proposal_dump_proposal_list(LOGGERNAME, ret);

	LOG_FUNC_END(1);

	return ret;
}

/**
 * Return first supported proposal given a proposal list and transforms 
 *
 * @param encr_algs
 * @param auth_algs
 *
 * \return List with a single proposal.
 */
struct proposal *proposal_get(GSList *proposals, GSList *encr_algs,
		GSList *auth_algs)
{
	GSList *t;
	struct proposal *proposal;
	transform_t *transform;
	gboolean found;

	LOG_FUNC_START(1);

	found = FALSE;

	while (proposals) {

		proposal = proposals->data;
		proposals = proposals->next;

		found = TRUE;

		/*
		 * Check all the tranforms in the proposal
		 */
		for (t = proposal->transforms; t && found; t = t->next) {

			transform = t->data;

			switch (transform_get_type(transform)) {
			case IKEV2_TRANSFORM_ENCR:
				found = transform_find(transform, encr_algs);
				break;

			case IKEV2_TRANSFORM_INTEGRITY:
				found = transform_find(transform, auth_algs);
				break;

			default:
				LOG_DEBUG("Unsupported transform type");
				break;
			}
		}

		if (found)
			break;
	}

	if (!found)
		proposal = NULL;

	LOG_FUNC_END(1);

	return proposal;
}

/**
 * Set SPI values for all transforms and return copy of proposal chain
 *
 * @param proposals	List of proposals
 * @param protocol
 * @param spi_size	Size of a SPI value
 * @param spi		SPI value
 *
 * \return Pointer to a new structure
 *
 * TODO: It is not necessary to copy transforms since no data is changed
 * in them. So, shared structures could be used to save memory space.
 *
 * Returned values are not checked!
 *
 * Protocol is not used for now!
 */
GSList *proposal_duplicate(GSList *proposals, guint8 protocol, guint8 spi_size,
		guint64 spi)
{
	GSList *newlist, *titerator;
	proposal_t *proposal, *p;
	transform_t *transform;

	newlist = NULL;
	while (proposals) {
		p = proposals->data;
		proposal = proposal_new();
		proposal_set_protocol(proposal, protocol);
		proposal_set_spi_size(proposal, spi_size);
		proposal_set_spi(proposal, spi);

		proposal_list_add_proposal(&proposals, proposal);

		titerator = p->transforms;
		while (titerator) {
			transform  = transform_dup(titerator->data);
			proposal_transform_append(proposal, transform);
			titerator = titerator->next;
		}
		proposals = proposals->next;
	}

	return newlist;
}

/**
 * Set SPI values for all transforms
 *
 * @param proposals             List of proposals
 * @param spi_size              Size of a SPI value
 * @param spi                   SPI value
 *
 */
void proposal_list_set_spi(GSList *proposals, guint8 spi_size, guint64 spi)
{
	LOG_FUNC_START(1);
	LOG_DEBUG("spi_size=%d, spi=%08llx", spi_size, spi);

	while (proposals) {
		proposal_set_spi_size(proposals->data, spi_size);
		proposal_set_spi(proposals->data, spi);
		proposals = proposals->next;
	}

	LOG_FUNC_END(1);
}

transform_t *proposal_list_find_transform(GSList *proposals, guint8 type,
		guint8 protocol)
{
	GSList *ct;
	proposal_t *p;

	LOG_FUNC_START(1);
	LOG_TRACE("type=%d, protocol=%d", type, protocol);

	proposal_dump_proposal_list(LOGGERNAME, proposals);

	while (proposals) {
		if (proposal_get_protocol(proposals->data) == protocol
			|| proposal_get_protocol(proposals->data)
						== IKEV2_PROTOCOL_UNSPEC) {
			p = proposals->data;
			for (ct = p->transforms; ct; ct = ct->next)
				if (transform_get_type(ct->data) == type)
					return ct->data;
		}

		proposals = proposals->next;
	}

	return NULL;
}

transform_t *proposal_find_transform(proposal_t *proposal, guint8 type,
		guint8 protocol)
{
	GSList *ct;
	transform_t *ret;

	LOG_FUNC_START(1);
	LOG_TRACE("type=%d, protocol=%d", type, protocol);

	ret = NULL;
	for (ct = proposal->transforms; ct; ct = ct->next)
		if (transform_get_type(ct->data) == type) {
			ret = ct->data;
			break;
		}

	LOG_FUNC_END(1);
	return ret;
}

/**
 * Search for a transform with a given DH group
 *
 * @param proposals	List of proposals
 * @param dh_group	DH group
 *
 * \return transform with a given group, or NULL if there's none
 */
transform_t *proposal_find_dh_group(GSList *proposals, guint16 dh_group)
{
	GSList *tc;
	proposal_t *p;
	transform_t *t;

	LOG_FUNC_START(1);

	while (proposals) {
		p = proposals->data;
		proposals = proposals->next;

		tc = p->transforms;
		while (tc) {
			t = tc->data;
			tc = tc->next;

			if (t->type == IKEV2_TRANSFORM_DH_GROUP
				 	&& t->id == dh_group)
				goto out;
		}
	}

	t = NULL;

out:
	LOG_FUNC_END(1);

	return t;
}

/*******************************************************************************
 * DEBUG FUNCTIONS
 ******************************************************************************/

void proposal_dump_proposal(char *loggername, proposal_t *proposal)
{
	GSList *counter;

	if (!proposal) {
		LOG_DEBUG("Received NULL pointer");
		return;
	}

	ikev2_log(loggername, LOG_PRIORITY_DEBUG,
			"PROPOSAL DUMP: %s (%d)",
			ikev2_protocols[proposal->protocol_id],
			proposal->protocol_id);

	ikev2_log(loggername, LOG_PRIORITY_DEBUG,
			"PROPOSAL DUMP: spi_size=%d, spi=%llu",
			proposal->spi_size,
			proposal->spi_size ? proposal->spi : 0);

	ikev2_log(loggername, LOG_PRIORITY_DEBUG,
			"Authentication proposals");
	for (counter = proposal->transforms; counter; counter = counter->next) {
		if (((transform_t *)counter->data)->type
						== IKEV2_TRANSFORM_INTEGRITY)
			transform_dump(loggername, counter->data);
	}

	ikev2_log(loggername, LOG_PRIORITY_DEBUG, "Encryption proposals");
	for (counter = proposal->transforms; counter; counter = counter->next) {
		if (((transform_t *)counter->data)->type
						== IKEV2_TRANSFORM_ENCR)
			transform_dump(loggername, counter->data);
	}

	ikev2_log(loggername, LOG_PRIORITY_DEBUG, "PRF proposals");
	for (counter = proposal->transforms; counter; counter = counter->next) {
		if (((transform_t *)counter->data)->type
						== IKEV2_TRANSFORM_PRF)
			transform_dump(loggername, counter->data);
	}

	ikev2_log(loggername, LOG_PRIORITY_DEBUG, "DH group proposals");
	for (counter = proposal->transforms; counter; counter = counter->next) {
		if (((transform_t *)counter->data)->type
						== IKEV2_TRANSFORM_DH_GROUP)
			transform_dump(loggername, counter->data);
	}
}

void proposal_dump_proposal_list(char *loggername, GSList *proposals)
{
	while (proposals) {
		proposal_dump_proposal(loggername, proposals->data);
		proposals = proposals->next;
	}
}

