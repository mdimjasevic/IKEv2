#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <beecrypt/beecrypt.h>
#include <beecrypt/aesopt.h>

/*
 * Buffer sizes. Must be multiple of 16!
 */
#define BUFFER_SIZE	(16 * 1024)

int fromhex(byte* data, const char* hexdata)
{
	int length = strlen(hexdata);
	int count = 0, index = 0;
	byte b = 0;
	char ch;

	if (length & 1)
		count = 1;

	while (index++ < length)
	{
		ch = *(hexdata++);

		b <<= 4;
		if (ch >= '0' && ch <= '9')
			b += (ch - '0');
		else if (ch >= 'A' && ch <= 'F')
			b += (ch - 'A') + 10;
		else if (ch >= 'a' && ch <= 'f')
			b += (ch - 'a') + 10;

		count++;
		if (count == 2)
		{
			*(data++) = b;
			b = 0;
			count = 0;
		}
	}
	return (length+1) >> 1;
}

main(int argc, char **argv)
{
	FILE *infile, *outfile;
	aesParam param;
	unsigned char key[32];
	unsigned char *inbuf, *outbuf;
	size_t keybits, i, len, limit;
	int decrypt;

	if (argc != 5) {
		fprintf (stderr, "Usage: %s [-d|-e] <key> <infile> <outfile>\n",
			argv[0]);
		exit (1);
	}

	keybits = fromhex(key, argv[2]) << 3;

	if (strcmp(argv[1], "-d")) {
		if (strcmp(argv[1], "-e")) {
			fprintf (stderr, "You should specify encrypt or decrypt mode\n");
			exit(1);
		} else
			decrypt = 0;
	} else
		decrypt = 1;

	if (((inbuf = malloc(BUFFER_SIZE)) == NULL)
		|| ((outbuf = malloc(BUFFER_SIZE)) == NULL)) {
		fprintf (stderr, "Memory allocation error\n");
		exit(1);
	}

	if ((infile = fopen(argv[3], "r")) == NULL) {
		fprintf (stderr, "Error opening input file %s\n", argv[3]);
		exit (1);
	}

	if (aesSetup(&param, key, keybits, decrypt ? DECRYPT : ENCRYPT)) {
		fprintf (stderr, "Error seting up AES\n");
		exit(1);
	}

	if (aesSetIV(&param, NULL)) {
		fprintf (stderr, "Error seting up IV for AES\n");
		exit(1);
	}

	if ((outfile = fopen(argv[4], "w")) == NULL) {
		fprintf (stderr, "Error opening output file %s\n", argv[4]);
		exit (1);
	}

	memset(inbuf, 0, BUFFER_SIZE);
	while (len = fread(inbuf, 1, BUFFER_SIZE, infile)) {

		limit = (len + 15) & ~15;

		for (i = 0; i < limit; i += 16) {
			if (decrypt) {
				if (aesDecrypt(&param, (uint32_t *)(outbuf + i),
						(const uint32_t *)(inbuf + i))) {
					fprintf (stderr, "aesDecrypt error\n");
					exit (1);
				}
			} else {
				if (aesEncrypt(&param, (uint32_t *)(outbuf + i),
						(const uint32_t *)(inbuf + i))) {
					fprintf (stderr, "aesEncrypt error\n");
					exit (1);
				}
			}
		}

		if (aesSetIV(&param, outbuf + i)) {
			fprintf (stderr, "Error seting up IV for AES\n");
			exit(1);
		}

		if (fwrite(outbuf, 1, limit, outfile) != limit) {
			fprintf (stderr, "Error writing to output file!\n");
			exit (1);
		}

		memset(inbuf, 0, BUFFER_SIZE);
	}

	fclose(infile);
	fclose(outfile);
}
