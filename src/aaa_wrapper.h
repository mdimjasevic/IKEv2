/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __AAA_WRAPPER_H
#define __AAA_WRAPPER_H

#define AAA_MESSAGE	5

#define AAA_ERROR	-1
#define AAA_CONT	0
#define AAA_ACCEPT	1
#define AAA_REJECT	2
#define AAA_RECEIVED	3

#define AAA_MSG_READY	0
#define AAA_NO_RESPONSE 1

/**
 * Messages to state machines when something is received
 */
struct aaa_msg {
	/*
	 * message type = AAA_MESSAGE
	 */
	guint8 msg_type;

	/*
	 * session match key (pointer value)
	 */
	void *session;

	/*
	 * aaa message type:
	 * - AAA_MSG_READY
	 * - AAA_NO_RESPONSE
	 */
	int type;
};

enum { /* RFC 3748 */
	EAP_CODE_REQUEST = 1,
	EAP_CODE_RESPONSE = 2,
	EAP_CODE_SUCCESS = 3,
	EAP_CODE_FAILURE = 4
};

/**
 * EAP packet header
 */
struct aaa_eap_hdr {
	guint8 code;
	guint8 identifier;
	guint16 length;
} __attribute__((packed));

/**
* EAP additional data in packet (Request/Response packets only)
*/
struct aaa_eap_data {
	guint8 type;
	guint8 type_data[1];
} __attribute__((packed));

#ifndef __SESSION_H
struct session {};
#endif

/* interface to main.c */
int aaa_init(GAsyncQueue *, void *);
void aaa_unload();

/* interface to sm.c  */
int aaa_request(struct session *, void *, char *, int);
int aaa_get_response(struct session *);

/* geters for sm.c */
void *aaa_new_eap_idrq();
int aaa_get_eap_code(struct aaa_eap_hdr *);
int aaa_get_eap_length(struct aaa_eap_hdr *);
int aaa_get_msk(struct session *, char **, guint32 *);
int aaa_get_session_time_limit(struct session *);

/* 'free', used also from sm.c & session.c */
void aaa_free(struct session *);
void aaa_msg_free(struct aaa_msg *);

/* for radius.c */
struct aaa_msg *aaa_msg_new();

/* external dependencies: session.ch: session_use_radius() */

#endif /* __AAA_WRAPPER_H */

