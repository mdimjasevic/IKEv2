/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/ 

#ifndef __SUPPLICANT_H
#define __SUPPLICANT_H

#ifdef __SUPPLICANT_C

/*
 * Structure that holds data for client. This structure is used
 * as a context for all callback functions.
 */
typedef struct supplicant_data {
	struct eap_sm *eap_sm;
	struct wpa_config *wpa_conf;

	struct wpa_config_blob *blob;

	void *eapData;
	size_t eapData_len;

	struct eap_psk_data *ep_data;

	Boolean EAPOL_eapSuccess;
	Boolean EAPOL_eapRestart;
	Boolean EAPOL_eapFail;
	Boolean EAPOL_eapResp;
	Boolean EAPOL_eapNoResp;
	Boolean EAPOL_eapReq;
	Boolean EAPOL_portEnabled;
	Boolean EAPOL_altAccept;
	Boolean EAPOL_altReject;

	guint EAPOL_idleWhile;
} supplicant_t;

#else /* __SUPPLICANT_C */

typedef void supplicant_t;

#endif /* __SUPPLICANT_C */

gboolean supplicant_get_response(supplicant_t *);
gint supplicant_process_request(supplicant_t *, void *, gint);
gboolean supplicant_msk_get(supplicant_t *, gchar **, guint *);
gboolean supplicant_is_success(supplicant_t *);
gboolean supplicant_is_failure(supplicant_t *);

void *supplicant_get_eap_data (supplicant_t *);
size_t supplicant_get_eap_data_len (supplicant_t *);

supplicant_t *supplicant_new(const char *);
void supplicant_free(supplicant_t *);

void supplicant_uninit(void);
int supplicant_init(void);

#endif /* __SUPPLICANT_H */
