/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#define LOGGERNAME	"aes_xcbc"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <openssl/aes.h>
#include <glib.h>
#include "aes_xcbc.h"
#include "logging.h"

/**
 * This is implementation of internal part of aes_xcbc_mac_96 algorithm, as
 * specified in RFC 3566. Function implements whole algorithm, except last
 * result truncation, thus yielding 128-bit fingerprint.
 *
 * For function that completely implements RFC 3566, please call
 * aes_xcbc_mac_96, that will call this function, an then truncate result to
 * required 96 bits.
 *
 * @param key		key to be used; must be of size 128 bits.
 * @param key_len	length of key, in bytes; must be 16 (16*8=128)
 * @param text		text to be hashed; can be empty (of size zero);
 * 			may not be padded...
 * @param text_len	length of text
 * @param digest	where to store result? Must have room for 16 bytes.
 */
void aes_xcbc_mac_128(unsigned char *key, int key_len, unsigned char *text,
			int text_len, char *digest)
{
	AES_KEY aes_key, aes_key_K1;
	unsigned char K1[16];
	unsigned char K2[16];
	unsigned char K3[16];
	unsigned char Ebuf1[16], Ebuf2[16];
	unsigned char *e_prev, *e_curr;
	int blocks;
	int left;
	int i, pos;
	unsigned char *data;

	LOG_FUNC_START(1);

	/* By definition key_len MUST be 128 bits i.e. 16 bytes.
	 * Text len must not be negative.
	 */
	LOG_TRACE("aes_xcbc_mac_128: key_len = %d, text_len = %d.\n",
			key_len, text_len);
	g_assert(key_len == 16 && text_len >= 0);

	blocks = text_len / 16;
	left   = text_len % 16;
	if(!left) blocks--;

	memset(K1, 0x01, 16);
	memset(K2, 0x02, 16);
	memset(K3, 0x03, 16);

	AES_set_encrypt_key(key, key_len*8, &aes_key);
	AES_encrypt(&K1[0],&K1[0],&aes_key);
	AES_encrypt(&K2[0],&K2[0],&aes_key);
	AES_encrypt(&K3[0],&K3[0],&aes_key);

	AES_set_encrypt_key(&K1[0], 16*8, &aes_key_K1);
	e_prev = &Ebuf1[0];
	e_curr = &Ebuf2[0];
	memset(e_prev, 0x00, 16);

	if(!text_len) {
		e_curr[0] = (unsigned char)0x80 ^ K3[0];
		for(pos = 1; pos < 16; pos++) {
			e_curr[pos] = K3[pos];
		}
		AES_encrypt(e_curr,e_prev,&aes_key_K1);
	} else {
		data = text;	
		for(i = 0; i < blocks; i++) {
			for(pos = 0; pos < 16; pos++) {
				e_curr[pos] = e_prev[pos] ^ data[pos];
			}
			data += 16;
			AES_encrypt(e_curr,e_prev,&aes_key_K1);
		}
	
		if( !left ) {
			for(pos = 0; pos < 16; pos++) {
				e_curr[pos] = e_prev[pos] ^ data[pos] ^ K2[pos];
			}
			AES_encrypt(e_curr,e_prev,&aes_key_K1);
		} else {
			for(pos = 0; pos < left; pos++) {
				e_curr[pos] = data[pos];
			}
			e_curr[left] = (unsigned char)0x80;
			for(pos = left+1; pos < 16; pos++) {
				e_curr[pos] = (unsigned char)0x00;
			}
			for(pos = 0; pos < 16; pos++) {
				e_curr[pos] = e_prev[pos] ^ e_curr[pos] ^ K3[pos];
			}
			AES_encrypt(e_curr,e_prev,&aes_key_K1);
		}
	}
	memcpy(digest, e_prev, 16);
}

/**
 * This is simple wrapper around aes_xcbc_mac_128 function that produces 96-bit
 * fingerprint. This is full implementation of aes_xcbc_mac_96 algorithm, as
 * specified in RFC 3566.
 *
 * @param key		key to be used; must be of size 128 bits.
 * @param key_len	length of key, in bytes; must be 16 (16*8=128)
 * @param text		text to be hashed; can be empty (of size zero); may not
 *			be padded...
 * @param text_len	length of text
 * @param digest	where to store result? Must have room for 12 bytes.
 */
void aes_xcbc_mac_96(unsigned char *key, int key_len, unsigned char *text,
			int text_len, char *digest)
{
	unsigned char buf[16];
	aes_xcbc_mac_128(key, key_len, text, text_len, (gchar *)buf);
	memcpy(digest, &buf[0], 12);
}

/** 
 * 
 * This is implementation of pseudo random function AES-XCBC-PRF-128 as
 * defined in RFC 3664. Actually, this is a macro for calling
 * aes_xcbc_mac_128 function. This macro is actually defined in
 * aes_xcbc.h header file, but here is repeated and commented out
 * for documentation purposes. For parameter list see aes_xcbc_mac_128.
 */

/*
#define aes_xcbc_prf_128 aes_xcbc_mac_128
*/
