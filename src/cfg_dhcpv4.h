/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef CFG_DHCPV4

#ifndef __CFG_DHCPV4_H
#define __CFG_DHCPV4_H

/**
 * DHCP Client states
 */
#define DHCLNT_STATE_INIT		0
#define DHCLNT_STATE_SELECTING		1
#define DHCLNT_STATE_REQUESTING		2
#define DHCLNT_STATE_BOUND		3
#define DHCLNT_STATE_RENEWING		4
#define DHCLNT_STATE_REBINDING		5
#define DHCLNT_STATE_FINISHED		6
#define DHCLNT_STATE_ERROR		7

/**
 * DHCP Constants from RFCs
 */
#define BOOTPREQUEST			1
#define BOOTPREPLY			2

/**
 * Assigned hardware type for IPsec tunnels
 */
#define ARPHRD_IPSEC			31

/**
 * DHCP ports
 */
#define DHCP_SERVER_PORT		67
#define DHCP_CLIENT_PORT		68

/**
 * DHCP Options
 */
#define DHCP_OPTION_SUBNETMASK		1
#define DHCP_OPTION_ROUTER		3
#define DHCP_OPTION_DNS			6
#define DHCP_OPTION_HOSTNAME		12
#define DHCP_OPTION_DOMAINNAME		15
#define DHCP_OPTION_BROADCAST		28
#define DHCP_OPTION_NIS			40
#define DHCP_OPTION_NBNS		44
#define DHCP_OPTION_REQUESTEDIP		50
#define DHCP_OPTION_LEASETIME		51
#define DHCP_OPTION_MSGTYPE		53
#define DHCP_OPTION_SERVERID		54
#define DHCP_OPTION_PARAMREQLIST	55
#define DHCP_OPTION_MESSAGE		56
#define DHCP_OPTION_MAXMSGSIZE		57
#define DHCP_OPTION_RENEWALTIME		58
#define DHCP_OPTION_REBINDINGTIME	59
#define DHCP_OPTION_VENDORCLASSID	60
#define DHCP_OPTION_CLIENTID		61
#define DHCP_OPTION_END			255

/**
 * DHCP Message Types
 */
#define DHCP_MSGTYPE_DISCOVER	1
#define DHCP_MSGTYPE_OFFER	2
#define DHCP_MSGTYPE_REQUEST	3
#define DHCP_MSGTYPE_DECLINE	4
#define DHCP_MSGTYPE_ACK	5
#define DHCP_MSGTYPE_NAK	6
#define DHCP_MSGTYPE_RELEASE	7
#define DHCP_MSGTYPE_INFORM	8

#define HWADDR_LEN	6

/**
 * Event types
 */
#define EVENT_NONE		0
#define EVENT_PACKET_RECEIVED	1
#define EVENT_T1_EXPIRED	2
#define EVENT_T2_EXPIRED	3
#define EVENT_TIMEOUT		4
#define EVENT_RELEASE		5

/**
 * DHCPv4 provider's configuration attributes
 */
#define DHCPV4_ATTR_SERVER	"server"
#define DHCPV4_ATTR_RELAY	"relay"
#define DHCPV4_ATTR_PORT	"port"
#define DHCPV4_ATTR_TIMEOUT	"timeout"
#define DHCPV4_ATTR_RETRY	"retry"

/**
 * DHCP Packet
 */
struct dhcpv4_packet {
	/**
	 * OP code, either 1 - BOOTREQUEST, or 2 - BOOTREPLY
	 */
	guint8 op;

	/**
	 * Hardware address type
	 */
	guint8 htype;

	/**
	 * Hardware address length
	 */
	guint8 hlen;

	/**
	 * Number of hops this packet travelled
	 */
	guint8 hops;

	/**
	 * Unique identifier for request
	 */
	guint32 xid;

	/**
	 * Number of seconds this request is valid
	 */
	guint16 secs;
	guint16 flags;

	/**
	 *
	 */
	guint32 ciaddr;

	/**
	 *
	 */
	guint32 yiaddr;

	/**
	 * Server's IP address
	 */
	guint32 siaddr;

	/**
	 * Gateway's, a.k.a. relay's, address
	 */
	guint32 giaddr;

	/**
	 * Client hardware address
	 */
	guint8 chaddr[16];
	guint8 sname[64];
	guint8 files[128];

	guint8 magic_cookie[4];
};

struct dhcpv4_event {

	/**
	 * Event type
	 */
	guint8	type;

	/**
	 * Client to which this particular event is bound
	 */
	struct cfg_data *cfg_data;

	/**
	 * Data specific to event type
	 */
	union {
		struct dhcpv4_data *dd;
		struct timeout_msg *tmsg;
	};
};

#define DHCP_MAXIMUM_PACKET_SIZE	512

/**
 * DHCPv4 data received in a packet or to be sent to a server
 */
struct dhcpv4_data {
	/**
	 * Message type in header
	 */
	guint8 bootp_type;

	/**
	 * Message type in options
	 */
	guint8 dhcp_type;

	/**
	 * Client's identifier to relate requests and responses
	 */
	guint32 xid;

	/**
	 * Subnet mask
	 */
	guint32 subnet;

	/**
	 * Client IP address
	 */
	guint32 ciaddr;

	/**
	 * IP address assigned by DHCP server
	 */
	guint32 yiaddr;

	/**
	 * Default router
	 */
	guint32 router;

	/**
	 * Number of DNS server returned
	 */
	guint8 dns_servers;

	/**
	 * Array with DNS servers
	 */
	guint32 *dns;

	/**
	 * Message len
	 */
	gint message_len;

	/**
	 * Message
	 */
	gchar *message;

	/**
	 * Maximum DHCP Message Size
	 */
	guint16 max_msg_size;

	/**
	 * Lease time
	 */
	guint32 lease_time;

	/**
	 * Server's ID
	 */
	guint32 serverid;

	/**
	 * Renewal time
	 */
	guint32 renew_time;

	/**
	 * Client's ID length
	 */
	guint8 cid_len;

	/**
	 * Client's ID
	 */
	gchar *cid;
};

/**
 * Configuration structure
 */
struct dhcpv4_cfg {

	/**
	 * Reference counter for this structure
	 */
	gint refcnt;

	/**
	 * Timeout value
 	 */
	guint8 timeout;

	/**
	 * Maximum number of retries before giving up on DHCP server
	 */
	guint8 retries;

	/**
	 * DHCP server IP address
	 */
	struct in_addr server;

	/**
	 * DHCP server port
	 */
	guint16 port;

	/**
	 * DHCP releay IP address
	 */
	struct in_addr relay;

	/**
	 * DHCP releay hardware address
	 */
	guchar dhcrelay_hwaddr[HWADDR_LEN];

};

/**
 * Structure with specific client state
 */
struct dhcpv4_client {

	/**
	 * Lock to protect client state
	 */
	GMutex *mutex;

	/**
	 * DHCP instance to use for obtaining parameters
	 */
	struct dhcpv4_cfg *dhcpv4_cfg;

	/**
	 * Queue on which notification about completed request will be
	 * sent
	 */
	GAsyncQueue *queue;

	/**
	 * Data will be sent on a notification queue upon completition of
	 * REQUEST exchange (no matter if it's successful or not)
	 */
	gpointer data;

	/**
	 * Current client state
	 */
	gint8 state;

	/**
	 * Number of retries so far
	 */
	gint8 retries;

	/**
	 * Message to be sent...
	 */
	void *msg;

	/**
	 * Size of the message
	 */
	gint msg_size;

	/**
	 * XID used for the last request
	 */
	guint32 xid;

	/**
	 * Server's ID
	 */
	guint32 serverid;

	/**
	 * When a request is sent, timeout is initialized and stored
	 * in this structure.
	 */
	timeout_t *to;
};

/**
 * Runtime data
 */
struct dhcpv4_provider_data {

	/**
	 * Reference counting
	 */
	gint refcnt;

	/**
	 * Network socket to receive and send DHCP messages
	 */
	network_t *ns;

	/**
	 * Last XID used
	 */
	guint32 xid;

	/**
	 * List of currently active clients
	 */
	GSList *clients;

	/**
	 * Lock for protecting clients list
	 */
	GStaticRWLock clients_lock;
};

/******************************************************************************
 * dhcp_data manipulation functions
 ******************************************************************************/
struct dhcpv4_data *dhcpv4_data_new();
void dhcpv4_data_free(struct dhcpv4_data *);
void dhcpv4_data_dump(struct dhcpv4_data *);

/******************************************************************************
 * Networking related functions
 ******************************************************************************/
struct dhcpv4_data *dhcpv4_packet_parse(struct dhcpv4_packet *, int);
void dhcpv4_send_msg(struct cfg_data *);
gint dhcpv4_add_options(struct dhcpv4_client *, gint);
void dhcpv4_send_discover(network_t *, struct dhcpv4_data *);
void dhcpv4_send_request(network_t *, struct dhcpv4_data *);
void dhcpv4_send_release(network_t *, struct dhcpv4_data *);

/******************************************************************************
 * dhcpv4_client manipulation functions
 ******************************************************************************/
gint dhcpv4_client_init(struct cfg_data *, struct cfg_config_provider *);
int dhcpv4_client_request(struct cfg_data *, GAsyncQueue *, gpointer);
gboolean dhcpv4_client_has_request(struct cfg_data *);
int dhcpv4_client_release(struct cfg_data *, GAsyncQueue *, gpointer);
void dhcpv4_client_free(struct cfg_data *);
int dhcpv4_client_step(struct cfg_data *, struct dhcpv4_event *);
struct in_addr *dhcpv4_client_get_server(struct dhcpv4_client *);
guint16 dhcpv4_client_get_port(struct dhcpv4_client *);

/******************************************************************************
 * Initialization functions
 ******************************************************************************/
void cfg_dhcpv4_instance_free(struct cfg_config_provider *);
int cfg_dhcpv4_instance_init(struct cfg_config_provider *);
void cfg_dhcpv4_unload();
int cfg_dhcpv4_init();

#endif /* __CFG_DHCPV4_H */

#endif /* CFG_DHCPV4 */
