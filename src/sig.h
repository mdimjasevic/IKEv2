/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __SIG_H
#define __SIG_H

#define SIG_MSG		2

/*
 * The following structure is put into state machine's queue when a new
 * signal is received...
 */

struct sig_msg {
	/*
	 * Different message types could be sent to state machine subsystem
	 * and the following field holds discriminator to exact type. For
	 * signal_msg type is set to SIGNAL_MSG.
	 */
	guint8 msg_type;

	/*
	 * Exact signal received from kernel
	 */
	int signum;
};

#endif /* __SIG_H */
