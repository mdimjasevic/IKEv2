/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing network module
 *
 * List all IP addresses on the computer
 */

#define __NETWORK_TEST_C

#define LOGGERNAME	"network"

#define BUFF_SIZE	256

#include <stdio.h>

#include <sys/types.h>
#include <netinet/ip.h>
#include <net/ethernet.h>

#include <netpacket/packet.h>
#include <netinet/ether.h>

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "network.h"
#include "network_msg.h"
#include "config.h"

int main(int argc, char **argv)
{
	GMainLoop *ml;
	network_address_iterator_t *nai;
	netaddr_t *na;
	char buff[256], *interface = NULL;

	struct config *config;

	if (argc != 2 && argc != 3) {
		LOG_ERROR("Usage: %s <network cfg> [<interface>]", argv[0]);
		goto out;
	}

	if (argc == 3)
		interface = argv[2];

	/*
	 * Initialize logging subsystem
	 */
	if (config_init(argv[1], &config) < 0)
		goto out;

	if (logging_init(config->logging) < 0)
		goto out;

	LOG_NOTICE("Initializing network module");
	if (network_init(NULL) < 0)
		goto out;

	nai = network_address_iterator_new(AF_INET, interface);
	if (!nai)
		goto out;

	while ((na = network_address_iterator_next(nai))) {
		LOG_NOTICE("Got address %s",
				netaddr_ip2str(na, buff, BUFF_SIZE));
	}

	network_address_iterator_free(nai);

	/*
	 * Unload network module
	 */
	LOG_NOTICE("Unloading network module");
	network_unload();

	LOG_NOTICE("Exiting");

out:
	logging_unload();

	return 0;
}
