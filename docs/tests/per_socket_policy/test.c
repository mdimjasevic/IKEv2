/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
 *
 * This short program demonstrates how to set up IPsec policy for a specific
 * socket. To compile it into executable use the following command:
 *
 * gcc -I/usr/src/kernels/2.6.15-1.1977_FC5-x86_64/include/ -o test test.c
 *
 * Note that you have to substitute appropriate path to kernel include files
 * for I option. After you start executable it will create socket, define
 * IPsec bypass policy and then sleep for 50 seconds. In the mean time
 * you can check with setkey/ip commands that policy is indeed defined for
 * socket.
 *
 * Note: Seting BYPASS policy is much easier than protect policy. With
 * protect policy you have to define additional data structures that are
 * not used here. If you need that case then look into kernel source... :)
 *
 * Note2: Also, after running this program it might happen that after
 * running this test program error "Operation not permitted" is reported.
 * In that case on of the causes is that af_key module is missing from
 * kernel.
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include <linux/pfkeyv2.h>
#include <linux/ipsec.h>

#define IP_IPSEC_POLICY		16

#define PORT		10000

main (int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in addr;
	struct sadb_x_policy sadb_x_policy;

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		perror ("socket");
		return 1;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0) {
		perror ("bind");
		return 1;
	}

	sadb_x_policy.sadb_x_policy_len = sizeof(struct sadb_x_policy) / 8;
	sadb_x_policy.sadb_x_policy_exttype = SADB_X_EXT_POLICY;
	sadb_x_policy.sadb_x_policy_type = IPSEC_POLICY_BYPASS;
	sadb_x_policy.sadb_x_policy_dir = IPSEC_DIR_INBOUND;

	if (setsockopt(sockfd, SOL_IP, IP_IPSEC_POLICY, &sadb_x_policy, 
					sizeof(struct sadb_x_policy)) < 0) {
		perror ("setsockopt");
		return 1;
	}

	sadb_x_policy.sadb_x_policy_dir = IPSEC_DIR_OUTBOUND;

	if (setsockopt(sockfd, SOL_IP, IP_IPSEC_POLICY, &sadb_x_policy, 
					sizeof(struct sadb_x_policy)) < 0) {
		perror ("setsockopt");
		return 1;
	}

	sleep(50);

	close (sockfd);

	return 0;
}
