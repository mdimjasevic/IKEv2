/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __NETWORK_H
#define __NETWORK_H

/*
 * Buffer size for communication with NETLINK
 */
#define NETLINK_BUFFER_SIZE	2048

/*
 * Maximum message size we can receive from kernel
 *
 * This parameter should be defined during registration of a socket
 */
#define NETWORK_BUFFER_SIZE	4096

/*
 * Maximum size of control information received through recvmsg function call
 */
#define MSG_CONTROL_LEN		128

/***************************************************************************
 * Flags that specify additional services of network subsystem
 ***************************************************************************/

/*
 * Provide destination address of received UDP packet
 */
#define NETWORK_F_DSTADDR	(1 << 0)

/*
 * Listen on IPv6 socket only (if address family is AF_INET6)
 */
#define NETWORK_F_IPV6ONLY	(1 << 1)

/*
 * Internal flag
 */
#define NETWORK_F_NOTIFY_QUEUE	(1 << 10)

#ifdef __NETWORK_C 

/*
 * Per interface data
 */
struct netif {

	/*
	 * List of 'struct netaddr' structures each holding one
	 * IPv4 or IPv6 address.
	 */
	GSList *addresses;

	/*
	 * asciiz name of the interace.
	 */
	char *name;

	/*
	 * Interface index and flags...
	 */
	gint if_index;
	gint if_flags;
};

/*
 * Per address data, either IPv4 of IPv6...
 */
typedef struct network_address {
	/*
	 * Interface to which this address belongs...
	 */
	struct netif *interface;

	/*
	 * Address
	 */
	struct netaddr *netaddr;
} network_address_t;

/*
 * Structure used to implement interation over active addresses
 */
typedef struct network_address_iterator {
	/*
	 * Address family we are searching
	 */
	int family;

	/*
	 * Interface whose addresses we are searching
	 */
	struct netif *ni;

	/*
	 * Address
	 */
	GSList *curr;
} network_address_iterator_t;

/*
 * Per active socket structure
 */
typedef struct network_socket {
	/*
	 * Socket ID
	 */
	gint sockfd;

	/*
	 * Fields for network communication and monitoring
	 */
	GIOChannel *sockfd_io_channel;
	GSource *network_cb;

	/*
	 * Notify channel. In case queues are used for notification
	 * (flags has TO_NOTIFY_QUEUE bit set) then this is
	 * GAsyncQueue, otherwise, it's a thread function.
	 */
	union {
		GAsyncQueue *queue;
		GThreadFunc func;
	} notify;

	/*
	 * Flags that specify additional behavior of a socket
	 */
	int flags;

	/*
	 * Maximum message size that can be received via this socket
	 */
	gint max_msg_len;

	/*
	 * Reference count...
	 */
	gint refcnt;
} network_t;

/*
 * State for network subsystem
 */
struct network_data {
	/*
	 * NETLINK route socket for quering and monitoring interface
	 * and address changes.
	 */
	int rtsock;
	GIOChannel *netlink_io_channel;
	GSource *netlink_cb;

	/*
	 * Context in which networking subsystem runs
	 */
	GMainContext *context;

	/*
	 * List of active IP addresses. Note that the elements of this
	 * list can be shared with elements in bind_addrs list.
	 *
	 * Lock is used to prevent simultaneous reading and modification
	 * to addresses list!
	 *
	 * TODO: lock should be used for interfaces too
	 */
	GStaticRWLock addresses_lock;
	GSList *addresses;

	/*
	 * List of interfaces on a system.
	 *
	 * TODO: This has to be converted into hash indexed by interface name
	 */
	GStaticRWLock interfaces_lock;
	GSList *interfaces;
};

#else /* __NETWORK_C */

typedef void network_t;
typedef void network_address_t;
typedef void network_address_iterator_t;

#endif /* __NETWORK_C */

/*******************************************************************************
 * FUNCTION PROTOTYPES
 ******************************************************************************/

int network_send_packet(network_t *, void *, guint32, struct netaddr *);
gboolean network_data_pending_cb(GIOChannel *, GIOCondition, gpointer);

/*******************************************************************************
 * NETWORK_INTERFACE MANIPULATION FUNCTIONS
 ******************************************************************************/
struct netif *netif_new(void);
void netif_free(struct netif *);
void netif_list_free(GSList *);
GSList *netif_find_addr(struct netif *, struct netaddr *);
struct netif *netif_find_by_id(GSList *, gint);
struct netif *netif_find_by_addr(struct netaddr *);
int netif_remove_by_id(GSList *, gint);
int netif_remove_addr(struct netif *, network_address_t *);
char *netif_get_name(struct netif *);
struct netif *netif_find_by_net(struct netaddr *);

/*******************************************************************************
 * NETWORK_ADDRESS MANIPULATION FUNCTIONS
 ******************************************************************************/
struct network_address *network_address_new(void);
void network_address_free(struct network_address *);

/*******************************************************************************
 * FUNCTIONS ALLOW OTHER SUBSYSTEMS TO ACCESS NETWORK ADDRESSES AND INTERFACES
 ******************************************************************************/
network_address_iterator_t *network_address_iterator_new(int, char *);
netaddr_t *network_address_iterator_next(network_address_iterator_t *);
void network_address_iterator_free(network_address_iterator_t *);

/*******************************************************************************
 * NETWORK_SOCKET MANIPULATION FUNCTIONS
 ******************************************************************************/
struct network_socket *network_socket_new(void);
void network_socket_free(struct network_socket *);
struct network_socket *network_socket_dup_ro(struct network_socket *);
int network_socket_get_sockfd(network_t *);

/*******************************************************************************
 * SOCKET MANIPULATION FUNCTIONS
 ******************************************************************************/
network_t *network_socket_register(int, int, int, struct netaddr *,
		int, gpointer);
network_t *network_socket_register_queue(int, int, int, struct netaddr *,
		int, GAsyncQueue *);
network_t *network_socket_register_thread(int, int, int, struct netaddr *,
		int, gpointer (*GThreadFunc)(gpointer data));
void network_socket_unregister(network_t *);

/*******************************************************************************
 * SUBSYSTEM GLOBAL FUNCTIONS
 ******************************************************************************/
void network_schedule_shutdown(void);
void network_unload(void);
int network_init(GMainContext *);

#endif /* __NETWORK_H */
