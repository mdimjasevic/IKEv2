/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file is used only for testing timeout module
 *
 * This module creates three timeouts that are triggered after 3 (recurring),
 * 7 and 10 seconds. All those timeouts are served by single thread that waits
 * on a queue. Main function also creates timeout thread that after
 * 4 seconds cancels timeout that should expire after 7 seconds. Finally,
 * timeout that triggers after 15 seconds terminates main loop.
 */

#define __TIMEOUT_TEST_C

#define LOGGERNAME	"timeout"

#include <stdio.h>
#include <glib.h>

#include "timeout.h"

/**
 * Thread that servers timeouts for 3, 7 and 10 seconds.
 */
gpointer wait_thread(gpointer _queue)
{
	GTimeVal t1, t2, t;
	GAsyncQueue *queue = _queue;
	GMainLoop *ml;

	/*
	 * Wait for a message...
	 */
	while (1) {
		printf ("Wait thread waiting on a queue\n");
		g_get_current_time(&t1);
		ml = g_async_queue_pop(queue);
		g_get_current_time(&t2);

		t.tv_usec = t2.tv_usec - t1.tv_usec;
		t.tv_sec = t2.tv_sec - t1.tv_sec;
		if (t.tv_usec < 0) {
			t.tv_usec += 1000000;
			t.tv_sec--;
		}

		printf ("Waiting thread was blocked for %u secs %u usecs\n",
				(guint)t.tv_sec, (guint)t.tv_usec);
	}

	return NULL;
}

gpointer cancel_thread(gpointer _tid)
{
	printf ("Started periodic thread. Canceling timeout\n");

	timeout_cancel(_tid);

	return NULL;
}

gpointer terminate_thread(gpointer ml)
{
	printf ("Terminating main loop\n");

	g_main_loop_quit(ml);

	return NULL;
}

int main(int argc, char **argv)
{
	GTimeVal tv, tv1, tv2;
	GMainLoop *ml;
	GTimeVal mainrun;
	GAsyncQueue *queue;
	GThread *wt;
	timeout_t *t1;		// After 3 seconds
	timeout_t *t2;		// After 7 seconds
	timeout_t *t3;		// After 10 seconds
	timeout_t *t4;		// After 4 seconds
	timeout_t *t5;		// After 15 seconds

	/*
	 * Initialize threads in GLib...
	 */
	g_thread_init(NULL);

	/*
	 * We have to provide GLib's main loop to timeout module
	 */
	ml = g_main_loop_new(NULL, FALSE);

	/*
	 * Queue on which we'll send/wait messages
	 */
	queue = g_async_queue_new();

	/*
	 * Start thread that will wait on a queue
	 */
	printf ("Creating waiting thread\n");
	wt = g_thread_create(wait_thread, queue, FALSE, NULL);

	printf ("Initializing timeout module\n");
	timeout_init();

	printf ("Registering timeout in 3 seconds (t1)\n");
	mainrun.tv_sec = 3;
	mainrun.tv_usec = 0;
	t1 = timeout_register_queue(&mainrun, TO_RECURRING, ml, queue);

	printf ("Registering timeout in 7 seconds (t2)\n");
	mainrun.tv_sec = 7;
	mainrun.tv_usec = 0;
	t2 = timeout_register_queue(&mainrun, 0, ml, queue);

	printf ("Registering timeout in 10 seconds (t3)\n");
	mainrun.tv_sec = 10;
	mainrun.tv_usec = 0;
	t3 = timeout_register_queue(&mainrun, 0, ml, queue);

	printf ("Registering timeout in 4 seconds to terminate t2 (t4)\n");
	mainrun.tv_sec = 4;
	mainrun.tv_usec = 0;
	t4 = timeout_register_thread(&mainrun, 0, t2, cancel_thread);

	printf ("Registering timeout in 15 seconds to terminate main loop\n");
	mainrun.tv_sec = 15;
	mainrun.tv_usec = 0;
	t5 = timeout_register_thread(&mainrun, 0, ml, terminate_thread);

	/*
	 * Start main loop...
	 */
	printf ("Starting main loop\n");
	g_get_current_time(&tv1);
	g_main_loop_run(ml);
	g_get_current_time(&tv2);

	tv.tv_usec = tv2.tv_usec - tv1.tv_usec;
	tv.tv_sec = tv2.tv_sec - tv1.tv_sec;
	if (tv.tv_usec < 0) {
		tv.tv_usec += 1000000;
		tv.tv_sec--;
	}

	printf ("Main loop ran for %u secs %u usecs\n",
			(guint)tv.tv_sec, (guint)tv.tv_usec);

	/*
	 * Unload timeout module
	 */
	printf ("Unloading timeout module\n");
	timeout_unload();

	/*
	 * Unreference mainloop, i.e. destroy it...
	 */
	g_main_loop_unref(ml);

	printf ("Exiting\n");
	return 0;
}
