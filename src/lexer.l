%{
/***************************************************************************
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <syslog.h>
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/ip.h>
#endif /* HAVE_NETINET_IN_H */

#include <glib.h>

#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "parser.h"
#include "proposals.h"
#include "radius.h"
#include "config.h"

#undef YY_INPUT
#define YY_INPUT(buf, result, max_size) \
  { result = conf_fgets(buf, max_size); }

#define YY_NO_UNPUT

int conf_fgets(char *, int);
int line_number=1;

static int yy_first_time = 1;
%}

qstring				\"[^\"]*[\"]
ipaddr				([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})|(\[?([0-9A-Fa-f]{1,4}:)([0-9A-Fa-f:]{1,34})\]?)|(::)

%s S_NORM S_ONOFF S_SWCH S_SWCH_ONOFF S_KSPD S_LEVL S_FCLTY S_LGFLG S_ID
%s S_SUBSYS S_GNRL_MODE S_GNRL_SNTY S_PROV S_PROVNAME S_VAL S_REQ

%%

%{
	if (yy_first_time) {
		BEGIN S_NORM;
		yy_first_time = 0;
	}
%}

{qstring} {
	yylval.string = g_memdup(yytext + 1, strlen(yytext) - 1);
	yylval.string[strlen(yytext) - 2] = 0;

	return T_QSTRING;
}

<S_ID>any				{
	/*
	 * Catch any keyword
	 */
	yylval.string = g_strdup(yytext);

	return T_ID;
}

<S_ID>[a-z_46]+\:\[.*\]			{
	/*
	 * Catch identifiers that use sqare braces
	 */
	yylval.string = g_strdup(yytext);

	return T_ID;
}

<S_ID>[a-z_46]+\:[^]{,;[ \t]+		{
	/*
	 * Catch identifiers that don't use sqare braces
	 */
	yylval.string = g_strdup(yytext);

	return T_ID;
}

<S_ID>{ipaddr}(\/[0-9]+)		{
	yylval.string = strdup(yytext);
	return T_ID;
}

<S_ID>{ipaddr}			{

	/*
	 * Check if the first character is '[' and ignore it if it is
	 */
	if (yytext[0] == '[') {
		yylval.string = strdup(yytext + 1);
		yylval.string[strlen(yylval.string) - 1] = 0;
	} else
		yylval.string = strdup(yytext);

	return T_ID;
}

<S_ID>\,				return T_COMMA;

<S_ID>;					{
	BEGIN S_NORM;

	return T_SEMICOLON;
}

<S_ID>\{				{
	BEGIN S_NORM;

	return T_OBRACE;
}

<S_PROVNAME>[a-z0-9_\.:]+		{
	BEGIN S_PROV;
	yylval.string = strdup(yytext);
	return T_SEQ;
}
<S_PROV>type				{
	BEGIN S_VAL;
	return T_TYPE;
}
<S_PROV>provides			{
	BEGIN S_VAL;
	return T_PROVIDES;
}
<S_PROV>;				return T_SEMICOLON;
<S_PROV>[a-z0-9_\.:]+			{
	BEGIN S_VAL;
	yylval.string = strdup(yytext);
	return T_SEQ;
}
<S_PROV>\}				{
	BEGIN S_NORM;
	return T_EBRACE;
}

<S_VAL>[^;, \t{}-]+			{
	yylval.string = strdup(yytext);
	return T_SEQ;
}

<S_VAL>\-				return T_HYPHEN;
<S_VAL>\;				{
	BEGIN S_PROV;
	return T_SEMICOLON;
}

,					return T_COMMA;
\#[^\n]*				/* ignore comments */
[ \t]+					/* ignore white spaces */
\n					{ ++line_number; }
\{					return T_OBRACE;

<S_NORM>{ipaddr}(\/[0-9]+)		{
	yylval.string = strdup(yytext);
	return T_NETADDR;
}

<S_NORM>{ipaddr}(:[0-9]+)		{
	yylval.string = strdup(yytext);
	return T_IPADDR_AND_PORT;
}

<S_NORM>{ipaddr}			{

	/*
	 * Check if the first character is '[' and ignore it if it is
	 */
	if (yytext[0] == '[') {
		yylval.string = strdup(yytext + 1);
		yylval.string[strlen(yylval.string) - 1] = 0;
	} else
		yylval.string = strdup(yytext);

	return T_IPADDR;
}

<S_NORM>general				return T_GENERAL;

<S_NORM>network				return T_NETWORK;

<S_NORM>radius_server			return T_RADIUS_SERVER;

<S_NORM>logging				return T_LOGGING;

<S_NORM>file				return T_FILE;
<S_NORM>log				{ BEGIN S_LGFLG; return T_LOG; }
<S_NORM>level				{ BEGIN S_LEVL; return T_LOGLEVEL; }
<S_NORM>subsystem			{ BEGIN S_SUBSYS; return T_SUBSYSTEM; }
<S_NORM>syslog				{ BEGIN S_FCLTY; return T_SYSLOG; }
<S_NORM>stderr				return T_STDERR;

<S_ONOFF>on				{ BEGIN S_NORM; yylval.number = TRUE; return T_ONOFF; }
<S_ONOFF>off				{ BEGIN S_NORM; yylval.number = FALSE; return T_ONOFF; }

<S_LEVL>fatal				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_FATAL;
	return T_LOGVALUE;
}

<S_LEVL>alert				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_ALERT;
	return T_LOGVALUE;
}

<S_LEVL>crit				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_CRIT;
	return T_LOGVALUE;
}

<S_LEVL>error				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_ERROR;
	return T_LOGVALUE;
}

<S_LEVL>warn				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_WARN;
	return T_LOGVALUE;
}

<S_LEVL>notice				{

	BEGIN S_NORM;
	yylval.number = LOG_PRIORITY_NOTICE;
	return T_LOGVALUE;
}

<S_LEVL>info				{ BEGIN S_NORM; yylval.number = LOG_PRIORITY_INFO;  return T_LOGVALUE; }
<S_LEVL>debug				{ BEGIN S_NORM; yylval.number = LOG_PRIORITY_DEBUG; return T_LOGVALUE; }
<S_LEVL>trace				{ BEGIN S_NORM; yylval.number = LOG_PRIORITY_TRACE; return T_LOGVALUE; }

<S_FCLTY>authpriv			{

	BEGIN S_NORM;
//	yylval.number = LOG_AUTHPRIV;
	return T_FACILITY;
}

<S_FCLTY>local0				{

	BEGIN S_NORM;
//	yylval.number = LOG_LOCAL0;
	return T_FACILITY;
}

<S_FCLTY>daemon				{

	BEGIN S_NORM;
//	yylval.number = LOG_DAEMON;
	return T_FACILITY;
}

<S_FCLTY>user				{

	BEGIN S_NORM;
//	yylval.number = LOG_USER;
	return T_FACILITY;
}

<S_LGFLG>timestamp			{

	yylval.number = LOG_F_TIMESTAMP;
	return T_LOG_FLAG;
}

<S_LGFLG>line				{

	yylval.number = LOG_F_LINE;
	return T_LOG_FLAG;
}

<S_LGFLG>function			{

	yylval.number = LOG_F_FUNCTION;
	return T_LOG_FLAG;
}

<S_LGFLG>severity			{

	yylval.number = LOG_F_SEVERITY;
	return T_LOG_FLAG;
}

<S_LGFLG>subsystem			{

	yylval.number = LOG_F_SUBSYSTEM;
	return T_LOG_FLAG;
}

<S_LGFLG>;				{

	BEGIN S_NORM;
	return T_SEMICOLON;
}

<S_SUBSYS>;				{

	BEGIN S_NORM;
	return T_SEMICOLON;
}

<S_NORM>ikesa_max			{ return T_IKESA_MAX; }
<S_NORM>ikesa_max_halfopened		{ return T_IKESA_MAX_HALFOPENED; }
<S_NORM>rand_device			{ return T_RANDOM_DEVICE; }
<S_NORM>dos_treshold			{ return T_DOS_TRESHOLD; }
<S_NORM>sm_threads			{ return T_SM_THREADS; }
<S_NORM>crl				{ return T_CRL; }
<S_NORM>ca				{ return T_CA; }
<S_NORM>cert				{ return T_CERT; }
<S_NORM>type_x509			{ return T_TYPE_X509; }
<S_NORM>check_interval			{ return T_CHECK_INTERVAL; }
<S_NORM>mode				{ BEGIN S_GNRL_MODE; return T_MODE; }
<S_GNRL_MODE>unspec			{
	BEGIN S_NORM;
	return T_MODE_UNSPEC;
}

<S_GNRL_MODE>initiator			{
	BEGIN S_NORM;
	return T_MODE_INITIATOR;
}

<S_GNRL_MODE>responder			{
	BEGIN S_NORM;
	return T_MODE_RESPONDER;
}
<S_NORM>sanity_check			{ BEGIN S_GNRL_SNTY; return T_SANITY_CHECK; }
<S_GNRL_SNTY>off			{
	BEGIN S_NORM;
	return T_SANCHK_OFF;
}

<S_GNRL_SNTY>warning			{
	BEGIN S_NORM;
	return T_SANCHK_WARNING;
}

<S_GNRL_SNTY>error			{
	BEGIN S_NORM;
	return T_SANCHK_ERROR;
}

<S_NORM>auth_server			return T_AUTH_SERVER;
<S_NORM>acct_server			return T_ACCT_SERVER;
<S_NORM>shared_secret			return T_SHARED_SECRET;
<S_NORM>nas_identifier			return T_NAS_IDENTIFIER;
<S_NORM>timeout				return T_TIMEOUT;
<S_NORM>retries				return T_RETRIES;

<S_NORM>remote				return T_REMOTE;
<S_NORM>natt				{
	BEGIN S_SWCH;
	return T_NATT;
}
<S_NORM>natt_keepalive			return T_NATT_KEEPALIVE;

<S_NORM>any				return T_ANY;

<S_NORM>response_timeout		return T_RESPONSE_TIMEOUT;
<S_NORM>response_retries		return T_RESPONSE_RETRIES;

<S_NORM>(s|sec|secs|second|seconds)	{
	yylval.number = 1;
	return T_TIMEUNIT;
}

<S_NORM>(min|mins|minute|minutes)	{
	yylval.number = 60;
	return T_TIMEUNIT;
}

<S_NORM>(h|hour|hours)			{
	yylval.number = 3600;
	return T_TIMEUNIT;
}

<S_NORM>(day|days)			{
	yylval.number = 86400;
	return T_TIMEUNIT;
}

<S_NORM>(week|weeks)			{
	yylval.number = 604800;
	return T_TIMEUNIT;
}

<S_NORM>(byte|bytes|B)			{
	yylval.number = 1;
	return T_OCTETUNIT;
}

<S_NORM>(KB|kB)				{
	yylval.number = 1LL << 10;
	return T_OCTETUNIT;
}

<S_NORM>(MB)				{
	yylval.number = 1LL << 20;
	return T_OCTETUNIT;
}

<S_NORM>(GB)				{
	yylval.number = 1LL << 30;
	return T_OCTETUNIT;
}

<S_NORM>(TB)				{
	yylval.number = 1LL << 40;
	return T_OCTETUNIT;
}

<S_NORM>peer				{
	BEGIN S_ID;
	return T_PEER;
}

<S_NORM>my_identifier			{
	BEGIN S_ID;
	return T_MY_IDENTIFIER;
}
<S_NORM>authlimit			return T_AUTHLIMIT;
<S_NORM>rekeylimit			return T_REKEYLIMIT;
<S_NORM>octets				return T_OCTETS;
<S_NORM>time				return T_TIME;
<S_NORM>allocs				return T_ALLOCS;
<S_NORM>ike_max_idle			return T_IKE_MAX_IDLE;
<S_NORM>proposal			return T_PROPOSAL;
<S_NORM>encryption_algorithm		return T_PROPOSAL_ENCR;
<S_NORM>auth_algorithm			return T_PROPOSAL_AUTH;
<S_NORM>spdinstall			return T_SPDINSTALL;
<S_NORM>ipsec_level			return T_IPSECLEVEL;
<S_NORM>ipsec_mode			return T_IPSECMODE;
<S_NORM>ipsec_proto			return T_IPSECPROTO;
<S_NORM>pseudo_random_function		return T_PROPOSAL_PRF;
<S_NORM>dh_group			return T_DH_GROUP;
<S_NORM>sainfo				return T_SAINFO;
<S_NORM>local				return T_LOCAL;
<S_NORM>softlimit			return T_SOFTLIMIT;
<S_NORM>hardlimit			return T_HARDLIMIT;
<S_NORM>listen				return T_LISTEN;
<S_NORM>natt_port			return T_NATT_PORT;
<S_NORM>port				return T_PORT;
<S_NORM>opaque				return T_OPAQUE;
<S_NORM>pre_shared_key			return T_PSK;
<S_NORM>rsa_sig				return T_RSA_SIG;
<S_NORM>dss_sig				return T_DSS_SIG;
<S_NORM>ecdss_sig			return T_ECDSS_SIG;
<S_NORM>nonce_size			return T_NONCE_SIZE;
<S_NORM>address				return T_ADDRESS;
<S_NORM>from				return T_FROM;
<S_NORM>ip				return T_IP;
<S_NORM>ipv4				return T_IPV4;
<S_NORM>ipv6				return T_IPV6;
<S_NORM>proto				return T_PROTO;
<S_NORM>icmp-type			return T_ICMPTYPE;
<S_NORM>icmp-code			return T_ICMPCODE;
<S_NORM>auth_method			return T_AUTH_METHOD;
<S_NORM>peer_auth_method		return T_PEER_AUTH_METHOD;
<S_NORM>skip_id_verify			return T_SKIP_ID_VERIFY;
<S_NORM>eap				return T_EAP;
<S_NORM>pidfile				return T_PIDFILE;
<S_NORM>psk_file			return T_PSK_FILE;
<S_NORM>wpa_conf			return T_WPA_CONF;
<S_NORM>cfg_provider			return T_CFG_PROVIDER;
<S_NORM>cfg_request			return T_CFG_REQUEST;
<S_NORM>cfg				return T_CFG;
<S_NORM>providerid			return T_PROVIDERID;
<S_NORM>provider			{
	BEGIN S_PROVNAME;
	return T_PROVIDER;
}
<S_NORM>script_up			return T_SCRIPT_UP;
<S_NORM>script_down			return T_SCRIPT_DOWN;
<S_NORM>remote_id			{
	BEGIN S_ID;
	return T_REMOTE_ID;
}
<S_NORM>nbns				return T_NBNS;
<S_NORM>subnets				return T_SUBNETS;
<S_NORM>subnets6			return T_SUBNETS6;
<S_NORM>appver				return T_APPVER;
<S_NORM>addr				return T_ADDR;
<S_NORM>addr6				return T_ADDR6;
<S_NORM>dhcp				return T_DHCP;
<S_NORM>dhcp6				return T_DHCP6;
<S_NORM>dns				return T_DNS;
<S_NORM>dns6				return T_DNS6;
<S_NORM>request				{ BEGIN S_REQ; return T_REQUEST; }

<S_REQ>;				{ BEGIN S_NORM; return T_SEMICOLON; }

<S_NORM>interface			return T_INTERFACE;
<S_NORM>option				return T_OPTION;

<S_NORM>ocsp				return OCSP;

<S_NORM>kernel_spd			{ BEGIN S_KSPD; return T_KERNELSPD; }
<S_NORM>pfp_flag			{ BEGIN S_SWCH; return PFPFLAG; }
 /* <S_NORM>mobike				{ BEGIN S_SWCH; return T_MOBIKE; } */
	/*
	 * FIXME
	 * Return routability check tokens - short and long option
	 * Mobike support indication token
	*/

<S_NORM>return_routability_check { BEGIN S_SWCH_ONOFF; return RETURN_ROUTABILITY; }
<S_NORM>return_routability { BEGIN S_SWCH_ONOFF; return RETURN_ROUTABILITY; }
<S_NORM>return_routability_timeout	{ return RETURN_ROUTABILITY_TIMEOUT; }
<S_NORM>mobike			{ BEGIN S_SWCH_ONOFF; return T_MOBIKE; }
<S_NORM>cookie2			{ BEGIN S_SWCH_ONOFF; return T_COOKIE2; }
<S_NORM>sa_halfclosed_wait		return SA_HALFCLOSED_WAIT;
<S_NORM>pfs				{ BEGIN S_SWCH; return PFS; }

<S_SWCH>on				{ BEGIN S_NORM; return T_ON;	}
<S_SWCH>off				{ BEGIN S_NORM; return T_OFF; }
<S_SWCH>passive				{ BEGIN S_NORM; return T_PASSIVE; }

	/*
	 * FIXME
	 * New start condition - similiar to S_SWCH, but it does not have 'passive' token
	 */
<S_SWCH_ONOFF>on			{ BEGIN S_NORM; return T_ON;	}
<S_SWCH_ONOFF>off			{ BEGIN S_NORM; return T_OFF; }

<S_KSPD>flush				{ BEGIN S_NORM; return T_KSPD_FLUSH; }
<S_KSPD>sync				{ BEGIN S_NORM; return T_KSPD_SYNC; }
<S_KSPD>rosync				{ BEGIN S_NORM; return T_KSPD_ROSYNC; }
<S_KSPD>generate			{ BEGIN S_NORM; return T_KSPD_GENERATE; }

<S_NORM>\"				return QUOTE;
<S_NORM>;				return T_SEMICOLON;
<S_NORM>:				return T_COLON;
<S_NORM>\}				return T_EBRACE;
<S_NORM>\-				return T_HYPHEN;

[0-9]+					{
	yylval.number = strtoull(yytext, NULL, 0);
	return T_NUMBER;
}

[a-zA-Z0-9_\.]+				{
	yylval.string = strdup(yytext);
	return T_SEQ;
}

%%
