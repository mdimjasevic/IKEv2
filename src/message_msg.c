/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __MESSAGE_C

#define LOGGERNAME	"message"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#include <sys/time.h>

#include <openssl/rand.h>
#include <openssl/sha.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>
#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "proposals.h"
#include "ts.h"
#include "auth.h"
//#include "config.h"
#include "network.h"
#include "payload.h"
#include "sad.h"
#include "message_msg.h"
#include "crypto.h"
#include "cfg.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE MESSAGE_MSG STRUCTURE
 ******************************************************************************/

/**
 * Allocate memory for message subsystem message.
 *
 * \return Pointer to a new message structure or NULL in case of an error.
 */
struct message_msg *message_msg_alloc(void)
{
	struct message_msg *msg;

	LOG_FUNC_START(1);

	if ((msg = g_malloc0(sizeof(struct message_msg))) == NULL)
		return NULL;

	msg->msg_type = MESSAGE_MSG;
	msg->integrity_ok = FALSE;

	LOG_FUNC_END(1);

	return msg;
}

/**
 * Free memory occupied by message subsystem message.
 *
 * @param msg	Pointer to a message structure
 */
void message_msg_free(struct message_msg *msg)
{

	LOG_FUNC_START(1);

	if (msg) {

		if (msg->srcaddr)
			g_free(msg->srcaddr);

		if (msg->dstaddr)
			g_free(msg->dstaddr);

		switch(msg->exchg_type) {
		case IKEV2_EXT_IKE_SA_INIT:

			if (msg->ike_sa_init.expected_cookie)
				g_free(msg->ike_sa_init.expected_cookie);
			
			if (msg->ike_sa_init.tsi)
				ts_list_free(msg->ike_sa_init.tsi);	

			if (msg->ike_sa_init.tsr)
				ts_list_free(msg->ike_sa_init.tsr);	

			if (msg->ike_sa_init.peer_nonce)
				BN_free(msg->ike_sa_init.peer_nonce);

			/*
			 * We do not free nat_sourceip because it's only
			 * pointer to a received packet! The same goes for
			 * nat_destip.
			 */

			break;

		case IKEV2_EXT_IKE_AUTH:
			
			if (msg->ike_auth.i_id)
				id_free(msg->ike_auth.i_id);

			if (msg->ike_auth.r_id)
				id_free(msg->ike_auth.r_id);
			
			if (msg->ike_auth.tsi)
				ts_list_free(msg->ike_auth.tsi);	

			if (msg->ike_auth.tsr)
				ts_list_free(msg->ike_auth.tsr);	

			if (msg->ike_auth.proposals)
				proposal_list_free(msg->ike_auth.proposals); 

#if defined(CFG_CLIENT) || defined(CFG_MODULE)
			if (msg->ike_auth.cfg)
				cfg_free(msg->ike_auth.cfg); 
#endif /* defined(CFG_CLIENT) || defined(CFG_MODULE) */

#if 0
			/**
			 * FIXME: Implement *_free functions!
			 *
			 */
			if (msg->cert) {
				if (msg->cert->pub_key)
					g_free (msg->cert->pub_key);
	
				if (msg->cert->certificate)
					g_free (msg->cert->certificate);
			}

			if (msg->certreq) {
				if (msg->certreq->hash_list)
					g_free (msg->certreq->hash_list);
			}
#endif

			break;

		case IKEV2_EXT_CREATE_CHILD_SA:
		
			/*
			 * CREATE_CHILD_SA request
			 */
			if (msg->create_child_sa.proposals)
				proposal_list_free(
					msg->create_child_sa.proposals);

			if (msg->create_child_sa.peer_nonce)
				BN_free(msg->create_child_sa.peer_nonce);
	
			if (msg->create_child_sa.peer_pub_key)
				BN_free(msg->create_child_sa.peer_pub_key);

			if (msg->create_child_sa.tsi)
				ts_list_free(msg->create_child_sa.tsi);

			if (msg->create_child_sa.tsr)
				ts_list_free(msg->create_child_sa.tsr);

			break;

		case IKEV2_EXT_INFORMATIONAL:

			if (msg->informational.deletes)
				LOG(LOG_PRIORITY_ERROR, "Free memory occupied by"
					" DELETE payload");
			break;

		default:
			break;
		}

		g_free (msg);
	}

	LOG_FUNC_END(1);
}

/**
 * Calculate digest of received message
 */
void message_msg_calc_digest(struct message_msg *msg)
{
	msg->digest = g_malloc(MESSAGE_MSG_DIGEST_SIZE);

	aes_xcbc_mac_128((unsigned char *)DIGEST_SECRET,
			DIGEST_SECRET_LEN,
			msg->buffer, msg->size, msg->digest);
}

/**
 * Return digest of a received message
 */
gpointer message_msg_get_digest(struct message_msg *msg)
{
	return msg->digest;
}

/**
 * Return duplicate digest of a received message
 */
gpointer message_msg_dup_digest(struct message_msg *msg)
{
	return g_memdup(msg->digest, MESSAGE_MSG_DIGEST_SIZE);
}

/**
 * Return size of a digest
 */
gint message_msg_get_digest_size(struct message_msg *msg)
{
	return MESSAGE_MSG_DIGEST_SIZE;
}
