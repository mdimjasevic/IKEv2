This file contains random notes on different implementation decisions.

N(COOKIE)
=========

As per IKEv2 draft:

"... a responder SHOULD - when it detects a large number of half-open
IKE_SAs - reject initial IKE messages unless they contain a Notify
payload of type COOKIE. It SHOULD instead send an unprotected IKE
message as a response and include COOKIE Notify payload with the
cookie data to be returned."

So, the idea is as follows:

- each time new session is created increase number of created but not yet
  fully operational sessions, let's call this counter half_opened_sessions

- when IKE AUTH message is received decrease half_opened_sessions

- now, to create a new session half_opened_sessions has to be below some
  treshold (which is specified in configuration file) OR message has to
  include proper notify payload (cookie)

- in case there is notify payload but it's not a valid one then simply
  ignore request

- in case no session is created because half_opened_sessions is above
  specified treshold and there is NO notify payload send back a message
  with newly created cookie

At the moment the best place for half_opened_sessions counter is ikev2_data
structure.

We support COOKIE payload as of rXYZ.

RETRANSMISIONS
==============

WINDOW SIZE GREATER THAN 1
==========================

There is nothing major at the moment that prevents daemon for allowing window
sizes greater than one. There are few possible problems and/or obstacles that
have to be reconsidered or implemented:

- When thread operates on a CHILD SA it holds a lock on whole session and
  thus prevents other threads from operating on different CHILD SAs of the
  same session. This could be solved by introducing separate locks for
  CHILD SAs, but with caution since some operations in that case might not
  be possible to optimize (e.g. deleteting bunch of CHILD SAs in one go).

  Care should be taken with respect to fields that are changable but also
  used by CHILD SA (i.e. ?_msg_id fields)

- The code does not allow for selective acknowledge of request messages. For
  example, if initiator sent three requests, A, B, C, and responder processed
  B and sent response, then current implementation (if expanded as is) will
  also acknowledge request A!

Handling incoming requests
--------------------------

When new request comes to IKEv2 deamon it has to check if it is expected one.
This check is based on the window size. In the following text we'll use
three variables, peer_msgid_lo, peer_msgid_hi and peer_wsize, all members of
session structure. peer_msgid_lo holds lowest message id of all received
request that we didn't sent response. peer_msgid_hi is the highest request
number seen so far from peer. Finally, peer_wsize holds window size that we
indicated to accept! Apart from those three variables there is also one
important list, sent_resp, also member of sesion structure. sent_resp has
all the received requests with their msg_id's sorted in ascending order.
Finally, msgid will denote message id of received request.

So, when when request comes we first check if it's message id is above allowed
window, i.e.

		msg_id >= peer_msgid_lo + peer_wsize

If it is, then we simply discard packet. Next, if it has message id below
window size, i.e.

		msg_id < peer_msgid_lo - peer_wsize

then it is also discarded. The next case is when msg_id is in the following
range:

		peer_msgid_lo - peer_wsize <= msg_id < peer_msgid_lo

in that case we have repeated request for which we already sent response,
so we should retransmit response.

Finally, when msg_id is within the following bounds:

		peer_msgid_lo <= msg_id < peer_msgid_lo + peer_wsize

then we either already received copy of request and we should ignore this
one, or, we received new request. To discriminate between these two cases,
for each request received, we allocate sent_msg structure with NULL
pointer to buffer and -1 length of packet. Then, we put this structure
into sent_resp list. So, to differentiate between already received request,
and new request, we search through sent_resp list for an element that has
pointer to packet NULL and length of packet -1. If it is there, then we are
processing request, otherwise, it's new request.

When parsing is done, there are two possibilities. Either error occured,
in which case we have to remove reserved sent_msg structure from sent_resp
queue. If there was no error, then, update peer_msgid_hi variable and
free all response sent_msg structures that are below peer's window.

After all the processing is done, and, after the response has been sent,
updates are necessary of state variable, peer_msgid_lo. If just sent
response has msg_id equal to peer_msgid_lo, then this lower bound has to
be increased to the first unsent response (either sent_msg structure doesn't
exist or it exists but has no associated payload and it's length).

Handling incoming responses
---------------------------

For received response, first we have to check if it's inside receive window,
i.e. that it fullfils the following equation:

		msgid_lo <= msg_id < msgid_hi

Then, we search through request queue to find request (note that request
has to be issued!). If there is none, then we already processed response,
so this one should be dropped!

We process response, and if it's valid we have to mark it has been answered
to. It is done by freeing sent_msg and updating variable msgid_lo.

PAD Implementation
==================

IKEv2 supports a form of a PAD via it's configuration file format.
The main features of the PAD as defined in RFC4301 are:

	- possibility to determine authentication type based on
	  the peer's ID

	- determine credentials to use for authentication based
	  on the peer's ID

	- possibility to enforce TSs based on the IDs of the peers

In order to implement those feature the configuration file is
divided into several sections. For the purpose of PAD the most
important section is a list of peer sections. Each peer section
defines behavior for a single or a set of peers. Each peer
section defines authentication methods to be used by both
sides, credentials to be used, as well as supported traffic
selectors along with algorithms to use.

Initiator
---------

Responder
---------
Uppon asserting identity by the Initiator, responder searches
all the peer sections by ID until a match is found.

IKEv2 and SPD
=============

In the theory, SPD contains policy information that IKEv2 uses in some
way to manipulate content of SAD. In other words, IKEv2 _does not_
modify SPD. Based on that assumption PF_KEY was designed.

BUT, there are cases when IKEv2 has to manipulate SPD, or, when it is
more convenient to do so. Those cases are:

1. In case of NAT we don't know the real IP address of the Initiator.

2. More generally, when the client connects from the Internet we
   can not specify policy until client actually connects and
   reveals it's IP address.

3. When client dynamically configures via CFG payloads, we also
   have to modify policy to adjust it to a given set of parameters.

4. In case of a VPN gateway that can have thousands, or even
   hundered of thousands of clients, it is not wise to have
   SPD populated with all the possible client polices (if we
   know in advance their IP addresses). It is much better that
   VPN gateway starts with empty SPD, and then it dynamically
   adds policies as clients connect.

Still, there is one point I don't understand. Namely, in RFC4301
SPD cache is mentioned. Is this cache manipulated by IKEv2 daemon?
Is NETLINK's feature to specify traffic selectors along with SAD
items actually SPD cache?

In the rest of this section we discuss relation between IKEv2
and SPD. The goal is to search and define a possible solution
for implementation that will bahave best in deployments.

For IKEv2 daemon there is a parameter "kernel_spd" in the global
section of the configuration file that defines how IKEv2 will
interact with SPD. The possible values for this parameter are:

flush
	In this case IKEv2 daemon flushes SPD and then adds
	policies as needed. This is intended to be used when
	IKEv2 behaves as a VPN gateway.

sync
	In this case IKEv2 daemon reads all the entries from
	the SPD and matches them with the sainfo configuration
	data. Each sainfo section that is more general than
	the SPD item is bound to the given item. If later
	more specifical TSs are negotiated, IKEv2 places new
	policies into SPD.

rosync
	This one is same as sync, but IKEv2 daemon doesn't
	do any modifications to the policy database.

generate
	In this case IKEv2 daemon inserts into SPD all the
	SAINFO sections that are marked with a keyword
	"spdgenerate". In this case all the sainfo sections
	in the configuration file are active, meaning IKEv2
	will use them during CHILD SA negotiations. Also,
	if more specific traffic selectors are negotiated
	IKEv2 will place them into the policy database.

Now, based on the role of the IKEv2 daemon (initiator or
responder) and additional parameters (dynamic configuration)
and circumstances (NAT) we analyse what happens.

Initiator's behavior
--------------------

We suppose that there will be two cases of starting IKEv2
daemon and establishing VPN connection, on-demand and
preestablishment cases.

In on-demand case the user starts an application that tries
to send some data which, according to SPD, has to be protected.
Kernel then sends request for appropriate SA to IKEv2. In this
case IKEv2 receives ID of the policy that triggered request
and is able to find appropriate configuration section. Notes
for this case:

	We don't know ID of the responder and thus we can not
	search peer sections according to this information.

	We could in remote sections embedd ID of the
	responder. Since gateways will usually be on the
	fixed addresses (or at least at fixed FQDNs) this
	is a viable possiblity.

	We could assume that there is one to one mapping
	between sainfo sections and SPD entries. So, if were
	a given SPD ID we can find peer section. But, when
	CFG and NAT are deployed this might be wrong
	assumption since in that case traffic selectors
	in sainfo sections will have to be more generic!

	Additional question is what to do when we receive
	narrower TSs than were sent? Namely, in that case
	it might happen that Initiator's kernel sends
	traffic according to broader TSs that the other
	side discards because of narrower TSs!

		This case can be simulated with UNIQUE
		SAs in all cases but when syncro parameter
		is given to the kernel_spd directive.

		NETLINK supposedly supports definition of
		traffic selectors for each SA? Should this
		possibility be used no matter what has been
		given to kernel_spd directive?

		If kernel_spd is given sync we are not
		allowed to modify SPD and thus, we can't
		do anything about that problem.

In the preestablishment case, user requests establishment of
connection to VPN gateway _before_ any data is sent. After
connection is established, user starts applications that use
this newly created connection.

	In this case we recevied responder's ID in the
	command line. So, we search remote sections by
	this ID. The ID is given as a remote_id configuration
	parameter.

	We also search peer sections by this ID. Since
	there might be more than one SAINFO section the
	question is which one should be established? For the
	first CHILD SA we take first SAINFO section and
	use it for negotiations. All the other SAINFO
	sections are placed in SPD to trigger further
	CHILD SA negotiations on-demand.

Responder's behavior
--------------------

Responder's behavior is divided into two parts. The first part
is pretprocesing stage executing during the IKEv2 startup
procedure. The other stage executes during IKEv2's runtime.

Responder, during startup, does some preprocessing. This
preprocessing depends on the kernel_spd parameter but it's goal
is to generate all the SPDs that will be used for peers and to
synchronize them with the configuration data.

flush
	IKEv2 flushes kernel' SPD and generates a list of
	policies from the configuration data. No policy
	is placed inside the kernel until needed.

sync
	Policies inside the kernel are read in and matched
	to the configuration data. Data in SPD takes
	priority over configuration data. SAINFO configuration 
	sections that do not have corresponding SPD are
	still used. If some new SPD is necessary, it is
	placed into the kernel and removed when it's not
	needed any more.

rosync
	This one is almost indentical to the sync, but the
	sainfo configuration sections that have no corresponding
	policy in the kernel are disabled. No later modifications
	are possible.

generate
	This one is the same as flush, but some policies,
	those marked with spdinstall, will be installed into
	the kernel.


Responder, during TS processing, behaves as specified in RFC4317.
It performs the following steps to decide what traffic selectors
to use, and based on that, which SAINFO section to use:

1. Search through the SPD for the first item that is the superset
   of all the received traffic selectors from the peer. If one is
   found, then this SAINFO section is a result of the search.

2. Search through the SPD for the first item that is superset of
   the TSi[1]/TSr[1] pair. If one is found, then do intersection
   with of complete TSs received from a peer with the SPD item
   and that is the result of a search for traffic selectors and
   also for the SPD section.

3. Makes intersections, starting from the first SPD item, with
   all of the TSi/TSr received from peer until first non-NULL
   intersection is found. On the first non-NULL intersection
   search stops and the given SPD item is used for all the further
   configurations.

4. No acceptable traffic selectors were found so TS_UNACCEPTABLE
   is returned.

If CFG payload has been received, and IP address has been obtained
for the initiator, then do additional intersection of the resulting
TS with a TS of the following form (IP is obtained address):

	TSi = (IP - IP, 0 - 65535, 0)
	TSr = (0.0.0.0 - 255.255.255.255, 0 - 65535, 0)

IKEv2 and TRAFFIC SELECTORS
===========================

Traffic selectors are in the foundation of the IPsec architecture
and are thus one of the most important feature. They are very complex
data structures. The following TSs example can be used to specify telnet,
http or DNS (udp based) traffic to the local network with the address
192.168.0.0/24 originating from the host 192.168.0.11:

	TSi =	(192.168.0.11, 192.168.0.11, tcp, any)
	TSr =	(192.168.0.0, 192.168.0.255, tcp, 23)
		(192.168.0.0, 192.168.0.255, tcp, 80)

	TSi =	(192.168.0.11, 192.168.0.11, udp, any)
	TSr =	(192.168.0.11, 192.168.0.11, udp, 53)

IKEv2 supports negotation of traffic selectors by including TS payloads
into the IKE_SA_AUTH or CREATE_CHILD_SA messages when creating CHILD
SA. Very complex possibilites arise from this payloads.

Given the above traffic selectors, it is not possible to negotiate them
in a IKEv2 message. If we try to do it the following way:

	TSi =	(192.168.0.11, 192.168.0.11, tcp, any)
		(192.168.0.11, 192.168.0.11, udp, any)
	TSr =	(192.168.0.11, 192.168.0.11, udp, 53)
		(192.168.0.0, 192.168.0.255, tcp, 23)
		(192.168.0.0, 192.168.0.255, tcp, 80)

Then it might mean any combination of TSi and TSr, for example, the
following one:

	TSi =	(192.168.0.11, 192.168.0.11, udp, any)
	TSr =	(192.168.0.0, 192.168.0.255, tcp, 23)

Which is clearly incorrect.

Furthermore, Linux kernel doesn't support multiple traffic selectors
per SPD entry. So, to specify the previous example in the SPD of
the Linux kernel, we should use the following set of policies:

	SPD1
		TSi =	(192.168.0.11, 192.168.0.11, tcp, any)
		TSr =	(192.168.0.0, 192.168.0.255, tcp, 23)

	SPD2
		TSi =	(192.168.0.11, 192.168.0.11, tcp, any)
		TSr =	(192.168.0.0, 192.168.0.255, tcp, 80)

	SPD3
		TSi =	(192.168.0.11, 192.168.0.11, udp, any)
		TSr =	(192.168.0.11, 192.168.0.11, udp, 53)

For the following release of the IKEv2 daemon we decided to implement
TSs as follows:

1. Each sainfo section in configuration file can have only one
   TSi and one TSr

	[It is possible to allow multiple TSs in sainfo section and
	then to decorrelate them]

2. SPD entry can also have single traffic selector. Since, in IKEv2,
   SPD entry contains one sainfo section that describes it, there is
   a seperate TSi and TSr fields in SPD. If those fields are NULL
   then traffic selectors are inherited from the sainfo section,
   otherwise, they are taken from the SPD itself.

3. Each SAD item contains a pointer to a corresponding SPD item. Also,
   SAD item can contain a single TSi and TSr. If those are NULL, then
   items are inherited from the SPD.

One SPD entry can point to a single sainfo section. But a single sainfo
section might be pointed to by multiple SPD items. Furthermore, a
single SAD item can point to a single SPD entry, but single SPD entry
might be pointed to by multiple SA info sections.

AUTHENTICATION
==============

MSGi = ikesa_init_i message | Nr | prf(SK_pi, IDi')

MSGr = ikesa_init_r message | Ni | prf(SK_pr, IDr')

PSK
---

INITIATOR generated AUTH payload

	AUTH = prf(prf(Shared Secret,"Key Pad for IKEv2"), MSGi)

RESPONDER generated AUTH payload

	AUTH = prf(prf(Shared Secret,"Key Pad for IKEv2"), MSGr)

EAP without MSK
---------------

INITIATOR generated AUTH payload

	AUTH = prf(SK_pi, MSGi)

RESPONDER generated AUTH payload

	AUTH = prf(SK_pr, MSGr)

EAP with MSK
------------

RESPONDER generated AUTH payload

	AUTH = prf(MSK, MSGr)

INITIATOR generated AUTH payload

	AUTH = prf(MSK, MSGi)

CHILD SA REKEYING
=================

The question is what happens if during CHILD SA one of the peers
changes traffic selectors and/or proposals? Do we allow this?

	In IKEv2 we require that the traffic selectors be the
	same, and if they are not, then we delete CHILD SA.
	The same goes for the proposals.

What should we do when hard limit is reached? Kernel immediatelly
removes those from the kernel. Should we try to rekey it, ignore
the event, or send delete to the other side?

	For hard limit we send delete to the other side.

	For soft limit we try to rekey SA. If rekeying is not
	successful we wait for the hard limit to expire.

What if CHILD SA used tunnel mode and during rekeying the other
side requested transport mode? The same goes for vice versa.

	We reject this situation.

ROAD WARRIOR BEHAVIOR
=====================

INITIATOR
---------

By calling the initiator in the following way:

                ./ikev2 --iras <iras_id>

The following sequence of steps is executed:

        1. Initiator searches remote section, to find the one that
        has "remote_id" directive equal to <iras_id>

                E: In case there is no <remote_id> section terminate

        2. initiates connection to responder (IKE_SA_INIT)
        2a. requests configuration parameters

        3. Search for a peer section that matches <iras_id>

                E: In case there is no <remote_id> peer section
                send INVALID_SYNTAX and terminate

        4. Sends IKE AUTH request
        4a. offers TSs from the appropriate peer section

        5. upon receiving configuration parameters calls script
        to activate parameters
                - give to the scripte remote and local IDs
                - script also gets all the received configuration parameters

                - if script returned non-zero value, send DELETE IKE SA
                        notify and terminate further processing


        5. puts received TSs into SPD

        6. if lease time has been given, starts the timer to
        renew CFG parameters

When the lease time expires
        1. sends new CFG request parameters
        2. sets new timer
                - what if the parameters are not the same?

Initiator started disconnect
        1. send IKE SA delete request.
                - waits for response certain timeout period
        2. removes TSs from the SPD
        3. calls a script to remove cfg parameters from the kernel
        4. removes IKE SA

Responder started disconnect
        1. receives IKE SA delete request.
        2. removes TSs from the SPD
        3. calls a script to remove cfg parameters from the kernel
        4. sends response and removes IKE SA

RESPODER
--------

Upon receiving IKE_AUTH with a CFG request
        1. Requests IP configuration parameters
        2. Takes all the routes from the routing table
                - or it has configured routes in it's configuration file

        3. Creates TSs
                - intersects TSs proposed by peers
                how!?
                        - maybe by replacing remote IP address with the
                        given IP address?

                - normal sequence of steps is to

        4. Send response
                - calls a script to setup all the parameters in the
                kernel for the client to be able to access internal
                network

        5. Limit IKE SA to lease time

        6. Send response with all the parameters

In case time lease expires

        1. send delete for IKE SA
                - after receiving response
        2. call a script to remove parameters from the kernel
        3. remove SPD entries
        4. remove IKE SA

Received CFG request from the initiator

        1. cancel existing timeout
        2. send DHCP renew request
        3. after receiving response start timer again
        4. send response with CFG payload

Initiator started disconnect

        1. cancel existing timeout (if any)
        2. release IP configuration data
        3. send response
        4. delete IKE SA and all CHILD SAs

Responder started disconnect

        1. cancel existing timeout
        2. send IKE SA delete message
                - after receiving response

        3. release IP configuration data
        4. delete IKE SA and all CHILD SAs
