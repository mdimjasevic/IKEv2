/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __TIMEOUT_H
#define __TIMEOUT_H

/***************************************************************************
 * Flags used when registering timeout
 ***************************************************************************/

/*
 * Used to register timeout in some specific point in time, otherwise,
 * value received is treated as relative in respect to current time.
 */
#define TO_ABSOLUTE		(1<<0)
#define	TO_RECURRING		(1<<2)

#ifdef __TIMEOUT_C

/*
 * Internal flag to demultiplex between queues and threads
 */
#define	TO_NOTIFY_QUEUE		(1<<1)

/*
 * Element that holds timeout data
 */
typedef struct timeout_item {

	/*
	 * Notify type when timeout triggers.
	 */
	gint32 flags;

	/*
	 * Expiration time of this timeout item
	 */
	GTimeVal expire;

	/*
	 * Recurring time of this timeout item
	 */
	GTimeVal recurring;

	/*
	 * Notify channel. In case queues are used for notification
	 * (flags has TO_NOTIFY_QUEUE bit set) then this is
	 * GAsyncQueue, otherwise, it's a thread function.
	 */
	union {
		GAsyncQueue *queue;
		GThreadFunc func;
	} notify;

	/*
	 * Data to be sent to a process
	 */
	void *data;

} timeout_t;

/*
 * Private data for timeout subsystem
 */
struct timeout_data {
	/*
	 * Source ID of a thread that periodically check for expired timeouts
	 */
	guint check_thread;

	/*
	 * Next expected timeout
	 */
	struct timeout_item *next;

	/*
	 * ID of source that will trigger next timeout
	 */
	gint next_src;

	/*
	 * Lock to protect additions and removals from a list of timeouts
	 */
	GStaticRWLock to_lock;

	/*
	 * List of all the registered timeouts sorted in
	 * ascending order.
	 */
	GSList *to_list;
};

#else /* __TIMEOUT_C */

typedef void timeout_t;

#endif /* __TIMEOUT_C */

/*******************************************************************************
 * FUNCTION PROTOTYPES
 ******************************************************************************/

void timeout_reschedule_timeout(void);

struct timeout_item *timeout_item_new();
void timeout_item_free(struct timeout_item *timeout_item);

timeout_t *timeout_register_queue(GTimeVal *, guint32, gpointer, GAsyncQueue *);
timeout_t *timeout_register_thread(GTimeVal *, guint32, gpointer,
		gpointer (*GThreadFunc)(gpointer data));
timeout_t *timeout_register(GTimeVal *, guint32, gpointer, gpointer);
void timeout_cancel(timeout_t *);

gboolean timeout_thread(gpointer);

void timeout_unload(void);
int timeout_init(void);

#endif /* __TIMEOUT_H */
