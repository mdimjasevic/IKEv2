/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

/*
 * This file contains function for manipulation with kernel's security
 * policy database.
 */

#define __SAD_C

#define LOGGERNAME	"sad"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#include <stdlib.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "ikev2.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "crypto.h"
#include "ts.h"
#include "auth.h"
#include "proposals.h"
#include "sad.h"
#include "spd.h"
#include "supplicant.h"
#include "timeout.h"
#include "session.h"

#ifdef PFKEY_LINUX
#include "pfkey_linux.h"
#endif /* PFKEY_LINUX */

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * State for SAD in IKEv2
 */
struct sad_data *sad_data;

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS FOR SAD_ITEM STRUCTURE
 ******************************************************************************/

/**
 * Allocate memory for sad_item structure
 *
 * \return Pointer to a new sad_item structure or NULL in case of an error.
 */
struct sad_item *sad_item_new(void)
{
	struct sad_item *si;

	LOG_FUNC_START(1);

	si = g_malloc0(sizeof(struct sad_item));

	si->mutex = g_mutex_new();
	si->state = SA_STATE_INIT;

	return si;
}

/**
 * Create new sad_item by cloning existing sad_item
 *
 *
 * This function is used during rekeying of SAs when almost identical
 * copy of the SA is necessary.
 *
 * During cloning process, proposals from the template sad_item
 * are transfered to the new sad_item! This is not exactly
 * cloning but it's more efficient and we do not need more than
 * that!
 */
struct sad_item *sad_item_clone(struct sad_item *si)
{
	struct sad_item *nsi;

	nsi = sad_item_new();

	nsi->state = SA_STATE_CREATE;

	nsi->peer_spi = si->peer_spi;

	nsi->ipsec_mode = si->ipsec_mode;

	nsi->proposals = si->proposals;
	si->proposals = NULL;

	nsi->tsl = si->tsl;
	si->tsl = NULL;

	nsi->tsr = si->tsr;
	si->tsr = NULL;

	nsi->spd = spd_item_dup(si->spd);

	return nsi;
}

/**
 * Free sad_item
 *
 * @param sad
 */
void sad_item_free(struct sad_item *si)
{

	LOG_FUNC_START(1);

	if (si) {
		g_mutex_free(si->mutex);

		sad_item_remove_transient(si);

		if (si->spd)
			spd_item_free(si->spd);

		if (si->sk_el)
			g_free(si->sk_el);

		if (si->sk_er)
			g_free(si->sk_er);

		if (si->sk_al)
			g_free(si->sk_al);

		if (si->sk_ar)
			g_free(si->sk_ar);

		if (si->tsl)
			ts_list_free(si->tsl);

		if (si->tsr)
			ts_list_free(si->tsr);

		if (si->src)
			netaddr_free(si->src);

		if (si->dst)
			netaddr_free(si->dst);

		g_free (si);
	}

	LOG_FUNC_END(1);
}

/**
 * Free a list of sad_item items.
 *
 * @param sad_list
 *
 */
void sad_item_list_free(GSList *sad_list)
{
	while (sad_list) {
		sad_item_free(sad_list->data);
		sad_list = g_slist_remove(sad_list, sad_list->data);
	}
}

/*******************************************************************************
 * GETTERS AND SETTERS FOR sad_item STRUCTURE
 ******************************************************************************/

/**
 * Set msg id used for message
 *
 * @param si		CHILD SA structure who's msg_id shoud be set
 * @param msg_id	New MSG ID value
 */
void sad_item_set_msg_id(struct sad_item *si, guint32 msg_id)
{
	si->msg_id = msg_id;
}

/**
 * Get msg id used for CHILD SA message
 *
 * @param csa   CHILD SA structure who's msg_id shoud be returned
 *
 * \return MSG ID
 */
guint32 sad_item_get_msg_id(struct sad_item *si)
{
	return si->msg_id;
}

gboolean sad_item_get_initiator(struct sad_item *si)
{
	return si->initiator;
}

void sad_item_set_initiator(struct sad_item *si, gboolean initiator)
{
	si->initiator = initiator;
}

/**
 * Return negotiated local TS for the CHILD SA
 *
 * @param si
 *
 * This function returns local traffic selector (meaning, it maches
 * local address - destination address in inbound packets, and source
 * address in outbound packets). The value returned is either negotiated
 * traffic selector, or traffic selectors from the SPD.
 *
 * Negotiated traffic selectors are always single, that is, only one
 * TS per direction is present in the linked list.
 */
GSList *sad_item_get_tsl(struct sad_item *si)
{
	/*
	 * Do we have agreed upon traffic selectors for this
	 * CHILD SA? If so, return it, otherwise, return
	 * configuration defined TS.
	 */
	if (si->tsl) {
		LOG_DEBUG("Found SA specific traffic selectors");
		return si->tsl;
	}

	if (si->spd)
		return spd_item_get_tsl(si->spd);

	return NULL;
}

/**
 * Return negotiated remote TS for the CHILD SA
 *
 * This function returns remote traffic selector (meaning, it maches
 * remote address - destination address in outbound packets, and source
 * address in inbound packets).
 */
GSList *sad_item_get_tsr(struct sad_item *si)
{
	/*
	 * Do we have agreed upon traffic selectors for this
	 * CHILD SA? If so, return it, otherwise, return
	 * configuration defined TS.
	 */
	if (si->tsr)
		return si->tsr;

	if (si->spd)
		return spd_item_get_tsr(si->spd);

	return NULL;
}

/**
 * Set traffic selectors for the CHILD SA
 *
 * @param si
 * @param tsl	Local traffic selector
 * @param tsr	Remote traffic selector
 *
 * \return 0	if identical traffic selectors to existing in
 *		the policy were added (actually, in this case
 *		nothing was added since it's already there)
 *	   1	traffic selector were added
 *	   -1	traffic selectors do not match traffic selectors in
 *	   	the policy
 *
 * Before seting given traffic selectors a check is performed to
 * verify that the new traffic selectors are a subset of the
 * existing ones.
 *
 * Furthermore, if new traffic selectors are the same as the
 * existing ones, then they will be not changed, and the success
 * will be returned.
 */
int sad_item_set_ts(struct sad_item *si, GSList *tsl, GSList *tsr)
{
	struct spd_item *sp;

	if (!ts_lists_are_subset(sad_item_get_tsl(si),
			sad_item_get_tsr(si), tsl, tsr))
		return -1;

	if (!ts_lists_are_subset(tsl, tsr, sad_item_get_tsl(si),
			sad_item_get_tsr(si))) {

		sp = si->spd;
		si->spd = spd_item_new_from_prototype(sp, tsl, tsr);

		spd_item_free(sp);

		return 0;
	}

	LOG_DEBUG("Peer sent identical traffic selectors");
	return 0;
}

/**
 * Set local traffic selectors for the CHILD SA
 *
 * @param si
 * @param tsl	Local traffic selector
 *
 * \return 0	if identical traffic selectors to existing in
 *		the policy were added (actually, in this case
 *		nothing was added since it's already there)
 *	   1	traffic selector were added
 *	   -1	traffic selectors do not match traffic selectors in
 *	   	the policy
 *
 * Before seting given traffic selectors a check is performed to
 * verify that the new traffic selectors are a subset of the
 * existing ones.
 *
 * Furthermore, if new traffic selectors are the same as the
 * existing ones, then they will be not changed, and the success
 * will be returned.
 */
int sad_item_set_tsl(struct sad_item *si, struct ts *tsl)
{
	GSList *c;
	gint retval;

	LOG_FUNC_START(1);

	retval = 0;
	/*
	 * Check if there are already traffic selectors and if
	 * they are equal to the existing ones, if so, return.
	 */
	for (c = si->tsl; c; c = c->next)
		if (ts_is_equal(tsl, c->data))
			goto out;

	/*
	 * Next, check if we are allowed by policy to set the
	 * requested traffic selector
	 */
	for (c = spd_item_get_tsl(si->spd); c; c = c->next) {
		if (ts_is_subset(tsl, c->data)) {
			si->tsl = g_slist_append(si->tsl, tsl);
			retval = 1;
			goto out;
		}
	}

	retval = -1;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Set remote traffic selectors for the CHILD SA
 *
 * @param si
 * @param tsr	Remote traffic selector
 *
 * \return 0	if identical traffic selectors to existing in
 *		the policy were added (actually, in this case
 *		nothing was added since it's already there)
 *	   1	traffic selector were added
 *	   -1	traffic selectors do not match traffic selectors in
 *	   	the policy
 *
 * Before seting given traffic selectors a check is performed to
 * verify that the new traffic selectors are a subset of the
 * existing ones.
 *
 * Furthermore, if new traffic selectors are the same as the
 * existing ones, then they will be not changed, and the success
 * will be returned.
 */
int sad_item_set_tsr(struct sad_item *si, struct ts *tsr)
{
	GSList *c;
	gint retval;
	struct ts *ntsr;

	LOG_FUNC_START(1);

	/*
	 * Check if there are already traffic selectors and if
	 * they are equal to the existing ones, if so, return.
	 */
	for (c = si->tsr; c; c = c->next)
		if (ts_is_equal(tsr, c->data)) {
			retval = 0;
			LOG_DEBUG("Found existing traffic selector. "
					"Ignoring new one.");
			goto out;
		}

	/*
	 * Next, check if we are allowed by the parent policy
	 * to set the requested traffic selector
	 */
	for (c = spd_item_get_tsr(si->spd); c; c = c->next) {
		if (ts_is_subset(tsr, c->data)) {
			LOG_DEBUG("New traffic selector supported by the "
					"parent policy. Adding it to SA.");
			ntsr = ts_dup(tsr);
			si->tsr = g_slist_append(si->tsr, ntsr);
			retval = 1;
			goto out;
		}
	}

	LOG_DEBUG("Traffic selector not allowed by the parrent policy. "
			"Ignoring it!");
	retval = -1;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Clear local traffic selectors in SA item (do not touch SP item)
 */
void sad_item_clear_tsl(struct sad_item *si)
{
	if (si->tsl)
		si->tsl = NULL;
}

/**
 * Clear remote traffic selectors in SA item (do not touch SP item)
 */
void sad_item_clear_tsr(struct sad_item *si)
{
	if (si->tsr)
		si->tsr = NULL;
}

/**
 * Get SPI size used for CHILD SA
 *
 * @param csa   CHILD SA structure who's spi size shoud be returned
 *
 * \return SPI size of CHILD SA
 */
guint8 sad_item_get_spi_size(struct sad_item *si)
{
	return si->spi_size;
}

/**
 * Set SPI size value for CHILD SA
 *
 * @param csa           CHILD SA structure who's spi size shoud be set
 * @param spi_size      New SPI size value
 */
void sad_item_set_spi_size(struct sad_item *si, guint8 spi_size)
{
	si->spi_size = spi_size;
}

/**
 * Get local SPI value for CHILD SA
 *
 * @param csa
 *
 * \return
 */
guint64 sad_item_get_spi(struct sad_item *si)
{
	return si->spi;
}

/**
 * Set local SPI value for CHILD SA
 *
 * @param csa
 * @param spi
 */
void sad_item_set_spi(struct sad_item *si, guint64 spi)
{
	si->spi = spi;
}

/**
 * Get peer's SPI value for CHILD SA
 *
 * @param csa
 *
 * \return
 */
guint64 sad_item_get_peer_spi(struct sad_item *si)
{
	return si->peer_spi;
}

/**
 * Set peer's SPI value for CHILD SA
 *
 * @param csa
 * @param peer_spi
 */
void sad_item_set_peer_spi(struct sad_item *si, guint64 peer_spi)
{
	si->peer_spi = peer_spi;
}

/**
 * Get mode of an CHILD SA
 *
 * @param si		CHILD SA structure who's mode shoud be returned
 *
 * \return Mode
 */
guint8 sad_item_get_ipsec_mode(struct sad_item *si)
{
	if (si->ipsec_mode)
		return si->ipsec_mode;

	return spd_item_get_ipsec_mode(si->spd);
}

/**
 * Set mode for the CHILD SA
 *
 * @param si		CHILD SA structure who's mode shoud be set
 * @param mode		IPSEC_MODE_TUNNEL or IPSEC_MODE_TRANSPORT
 *
 * \return Mode
 */
void sad_item_set_ipsec_mode(struct sad_item *si, guint8 mode)
{
	si->ipsec_mode = mode;
}

guint32 sad_item_get_reqid(struct sad_item *si)
{
	return spd_item_get_reqid(si->spd);
}

guint32 sad_item_get_reqid_in(struct sad_item *si)
{
	return spd_item_get_reqid_in(si->spd);
}

guint32 sad_item_get_reqid_fwd(struct sad_item *si)
{
	return spd_item_get_reqid_fwd(si->spd);
}

guint32 sad_item_get_pid(struct sad_item *si)
{
	return spd_item_get_pid(si->spd);
}

guint32 sad_item_get_pid_in(struct sad_item *si)
{
	return spd_item_get_pid_in(si->spd);
}

/**
 * Get proposals of an CHILD SA
 *
 * @param csa           CHILD SA structure who's proposals should be returned
 */
GSList *sad_item_get_proposals(struct sad_item *si)
{
	return si->proposals;
}

/**
 * Set proposals for CHILD SA
 *
 * @param csa           CHILD SA structure who's proposals shoud be set
 * @param proposals     Proposals
 */
void sad_item_set_proposals(struct sad_item *si, GSList *proposals)
{
	if (si->proposals)
		proposal_list_free(si->proposals);

	si->proposals = proposals;
}

/**
 * Set a single proposal for CHILD SA
 *
 * @param csa           CHILD SA structure who's proposals shoud be set
 * @param proposal      Proposal to be set
 */
void sad_item_set_proposal(struct sad_item *si, proposal_t *proposal)
{
	if (si->proposals)
		proposal_list_free(si->proposals);

	si->proposals = g_slist_append(NULL, proposal);
}

/**
 * Get read-only copy of encryption key
 *
 * The returned value MUST NOT be free'd
 */
gchar *sad_item_get_sk_el(struct sad_item *si)
{
	return si->sk_el;
}

/**
 * Get read-only copy of encryption key
 *
 * The returned value MUST NOT be free'd
 */
gchar *sad_item_get_sk_er(struct sad_item *si)
{
	return si->sk_er;
}

/**
 * Get read-only copy of authentication key
 *
 * The returned value MUST NOT be free'd
 */
gchar *sad_item_get_sk_al(struct sad_item *si)
{
	return si->sk_al;
}

/**
 * Get read-only copy of authentication key
 *
 * The returned value MUST NOT be free'd
 */
gchar *sad_item_get_sk_ar(struct sad_item *si)
{
	return si->sk_ar;
}

/**
 * Get encryption algorithms to use for proposals
 */
GSList *sad_item_get_encr_algs(struct sad_item *si)
{
	return spd_item_get_encr_algs(si->spd);
}

/**
 * Get selected encryption algorithm for SA
 */
transform_t *sad_item_get_encr(struct sad_item *si)
{
	return si->encr;
}

/**
 * Get authentication algorithms to use for proposals
 */
GSList *sad_item_get_auth_algs(struct sad_item *si)
{
	return spd_item_get_auth_algs(si->spd);
}

/**
 * Get selected authentication algorithm for SA
 */
transform_t *sad_item_get_auth(struct sad_item *si)
{
	return si->auth;
}

/**
 * Get PFKEY sequence used in ACQUIRE message
 *
 * @param csa           CHILD SA structure who's seq shoud be retrieved
 *
 * This function should be moved to PFKEY module.
 */
guint32 sad_item_get_seq(struct sad_item *si)
{
	return si->seq;
}

/**
 * Set PFKEY sequence used in ACQUIRE message
 *
 * @param csa           CHILD SA structure who's seq shoud be set
 * @param seq           Sequence number
 *
 * This function should be moved to PFKEY module.
 */
void sad_item_set_seq(struct sad_item *si, guint32 seq)
{
	si->seq = seq;
}

/**
 * Get PFKEY sequence used in ACQUIRE message
 *
 * @param csa           CHILD SA structure who's seq shoud be retrieved
 *
 * This function should be moved to PFKEY module.
 */
guint32 sad_item_get_seq_in(struct sad_item *si)
{
	return si->seq_in;
}

/**
 * Set PFKEY sequence used in ACQUIRE message
 *
 * @param csa           CHILD SA structure who's seq shoud be set
 * @param seq           Sequence number
 *
 * This function should be moved to PFKEY module.
 */
void sad_item_set_seq_in(struct sad_item *si, guint32 seq_in)
{
	si->seq_in = seq_in;
}

/**
 * Get state for CHILD SA
 *
 * @param csa   CHILD SA structure which state shoud be returned
 *
 * \return Current state of the CHILD SA
 */
guint8 sad_item_get_state(struct sad_item *si)
{
	return si->state;
}

/**
 * Set state for CHILD SA
 *
 * @param csa   CHILD SA structure which state shoud be set
 * @param state New state of a CHILD SA
 */
void sad_item_set_state(struct sad_item *si, guint8 state)
{
	LOG_DEBUG("Setting sad_item state to %u", state);
	si->state = state;
}

/**
 * Get destination address for rekeying CHILD SA
 *
 * @param csa   CHILD SA structure which destination address shoud be returned
 *
 * \return Destination address
 */
struct netaddr *sad_item_get_rekey_dstaddr(struct sad_item *si)
{
	return si->rekey_dstaddr;
}

/**
 * Set destination address for rekeying CHILD SA
 *
 * @param si	SA structure for whom address should be initialized
 * @param addr	Address of the other end
 */
void sad_item_set_rekey_dstaddr(struct sad_item *si, struct netaddr *addr)
{
	if (si->rekey_dstaddr)
		netaddr_free(si->rekey_dstaddr);

	si->rekey_dstaddr = addr;
}

guint32 sad_item_get_remove_time(struct sad_item *si)
{
	return si->remove_time;
}

void sad_item_set_remove_time(struct sad_item *si, guint32 remove_time)
{
	si->remove_time = remove_time;
}

BIGNUM *sad_item_get_i_nonce(struct sad_item *si)
{
	return si->i_nonce;
}

BIGNUM *sad_item_get_r_nonce(struct sad_item *si)
{
	return si->r_nonce;
}

DH *sad_item_get_dh(struct sad_item *si)
{
	return si->dh;
}

transform_t *sad_item_get_dh_group(struct sad_item *si)
{
	return si->dh_group;
}

struct spd_item *sad_item_get_spd(struct sad_item *si)
{
	return si->spd;
}

void sad_item_set_spd(struct sad_item *si, struct spd_item *spd)
{
	si->spd = spd;
}

/*******************************************************************************
 * MISC spd_item FUNCTIONS
 ******************************************************************************/

void sad_item_lock(struct sad_item *si)
{
	LOG_FUNC_START(1);

	if (si)
		g_mutex_lock(si->mutex);

	LOG_FUNC_END(1);
}

gboolean sad_item_try_lock(struct sad_item *si)
{
	LOG_FUNC_START(1);

	if (si)
		return g_mutex_trylock(si->mutex);

	LOG_FUNC_END(1);
	return FALSE;
}

void sad_item_unlock(struct sad_item *si)
{
	LOG_FUNC_START(1);

	if (si)
		g_mutex_unlock(si->mutex);

	LOG_FUNC_END(1);
}

/**
 * Return protocol bound to SA
 *
 * @param si
 *
 * \return
 *
 * This function returns protocol configured in SPD. Protocol that is
 * used by SA is embedded into proposal.
 */
guint8 sad_item_get_protocol(struct sad_item *si)
{
	return spd_item_get_ipsec_proto(si->spd);
}

/**
 * Return TRUE if given SA waits for a response
 *
 * @param csa
 *
 * \return      TRUE if request has been sent and csa is waiting response,
 *              otherwise FALSE
 */
gboolean sad_item_wait_response(struct sad_item *si)
{
	return (si->state == SA_STATE_CREATE_WAIT
			|| si->state == SA_STATE_REKEY_WAIT);
}

/**
 * Compute all needed session keys for a new child_sa
 *
 * @param si
 * @param sk_d
 * @param prf_type
 * @param i_nonce
 * @param r_nonce
 * @param initiator	Are we on the initiator (true) or on
 *			the responder (false)
 *
 * \return 0 if everything was OK, -1 in case of an error
 *
 * The nonce values are passed separately since in IKE_AUTH exchange
 * we are using nonce values exchanged during IKE_SA_INIT exchange,
 * while during CREATE_CHILD_SA exchange those nonces are stored in
 * sad_item.
 */
int sad_item_compute_keys(struct sad_item *si, char *sk_d,
		BIGNUM *i_nonce, BIGNUM *r_nonce, transform_t *prf_type,
		gboolean initiator)
{
	guint32 nli, nlr, elen = 0, alen = 0, keylen;
	char *pnonces, *nonces, *key, *p;

	LOG_FUNC_START(1);

	nli = BN_num_bytes(i_nonce);
	nlr = BN_num_bytes(r_nonce);

	if (si->encr)
		elen = encr_keylen_get(si->encr);

	if (si->auth)
		alen = integ_keylen_get(si->auth);

	keylen = 2 * elen + 2 * alen;

	pnonces = g_malloc(nli + nlr);

	nonces = pnonces;

	BN_bn2bin(i_nonce, (guchar *)nonces);
	BN_bn2bin(r_nonce, (guchar *)nonces + nli);

	LOG_DEBUG("sk_d");
	log_data(LOGGERNAME, sk_d, transform_get_key_len(prf_type));

	LOG_DEBUG("i_nonce");
	log_data(LOGGERNAME, nonces, nli);

	LOG_DEBUG("r_nonce");
	log_data(LOGGERNAME, nonces + nli, nlr);

#warning "Care should be taken if prf algorithm takes fixed lenght plain text!"
	
	prf_plus(prf_type, sk_d, transform_get_key_len(prf_type),
			nonces, nli + nlr, &key, keylen);

        g_free(pnonces);

	p = key;

	if (si->encr) {
		if (initiator) {
			si->sk_el = g_memdup(p, elen);
			encr_build_key(si->encr, si->sk_el);
		} else {
			si->sk_er = g_memdup(p, elen);
			encr_build_key(si->encr, si->sk_er);
		}
		p += elen;

	}

	if (si->auth) {
		if (initiator)
			si->sk_al = g_memdup(p, alen);
		else
			si->sk_ar = g_memdup(p, alen);
		p += alen;
	}

	if (si->encr) {
		if (initiator) {
			si->sk_er = g_memdup(p, elen);
			encr_build_key(si->encr, si->sk_er);
		} else {
			si->sk_el = g_memdup(p, elen);
			encr_build_key(si->encr, si->sk_el);
		}
		p += elen;
	}

	if (si->auth) {
		if (initiator)
			si->sk_ar = g_memdup(p, alen);
		else
			si->sk_al = g_memdup(p, alen);
	}

	if (si->encr) {
		LOG_DEBUG("si->sk_el");
		log_data(LOGGERNAME, si->sk_el, elen);

		LOG_DEBUG("si->sk_er");
		log_data(LOGGERNAME, si->sk_er, elen);
	}

	if (si->auth) {
		LOG_DEBUG("si->sk_al");
		log_data(LOGGERNAME, si->sk_al, alen);

		LOG_DEBUG("si->sk_ar");
		log_data(LOGGERNAME, si->sk_ar, alen);
	}

	g_free(key);

	LOG_FUNC_END(1);

	return 0;
}

/**
 * Clean transient fields in SA structure
 */
void sad_item_remove_transient(struct sad_item *si)
{
	LOG_FUNC_START(1);

	/*
	 * Remove i_nonce and r_nonce fields
	 */
	if (si->i_nonce) {
		g_free(si->i_nonce);
		si->i_nonce = NULL;
	}

	if (si->r_nonce) {
		g_free(si->r_nonce);
		si->r_nonce = NULL;
	}

	/*
	 * Free rekey_dstaddr field
	 */
	if (si->rekey_dstaddr) {
		netaddr_free(si->rekey_dstaddr);
		si->rekey_dstaddr = NULL;
	}

	/*
	 * Clean fields for rekeying IKE SA
	 */

	/*
	 * Undo effects of the clone function if nsa field is
	 * defined
	 */

	LOG_FUNC_END(1);
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SAD IN KERNEL
 ******************************************************************************/

int sad_getspi(struct sad_item *si, struct netaddr *saddr,
		struct netaddr *daddr)
{
	/*
	 * Initiate request
	 */
	si->spi = sad_data->sad_op->sad_getspi(saddr, daddr,
				sad_item_get_protocol(si),
				sad_item_get_ipsec_mode(si),
				sad_item_get_reqid_in(si),
				si->seq);
	return 0;
}

void sad_update()
{
}

/**
 * Install security association into the kernel
 *
 * @param si
 * @param laddr
 * @param raddr
 * @param spdmodify
 * @param natt
 *
 * \return 0 if everything was OK, -1 in case of an error
 */
int sad_add(struct sad_item *si, struct netaddr *laddr,
		struct netaddr *raddr, gboolean spdmodify,
		gboolean natt)
{
	return sad_data->sad_op->sad_add(si, laddr, raddr, spdmodify, natt);
}

void sad_delete(struct sad_item *si, struct netaddr *laddr,
		struct netaddr *raddr)
{
	sad_data->sad_op->sad_delete(si, laddr, raddr);
}

void sad_get()
{
}

void sad_acquire()
{
}

void sad_register()
{
}

void sad_expire()
{
}

void sad_flush()
{
}

void sad_dump()
{
}

/*******************************************************************************
 * MISCALLENOUS FUNCTIONS
 ******************************************************************************/

/**
 * Thread that waits for responses and notifies from lower level interface
 * to SAD
 */
gpointer sad_thread(gpointer data)
{
	struct pfkey_msg *msg;

	while (TRUE) {

		LOG_DEBUG("Waiting for the message from kernel");
		msg = g_async_queue_pop(sad_data->queue);
		LOG_DEBUG("Received message");

		g_async_queue_push(sad_data->sm_queue, msg);
	}
}

/*******************************************************************************
 * INITIALIZATION FUNCTIONS
 ******************************************************************************/

void sad_set_shutdown_state(void)
{
	if (sad_data->sad_op && sad_data->sad_op->sad_shutdown)
		sad_data->sad_op->sad_shutdown(sad_data->sad_op);
}

void sad_unload()
{
	if (sad_data) {

		if (sad_data->sad_op && sad_data->sad_op->sad_unload)
			sad_data->sad_op->sad_unload(sad_data->sad_op);

		g_free(sad_data);
	}
}

/**
 * Initialize interface to SA database
 *
 * @param context
 * @param sm_queue	Queue for sending notifies and responses to
 *			state machines
 *
 * return 0 initialization was successful, -1 in case of an error
 */
int sad_init(GMainContext *context, GAsyncQueue *sm_queue)
{
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	sad_data = g_malloc0(sizeof(struct sad_data));

	sad_data->sm_queue = sm_queue;

	sad_data->queue = g_async_queue_new();

#ifdef PFKEY_LINUX
	/*
	 * Initialize PFKEY subsystem and return it's handle...
	 */
	if ((sad_data->sad_op = pfkey_linux_sad_init(context,
			sad_data->queue)) == NULL) {
		sad_unload();
		goto out;
	}
#endif

#ifdef PFKEY_SOLARIS
	if ((sad_data->sad_op = pfkey_solaris_init(context,
			sad_data->queue)) == NULL) {
		sad_unload();
		goto out;
	}
#endif

#ifdef NETLINK_LINUX
	if ((sad_data->sad_op = netlink_init(context,
			sad_data->queue)) == NULL) {
		sad_unload();
		goto out;
	}
#endif

	sad_data->thread = g_thread_create(sad_thread, NULL, FALSE, NULL);

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}
