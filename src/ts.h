/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __TS_H
#define __TS_H

/**
 * Traffic selector list
 *
 * We define traffic selector to be the following structure, while
 * TS PAIR (or traffic selector set) is pair (TSi, TSr), i.e.
 * two traffic selectors that can be applied on source and destination
 * addresses in IP packet.
 */
struct ts {
	/**
	 * IP protocol ID that this traffic selector maches (e.g.
	 * ICMP, TCP, UDP, ...). Zero in this field means ANY
	 * IP protocol.
	 */
	guint8 ip_proto_id;

	/*
	 * We are placing start and end ports and addresses in the
	 * following structures.
	 */
	struct netaddr *saddr;
	struct netaddr *eaddr;
};

/*******************************************************************************
 * CONSTRUCTORS AND DESTRUCTORS
 ******************************************************************************/

struct ts *ts_new();
void ts_free(struct ts *);
struct ts *ts_dup(struct ts *);
GSList *ts_list_dup(GSList *);
void ts_list_free(GSList *);
struct ts *ts_new_and_set(guint8, guint16, guint16,
		struct netaddr *, struct netaddr *);

/*******************************************************************************
 * GETTERS AND SETTERS
 ******************************************************************************/

void ts_set_saddr(struct ts *, struct netaddr *);
void ts_set_saddr_from_inaddr(struct ts *, struct in_addr *);
void ts_set_saddr_from_netaddr(struct ts *, struct in_addr *, int);
void ts_set_saddr_from_in6addr(struct ts *, struct in6_addr *);
void ts_set_saddr_from_netaddr6(struct ts *, struct in6_addr *, int);
void ts_set_eaddr(struct ts *, struct netaddr *);
void ts_set_eaddr_from_inaddr(struct ts *, struct in_addr *);
void ts_set_eaddr_from_netaddr(struct ts *, struct in_addr *, int);
void ts_set_eaddr_from_in6addr(struct ts *, struct in6_addr *);
void ts_set_eaddr_from_netaddr6(struct ts *, struct in6_addr *, int);
void ts_set_sport(struct ts *, guint16);
guint16 ts_get_sport(struct ts *);
void ts_set_eport(struct ts *, guint16);
guint16 ts_get_eport(struct ts *);
struct netaddr *ts_get_saddr(struct ts *);
struct netaddr *ts_get_eaddr(struct ts *);
void ts_set_ip_proto_id(struct ts *, guint8);
guint8 ts_get_ip_proto_id(struct ts *);

/*******************************************************************************
 * OTHER METHODS
 ******************************************************************************/

gboolean ts_is_subset(struct ts *, struct ts *);

void *ts_get_saddr_sa(struct ts *);
void *ts_get_eaddr_sa(struct ts *);
guint8 ts_get_ts_type(struct ts *);
int ts_copy_saddr(struct ts *, void *);
int ts_copy_eaddr(struct ts *, void *);
gboolean ts_check_addr(GSList *, struct netaddr *);

GSList *ts_intersect(struct ts *, GSList *);
GSList *ts_lists_intersect(GSList *, GSList *);
GSList *ts_list_intersect(struct ts *, GSList *);
sa_family_t ts_sa_family(struct ts *);

gboolean ts_lists_are_subset(GSList *, GSList *, GSList *, GSList *);
gboolean ts_lists_are_equal(GSList *, GSList *, GSList *, GSList *);
gboolean ts_is_equal(struct ts *, struct ts *);

gboolean ts_list_is_subset(struct ts *, struct ts *, GSList *, GSList *);

gboolean ts_pair_is_subset(struct ts *, struct ts *, struct ts *, struct ts *);

struct netaddr *ts_ts2netaddr(struct ts *);

/*******************************************************************************
 * DEBUG METHODS
 ******************************************************************************/

void ts_dump(char *loggername, struct ts *);
void ts_list_dump(char *loggername, GSList *);


#endif /* __TS_H */

