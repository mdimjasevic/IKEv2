#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/evp.h>
#include <openssl/aes.h>

#define BUFFER_SIZE	(16 * 1024)

int fromhex(unsigned char *data, const char* hexdata)
{
	int length = strlen(hexdata);
	int count = 0, index = 0;
	unsigned char b = 0;
	char ch;

	if (length & 1)
		count = 1;

	while (index++ < length)
	{
		ch = *(hexdata++);

		b <<= 4;
		if (ch >= '0' && ch <= '9')
			b += (ch - '0');
		else if (ch >= 'A' && ch <= 'F')
			b += (ch - 'A') + 10;
		else if (ch >= 'a' && ch <= 'f')
			b += (ch - 'a') + 10;

		count++;
		if (count == 2)
		{
			*(data++) = b;
			b = 0;
			count = 0;
		}
	}
	return (length+1) >> 1;
}

main(int argc, char **argv)
{
	FILE *infile, *outfile;
	unsigned char *inbuf, *outbuf;
	AES_KEY key;
	int keylen, len, limit, i;
	unsigned char ckey[24];
	int decrypt;

	if (argc != 5) {
		fprintf (stderr, "Usage: %s [-e|-d] <key> <infile> <outfile>\n",
			argv[0]);
		exit (1);
	}

	if (strcmp(argv[1], "-d")) {
		if (strcmp(argv[1], "-e")) {
			fprintf (stderr, "Usage: %s [-e|-d] <key> <infile> <outfile>\n",
				argv[0]);
			exit (1);
		} else
			decrypt = 0;
	} else
		decrypt = 1;

	keylen = fromhex(ckey, argv[2]) << 3;

	if ((inbuf = malloc(BUFFER_SIZE)) == NULL) {
		fprintf (stderr, "Memory allocation error\n");
		exit (1);
	}

	if ((outbuf = malloc(BUFFER_SIZE)) == NULL) {
		fprintf (stderr, "Memory allocation error\n");
		exit (1);
	}

	if ((infile = fopen(argv[3], "r")) == NULL) {
		fprintf (stderr, "Unable to open input file %s\n", argv[3]);
		exit (1);
	}

	if ((outfile = fopen(argv[4], "w")) == NULL) {
		fprintf (stderr, "Unable to open output file %s\n", argv[4]);
		exit (1);
	}

	if (decrypt)
		AES_set_decrypt_key(ckey, keylen, &key);
	else
		AES_set_encrypt_key(ckey, keylen, &key);

	while (len = fread(inbuf, 1, BUFFER_SIZE, infile)) {

		limit = (len + 15) & ~15;

		for (i = 0; i < limit; i += 16) {
			if (decrypt)
				AES_decrypt(inbuf + i, outbuf + i, &key);
			else
				AES_encrypt(inbuf + i, outbuf + i, &key);
		}

		if (fwrite(outbuf, 1, limit, outfile) != limit) {
			fprintf (stderr, "Error writing to output file\n");
			exit (1);
		}
	}

	fclose (outfile);
	fclose (infile);

	free(outbuf);
	free(inbuf);
}
