/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __NETWORK_C

#define LOGGERNAME	"network"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <net/if.h>
#include <fcntl.h>

#include <linux/pfkeyv2.h>
#include <linux/ipsec.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "config.h"
#include "network_msg.h"
#include "network.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/**
 * Workaround for some implmenentations that don't define this
 * constant
 */
#ifndef IPV6_PKTINFO
#define IPV6_PKTINFO		50
#endif

/*
 * Private configuration data for network subsystem.
 *
 * TODO: It seems unnecessary to keep list of socket structures here. Maybe
 * it would be better for each module that registers socket to keep a pointer
 * to it (and also protects a list with appropriate sockets). Consistency
 * could be used with reference counting. If implemented that way, then we
 * would be more ceratin that there will be no deadlocks.
 */
struct network_data *network_data = NULL;

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE NETWORK_INTERFACE STRUCTURE
 ******************************************************************************/

/**
 * Create new structure...
 *
 * \return
 */
struct netif *netif_new(void)
{
	struct netif *ni;

	ni = g_malloc0(sizeof(struct netif));

	return ni;
}

/**
 * Free structure
 *
 * @param ni
 *
 */
void netif_free(struct netif *ni)
{
	if (ni) {
		while (ni->addresses) {
			netaddr_free(ni->addresses->data);
			ni->addresses = g_slist_remove(ni->addresses,
							ni->addresses->data);
		}

		if (ni->name)
			g_free(ni->name);
	}
}

/**
 * Return ASCIIZ name of the interface
 *
 * \return Pointer to immutable string containing interface name
 */
char *netif_get_name(struct netif *netif)
{
	return netif->name;
}

/**
 * Free list
 *
 * @param nil
 */
void netif_list_free(GSList *nil)
{
	while (nil) {
		netif_free(nil->data);
		nil = g_slist_remove(nil, nil->data);
	}
}

/**
 * Search interface for a given address.
 *
 * @param ni
 * @param addr
 *
 * \return If address is found return it's GSList node, otherwise, return NULL.
 */
GSList *netif_find_addr(struct netif *ni,
		struct netaddr *addr)
{
	GSList *ac;

	for (ac = ni->addresses; ac; ac = ac->next)
		if (!netaddr_cmp_ip2ip(ac->data, addr))
			return ac;

	return NULL;
}

/**
 * Search interface with a given index.
 *
 * @param interfaces
 * @param if_index
 *
 * \return If interface is found return it's address, otherwise, return NULL.
 */
struct netif *netif_find_by_id(GSList *interfaces,
		gint if_index)
{
	GSList *ac;
	struct netif *ni;

	for (ac = interfaces; ac; ac = ac->next) {
		ni = ac->data;
		if (ni->if_index == if_index)
			return ac->data;
	}

	return NULL;
}

/**
 * Search for a interface with a given IP address.
 *
 * @param addr	Address to search for
 *
 * \return If interface is found return pointer to it, otherwise, return NULL.
 */
struct netif *netif_find_by_addr(struct netaddr *addr)
{
	GSList *c;
	struct network_address *netaddr;

	for (c = network_data->addresses; c; c = c->next) {
		netaddr = c->data;
		if (!netaddr_cmp_ip2ip(addr, netaddr->netaddr))
			return netaddr->interface;
	}

	return NULL;
}

/**
 * Search for a interface with a network that encompass given IP address.
 *
 * @param addr	Address to search for
 *
 * \return If interface is found return pointer to it, otherwise, return NULL.
 *
 * This function has a bug since it searches in random order instead
 * of trying to find longest match. It is easy to implement this
 * functionality but it would be very inefficient. For the moment
 * we are not going to try to resolve this bug until decision is
 * made of what is the most efficient way to handle this situation.
 * Possibilities are: through the kernel, we build FIB using some
 * existing algorithm.
 */
struct netif *netif_find_by_net(struct netaddr *addr)
{
	GSList *c;
	struct network_address *naddr;
	struct netif *netif;

	LOG_FUNC_START(1);

	for (c = network_data->addresses; c; c = c->next) {
		naddr = c->data;
		if (!netaddr_cmp_net2ip(naddr->netaddr, addr))
			break;
	}

	if (c)
		netif = naddr->interface;
	else
		netif = NULL;

	LOG_FUNC_END(1);

	return netif;
}

/**
 * Remove interface with a given index.
 *
 * @param interfaces
 * @param if_index
 *
 * \return 0 if interface was removed, -1 if it was not found.
 */
int netif_remove_by_id(GSList *interfaces, gint if_index)
{
	GSList *ac;
	struct netif *ni;

	for (ac = interfaces; ac; ac = ac->next) {
		ni = ac->data;
		if (ni->if_index == if_index) {
			interfaces = g_slist_remove(interfaces, ni);
			netif_free(ni);
			return 0;
		}
	}

	return -1;
}

/**
 * Remove IP address from network interface
 *
 * @param ni
 * @param addr
 *
 * \return -1 No address found, 0 address successfuly removed
 */
int netif_remove_addr(struct netif *ni,
		struct network_address *addr)
{
	GSList *ac;

	for (ac = ni->addresses; ac; ac = ac->next)
		if (!netaddr_cmp_ip2ip(ac->data, addr->netaddr)) {
			ni->addresses = g_slist_remove(ni->addresses,
						addr->netaddr);
			return 0;
		}

	return -1;
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE NETWORK_ADDRESS STRUCTURE
 ******************************************************************************/

/**
 * Create new structure...
 *
 * \return
 */
struct network_address *network_address_new(void)
{
	struct network_address *na;

	na = g_malloc0(sizeof(struct network_address));

	return na;
}

/**
 * Free structure
 *
 * \return na
 */
void network_address_free(struct network_address *na)
{
	if (na) {
		/*
		 * Detach network address from interface
		 */
		na->interface = NULL;

		netaddr_free(na->netaddr);
	}
}

/*******************************************************************************
 * FUNCTIONS ALLOW OTHER SUBSYSTEMS TO ACCESS NETWORK ADDRESSES AND INTERFACES
 ******************************************************************************/

/**
 * Create iterator that will allow access to all addresses
 *
 * @param family	Address family to return. AF_UNSPEC if all addresses
 *			should be returned
 * @param interface	Interface whose addresses should be returned, or
 *			NULL if all interfaces should be taken
 *
 * \return Iterator, or NULL in case of an error
 *
 * After this function is called, each IP address can be obtained by calling
 * network_address_iterator_next. Iterator should be removed by calling
 * function network_address_iterator_free.
 */
network_address_iterator_t *network_address_iterator_new(int family,
		char *interface)
{
	network_address_iterator_t *nai;
	GSList *nic;
	struct netif *ni = NULL;

	LOG_FUNC_START(1);

	/*
	 * Obtain _read_ lock on address structure
	 */
	g_static_rw_lock_reader_lock(&network_data->addresses_lock);

	nai = g_malloc0(sizeof(struct network_address_iterator));

	nai->family = family;
	nai->curr = network_data->addresses;

	if (!interface)
		goto out;

	nic = network_data->interfaces;
	while(nic) {
		ni = nic->data;
		nic = nic->next;
		if (!strcmp(ni->name, interface)) {
			nai->ni = ni;
			break;
		}
	}

	if (!nai->ni) {
		LOG_ERROR("Specified interface not found!");
		g_free(nai);
		nai = NULL;
		goto out;
	}

	if (nai->curr &&
		((struct network_address *)nai->curr->data)->interface != ni)
		network_address_iterator_next(nai);

out:
	LOG_FUNC_END(1);

	return nai;
}

/**
 * Get next address
 *
 * @param nai	Iterator
 *
 * \return Pointer to netaddr_t structure or NULL if there are no more elements
 */
netaddr_t *network_address_iterator_next(network_address_iterator_t *nai)
{
	netaddr_t *addr = NULL;

	while (nai->curr &&
		(netaddr_get_family(
			((struct network_address *)nai->curr->data)->netaddr)
				!= nai->family
		|| (nai->ni != NULL &&
			((struct network_address *)nai->curr->data)->interface
				!= nai->ni))) {
		nai->curr = nai->curr->next;
	}

	if (nai->curr) {
		addr = ((struct network_address *)nai->curr->data)->netaddr;
		nai->curr = nai->curr->next;
	}

	return addr;
}

/**
 * Free iterator
 *
 * @param nai	Iterator to be freed
 *
 * This function _has_ to be called after iterator is no more used! Otherwise,
 * IKEv2 daemon will deadlock!
 */
void network_address_iterator_free(network_address_iterator_t *nai)
{
	g_free(nai);

	/*
	 * Release _read_ lock on address structure
	 */
	g_static_rw_lock_reader_unlock(&network_data->addresses_lock);
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE NETWORK_SOCKET STRUCTURE
 ******************************************************************************/

/**
 * Create new structure...
 *
 * \return
 */
network_t *network_socket_new(void)
{
	network_t *ns;

	ns = g_malloc0(sizeof(network_t));

	ns->refcnt = 1;
	ns->max_msg_len = NETWORK_BUFFER_SIZE;

	return ns;
}

/**
 * Free structure
 *
 * @param ns
 */
void network_socket_free(network_t *ns)
{
	if (ns && g_atomic_int_dec_and_test(&(ns->refcnt))) {

		if (ns->network_cb)
			g_source_destroy(ns->network_cb);

		if (ns->sockfd_io_channel)
			g_io_channel_shutdown(ns->sockfd_io_channel, FALSE,
				NULL);

		if (ns->sockfd)
			close(ns->sockfd);

		if ((ns->flags & NETWORK_F_NOTIFY_QUEUE) && ns->notify.queue)
			g_async_queue_unref(ns->notify.queue);

		g_free(ns);
	}
}

/**
 * Provide read only copy of network_t type
 */
network_t *network_socket_dup_ro(network_t *ns)
{
	g_atomic_int_inc(&(ns->refcnt));

	return ns;
}

/**
 * Return sockfd from network_socket structure
 */
int network_socket_get_sockfd(network_t *ns)
{
	return ns->sockfd;
}

/*******************************************************************************
 * OTHER FUNCTIONS
 ******************************************************************************/

/**
 * Send a packet
 *
 * @param ns		socket to use for sending data.
 * @param data		data to send
 * @param len		length of data
 * @param daddr		destination address
 *
 * \return
 */
int network_send_packet(network_t *ns, void *data, guint32 len,
		struct netaddr *daddr)
{
	struct msghdr msghdr;
	struct iovec iovec[2];
	int ret = -1;

	LOG_FUNC_START(1);

	char a[INET6_ADDRSTRLEN];
	LOG_DEBUG("ns=%p, data=%p, len=%u, daddr=%s", ns, data, len,
				netaddr2str(daddr, a, INET6_ADDRSTRLEN));

	log_buffer(LOGGERNAME, data, len);

	memset(&iovec, 0, 2 * sizeof(struct iovec));
	iovec[1].iov_base = data;
	iovec[1].iov_len = len;

	memset(&msghdr, 0, sizeof(struct msghdr));
	msghdr.msg_name = daddr;
	msghdr.msg_namelen = netaddr_get_sa_size(daddr);

	msghdr.msg_iov = &iovec[1];
	msghdr.msg_iovlen = 1;
	ret = sendmsg(ns->sockfd, &msghdr, 0);

	if (ret < 0)
		LOG_PERROR(LOG_PRIORITY_ERROR);

	LOG_FUNC_END(1);

	return ret;
}

/**
 * This function is called by GLib whenever there is data coming from
 * a network.
 *
 * @param source
 * @param condition
 * @param data		Pointer to a network_t type
 *
 * \return
 *
 * TODO
 *
 * Here has to come protection against DoS attack with large(huge?)
 * data packets....
 *
 * Also, in case this function encounteres error during execution,
 * maybe we should cause some delay in processing. In that case glib
 * should call us again to take message....
 */
gboolean network_data_pending_cb(GIOChannel *source,
		GIOCondition condition, gpointer data)
{
	network_t *ns;
	struct network_msg *msg;
	struct msghdr netmsg;
	struct iovec iovec;
	struct cmsghdr *cmsg;

	LOG_FUNC_START(1);

	ns = data;

	memset(&netmsg, 0, sizeof(struct msghdr));
	memset(&iovec, 0, sizeof(struct iovec));

	msg = network_msg_alloc();

	iovec.iov_base = g_malloc(ns->max_msg_len);
	iovec.iov_len = ns->max_msg_len;

	netmsg.msg_name = netaddr_new();
	netmsg.msg_namelen = netaddr_size();

	netmsg.msg_iov = &iovec;
	netmsg.msg_iovlen = 1;

	netmsg.msg_control = g_malloc(MSG_CONTROL_LEN);
	netmsg.msg_controllen = MSG_CONTROL_LEN;

	if ((msg->size = recvmsg(ns->sockfd, &netmsg, 0)) < 0) {
		LOG_PERROR(1);
		goto out;
	}

	log_buffer(LOGGERNAME, iovec.iov_base, msg->size);

	msg->srcaddr = netmsg.msg_name;
	netmsg.msg_name = NULL;

	if (ns->flags & NETWORK_F_DSTADDR) {
		for (cmsg = CMSG_FIRSTHDR(&netmsg); cmsg;
				cmsg = CMSG_NXTHDR(&netmsg, cmsg)) {
			if (cmsg->cmsg_level == SOL_IP
					&& cmsg->cmsg_type == IP_PKTINFO) {

				msg->dstaddr = netaddr_new_from_inaddr(
					(struct in_addr *)
					((char *)cmsg
						+ sizeof(struct cmsghdr)
						+ sizeof(unsigned int)
						+ sizeof(struct in_addr)));
//				char buf[64];
//				LOG_DEBUG("DSTADDR: %s",
//					netaddr_ip2str(msg->dstaddr, buf, 64));
//				LOG_DEBUG("SRCADDR: %s",
//					netaddr_ip2str(msg->srcaddr, buf, 64));

			} else if (cmsg->cmsg_level == IPPROTO_IPV6 &&
					cmsg->cmsg_type == IPV6_PKTINFO) {

				struct in6_pktinfo *in6_pktinfo;
				in6_pktinfo = (struct in6_pktinfo *)
						CMSG_DATA(cmsg);

				msg->dstaddr = netaddr_new_from_in6addr(
					(struct in6_addr*)
					&(in6_pktinfo->ipi6_addr));

				char buf[INET6_ADDRSTRLEN];
				LOG_DEBUG("DSTADDR: %s",
						netaddr2str(msg->dstaddr, buf,
						INET6_ADDRSTRLEN));
				LOG_DEBUG("SRCADDR: %s",
						netaddr2str(msg->srcaddr, buf,
						INET6_ADDRSTRLEN));
			}
		}
	}

	msg->buffer = g_realloc(iovec.iov_base, msg->size);
	iovec.iov_base = NULL;

	msg->ns = network_socket_dup_ro(ns);

	if (ns->flags & NETWORK_F_NOTIFY_QUEUE)
		g_async_queue_push(ns->notify.queue, msg);
	else
		g_thread_create(ns->notify.func, msg, FALSE, NULL);

	msg = NULL;

out:
	if (netmsg.msg_control)
		g_free(netmsg.msg_control);

	if (netmsg.msg_name)
		g_free(netmsg.msg_name);

	if (iovec.iov_base)
		g_free(iovec.iov_base);

	if (msg)
		network_msg_free(msg);

	LOG_FUNC_END(1);

	return TRUE;
}

/*******************************************************************************
 * NETWORK SUBSYSTEM MAINTENANCE FUNCTIONS
 ******************************************************************************/

/**
 * Process NETLINK message about link status change
 *
 * @param data
 * @param nlh
 *
 * \return -1 in case of an error, 0 otherwise
 */
int rt_process_linkmsg(struct network_data *data, struct nlmsghdr *nlh)
{
	int len, retval;
	struct ifinfomsg *ifinfomsg;
	struct rtattr *rta;
	struct netif *ni;

	LOG_FUNC_START(1);

	len = nlh->nlmsg_len - NLMSG_LENGTH(0);
	ifinfomsg = NLMSG_DATA(nlh);

	/*
	 * Skip processing if it's a loopback interface
	 */
	if (ifinfomsg->ifi_flags & IFF_LOOPBACK) {
		LOG_DEBUG("Skipping loopback interface with id %u",
				ifinfomsg->ifi_index);
		retval = 0;
		goto out;
	}

	retval = -1;

	switch(nlh->nlmsg_type) {
	case RTM_NEWLINK:

		/*
		 * Search for interface that caused the change
		 */
		ni = netif_find_by_id(data->interfaces,
					ifinfomsg->ifi_index);

		/*
		 * If there is no interface with given ID then add it to a list
		 */
		if (!ni) {

			/*
			 *
			 * FIXME: We have to check if we are allowed to bind to
			 * this interface...
			 *
			 * If we were given interface names on which to listen
			 * then also skip further processing.
			 */
			if ((ni = netif_new()) == NULL)
				goto out;

			ni->if_index = ifinfomsg->ifi_index;
			len -= sizeof(struct ifinfomsg);

			rta = (struct rtattr *)(ifinfomsg + 1);
			while (RTA_OK(rta, len)) {
				if (rta->rta_type == IFLA_IFNAME) {
					ni->name = g_strdup(RTA_DATA(rta));
					LOG_NOTICE("Adding interface %s",
								ni->name);
					break;
				}
				rta = RTA_NEXT(rta, len);
			}

			data->interfaces = g_slist_append(data->interfaces, ni);
		}

		/*
		 * In any case, just record interface status...
		 */
		ni->if_flags = ifinfomsg->ifi_flags;

		break;

	case RTM_DELLINK:
		netif_remove_by_id(data->interfaces,
				ifinfomsg->ifi_index);
                break;
        default:
                LOG_ERROR ("Unknown message from NETLINK\n");
		goto out;
        }

	retval = 0;

	LOG_FUNC_END(1);

out:
	return retval;
}

/**
 * Process NETLINK message about IP address status change
 *
 * @param data
 * @param nlh
 *
 * \return -1 in case of an error, 0 otherwise
 */
int rt_process_addrmsg(struct network_data *data, struct nlmsghdr *nlh)
{
	int len, retval;
	struct netif *ni;
	struct network_address *na;
	struct ifaddrmsg *ifaddrmsg;
	struct rtattr *rta;
	char addr[64];

	LOG_FUNC_START(1);

	len = nlh->nlmsg_len - NLMSG_LENGTH(0);
	ifaddrmsg = NLMSG_DATA(nlh);

	/*
	 * Search for interface on which change occured
	 */
	ni = netif_find_by_id(data->interfaces,
					ifaddrmsg->ifa_index);

	if (!ni) {
		LOG_DEBUG("Change on unmonitored interface %u",
				ifaddrmsg->ifa_index);
		retval = 0;
		goto out;
	}

	retval = -1;

	/*
	 * FIXME: We have to check if we are allowed to bind to
	 * this address...
	 */
	if ((na = network_address_new()) == NULL)
		goto out;

	/*
	 * Search for address and add it to a list. Note: we
	 * assume that the same address is not in the list
	 */
	rta = (struct rtattr *)(ifaddrmsg + 1);
	while (RTA_OK(rta, len)) {
		if (rta->rta_type == IFA_LOCAL) {
			switch(ifaddrmsg->ifa_family) {
			case AF_INET:
				na->netaddr = netaddr_new_from_inaddr(
						RTA_DATA(rta));
				if (!na->netaddr) {
					network_address_free(na);
					na = NULL;
				}
				netaddr_set_prefix(na->netaddr,
						ifaddrmsg->ifa_prefixlen);
				break;
			case AF_INET6:
				na->netaddr = netaddr_new_from_in6addr(
						RTA_DATA(rta));
				if (!na->netaddr) {
					network_address_free(na);
					na = NULL;
				}
				netaddr_set_prefix(na->netaddr,
						ifaddrmsg->ifa_prefixlen);
				break;
			default:
				LOG_ERROR("Internal error!\n");
				network_address_free(na);
				na = NULL;
				break;
			}
			break;
		}
		rta = RTA_NEXT(rta, len);
	}

	if (!na)
		goto out;

	switch(nlh->nlmsg_type) {
	case RTM_NEWADDR:

		/*
		 * This dumping of address inefficient
		 */
		LOG_NOTICE("Adding IP address %s on interface %s",
				netaddr_net2str(na->netaddr, addr, 64),
				ni->name);
		na->interface = ni;
		data->addresses = g_slist_append(data->addresses, na);

		retval = 0;
		break;

	case RTM_DELADDR:
		LOG_NOTICE("Removing IP address %s from interface %s",
				netaddr_ip2str(na->netaddr, addr, 64),
				ni->name);
		netif_remove_addr(ni, na);

		retval = 0;
		break;
	default:
                LOG_ERROR ("Unknown message from NETLINK\n");
		goto out;
        }

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Receive notifications about changes in interfaces and/or IP addresses.
 *
 * @param source
 * @param condition
 * @param data
 *
 * \return
 *
 * Note: This function should execute relatively rarely.
 */
gboolean network_netlink_pending_cb(GIOChannel *source,
			GIOCondition condition, gpointer data)
{
	int nlhlen;
	struct network_data *network_data;
	struct nlmsghdr *nlh;
	struct nlmsgerr *nle;
	void *buffer;

	LOG_FUNC_START(1);

	network_data = data;

	buffer = nlh = g_malloc(NETLINK_BUFFER_SIZE);

	if ((nlhlen = recv(network_data->rtsock, buffer,
					NETLINK_BUFFER_SIZE, 0)) < 0) {
		LOG_PERROR(1);
		goto out_free;
	}

	/*
	 * Log received data
	 */
	LOG_TRACE("Received %u octets, stated len=%u, type=%d",
			nlhlen, nlh->nlmsg_len, nlh->nlmsg_type);

	/*
	 * Process received message...
	 */
	switch(nlh->nlmsg_type) {
	case NLMSG_ERROR:
		nle = NLMSG_DATA(nlh);
		LOG_ERROR("Received error code %u from NETLINK", nle->error);
		break;
	case RTM_NEWLINK:
	case RTM_DELLINK:
		rt_process_linkmsg(network_data, nlh);
		break;
	case RTM_NEWADDR:
	case RTM_DELADDR:
		rt_process_addrmsg(network_data, nlh);
		break;
	default:
		LOG_ERROR ("Received unrecognized message type from NETLINK");
		break;
	}
	

out_free:
	g_free(buffer);

	LOG_FUNC_END(1);

	return TRUE;
}

/**
 * Function that enumerates all running the interfaces in the system.
 *
 * @param data
 *
 * \return 0 if everyting was OK, otherwise -1
 */
int network_enumerate_interfaces(struct network_data *data)
{
	int len, nlhlen, retval, rtsock;
	void *buffer;
	struct nlmsghdr *nlh;
	struct nlmsgerr *nle;
	struct ifinfomsg *ifinfomsg;
	struct rtattr *rta;
	struct netif *ni;

	LOG_FUNC_START(1);

	retval = -1;

	/*
	 * Open NETLINK socket...
	 */
	if ((rtsock = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0) {
		LOG_PERROR(1);
		goto out;
	}

	/*
	 * Obtain memory for request and response messages...
	 */
	buffer = nlh = g_malloc0(NETLINK_BUFFER_SIZE);

	/*
	 * Create request message to dump all active interfaces...
	 */
	nlh->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifinfomsg));
	nlh->nlmsg_type = RTM_GETLINK;
	nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	nlh->nlmsg_seq = 1;
	nlh->nlmsg_pid = getpid();

	if (send(rtsock, nlh, NLMSG_LENGTH(sizeof(struct ifinfomsg)),
								0) < 0) {
		LOG_PERROR(1);
		goto out_free;
	}

	if ((nlhlen = recv(rtsock, nlh, NETLINK_BUFFER_SIZE, 0)) < 0) {
                LOG_PERROR(1);
		goto out_free;
        }

	LOG_TRACE("Received %u octets, stated len=%u, type=%d",
			nlhlen, nlh->nlmsg_len, nlh->nlmsg_type);

	/*
	 * Check if this is an error notification.
	 */
	if (nlh->nlmsg_type == NLMSG_ERROR) {
		nle = NLMSG_DATA(nlh);
		LOG_ERROR("Received error code %u from NETLINK\n", nle->error);
		goto out_free;
        }

	/*
	 * Parse received messages
	 */
	while (nlh->nlmsg_flags & NLM_F_MULTI && nlh->nlmsg_type != NLMSG_DONE) {

		len = nlh->nlmsg_len - NLMSG_LENGTH(0);

		ifinfomsg = NLMSG_DATA(nlh);
		nlh = NLMSG_NEXT(nlh, nlhlen);

		/*
		 * Skip loopback interfaces...
		 */
		if (ifinfomsg->ifi_flags & IFF_LOOPBACK) {
			LOG_DEBUG("Skipping loopback interface with id %u",
					ifinfomsg->ifi_index);
			continue;
		}
		/*
		 * Skip interfaces that have not been brought up
		 */
		if (!(ifinfomsg->ifi_flags & IFF_UP)) {
			LOG_DEBUG("Skipping interface with id %u because it is down",
					  ifinfomsg->ifi_index);
			continue;
		}

		/*
		 *
		 * FIXME: We have to check if we are allowed to bind to
		 * this interface...
		 *
		 * If we were given interface names on which to listen
		 * then also skip further processing.
		 */
		if ((ni = netif_new()) == NULL) {
			LOG_ERROR("Out of memory");
			netif_list_free(data->interfaces);
			goto out_free;
		}

		ni->if_index = ifinfomsg->ifi_index;
		ni->if_flags = ifinfomsg->ifi_flags;

		rta = (struct rtattr *)(ifinfomsg + 1);
		while (RTA_OK(rta, len)) {
			if (rta->rta_type == IFLA_IFNAME) {
				ni->name = g_strdup(RTA_DATA(rta));
				LOG_NOTICE("Adding interface %s", ni->name);
				break;
			}
			rta = RTA_NEXT(rta, len);
		}

		data->interfaces = g_slist_append(data->interfaces, ni);
	}

	/*
	 * Function executed properly...
	 */
	retval = 0;

out_free:
	g_free(buffer);

	close(rtsock);

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Function that enumerates all addresses but only on running interfaces
 * in the system.
 *
 * @param data
 * @param af_family
 *
 * \return 0 if everyting was OK, otherwise -1
 *
 * Note: This function has to be called _after_ network_enumerate_interfaces!
 */
int network_enumerate_addresses(struct network_data *data, int af_family)
{
	int len, nlhlen, retval, rtsock;
	void *buffer;
	struct nlmsghdr *nlh;
	struct nlmsgerr *nle;
	struct ifaddrmsg *ifaddrmsg;
	struct rtattr *rta;
	struct netif *ni;
	struct network_address *na;
	char addr[INET6_ADDRSTRLEN];

	LOG_FUNC_START(1);

	retval = -1;

	/*
	 * Open NETLINK socket...
	 */
	if ((rtsock = socket(AF_NETLINK, SOCK_DGRAM, NETLINK_ROUTE)) < 0) {
		LOG_PERROR(1);
		goto out;
	}

	/*
	 * Obtain memory for request and response messages...
	 */
	buffer = nlh = g_malloc0(NETLINK_BUFFER_SIZE);

	/*
	 * Create request message to dump all active addresses...
	 */
	nlh->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	nlh->nlmsg_type = RTM_GETADDR;
	nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	nlh->nlmsg_seq = 1;
	nlh->nlmsg_pid = getpid();

	ifaddrmsg = NLMSG_DATA(nlh);
	ifaddrmsg->ifa_family = af_family;

	if (send(rtsock, nlh, NLMSG_LENGTH(sizeof(struct ifaddrmsg)),
								0) < 0) {
		LOG_PERROR(1);
		goto out_free;
	}

	if ((nlhlen = recv(rtsock, nlh, NETLINK_BUFFER_SIZE, 0)) < 0) {
                LOG_PERROR(1);
		goto out_free;
        }

	LOG_TRACE("Received %u octets, stated len=%u, type=%d",
			nlhlen, nlh->nlmsg_len, nlh->nlmsg_type);

	/*
	 * Check if this is an error notification.
	 */
	if (nlh->nlmsg_type == NLMSG_ERROR) {
		nle = NLMSG_DATA(nlh);
		LOG_ERROR("Received error code %u from NETLINK", nle->error);
		goto out_free;
        }

	/*
	 * Parse received messages
	 */
	while (nlh->nlmsg_flags & NLM_F_MULTI && nlh->nlmsg_type != NLMSG_DONE) {

		len = nlh->nlmsg_len - NLMSG_LENGTH(0);

		ifaddrmsg = NLMSG_DATA(nlh);
		nlh = NLMSG_NEXT(nlh, nlhlen);

		/*
		 * First search if a given interface is active...
		 */
		if ((ni = netif_find_by_id(data->interfaces,
				ifaddrmsg->ifa_index)) == NULL) {
			LOG_DEBUG("Skipping interface with id %u",
				ifaddrmsg->ifa_index);
			continue;
		}

#warning "If uncomented, the following code is not correctly compile! na has value 0x1!"
#warning "Compiler is gcc-4.1.2-27.fc7"
//		if (!na) {
			if ((na = network_address_new()) == NULL)
				goto out_free;
//		}

		/*
		 * Search for address and add it to a list. Note: we
		 * assume that the same address is not in the list beacuse
		 * we are in scanning phase.
		 */
		rta = (struct rtattr *)(ifaddrmsg + 1);
		while (RTA_OK(rta, len)) {

			/*
			 * It's interesting to note that interface ID can
			 * be obtained by IFA_CACHEINFO. In that case
			 * prefix part will be filled with all ones.
			 *
			 * Also, note that in the case of point-to-point
			 * links IFA_ADDRESS is the address of the peer,
			 * and not our address!
			 */
			if (rta->rta_type == IFA_LOCAL ||
					(rta->rta_type == IFA_ADDRESS &&
					!(ni->if_flags & IFF_POINTOPOINT))) {
				switch(af_family) {
				case AF_INET:
					na->netaddr = netaddr_new_from_inaddr(
							RTA_DATA(rta));
					if (!na->netaddr) {
						network_address_free(na);
						na = NULL;
					}
					netaddr_set_prefix(na->netaddr,
						ifaddrmsg->ifa_prefixlen);
					break;
				case AF_INET6:
					na->netaddr = netaddr_new_from_in6addr(
							RTA_DATA(rta));
					if (!na->netaddr) {
						network_address_free(na);
						na = NULL;
					}
					netaddr_set_prefix(na->netaddr,
						ifaddrmsg->ifa_prefixlen);
					break;
				default:
					LOG_ERROR("Internal error!\n");
					network_address_free(na);
					na = NULL;
					break;
				}
				break;
			}
			rta = RTA_NEXT(rta, len);
		}

		if (na && na->netaddr) {
			/*
			 * This is inefficient and insecure!
			 */
			LOG_NOTICE("Adding IP address %s on interface %s",
					netaddr_net2str(na->netaddr, addr,
						INET6_ADDRSTRLEN),
					ni->name);
			na->interface = ni;
			data->addresses = g_slist_append(data->addresses, na);

			na = NULL;
		}
	}

	retval = 0;

out_free:
	g_free(buffer);

	close(rtsock);

out:
	LOG_FUNC_END(1);

	return retval;
}

/***************************************************************************
 * NETWORK REGISTRATION AND DEREGISTRATION FUNCTIONS
 ***************************************************************************/

/**
 * Register IPv4 or IPv6 socket with queue to receive data
 *
 * @param sa_family	AF_INET or AF_INET6
 * @param type		SOCK_STREAM, SOCK_DGRAM, SOCK_SEQPACK, ...
 * @param proto		IPPROTO_UDP, IPPROTO_TCP, IPPROTO_SCTP, ...
 * @param addr		Address to bind to. If NULL then do not bind.
 * @param flags		Flags that define additional processing needed
 *			by network subsystem.
 * @param notify	Queue on which to send received packets.
 *			If addr is NULL then this parameter is not used.
 *			If NULL then this socket will not receive
 *			packets (i.e. it's used for sending).
 *
 * \return	NULL	An error occured
 *			ID of a listening socket
 *
 * This is a general purpose function.
 *
 * TODO: If we register on currently unexistent IP address then network
 *       module has to watch when this address shows. Also, some way of
 *       notifying upper layers about interface/address add and removing
 *       has to exist.
 *       Also, registration function that is bound to interface should
 *       exist!
 */
network_t *network_socket_register(int sa_family, int type, int proto,
		struct netaddr *addr, int flags, gpointer notify)
{
	guint source_id;
	int flag;
	network_t *ns = NULL;
	char ipaddr[INET6_ADDRSTRLEN];

	LOG_FUNC_START(1);

	LOG_NOTICE("Binding on address %s interface %s port %d",
			netaddr2str(addr, ipaddr, sizeof(ipaddr)),
			netaddr_get_ifname(addr),
			netaddr_get_port(addr));

	if ((ns = network_socket_new()) == NULL)
		goto out;

	ns->flags = flags;

	if ((ns->sockfd = socket(sa_family, type, proto)) < 0) {
		LOG_PERROR(LOG_PRIORITY_ERROR);
		network_socket_free(ns);
		ns = NULL;
		goto out;
	}

	/*
	 * When we do exec, close the socket.
	 *
	 * TODO: Maybe this should be parameter to the function?
	 */
	if (fcntl(ns->sockfd, F_SETFD, FD_CLOEXEC) < 0) {
		LOG_PERROR(LOG_PRIORITY_ERROR);
		network_socket_free(ns);
		ns = NULL;
		goto out;
	}

	/*
	 * If there is a interface in address structure, bind to it!
	 */
	if (netaddr_get_ifname(addr)) {
		LOG_DEBUG("Binding to interface %s", netaddr_get_ifname(addr));

		if (setsockopt(ns->sockfd, SOL_SOCKET, SO_BINDTODEVICE,
				netaddr_get_ifname(addr),
				strlen(netaddr_get_ifname(addr))) < 0) {
			LOG_PERROR(LOG_PRIORITY_ERROR);
			network_socket_free(ns);
			ns = NULL;
			goto out;
		}
	}

	if (ns->flags & NETWORK_F_DSTADDR) {
		/*
		 * Set this option so that kernel sends us also destination
		 * address of received datagram..
		 *
		 * TODO: The options have to be selected according to flags
		 * input parameter!
		 */
		flag = TRUE;

		if (sa_family == AF_INET) {
			if (setsockopt(ns->sockfd, SOL_IP, IP_PKTINFO, &flag,
					sizeof(flag)) < 0) {
				LOG_PERROR(1);
				network_socket_free(ns);
				ns = NULL;
				goto out;
			}
		}

		if (sa_family == AF_INET6) {

			if (setsockopt(ns->sockfd, IPPROTO_IPV6, IPV6_RECVPKTINFO, 
					&flag, sizeof(flag)) < 0) {
				LOG_PERROR(1);
				network_socket_free(ns);
				ns = NULL;
				goto out;
			}
		}
	}

	if (sa_family == AF_INET6 && (flags & NETWORK_F_IPV6ONLY)) {

	        if (setsockopt(ns->sockfd, IPPROTO_IPV6, IPV6_V6ONLY,
                       		&flag, sizeof(flag)) < 0){
			LOG_PERROR(1);
			network_socket_free(ns);
			ns = NULL;
			goto out;
		}
	}

	if (addr) {
		if (bind(ns->sockfd, netaddr_get_sa(addr),
				netaddr_get_sa_size(addr)) < 0) {
			LOG_PERROR(LOG_PRIORITY_ERROR);
			network_socket_free(ns);
			ns = NULL;
			goto out;
		}
	}

	if (flags & NETWORK_F_NOTIFY_QUEUE) {
		g_async_queue_ref(notify);
		ns->notify.queue = notify;
	} else
		ns->notify.func = notify;

	ns->sockfd_io_channel = g_io_channel_unix_new(ns->sockfd);
	g_io_channel_set_encoding(ns->sockfd_io_channel, NULL, NULL);

	/*
	 * TODO: Check if we have to memorize source_id for
	 * later?
	 */
	source_id = g_io_add_watch (ns->sockfd_io_channel,
				G_IO_IN|G_IO_PRI,
				(GIOFunc)network_data_pending_cb,
				ns);
	ns->network_cb = g_main_context_find_source_by_id(
				network_data->context, source_id);

	LOG_DEBUG("Created network socket with address %p", ns);

	LOG_FUNC_END(1);

out:
	return ns;
}

/**
 * Register socket and a queue that should receive network data
 *
 * For meaning of parameters see network_socket_register function
 */
network_t *network_socket_register_queue(int sa_family, int type, int proto,
		struct netaddr *addr, int flags, GAsyncQueue *queue)
{
	return network_socket_register(sa_family, type, proto, addr,
				flags | NETWORK_F_NOTIFY_QUEUE, queue);
}

/**
 * Register socket and a thread that should process received network data
 *
 * @param sa_family
 * @param type
 * @param proto
 * @param addr
 * @param flags
 * @param func
 *
 */
network_t *network_socket_register_thread(int sa_family, int type, int proto,
		struct netaddr *addr, int flags, GThreadFunc func)
{
	return network_socket_register(sa_family, type, proto, addr,
				flags, func);
}

/**
 * Unregister and close socket
 *
 * @param ns
 */
void network_socket_unregister(network_t *ns)
{
	network_socket_free(ns);
}

/***************************************************************************
 * NETWORK SUBSYTEM INITIALIZATION AND DEINITIALIZATION FUNCTIONS
 ***************************************************************************/

void network_schedule_shutdown(void)
{
	LOG_FUNC_START(1);

	LOG_FUNC_END(1);
}

/**
 * Unload network subsystem.
 *
 * Be very careful that unloading is done after all the sockets were
 * unregistered. Maybe we should introduce some kind of reference counting...
 */
void network_unload(void)
{
	LOG_FUNC_START(1);

	g_free(network_data);
	network_data = NULL;

	LOG_FUNC_END(1);
}

/**
 * Initialize networking subsystem
 *
 * @param context	Context or NULL if we should use default context
 *
 * \return	-1	If an error occured
 *		0	Registration successfull
 */
int network_init(GMainContext *context)
{
	int retval;

	LOG_FUNC_START(1);

	retval = -1;

	network_data = g_malloc0(sizeof(struct network_data));

	network_data->context = context;

	/*
	 * Initialize protection lock for addresses
	 */
	g_static_rw_lock_init(&network_data->addresses_lock);

	if (network_enumerate_interfaces(network_data) < 0)
		goto out;

	/*
	 * Search for IPv4 addresses
	 */
	if (network_enumerate_addresses(network_data, AF_INET) < 0)
		goto out;

	/*
	 * Search for IPv6 addresses
	 */
	if (network_enumerate_addresses(network_data, AF_INET6) < 0)
		goto out;

#if 0
	/*
	 * This code has to be moved into message subsystem!
	 */
	struct sockaddr_nl snl;
	guint source_id;

	/*
	 * We are also adding code to monitor changes on interfaces.
	 * Note that monitoring will not start until glib's main loop
	 * starts runing, but hopefully, all events will be queued.
	 */
	if ((network_data->rtsock = socket(PF_NETLINK, SOCK_RAW,
			NETLINK_ROUTE)) < 0) {
		LOG_PERROR(1);
		goto out;
	}

	/*
	 * Bound to multicast groups that receive notifications about
	 * IP address and link changes.
	 */
	memset(&snl, 0, sizeof(struct sockaddr_nl));
	snl.nl_family = AF_NETLINK;
	snl.nl_pid = getpid();
	snl.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR;

	if (bind(network_data->rtsock, (struct sockaddr *)&snl,
				sizeof(struct sockaddr_nl)) < 0) {
		LOG_PERROR(1);
		goto out;
	}

	network_data->netlink_io_channel =
				g_io_channel_unix_new(network_data->rtsock);

	source_id = g_io_add_watch (network_data->netlink_io_channel,
				G_IO_IN|G_IO_PRI,
				(GIOFunc) network_netlink_pending_cb,
				network_data);
	network_data->netlink_cb = g_main_context_find_source_by_id(context,
				source_id);

	/*
	enumerate all interfaces
	enumerate all addresses
	start listening on NETLINK socket for change

	 * We have to find out if were bind to unspecified address or
	 * not.
	if (!config->interfaces && !config->addresses) {
		bind to unspecified address port 500

		if (nat enabled) {
			bind to unspecified address port 4500
		}
	} else {
		for each enabled IP address:
			bind on port 500
			if (nat enabled)
				bind on port 4500
		
	}
	 */

	if (config->per_socket_policy) {

		/*
		 * Also, request from kernel to not apply IPSEC to this sockets.
		 */
		sadb_x_policy.sadb_x_policy_len =
					sizeof(struct sadb_x_policy) / 8;
		sadb_x_policy.sadb_x_policy_exttype = SADB_X_EXT_POLICY;
		sadb_x_policy.sadb_x_policy_type = IPSEC_POLICY_BYPASS;
		sadb_x_policy.sadb_x_policy_dir = IPSEC_DIR_INBOUND;

		if (setsockopt(network_data->sockfd, SOL_IP, IP_IPSEC_POLICY,
					&sadb_x_policy,
					sizeof(struct sadb_x_policy)) < 0) {
			LOG_ERROR("setsockopt: %s", strerror(errno));
			goto out;
		}

		if (config->addresses_natt || config->natt_listen) {
			if (setsockopt(network_data->sockfd_natt, SOL_IP,
					IP_IPSEC_POLICY, &sadb_x_policy,
					sizeof(struct sadb_x_policy)) < 0) {
				LOG_ERROR("setsockopt: %s", strerror(errno));
				goto out;
			}
		}

		sadb_x_policy.sadb_x_policy_dir = IPSEC_DIR_OUTBOUND;

		if (setsockopt(network_data->sockfd, SOL_IP, IP_IPSEC_POLICY,
					&sadb_x_policy,
					sizeof(struct sadb_x_policy)) < 0) {
			LOG_ERROR("setsockopt: %s", strerror(errno));
			goto out;
		}

		if (config->addresses_natt || config->natt_listen) {
			if (setsockopt(network_data->sockfd_natt, SOL_IP,
					IP_IPSEC_POLICY, &sadb_x_policy,
					sizeof(struct sadb_x_policy)) < 0) {
				LOG_ERROR("setsockopt: %s", strerror(errno));
				goto out;
			}
		}
	}

#endif

	retval = 0;

	LOG_FUNC_END(1);

out:
	return retval;
}

/**
  * Compares two interfaces by name and index.
  *
  * @param ni1		First interface to compare
  * @param ni2		Second interface to compare
  *
  * \return 1		If two interfaces differ by name or index or both
  *		0			Otherwise
  */
int network_compare_interfaces(struct netif *ni1, struct netif *ni2) {

	LOG_FUNC_START(1);

	if(strcmp(netif_get_name(ni1), netif_get_name(ni2)))
		return 1;

	LOG_FUNC_END(1);

	return ni1->if_index == ni2->if_index ? 0 : 1;
}

/**
 * Compares interfaces in two network_data structures.
 *
 * @param data1		First network_data data which interfaces are to be compared
 * @param data2		Second network_data data to be compared to data1 interfaces
 *
 * \return	1	If interfaces in data1 and data2 differ
 *		0	otherwise
 */
int network_compare_interface_lists(struct network_data *data1,
									struct network_data *data2) {

	int retval;
	GSList *list1, *list2;

	LOG_FUNC_START(1);

	retval = 0;

	list1 = data1->interfaces;
	list2 = data2->interfaces;

	/*
	 * Iterate through both interface lists one interface at a time
	 */
	while(list1 && list2 && !retval) {

		if(network_compare_interfaces(list1->data, list2->data))
			retval = 1;

		list1 = list1->next;
		list2 = list2->next;
	}

	if(list1 || list2)
		retval = 1;

	LOG_FUNC_END(1);

	return retval;
}


/**
 * Compares addresses in two network_data structures.
 *
 * @param data1		First network_data data which addresses are to be compared
 * @param data2		Second network_data data to be compared to data1 addresses
 *
 * \return	1	If addresses in data1 and data2 differ
 *		0	otherwise
 */
int network_compare_address_lists(struct network_data *data1,
											struct network_data *data2) {

	int retval;
	GSList *list1, *list2;
	struct network_address *a1, *a2;

	LOG_FUNC_START(1);

	retval = 0;

	list1 = data1->addresses;
	list2 = data2->addresses;

	/*
	 * Iterate through both address lists one address at a time
	 */
	while(list1 && list2 && !retval) {

		a1 = (struct network_addresss *)(list1->data);
		a2 = (struct network_addresss *)(list2->data);

		if(netaddr_same_family(a1->netaddr, a2->netaddr)) {
			if(netaddr_cmp_ip2ip(a1->netaddr, a2->netaddr))
				retval = 1;
		}
		else
			retval = 1;


		list1 = list1->next;
		list2 = list2->next;
	}

	if(list1 || list2)
		retval = 1;

	LOG_FUNC_END(1);

	return retval;
}

/**
  * Checks for changes in network interfaces and addresses.
  */
void network_check_links() {

	struct network_data *network_data_new;
	gboolean queue_message;

	LOG_FUNC_START(1);

	network_data_new = g_malloc0(sizeof(struct network_data));
	queue_message = FALSE;

	/*
	 * TODO: what does context represent?
	 */
	network_data_new->context = NULL;

	/*
	 * Lock interfaces and addresses in network_data for reading
	 */
	g_static_rw_lock_reader_lock(&network_data->interfaces_lock);
	g_static_rw_lock_reader_lock(&network_data->addresses_lock);

	if (network_enumerate_interfaces(network_data_new) < 0)
		goto out;

	/*
	 * Search for IPv4 addresses
	 */
	if (network_enumerate_addresses(network_data_new, AF_INET) < 0)
		goto out;

	/*
	* Search for IPv6 addresses
	*/
	if (network_enumerate_addresses(network_data_new, AF_INET6) < 0)
		goto out;

	/*
	* Compare interfaces and addresses
	*/
	if(network_compare_interface_lists(network_data_new, network_data))
		queue_message = TRUE;
	else if(network_compare_address_lists(network_data_new, network_data))
		queue_message = TRUE;

	/*
	 * Send message to some queue if interfaces or addresses have changed
	 */
	if(queue_message == TRUE) {
		/*
		 * Tu treba poslati neku poruku (treba ju oblikovati) u neki red.
		 * Netko po primitku te poruke treba izbrisati postojeći automat
		 * stanja parova IP adresa
		 */
	}

out:
	g_static_rw_lock_reader_unlock(&network_data->interfaces_lock);
	g_static_rw_lock_reader_unlock(&network_data->addresses_lock);

	LOG_FUNC_END(1);
}
