/**
 * The code in this file demonstrates how to track IP address changes
 * all the interfaces in the system.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE	10240
#define MAX_ADDR_LEN	64

void rt_dump_addrmsg(struct nlmsghdr *);

int main(int argc, char **argv)
{
	int sock_fd, nlhlen;
	char buffer[BUFFER_SIZE];
	struct sockaddr_nl snl;
	struct nlmsghdr *nlh;
	struct nlmsgerr *nle;
	struct ifaddrmsg *ifaddrmsg;

	/*
	 * Open a socket to a RTNETLINK...
	 */
	if ((sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		perror("socket");
		exit (1);
	}

	/*
	 * Bound to multicast groups that receive notifications about
	 * IP address and link changes.
	 */
	memset(&snl, 0, sizeof(struct sockaddr_nl));
	snl.nl_family = AF_NETLINK;
	snl.nl_pid = getpid();
	snl.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR;

	if (bind(sock_fd, (struct sockaddr *)&snl,
				sizeof(struct sockaddr_nl)) < 0) {
		perror("bind");
		exit(1);
	}

	nlh = (struct nlmsghdr *)buffer;

	while (1) {

		nlhlen = recv(sock_fd, buffer, BUFFER_SIZE, 0);
		if (nlhlen < 0) {
			perror ("recv");
			continue;
		}

		/*
		 * If requested, send acknowledge to kernel.
		 */
		printf ("Received %u octets, stated len=%u, type=%d\n",
				nlhlen, nlh->nlmsg_len, nlh->nlmsg_type);

		/*
		 * Check what we have received...
		 */
		switch(nlh->nlmsg_type) {
		case NLMSG_ERROR:
			nle = NLMSG_DATA(nlh);
			printf("Received error code %u\n", nle->error);
			continue;
		case RTM_NEWLINK:
		case RTM_DELLINK:
			rt_dump_linkmsg(nlh);
			break;
		case RTM_NEWADDR:
		case RTM_DELADDR:
			rt_dump_addrmsg(nlh);
			break;
		default:
			printf ("Ignoring received message!\n");
			break;
		}
	}

	close (sock_fd);

	return 0;

}
