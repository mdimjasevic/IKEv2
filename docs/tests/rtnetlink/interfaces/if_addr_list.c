/**
 * The code in this file demonstrates how to list all the interfaces
 * in the system as well as how to obtain their addresses.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <arpa/inet.h>
#include <unistd.h>

#define BUFFER_SIZE	10240
#define MAX_ADDR_LEN	64

void rt_dump_addrmsg(struct nlmsghdr *);

int main(int argc, char **argv)
{
	int sock_fd, nlhlen;
	char buffer[BUFFER_SIZE];
	struct nlmsghdr *nlh;
	struct nlmsgerr *nle;
	struct ifaddrmsg *ifaddrmsg;

	/*
	 * Open a socket to a RTNETLINK...
	 */
	if ((sock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		perror("socket");
		exit (1);
	}

	nlh = (struct nlmsghdr *)buffer;

	memset(nlh, 0, BUFFER_SIZE);
	nlh->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg));
	nlh->nlmsg_type = RTM_GETADDR;
	nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ROOT;
	nlh->nlmsg_seq = 1;
	nlh->nlmsg_pid = getpid();

	ifaddrmsg = NLMSG_DATA(nlh);
//	ifaddrmsg->ifa_family = AF_INET6;

	if (send(sock_fd, buffer, NLMSG_LENGTH(sizeof(struct ifaddrmsg)), 0) < 0) {
		perror("sendto");
		return 1;
	}

	nlhlen = recv(sock_fd, buffer, BUFFER_SIZE, 0);
	if (nlhlen < 0) {
		perror ("recv");
		return 1;
	}

	/*
	 * If requested, send acknowledge to kernel.
	 */
	printf ("Received %u octets, stated len=%u, type=%d\n",
			nlhlen, nlh->nlmsg_len, nlh->nlmsg_type);

	/*
	 * Check if this is an error notification.
	 */
	if (nlh->nlmsg_type == NLMSG_ERROR) {
		nle = NLMSG_DATA(nlh);
		printf("Received error code %u\n", nle->error);
		return 1;
	}

	/*
	 * Parse received messages
	 */
	while (nlh->nlmsg_flags & NLM_F_MULTI && nlh->nlmsg_type != NLMSG_DONE) {
		rt_dump_addrmsg(nlh);
		nlh = NLMSG_NEXT(nlh, nlhlen);
	}

	close (sock_fd);

	return 0;
}
