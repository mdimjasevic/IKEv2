/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __TRANSFORMS_H
#define __TRANSFORMS_H

typedef struct {
	/* Transform type, e.g. encryption, authentication, .... */
	guint8		type;

	/* Algorithm ID */
	guint16		id;

/*
 * Transform is implemented and supported internally by ikev2
 */
#define	TRANSFORM_IKE		(1 << 0)

/*
 * Transform is implemented and supported by the kernel
 */
#define	TRANSFORM_KERNEL	(1 << 1)

#define	TRANSFORM_PRF		(1 << 2)
#define	TRANSFORM_INTEGRITY	(1 << 3)

	/* Flags that specify who supports this algorithm */
	guint8		flags;

	/* Key len used for algorithm */
	guint16		key_len;

	/* Minimum key length */
	guint16		min_key_len;

	/* Maximum key length */
	guint16		max_key_len;

	/* Initialization lenght by algorithm */
	guint8		ivlen;

	/* (Digest) Output size of algorithm */
	guint16		olen;

	/*
	 * Name of transform in configuration file and for debugging purposes
 	 */
	char		*name;
	char		*conf_name;

	/*
	 * Functions to be called for this particular transform...
	 */
	union {
		void (*hmac)(guchar *key, gint klen, guchar *text, gint tlen, caddr_t digest);
		void (*encr)(gpointer data, guint32 dlen, gpointer iv, gpointer key, gint enc);
	} f;
} transform_t;

/*
 Crypto:
	xcrypt_3des(char *data, guint32 dlen, DES_cblock *iv, char *key, int enc)
	xcrypt_des(char *data, guint32 dlen, DES_cblock *iv, char *key, int enc)
	xcrypt_aes_cbc(char *data, guint32 dlen, unsigned char *iv, unsigned char *key, int enc, guint32 keylen)
	void xcrypt_aes_ctr128(char *data, guint32 dlen, unsigned char *iv, unsigned char *key, int enc, guint32 keylen)

 PRF (HMAC-H)
	void prf_hmac_md5(unsigned char *key, int key_len, unsigned char *text, int text_len, caddr_t digest)
	void prf_hmac_sha1(unsigned char *key, int key_len, unsigned char *text, int text_len, caddr_t digest)

 Integrity (HMAC-H-96)
	void prf_hmac_md5(unsigned char *key, int key_len, unsigned char *text, int text_len, caddr_t digest)
	void prf_hmac_sha1(unsigned char *key, int key_len, unsigned char *text, int text_len, caddr_t digest)
 */

/*
 * CONSTRUCTORS
 */

/*
 * Create new transform with transform type and algorithm id
 */
transform_t *transform_new(guint8, guint16);

/*
 * Create new encription transform with algorithm id
 */
transform_t *transform_encr_new(guint16);

/*
 * Create new pseudo-random function transform with algorithm id
 */
transform_t *transform_prf_new(guint16);

/*
 * Create new authentication transform with algorithm id
 */
transform_t *transform_auth_new(guint16);

/*
 * Create new Diffie-Hellman Group transform. 'id' is group index
 */
transform_t *transform_dh_new(guint16);

/*
 * Create new Extended sequence numbers transform. 'id' is flag:
 * 0 means "dont use it", 1 means "use it". Other values are reserved.
 */
transform_t *transform_esn_new(guint16);

/*
 * Create new transform as an exact copy of given transform. Be warned that
 * this method will not reallocate 'name' and 'conf_name' fields. If those
 * fields are not NULL, only pointers will be copied, effectivly sharing
 * allocated memory between original and new transforms.
 */
transform_t *transform_dup(transform_t *);

transform_t *transform_find_by_name(guint8, guint8, const char *);

gboolean transform_find(transform_t *, GSList *);

/*
 * DESTRUCTORS
 */

/*
 * Destructor. Only memory reserved for transform will be freed. If memory
 * for 'name' and 'conf_name' was manually allocated, then this memory must
 * also be freed manually, or a memory leak will occur.
 */
void transform_free(transform_t *);
void transform_list_free(GSList *);

/*
 * Getter/setter methods
 */
guint8 transform_get_type(transform_t *);
void transform_set_type(transform_t *, guint8);

guint16 transform_get_id(transform_t *);
void transform_set_id(transform_t *, guint16);

guint16 transform_get_key_len(transform_t *);
void transform_set_key_len(transform_t *, guint16);

guint16 transform_get_min_key_len(transform_t *);
void transform_set_min_key_len(transform_t *, guint16);

guint16 transform_get_max_key_len(transform_t *);
void transform_set_max_key_len(transform_t *, guint16);

guint16 transform_prf_get_key_len(transform_t *);
guint16 transform_prf_get_digest_len(transform_t *);

/*
 * Other methods
 */
gboolean transform_equal(transform_t *, transform_t *);
void transform_list_all(void);

/*
 * Debug function prototypes
 */
void transform_dump(char *, transform_t *);
void transform_list_dump(char *, GSList *);

#endif /* __TRANSFORMS_H */

