/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __SM_C

#define LOGGERNAME	"sm"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#include <signal.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#include <sys/time.h>

#include <openssl/bn.h>
#include <openssl/dh.h>
#include <openssl/rand.h>
#include <openssl/sha.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "timeout.h"
#include "auth.h"
#include "cfg.h"
#include "cfg_module.h"
#include "cfg_client.h"
#include "transforms.h"
#include "proposals.h"
#include "sad.h"
#include "spd.h"
#include "ts.h"
#include "pfkey_msg.h"
#include "pfkey_linux.h"
#include "cert.h"
#include "cert_msg.h"
#include "config.h"
#include "network.h"
#include "aes_xcbc.h"
#include "crypto.h"
#include "payload.h"
#include "supplicant.h"
#include "timeout.h"
#include "session.h"
#include "message_msg.h"
#include "message.h"
#include "sig.h"
#include "ikev2.h"
#include "aaa_wrapper.h"
#include "sm.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*
 * There is one interesting performance question related to the way requests
 * are processed in this state machine: Should we allow parallel processing
 * of more than one request per peer? In other words, are there:
 *
 * 1) so many outstanding requests coming from one peer, and
 * 2) some of those requests take substantial amount of time to be
 *    processed (note, not completely processed, but, until state machines
 *    postpone processing)
 *
 * If we don't allow multiple parallel processing on the same peer then
 * no per-childsa lock is necessary (it can be removed!)
 *
 * The other question that is not touched in this code is QoS! Framework
 * doesn't provide this functionality!
 */

/*
 * Data global to state machines
 */
struct ikev2_data *ikev2_data;

/*******************************************************************************
 * connect_msg functions
 ******************************************************************************/

struct connect_msg *connect_msg_new()
{
	return g_malloc0(sizeof(struct connect_msg));
}

void connect_msg_free(struct connect_msg *cm)
{
	if (cm) {
		id_free(cm->id);

		g_free(cm);
	}
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SM_MSG STRUCTURE
 ******************************************************************************/

void sm_msg_free(union sm_msg *msg)
{
	if (!msg)
		return;

	switch (msg->msg_type) {
	case PFKEY_MSG:
		pfkey_msg_free(&msg->pfkey_msg);
		break;
	case MESSAGE_MSG:
		message_msg_free(&msg->message_msg);
		break;
	case TIMEOUT_MSG:
		timeout_msg_free(&msg->timeout_msg);
		break;
	case SM_SESSION_MSG:
		sm_session_msg_free(&msg->sm_session_msg);
		break;
#ifdef AAA_CLIENT
	case AAA_MESSAGE:
		aaa_msg_free(&msg->aaa_msg);
		break;
#endif /* AAA_CLIENT */

#ifdef CFG_MODULE
	case CFG_MSG:
		cfg_msg_free(&msg->cfg_msg);
		break;
#endif /* CFG_MODULE */

	case CONNECT_MSG:
		connect_msg_free(&msg->connect_msg);
		break;
	case CERT_MSG:
		cert_msg_free(&msg->cert_msg);
		break;

	default:
		LOG_BUG("Unhandled message with code %u", msg->msg_type);
		break;
	}
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SM_SESSION_MSG STRUCTURE
 ******************************************************************************/

struct sm_session_msg *sm_session_msg_new(guint8 message)
{
	struct sm_session_msg *msg;

	msg = g_malloc0(sizeof(struct sm_session_msg));

	msg->msg_type = SM_SESSION_MSG;
	msg->message = message;

	return msg;
}

void sm_session_msg_free(struct sm_session_msg *msg)
{
	if (msg)
		g_free(msg);
}

/*******************************************************************************
 * FUNCTIONS MANAGING TIMEOUT FOR SESSION ON RESPONDER
 ******************************************************************************/

/**
 * Set time the responder will wait for new request from initiator
 *
 * @param session
 *
 * This function sets timeout value and initalizes necessary
 * structures that the reponder will wait for a new request from
 * the initiator.
 */
void sm_session_responder_timeout_set(struct session *session)
{

	if (session->cr->response_timeout) {
		LOG_DEBUG("Registering responder timeout on session");

		session_ref(session);

		session->responder_wait.tv.tv_usec = 0;
		session->responder_wait.tv.tv_sec =
				session->cr->response_timeout / 1000;
		session->responder_wait.retries =
				session->cr->response_retries;
		session->responder_wait.to = timeout_register_thread(
				&session->responder_wait.tv,
				0, session,
				sm_ikesa_responder_wait_timeout);
	}
}

/**
 * Cancel already registered timeout on responder for given session
 *
 * @param session
 *
 * This function cancels timeout for the session
 */
void sm_session_responder_timeout_cancel(struct session *session)
{
	if (session->responder_wait.to) {
		LOG_DEBUG("Canceling responder timeout on session");

		/*
		 * We call session_free simply to decrement reference
		 * counter and we don't check return value as the
		 * assumption is that session has refcnt > 0!
		 */
		session_free(session);
		timeout_cancel(session->responder_wait.to);
		session->responder_wait.to = NULL;
	}
}

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE SET OF SESSIONS
 ******************************************************************************/

/**
 * Obtain writer lock on a set of session
 */
void sm_sessions_writer_lock()
{
	g_static_rw_lock_writer_lock(&(ikev2_data->session_lock));
	LOG_TRACE("Obtained writer lock on a set of sessions");
}

/**
 * Release writer lock on a set of session
 */
void sm_sessions_writer_unlock()
{
	g_static_rw_lock_writer_unlock(&(ikev2_data->session_lock));
	LOG_TRACE("Released writer lock on a set of sessions");
}

/**
 * Obtain reader lock on a set of session
 */
void sm_sessions_reader_lock()
{
	g_static_rw_lock_reader_lock(&(ikev2_data->session_lock));
	LOG_TRACE("Obtained reader lock on a set of sessions");
}

/**
 * Release reader lock on a set of session
 */
void sm_sessions_reader_unlock()
{
	g_static_rw_lock_reader_unlock(&(ikev2_data->session_lock));
	LOG_TRACE("Released reader lock on a set of sessions");
}

/**
 * Add new session into a set of sessions
 *
 * @param session
 *
 * When adding session into the list, reference counter of session
 * is not increased since we assume that after session was
 * created it reference counter is set to 1
 */
void sm_sessions_session_add(struct session *session)
{
	sm_sessions_writer_lock();
	ikev2_data->sessions = g_slist_append(ikev2_data->sessions, session);
	sm_sessions_writer_unlock();
}

/**
 * Add new session into a set of sessions with lock already acquired
 *
 * @param session
 *
 * When adding session into the list, reference counter of session
 * is not increased as we assume that after session was created it's
 * reference counter has been set to 1!
 */
void sm_sessions_session_add_unlocked(struct session *session)
{
	ikev2_data->sessions = g_slist_append(ikev2_data->sessions, session);
}

/**
 * Remove a session from a set of sessions
 */
void sm_sessions_remove(struct session *session)
{
	sm_sessions_writer_lock();
	ikev2_data->sessions = g_slist_remove(ikev2_data->sessions, session);
	sm_sessions_writer_unlock();
}

/**
 * Remove a session from a set of sessions with lock already acquired
 */
void sm_sessions_remove_unlocked(struct session *session)
{
	ikev2_data->sessions = g_slist_remove(ikev2_data->sessions, session);
}

/**
 * Check if the session set is empty
 *
 * \return TRUE if empty, FALSE otherwise
 */
gboolean sm_sessions_is_empty()
{
	return (ikev2_data->sessions == NULL);
}

/**
 * Search sessions by a given remote address
 *
 * @param addr	Address to search for
 *
 * Session is searched by address with a reader lock taken. If
 * session is found, then the lock is taken on session before
 * releasing global reader lock.
 */
struct session *sm_sessions_search_by_daddr(struct netaddr *daddr)
{
	GSList *slist;
	struct session *session;

	LOG_FUNC_START(1);

	sm_sessions_reader_lock();

	for (slist = ikev2_data->sessions; slist; slist = slist->next) {
		session = slist->data;

		if (session_get_state(session) != IKE_SM_MATURE)
			continue;

		/*
		 * If this side is initiator, then search by responder's
		 * address. Otherwise, search by initiator's address.
		 */
		if (session_get_ike_type(session) == IKEV2_INITIATOR) {
			if (session->r_addr &&
					!netaddr_cmp_ip2ip(daddr,
						session->r_addr)) {
				session_lock(session);
				goto out;
			}
		} else {
			if (session->i_addr &&
					!netaddr_cmp_ip2ip(daddr,
						session->i_addr)) {
				session_lock(session);
				goto out;
			}
		}
        }

	session =  NULL;

out:
	sm_sessions_reader_unlock();

	LOG_FUNC_END(1);

	return session;
}

/**
 * Search session with given parameters
 *
 * @param sessions              List of all sessions
 * @param i_spi                 i_spi of received packet
 * @param r_spi                 r_spi of received packet
 * @param addr                  Source of received packet
 * @param ikesa_max
 * @param ikesa_max_halfopened
 *
 * \return      NULL    No session structure found
 *              1       No session structure, but do not create new
 *                      session structure
 *                      Otherwise, pointer to a session structure
 *
 * This function has to be called with (at least with) reader lock
 * held on session list structure to guarantee that session list will
 * not change while it is searching through it...
 *
 * This function should be integrated with the function
 * sm_sessions_search_by_msg but it is left for later.
 */
struct session *_sm_session_search(GSList *sessions, guint64 i_spi,
                guint64 r_spi, struct netaddr *addr,
		gpointer digest, gint digest_size,
                guint32 ikesa_max, guint32 ikesa_max_halfopened)
{
	struct session *session;
	struct session *sel_sess;
	gint opened, half_opened;
	gboolean new_req;

	opened = half_opened = 0;
	new_req = FALSE;
	sel_sess = NULL;

	LOG_TRACE("Searching for session with i_spi=%016llX, r_spi=%016llX",
			i_spi, r_spi);

	while (sessions) {
		session = sessions->data;
		sessions = sessions->next;

		/*
		 * Check that we have session belonging to a given
		 * peer...
		 */
		if ((session->i_addr &&
				!netaddr_cmp_ip2ip(addr, session->i_addr)) ||
				(session->r_addr &&
				!netaddr_cmp_ip2ip(addr, session->r_addr))) {

			LOG_TRACE("i_spi=%016llX r_spi=%016llX session_type=%d "
					"session_state=%d", session->i_spi,
					session->r_spi,
					session_get_ike_type(session),
					session_get_state(session));

			if (session_get_ike_type(session) == IKEV2_INITIATOR) {
				if (session->r_spi)
					opened++;
				else
					half_opened++;

				/*
				 * Catch response to initiator's request...
				 * N(COOKIE), N(INVALID_KE_PAYLOAD), or regular
				 * response
				 */
				if (i_spi && session->i_spi == i_spi
						&& session->r_spi == 0
						&& session_get_state(session)
							== IKE_SMI_AUTH)
					return session;
			}

			if (session_get_ike_type(session) == IKEV2_RESPONDER) {
				if (session->i_spi && session->r_spi
						&& session_get_state(session)
							!= IKE_SMR_INIT
						&& session_get_state(session)
							!= IKE_SMR_AUTH)
					opened++;
				else
					half_opened++;

				/*
				 * Catch request for a new session with
				 * existing one...
				 */
				if (i_spi && session->i_spi != i_spi
						&& !r_spi && session->r_spi) {
					new_req = TRUE;
					sel_sess = session;
				}

				/*
				 * Check session according to digest message...
				 */
				if (session->digest && digest) {
					if (!memcmp(session->digest, digest,
							digest_size))
						return session;
				}
			}

			/*
			 * Catch regular request or response...
			 */
			if (i_spi && r_spi
					&& session->i_spi == i_spi
					&& session->r_spi == r_spi)
				return session;
		}
	}

	if (sel_sess) {
		if (half_opened <= ikesa_max_halfopened && opened <= ikesa_max
				&& new_req)
			return NULL;
	} else
		return NULL;

	LOG_ERROR("Too many opened and/or half opened sessions"
			" already with a given peer. Terminating.");

	return GUINT_TO_POINTER(1);
}

/**
 * Search sessions by received IKEv2 message
 *
 * @param msg
 *
 * \return
 */
struct session *sm_sessions_search_by_msg(struct message_msg *msg)
{
	struct session *session;
	gint digest_size = 0;
	gpointer digest = NULL;

	LOG_FUNC_START(1);

	if (msg->exchg_type == IKEV2_EXT_IKE_SA_INIT
			&& !msg->msg_id
			&& !msg->response) {
		message_msg_calc_digest(msg);
		digest = message_msg_get_digest(msg);
		digest_size = message_msg_get_digest_size(msg);
	}

	session = _sm_session_search(ikev2_data->sessions,
			msg->i_spi, msg->r_spi, msg->srcaddr,
			digest, digest_size,
			ikev2_data->config->cg->ikesa_max,
			ikev2_data->config->cg->ikesa_max_halfopened);

	LOG_TRACE("Found session=%p", session);

	return session;
}

/*******************************************************************************
 * FUNCTIONS THAT PROCESS CREATE_CHILD_SA EXCHANGE
 ******************************************************************************/

/**
 * Create new CHILD SA
 *
 * @param session	Peer with whom we are creating CHILD SA
 * @param si		New sad_item that has to be created
 * @param sm_msg	Message that initiated creation of a new CHILD SA
 *
 * \return	-1	An error occured and child_sa has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that initiates process of
 * creating a new CHILD SA (hence, _i suffix).
 */
gint sm_child_sa_create_i(struct session *session, struct sad_item *si,
		union sm_msg *sm_msg)
{
	proposal_t *proposal;
	unsigned char *nonce;
	gint retval;

	LOG_FUNC_START(1);

	retval = -1;

	switch(sad_item_get_state(si)) {
	case SA_STATE_CREATE:

		/*
		 * This state is entered when kernel requests new CHILD SA
		 * via ACQUIRE message.
		 */
		LOG_DEBUG("STATE SA_STATE_CREATE");

		if (sm_msg->msg_type == PFKEY_MSG) {
			/*
			 * Find SPD that triggered ACQUIRE message
			 */
			si->spd = spd_find_by_pid(sm_msg->pfkey_msg.acquire.pid);

			/*
			 * If there is no SPD, terminate. This should
			 * not occur in normal circumstances
			 */
			if (!si->spd) {
				LOG_ERROR("No SPD found that triggered ACQUIRE");
				break;
			}

			spd_item_ref(si->spd);

			LOG_DEBUG("Selected policy with ID %u", si->spd->pid);

			/*
			 * If the given policy doesn't have configuration
			 * structure, signal an error and terminate
			 */
			if (!si->spd->ccsa) {
				LOG_ERROR("Could not find appropriate configuration "
						"section for a policy ID %u",
						sm_msg->pfkey_msg.acquire.pid);
				break;
			}

			sad_item_set_seq(si, sm_msg->pfkey_msg.seq);
			sad_item_set_initiator(si, TRUE);
		}

		LOG_DEBUG("CHILD SA configured encr transforms");
		transform_list_dump(LOGGERNAME,
				sad_item_get_encr_algs(si));

		LOG_DEBUG("CHILD SA configured auth transforms");
		transform_list_dump(LOGGERNAME,
				sad_item_get_auth_algs(si));

		sad_item_set_spi_size(si, sizeof(guint32));

		if (session_get_ike_type(session) == IKEV2_INITIATOR) {
			sad_getspi(si, session_get_r_addr(session),
					session_get_i_addr(session));
		} else {
			sad_getspi(si, session_get_i_addr(session),
					session_get_r_addr(session));
		}

		if (!sad_item_get_spi(si))
			g_assert_not_reached();

		sad_item_set_proposals(si,
			proposal_list_create(
				sad_item_get_encr_algs(si),
				sad_item_get_auth_algs(si),
				sad_item_get_protocol(si),
				sad_item_get_spi_size(si),
				sad_item_get_spi(si)));

		LOG_DEBUG("CHILD SA offered proposals");
		proposal_dump_proposal_list(LOGGERNAME,
				sad_item_get_proposals(si));

		LOG_DEBUG("Starting CREATE_CHILD_SA exchange");

		if (session_get_ike_type(session) == IKEV2_INITIATOR) {
			sad_item_set_rekey_dstaddr(si,
				netaddr_dup_ro(session_get_r_addr(session)));
		} else {
			sad_item_set_rekey_dstaddr(si,
				netaddr_dup_ro(session_get_i_addr(session)));
		}

		/*
		 * Generate NONCE value
		 */
		nonce = g_malloc(session->cr->nonce_size);

		if (!rand_bytes(nonce, session->cr->nonce_size)) {
			LOG_ERROR("Error generating random value");
			g_free(nonce);
			break;
		}

		LOG_DEBUG("Generated NONCE value");
		log_data(LOGGERNAME, nonce, session->cr->nonce_size);

		si->i_nonce = BN_bin2bn(nonce,
					session->cr->nonce_size,
					NULL);

		g_free(nonce);

		sad_item_set_msg_id(si, session_get_next_msgid(session));

		sad_item_set_state(si, SA_STATE_CREATE_WAIT);

		message_send_child_sa_i(session, si);

		retval = 0;

		break;

	case SA_STATE_CREATE_WAIT:

		/*
		 * Process response on CREATE_CHILD_SA request to create new
		 * CHILD SA
		 */
		LOG_DEBUG("STATE SA_STATE_CREATE_WAIT");

		/*
		 * We could received notify NO_ADDITIONAL_SAS. In that case
		 * reverse all changes and stop processing...
		 */
		if (sm_msg->message_msg.create_child_sa.no_additional_sas) {
			LOG_ERROR("Peer does not allow more CHILD SAs.");
			break;
		}

		/*
		 * We could received notify TS_UNACCEPTABLE. In that case
		 * reverse all changes, log an error and stop processing...
		 */
		if (sm_msg->message_msg.create_child_sa.ts_unacceptable) {
			LOG_ERROR("Peer does not agree on traffic selectors.");
			break;
		}

		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (sm_msg->message_msg.create_child_sa.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			break;
		}

		/*
		 * We could received notify NO_PROPOSAL_CHOSEN. In that case
		 * reverse all changes and stop processing...
		 */
		if (sm_msg->message_msg.create_child_sa.no_proposal_chosen) {
			LOG_ERROR("Peer does not accept proposals.");
			break;
		}

		/*
		 * From now on we must not simply remove CHILD SA since
		 * peer also created SAs. We first have to send delete
		 * notify, and then we should remove CHILD SA!
		 */
		retval = 0;

		/*
		 * Check mode requested by peer (TUNNEL or TRANSPORT)
		 */
		if (sm_msg->message_msg.create_child_sa.transport_mode) {
			if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
				LOG_ERROR("Peer requested TUNNEL mode which "
						"is not supported by local"
						" configuration!");
				sad_item_set_state(si, SA_STATE_DEAD);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_SA_DELETE));
				break;
			}

		} else {

			if (sad_item_get_ipsec_mode(si)
					== IPSEC_MODE_TRANSPORT) {
				LOG_ERROR("Peer requested TRANSPORT mode which "
						"is not supported by local"
						" configuration!");
				sad_item_set_state(si, SA_STATE_DEAD);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_SA_DELETE));
				break;
			}
		}

		/*
		 * Check if peer selected valid proposal, in case it tries
		 * to cheet...
		 */
		proposal = proposal_intersect(
				sm_msg->message_msg.create_child_sa.proposals,
				sad_item_get_proposals(si),
				sad_item_get_protocol(si));

		LOG_DEBUG("LOCAL proposals");
		proposal_dump_proposal_list(LOGGERNAME,
				sad_item_get_proposals(si));
		LOG_DEBUG("PEER's proposals");
		proposal_dump_proposal_list(LOGGERNAME,
				sm_msg->message_msg.create_child_sa.proposals);
		LOG_DEBUG("RESULT proposal");
		proposal_dump_proposal(LOGGERNAME, proposal);

		if (proposal == NULL) {
			/*
			 * Log an error. Peer IKE daemon did not select one of
			 * the offered proposals! Log an error and terminate
			 * further connection.
			 *
			 * TODO: Should we send a notify?
			 */
			LOG_ERROR("The other and selected unsupported"
					" proposal.");
			sad_item_set_state(si, SA_STATE_DEAD);
			session_msg_push(session,
					sm_session_msg_new(MSG_SA_DELETE));
			break;
                }

		sad_item_set_spi_size(si, proposal_get_spi_size(proposal));
		sad_item_set_peer_spi(si, proposal_get_spi(proposal));

		/*
		 * Free all proposals excepts selected one...
		 */
		sad_item_set_proposal(si, proposal);

#warning "Move to sad.c"
		si->encr = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_ENCR,
						sad_item_get_protocol(si));
		si->auth = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_INTEGRITY,
						sad_item_get_protocol(si));

		proposal_dump_proposal_list(LOGGERNAME,
						sad_item_get_proposals(si));

		/*
		 * Replace traffic resulting traffic selectors from
		 * intersection of configuration file, peer's offer,
		 * and kernel's policy.
		 *
		 * Call to function sad_item_set_ts will fail if
		 * given traffic selectors are not a subset of the
		 * existing ones.
		 */
		if (sad_item_set_ts(si, sm_msg->message_msg.create_child_sa.tsr,
				sm_msg->message_msg.create_child_sa.tsi) < 0) {

			LOG_ERROR("Peer sent illegal traffic selectors!");

			LOG_DEBUG("TSi value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));

			LOG_DEBUG("TSr value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));

			LOG_DEBUG("message->tsr");
			ts_list_dump(LOGGERNAME,
				sm_msg->message_msg.create_child_sa.tsr);

			LOG_DEBUG("message->tsi");
			ts_list_dump(LOGGERNAME,
				sm_msg->message_msg.create_child_sa.tsi);

			sad_item_set_state(si, SA_STATE_DEAD);
			session_msg_push(session,
					sm_session_msg_new(MSG_SA_DELETE));

			break;
                }

		sm_msg->message_msg.create_child_sa.tsr = NULL;
		sm_msg->message_msg.create_child_sa.tsi = NULL;

		si->r_nonce = sm_msg->message_msg.create_child_sa.peer_nonce;
		sm_msg->message_msg.create_child_sa.peer_nonce = NULL;

		sad_item_compute_keys(si, session->sk_d,
				si->i_nonce, si->r_nonce,
				session->prf_type,
				session_get_ike_type(session) ==
					IKEV2_INITIATOR);

		if (session_get_ike_type(session) == IKEV2_INITIATOR)
			sad_add(si, session_get_i_addr(session),
					session_get_r_addr(session),
					session->cg->kernel_spd !=
						KERNEL_SPD_ROSYNC,
					session->natt);
		else
			sad_add(si, session_get_r_addr(session),
					session_get_i_addr(session),
					session->cg->kernel_spd !=
						KERNEL_SPD_ROSYNC,
					session->natt);

		sad_item_remove_transient(si);

		/*
		 * We just made CHILD SA negotiations so increase number of
		 * allocations done in this IKE SA
		 */
		session->allocations++;

		sad_item_set_state(si, SA_STATE_MATURE);

		LOG_DEBUG("child_sa local ts dump");
		ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
		LOG_DEBUG("child_sa remote ts dump");
		ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));

		retval = 0;
		break;

	default:
		LOG_BUG("Unknown csa state %u", sad_item_get_state(si));
		break;
	}

	/*
	 * Check if this session structure is reauthentification of
	 * an existing session, and if so then continue transfer of a
	 * CHILD SAs...
	 */
	if (sad_item_get_state(si) != SA_STATE_CREATE_WAIT
			&& session->session) {
		session_lock(session->session);
		session_msg_push(session,
				sm_session_msg_new(MSG_SA_TRANSFER));
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Create new CHILD SA
 *
 * @param session
 * @param child_sa
 * @param msg
 *
 * \return	-1	An error occured and child_sa has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that respondes to a request
 * of creating a new CHILD SA (hence, _i suffix).
 *
 * It is expected that child_sa is locked, and session is unlocked.
 */
gint sm_child_sa_create_r(struct session *session, struct sad_item *si,
		struct message_msg *msg)
{
	proposal_t *proposal;
	unsigned char *nonce;
	gint retval;

	LOG_FUNC_START(1);

	LOG_DEBUG("Processing CHILD SA create request (STATE)");

	retval = -1;

	/*
	 * Actually, notify should be zero already, but just in case...
	 */
	msg->notify = 0;

	if (session_get_ike_type(session) == IKEV2_INITIATOR) {

		sad_item_set_rekey_dstaddr(si,
			netaddr_dup_ro(session_get_r_addr(session)));

	} else {

		sad_item_set_rekey_dstaddr(si,
			netaddr_dup_ro(session_get_i_addr(session)));
	}

	/*
	 * Search for exact match with a list of SPDs. The
	 * search is restricted only to a set of SPDs that
	 * are covered by a config_peer structure selected
	 * for this peer.
	 */
	si->spd = spd_item_find_by_ts(msg->create_child_sa.tsr,
				msg->create_child_sa.tsi,
				session->cp->sainfo, &si->tsl, &si->tsr);

	if (si->spd == NULL) {

		LOG_ERROR("Could not find appropriate policy");

		/*
		 * Send response. The function will include
		 * appropriate NOTIFY payload with an error!
		 */
		msg->notify = N_TS_UNACCEPTABLE;
		message_send_child_sa_r(session, si, msg);

		goto out;
	}

	spd_item_ref(si->spd);

	/*
	 * Set SPI value sent by peer...
	 * TODO: We assume that peer sent us a valid proposal
	 * with SPIs...
	 */
	sad_item_set_initiator(si, FALSE);

	/*
	 * Find intersection between proposals sent by peer and
	 * proposals defined in configuration file.
	 */
	proposal = proposal_get(msg->create_child_sa.proposals,
				sad_item_get_encr_algs(si),
				sad_item_get_auth_algs(si));

	if (proposal == NULL) {

		LOG_ERROR("No appropriate proposal for a CHILD SA");

		/*
		 * Send response. The function will include
		 * appropriate NOTIFY payload with an error!
		 */
		msg->notify = N_NO_PROPOSAL_CHOSEN;
		message_send_child_sa_r(session, si, msg);

		goto out;
	}

	sad_item_set_spi_size(si, proposal_get_spi_size(proposal));
	sad_item_set_peer_spi(si, proposal_get_spi(proposal));

	LOG_DEBUG("PEER PROPOSALS");
	proposal_dump_proposal_list(LOGGERNAME,
			msg->create_child_sa.proposals);

	LOG_DEBUG("RESULT PROPOSAL");
	proposal_dump_proposal(LOGGERNAME, proposal);

	sad_item_set_spi_size(si, sizeof(guint32));
	if (session_get_ike_type(session) == IKEV2_INITIATOR) {

		/*
		 * TODO
		 * Note that this is an error and that we should
		 * instead use traffic selectors for creating
		 * SA in kernel...
		 */
		sad_getspi(si, session_get_r_addr(session),
				session_get_i_addr(session));

	} else {

		/*
		 * TODO
		 * Note that this is an error and that we should
		 * instead use traffic selectors for creating
		 * SA in kernel...
		 */
		sad_getspi(si, session_get_i_addr(session),
				session_get_r_addr(session));
	}

	if (!sad_item_get_spi(si))
		g_assert_not_reached();

	msg->create_child_sa.proposals = g_slist_remove(
						msg->create_child_sa.proposals,
						proposal);

	sad_item_set_proposal(si, proposal);

	proposal_list_set_spi(sad_item_get_proposals(si),
				sad_item_get_spi_size(si),
				sad_item_get_spi(si));

	/*
	 * Adjust mode (TUNNEL or TRANSPORT)
	 */
	if (msg->create_child_sa.transport_mode) {
		if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
			LOG_ERROR("Peer requested TUNNEL mode which is not "
					"supported by local configuration!");
			msg->notify = N_INVALID_SYNTAX;
			message_send_child_sa_r(session, si, msg);
			goto out;
		}

		sad_item_set_ipsec_mode(si, IPSEC_MODE_TRANSPORT);

	} else {

		if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TRANSPORT) {
			LOG_ERROR("Peer requested TRANSPORT mode which is not "
					"supported by local configuration!");
			msg->notify = N_INVALID_SYNTAX;
			message_send_child_sa_r(session, si, msg);
			goto out;
		}

		sad_item_set_ipsec_mode(si, IPSEC_MODE_TUNNEL);
	}

	/*
	 * Generate NONCE value
	 */
	nonce = g_malloc(session->cr->nonce_size);

	if (!rand_bytes(nonce, session->cr->nonce_size)) {
		LOG_ERROR("Error generating random value");
		msg->notify = N_INVALID_SYNTAX;
		message_send_child_sa_r(session, si, msg);
		goto out;
	}

	LOG_DEBUG("Generated NONCE value");
	log_data(LOGGERNAME, nonce, session->cr->nonce_size);

	si->r_nonce = BN_bin2bn(nonce,
				session->cr->nonce_size,
				NULL);

	g_free(nonce);

	/*
	 * Take received NONCE value (initiator's)
	 */
	si->i_nonce = msg->create_child_sa.peer_nonce;
	msg->create_child_sa.peer_nonce = NULL;

	message_send_child_sa_r(session, si, msg);

	si->encr = proposal_list_find_transform(sad_item_get_proposals(si),
				IKEV2_TRANSFORM_ENCR,
				sad_item_get_protocol(si));

	si->auth = proposal_list_find_transform(sad_item_get_proposals(si),
				IKEV2_TRANSFORM_INTEGRITY,
				sad_item_get_protocol(si));

	sad_item_compute_keys(si, session->sk_d, si->i_nonce,
			si->r_nonce, session->prf_type,
			session_get_ike_type(session) == IKEV2_INITIATOR);

	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		sad_add(si, session_get_i_addr(session),
				session_get_r_addr(session),
				session->cg->kernel_spd != KERNEL_SPD_ROSYNC,
				session->natt);
	else
		sad_add(si, session_get_r_addr(session),
				session_get_i_addr(session),
				session->cg->kernel_spd != KERNEL_SPD_ROSYNC,
				session->natt);

	sad_item_remove_transient(si);

	/*
	 * We just established CHILD SA so increase number of
	 * allocations done in this IKE SA
	 */
	session->allocations++;

	sad_item_set_state(si, SA_STATE_MATURE);

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Rekey existing CHILD SA
 *
 * @param session	Session structure
 * @param si		CHILD SA to be rekeyed
 * @param sm_msg	Kernel message, or response from peer
 *
 * \return	-1	An error occured and child_sa has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that initiates process of
 * rekeying CHILD SA (hence, _i suffix).
 *
 * It is expected that child_sa is locked, and session is unlocked.
 */
gint sm_child_sa_rekey_i(struct session *session, struct sad_item *si,
		union sm_msg *sm_msg)
{
	proposal_t *proposal;
	unsigned char *nonce;
	gint retval;
	struct create_child_sa *msg;

	LOG_FUNC_START(1);

	retval = -1;

	switch(sad_item_get_state(si)) {
	case SA_STATE_REKEY:

		/*
		 * Start rekeying of CHILD SA
		 */
		LOG_DEBUG("STATE SA_STATE_REKEY");

		si->nsi = sad_item_clone(si);

		sad_item_set_seq(si->nsi, sm_msg->pfkey_msg.seq);

		sad_item_set_spi_size(si->nsi, sizeof(guint32));
		sad_getspi(si->nsi, sm_msg->pfkey_msg.expire.daddr,
				sm_msg->pfkey_msg.expire.saddr);

		if (!sad_item_get_spi(si->nsi))
			g_assert_not_reached();

#warning "This is redundant. We should use BN_bin2bn only when creating crypto material!"
		/*
		 * Generate NONCE value
		 */
		nonce = g_malloc(session->cr->nonce_size);

		if (!rand_bytes(nonce, session->cr->nonce_size)) {
			LOG_ERROR("Error generating random value");
			sad_item_remove_transient(si);
			g_free(nonce);
			break;
		}

		si->nsi->i_nonce = BN_bin2bn(nonce,
					session->cr->nonce_size,
					NULL);

		g_free(nonce);

#warning "Here, we should generate dh values if PFS flag is set"

		if (session_get_ike_type(session) == IKEV2_INITIATOR)
			sad_item_set_rekey_dstaddr(si->nsi,
				netaddr_dup_ro(session_get_r_addr(session)));
		else
			sad_item_set_rekey_dstaddr(si->nsi,
				netaddr_dup_ro(session_get_i_addr(session)));

		proposal_list_set_spi(sad_item_get_proposals(si->nsi),
					sad_item_get_spi_size(si->nsi),
					sad_item_get_spi(si->nsi));

		sad_item_set_msg_id(si, session_get_next_msgid(session));

		sad_item_set_state(si, SA_STATE_REKEY_WAIT);

		message_send_child_sa_i(session, si);

		retval = 0;

		break;

	case SA_STATE_REKEY_WAIT:

		LOG_DEBUG("STATE SA_STATE_REKEY_WAIT");

		msg = &sm_msg->message_msg.create_child_sa;

		/*
		 * We could received notify TS_UNACCEPTABLE. In that case
		 * reverse all changes, log an error and stop processing...
		 */
		if (msg->ts_unacceptable) {
			LOG_ERROR("No acceptable traffic selectors from peer. "
					"Could not rekey CHILD SA.");
			sad_item_remove_transient(si);
			break;
		}

		if (msg->no_additional_sas) {
			LOG_ERROR("Peer doesn't accept more CHILD SA over "
					"existing IKE SA");
			sad_item_remove_transient(si);
			break;
		}

		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (msg->invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			sad_item_remove_transient(si);
			break;
		}

		/*
		 * Check mode requested by peer (TUNNEL or TRANSPORT)
		 */
		if (msg->transport_mode) {

			if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
				LOG_ERROR("Peer requested TUNNEL mode which "
						"is not supported by local"
						" configuration!");
				break;
			}

		} else {

			if (sad_item_get_ipsec_mode(si)
					== IPSEC_MODE_TRANSPORT) {
				LOG_ERROR("Peer requested TRANSPORT mode which "
						"is not supported by local"
						" configuration!");
				break;
			}
		}

		si->nsi->r_nonce = msg->peer_nonce;
		msg->peer_nonce = NULL;

		/*
		 * Check if peer selected valid proposal, in case it tries
		 * to cheet...
		 */
		proposal = proposal_get(msg->proposals,
					sad_item_get_encr_algs(si->nsi),
					sad_item_get_auth_algs(si->nsi));

		LOG_DEBUG("LOCAL proposals");
		proposal_dump_proposal_list(LOGGERNAME,
					sad_item_get_proposals(si->nsi));
		LOG_DEBUG("PEER's proposals");
		proposal_dump_proposal_list(LOGGERNAME, msg->proposals);

		if (proposal == NULL) {
			/*
			 * Log an error. Peer IKE daemon did not select one of
			 * the offered proposals! Log an error and terminate
			 * further connection.
			 *
			 * TODO: Should we send a notify?
			 */
			sad_item_remove_transient(si);
			LOG_ERROR("The other and selected unsupported"
					" proposal.");
			break;
		}

		if (sad_item_get_spi_size(si->nsi), sizeof(guint32));

		msg->proposals = g_slist_remove(msg->proposals, proposal);

		LOG_DEBUG("RESULT proposal");
		proposal_dump_proposal(LOGGERNAME, proposal);

		sad_item_set_initiator(si->nsi, TRUE);

		sad_item_set_spi_size(si->nsi, proposal_get_spi_size(proposal));
		sad_item_set_peer_spi(si->nsi, proposal_get_spi(proposal));

		/*
		 * Free all proposals excepts selected one...
		 */
		sad_item_set_proposal(si->nsi, proposal);

#warning "Move to sad.c into sad_item_compute_keys"
		si->nsi->encr = proposal_list_find_transform(
					sad_item_get_proposals(si->nsi),
					IKEV2_TRANSFORM_ENCR,
					sad_item_get_protocol(si->nsi));
		si->nsi->auth = proposal_list_find_transform(
					sad_item_get_proposals(si->nsi),
					IKEV2_TRANSFORM_INTEGRITY,
					sad_item_get_protocol(si->nsi));

		proposal_dump_proposal_list(LOGGERNAME,
					sad_item_get_proposals(si->nsi));

		sad_item_compute_keys(si->nsi, session->sk_d,
				si->nsi->i_nonce, si->nsi->r_nonce,
				session->prf_type,
				session_get_ike_type(session) ==
					IKEV2_INITIATOR);

		if (session_get_ike_type(session) == IKEV2_INITIATOR)
			sad_add(si->nsi, session_get_i_addr(session),
					session_get_r_addr(session),
					session->cg->kernel_spd !=
						KERNEL_SPD_ROSYNC,
					session->natt);
		else
			sad_add(si->nsi, session_get_r_addr(session),
					session_get_i_addr(session),
					session->cg->kernel_spd !=
						KERNEL_SPD_ROSYNC,
					session->natt);

		/*
		 * Add new sad_item to a list of SAs belonging to a
		 * session.
		 *
		 * We assume that lock is held on session so that no one
		 * can interfere with the addition process.
		 */
		sad_item_set_state(si->nsi, SA_STATE_MATURE);
		session_sad_item_add(session, si->nsi);

		si->nsi = NULL;

		sad_item_set_state(si, SA_STATE_DEAD);
		session_msg_push(session, sm_session_msg_new(MSG_SA_DELETE));

		retval = 0;

		break;

	default:
		LOG_BUG("Unknown csa state %u", sad_item_get_state(si));
		break;
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Rekey existing CHILD SA
 *
 * @param session	Session structure
 * @param child_sa	CHILD SA to be rekeyed
 * @param msg		Message request from peer
 *
 * \return	-1	An error occured and child_sa has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that responds to process of
 * rekeying CHILD SA (hence, _r suffix).
 */
gint sm_child_sa_rekey_r(struct session *session, struct sad_item *si,
		struct message_msg *msg)
{
	proposal_t *proposal;
	unsigned char *nonce;
	gint retval;

	LOG_FUNC_START(1);

	LOG_DEBUG("Processing CHILD SA rekey request (STATE)");

	msg->notify = 0;
	retval = -1;

	LOG_DEBUG("Proposals offered by peer");
	proposal_dump_proposal_list(LOGGERNAME,
			msg->create_child_sa.proposals);

	LOG_DEBUG("Proposed local traffic selector");
	ts_list_dump(LOGGERNAME, msg->create_child_sa.tsi);

	LOG_DEBUG("Proposed remote traffic selector");
	ts_list_dump(LOGGERNAME, msg->create_child_sa.tsr);

	/*
	 * Check that old and new traffic selectors are equal...
	 */
	if (!ts_lists_are_equal(sad_item_get_tsl(si), sad_item_get_tsr(si),
				msg->create_child_sa.tsr,
				msg->create_child_sa.tsi)) {

		LOG_ERROR("Peer sent unexpected traffic selectors. "
				"Terminating rekeying");

		LOG_DEBUG("Existing local traffic selector");
		ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
		LOG_DEBUG("Existing remote traffic selector");
		ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));

		msg->notify = N_TS_UNACCEPTABLE;
		message_send_child_sa_r(session, si, msg);
		goto out;
	}

	/*
	 * Find intersection between proposals sent by peer and
	 * proposals defined in configuration file.
	 */
	proposal = proposal_get(msg->create_child_sa.proposals,
				sad_item_get_encr_algs(si),
				sad_item_get_auth_algs(si));

	if (proposal == NULL) {
		/*
		 * No proposal chosen so send back notify...
		 */
		msg->notify = N_NO_PROPOSAL_CHOSEN;
		message_send_child_sa_r(session, si, msg);
		goto out;
	}

	si->nsi = sad_item_clone(si);

	/*
	 * Adjust mode (TUNNEL or TRANSPORT)
	 */
	if (msg->create_child_sa.transport_mode) {
		if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
			LOG_ERROR("Peer requested TUNNEL mode which "
					"is not supported by local"
					" configuration!");
			msg->notify = N_INVALID_SYNTAX;
			message_send_child_sa_r(session, si, msg);
			goto out;
		}

		sad_item_set_ipsec_mode(si, IPSEC_MODE_TRANSPORT);

	} else {

		if (sad_item_get_ipsec_mode(si)
				== IPSEC_MODE_TRANSPORT) {
			LOG_ERROR("Peer requested TRANSPORT mode which "
					"is not supported by local"
					" configuration!");
			msg->notify = N_INVALID_SYNTAX;
			message_send_child_sa_r(session, si, msg);
			goto out;
		}

		sad_item_set_ipsec_mode(si, IPSEC_MODE_TUNNEL);
	}

	/*
	 * Generate NONCE value
	 */
	nonce = g_malloc(session->cr->nonce_size);

	if (!rand_bytes(nonce, session->cr->nonce_size)) {
		LOG_ERROR("Error generating random value");
		msg->notify = N_INVALID_SYNTAX;
		message_send_child_sa_r(session, si, msg);
		goto out;
	}

	si->nsi->r_nonce = BN_bin2bn(nonce,
				session->cr->nonce_size,
				NULL);

	g_free(nonce);

	sad_item_set_spi_size(si->nsi, proposal_get_spi_size(proposal));
	if (sad_item_get_spi_size(si->nsi) != sizeof(guint32)) {
		LOG_ERROR("Invalid SPI size in received message");
		msg->notify = N_INVALID_SYNTAX;
		message_send_child_sa_r(session, si, msg);
		goto out;
	}

	sad_item_set_peer_spi(si->nsi, proposal_get_spi(proposal));

	LOG_DEBUG("RESULT PROPOSAL");
	proposal_dump_proposal(LOGGERNAME, proposal);

	/*
	 * Set SPI value sent by peer...
	 * TODO: We assume that peer sent us a valid proposal
	 * with SPIs...
	 */
	sad_item_set_initiator(si->nsi, FALSE);

	sad_getspi(si->nsi, session_get_i_addr(session),
			session_get_r_addr(session));

	if (!sad_item_get_spi(si->nsi))
		g_assert_not_reached();

	msg->create_child_sa.proposals = g_slist_remove(
					msg->create_child_sa.proposals,
					proposal);

	sad_item_set_proposal(si->nsi, proposal);

	proposal_list_set_spi(sad_item_get_proposals(si->nsi),
			sad_item_get_spi_size(si->nsi),
			sad_item_get_spi(si->nsi));

	LOG_DEBUG("Chosen proposal for CHILD SA");
	proposal_dump_proposal(LOGGERNAME, proposal);

	si->nsi->i_nonce = msg->create_child_sa.peer_nonce;
	msg->create_child_sa.peer_nonce = NULL;

	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		sad_item_set_rekey_dstaddr(si->nsi,
				netaddr_dup(session_get_r_addr(session)));
	else
		sad_item_set_rekey_dstaddr(si->nsi,
				netaddr_dup(session_get_i_addr(session)));

#warning "Move to sad.c into sad_item_compute_keys"
	si->nsi->encr = proposal_find_transform(proposal,
					IKEV2_TRANSFORM_ENCR,
					sad_item_get_protocol(si->nsi));

	si->nsi->auth = proposal_find_transform(proposal,
					IKEV2_TRANSFORM_INTEGRITY,
					sad_item_get_protocol(si->nsi));

	message_send_child_sa_r(session, si, msg);

	sad_item_compute_keys(si->nsi, session->sk_d,
			si->nsi->i_nonce, si->nsi->r_nonce,
			session->prf_type,
			session_get_ike_type(session) == IKEV2_INITIATOR);

	if (session_get_ike_type(session) == IKEV2_INITIATOR)
		sad_add(si->nsi, session_get_i_addr(session),
				session_get_r_addr(session),
				session->cg->kernel_spd !=
					KERNEL_SPD_ROSYNC,
				session->natt);
	else
		sad_add(si->nsi, session_get_r_addr(session),
				session_get_i_addr(session),
				session->cg->kernel_spd !=
					KERNEL_SPD_ROSYNC,
				session->natt);

	sad_item_remove_transient(si->nsi);

	sad_item_set_state(si->nsi, SA_STATE_MATURE);
	session_sad_item_add(session, si->nsi);

	si->nsi = NULL;

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Delete existing CHILD SA
 *
 * @param session	Session structure
 * @param child_sa	CHILD SA to be deleted
 *
 * This function is called in the IKEv2 daemon when CHILD SA has to be
 * removed. Note that this function is not like the previous one since
 * response will be processed inside sm_process_informational message.
 * Also, because of the same reasons, there is no similar function for
 * responder!
 */
gint sm_child_sa_delete_i(struct session *session, struct sad_item *si)
{
	struct timeval tv;

	/*
	 * These are used for deleting CHILD SAs...
	 */
	GSList *deletes;
	struct payload_delete_data pddata;
	guint32 sad_item_spi;

	LOG_FUNC_START(1);

	deletes = NULL;
	deletes = g_slist_append(deletes, &pddata);
	pddata.protocol = sad_item_get_protocol(si);
	pddata.spi_size = sad_item_get_spi_size(si);
	pddata.n_spi = 1;
	pddata.spis = &sad_item_spi;

	sad_item_spi = sad_item_get_peer_spi(si);

	message_send_delete(session, NULL, deletes);

	deletes = g_slist_remove(deletes, &pddata);

	/*
	 * TODO
	 * We are waiting for response, and if we do not
	 * receive it until message subsytem retries
	 * serveral times, we _should_ timeout and remove.
	 * Note that there _shoud_ be connection between
	 * unsuccessfull retransmission and house-keeping
	 * stuff that has to be done if we do not receive
	 * response....
	 */

	gettimeofday(&tv, NULL);
	sad_item_set_remove_time(si, tv.tv_sec);
	sad_item_set_state(si, SA_STATE_REMOVE);

	LOG_FUNC_END(1);

	return 0;
}

/*******************************************************************************
 * FUNCTIONS THAT PROCESS IKE SA RELATED EXCHANGES
 ******************************************************************************/

/**
 * Rekey IKE SA
 *
 * @param session	Session structure
 * @param msg	
 *
 * \return	-1	An error occured and session has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that initiates process of
 * rekeying IKE SA (hence, _i suffix).
 *
 * This function expects session structure to be locked, but not child_sa
 * structure.
 *
 * @todo Remove parameter child_sa and make this function behave as
 * sm_ike_sa_rekey_r, by using temporary session structure
 */
gint sm_ike_sa_rekey_i(struct session *session, struct message_msg *msg)
{
	struct timeval tv;
	proposal_t *proposal;
	unsigned char *nonce;
	struct netaddr *netaddr;
	gint retval;

	LOG_FUNC_START(1);

	retval = -1;

	switch(session_get_state(session)) {
	case IKE_SM_REKEY:
		/*
		 * Initiate IKE SA rekeying
		 */
		LOG_DEBUG("STATE IKE_SM_REKEY session=%p", session);

		LOG_NOTICE("Using REMOTE section at line %u",
				session->cr->cfg_line);
		/*
		 * Create temporary session structure to hold data necessary
		 * for rekeying
		 */
		session->session = session_temporary_new();
		if (!session->session)
			break;

		/*
		 * TODO: This should be moved somewhere earlier since
		 * window management depends on correct reservation of
		 * this number
		 */
		session->msgid = session_get_next_msgid(session);

		/*
		 * Generate initiator SPI...
		 */
		if (!rand_bytes((unsigned char *)&(session->session->i_spi),
				sizeof(session->session->i_spi))) {
			LOG_ERROR("Error generating random values");
			g_assert(session_free(session->session));
			session->session = NULL;
			break;
		}

		LOG_DEBUG("I_SPI:");
		log_data(LOGGERNAME, (char *)&(session->session->i_spi),
				sizeof(session->i_spi));

		/*
		 * Generate KE material
		 */
		session->session->dh_group = proposal_list_find_transform(
						session_get_proposals(session),
						IKEV2_TRANSFORM_DH_GROUP,
						IKEV2_PROTOCOL_IKE);

		session->session->dh = dh_ke_generate(transform_get_id(
						session->session->dh_group));

		/*
		 * Generate NONCE value
		 */
		nonce = g_malloc(session->cr->nonce_size);

		if (!rand_bytes(nonce, session->cr->nonce_size)) {
			LOG_ERROR("Error generating random value");
			g_assert(session_free(session->session));
			session->session = NULL;
			break;
		}

		session->session->i_nonce = BN_bin2bn(nonce,
							session->cr->nonce_size,
							NULL);

		g_free(nonce);

		message_send_ike_sa_rekey_i(session);

		session_set_state(session, IKE_SM_REKEY_RESPONSE);

		retval = 0;

		break;

	case IKE_SM_REKEY_RESPONSE:
		/*
		 * Process response on IKE SA rekey request
		 */
		LOG_DEBUG("STATE IKE_SM_REKEY_RESPONSE session=%p",
				session);

		/*
		 * In case no additional sas are allowed then instead of
		 * rekeying start reauthentication...
		 */
		if (msg->create_child_sa.no_proposal_chosen) {
			LOG_ERROR("Could not rekey IKE SA");
			break;
		}

		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (msg->create_child_sa.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			break;
		}

		proposal = proposal_intersect(
					session_get_proposals(session),
					msg->create_child_sa.proposals,
					IKEV2_PROTOCOL_IKE);

		if (proposal == NULL) {
			LOG_ERROR("Peer choosed unoffered proposal! "
				"Terminating session!");
			break;
		}

		session_set_selected_proposal(session, proposal);

		session->prf_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_PRF,
						IKEV2_PROTOCOL_IKE);

		session->encr_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_ENCR,
						IKEV2_PROTOCOL_IKE);

		session->integ_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_INTEGRITY,
						IKEV2_PROTOCOL_IKE);

		session->dh_group = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_DH_GROUP,
						IKEV2_PROTOCOL_IKE);

		session_set_i_spi(session, session_get_i_spi(session->session));
		session_set_r_spi(session, proposal->spi);

		/*
		 * Change source and destination addresses if IKE is
		 * changing the role from initiator to responder...
		 */
		if (session_get_ike_type(session) == IKEV2_RESPONDER) {
			netaddr = session_get_r_addr(session);

			session_set_r_addr(session,
					session_get_i_addr(session));
			session_set_i_addr(session, netaddr);
		}

		gettimeofday(&tv, NULL);
		session->rekey_time = tv.tv_sec;
		session->last_seen = tv.tv_sec;
		session->allocations = 0;

		session->dh = session->session->dh;
		session->session->dh = NULL;

		session_set_ike_type(session, IKEV2_INITIATOR);

		session->i_nonce = session->session->i_nonce;
		session->session->i_nonce = NULL;

		session->r_nonce = msg->create_child_sa.peer_nonce;
		msg->create_child_sa.peer_nonce = NULL;

		session->pub_key = msg->create_child_sa.peer_pub_key;
		msg->create_child_sa.peer_pub_key = NULL;

		g_assert(session_free(session->session));
		session->session = NULL;

		/*
		 * Calculate new keys...
		 */
		session_crypto_material(session);

		session_set_state(session, IKE_SM_MATURE);

		retval = 0;

		break;

	default:
		LOG_BUG("Unknown session state %u", session_get_state(session));
		break;
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Process rekey IKE SA request and generate and send response
 *
 * @param session	Session structure
 * @param msg		Received rekey request
 *
 * \return	-1	An error occured and session has to be removed
 *		0	Everything went OK, or error occured but nothing
 *			has to be done
 *
 * This function is called in the IKEv2 daemon that responds to a process
 * of rekeying IKE SA (hence, _r suffix).
 */
gint sm_ike_sa_rekey_r(struct session *session, struct message_msg *msg)
{
	struct timeval tv;
	struct session *nsession;
	proposal_t *proposal;
	unsigned char *nonce;
	struct netaddr *netaddr;
	gint retval;

	LOG_FUNC_START(1);

	LOG_DEBUG("Processing IKE SA rekey request (STATE)");

	retval = -1;

	/*
	 * Create temporary CHILD SA structure until we calculate new
	 * keys!
 	 */
	if ((nsession = session_temporary_new()) == NULL)
		goto out;

	/*
	 * We are rekeying IKE protocol, so use
	 * appropriate proposals
	 */
	proposal = proposal_intersect(
				session_get_proposals(session),
				msg->create_child_sa.proposals,
				IKEV2_PROTOCOL_IKE);

	LOG_DEBUG("CONFIGURATION");
	proposal_dump_proposal_list(LOGGERNAME,
			session_get_proposals(session));
	LOG_DEBUG("RECEIVED");
	proposal_dump_proposal_list(LOGGERNAME,
			msg->create_child_sa.proposals);
	LOG_DEBUG("RESULT");
	proposal_dump_proposal(LOGGERNAME, proposal);

	/*
	 * In case there is no common proposal selected send NOTIFY
	 * and terminate child_sa...
	 */
	if (proposal == NULL) {
		LOG_ERROR("No proposal chosen for IKE SA. "
					"Sending NOTIFY to peer!");

		msg->notify = N_NO_PROPOSAL_CHOSEN;
		message_send_notify(session, msg);
		goto out_free;
	}

	/*
	 * Set SPI value sent by peer...
	 * TODO: We assume that peer sent us a valid proposal with
	 * SPIs...
	 */
	session_set_i_spi(nsession, proposal->spi);

	session_set_selected_proposal(nsession, proposal);

	/*
	 * Generate responder SPI...
	 * We ignore distinction between 0 and 1 return values.
	 * See the man page for meaning of those values...
	 */
	if (!rand_bytes((unsigned char *)&(nsession->r_spi), sizeof(guint64))) {
		LOG_DEBUG("Error generating random values");
		goto out_free;
	}

	/*
	 * Generate KE material
	 *
	 * TODO: If to create CHILD SA it is necessary to include KE payload,
	 * then this code has to be slightly modified to take that into account
	 * (probably via some configuration parameter)
	 */
	nsession->dh_group = proposal_find_transform(
					session_get_selected_proposal(nsession),
					IKEV2_TRANSFORM_DH_GROUP,
					IKEV2_PROTOCOL_IKE);

	nsession->dh = dh_ke_generate(transform_get_id(nsession->dh_group));

	if (session_get_ike_type(session) == IKEV2_INITIATOR) {

		session_set_r_addr(nsession,
				netaddr_dup_ro(session_get_i_addr(session)));
		session_set_i_addr(nsession,
				netaddr_dup_ro(session_get_r_addr(session)));

	} else {

		session_set_r_addr(nsession,
				netaddr_dup_ro(session_get_r_addr(session)));
		session_set_i_addr(nsession,
				netaddr_dup_ro(session_get_i_addr(session)));
	}

	nsession->prf_type = proposal_find_transform(
					proposal,
					IKEV2_TRANSFORM_PRF,
					IKEV2_PROTOCOL_IKE);
	if (nsession->prf_type == NULL) {
		LOG_ERROR("No PRF function");
		goto out_free;
	}

	nsession->encr_type = proposal_find_transform(
					proposal,
					IKEV2_TRANSFORM_ENCR,
					IKEV2_PROTOCOL_IKE);
	if (nsession->encr_type == NULL) {
		LOG_ERROR("No encription function");
		goto out_free;
	}

	nsession->integ_type = proposal_find_transform(
					proposal,
					IKEV2_TRANSFORM_INTEGRITY,
					IKEV2_PROTOCOL_IKE);
	if (nsession->integ_type == NULL) {
		LOG_ERROR("No integrity protection function");
		goto out_free;
	}

	/*
	 * Generate NONCE value
	 */
	nonce = g_malloc(session->cr->nonce_size);

	if (!rand_bytes(nonce, session->cr->nonce_size)) {
		LOG_ERROR("Error generating random value");
		g_free(nonce);
		goto out_free;
	}

	nsession->r_nonce = BN_bin2bn(nonce, session->cr->nonce_size, NULL);

	g_free(nonce);

	/*
	 * Take received NONCE value (initiator's)
	 */
	nsession->i_nonce = msg->create_child_sa.peer_nonce;
	msg->create_child_sa.peer_nonce = NULL;

	/*
	 * Take peer's public key
	 */
	nsession->pub_key = msg->create_child_sa.peer_pub_key;
	msg->create_child_sa.peer_pub_key = NULL;

	/*
	 * Setup addresses...
	 */
	if (session_get_ike_type(session) == IKEV2_INITIATOR) {

		netaddr = session_get_i_addr(session);

		session_set_i_addr(session, session_get_r_addr(session));
		session_set_r_addr(session, netaddr);
	}

	message_send_ike_sa_rekey_r(session, nsession, msg);

	session_set_i_spi(session, session_get_i_spi(nsession));
	session_set_r_spi(session, session_get_r_spi(nsession));

	gettimeofday(&tv, NULL);
	session->rekey_time = tv.tv_sec;
	session->last_seen = tv.tv_sec;
	session->allocations = 0;

	/*
	 * Calculate new keys...
	 */
	session_crypto_material(nsession);

	/*
	 * Transfer new keys and transforms...
	 */

	proposal_free(session_get_selected_proposal(session));
	session_set_selected_proposal(session,
			session_get_selected_proposal(nsession));
	session_set_selected_proposal(nsession, NULL);

	session->prf_type = nsession->prf_type;
	nsession->prf_type = NULL;
	session->encr_type = nsession->encr_type;
	nsession->encr_type = NULL;
	session->integ_type = nsession->integ_type;
	nsession->integ_type = NULL;
	session->dh_group = nsession->dh_group;
	nsession->dh_group = NULL;

	if (session->sk_d)
		g_free(session->sk_d);
	session->sk_d = nsession->sk_d;
	nsession->sk_d = NULL;

	if (session->sk_ai)
		g_free(session->sk_ai);
	session->sk_ai = nsession->sk_ai;
	nsession->sk_ai = NULL;

	if (session->sk_ar)
		g_free(session->sk_ar);
	session->sk_ar = nsession->sk_ar;
	nsession->sk_ar = NULL;

	if (session->sk_ei)
		g_free(session->sk_ei);
	session->sk_ei = nsession->sk_ei;
	nsession->sk_ei = NULL;

	if (session->sk_er)
		g_free(session->sk_er);
	session->sk_er = nsession->sk_er;
	nsession->sk_er = NULL;

	if (session->sk_pi)
		g_free(session->sk_pi);
	session->sk_pi = nsession->sk_pi;
	nsession->sk_pi = NULL;

	if (session->sk_pr)
		g_free(session->sk_pr);
	session->sk_pr = nsession->sk_pr;
	nsession->sk_pr = NULL;

	session_set_ike_type(session, IKEV2_RESPONDER);
	session_set_state(session, IKE_SM_MATURE);

	retval = 0;

out_free:
	g_assert(session_free(nsession));

out:
	LOG_FUNC_END(1);

	return retval;
}

/*******************************************************************************
 * MAIN FUNCTIONS
 ******************************************************************************/

/**
 * Signal handling routine.
 *
 * TODO: Probably this routine should suspend all the other activity until
 * signal is properly handled...
 */
void sm_signal_handler(struct ikev2_data *ikev2_data, struct sig_msg *msg)
{
	GSList *lsession;
	struct session *session;
	struct config *config;
	gboolean found;

	LOG_FUNC_START(1);

	/*
	 * In case we received SIGTERM we should prepare everything to
	 * stop a daemon.
	 *
	 * So, set a flag that daemon is dying. Then, put each idle session
	 * into dying state and triger its evaluation. All the other
	 * sessions will be removed when they transit into MATURE state.
	 * If they don't transit into mature state than timeout mechanism
	 * associated with each peer will kill them.
	 */
	if (msg->signum == SIGTERM || msg->signum == SIGINT) {

		LOG_NOTICE("Shutdown signal received");

		ikev2_data->shutdown = TRUE;
		found = FALSE;

		for (lsession = ikev2_data->sessions; lsession;
						lsession = lsession->next) {
			found = TRUE;
			session = lsession->data;
			session_set_shutdown(session, TRUE);
			if (session_get_state(session) != IKE_SM_DYING) {
				session_lock(session);
				if (session_get_state(session) != IKE_SMI_INIT
					&& session_get_state(session) !=
								IKE_SMI_AUTH) {

					if (session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE))) {
						sm_session_activate(session);
					}

				} else
					sm_session_remove(session);
			}
		}

		if (!found)
			g_async_queue_push (ikev2_data->queue,
					GUINT_TO_POINTER(1));
	}

	/*
	 * In case we received SIGUSR1 dump all the current information
	 * about sessions and child sa's into log file...
	 */
	if (msg->signum == SIGUSR1) {
		for (lsession = ikev2_data->sessions; lsession;
						lsession=lsession->next)
			_session_dump(lsession->data);
	}

	/*
	 * In case of receiving SIGHUP signal, reread config file...
	 */
	if (msg->signum == SIGHUP) {
		LOG_NOTICE("Received SIGHUP, reloading configuration file");
		g_static_rw_lock_writer_lock(&(ikev2_data->config_lock));
		if ((config = config_reread_cfg_file(ikev2_data->config))) {
			config_free(ikev2_data->config);
			ikev2_data->config = config;
		}
		g_static_rw_lock_writer_unlock(&(ikev2_data->config_lock));
	}

	/*
	 * In case we received SIGPIPE signal, exit...
	 */
	if (msg->signum == SIGPIPE) {
		exit(1);
	}

	g_free(msg);

	LOG_FUNC_END(1);
}

/**
 * Function that activates processing of given session.
 *
 * @param session	Session to be activated
 *
 * This function has to be called with lock held on session structure.
 * Furthermore, this lock is not allowed to be released.
 */
void sm_session_activate(struct session *session)
{
	LOG_FUNC_START(1);
	g_thread_pool_push (ikev2_data->thread_pool, session, NULL);
	LOG_FUNC_END(1);
}

/**
 * Immediately remove session structure from a memory
 *
 * @param session
 *
 * This function does "active" removal of session, while session_free
 * does "passive" removal. Passive removal means that only the memory
 * is free'd while active removal includes some additional steps, like
 * removing entries from SAD and SPD.
 */
void sm_session_remove(struct session *session)
{
	GSList *sad;
	struct sad_item *si;
#ifdef CFG_CLIENT
	struct netaddr *vpngw;
	struct netif *vpnif;
#endif /* CFG_CLIENT */
#ifdef CFG_MODULE
	struct netaddr *clntaddr, *vpngwaddr;
	struct netif *clntif;
#endif /* CFG_MODULE */

	LOG_FUNC_START(1);

	/*
	 * Remove session from a session list
	 */
	sm_sessions_remove(session);

#ifdef NATT
	/*
	 * Canceling appropriate timeout used for
	 * sending keepalive packets.
	 */
	if (session->natt_keepalive == TRUE)
		timeout_cancel(session->keepalive_timeout);
#endif /* NATT */

	if (session->responder_wait.to) {
		session->responder_wait.to = NULL;
		timeout_cancel(session->responder_wait.to);
	}

	/*
	 * We have to walk through all CHILD SAs and remove them from
	 * the kernel...
	 */
	for (sad = session->sad; sad; sad = sad->next) {
		si = sad->data;

		if (sad_item_get_state(si) == SA_STATE_MATURE
			|| sad_item_get_state(si) == SA_STATE_DEAD
			|| sad_item_get_state(si) == SA_STATE_REMOVE) {
			if (session_get_ike_type(session) == IKEV2_INITIATOR)
				sad_delete(si, session_get_i_addr(session),
						session_get_r_addr(session));
			else
				sad_delete(si, session_get_r_addr(session),
						session_get_i_addr(session));
		}
	}

	/*
	 * Call message subsystem to remove it's data
	 */
	message_remove_session(session);

#ifdef CFG_CLIENT
	/*
	 * If configuration data has been received from the
	 * responder execute external script to remove data
	 * from the system
	 */
	if (session->icfg) {

		vpngw = netaddr_dup(session_get_r_addr(session));
		netaddr_set_port(vpngw, 0);

		vpnif = netif_find_by_addr(session_get_i_addr(session));

		cfg_client_exec_script(session->cp->down_argv,
				session->icfg, session->cp->lid,
				vpngw, netif_get_name(vpnif));

		cfg_free(session->icfg);
		session->icfg = NULL;
	}
#endif /* CFG_CLIENT */

#ifdef CFG_MODULE
	/*
	 * If the configuration data has been given to the client
	 * call external script to remove all the data from the
	 * system.
	 */
	if (session->rcfg &&
		session->rcfg != GUINT_TO_POINTER(1) &&
		cfg_proxy_get_cfg(session->rcfg)) {

		clntaddr = netaddr_dup(session_get_i_addr(session));
		netaddr_set_port(clntaddr, 0);

		vpngwaddr = netaddr_dup(session_get_i_addr(session));
		netaddr_set_port(vpngwaddr, 0);

		clntif = netif_find_by_net(session_get_r_addr(session));

		/**
		 * We ignore error indication from the scripts beacuse
		 * there's nothing we can do in the case of an error
		 * but to ignore it and release all the data.
		 */
		cfg_module_exec_script(session->cp->gw_down_argv,
				cfg_proxy_get_cfg(session->rcfg),
				clntaddr, netif_get_name(clntif),
				vpngwaddr);

		cfg_proxy_release(session->rcfg);
		cfg_proxy_free(session->rcfg);
		session->rcfg = NULL;

		netaddr_free(clntaddr);
		netaddr_free(vpngwaddr);
	}
#endif /* CFG_MODULE */

#ifdef SUPPLICANT
	if (session->sd)
		supplicant_free(session->sd);
#endif /* SUPPLICANT */

#ifdef AAA_CLIENT
	if (session->aaa_data && GPOINTER_TO_UINT(session->aaa_data) != 1)
		aaa_free(session);
#endif /* AAA_CLIENT */

	if (session->embedded_sm_msg)
		sm_msg_free(session->embedded_sm_msg);

	if (!session_free(session))
		session_unlock(session);

	/*
	 * If shutdown is in a progress, then check that there are no
	 * more sessions and in that case trigger termination of main
	 * loop...
	 */
	if (sm_sessions_is_empty() && ikev2_data->shutdown)
		g_async_queue_push (ikev2_data->queue, GUINT_TO_POINTER(1));

	LOG_FUNC_END(1);
}

/**
 * Function that processes received DELETE notify.
 *
 * @param session
 * @param ikev2_data
 * @param message_msg
 *
 * \return	0 if everything went OK
 *		1 if there was error but nothing has to be done further
 *		-1 session structure has to be removed
 */
gint sm_process_delete(struct session *session,
			struct ikev2_data *ikev2_data,
			struct message_msg *msg)
{
	gint retval = 0;

	struct payload_delete_data *delete;
	guint32 *spi;
	int i;
	struct sad_item *si;

	GSList *deletes = NULL;
	struct payload_delete_data *pddata;
	guint32 *sad_item_spi;

	LOG_FUNC_START(1);

	LOG_DEBUG("Processing received DELETE payload(s)");

	while(msg->informational.deletes) {
		delete = msg->informational.deletes->data;

		if (delete->protocol == IKEV2_PROTOCOL_IKE) {

			if (retval)
				LOG_WARNING("Got two or more DELETE payloads "
						"for single IKE SA."
						" Continuing!");

			retval = -1;

		} else {

			spi = delete->spis;

			pddata = g_malloc0(sizeof(struct payload_delete_data));

			deletes = g_slist_append(deletes, pddata);
			pddata->protocol = delete->protocol;
			pddata->spi_size = delete->spi_size;
			pddata->n_spi = 0;

			sad_item_spi = g_malloc(delete->n_spi *
						delete->spi_size);

			pddata->spis = sad_item_spi;

			for(i = 0; i < delete->n_spi; i++) {
				si = session_sad_item_find_by_protocol_and_spi(
					session, delete->protocol, spi[i]);

				if (si) {
					pddata->n_spi++;
					if (session_get_ike_type(session) ==
							IKEV2_INITIATOR)
						sad_delete(si,
							session_get_i_addr(
								session),
							session_get_r_addr(
								session));
					else
						sad_delete(si,
							session_get_r_addr(
								session),
							session_get_i_addr(
								session));
					session_sad_item_remove(session, si);
					sad_item_spi[i] =
						sad_item_get_peer_spi(si);
					sad_item_free(si);

					continue;
				}

				LOG_ERROR("Peer requested invalid SA to be "
					"deleted (spi=%08x)", spi[i]);
			}
		}

		msg->informational.deletes = g_slist_remove(
					msg->informational.deletes,
					delete);
	}

	while (deletes) {
		pddata = deletes->data;
		g_free(pddata->spis);
		deletes = g_slist_remove(deletes, pddata);
		g_free(pddata);
	}

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Function that processes received CFG payload in received informational
 * request exchange.
 *
 * @param session
 * @param msg
 *
 * \return	0  Message successfuly processed
 *		1  If message should not be removed
 *
 * This function, in case it requests configuration data from provider,
 * might have to wait for a response. In that case, it changes the
 * session state and returns 1 meaning it has to be called again.
 */
gint sm_process_informational_cfg(struct session *session,
			struct message_msg *msg)
{
	gint retval = 0;

#ifdef CFG_CLIENT
	struct netaddr *vpngw;
	struct netif *vpnif;
#endif /* CFG_CLIENT */

	/*
	 * Process CFG_REQUEST message from the Initiator
	 */
	if (session_get_ike_type(session) == IKEV2_RESPONDER) {

#ifdef CFG_MODULE
		/*
		 * We are expecting only requests here, so check
		 * if it's true. If not, ignore response and log
		 * a warning.
		 */
		if (!cfg_is_request(msg->informational.cfg)) {
			LOG_WARNING("Initiator sent response CFG"
					"payload! Ignoring!");
			goto out;
		}

		/*
		 * Check that initiator is allowed to request
		 * configuration data. If not then simply ignore
		 * intiator's request.
		 */
		if (session->cp->providers) {
			LOG_WARNING("Initiator requested configuration "
					"data but there are no "
					"providers defined!");
			goto out;
		}

		/*
		 * Initialize provider if this is the first
		 * time we are requesting configuration data
		 */
		if (!session->rcfg) {
			session->rcfg = cfg_proxy_new(
						session->cp->providers,
						msg->informational.cfg,
						session->peer_id);

			if (!session->rcfg)
				goto out;
		}

		/*
		 * Request configuration data
		 */
		retval = cfg_proxy_request(session->rcfg, session);

		if (retval < 0) {

			LOG_WARNING("No configuration data has been "
					"obtained. Continuing.");

			cfg_proxy_free(session->rcfg);
			session->rcfg = NULL;

		} else if (retval > 0) {

			/*
			 * We have to wait for configuration
			 * data to be obtained.
			 */
			session->preserve_message = TRUE;
			session->embedded_sm_msg = msg;
			retval = 1;

		} else {

			/*
			 * Dump received data
			 */
			 cfg_proxy_dump(session->rcfg);
		}
#else /* CFG_MODULE */
		LOG_WARNING("Initiator sent CFG payload for which no support"
				" is compiled in! Ignoring");
#endif /* CFG_MODULE */
	} else if (session_get_ike_type(session) == IKEV2_INITIATOR) {
#ifdef CFG_CLIENT

		/*
		 * In the following code, when an error occured, we assume
		 * that it's not fatal, so we ignore it and rely on other
		 * mechanisms to remove session if it _is_ a fatal error.
		 *
		 * Note that for now ikev2 doesn't request IP address via
		 * this exchange, only renewals are done this way.
		 */

		/*
		 * Check if this is a request for configuration data.
		 */
		if (cfg_is_request(msg->informational.cfg)) {
			LOG_WARNING("Responder sent response CFG"
					"payload! Ignoring!");
			goto out;
		}

		/*
		 * Did we request configuration data?
		 */
		if (!session->icfg || !session->icfg->flags) {
			LOG_WARNING("Responder sent unrequestd CFG response."
					" Ignoring");
			goto out;
		}

		/*
		 * Free previous CFG structure, set new one, and run external
		 * script to apply parameters.
		 */
		cfg_dump(msg->informational.cfg);

		cfg_free(session->icfg);
		session->icfg = msg->informational.cfg;
		msg->informational.cfg = NULL;

		vpngw = netaddr_dup(session_get_r_addr(session));
		netaddr_set_port(vpngw, 0);

		vpnif = netif_find_by_addr(session_get_i_addr(session));

		if (cfg_client_exec_script(session->cp->up_argv,
				session->icfg, session->cp->lid,
				vpngw, netif_get_name(vpnif)) < 0) {

			cfg_free(session->icfg);
			session->icfg = NULL;

			session_msg_push(session,
					sm_session_msg_new(
						MSG_IKE_TERMINATE));
                }

#else /* CFG_CLIENT */
		LOG_WARNING("Responder sent CFG payload for which no support"
				" is compiled in! Ignoring");

#endif /* CFG_CLIENT */
	} else
		g_assert_not_reached();

#if defined(CFG_CLIENT) || defined(CFG_MODULE)
out:
#endif /* defined(CFG_CLIENT) || defined(CFG_MODULE) */
	return retval;
}

/**
 * Function that processes received INFORMATIONAL request exchange.
 *
 * @param session
 * @param message_msg
 *
 * \return	0  Message successfuly processed and responded
 *		1  If message should not be removed
 *		-1 session structure has to be removed
 */
gint sm_process_informational(struct session *session, struct message_msg *msg)
{
	gint retval = 0;

	LOG_FUNC_START(1);

	/*
	 * Is this response message for IKE SA delete notify? If
	 * so, return immediatelly and terminate session.
	 */
	if (!msg->informational.deletes &&
			msg->response
			&& session_get_state(session) == IKE_SM_DYING) {
		LOG_NOTICE("Received response to IKE SA delete message!");

		/*
		 * Check if we should terminate ikev2?
		 */
		if (session_get_user_initiated(session))
			g_async_queue_push (ikev2_data->queue,
					GUINT_TO_POINTER(1));

		retval = -1;
		goto out;
	}

	/*
	 * Process DELETE payloads if we got them...
	 */
	if (msg->informational.deletes) {
		retval = sm_process_delete(session, ikev2_data, msg);
		if (retval < 0)
			goto out;
	}
		

	/*
	 * Check if we got NOTIFY payloads
	 */

	/* INVALID_SPI */

	/* SET_WINDOW_SIZE */

#if defined(CFG_CLIENT) || defined(CFG_MODULE)
	/*
	 * Check if we got CONFIGURATION payloads
	 */
	if (msg->informational.cfg) {
		retval = sm_process_informational_cfg(session, msg);
	}
#endif /* defined(CFG_CLIENT) || defined(CFG_MODULE) */

out:
	/*
	 * If sm_process_informational_cfg didn't request delay of further
	 * processing (i.e. retval == 1) then finish processing by
	 * sending response to a request.
	 */
	if (!msg->response)
		sm_process_informational_finish(session, msg);

	LOG_FUNC_END(1);

	return retval;
}

/**
 * Finish processing of received INFORMATIONAL request exchange.
 *
 * @param session
 * @param message_msg
 *
 * \return	0  Message successfuly processed and responded
 *		-1 session structure has to be removed
 *
 * This part of funcionality is not included in sm_process_informational
 * since in case of CFG request we have to wait for a response. Thus
 * the part that has to execute after the received response is placed
 * here. Note that if there is no response to wait for, then this
 * function is called directly by sm_process_informational. This way
 * we avoid code duplication.
 */
gint sm_process_informational_finish(struct session *session,
		struct message_msg *msg)
{
	gint retval = 0;

	if (msg->notify == N_INVALID_SYNTAX)
		message_send_notify(session, msg);
	else
		message_send_informational_r(session, msg);

	return retval;
}


/**
 * Initiate CRL download and re-verify used certificates with new CRL
 *
 * @param session	session that reverifies its certs
 * @param sm_msg	container for CERT_MSG (after CRL download)
 *
 * \return	0  Verification with new CRL successful
 *		-1 error, session structure has to be removed
 *
 * Called twice: 1st time after crl_update timeout; initiates download at 
 * that point. 2nd time after download is finished; verifies certificates 
 * using new CRL.
 */
gint sm_ike_crl_update(struct session *session, union sm_msg *sm_msg)
{
	GSList *verified_certs = NULL;
	GSList *used_cas;
	gint retval = -1;
	time_t next_crl_update, now;

	LOG_FUNC_START(1);

	now = time(NULL);
	next_crl_update = session->next_crl_update;

	/*
	 * Reset next_crl_update if already triggered
	 */
	if (next_crl_update < (now + 1))
		next_crl_update = (time_t) -1;

	/*
	 * Have we already initiated the download? 
	 */
	if (sm_msg && sm_msg->msg_type == CERT_MSG) {
		if (!sm_msg->cert_msg.success) {
			LOG_ERROR("Download unsuccessful! Terminating...");
			goto out;
		}
	}

	if (!cert_check_auth_material(NULL, session->cp->ca_list, session, 
			NULL, &next_crl_update)) {
		retval = 1;
		goto out;
	}

	session->next_crl_update = next_crl_update;

	/*
	 * Check each CA that session uses with new CRL
	 * (verify certs belonging to this session)
	 * TODO: optimize and use only CAs which have a new CRL
	 */
	used_cas = session->used_cas;
	while (used_cas) {
		if (!cert_verify_ca(used_cas->data, session)) {
			retval = -1;
			goto out;
		}

		used_cas = used_cas->next;
	}

	session_set_state(session, IKE_SM_MATURE);
	retval = 0;

out:
	LOG_FUNC_END(1);
	return retval;
}

/**
 * Main function of the thread pool.
 *
 * POSSIBLE BUG: There is condition that leads to a bug and premature end of
 * IKEv2 daemon. If we push session twice for processing, and first processing
 * terminates session, then the second one, after getting in turn, will
 * reference unexisting memory area. The bug is actually a fact that if
 * session is on processing in thread pool, then, we should not push it again.
 * Only after processing of the first one is finished, should be the second
 * one pushed into
 *
 * It's easy to reproduce a bug, let just the opposit side send error
 * notification during, e.g. authentication (invalid authentication).
 */
void sm_ike_thread(gpointer _session, gpointer _ikev2_data)
{
	static GStaticPrivate current_thread_number = G_STATIC_PRIVATE_INIT;
	static GStaticMutex current_thread_mutex = G_STATIC_MUTEX_INIT;
	static int thread_counter = 0;
	int *current_number;

	struct timeval tv;

	struct session *session = _session;
	struct session *nsession;

	struct create_child_sa *ccs;

	GSList *deletes, *sad_item_count;
	struct payload_delete_data pddata;
	union sm_msg *sm_msg = NULL;
	struct sad_item *si, *nsi;
	int parse_status;

#ifdef CFG_MODULE
	gint retval;
#endif /* CFG_MODULE */

#ifdef AAA_CLIENT
	int packet_ok;
#endif /*AAA_CLIENT*/

	current_number = g_static_private_get(&current_thread_number);
	if (!current_number) {
		g_static_mutex_lock(&current_thread_mutex);
		current_number = g_new(int, 1);
		*current_number = thread_counter++;
		g_static_private_set(&current_thread_number, current_number,
				g_free);
		g_static_mutex_unlock(&current_thread_mutex);
	}

	LOG_TRACE("Starting thread id %d for session %p",
			*current_number, session);

	/*
	 * Retrieve message that has to be processed
	 *
	 * We should _try_ to pop from session since it might happen
	 * that there is race condition between pushing a message,
	 * and activating a session, while some other thread checks
	 * messages in-between!
	 */
	sm_msg = session_msg_pop(session);
	si = NULL;

	switch(sm_msg->msg_type) {
	case SM_SESSION_MSG:

		/*
		 * We received internal message from state machines.
		 */
		switch (sm_msg->sm_session_msg.message) {
		case MSG_IKE_REKEY:

			/*
			 * Start rekeying IKE SA
			 */
			LOG_DEBUG("Processing MSG_IKE_REKEY");

			session_set_state(session, IKE_SM_REKEY);
			if (sm_ike_sa_rekey_i(session,
					&sm_msg->message_msg) < 0) {
				/*
				 * Error occured, so terminate session.
				 *
				 * TODO: We have to mark this session so that
				 * it is not used for creating new child sa's
				 * etc!
				 */
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			}

			break;

		case MSG_IKE_REAUTH:

			/*
			 * Start reautentification of IKE SA
			 */
			LOG_DEBUG("Processing MSG_IKE_REAUTH");

			/*
			 * Mark the current session in reautentification
			 * state.
			 */
			session_set_state(session, IKE_SM_REAUTH);

			nsession = session_new();
			session_set_ike_type(nsession, IKEV2_INITIATOR);
			session_set_state(nsession, IKE_SMI_INIT);

			if (session_get_ike_type(session) == IKEV2_INITIATOR) {
				session_set_i_addr(nsession,
						session_get_i_addr(session));
				session_set_r_addr(nsession,
						session_get_r_addr(session));
			} else {
				session_set_r_addr(nsession,
						session_get_i_addr(session));
				session_set_i_addr(nsession,
						session_get_r_addr(session));
			}

			session_lock(nsession);
			nsession->session = session;

			sm_sessions_session_add(nsession);

			if (sm_ike_i_thread(nsession, sm_msg) < 0) {
				sm_session_remove(nsession);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			} else {
				if (session_check_queue(nsession)) {
					LOG_DEBUG("Activating session");
					sm_session_activate(nsession);
				} else
					session_unlock(nsession);
			}

			break;

		case MSG_IKE_DPD:

			/*
			 * Start dead peer detection process
			 */
			LOG_DEBUG("Processing MSG_IKE_DPD");

			/*
			 * This is very easy and elegant. We just send
			 * empty NOTIFY request and wait for empty
			 * response. If there is no response within
			 * timeout, time retransmission mechanism will
			 * initiate termination of IKE SA!
			 */
			message_send_notify(session, NULL);

			break;

		case MSG_IKE_REMOVE:

			/*
			 * Remove session from memory.
			 */
			LOG_DEBUG("Processing MSG_IKE_REMOVE");

			sm_session_remove(session);
			session = NULL;

			break;

		case MSG_IKE_TERMINATE:

			LOG_DEBUG("Processing MSG_IKE_TERMINATE");

			deletes = NULL;
			deletes = g_slist_append(deletes, &pddata);
			pddata.protocol = IKEV2_PROTOCOL_IKE;
			pddata.spi_size = 0;
			pddata.n_spi = 0;
			pddata.spis = NULL;

			/*
			 * TODO:
			 * We have to empty msg_queue so that response
			 * is immediatelly processed!
			 */
			message_send_delete(session, NULL, deletes);

			deletes = g_slist_remove(deletes, &pddata);

			session_set_state(session, IKE_SM_DYING);

			/*
			 * TODO
			 * Interesting situation. If we have a queue of
			 * requests waiting to be served, and we want to
			 * terminate a session. What should we do? To
			 * serve all the requests first and then terminate,
			 * or, just terminate?
			 *
			 * I suppose we should just terminate, but how!?
			 */
			break;

#ifdef CFG_CLIENT
		case MSG_IKE_LEASE_RENEW:

			/*
			 * Lease time expired. We receive this message
			 * only when acting as a Initiator. The purpose
			 * of thise message is to try to renew lease
			 * time.
			 *
			 * We do not (for now) set state to any special
			 * state.
			 */
			LOG_DEBUG("Processing MSG_IKE_LEASE_RENEW");

			if (message_send_informational_renew_i(session) < 0)
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));

			break;
#endif /* CFG_CLIENT */

		case MSG_SA_TRANSFER:

			/*
			 * Perform CHILD SA transfers to a new IKE SA. If
			 * there are no CHILD SAs to be transfered then
			 * initiate IKE SA delete message.
			 */
			LOG_DEBUG("Processing MSG_SA_TRANSFER");

			si = session->session->sad->data;

			if (sad_item_get_state(si) == SA_STATE_MATURE) {

				/*
				 * First, remove CHILD SA from the head
				 * of the list and move it to the end.
				 */
				session_sad_item_remove(session->session, si);
				session_sad_item_add(session->session, si);

				/*
				 * Set CHILD SA state to DEAD
				 */
				sad_item_set_state(si, SA_STATE_DEAD);

				nsi = sad_item_clone(si);
				session_sad_item_add(session, nsi);

				sm_child_sa_create_i(session, nsi, sm_msg);

				session_unlock(session->session);

			} else {

				/*
				 * There are no more CHILD SAs to be moved
				 * to the new IKE SA, so initiate removal
				 * of the old IKE SA.
				 *
				 * TODO:
				 * See the note for the MSG_IKE_TERMINATE
				 * since we have some race conditions!
				 */
				deletes = NULL;
				deletes = g_slist_append(deletes, &pddata);
				pddata.protocol = IKEV2_PROTOCOL_IKE;
				pddata.spi_size = 0;
				pddata.n_spi = 0;
				pddata.spis = NULL;

				message_send_delete(session->session,
						NULL, deletes);

				deletes = g_slist_remove(deletes, &pddata);

				session_set_state(session->session,
						IKE_SM_DYING);

				if (session_check_queue(session->session)) {
					LOG_DEBUG("Activating session");
					sm_session_activate(session->session);
				} else
					session_unlock(session->session);

				session->session = NULL;
			}

			break;

		case MSG_SA_DELETE:
			/*
			 * We received message to delete CHILD SA. Find
			 * first child sa's that is in the state
			 * SA_STATE_DELETE and start delete exchange!
			 *
			 * Session has to be locked!
			 */
			LOG_DEBUG("Processing MSG_SA_DELETE");

			sad_item_count = session->sad;
			while (sad_item_count) {
				si = sad_item_count->data;
				if (sad_item_get_state(si) == SA_STATE_DEAD) {
					if (sm_child_sa_delete_i(session,
							si) < 0) {
						session_sad_item_remove(session,
								si);
						sad_item_free(si);
					}
					break;
				}
				sad_item_count = sad_item_count->next;
			}

			break;

		case MSG_SA_CLEAN:
			/*
			 * We received message to remove CHILD SA. Find
			 * all child sa's that are in the state
			 * SA_STATE_REMOVE and remove them!
			 *
			 * Both child_sa and session have to be locked.
			 */
			LOG_DEBUG("Processing MSG_SA_CLEAN");

			sad_item_count = session->sad;
			while (sad_item_count) {
				si = sad_item_count->data;
				if (sad_item_get_state(si)
						== SA_STATE_REMOVE) {
					session_sad_item_remove(session, si);
					sad_item_free(si);
					continue;
				}
				sad_item_count = sad_item_count->next;
			}

			break;

		case MSG_IKE_CRL_UPDATE:

			LOG_DEBUG("Processing MSG_IKE_CRL_UPDATE");

			session_set_state(session, IKE_SM_CRL_UPDATE);
			if (sm_ike_crl_update(session, NULL) < 0) {
				/*
				 * Error occured, so terminate session.
				 */
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			}

			break;

		default:
			LOG_BUG("Unhandled message!");
		}
		break;

	case PFKEY_MSG:

		/*
		 * We received a new message from a kernel, e.g. to
		 * rekey existing CHILD SA, create new CHILD SA, ...
		 */
		LOG_DEBUG("Processing PFKEY_MSG");

		/*
		 * Found out what event caused this state machine
		 * to execute...
		 */
		switch (sm_msg->pfkey_msg.action) {
		case SADB_ACQUIRE:

			/*
			 * If there is no connection with peer yet, then
			 * CHILD SA is created by other means (IKE_SA_INIT +
			 * IKE_AUTH exchanges).
			 *
			 * TODO:
			 * There is also possibility that we are rekeying
			 * IKE SA, in which case we should postpone CHILD
			 * SA creation.
			 */
			if (session_get_state(session) == IKE_SMI_INIT) {
				if (sm_ike_i_thread(session, sm_msg) < 0) {
					/*
					 * TODO: Update half opened connections,
					 * if necessary!
					 */
					sm_session_remove(session);
					session = NULL;
				}
				break;
			}

			/*
			 * Otherwise, there is already IKE SA so create new
			 * CHILD SA.
			 */
			if ((si = sad_item_new()) == NULL)
				break;

			sad_item_set_state(si, SA_STATE_CREATE);
			session_sad_item_add(session, si);

			if (sm_child_sa_create_i(session, si, sm_msg) < 0) {
				sad_item_set_state(si, SA_STATE_REMOVE);
				session_msg_push(session,
					sm_session_msg_new(MSG_SA_CLEAN));
			}

			break;

		case SADB_EXPIRE:

			/*
			 * If EXPIRE message has been received search for
			 * associated CSA. In case there is no appropriate child
			 * sa then ignore message, otherwise, create new CSA
			 * structure move expiring one as it's child and start
			 * CSA rekeying. Also, set up rekey_spi filed.
			 *
			 * Also, per IKEv2 draft it is wise to add jitter to
			 * rekeying CHILD SA, but maybe it's better to do during
			 * SA installation into kernel.
			 *
			 * According to IKE draft clarifications:
			 * (draft-eronen-ipsec-ikev2-clarifications-04.txt)
			 *
			 * "Since CHILD_SAs always exist in pairs, there are two
			 *  different SPIs. The SPI placed in the REKEY_SA
			 *  notification is the SPI the exchange initiator would
			 *  expect in inbound ESP or AH packets (just as in
			 *  DELETE payloads)."
			 */
			si = session_sad_item_find_by_peer_spi(session,
						sm_msg->pfkey_msg.expire.spi);

			if (!si)
				break;

			/*
			 * If this CHILD SA is already in the process of
			 * rekeying, then it will be in the state of
			 * SA_STATE_DEAD.
			 */
			if (sad_item_get_state(si) != SA_STATE_MATURE) {
				si = NULL;
				break;
			}

			/*
			 * If we reached hard limit send delete notify
			 * to the peer.
			 */
			if (sm_msg->pfkey_msg.expire.hard_limit) {
                               	sad_item_set_state(si, SA_STATE_REMOVE);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_SA_CLEAN));
				break;
			}

			sad_item_set_state(si, SA_STATE_REKEY);

			LOG_DEBUG("state=%d si=%p spi=%08X peer_spi=%08X",
					sad_item_get_state(si), si,
					(guint32)sad_item_get_spi(si),
					(guint32)sad_item_get_peer_spi(si));


			if (sm_child_sa_rekey_i(session, si, sm_msg) < 0) {
				LOG_WARNING("Rekeying was not successful. "
						"Using SA until hard limit "
						"expires!");
                               	sad_item_set_state(si, SA_STATE_MATURE);
			}
		}

		break;

	case MESSAGE_MSG:

#warning "There is a race here when we receive message faster then we are able to determine keys"
#warning "Something is not right with locking since session lock should prevent this from happening"
		LOG_DEBUG("Processing MESSAGE_MSG");

		/*
		 * Mark that we received something from a peer so that
		 * dead peer detection mechanism is not activated.
		 */
		gettimeofday(&tv, NULL);
		session->last_seen = tv.tv_sec;

#ifdef NATT
		/*
		 * If NAT-T is turned on for the peer, then update port
		 * from which we received the packet since it's the port
		 * on which we should send reply.
		 *
		 * The question is if this is a right place to put this
		 * check, and also, what if NAT-T changes port and/or
		 * address!
		 */
		if (session_get_ike_type(session) == IKEV2_RESPONDER &&
				session->natt && sm_msg->message_msg.msg_id)
			netaddr_set_port(session_get_i_addr(session),
					netaddr_get_port(
						sm_msg->message_msg.srcaddr));
#endif

		parse_status = message_parse(&sm_msg->message_msg, session);

		if (parse_status & MSGPARSE_SEND_NOTIFY)
			message_send_notify(session, &sm_msg->message_msg);

		if (parse_status & MSGPARSE_TERMINATE_SESSION) {
			sm_session_remove(session);
			session = NULL;
		}

		if (parse_status & MSGPARSE_TERMINATE_MSG) {
			LOG_TRACE("Terminating message processing");
			break;
		}

		/*
		 * Peer sent us a message, it could be either request
		 * or a response to our request...
		 */
		switch (sm_msg->message_msg.exchg_type) {
		case IKEV2_EXT_IKE_SA_INIT:
		case IKEV2_EXT_IKE_AUTH:
			LOG_DEBUG("Processing IKEV2_EXT_IKE_SA_INIT "
					"or IKEV2_EXT_IKE_AUTH");

			if (sm_msg->message_msg.response) {
				if (sm_ike_i_thread(session, sm_msg) < 0) {
					/*
					 * TODO: Update half opened connections,
					 * if necessary!
					 */
					LOG_DEBUG("Function sm_ike_i_thread "
							"returned ERROR. "
							"Removing session!");
#warning "If necessary, send DELETE to peer!"
					sm_session_remove(session);
					session = NULL;
				}

			} else {

				if (sm_ike_r_thread(session, sm_msg) < 0) {

					/*
					 * TODO: Update half opened connections,
					 * if necessary!
					 */
					LOG_DEBUG("Function sm_ike_r_thread "
							"returned ERROR. "
							"Removing session!");
					sm_session_remove(session);
					session = NULL;
				} else {
					/*
					 * Prevent freeing of sm_msg in case it
					 * was saved by CFG handling code...
					 */
					if (session_get_state(session) ==
							IKE_SMR_CFG_WAIT)
						sm_msg = NULL;
				}
			}
			break;

		case IKEV2_EXT_CREATE_CHILD_SA:

			/*
			 * This exchange is used for the following purposes
			 *
			 * - rekey existing CHILD SA
			 * - create new CHILD SA
			 * - rekey IKE SA
			 *
			 * we differentiate between those cases and then
			 * call appropriate processing function. Note that
			 * we do not keep any state to not cause deadlock
			 * with peer sending request and in the same time
			 * we sending request.
			 */

			/*
			 * This is declared here to make lines a bit shorter!
			 */
			ccs = &sm_msg->message_msg.create_child_sa;

			/*
			 * If we are receiving response, first check if this
			 * response is related to IKE SA rekeying.
			 */
			if (sm_msg->message_msg.response
				&& sm_msg->message_msg.msg_id ==
						session->msgid) {

				if (sm_ike_sa_rekey_i(session,
						&sm_msg->message_msg) < 0)
					session_msg_push(session,
                                       		sm_session_msg_new(
							MSG_IKE_REMOVE));

				break;
			}

			/*
			 * If this is response then search for child sa that
			 * issued appropriate request...
			 */
			if (sm_msg->message_msg.response) {

				si = session_sad_item_find_by_msg_id(session,
						sm_msg->message_msg.msg_id);

				if (!si) {
					LOG_ERROR("Received response on "
							"unsent request!");
					break;
				}

				switch(sad_item_get_state(si)) {
				case SA_STATE_CREATE_WAIT:

					if (sm_child_sa_create_i(session,
							si, sm_msg) < 0) {
                                		sad_item_set_state(si,
							SA_STATE_REMOVE);
						session_msg_push(session,
                                        		sm_session_msg_new(
								MSG_SA_CLEAN));
					}

					break;

				case SA_STATE_REKEY_WAIT:

					if (sm_child_sa_rekey_i(session,
							si, sm_msg) < 0) {
                                		sad_item_set_state(si,
							SA_STATE_REMOVE);
						session_msg_push(session,
                                        		sm_session_msg_new(
								MSG_SA_CLEAN));
					}

					break;
				}

				break;
			}

			/*
			 * So, this is a request.
			 */

			/*
			 * First we process create CHILD SA. We recognize this
			 * case by checking that rekey_spi is not defined, and
			 * there are TSi and TSr payloads!
			 */
			if (!ccs->rekey_spi && ccs->tsi && ccs->tsr) {

				/*
				 * Here we must check if additional SAs are
				 * allowed on this IKE SA. If not then send
				 * back notify.
				 */
				if (session->cp->auth_limits->allocs <=
							session->allocations) {
					LOG_ERROR("No more CHILD SAs allowed"
							" on this IKE SA");
					sm_msg->message_msg.notify =
							N_NO_ADDITIONAL_SAS;
					message_send_notify(session,
							&sm_msg->message_msg);
					break;
				}

				si = sad_item_new();

				session_sad_item_add(session, si);

				if (sm_child_sa_create_r(session, si,
						&sm_msg->message_msg) < 0) {
                               		sad_item_set_state(si,
							SA_STATE_REMOVE);
                               		session_msg_push(session,
                                        		sm_session_msg_new(
								MSG_SA_CLEAN));
				}

				break;
			}

			/*
			 * Now, check if this is CHILD SA rekeying request.
			 */
			if (ccs->rekey_spi && ccs->tsi && ccs->tsr) {

				/*
				 * Try to find CSA which should be rekeyed...
				 */
				si = session_sad_item_find_by_spi(session,
							ccs->rekey_spi);

				if (!si) {
					LOG_ERROR("Received request for "
						"rekeying non-existing "
						" CHILD SA (spi=%08X)",
						ccs->rekey_spi);
					sm_msg->message_msg.notify =
							N_INVALID_SYNTAX;
					message_send_notify(session,
							&sm_msg->message_msg);
					break;
				}

				if (sad_item_get_state(si) != SA_STATE_MATURE)
					break;

				/*
				 * TODO: We have to check for the case
				 * of crossed requests!!!!
				 */
				if (sm_child_sa_rekey_r(session, si,
						&sm_msg->message_msg) < 0) {
                               		sad_item_set_state(si,
							SA_STATE_REMOVE);
                               		session_msg_push(session,
                                        		sm_session_msg_new(
								MSG_SA_CLEAN));
				}


				break;
			}

			/*
			 * Finally, check if this is IKE SA rekeying request.
			 */
			if (!ccs->tsi && !ccs->tsr) {

				if (sm_ike_sa_rekey_r(session,
						&sm_msg->message_msg) < 0)
					session_msg_push(session,
							sm_session_msg_new(
								MSG_SA_CLEAN));

				break;
			}

			LOG_BUG("It's a miracle! We reached unreachable code!");

			break;

		case IKEV2_EXT_INFORMATIONAL:
			if (sm_process_informational(session,
					&sm_msg->message_msg) < 0) {
				sm_session_remove(session);
				session = NULL;
			}

			break;

		default:
			/*
			 * TODO
			 * Notify has to be sent because we received a message
			 * with unexpected/unknown exchange type.
			 */
			LOG_ERROR("Received unexpected exchange type %u",
					sm_msg->message_msg.exchg_type);
			break;
		}
		break;

#ifdef AAA_CLIENT
	case AAA_MESSAGE:		

		/*
		 * If AAA server didn't respond (even after retries)
		 */
		if (((struct aaa_msg *)sm_msg)->type == AAA_NO_RESPONSE) {
			/* send AUTH failed to peer */
			((struct message_msg*) session->embedded_sm_msg)
					->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, (struct message_msg *)
					session->embedded_sm_msg);

			sm_session_remove(session);
			session = NULL;

			break;
		}
		/*
		 * Check if we are waiting a response from AAA:
		 * - if state is one of: IKE_SMR_EAP_AAA_REQUEST or
		 *                       IKE_SMR_AUTH_RESPONSE
		 */
		switch (session_get_state(session)) {
		case IKE_SMR_EAP_AAA_REQUEST:
		case IKE_SMR_AUTH_RESPONSE:
			packet_ok = TRUE;
			break;
		default:
			packet_ok = FALSE;
			LOG_WARNING("AAA packet received, but was not"
					" expected");
		}
		if (!packet_ok)
			break;		

		if (sm_ike_r_thread(session, sm_msg) < 0) {
			sm_session_remove(session);
			session = NULL;
		}

		break;
#endif /* AAA_CLIENT */

#ifdef CFG_MODULE
	case CFG_MSG:

		g_assert(session->preserve_message);

		if (session_get_state(session) == IKE_SMR_CFG_WAIT)
			retval = sm_ike_r_thread(session, sm_msg);
		else
			retval = sm_process_informational_finish(session,
					session->embedded_sm_msg);

		if (retval < 0) {
			sm_session_remove(session);
			session = NULL;
		}

		break;
#endif /* CFG_MODULE */

	case CONNECT_MSG:

		/*
		 * We received a the message to establish a connection
		 * to a remote peer. Peer is selected by a given ID in
		 * the received message.
		 */
		LOG_DEBUG("Processing CONNECT_MSG");

		/*
		 * Initiate IKE_SA_INIT exchange
		 */
		if (sm_ike_i_thread(session, sm_msg) < 0) {
			/*
			 * TODO: Update half opened connections,
			 * if necessary!
			 */
			sm_session_remove(session);
			session = NULL;

			/*
			 * TODO: In case of an error we should terminate ikev2
			 */
			g_async_queue_push (ikev2_data->queue,
					GUINT_TO_POINTER(1));
		}

		break;

	case CERT_MSG:
		/*
		 * CERT_MSG is received in following cases:
		 *	1. cert material download (in AUTH phase) finished
		 *	2. new CRL version download is finished
		 */
		LOG_DEBUG("Processing CERT_MSG");

		/*
		 * case 2: proceed to verification with new CRL
		 */
		if (session_get_state(session) == IKE_SM_CRL_UPDATE) {
			if (sm_ike_crl_update(session, sm_msg) < 0) {
			/* TODO: pitaj: remove ili TERMINATE??? */
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			}
			break;
		}

		/* 
		 * case 1: check session type
		 */
		if (session_get_ike_type(session) == IKEV2_INITIATOR) {
			if (sm_ike_i_thread(session, sm_msg) < 0) {
				sm_session_remove(session);
				session = NULL;
			}
		} else {
			if (sm_ike_r_thread(session, sm_msg) < 0) {
				sm_session_remove(session);
				session = NULL;
			}
		}

		break;
	} /* switch(sm_msg->msg_type) */

	if (sm_msg)
		if (!session || !session->preserve_message)
			sm_msg_free(sm_msg);

	if (session) {

		if (!session->preserve_message && session->embedded_sm_msg) {
			sm_msg_free(session->embedded_sm_msg);
			session->embedded_sm_msg = NULL;
		}

		if (session_check_queue(session)) {
			LOG_DEBUG("Activating session");
			sm_session_activate(session);
		} else
			session_unlock(session);
	}

	LOG_TRACE("Ending thread id %d for session %p", *current_number,
			session);

	LOG_FUNC_END(1);
}

/**
 * Main function for initiator during IKE_SA_INIT and IKE_AUTH exchanges.
 *
 * @param session	Session that has to be processed
 * @param sm_msg	Received message, either from kernel or from peer
 *
 * \return	 0 if everything went OK, or there was an error, but session
 * 		   will be removed by some other mechanism
 *		 1 if there was error but IKE SA has been successfuly established
 *		   Caler has to release child_sa (there will be single child sa
 *		   structure anyway)
 *		-1 establishment of IKE SA was not successful and session
 *		   structure has to be removed.
 */
int sm_ike_i_thread(struct session *session, union sm_msg *sm_msg)
{
	struct timeval tv;
	struct sad_item *si = NULL, *ochild_sa;
	proposal_t *proposal;
	gboolean auth_failed;
	unsigned char *nonce;
	struct auth_data auth_data;
	char ip_buf[INET6_ADDRSTRLEN];
	gint retval;
	gchar *idstr;
#ifdef MOBIKE
	guint8 mobike_port_flags = 0;
#endif /* MOBIKE */

#ifdef CFG_CLIENT
	struct netaddr *vpngw;
	struct netif *vpnif;
#endif /* CFG_CLIENT */

#ifdef NATT
	/*
	 * NAT variables
	 */
	GSList *iterator;
	guint64 i_spi, r_spi;
	gint32 hash_len;
	struct network_address_iterator_t *ip_addresses = NULL;
	netaddr_t *ip_addr;
	guchar *hash, *digest, *addr;
	gboolean natt, nat_keep_alive;
	guint16 port;
	GTimeVal keepalive_tv;
	char buffer[INET6_ADDRSTRLEN];
#endif /* NATT */

	/*
	 * Certificate variables
	 */
	unsigned char *cert_file;
	time_t next_crl_update;
	GSList *verified_certs = NULL, *verified_cert_items = NULL;
	GSList *peers_cert_items = NULL;
	GSList *ca_items = NULL;

	LOG_FUNC_START(1);

	/*
	 * Initialize all the variables
	 */
	retval = -1;

	/*
	 * Check if we have cookie present. If so, and we are expecting
	 * response (i.e. we are in state IKE_SMI_AUTH), then put state
	 * machine in state IKE_SMI_COOKIE
	 */
	if (session_get_state(session) == IKE_SMI_AUTH &&
			sm_msg->msg_type == MESSAGE_MSG &&
			sm_msg->message_msg.ike_sa_init.cookie)
		session_set_state(session, IKE_SMI_COOKIE);

	/*
	 * Check if we have notify INVALID_KE_PAYLOAD present. If so, and
	 * we are expecting response (i.e. we are in state IKE_SMI_AUTH),
	 * then put state machine in state IKE_SMI_INVALIDKE
	 */
	if (session_get_state(session) == IKE_SMI_AUTH &&
			sm_msg->msg_type == MESSAGE_MSG &&
			sm_msg->message_msg.ike_sa_init.peer_dh_group < 0)
		session_set_state(session, IKE_SMI_INVALIDKE);

	/*
	 * Process received message depending on the current state of
	 * the state machine.
	 */
	switch (session_get_state(session)) {
	case IKE_SMI_INIT:

		LOG_DEBUG("STATE IKE_SMI_INIT");

		/*
		 * Find specific configuration data for peer.
		 */
		g_static_rw_lock_reader_lock(&(ikev2_data->config_lock));

		if (sm_msg->msg_type == CONNECT_MSG) {
			idstr = id_id2str(sm_msg->connect_msg.id);
			LOG_NOTICE("Searching configuration data for peer"
					" with id %s", idstr);
			g_free(idstr);
			session->cr = config_remote_find_by_id(
						ikev2_data->config,
						sm_msg->connect_msg.id);
		} else {
			LOG_NOTICE("Searching configuration data for peer"
					" with address %s",
					netaddr_ip2str(
						session_get_r_addr(session),
						ip_buf, INET6_ADDRSTRLEN));
			session->cr = config_remote_find_by_addr(
						ikev2_data->config,
						session_get_r_addr(session));
		}
		if (session->cr)
			g_atomic_int_inc(&(session->cr->refcnt));

		g_static_rw_lock_reader_unlock(&(ikev2_data->config_lock));

		if (session->cr == NULL) {
			LOG_ERROR("Could not find configuration data");
			goto out;
		}

		LOG_NOTICE("Selected REMOTE section at line %u",
				session->cr->cfg_line);

		/*
		 * Find corresponding CHILD SA structure. These have to
		 * be here instead of IKE_SMI_AUTH because of pfkey
		 * message availability...
		 */
		si = session_get_sad_item(session);
		sad_item_set_initiator(si, TRUE);

		/*
		 * Check whether we are doing reautenthication, or we are
		 * creating new IKE SA.
		 */
		switch (sm_msg->msg_type) {
		case PFKEY_MSG:

			/*
			 * Find SPD that triggered ACQUIRE message
			 */
			si->spd = spd_find_by_pid(
					sm_msg->pfkey_msg.acquire.pid);

			/*
			 * If there is no SPD, terminate. This should
			 * not occur in normal circumstances
			 */
			if (!si->spd) {
				LOG_ERROR("No SPD found that triggered"
						" ACQUIRE");
				goto out;
			}

			spd_item_ref(si->spd);

			LOG_DEBUG("Selected policy with ID %u",
					si->spd->pid);

			/*
			 * If the given policy doesn't have configuration
			 * structure, signal an error and terminate
			 */
			if (!si->spd->ccsa) {
				LOG_ERROR("There is no appropriate "
						"configuration section for a"
						" policy ID %u",
						sm_msg->pfkey_msg.acquire.pid);
				goto out;
			}

			g_static_rw_lock_reader_lock(
					&(ikev2_data->config_lock));

			/*
			 * Find peer section related to this SPD
			 */
			session->cp = config_peer_find_by_ccsa(
						ikev2_data->config->peer,
						si->spd->ccsa);

			if (session->cp) {
				g_atomic_int_inc(&(session->cp->refcnt));

				session->cg = ikev2_data->config->cg;
				g_atomic_int_inc(&(session->cg->refcnt));

				g_atomic_int_inc(&(si->spd->refcnt));
			}

			g_static_rw_lock_reader_unlock(
					&(ikev2_data->config_lock));

			if (session->cp == NULL) {
				LOG_ERROR("Could not find configuration");
				goto out;
			}

			LOG_NOTICE("Selected PEER section at line %u\n",
					session->cp->cfg_line);

			break;

		case CONNECT_MSG:

			if (session->cr->ts_list->next)
				LOG_WARNING("Remote section has more than "
						"one traffic selector. Using "
						"only the first one");

			if (netaddr_cmp_ip2ip(
					ts_get_saddr(
						session->cr->ts_list->data),
					ts_get_eaddr(
						session->cr->ts_list->data))) {
				LOG_WARNING("Remote section does not specify"
						" a single IP address!");
			}

			session_set_r_addr(session,
					ts_get_saddr(
						session->cr->ts_list->data));

			g_static_rw_lock_reader_lock(
					&(ikev2_data->config_lock));

			/*
			 * Find peer section by the given ID
			 */
			session->cp = config_peer_find_by_id(
						ikev2_data->config->peer,
						sm_msg->connect_msg.id);

			if (session->cp) {
				g_atomic_int_inc(&(session->cp->refcnt));

				session->cg = ikev2_data->config->cg;
				g_atomic_int_inc(&(session->cg->refcnt));
			}

			g_static_rw_lock_reader_unlock(
					&(ikev2_data->config_lock));

			if (session->cp == NULL) {
				idstr = id_id2str(sm_msg->connect_msg.id);
				LOG_ERROR("Could not find configuration "
						"for the ID %s", idstr);
				g_free(idstr);
				goto out;
			}

			LOG_NOTICE("Selected PEER section at line %u\n",
					session->cp->cfg_line);

			/*
			 * Take the first SAINFO section to be established
			 * immediatelly upon connection. This retrevial of
			 * the SPD should be done via some function from
			 * the configuration module, but, some good name
			 * should be given to it...
			 */
			si->spd = spd_item_new();
			si->spd->ccsa = session->cp->sainfo->data;

			break;

		default:

			/*
			 * We enter here when IKEv2 is doint reautentification
			 * of IKE SA. In that case, peer section in the
			 * configuration file is selected based on the
			 * known identity of the other side!
			 */
			if (!session->session->sad ||
					!session->session->sad->data) {
				LOG_ERROR("Session has no CHILD SAs. "
						"Terminating session.");
				goto out;
			}

			g_static_rw_lock_reader_lock(
					&(ikev2_data->config_lock));

			/*
			 * Find peer section by the ID from the old IKE SA
			 */
			session->cp = config_peer_find_by_id(
						ikev2_data->config->peer,
						session->session->peer_id);

			if (session->cp) {
				g_atomic_int_inc(&(session->cp->refcnt));

				session->cg = ikev2_data->config->cg;
				g_atomic_int_inc(&(session->cg->refcnt));
			}

			g_static_rw_lock_reader_unlock(
					&(ikev2_data->config_lock));

			if (session->cp == NULL) {
				idstr = id_id2str(sm_msg->connect_msg.id);
				LOG_ERROR("Could not find configuration "
					"for the ID %s", idstr);
				g_free(idstr);
				goto out;
			}

			LOG_NOTICE("Selected PEER section at line %u\n",
					session->cp->cfg_line);

			ochild_sa = session->session->sad->data;

			sad_item_set_spd(si, sad_item_get_spd(ochild_sa));

			sad_item_set_state(ochild_sa, SA_STATE_DEAD);

			session_sad_item_remove(session->session, ochild_sa);
			session_sad_item_add(session->session, ochild_sa);

			break;
		}

		/*
		 * Generate initiator SPI...
		 */
		if (!rand_bytes((unsigned char *)&(session->i_spi),
						sizeof(session->i_spi))) {
			LOG_ERROR("Error generating random values");
			goto out;
		}

		LOG_DEBUG("I_SPI:");
		log_data(LOGGERNAME, (char *)&(session->i_spi),
				sizeof(session->i_spi));

#ifdef NATT
		hash = NULL;
		digest = NULL;
		addr = NULL;

		/*
		 * If NATT is ON, prepare hashes of addresses to be sent to
		 * responder
		 */
		session->nat_src_hashes = NULL;
		session->nat_dest_hashes = NULL;

		if (session->cr->natt == NATT_ON) {

			LOG_DEBUG("NAT-T is on. Preparing hashes.");

			ip_addresses = network_address_iterator_new(AF_INET,
							NULL);

			i_spi = session_get_i_spi(session);
			r_spi = session_get_r_spi(session);

			hash = g_malloc(sizeof(struct in_addr) +
						sizeof(guint16) +
						2 * sizeof(guint64));

			ip_addr = network_address_iterator_next(ip_addresses);
			while (ip_addr) {

				hash_len = 0;

				memcpy(hash, netaddr_get_ipaddr(ip_addr), 
						sizeof(struct in_addr));
				hash_len += sizeof(struct in_addr);

				port = IKEV2_PORT;
				LOG_DEBUG("Port = %u", port);

				memcpy(hash + hash_len, &port, sizeof(port));
				hash_len += sizeof(port);

				memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
				hash_len += sizeof(i_spi);

				memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
				hash_len += sizeof(r_spi);

				digest = g_malloc(SHA_DIGEST_LENGTH);

				/* 
				 * Hashing IP address, port and SPIs
				 */
				SHA1(hash, hash_len, digest);

				LOG_DEBUG("Source address digest");
				log_buffer(LOGGERNAME, (char *)digest,
						SHA_DIGEST_LENGTH);

				session_nat_hash_append(session, digest);

				ip_addr = network_address_iterator_next(
							ip_addresses);
			}

			network_address_iterator_free(ip_addresses);

			/*
			 * Generating hash of destination IP address
			 */
			hash_len = 0;
			ip_addr = session_get_r_addr(session);

			memcpy(hash, netaddr_get_ipaddr(ip_addr), 
					sizeof(struct in_addr));
			hash_len += sizeof(struct in_addr);

			port = IKEV2_PORT;
			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);

			digest = g_malloc(SHA_DIGEST_LENGTH);

			/* 
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);
			session->nat_dest_hashes = digest;

			LOG_DEBUG("Destination address digest");
			log_buffer(LOGGERNAME, (char *)digest,
					SHA_DIGEST_LENGTH);

			g_free(hash);
		}
#endif /* NATT */

		/*
		 * Generate KE material
		 */
		session->dh_group = proposal_list_find_transform(
						session_get_proposals(session),
						IKEV2_TRANSFORM_DH_GROUP,
						IKEV2_PROTOCOL_IKE);

		session->dh = dh_ke_generate(
					transform_get_id(session->dh_group));

		/*
		 * Generate NONCE value
		 */
		nonce = g_malloc(session->cr->nonce_size);

		if (!rand_bytes(nonce, session->cr->nonce_size)) {
			LOG_ERROR("Error generating random value");
			goto out;
		}

		session->i_nonce = BN_bin2bn(nonce, session->cr->nonce_size,
					NULL);

		g_free(nonce);
		nonce = NULL;

		session_set_state(session, IKE_SMI_AUTH);

		/*
		 * This is here so that we have 0 in a first message no metter
		 * how many times we retransmitt it, and in the same time that
		 * the next message in exchange (IKE_SA_AUTH) has correct ID
		 */
		session_get_next_msgid(session);

		message_send_ike_sa_init_i(session, NULL);

		break;

	case IKE_SMI_INVALIDKE:

		LOG_DEBUG("STATE IKE_SMI_INVALIDKE");

		/*
		 * Find transform with a given DH GROUP
		 */
		session->dh_group = proposal_find_dh_group(
				session_get_proposals(session),
				-sm_msg->message_msg.ike_sa_init.peer_dh_group);

		if (session->dh_group == NULL) {
			LOG_ERROR("Peer requested invalid DH group");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			retval = 0;
			goto out;
		}

		/*
		 * Remove existing KE material
		 */
		DH_free(session->dh);

		/*
		 * Generate new KE material with a given DH group
		 */
		session->dh = dh_ke_generate(
					transform_get_id(session->dh_group));

		message_send_ike_sa_init_i(session, &sm_msg->message_msg);

		session_set_state(session, IKE_SMI_AUTH);

		break;

	case IKE_SMI_COOKIE:

		LOG_DEBUG("STATE IKE_SMI_COOKIE");
		message_send_ike_sa_init_i(session, &sm_msg->message_msg);

		session_set_state(session, IKE_SMI_AUTH);

		break;

	case IKE_SMI_AUTH:

		LOG_DEBUG("STATE IKE_SMI_AUTH");

		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_sa_init.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			goto out;
		}

		/*
		 * Check if we received notify NO_PROPOSAL_CHOSEN. In that case
		 * terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_sa_init.no_proposal_chosen) {
			LOG_ERROR("Peer sent NO_PROPOSAL_CHOSEN notify");
			goto out;
		}
		
		/*
		 * Check if we received HTTP_CERT_LOOKUP_SUPPORTED notify
		 */
		if (sm_msg->message_msg.ike_sa_init.http_cert_lookup)
			session->cert_lookup_supported = TRUE;

		/*
		 * Check if we have source IP address in session. If no,
		 * it's because we started connection on recepient of
		 * CONNECT_MSG message.
		 */
		if (!session_get_i_addr(session))
			session_set_i_addr(session,
					netaddr_dup(
						sm_msg->message_msg.dstaddr));

		/*
		 * This is initial IKE SA exchange, so take a copy of a packet
		 * for later authentication phase...
		 */
		session->ikesa_init_r = g_memdup(sm_msg->message_msg.buffer,
						sm_msg->message_msg.size);
		session->ikesa_init_r_len = sm_msg->message_msg.size;

		session_set_r_spi(session, sm_msg->message_msg.r_spi);

		session->pub_key =
			sm_msg->message_msg.ike_sa_init.peer_pub_key;
		sm_msg->message_msg.ike_sa_init.peer_pub_key = NULL;

		session->r_nonce =
			sm_msg->message_msg.ike_sa_init.peer_nonce;
		sm_msg->message_msg.ike_sa_init.peer_nonce = NULL;

#ifdef NATT
		/*
		 * NAT support.
		 * If peer sent NAT NOTIFY payloads and configuration allows
		 * it, check whether responder is behind a NAT device
		 */
		if (session->cr->natt == NATT_ON
				&& sm_msg->message_msg.ike_sa_init.nat_sourceip
				&& sm_msg->message_msg.ike_sa_init.nat_destip) {

			ip_addr = sm_msg->message_msg.srcaddr;

			LOG_DEBUG("Received responder's address %s",
					netaddr_ip2str(ip_addr, buffer,
						INET6_ADDRSTRLEN));

			hash = g_malloc(sizeof(struct in_addr) +
					sizeof(guint16) + 2 * sizeof(guint64));

			digest = g_malloc(SHA_DIGEST_LENGTH);

			memcpy(hash, netaddr_get_ipaddr(ip_addr),
					sizeof(struct in_addr));
			hash_len = sizeof(struct in_addr);

			port = netaddr_get_port(ip_addr);
			LOG_DEBUG("Received port %u", port);

			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			i_spi = session_get_i_spi(session);
			r_spi = session_get_r_spi(session);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(guint64);

			/*
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);

			LOG_DEBUG("Computed hash");
			log_buffer(LOGGERNAME, (char *)digest,
					SHA_DIGEST_LENGTH);

			/*
			 * Comparing hashes...
			 *
			 * Actually, we should received only one hash of the
			 * source address from the peer!
			 */
			natt = TRUE;
			iterator = sm_msg->message_msg.ike_sa_init.nat_sourceip;
			while (iterator) {

				if (!memcmp(digest, iterator->data,
						SHA_DIGEST_LENGTH)) {
					natt = FALSE;
					break;
				}

				iterator = iterator->next;
			}

			if (natt) {
				session->natt = TRUE;

				LOG_NOTICE("Responder is behind a NAT");

				/*
				 * Add code to enable NAT traversal
				 */

			} else
				LOG_NOTICE("No NAT detected in front"
						" of responder");

			/*
			 * Checking whether initiator is behind NAT
			 */
			ip_addr = sm_msg->message_msg.dstaddr;

			LOG_DEBUG("Received initiator's address %s",
					netaddr_ip2str(ip_addr, buffer,
						INET6_ADDRSTRLEN));

			hash_len = 0;

			memcpy(hash, netaddr_get_ipaddr(ip_addr),
					sizeof(struct in_addr));
			hash_len += sizeof(struct in_addr);

			port = IKEV2_PORT;
			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);

			/*
			 * Hashing IP address, port and SPIs
			 */

			SHA1(hash, hash_len, digest);

			LOG_DEBUG("Computed hash");
			log_buffer(LOGGERNAME, (char *)digest,
					SHA_DIGEST_LENGTH);

			addr = sm_msg->message_msg.ike_sa_init.nat_destip;

			LOG_DEBUG("Received hash");
			log_buffer(LOGGERNAME, (char *)addr,
					SHA_DIGEST_LENGTH);

			if (memcmp(addr, digest, SHA_DIGEST_LENGTH)) {
				LOG_DEBUG("Initiator is behind a NAT");
				session->natt = TRUE;
				nat_keep_alive = TRUE;
				session->natt_keepalive = TRUE;

				if(session->cr->natt_keepalive == 0) {
					/**
					 * Setting default value for
					 * keepalive interval
					 */
					LOG_WARNING("NAT-T keepalive interval "
							"not defined. "
							"Defaulting to 60 "
							"seconds.");
					keepalive_tv.tv_sec = 60;
				} else
					keepalive_tv.tv_sec = session->cr->
								natt_keepalive;

				keepalive_tv.tv_usec = 0;
				session->keepalive_timeout = timeout_register(
						&keepalive_tv, 
						TO_RECURRING,
						session,
						message_send_keepalive_packet);
				

			} else {
				LOG_DEBUG("Initiator is not behind a NAT");
				session->natt_keepalive = FALSE;
			}

			g_free(hash);
			g_free(digest);
		}

		if (session->cr->natt == NATT_ON &&
				(!sm_msg->message_msg.ike_sa_init.nat_sourceip
				|| !sm_msg->message_msg.ike_sa_init.nat_destip))
			LOG_WARNING("Peer didn't sent NAT NOTIFY payload."
					" NAT-T turned off!");

		/*
		 * If NAT has been detected, transfer all the further
		 * communication to port 4500.
		 */
		if (session->natt) {
			netaddr_set_port(session_get_r_addr(session),
					IKEV2_PORT_NATT);
			netaddr_set_port(session_get_i_addr(session),
					IKEV2_PORT_NATT);
		}

#endif /* NATT */

		/*
		 * FIXME
		 * Check if we should switch to MOBIKE port (4500) and send
		 * MOBIKE_SUPPORTED notification
		 */
#ifdef MOBIKE
#ifdef NATT
		if (sm_msg->message_msg.ike_sa_init.nat_sourceip)
			mobike_port_flags |= 1;
		if (sm_msg->message_msg.ike_sa_init.nat_destip)
			mobike_port_flags |= 2;
		if (session->cr) {
			if (session->cr->natt == NATT_ON)
				mobike_port_flags |= 4;
		}
#endif /* NATT */
		// Check for MOBIKE-related support
		if (session->cp) {
			if (session->cp->mobike == MOBIKE_ON)
				mobike_port_flags |= 8;
		} else if (session->cr) {
			if (session->cr->mobike == MOBIKE_ON)
				mobike_port_flags |= 8;
		}
		if (mobike_port_flags & 0x8)
			session->mobike_supported = TRUE;
		/*
		 * Check if we should send IKE AUTH from port 4500
		 */
#if 0
		if (mobike_port_flags == 0xF) {
			LOG_DEBUG("Using MOBIKE port %d", MOBIKE_PORT);
			session->previous_port_i =
					netaddr_get_port(session_get_i_addr(session));
			session->previous_port_r =
					netaddr_get_port(session_get_r_addr(session));
			netaddr_set_port(session_get_r_addr(session),
					MOBIKE_PORT);
			netaddr_set_port(session_get_i_addr(session),
					MOBIKE_PORT);
		}
		else
			LOG_DEBUG("Not using MOBIKE port %d. "
					  "mobike_port_flags = %d", MOBIKE_PORT, mobike_port_flags);
#endif /* 0 */
#endif /* MOBIKE */

		/*
		 * Find intersection between proposals sent by peer and
		 * proposals defined in configuration file.
		 */
		proposal = proposal_intersect(
				session_get_proposals(session),
				sm_msg->message_msg.ike_sa_init.proposals,
				IKEV2_PROTOCOL_IKE);

		LOG_DEBUG("Dumping configured proposals for a peer");
		proposal_dump_proposal_list(LOGGERNAME,
				session_get_proposals(session));
		LOG_DEBUG("Dumping received proposal");
		proposal_dump_proposal_list(LOGGERNAME,
				sm_msg->message_msg.ike_sa_init.proposals);

		if (proposal == NULL) {
			/*
			 * Log an error and stop negotiation. The other end
			 * on purpose did not select one of the offered 
			 * proposals.
			 */
			LOG_ERROR("The other end selected"
					" unsupported proposal.");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

		LOG_DEBUG("Dumping selected proposal");
                proposal_dump_proposal(LOGGERNAME, proposal);

		/*
		 * Check that DH group peer sent is the same as we are
		 * using
		 */
		if (transform_get_id(session->dh_group) !=
			transform_get_id(proposal_find_transform(proposal,
						IKEV2_TRANSFORM_DH_GROUP,
						IKEV2_PROTOCOL_IKE))) {
			LOG_ERROR("The other end returned"
					" unsupported DH group.");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

		session_set_selected_proposal(session, proposal);

		/*
		 * Take specific proposals...
		 */
		session->prf_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_PRF,
						IKEV2_PROTOCOL_IKE);
		session->encr_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_ENCR,
						IKEV2_PROTOCOL_IKE);
		session->integ_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_INTEGRITY,
						IKEV2_PROTOCOL_IKE);

		session_crypto_material(session);

#ifdef CFG_CLIENT
		/*
		 * Check if initiator should request configuration data.
		 * If so, then prepare necessary data to be sent in
		 * request.
		 */
		if (session->cp->flags) {
			session->icfg = cfg_new();

			session->icfg->flags = session->cp->flags;
		}
#endif /* CFG_CLIENT */

	case IKE_SMI_AUTH_DL:

		if (sm_msg->msg_type == CERT_MSG) 
			LOG_DEBUG("STATE IKE_SMI_AUTH_DL, session = %p"
					", msg = %p", session, sm_msg);

		auth_failed = FALSE;

		/*
		 * Prepare authentication data to be sent to peer...
		 */
		session->auth_data->auth_type = session_get_auth_type(session);
		switch (session_get_auth_type(session)) {
		case IKEV2_AUTH_PSK:
			session->auth_data->key = psk_secret_find_by_id(
							session->cg->psk,
							session->cp->lid);

			if (session->auth_data->key == NULL) {
				LOG_ERROR("Could not find my authentication"
						" data");
				auth_failed = TRUE;
				break;
			}

			session->auth_data->key_len =
					strlen(session->auth_data->key);

			break;

		case IKEV2_AUTH_RSA:
			/**
			 * Prepare data for sending CERT payload(s) and AUTH
			 * payload:
			 * 
			 * Find appropriate certificates according to the
			 * received and already parsed CERTREQ payload(s).
			 *
			 * Load the auth_data for creating the AUTH payload
			 * from the first CERT payload that we are planning to
			 * send.
			 *
			 * Preparing the data for the CERTREQ(s) sending is
			 * independent of the authentication method and is
			 * done in the appropriate message_send_* functions.
			 */

			/**
			 * First check if all certification material is
			 * available. If not, initiate download and wait
			 * for CERT_MSG. At this point check is undertaken
			 * for CA items which our certificates are verified
			 * with.
			 */
			next_crl_update = session->next_crl_update;
			session->preserve_message = FALSE;

			/*
			 * Have we already initiated the download?
			 */
			if (sm_msg->msg_type == CERT_MSG) {

				/*
				 * Was the dl successful?
				 */
				if (sm_msg->cert_msg.success) {

					/*
					 * Retrieve message
					 */
					sm_msg = sm_msg->cert_msg.sm_msg;

					/*
					 * Callee will delete embedded msg too
					 */
					session->embedded_sm_msg = sm_msg;

				} else {

					LOG_ERROR("Download unsuccessful! "
							"Terminating...");
					sm_msg_free(sm_msg->cert_msg.sm_msg);
					auth_failed = TRUE;
					break;
				}
			}

			if (!cert_check_auth_material(session->cg->ca_list, 
					NULL,
					session, sm_msg, &next_crl_update)) {

				retval = 1;

				/*
				 * Pointer to message is embedded in
				 * dl request
				 */
				session->preserve_message = TRUE;
				session_set_state(session, IKE_SMI_AUTH_DL);
				goto out;
			}

			/* when cert material is present, write when we'll need updating */
			session->next_crl_update = next_crl_update;

			session->certs_for_peer = cert_find_appropriate_certs(
				sm_msg->message_msg.ike_sa_init.certreq_list,
				session->cg->cert_list,
				session->cg->ca_list,
				session->cert_lookup_supported,
				session);

			if (session->certs_for_peer == NULL) {
				LOG_ERROR("No certificates for peer.");

				/*
				 * Log the error and send Notify Authentication
				 * failed because we do not implement any
				 * out-of-band way of RSA key material.
				 */
				auth_failed = TRUE;
				break;
			}

			session->used_cas = g_slist_concat(session->used_cas, 
				cert_find_used_cas(session->cg->ca_list,
				session));

			/**
			 * The data from the first CERT payload (first element
			 * of the list session->certs_for_peer) is used to
			 * sign the AUTH payload.
			 */
			session->auth_data->priv_key_file =
					((struct cert_item *)session->certs_for_peer->data)->priv_key_file;
			session->auth_data->cert_type =
					((struct cert_item *)session->certs_for_peer->data)->type;

			if (session->auth_data->priv_key_file == NULL) {
				LOG_ERROR("Could not find my authentication "
					"data for creating the AUTH payload.");
				auth_failed = TRUE;
				break;
			}
			break;

		case IKEV2_AUTH_EAP:
#ifdef SUPPLICANT
			LOG_NOTICE("Using EAP to authenticate to a peer.");
			break;
#endif /* SUPPLICANT */

		case IKEV2_AUTH_DSS:
		case IKEV2_AUTH_ECDSS:
		default:
			LOG_ERROR("Unsupported authentication type!");
			auth_failed = TRUE;
			break;
		}

		if (auth_failed) {
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

		si = session_get_sad_item(session);

		LOG_DEBUG("CHILD SA configured encr transforms");
		transform_list_dump(LOGGERNAME,
				sad_item_get_encr_algs(si));

		LOG_DEBUG("CHILD SA configured auth transforms");
		transform_list_dump(LOGGERNAME,
				sad_item_get_auth_algs(si));

		sad_item_set_spi_size(si, sizeof(guint32));
		sad_getspi(si, session_get_r_addr(session),
				session_get_i_addr(session));

		if (!sad_item_get_spi(si))
			g_assert_not_reached();

		sad_item_set_proposals(si,
			proposal_list_create(
				sad_item_get_encr_algs(si),
				sad_item_get_auth_algs(si),
				sad_item_get_protocol(si),
				sad_item_get_spi_size(si),
				sad_item_get_spi(si)));

		LOG_DEBUG("CHILD SA offered proposals");
		proposal_dump_proposal_list(LOGGERNAME,
				sad_item_get_proposals(si));

		LOG_DEBUG("Starting IKE AUTH exchange");

		session_set_state(session, IKE_SMI_AUTH_PEER);

		message_send_ike_auth_i(session);

		/*
		 * This is a time we will mark as INITIATOR IKE SA beginning, so
		 * take a time stamp.
		 */
		gettimeofday(&tv, NULL);
		session->create_time = tv.tv_sec;
		session->rekey_time = tv.tv_sec;

		break;

	case IKE_SMI_AUTH_PEER:

		LOG_DEBUG("STATE IKE_SMI_AUTH_PEER");

		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_auth.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

		/*
		 * Check if we received notify AUTHENTICATION_FAILED. In that
		 * case terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_auth.authentication_failed) {
			LOG_ERROR("Peer sent AUTHENTICATION_FAILED notify");
			retval = -1;
			goto out;
		}

		auth_data.auth_type = sm_msg->message_msg.ike_auth.auth_type;

		/*
		 * Take received ID for later...
		 */
		session->peer_id = sm_msg->message_msg.ike_auth.r_id;
		sm_msg->message_msg.ike_auth.r_id = NULL;

#ifdef MOBIKE
		/*
		 * Check if we sent N_MOBIKE_SUPPORTED notify payload
		 * and if peer sent the same payload back
		 */
		if (session->mobike_supported == TRUE) {
			if (!sm_msg->message_msg.ike_auth.peer_supports_mobike) {
				session->mobike_supported = FALSE;

				/*
				 * Switch back to whatever ports where used before
				 */
#if 0
				netaddr_set_port(session_get_r_addr(session),
						session->previous_port_r);
				netaddr_set_port(session_get_i_addr(session),
						session->previous_port_i);
#endif /* 0 */
			}
		}
#endif /* MOBIKE */

	case IKE_SMI_INSTALLCSA_DL:

		auth_failed = FALSE;

		/*
		 * Have we already initiated cert material download?
		 */
		if (sm_msg->msg_type == CERT_MSG) {
			LOG_DEBUG("STATE IKE_SMI_INSTALLCSA_DL");
			auth_data.auth_type = IKEV2_AUTH_RSA;
		}

		/*
		 * Check that peer sent valid credentials...
		 */
		switch (auth_data.auth_type) {
		case IKEV2_AUTH_PSK:
			auth_data.key = psk_secret_find_by_id(
						session->cg->psk,
						session->peer_id);

			if (auth_data.key == NULL) {
				LOG_ERROR("Could not find peer's authentication"
						" data");
				auth_failed = TRUE;
			} else
				auth_data.key_len = strlen(auth_data.key);

			break;

		case IKEV2_AUTH_RSA:

			/** 
			 * Peer authenticates with certificate based
			 * authentication. Verify his AUTH payload. (CERT
			 * payload(s) are already verified.) 
			 *
			 * Received certificates are stored in the
			 * message_msg->cert_list. If we have configured at
			 * least one CA, we try to verify that certificate(s),
			 * no matter if we are using PSK or RSA authentication.
			 *
			 * If we posses peer's certificate(s) stored locally,
			 * the received CERT payload(s) are ignored.
			 */

			/**
			 * First check if all certification material is
			 * available. If not, initiate download and wait
			 * for CERT_MSG. At this point check is undertaken
			 * for CA items which are defined for this peer.
			 */

			next_crl_update = session->next_crl_update;
			session->preserve_message = FALSE;

			/*
			 * Have we already initiated the download?
			 */
			if (sm_msg->msg_type == CERT_MSG) {
				/*
				 * Was the dl successful?
				 */
				if (sm_msg->cert_msg.success) {

					/*
					 * Retrieve message
					 */
					sm_msg = sm_msg->cert_msg.sm_msg;

					/*
					 * Callee will delete embedded msg too
					 */
					session->embedded_sm_msg = sm_msg;

				} else {
					LOG_ERROR("Download unsuccessful! Terminating...");
					sm_msg_free(sm_msg->cert_msg.sm_msg);
					auth_failed = TRUE;
					break;
				}
			}

			if (!cert_check_auth_material(NULL, 
					session->cp->ca_list,
					session, sm_msg, &next_crl_update)) {

				retval = 1;

				/*
				 * Pointer to message is embedded in dl request
				 */
				session->preserve_message = TRUE;
				session_set_state(session, IKE_SMI_INSTALLCSA_DL);
				goto out;
			}

			/*
			 * When cert material is present, write when
			 * we'll need updating
			 */
			session->next_crl_update = next_crl_update;

			ca_items = session->cp->ca_list;
			if (ca_items == NULL) {
				LOG_ERROR("CA certificates associated to "
						"peer's identity not found!");
				auth_failed = TRUE;
				break;
			}

			peers_cert_items = session->cp->cert_list;
			if (peers_cert_items != NULL) {

				verified_cert_items = 
					cert_prepare_cert_item_verify(
						peers_cert_items,
						ca_items,
						session);

				if (verified_cert_items == NULL) {
					LOG_ERROR("Couldn't verify peer's "
						"local certificate material!");
					auth_failed = TRUE;
					break;
				}

				if (!cert_check_public_keys(NULL, 
						peers_cert_items)) {
					LOG_ERROR("Local peer's certs do not "
						"have the same public key!");
					auth_failed = TRUE;
					break;
				}

				/*
				 * In case of verifing received AUTH payload,
				 * auth_data contains the public key from the
				 * first local peer's certificate.
				 */
				cert_file = ((struct cert_item*)
					session->cp->cert_list->data)->file; 

				auth_data.public_key_len = 
					crypto_read_pubkey_from_file(cert_file, 
						&(auth_data.public_key));

				if ((verified_cert_items == NULL) || 
					(auth_data.public_key == NULL)) {
					LOG_ERROR("Error reading public key "
						"from peer's certificate!");
					auth_failed = TRUE;
					break;
				}

				session->peer_local_certs = 
					verified_cert_items;
			}

			if ((peers_cert_items == NULL) 
					|| (verified_cert_items == NULL)) {

				verified_certs = cert_prepare_cert_verify(
					sm_msg->message_msg.ike_auth.cert_list,
					ca_items,
					session);

				if (verified_certs == NULL) {
					LOG_ERROR("Error verifying peer's certs "
						"in CERT payload(s)!");
					auth_failed = TRUE;
					break;
				}

				if (!cert_check_public_keys(
					sm_msg->message_msg.ike_auth.cert_list,
					NULL)) {
					LOG_ERROR("Payload peer's certs do not"
						" have the same public key!");
					auth_failed = TRUE;
					break;
				}

				session->peer_payload_certs = verified_certs;

				/*
				 * In case of verifing received AUTH payload,
				 * auth_data contains the public key from the
				 * first CERT payload.
				 */
				LOG_DEBUG("auth_data.public_key = %p",
						auth_data.public_key);
				auth_data.public_key = 
					((struct cert *)sm_msg->message_msg.ike_auth.cert_list->data)->pub_key;
				auth_data.public_key_len = 
					((struct cert *)sm_msg->message_msg.ike_auth.cert_list->data)->pub_key_len;
			}

			session->used_cas = g_slist_concat(session->used_cas, 
				cert_find_used_cas(session->cp->ca_list,
				session));

			/*
			 * Check if identity from ID payload matches the id
			 * from the Subject/SubjectAltName certificate field. 
			 */
			if ((!session->cp->options & OPTION_NO_ID_VERIFY) ||
					session->peer_id->type == 
						IKEV2_IDT_DER_ASN1_DN) {
				LOG_DEBUG("ID verification");

				auth_failed = !crypto_verify_id(
							verified_cert_items,
							verified_certs,
							session->peer_id);
				if (auth_failed) 
					LOG_ERROR("id from ID payload "
							"does not match id "
							"from certificate!");
			} else
				LOG_DEBUG("Skipping ID verification!");

			break;

		case IKEV2_AUTH_DSS:
		case IKEV2_AUTH_ECDSS:
		default:
			LOG_ERROR("Unsupported authentication type %u!",
					auth_data.auth_type);
			auth_failed = TRUE;
		}

		if (!auth_failed)
			auth_failed = auth_data_verify(session->prf_type,
				&auth_data,
				session->peer_id,
				session->ikesa_init_r,
				session->ikesa_init_r_len,
				session->r_nonce,
				session->sk_pr,
				sm_msg->message_msg.ike_auth.auth_payload,
				sm_msg->message_msg.ike_auth.auth_payload_len);

		if (auth_failed) {
			LOG_ERROR("Peer authentication failed. "
					"Deleting session.");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

#ifdef SUPPLICANT
		/*
		 * If we are using EAP then we just verified AUTH payload
		 * Next, process received EAP packet and send response;
		 *
		 * If we are not using EAP then we issue goto statement
		 * to finish IKE SA and first CHILD SA establishment.
		 *
		 * TODO: See if we can remove this goto statement!
		 */
		if (session_get_auth_type(session) != IKEV2_AUTH_EAP)
			goto install_csa;

		if (auth_data.auth_type != IKEV2_AUTH_RSA)
			LOG_WARNING("According to RFC4306 Responder MUST use "
					"certificates to authenticate itself");

	case IKE_SMI_EAP:

		if (session_get_state(session) == IKE_SMI_EAP) {
			LOG_DEBUG("STATE IKE_SMI_EAP");

			if (sm_msg->message_msg.ike_auth.invalid_syntax) {
				LOG_ERROR("Peer sent INVALID_SYNTAX notify");
				session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
				break;
			}

			if (sm_msg->message_msg.ike_auth.authentication_failed){
				LOG_ERROR("Peer sent AUTHENTICATION_FAILED "
						"notify");
				retval = -1;
				goto out;
			}
		}

		/*
		 * If no EAP packet is supplied we dont need to process this 
		 * trough supplicant SM.
		 */
		if (!sm_msg->message_msg.ike_auth.eap) {
			LOG_ERROR("Peer didn't send EAP packet! "
					"EAP authentication FAILED");

			sm_msg->message_msg.notify =
					N_AUTHENTICATION_FAILED;
			message_send_notify(session,
					&sm_msg->message_msg);

			session_set_state(session, IKE_SM_DYING);

			break;
		}
		 
		if (!session->sd) {
			if ((session->sd = supplicant_new(
					session->cp->wpa_conf)) == NULL) {
				session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
				break;
			}

			/*
			 * If there is ID payload in a message, save it for
			 * later...
			 */
			if (sm_msg->message_msg.ike_auth.r_id) {
				session->peer_id =
					sm_msg->message_msg.ike_auth.r_id;
				sm_msg->message_msg.ike_auth.r_id = NULL;
			}
		}

		supplicant_process_request(session->sd, 
				sm_msg->message_msg.ike_auth.eap,
				sm_msg->message_msg.ike_auth.eap_len);

		/*
		 * TODO
		 * Realize what is happening if supplicant sends
		 * success or failure message (last response form supplicant
		 * to responder) message_send_ike_eap_i() 
		 */

		if (supplicant_is_success(session->sd)) {
			LOG_DEBUG("EAP authentication completed successfuly");
			
			if (supplicant_msk_get(session->sd,
					&session->auth_data->key,
					&session->auth_data->key_len)) {
				LOG_DEBUG("EAP_MSK len=%d",
						session->auth_data->key_len);
				log_buffer(LOGGERNAME,
						(char *)session->auth_data->key,
						session->auth_data->key_len);
			} else {

				LOG_WARNING("EAP method used does not "
						"generate MSK. This is "
						"not recomended!");

				session->auth_data->key_len =
						transform_get_key_len(
							session->prf_type);
				session->auth_data->key = session->sk_pi;
			}
			message_send_ike_auth_final_i(session);
						
			session_set_state(session, IKE_SMI_INSTALLCSA);

		} else if (supplicant_is_failure(session->sd) || 
				!supplicant_get_response(session->sd)) {
			/*
			 * If EAP Failed, marked by EAPOL_eapFail, or
			 * if EAP SM didn't have any data to send back ->
			 * this is not acceptable!
			 * FIXME: Why EAP SM don't go to state FAILURE, even
			 * when received EAP-Fail packet?
			 */

			LOG_ERROR("EAP authentication FAILED");

			/*
			 * if EAP failed, but from responder didn't got EAP-Fail
			 * send him NOTIFY
			 */
			if (!sm_msg->message_msg.ike_auth.eap ||
				*((guint8 *) sm_msg->message_msg.ike_auth.eap)
					!= 4)
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));

		} else {

			/*
			 * EAP not finished yet, send reply and wait for next
			 * response
			 */
			session_set_state(session, IKE_SMI_EAP);
			message_send_ike_eap_i(session);
		}

		break;

	case IKE_SMI_INSTALLCSA:
		LOG_DEBUG("STATE IKE_SMI_INSTALLCSA");
		
		/*
		 * Check if we received notify INVALID_SYNTAX. In that case
		 * terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_auth.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
			break;
		}

		/*
		 * Check if we received notify AUTHENTICATION_FAILED. In that
		 * case terminate IKE SA..
		 */
		if (sm_msg->message_msg.ike_auth.authentication_failed) {
			LOG_ERROR("Peer sent AUTHENTICATION_FAILED notify");
			retval = -1;
			goto out;
		}

		/*
		 * Check last received AUTH payload with MSK or 'SK_pr'
		 */
		auth_data.auth_type = IKEV2_AUTH_EAP;

		if (!supplicant_msk_get(session->sd, &auth_data.key,
				&auth_data.key_len)) {
			LOG_DEBUG("Using SK_pr to authenticate peer");
			auth_data.key = session->sk_pr;
			auth_data.key_len =
					transform_get_key_len(
						session->prf_type);
		}
		
               	auth_failed = auth_data_verify(session->prf_type,
                		&auth_data,
				session->peer_id,
				session->ikesa_init_r,
				session->ikesa_init_r_len,
				session->r_nonce,
				session->sk_pr,  
				sm_msg->message_msg.ike_auth.auth_payload,
				sm_msg->message_msg.ike_auth.auth_payload_len);

                if (auth_failed) {
                	LOG_ERROR("Peer authentication FAILED. "
                			"Deleting session.");   
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));
                	break;
		}

		/*
		 * Second part of previous 'IKE_SMI_INSTALLCSA' state
		 */
install_csa:
#endif /* SUPPLICANT */
		/*
		 * Free data from first exchange
		 */
		g_free(session->ikesa_init_r);
		session->ikesa_init_r = NULL;

		/*
		 * Limit session SA auth time to cert validity 
		 * (if RSA used) - send verified_cert_items and 
		 * verified_certs to iterate all certs
		 */
		if (session->cp->auth_limits)
			session->auth_limit_time =
					session->cp->auth_limits->time;
		else
			session->auth_limit_time = 0;

		if (auth_data.auth_type == IKEV2_AUTH_RSA) {
			session->auth_limit_time =
					cert_sa_limit_adjust(
						session->create_time,
						session->auth_limit_time,
						verified_cert_items,
						verified_certs);
		}

#warning "Free auth_data structure"

		session_set_state(session, IKE_SM_MATURE);

		si = session_get_sad_item(session);

#ifdef CFG_CLIENT
		/*
		 * Check if we received notify INTERNAL_ADDRESS_FAILURE. In
		 * that case do not establish CHILD SA and delete IKE SA.
		 * Also, if we sent CFG request, and there is no CFG
		 * response, terminate session.
		 */
		if (sm_msg->message_msg.ike_auth.internal_address_failure
				|| (session->icfg &&
					!sm_msg->message_msg.ike_auth.cfg)) {
			LOG_ERROR("Peer didn't respond to CFG request!"
					" Aborting!");

			session_sad_item_remove(session, si);
			sad_item_free(si);

			cfg_free(session->icfg);
			session->icfg = NULL;

			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_TERMINATE));

			break;
		}

		/*
		 * Check if we expected _and_ received CFG payload. If
		 * so do the necessary processing, but postpone the
		 * error notification for later, when we process any
		 * received proposal. This is necessary so that we
		 * know if we should send CHILD SA delete notify or
		 * not.
		 */
		if (session->icfg &&
				sm_msg->message_msg.ike_auth.cfg) {

			cfg_dump(sm_msg->message_msg.ike_auth.cfg);

			cfg_free(session->icfg);
			session->icfg = sm_msg->message_msg.ike_auth.cfg;
			sm_msg->message_msg.ike_auth.cfg = NULL;

			vpngw = netaddr_dup(session_get_r_addr(session));
			netaddr_set_port(vpngw, 0);

			vpnif = netif_find_by_addr(session_get_i_addr(session));

			if (cfg_client_exec_script(session->cp->up_argv,
					session->icfg, session->cp->lid,
					vpngw, netif_get_name(vpnif)) < 0) {

				cfg_free(session->icfg);
				session->icfg = NULL;
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
				break;
			}
		}

		if (!session->icfg &&
				sm_msg->message_msg.ike_auth.cfg)
			LOG_WARNING("Received CFG payload though we did not"
					" requested it! Ignoring");

#endif /* CFG_CLIENT */

		/*
		 * Check if we received notify TS_UNACCEPTABLE. In that case
		 * terminate CHILD SA, log an error and stop processing...
		 */
		if (sm_msg->message_msg.ike_auth.ts_unacceptable) {
			LOG_ERROR("Peer sent TC_UNACCEPTABLE notify");

			session_sad_item_remove(session, si);
			sad_item_free(si);
			break;
		}

		/*
		 * Check if we received notify NO_PROPOSAL_CHOSEN. In that case
		 * terminate further processing but leave IKE SA...
		 */
		if (sm_msg->message_msg.ike_auth.no_proposal_chosen) {
			LOG_ERROR("Peer sent NO_PROPOSAL_CHOSEN notify");

			session_sad_item_remove(session, si);
			sad_item_free(si);
			break;
		}

		/*
		 * Check if peer selected valid proposal, in case it tries
		 * to cheet...
		 */
		proposal = proposal_intersect(
				sm_msg->message_msg.ike_auth.proposals,
				sad_item_get_proposals(si),
				sad_item_get_protocol(si));

		LOG_DEBUG("LOCAL proposals");
		proposal_dump_proposal_list(LOGGERNAME,
					sad_item_get_proposals(si));
		LOG_DEBUG("PEER's proposals");
		proposal_dump_proposal_list(LOGGERNAME,
				sm_msg->message_msg.ike_auth.proposals);
		LOG_DEBUG("RESULT proposal");
		proposal_dump_proposal(LOGGERNAME, proposal);

		if (proposal == NULL) {
			/*
			 * Log an error. Peer IKE daemon did not select one of
			 * the offered proposals! Log an error and send
			 * delete notify to treminate CHILD SA.
			 */
			LOG_ERROR("The other and selected unsupported"
					" proposal.");
			sad_item_set_state(si, SA_STATE_DEAD);
			session_msg_push(session,
					sm_session_msg_new(MSG_SA_DELETE));
			break;
		}

		sad_item_set_spi_size(si, proposal_get_spi_size(proposal));
		sad_item_set_peer_spi(si, proposal_get_spi(proposal));

		/*
		 * Free all proposals excepts selected one...
		 */
		sad_item_set_proposal(si, proposal);

		/*
		 * Check that peer selected proposed SA mode. If not, send
		 * delete notify to remove CHILD SA!
		 */
		if (sm_msg->message_msg.ike_auth.transport_mode) {

			if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
				LOG_ERROR("Responder requested transport mode");
				sad_item_set_state(si, SA_STATE_DEAD);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_SA_DELETE));
				break;
			}

		} else {

			if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TRANSPORT) {
				LOG_ERROR("Responder requested tunnel mode");
				sad_item_set_state(si, SA_STATE_DEAD);
				session_msg_push(session,
						sm_session_msg_new(
							MSG_SA_DELETE));
				goto out;
			}

		}

#warning "Move to sad.c"
		si->encr = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_ENCR,
						sad_item_get_protocol(si));
		si->auth = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_INTEGRITY,
						sad_item_get_protocol(si));

		proposal_dump_proposal_list(LOGGERNAME,
				sad_item_get_proposals(si));

		/*
		 * Replace traffic resulting traffic selectors from
		 * intersection of configuration file, peer's offer,
		 * and kernel's policy.
		 *
		 * Call to function sad_item_set_ts will fail if
		 * given traffic selectors are not a subset of the
		 * existing ones.
		 */
		if (sad_item_set_ts(si, sm_msg->message_msg.ike_auth.tsi,
				sm_msg->message_msg.ike_auth.tsr) < 0) {

			LOG_ERROR("Peer sent illegal traffic selectors!");

			LOG_DEBUG("TSi value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));

			LOG_DEBUG("TSr value");
			ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));

			LOG_DEBUG("message->tsr");
			ts_list_dump(LOGGERNAME,
					sm_msg->message_msg.ike_auth.tsr);

			LOG_DEBUG("message->tsi");
			ts_list_dump(LOGGERNAME,
					sm_msg->message_msg.ike_auth.tsi);

			/*
			 * Do not remove IKE SA, but put CHILD SA into queue
			 * for deletion...
			 */
			sad_item_set_state(si, SA_STATE_DEAD);
			session_msg_push(session,
					sm_session_msg_new(MSG_SA_DELETE));

			break;

		}

		sm_msg->message_msg.ike_auth.tsr = NULL;
		sm_msg->message_msg.ike_auth.tsi = NULL;

		sad_item_compute_keys(si, session->sk_d,
				session->i_nonce, session->r_nonce,
				session->prf_type,
				session_get_ike_type(session) ==
					IKEV2_INITIATOR);

		sad_add(si, session_get_i_addr(session),
				session_get_r_addr(session),
				session->cg->kernel_spd != KERNEL_SPD_ROSYNC,
				session->natt);

		sad_item_remove_transient(si);

		/*
		 * We just made CHILD SA negotiations so increase number of
		 * allocations done in this IKE SA
		 */
		session->allocations++;

		sad_item_set_state(si, SA_STATE_MATURE);

		LOG_DEBUG("child_sa local ts dump");
		ts_list_dump(LOGGERNAME, sad_item_get_tsl(si));
		LOG_DEBUG("child_sa remote ts dump");
		ts_list_dump(LOGGERNAME, sad_item_get_tsr(si));

		/*
		 * Check if this session structure is reauthentification of
		 * an existing session, and if so then continue transfer of a
		 * CHILD SAs...
		 */
		if (session->session) {
			/*
			 * If we have done reauthentication then remove flag
			 * that the session is user initated, otherwise,
			 * when we receive delete response, ikev2 will assume
			 * the only session is terminated and try to exit!
			 */
			if (session_get_user_initiated(session)) {
				session_set_user_initiated(session, FALSE);
				session_set_user_initiated(session->session,
					TRUE);
			}

			session_lock(session->session);
			session_msg_push(session,
					sm_session_msg_new(MSG_SA_TRANSFER));
		}
		
		session_transient_free(session);

		break;

	default:
		LOG_DEBUG("Session in unrecognized state %u",
				session_get_state(session));
		break;
	}

	retval = 0;

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * Main function for responder during IKE_SA_INIT and IKE_AUTH exchanges.
 *
 * \return	0 if everything went OK
 *		1 if there was error but nothing has to be done further
 *		-1 in case of an error and session structure has to be removed
 */
int sm_ike_r_thread(struct session *session, union sm_msg *sm_msg)
{
	struct timeval tv;
	struct message_msg *msg;
	proposal_t *proposal;
	struct sad_item *si;
	gint32 auth_failed;
	unsigned char *nonce;
	struct auth_data auth_data;
	char ip_buf[INET6_ADDRSTRLEN], *s;
	int retval;

	/*
	 * Certificate variables
	 */
	GSList *verified_certs = NULL, *verified_cert_items = NULL;
	GSList *peers_cert_items = NULL;
	GSList *ca_items = NULL;
	unsigned char *cert_file;
	time_t next_crl_update;

#ifdef NATT
	/*
	 * NAT variables
	 */
	GSList *iterator;
	netaddr_t *ip_addr;
	unsigned char *hash, *digest, *addr;
	gint32 hash_len = 0;
	guint64 i_spi, r_spi;
	gboolean keep_alive, natt;
	guint16 port;
	struct timeval keepalive_tv;
	char buffer[20];
#endif /* NATT */

#ifdef AAA_CLIENT
	/*
	 * AAA variables
	 */
	int aaa_eap_timeout;
	int aaa_status = AAA_ERROR;
	gchar *uname = NULL;
#endif /* AAA_CLIENT */

#ifdef CFG_MODULE
	/*
	 * CFG based variables
	 */
	GSList *c, *tsl, *tsr;
	gint ret;
	struct netaddr *clntaddr, *vpngwaddr;
	struct netif *clntif;
	struct ts *ts;
#endif /* CFG_MODULE */

	LOG_FUNC_START(1);

	retval = -1;

	/*
	 * Note that we are conditionally leavinig state machine processing
	 * on certain places and after returning we have to reinitalize
	 * variables as we never left the state machine! Thus, all such
	 * initializations should be placed here.
	 *
	 * Find corresponding CHILD SA structure
	 */
	si = session_get_sad_item(session);

	/*
	 * In most cases we enter here because we received message from
	 * the peer. If the reason to enter this function is something
	 * else, then appropriate initalization MUST be done!
	 */
	msg = &sm_msg->message_msg;

	switch (session_get_state(session)) {
	case IKE_SMR_INIT:

		LOG_DEBUG("STATE IKE_SMR_INIT");

		if (!session->cr) {
			/*
			 * Find specific configuration data for peer.
			 */
			g_static_rw_lock_reader_lock(&(
						ikev2_data->config_lock));
			session->cr = config_remote_find_by_addr(
						ikev2_data->config,
						session_get_i_addr(session));
			if (session->cr) {
				g_atomic_int_inc(&(session->cr->refcnt));

				session->cg = ikev2_data->config->cg;
				g_atomic_int_inc(&(session->cg->refcnt));
			}

			g_static_rw_lock_reader_unlock(
					&(ikev2_data->config_lock));

			if (session->cr == NULL) {
				LOG_ERROR("Could not find configuration"
						" for destination %s",
					netaddr_ip2str(
						session_get_i_addr(session),
						ip_buf, INET6_ADDRSTRLEN));
				break;
			}

			LOG_NOTICE("Selected REMOTE section at line %u\n",
					session->cr->cfg_line);
		}

		/*
		 * Take a copy of a packet for later authentication
		 * phase...
		 */
		session->ikesa_init_i = g_memdup(msg->buffer, msg->size);
		session->ikesa_init_i_len = msg->size;

#ifdef NATT
		/*
		 * Check if NAT-T is enabled and what peer sent to us.
		 * There are follwing possiblities:
		 *	NATT_ON
		 *		Initiator didn't sent NAT notify paylaod
		 *			Signal an error!
		 *		Initiator sent NAT notify payload
		 *			Process it!
		 *
		 *	NATT_PASSIVE
		 *		Initiator didn't sent NAT notify paylaod
		 *			Turn off NAT-T traversal
		 *		Initiator sent NAT notify payload
		 *			Process it!
		 *
		 *	NATT_OFF
		 *		Ignore client's NAT payload
		 */
		natt = FALSE;
		if ((session->cr->natt == NATT_ON ||
				session->cr->natt == NATT_PASSIVE)
				&& msg->ike_sa_init.nat_sourceip
				&& msg->ike_sa_init.nat_destip) {

			ip_addr = msg->srcaddr;

			LOG_TRACE("Received initiator's address %s",
					netaddr_ip2str(ip_addr, buffer, 20));

			hash = g_malloc(sizeof(struct in_addr) + 
					sizeof(guint16) +
					2 * sizeof(guint64));

			digest = g_malloc(SHA_DIGEST_LENGTH);

			/*
			 * First check if initiator is behind a NAT
			 */
			memcpy(hash, netaddr_get_ipaddr(ip_addr),
					sizeof(struct in_addr));
			hash_len = sizeof(struct in_addr);

			port = netaddr_get_port(ip_addr);
			LOG_TRACE("Source port %u", port);

			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			i_spi = session_get_i_spi(session);
			r_spi = session_get_r_spi(session);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);


			/*
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);

			LOG_DEBUG("Computed digest");
			log_buffer(LOGGERNAME, (char *)digest,
					SHA_DIGEST_LENGTH);

			iterator = msg->ike_sa_init.nat_sourceip;

			/*
			 * Comparing hashes...
			 */
			natt = TRUE;
			while (iterator) {

				addr = iterator->data;

				if (!memcmp(addr, digest, SHA_DIGEST_LENGTH)) {
					natt = FALSE;
					break;
				}

				iterator = iterator->next;
			}

			if (natt) {
				LOG_NOTICE("Initiator is behind a NAT");
				session->natt = TRUE;
			}

			/*
			 * Checking whether responder is behind a NAT
			 */
			ip_addr = msg->dstaddr;
			LOG_TRACE("Received responder address %s",
					netaddr_ip2str(ip_addr, buffer, 20));
			hash_len = 0;

			memcpy(hash, netaddr_get_ipaddr(ip_addr),
					sizeof(struct in_addr));
			hash_len += sizeof(struct in_addr);

			port = IKEV2_PORT;
			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);

			/*
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);

			addr = msg->ike_sa_init.nat_destip;

			LOG_DEBUG("Received digest");
			log_buffer(LOGGERNAME, (char *)addr,
					SHA_DIGEST_LENGTH);

			LOG_DEBUG("Computed digest");
			log_buffer(LOGGERNAME, (char *)digest,
					SHA_DIGEST_LENGTH);

			if (memcmp(addr, digest, SHA_DIGEST_LENGTH)) {
				LOG_DEBUG("NAT detected in front of Responder");
				keep_alive = TRUE;
				session->natt = TRUE;

				if (session->cr->natt_keepalive == 0) {
					/**
					 * Setting default value for
					 * keepalive interval
					 */
					keepalive_tv.tv_sec = 1;
				}
				else {
					keepalive_tv.tv_sec = session->cr->
								natt_keepalive;
				}

				keepalive_tv.tv_usec = 0;
				session->keepalive_timeout = timeout_register(
						&keepalive_tv,
						TO_RECURRING,
						session,
						message_send_keepalive_packet);
			} else {
				LOG_DEBUG("Responder is not a behind a NAT");
				session->natt_keepalive = FALSE;
			}

			g_free(digest);
			g_free(hash);
		}

		if (session->cr->natt == NATT_ON &&
				(!msg->ike_sa_init.nat_sourceip
				&& !msg->ike_sa_init.nat_destip))
			LOG_WARNING("NAT-T turned on and initiator didn't sent"
					" NAT payload");

		/*
		 * If NAT has been detected, transfer all the further
		 * communication to port 4500.
		 */
		if (session->natt)
			netaddr_set_port(session_get_r_addr(session),
					IKEV2_PORT_NATT);
#endif /* NATT */

		/*
		 * Find intersection between proposals sent by peer
		 * and proposals defined in configuration file.
		 */
		proposal = proposal_intersect(
					session_get_proposals(session),
					msg->ike_sa_init.proposals,
					IKEV2_PROTOCOL_IKE);

		/*
		 * In case there is no common proposal selected send NOTIFY
		 * and commence into state for terminating session...
		 */
		if (proposal == NULL) {
			LOG_ERROR("No proposal chosen! Sending NOTIFY!");
			msg->notify = N_NO_PROPOSAL_CHOSEN;
			message_send_notify(session, msg);
			break;
		}

		session->dh_group = proposal_find_transform(
						proposal,
						IKEV2_TRANSFORM_DH_GROUP,
						IKEV2_PROTOCOL_IKE);

		if (transform_get_id(session->dh_group) !=
				msg->ike_sa_init.peer_dh_group) {
			LOG_ERROR("Peer selected different DH group!");
			msg->notify = N_INVALID_KE_PAYLOAD;
			msg->notify_data16 =
					transform_get_id(session->dh_group);
			message_send_notify(session, msg);

			break;
		}

		session->prf_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_PRF,
						IKEV2_PROTOCOL_IKE);

		session->encr_type = proposal_find_transform(proposal,
						IKEV2_TRANSFORM_ENCR,
						IKEV2_PROTOCOL_IKE);

		session->integ_type = proposal_find_transform(
						proposal,
						IKEV2_TRANSFORM_INTEGRITY,
						IKEV2_PROTOCOL_IKE);

		session_set_selected_proposal(session, proposal);

		session->pub_key = msg->ike_sa_init.peer_pub_key;
		msg->ike_sa_init.peer_pub_key = NULL;

		session->i_nonce = msg->ike_sa_init.peer_nonce;
		msg->ike_sa_init.peer_nonce = NULL;

		session->integ_type->key_len =
				integ_keylen_get(session->integ_type);

		LOG_DEBUG("Selected proposal");
		proposal_dump_proposal(LOGGERNAME, proposal);

		/*
		 * Generate responder SPI...
		 * We ignore distinction between 0 and 1 return values.
		 * See the man page for meaning of those values...
		 */
		if (!rand_bytes((unsigned char *)&(session->r_spi),
						sizeof(session->r_spi))) {
			LOG_ERROR("Error generating random values");
			return -1;
		}

		LOG_DEBUG("R_SPI:");
		log_data(LOGGERNAME, (char *)&(session->r_spi),
						sizeof(session->r_spi));

#ifdef NATT
		/*
		 * NAT support.
		 * Preparing hashes to be sent to the initiator...
		 */
		session->nat_src_hashes = NULL;
		session->nat_dest_hashes = NULL;

		if ((session->cr->natt == NATT_ON ||
				session->cr->natt == NATT_PASSIVE)
				&& msg->ike_sa_init.nat_sourceip
				&& msg->ike_sa_init.nat_destip) {

			i_spi = session_get_i_spi(session);
			r_spi = session_get_r_spi(session);

			hash = g_malloc(sizeof(struct in_addr) +
					sizeof(guint16) +
					2 * sizeof(guint64));

			ip_addr = session_get_r_addr(session);
			hash_len = 0;

			memcpy(hash, netaddr_get_ipaddr(ip_addr),
					sizeof(struct in_addr));
			hash_len += sizeof(struct in_addr);

			port = IKEV2_PORT;
			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);

			digest = g_malloc(SHA_DIGEST_LENGTH);

			/*
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);

			session_nat_hash_append(session, digest);

			ip_addr = session_get_i_addr(session);
			hash_len = 0;

			memcpy(hash, netaddr_get_ipaddr(ip_addr), 
					sizeof(struct in_addr));
			hash_len += sizeof(struct in_addr);

			port = netaddr_get_port(ip_addr);
			LOG_DEBUG("Source port %u", port);

			memcpy(hash + hash_len, &port, sizeof(port));
			hash_len += sizeof(port);

			memcpy(hash + hash_len, &i_spi, sizeof(i_spi));
			hash_len += sizeof(i_spi);

			memcpy(hash + hash_len, &r_spi, sizeof(r_spi));
			hash_len += sizeof(r_spi);

			digest = g_malloc(SHA_DIGEST_LENGTH);

			/* 
			 * Hashing IP address, port and SPIs
			 */
			SHA1(hash, hash_len, digest);

			session->nat_dest_hashes = digest;

			g_free(hash);
		}
#endif /* NATT */

		/*
		 * Generate KE value
		 */
		session->dh = dh_ke_generate(
					transform_get_id(session->dh_group));

		/*
		 * Generate NONCE value
		 */
		nonce = g_malloc(session->cr->nonce_size);

		if (!rand_bytes(nonce, session->cr->nonce_size)) {
			LOG_ERROR("Error generating random value");
			g_free(nonce);
			goto out;
		}

		session->r_nonce = BN_bin2bn(nonce, session->cr->nonce_size,
						NULL);

		g_free(nonce);

		message_send_ike_sa_init_r(session, msg);

		session_crypto_material(session);

		/*
		 * This is a time we will mark as RESPONDER IKE SA beginning, so
		 * take a time stamp.
		 */
		gettimeofday(&tv, NULL);
		session->create_time = tv.tv_sec;
		session->rekey_time = tv.tv_sec;

		session_set_state(session, IKE_SMR_AUTH);

		sm_session_responder_timeout_set(session);

		retval = 0;

		break;

	case IKE_SMR_AUTH:

		LOG_DEBUG("STATE IKE_SMR_AUTH");

		sm_session_responder_timeout_cancel(session);

		/*
		 * Check if we received notify INVALID_SYNTAX. In
		 * that case terminate IKE SA..
		 */
		if (msg->ike_auth.invalid_syntax) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify");
#warning "Send empty response!"
			break;
		}

		/*
		 * Check if we received HTTP_CERT_LOOKUP_SUPPORTED
		 */
		if (msg->ike_auth.http_cert_lookup)
			session->cert_lookup_supported = TRUE;

		/*
		 * Find configuration peer section for peer
		 */
		g_static_rw_lock_reader_lock(&(ikev2_data->config_lock));
		session->cp = config_peer_find_by_id(
					ikev2_data->config->peer,
					msg->ike_auth.i_id);

		if (session->cp) {
			g_atomic_int_inc(&(session->cp->refcnt));

			session->cg = ikev2_data->config->cg;
			g_atomic_int_inc(&(session->cg->refcnt));

		}
		g_static_rw_lock_reader_unlock(&(ikev2_data->config_lock));

		if (session->cp == NULL) {
			s = id_id2str(msg->ike_auth.i_id);
			LOG_ERROR("Could not find configuration for ID %s", s);
			g_free(s);
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			goto out;
		}

		LOG_NOTICE("Selected PEER section at line %u",
					session->cp->cfg_line);

		session->peer_id = msg->ike_auth.i_id;
		msg->ike_auth.i_id = NULL;

		/**
		 * FIXME: Free memory occupied by struct auth_data
		 * *auth data.
		 *
		 * Also, check that peer is using allowed authentication
		 * method.
		 */
		auth_data.auth_type = msg->ike_auth.auth_type;

		if (!msg->ike_auth.auth_payload)
			auth_data.auth_type = IKEV2_AUTH_EAP;

		if (auth_data.auth_type != 
				(session->cp->peer_auth_method & 0x00ff)) {
			LOG_ERROR("Authentication FAILED: peer authentication "
					"method not supported");

			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			goto out;
		}

		if (auth_data.auth_type == IKEV2_AUTH_EAP && 
				session_get_auth_type(session)
					!= IKEV2_AUTH_RSA)
			LOG_WARNING("According to RFC4306 Responder MUST use "
					"certificates to authenticate itself");

#ifdef MOBIKE
		/*
		 * Check if we received N_MOBIKE_SUPPORTED notify payload; if it is
		 * true, respond with the same notify payload if we support it too
		 * for this particular peer
		 */
		if (msg->ike_auth.peer_supports_mobike == TRUE) {
			if (session->cp->mobike == MOBIKE_ON
				|| session->cp->mobike == MOBIKE_PASSIVE)
				session->mobike_supported = TRUE;
			else if (session->cr) {
				if ((session->cr->mobike == MOBIKE_ON
					|| session->cr->mobike == MOBIKE_PASSIVE)
					&& session->cp->mobike != MOBIKE_OFF)
					session->mobike_supported = TRUE;
			}
		}
#endif /* MOBIKE */

	case IKE_SMR_AUTH_DL_PEER:

		/*
		 * Have we already initiated cert material download?
		 */
		if (sm_msg->msg_type == CERT_MSG) {
			LOG_DEBUG("STATE IKE_SMR_AUTH_DL_PEER");
			auth_data.auth_type = IKEV2_AUTH_RSA;
		}

		auth_failed = FALSE;

		switch (auth_data.auth_type) {
		case IKEV2_AUTH_PSK:
			auth_data.key = psk_secret_find_by_id(
						session->cg->psk,
						session->peer_id);

			if (auth_data.key == NULL) {
				LOG_ERROR("Could not find "
					"authentication data for peer");
				auth_failed = TRUE;
			} else
				auth_data.key_len = strlen(auth_data.key);

			break;

		case IKEV2_AUTH_RSA:

			/** 
			 * Peer authenticates with certificate based
			 * authentication. Verify its AUTH payload.
			 * (CERT payload(s) are already verified.) 
			 *
			 * Received certificates are stored in the
			 * msg->cert_list. If we have configured
			 * at least one CA, we try to verify that
			 * certificate(s), no matter if we are using PSK
			 * or RSA authentication.
			 *
			 * If we posses peer's certificate(s) stored
			 * locally, the received CERT payload(s) are
			 * ignored.
			 */

			/**
			 * First check if all certification material is available.
			 * If not, initiate download and wait for CERT_MSG.
			 * At this point check is undertaken for CA items which 
			 * are defined for this peer.
			 */

			next_crl_update = session->next_crl_update;
			session->preserve_message = FALSE;

			/* have we already initiated the download? */
			if (sm_msg->msg_type == CERT_MSG) {
				/* was the dl successful? */
				if (sm_msg->cert_msg.success) {
					/* retrieve message */
					sm_msg = sm_msg->cert_msg.sm_msg;
					msg = &sm_msg->message_msg;
					/* callee will delete embedded msg too */
					session->embedded_sm_msg = sm_msg;
				} else {
					LOG_ERROR("Download unsuccessful! Terminating...");
					sm_msg_free(sm_msg->cert_msg.sm_msg);
					auth_failed = TRUE;
					break;
				}
			}

			if (!cert_check_auth_material(NULL, 
					session->cp->ca_list,
					session, sm_msg, &next_crl_update)) {

				retval = 1;
				/*
				 * pointer to message is embedded in
				 * dl request
				 */
				session->preserve_message = TRUE;
				session_set_state(session,
						IKE_SMR_AUTH_DL_PEER);
				goto out;
			}

			/*
			 * when cert material is present, write when
			 * we'll need updating
			 */
			session->next_crl_update = next_crl_update;

			ca_items = session->cp->ca_list;

			if (!ca_items) {
				LOG_DEBUG("Certificates associated to peer's "
						"identity not found!");
				auth_failed = TRUE;
				break;
			}

			peers_cert_items = session->cp->cert_list;
			if (peers_cert_items != NULL) {

				verified_cert_items =
						cert_prepare_cert_item_verify(
						peers_cert_items,
						ca_items,
						session);

				if (!cert_check_public_keys(NULL, 
						peers_cert_items)) {
					LOG_ERROR("Local peer's certs do not "
						"have the same public key!");
					auth_failed = TRUE;
					break;
				}

				/** In case of verifing received AUTH payload,
				 * auth_data contains the public key extracted
				 * from the first local peer's certificate.
				 */

				cert_file = ((struct cert_item *)
						session->cp->cert_list->data)->file; 

				auth_data.public_key_len =
					crypto_read_pubkey_from_file(cert_file,
							&(auth_data.public_key));

				if ((verified_cert_items == NULL) ||
						(auth_data.public_key == NULL)) {
					auth_failed = TRUE;
					break;
				}

				session->peer_local_certs = 
					verified_cert_items;
			}

			if (!peers_cert_items || verified_cert_items) {

				verified_certs = cert_prepare_cert_verify(
						msg->ike_auth.cert_list,
						ca_items,
						session);

				if (!verified_certs) {
					LOG_ERROR("No verified certificates "
						"in CERT payload(s)!");
					auth_failed = TRUE;
					break;
				}

				if (!cert_check_public_keys(
					msg->ike_auth.cert_list,
					NULL)) {
					LOG_ERROR("Payload peer's certs do not"
						" have the same public key!");
					auth_failed = TRUE;
					break;
				}

				session->peer_payload_certs = verified_certs;

				/**
				 * In case of verifing received AUTH payload,
				 * auth_data contains the public key from the
				 * first CERT payload.
				 */
				auth_data.public_key = 
					((struct cert *)msg->ike_auth.cert_list->data)->pub_key;
				auth_data.public_key_len = 
					((struct cert *)msg->ike_auth.cert_list->data)->pub_key_len;
			}

			session->used_cas = g_slist_concat(session->used_cas,
					cert_find_used_cas(session->cp->ca_list,
					session));

			/**
			 * Check if identity from ID payload matches the id
			 * from the Subject/SubjectAltName certificate field. 
			 */
			if ((!session->cp->options & OPTION_NO_ID_VERIFY) ||
					session->peer_id->type ==
						IKEV2_IDT_DER_ASN1_DN) {
				LOG_DEBUG("ID verification");
				auth_failed = !crypto_verify_id(
						verified_cert_items, 
						verified_certs,
						session->peer_id);
				if (auth_failed)
					LOG_ERROR("id from ID payload does"
					"not match id from certificate!");
			} else
				LOG_DEBUG("Skipping ID verification!");

			break;

#ifdef AAA_CLIENT
		case IKEV2_AUTH_EAP:
			break;
#endif /* AAA_CLIENT */

		case IKEV2_AUTH_DSS:
		case IKEV2_AUTH_ECDSS:
		default:
			LOG_ERROR("Peer requested unsupported "
				"authentication type! Terminating "
				"session.");
			auth_failed = TRUE;
		}

		if (auth_data.auth_type != IKEV2_AUTH_EAP) {

			if (!auth_failed)
				auth_failed = auth_data_verify(
						session->prf_type,
						&auth_data,
						session->peer_id,
						session->ikesa_init_i,
						session->ikesa_init_i_len,
						session->r_nonce,
						session->sk_pi,
						msg->ike_auth.auth_payload,
						msg->ike_auth.auth_payload_len);

			/*
			 * Free data from first exchange
			 */
			g_free(session->ikesa_init_i);
			session->ikesa_init_i = NULL;

			/*
			 * For now we treat unsuccessful authentication
			 * and some error that happened during
			 * auth_data_verify execution same.
			 */
			if (auth_failed) {
				LOG_ERROR("Peer authentication failed");
				msg->notify = N_AUTHENTICATION_FAILED;
				message_send_notify(session, msg);
				break;
			} else
				LOG_NOTICE("Peer authenticated successfuly");
		}

		/*
		 * Limit session SA auth time to cert validity (if
		 * RSA used) (send verified_cert_items and verified_certs
		 * to iterate all certs)
		 */
		if (session->cp->auth_limits)
			session->auth_limit_time =
					session->cp->auth_limits->time;
		else
			session->auth_limit_time = 0;

		if (auth_data.auth_type == IKEV2_AUTH_RSA)
			session->auth_limit_time =
					cert_sa_limit_adjust(
						session->create_time,
						session->auth_limit_time,
						verified_cert_items,
						verified_certs);

	case IKE_SMR_AUTH_DL_HOST:

		/*
		 * Have we already initiated cert material download?
		 */
		if (sm_msg->msg_type == CERT_MSG) {
			LOG_DEBUG("STATE IKE_SMR_AUTH_DL_HOST");
			auth_data.auth_type = IKEV2_AUTH_RSA;
		}

		/*
		 * Prepare authentication data to send to peer...
		 */
		auth_failed = FALSE;
		session->auth_data->auth_type = session_get_auth_type(session);

		switch (session->auth_data->auth_type) {
		case IKEV2_AUTH_PSK:
			session->auth_data->key = psk_secret_find_by_id(
							session->cg->psk,
							session->cp->lid);

			if (session->auth_data->key == NULL) {
				LOG_ERROR("No authentication data for peer");
				auth_failed = TRUE;
			} else
				session->auth_data->key_len =
						strlen(session->auth_data->key);
			break;

		case IKEV2_AUTH_RSA:

			/**
			 * Prepare data for sending CERT payload(s) and AUTH
			 * payload:
			 *
			 * Find appropriate certificates according to the
			 * received and already parsed CERTREQ payload(s).
			 *
			 * Load the auth_data for creating the AUTH payload
			 * from the first CERT payload that we are planning
			 * to send.
			 *
			 * Preparing the data for the CERTREQ(s) sending is
			 * independent of the authentication method and is
			 * done in the appropriate message_send_* functions.
			 */

			/**
			 * First check if all certification material is
			 * available. If not, initiate download and wait for
			 * CERT_MSG. At this point check is undertaken for
			 * CA items which our certificates are verified with.
			 */

			next_crl_update = session->next_crl_update;
			session->preserve_message = FALSE;

			/* have we already initiated the download? */
			if (sm_msg->msg_type == CERT_MSG) {
				/* was the dl successful? */
				if (sm_msg->cert_msg.success) {
					/* retrieve message */
					sm_msg = sm_msg->cert_msg.sm_msg;
					msg = &sm_msg->message_msg;
					/* callee will delete embedded msg too */
					session->embedded_sm_msg = sm_msg;
				} else {
					LOG_ERROR("Download unsuccessful! "
							"Terminating...");
					sm_msg_free(sm_msg->cert_msg.sm_msg);
					auth_failed = TRUE;
					break;
				}
			}

			if (!cert_check_auth_material(session->cg->ca_list, 
					NULL,
					session, sm_msg, &next_crl_update)) {

				retval = 1;
				/*
				 * pointer to message is embedded in
				 * dl request
				 */
				session->preserve_message = TRUE;
				session_set_state(session,
						IKE_SMR_AUTH_DL_HOST);
				goto out;
			}

			/*
			 * When cert material is present, write when
			 * we'll need updating
			 */
			session->next_crl_update = next_crl_update;

			session->certs_for_peer = cert_find_appropriate_certs(
					msg->ike_auth.certreq_list,
					session->cg->cert_list,
					session->cg->ca_list,
					session->cert_lookup_supported,
					session);

			if (session->certs_for_peer == NULL) {
				LOG_ERROR("No certificates for peer.");
				auth_failed = TRUE;
				break;
			}

			session->used_cas = g_slist_concat(session->used_cas, 
				cert_find_used_cas(session->cg->ca_list,
				session));

			/**
			 * The data from the first CERT payload (first
			 * element of the list session->certs_for_peer) is
			 * used to sign the AUTH payload.
			 */
			session->auth_data->priv_key_file =
					((struct cert_item *)session->certs_for_peer->data)->priv_key_file;
			session->auth_data->cert_type =
					((struct cert_item *)session->certs_for_peer->data)->type;

			if (session->auth_data->priv_key_file == NULL) {
				LOG_ERROR("Could not find my authentication "
					"data for creating the AUTH payload.");

				auth_failed = TRUE;
			}

			break;

		case IKEV2_AUTH_DSS:
		case IKEV2_AUTH_ECDSS:
		default:
			LOG_ERROR("Unsupported authentication type!");
			auth_failed = TRUE;
		}

		/*
		 * If authentication data preparation has been usuccessful
		 * send back AUTHENTICATION_FAILED notification...
		 */
		if (auth_failed) {
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			break;
		}

#ifdef CFG_MODULE
		/*
		 * Preprocess CFG request from a peer...
		 *
		 * We do not send error notification at this stage since
		 * it's possible that EAP authentication isn't yet
		 * finished, and, until we are certain who we are talking
		 * to, we do not send any potentialy leaking information.
		 */
		if (msg->ike_auth.cfg) {
			session->rcfg = cfg_proxy_new(session->cp->providers,
						msg->ike_auth.cfg,
						session->peer_id);

			/*
			 * Check if error occured! Note that we do not signal
			 * error until authentication is successful in order
			 * not to leak data to a potential attacker.
			 */
			if (!session->rcfg)
				session->rcfg = GUINT_TO_POINTER(1);
			else
				cfg_proxy_dump(session->rcfg);
		}
#endif /* CFG_MODULE */

		/*
		 * Adjust mode (TUNNEL or TRANSPORT)
		 */
		if (sm_msg->message_msg.ike_auth.transport_mode) {
			if (sad_item_get_ipsec_mode(si) == IPSEC_MODE_TUNNEL) {
				LOG_ERROR("Peer requested TUNNEL mode which "
						"is not supported by local"
						" configuration!");
				goto out;
			}

			sad_item_set_ipsec_mode(si, IPSEC_MODE_TRANSPORT);

		} else {

			if (sad_item_get_ipsec_mode(si)
					== IPSEC_MODE_TRANSPORT) {
				LOG_ERROR("Peer requested TRANSPORT mode which "
						"is not supported by local"
						" configuration!");
				goto out;
			}

			sad_item_set_ipsec_mode(si, IPSEC_MODE_TUNNEL);
		}

#ifdef CFG_MODULE
		/*
		 * We do not try to search SPD in case CFG returned
		 * an error. But since we have to wait until any
		 * potential authentication has been finished!
		 */
		if (session->rcfg != GUINT_TO_POINTER(1)) {
#endif /* CFG_MODULE */
			/*
			 * Search for exact match with a list of SPDs. The
			 * search is restricted only to a set of SPDs that
			 * are covered by a config_peer structure selected
			 * for this peer.
			 */
			si->spd = spd_item_find_by_ts(msg->ike_auth.tsr,
						msg->ike_auth.tsi,
						session->cp->sainfo,
						&si->tsl, &si->tsr);

			if (!si->spd)
				LOG_ERROR("Could not find appropriate policy."
						" Traffic selectors are "
						"unacceptable!");
			else
				spd_item_ref(si->spd);
#ifdef CFG_MODULE
		} else
			si->spd = NULL;
#endif /* CFG_MODULE */
	
		if (si->spd != NULL) {

			/*
			 * Set SPI value sent by peer...
			 * TODO: We assume that peer sent us a valid proposal
			 * with SPIs...
			 */
			sad_item_set_initiator(si, FALSE);

			/*
			 * Find intersection between proposals sent by peer and
			 * proposals defined in configuration file.
			 */
			proposal = proposal_get(msg->ike_auth.proposals,
					sad_item_get_encr_algs(si),
					sad_item_get_auth_algs(si));

			if (proposal) {
				sad_item_set_spi_size(si,
						proposal_get_spi_size(
							proposal));
				sad_item_set_peer_spi(si,
						proposal_get_spi(
							proposal));

				LOG_DEBUG("PEER PROPOSALS");
				proposal_dump_proposal_list(LOGGERNAME,
						msg->ike_auth.proposals);

				LOG_DEBUG("RESULT PROPOSAL");
				proposal_dump_proposal(LOGGERNAME, proposal);

				sad_item_set_proposal(si, proposal);

				sad_getspi(si, session_get_i_addr(session),
						session_get_r_addr(session));

				if (!sad_item_get_spi(si))
					g_assert_not_reached();

				msg->ike_auth.proposals = g_slist_remove(
							msg->ike_auth.proposals,
							proposal);

				proposal_list_set_spi(
						sad_item_get_proposals(si),
						sad_item_get_spi_size(si),
						sad_item_get_spi(si));
			} else
				LOG_ERROR("No proposal chosen!");

		}

#ifdef AAA_CLIENT
		/*
		 * If peer requested EAP authentication, send initial
		 * AAA request (RADIUS) and wait for response, or timeout
		 */
		if (auth_data.auth_type == IKEV2_AUTH_EAP) {

			/*
			 * Check if session->peer_id is apropriate username
			 * for use in EAP packet as 'username'
			 *
			 * If not, create EAP-Identity/Request packet and
			 * sent it to Initiator
			 *
			 * FIXME: other id types as username?
			 *
			 * Note for ikev2.conf file: if EAP is used, and 
			 * my_identifier is set to mail:* then ID will be
			 * used as a username!
			 */
			if (session->peer_id->type != IKEV2_IDT_RFC822_ADDR) {

				/*
				 * Send EAP-Id-Rq to Initiator
				 */
				session->eap_packet = aaa_new_eap_idrq();
				session->eap_packet_len = 
						aaa_get_eap_length(
							session->eap_packet);
				aaa_status = AAA_RECEIVED;

			} else
				aaa_status = aaa_request(
						session,
						NULL,
						session->peer_id->id,
						session->peer_id->len);

			if (aaa_status == AAA_CONT) {
				session_set_state(session, 
						IKE_SMR_AUTH_RESPONSE);
				session->preserve_message = TRUE;
				session->embedded_sm_msg = (union sm_msg *)msg;
				retval = 0;
				break;

			} else {
				/*
				 * It would be nice to get rid of all
				 * the goto's in the code
				 */
				goto got_first_eap_response;
			}
		}
#endif /* AAA_CLIENT */

	case IKE_SMR_AUTH_FINALIZE:

		if (session_get_state(session) == IKE_SMR_AUTH_FINALIZE)
			LOG_DEBUG("STATE IKE_SMR_AUTH_FINALIZE");

#ifdef AAA_CLIENT
		if (session->aaa_data) {
			sm_session_responder_timeout_cancel(session);

			/*
			 * Check 'AUTH' payload, crypted with MSK obtained 
			 * from EAP if given, otherwise like without EAP
			 */
			/*
			 * 'auth_data' undefined when EAP is used !!!
			 * When this thread is started in IKE_SMR_AUTH_FINALIZE
			 * state, 'auth_data' is not filled with values
			 */
			auth_failed = FALSE;

			auth_data.auth_type = IKEV2_AUTH_EAP;
			if (!aaa_get_msk(session, &auth_data.key, 
					&auth_data.key_len)) {

				LOG_WARNING("Used EAP method does not generate"
						" MSK.");

				auth_data.key = session->sk_pi;
				auth_data.key_len = transform_get_key_len(
						session->prf_type);
			}

			if (!auth_failed)
				auth_failed = auth_data_verify(
					session->prf_type,
					&auth_data,
					session->peer_id,
					session->ikesa_init_i,
					session->ikesa_init_i_len,
					session->r_nonce,
					session->sk_pi,
					msg->ike_auth.auth_payload,
					msg->ike_auth.auth_payload_len);

			g_free(session->ikesa_init_i);
			session->ikesa_init_i = NULL;

			/*
			 * For now we treat unsuccessful authentication
			 * and some error that happened during
			 * auth_data_verify execution same.
			 */
			if (auth_failed) {
				LOG_ERROR("EAP authentication completed "
					"succesfully, but AUTH payload is "
					"wrongly crypted or decrypted");
				msg->notify = N_AUTHENTICATION_FAILED;
				message_send_notify(session, msg);
				break;
			} else
				LOG_NOTICE("Peer authenticated successfuly");
		}
#endif /* AAA_CLIENT */

		/*
		 * Also, because we finished IKE SA negotiation decrease
		 * number of half opened sessions...
		 */
		ikev2_data->half_opened_sessions--;

#ifdef CFG_MODULE
		/*
		 * Check if peer requested configuration data.
		 */
		if (session->rcfg && session->rcfg != GUINT_TO_POINTER(1)) {

			/*
			 * Request parameters...
			 */
			ret = cfg_proxy_request(session->rcfg, session);

			if (ret < 0) {

				LOG_WARNING("No configuration data has been "
						"obtained. Continuing.");

				cfg_proxy_free(session->rcfg);
				session->rcfg = GUINT_TO_POINTER(1);

			} else if (ret > 0) {

				/*
				 * We have to wait for configuration
				 * data to be obtained.
				 */
				session_set_state(session, IKE_SMR_CFG_WAIT);
				session->preserve_message = TRUE;
				session->embedded_sm_msg = msg;
				retval = 0;
				break;

			} else {

				/*
				 * Dump received data
				 */
				cfg_proxy_dump(session->rcfg);
			}
		}

	case IKE_SMR_CFG_WAIT:

		/*
		 * Check if we were waiting for configuration data...
		 */
		if (session_get_state(session) == IKE_SMR_CFG_WAIT) {
			LOG_DEBUG("STATE IKE_SMR_CFG_WAIT");

			msg = session->embedded_sm_msg;
			session->preserve_message = FALSE;
		}

		/*
		 * If no error occured, continue with the configuration
		 * process.
		 */
		if (session->rcfg &&
				session->rcfg != GUINT_TO_POINTER(1) &&
				!cfg_proxy_is_error(session->rcfg)) {

			cfg_proxy_dump(session->rcfg);

			/*
			 * Execute external scripts that will setup all the
			 * necessary network parameters
			 */
			clntaddr = netaddr_dup(session_get_i_addr(session));
			netaddr_set_port(clntaddr, 0);
			vpngwaddr = netaddr_dup(session_get_r_addr(session));
			netaddr_set_port(vpngwaddr, 0);

			clntif = netif_find_by_net(vpngwaddr);

			if (cfg_module_exec_script(session->cp->gw_up_argv,
					cfg_proxy_get_cfg(session->rcfg),
					clntaddr, netif_get_name(clntif),
					vpngwaddr) < 0) {

				cfg_proxy_release(session->rcfg);
				cfg_proxy_free(session->rcfg);
				session->rcfg = GUINT_TO_POINTER(1);

			} else {

				/*
				 * Narrowing of the remote traffic selector in
				 * policy has to be done for each IP address.
				 *
				 * What we do is take remote traffic selectors
				 * already negotiated by initiator and
				 * responder, and then we do further narrowing
				 * to IP addresses obtained from the
				 * configuration provider.
				 */
				tsr = sad_item_get_tsr(si);
				sad_item_clear_tsr(si);

				for (c = cfg_proxy_get_netaddrs(session->rcfg);
						c; c = c->next) {

					ts = ts_new();
					ts_set_saddr(ts, netaddr_dup(c->data));
					netaddr_set_port(ts_get_saddr(ts), 0);
					ts_set_eaddr(ts, netaddr_dup(c->data));
					netaddr_set_port(ts_get_eaddr(ts),
							65535);

					tsl = ts_list_intersect(ts, tsr);

					/*
					 * Note that we here assume that
					 * intersection produced _single_
					 * traffic selector no matter we
					 * are actually receiving back
					 * a list.
					 */
					if (tsl && sad_item_set_tsr(si,
							tsl->data) < 0)
						LOG_ERROR("Setting remote "
							"traffic selector "
							"failed!");

					ts_free(ts);
					ts_list_free(tsl);
				}

				ts_list_free(tsr);

#if 0
				/*
				 * Limit CHILD SA's lifetime on expiry
				 */
				gettimeofday(&tv, NULL);
				if (!session->rcfg->expiry4 &&
						!session->rcfg->expiry6)
					session->lease_time = 0xFFFFFFFF;
				else if (!session->rcfg->expiry4 &&
						session->rcfg->expiry6)
					session->lease_time = tv.tv_sec +
							session->rcfg->expiry6;
				else if (session->rcfg->expiry4 &&
						!session->rcfg->expiry6)
					session->lease_time = tv.tv_sec +
							session->rcfg->expiry4;
				else if (session->rcfg->expiry4 <
						session->rcfg->expiry6)
					session->lease_time = tv.tv_sec +
							session->rcfg->expiry4;
				else
					session->lease_time = tv.tv_sec +
							session->rcfg->expiry6;
#endif

			}

			netaddr_free(clntaddr);
			clntaddr = NULL;
			netaddr_free(vpngwaddr);
			vpngwaddr = NULL;

		} else {

			if (session->rcfg &&
					session->rcfg != GUINT_TO_POINTER(1)) {
				cfg_proxy_free(session->rcfg);
				session->rcfg = GUINT_TO_POINTER(1);
			}
		}
#endif /* CFG_MODULE */

#ifdef AAA_CLIENT
		if (session->aaa_data) {
			if (!aaa_get_msk(session, &session->auth_data->key,
					&session->auth_data->key_len)) {
				LOG_WARNING("EAP method used does not "
						"generate MSK. This is against"
						"  RFC4306!");
				session->auth_data->key = session->sk_pr;
				session->auth_data->key_len =
						transform_get_key_len(
							session->prf_type);
			}

			session->auth_data->auth_type = IKEV2_AUTH_EAP;
			
			message_send_ike_auth_final_r(session, msg);

			/*
			 * FIXME: Verify that we don't have memory leak here!
			 */
			session->auth_data->key = NULL;
			session->auth_data->key_len = 0;

			/*
			 * check if "Session-Timeout" attribute is set
			 * and act on it
			 */
			aaa_eap_timeout = aaa_get_session_time_limit(session);
			if (aaa_eap_timeout > 0 && 
				aaa_eap_timeout < session->auth_limit_time)
				session->auth_limit_time = aaa_eap_timeout;

			/*
			 * free session->aaa_data, but mark that EAP was
			 * used for authentication (on Session-Timeout
			 * remove CA)
			 */
			aaa_free(session);
			session->aaa_data = GUINT_TO_POINTER(1);

			session->preserve_message = FALSE;
		} else
#endif /* AAA_CLIENT */
			message_send_ike_auth_r(session, msg);

		/*
		 * Authentication has been successful, so from now on in case
		 * of an error we do not terminate session!
		 */
		retval = 1;

		/*
		 * If CHILD SA has been successfuly negotiated, compute
		 * keys and finish the process.
		 */
		if (si->proposals && si->spd
#ifdef CFG_MODULE
			&& ((session->rcfg
				&& session->rcfg != GUINT_TO_POINTER(1))
				|| !session->rcfg)
#endif /* CFG_MODULE */
				) {
			si->encr = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_ENCR,
						sad_item_get_protocol(si));

			si->auth = proposal_list_find_transform(
						sad_item_get_proposals(si),
						IKEV2_TRANSFORM_INTEGRITY,
						sad_item_get_protocol(si));

			sad_item_compute_keys(si,
					session->sk_d,
					session->i_nonce,
					session->r_nonce,
					session->prf_type,
					session_get_ike_type(session) ==
						IKEV2_INITIATOR);

			sad_add(si, session_get_r_addr(session),
					session_get_i_addr(session),
					session->cg->kernel_spd !=
						KERNEL_SPD_ROSYNC,
					session->natt);

			sad_item_remove_transient(si);

			/*
			 * We just established CHILD SA so increase number of
			 * allocations done in this IKE SA
			 */
			session->allocations++;

			sad_item_set_state(si, SA_STATE_MATURE);

		} else {

			LOG_DEBUG("Removing sad_item");
			session_sad_item_remove(session, si);
			sad_item_free(si);

		}

		session_set_state(session, IKE_SM_MATURE);
		
		session_transient_free(session);

		retval = 0;

		break;

#ifdef AAA_CLIENT
	case IKE_SMR_AUTH_RESPONSE:
		LOG_DEBUG("STATE IKE_SMR_AUTH_RESPONSE");

		msg = (struct message_msg *) session->embedded_sm_msg;
		session->preserve_message = FALSE;

		aaa_status = aaa_get_response(session);

got_first_eap_response:
		if (!session->eap_packet)
			aaa_status = AAA_ERROR;
		else if (aaa_get_eap_code(session->eap_packet)
				== EAP_CODE_FAILURE)
			aaa_status = AAA_REJECT;

		if (aaa_status == AAA_ERROR) {
			LOG_ERROR("RADIUS ended authentication");
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			break;
		}

		LOG_DEBUG("eap_len=%d", session->eap_packet_len);
		message_send_ike_auth_r(session, msg);
		
		if (!session->aaa_data) {
			/*
			 * Free first EAP-IdRq packet
			 */
			g_free(session->eap_packet);
		}

		/*
		 * No EAP method generate EAP_CODE_SUCCESS on first reply, 
		 * so it must be EAP_CODE_REQUEST
		 */
		if (aaa_status == AAA_CONT || aaa_status == AAA_RECEIVED) {
			session_set_state(session, 
					IKE_SMR_EAP_INITIATOR_REQUEST);
			retval = 0;

			sm_session_responder_timeout_set(session);
		
		} else {
			LOG_ERROR("RADIUS auth. ended with reject (code=%d)",
					aaa_status);
		}

		break;

	case IKE_SMR_EAP_INITIATOR_REQUEST:

		/*
		 * We enter this state when initiator sends us IKE AUTH
		 * request with embedded EAP packet.
		 */
		LOG_DEBUG("STATE IKE_SMR_EAP_INITIATOR_REQUEST");

		sm_session_responder_timeout_cancel(session);

		if (msg->ike_auth.invalid_syntax || 
				msg->ike_auth.authentication_failed) {
			LOG_ERROR("Peer sent INVALID_SYNTAX notify or "
					"AUTHENTICATION FAILED");
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			break;
		}

		/*
		 * EAP packet in msg->ike_auth.eap, send it to AAA
		 */
		aaa_status = aaa_request(session, msg->ike_auth.eap, NULL, 0);

		if (aaa_status == AAA_CONT) {
			session_set_state(session, IKE_SMR_EAP_AAA_REQUEST);
			session->embedded_sm_msg = (union sm_msg *) msg;
			session->preserve_message = TRUE;
			retval = 0;
			break;
		}
		/*
		 * If got message, continue...
		 */

	case IKE_SMR_EAP_AAA_REQUEST:

		if (session_get_state(session) == IKE_SMR_EAP_AAA_REQUEST) {
			LOG_DEBUG("STATE IKE_SMR_EAP_AAA_REQUEST");
			
			aaa_status = aaa_get_response(session);
			msg = (struct message_msg *) session->embedded_sm_msg;
			session->preserve_message = FALSE;
		}

		if (!session->eap_packet)
			aaa_status = AAA_ERROR;
		else if (aaa_get_eap_code(session->eap_packet)
				== EAP_CODE_FAILURE) {
			aaa_status = AAA_REJECT;
		}

		if (aaa_status == AAA_ERROR) {
			LOG_ERROR("RADIUS ended authentication");
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			break;
		}

		/*
		 * Got response from AAA (starting from the 2nd, the
		 * first one is handled by another state)
		 *
		 * 1. Send ( HDR, SK {EAP} ) to initiator
		 *
		 * 2. if EAP_CODE_SUCCESS received
		 *		move to state IKE_SMR_AUTH_FINALIZE
		 *    else if EAP_CODE_REQUEST
		 *		move to state IKE_SMR_EAP_INITIATOR_REQUEST
		 *    else (EAP_CODE_FAILURE)
		 *		drop session
		 */

		/*
		 * Send EAP packet to initiator
		 */
		if (message_send_ike_eap_r(session, msg)) {
			LOG_ERROR("Message hasn't been sent to initiator");
			msg->notify = N_AUTHENTICATION_FAILED;
			message_send_notify(session, msg);
			goto out;
		}

		/*
		 * Check AAA status
		 */
		uname = id_id2str(session->peer_id);
		switch (aaa_status) {
		case AAA_REJECT:
			LOG_ERROR("EAP authentication FAILED for user %s", uname); 
			break;

		case AAA_CONT:
			session_set_state(session,
					IKE_SMR_EAP_INITIATOR_REQUEST);
			retval = 0;
			sm_session_responder_timeout_set(session);
			break;

		case AAA_ACCEPT:
			if (aaa_get_eap_code(session->eap_packet) == EAP_CODE_SUCCESS) {
				LOG_TRACE("EAP authentication completed "
					"successfully for user %s", uname); 
				session_set_state(session, 
						IKE_SMR_AUTH_FINALIZE);
				retval = 0;

				sm_session_responder_timeout_set(session);
			} else
				LOG_ERROR("Got AAA_ACCEPT but no EAP ",
					"packet with EAP_CODE_SUCCESS");
			break;
		}
		g_free(uname);
		break;
#endif /* AAA_CLIENT */

	default:
		break;
	}

out:
	LOG_FUNC_END(1);

	return retval;
}

/**
 * This is a main IKEv2 thread. All the events are routed to this function
 * which demultiplexes it to appropriate handler. Only one instance of
 * this function is executed ever...
 *
 * TODO: Retransmission handling in this function might slow it down because
 *		 of locking. Maybe it should be handled to thread pool...
 */
gpointer sm_main_thread(gpointer data)
{
	union sm_msg *msg;
	struct session *session;

	/*
	 * These are for convinience only...
	 */
	struct ikev2_data *ikev2_data = data;

	LOG_FUNC_START(1);

	while (TRUE) {
		LOG_TRACE("Waiting for a message on a queue");
		msg = g_async_queue_pop(ikev2_data->queue);
		LOG_TRACE("Got a message from a queue");

		if (GPOINTER_TO_UINT(msg) == 1) {
			sad_set_shutdown_state();
			spd_set_shutdown_state();
			break;
		}

		/*
		 * Check received message type
		 */
		switch (msg->msg_type) {
		case PFKEY_MSG:
			LOG_DEBUG("Processing PFKEY_MSG");

			switch(msg->pfkey_msg.action) {
			case SADB_ACQUIRE:
				session = sm_sessions_search_by_daddr(
						msg->pfkey_msg.acquire.daddr);

				/*
				 * If there is no IKE SA between source and
				 * destination and also, kernel sent us
				 * SADB_ACQUIRE, prepare for establishment of
				 * IKE SA.
				 */
				if (session == NULL) {

					session = session_new();
					session_lock(session);

					session_set_ike_type(session,
							IKEV2_INITIATOR);
					session_set_state(session,
							IKE_SMI_INIT);

					session_set_i_addr(session,
						msg->pfkey_msg.acquire.saddr);
					session_set_r_addr(session,
						msg->pfkey_msg.acquire.daddr);

					/*
					 * Add session to existing set of
					 * sessions
					 */
					sm_sessions_session_add(session);
				}

				if (session_msg_push(session, msg))
					sm_session_activate(session);

				break;

			case SADB_EXPIRE:
				session = sm_sessions_search_by_daddr(
						msg->pfkey_msg.acquire.daddr);

				if (session == NULL) {
					LOG_WARNING("Received EXPIRE message "
							"for SA not known to "
							"ikev2. Ignoring!");
					pfkey_msg_free(&msg->pfkey_msg);
					break;
				}

				if (session_msg_push(session, msg))
					sm_session_activate(session);

				break;

			default:
				LOG_WARNING("Received unhandled message from "
						"the kernel. Ignoring!");
				sm_msg_free(msg);
				break;
			}

			break;

		case MESSAGE_MSG:

			/*
			 * We received a message from a peer through the
			 * network. The following code finds corresponding
			 * session and passes all the data to state machine
			 * for processing.
			 *
			 * In case there is no corresponding session to a
			 * received message, a new one is created and
			 * then all the data is passed to a state machine.
			 *
			 * Each session is uniquely identified by peer's
			 * IP addresses. Note that also I_SPI and R_SPI
			 * uniquely identify session! That info should be
			 * used in case there is more than one session
			 * between two IKE peers.
			 */

			/*
			 * First, special case: DOS attack mitigation.
			 *
			 * If this is IKE_SA_INIT request and there are already
			 * too many half opened connections, check if cookie
			 * is present. If it is, continue normal processing,
			 * otherwise generate and cookie and terminate
			 * processing.
			 */
			if (msg->message_msg.exchg_type == IKEV2_EXT_IKE_SA_INIT
				&& msg->message_msg.i_spi
				&& !msg->message_msg.r_spi
				&& !msg->message_msg.msg_id
				&& !msg->message_msg.response
				&& ikev2_data->half_opened_sessions >=
					ikev2_data->config->cg->dos_treshold) {

				/*
				 * Search cookie in received message
				 * and in case there is an error in
				 * packet simply drop it...
				 */
				if (message_parse_quick(&msg->message_msg) < 0) {
					message_msg_free(&msg->message_msg);
					break;
				}

				/*
				 * Calculate expected cookie with a latest
				 * secret. In rear situations we'll need old
				 * secret, because cookie has been calculated
				 * with it, but then we'll recalculate it...
				 */
				message_calculate_cookie(&msg->message_msg);

				/*
				 * If there is no cookie NOTIFY
				 */
				if (!msg->message_msg.ike_sa_init.cookie) {
					message_send_cookie(&msg->message_msg);

					message_msg_free(&msg->message_msg);
					break;
				}

				/* 
				 * Lets check this calculated cookie with
				 * valid ones:
				 */
				if (!message_check_cookie(&msg->message_msg)) {
					LOG_ERROR("Received invalid cookie");
					message_msg_free(&msg->message_msg);
					break;
				}

				LOG_DEBUG("Received valid cookie");
			}

			/*
			 * According to clarification document when we received
			 * IKE_SA_INIT request exchange we should also search by
			 * hash on a packet. The problem is that there might be
			 * more stations behind NAT that have same SPI values!
			 */

			/*
			 * TODO: Maybe to break the code inside this writer lock
			 * into two parts, one that require only reader lock and
			 * the other that requires writer lock?
			 */
			LOG_TRACE("Acquiring writer lock");
			sm_sessions_writer_lock();

			LOG_DEBUG("Received message with "
					"i_spi=%016llX, r_spi=%016llX",
					msg->message_msg.i_spi,
					msg->message_msg.r_spi);

			session = sm_sessions_search_by_msg(&msg->message_msg);

			if (GPOINTER_TO_UINT(session) == 1) {
				/*
				 * There are too many half opened sessions
				 * with a given peer...
				 */
				LOG_ERROR("Too many half opened IKE SA "
						"connections with peer");
				message_msg_free(&msg->message_msg);
				sm_sessions_writer_unlock();
				break;
			}

			/*
			 * If we are under DOS attack, and someone forged
			 * some field in IKE SA INIT request packet to
			 * circumvent DOS protection, session will be NULL
			 * and we are terminating further processing!
			 */
			if (!session &&
				(msg->message_msg.exchg_type !=
					IKEV2_EXT_IKE_SA_INIT
				|| !msg->message_msg.i_spi
				|| msg->message_msg.r_spi
				|| msg->message_msg.msg_id
				|| msg->message_msg.response)) {

				LOG_ERROR("Received invalid packet");

				/*
				 * This IF is separated from the previous
				 * one to allow adding of code to send
				 * notify to a peer...
				 */
				if (ikev2_data->half_opened_sessions >=
					ikev2_data->config->cg->dos_treshold) {
					LOG_NOTICE("DOS attack detected.");
				}

				message_msg_free(&msg->message_msg);
				sm_sessions_writer_unlock();
				break;

			}

			if (session == NULL) {

				if ((session = session_new()) == NULL) {
					LOG_ERROR("Error creating "
							"session structure");
					sm_sessions_writer_unlock();
					message_msg_free(&msg->message_msg);
					break;
				}

				session_set_i_addr(session,
						msg->message_msg.srcaddr);
				session_set_r_addr(session,
						msg->message_msg.dstaddr);
				session_set_i_spi(session,
						msg->message_msg.i_spi);

				/*
				 * Increase counter of half opened sessions.
				 * It is used to detect potential DOS
				 * attack.
				 */
				ikev2_data->half_opened_sessions++;

				session->digest = message_msg_dup_digest(msg);

				session_set_ike_type(session, IKEV2_RESPONDER);
				session_set_state(session, IKE_SMR_INIT);

				sm_sessions_session_add_unlocked(session);
			}

			session_msg_push(session, msg);

			if (session_try_lock(session))
				sm_session_activate(session);

			sm_sessions_writer_unlock();
			break;

		case SIG_MSG:
			sm_signal_handler(ikev2_data, &msg->sig_msg);
			break;

#ifdef AAA_CLIENT
		case AAA_MESSAGE:

			session = msg->aaa_msg.session;

			LOG_TRACE("Using session=%p", session);

			if (session) {
				session_msg_push(session, msg);

				if (session_try_lock(session))
					sm_session_activate(session);
			} else {
				LOG_ERROR("No session found");
				sm_msg_free(msg);
                        }

			break;
#endif /* AAA_CLIENT */
		case TIMEOUT_MSG:
			LOG_ERROR("Received TIMEOUT message, terminating"
					" session");

			session = msg->timeout_msg.session;
			sm_msg_free(msg);

			/*
			 * FIXME: Check if session is still alive...
			 *
			 * Move this into thread pool as session_lock
			 * can block and stall further processings
			 */
			session_lock(session);

			sm_session_remove(session);

			break;

#ifdef CFG_MODULE
		case CFG_MSG:

			session = msg->cfg_msg.session;

			/*
			 * FIXME: Check if session is still alive...
			 */
			session_msg_push(session, msg);

			if (session_try_lock(session))
				sm_session_activate(session);

			break;
#endif /* CFG_MODULE */

		case CONNECT_MSG:

			/*
			 * First request for a connection to a given host.
			 * Create necessary structures and push processing
			 * into the thread pool.
			 */
			session = session_new();

			session_lock(session);

			session_set_ike_type(session, IKEV2_INITIATOR);
			session_set_state(session, IKE_SMI_INIT);

			/*
			 * Mark this session as a special so that in case
			 * an error occurs, we terminate ikev2 daemon.
			 */
//			session_set_user_initiated(session, TRUE);

			/*
			 * Set global lock. It is because this
			 * function could change state of all
			 * sessions and child sa's....
			 */
			sm_sessions_session_add(session);

			if (session_msg_push(session, msg))
				sm_session_activate(session);
			break;

		case CERT_MSG:

			session = msg->cert_msg.session;

			/*
			 * FIXME: Check if session is still alive...
			 */
			session_lock(session);

			if (session_msg_push(session, msg))
				sm_session_activate(session);
			break;

		} /* switch (msg->msg_type) */

	}

	g_main_loop_quit(ikev2_data->mainloop);

	LOG_FUNC_END(1);

	return NULL;
}

/**
 * Function called after responder hasn't received new request
 *
 * @param _ikev2_data
 *
 * \return TRUE, FALSE
 *
 * This function is called in IKE_SMR_INIT and IKE_SMR_AUTH
 * states after initiator hasn't sent new request. We wait
 * timeout value (with exponential backoff) and after specified
 * number of retries, the session is removed.
 */
gpointer sm_ikesa_responder_wait_timeout(gpointer _session)
{
	struct session *session = _session;

	LOG_FUNC_START(1);

	session_lock(session);

	/*
	 * If reference counter for session is 1 then we are the
	 * last user of this structure, so free it and terminate
	 * further processing.
	 */
	if (session_free(session)) {
		LOG_DEBUG("Timeout occured for unused session");
		goto out;
	} else
		session_unlock(session);

	/*
	 * If timeout value is NULL then timeout was canceled
	 * while we were waiting for a lock, so no further
	 * processing is necessary.
	 */
	if (session->responder_wait.to) {

		LOG_WARNING("No new request from initiator for %d seconds",
				session->responder_wait.tv.tv_sec);

		/*
		 * If we didn't reach maximum number of retries,
		 * double the timeout value and wait again
		 */
		if (session->responder_wait.retries--) {

			session_ref(session);

			session->responder_wait.tv.tv_usec = 0;
			session->responder_wait.tv.tv_sec <<= 1;
			session->responder_wait.to = timeout_register_thread(
					&session->responder_wait.tv,
					0, session,
					sm_ikesa_responder_wait_timeout);

			LOG_WARNING("Waiting another %d seconds for request",
					session->responder_wait.tv.tv_sec);
		} else {

			LOG_ERROR("Limit on number of retries reached."
					" Terminating session!");

			/*
			 * Maximum number of retries has been reached
			 * so remove session from the memory.
			 */
			session_msg_push(session,
					sm_session_msg_new(MSG_IKE_REMOVE));
			sm_session_activate(session);
		}
	}

	session_unlock(session);

out:
	LOG_FUNC_END(1);

	return NULL;
}

/**
 * Periodic thread that check timeouts for each IKE SA
 *
 * The following timeouts are checked
 *
 * - If more than ike_limit_hard_time passed since IKE SA has been created
 *	 initiate removal of IKE SA. New IKE SA has to be created.
 *
 * - If more than ike_limit_soft_time passed since IKE SA has been created
 *	 initiate rekeying of IKE SA
 *
 * - If more than ike_max_idle has passed since we last time got something from
 *	 peer, then initiate dead peer detection.
 *
 * TODO: This function currently does not lock each session it tries to check
 *	because we acquired read lock on ALL sessions and we are certain that
 *	no session will be removed during processing in this loop! All data
 *	that is processed _i think_ does not depend on updated value (i.e.
 *	we took value, it was updated in mean time, and we are working with
 *	old one).
 *
 *	If it turns out that locking is necessary, then on each session we
 *	should _try_ to get lock, but if we couldn't then simply skip that
 *	session, because something is happening to it so it's not so
 *	important to check it immeditely. The problem is if it's locked
 *	constantly!
 */
gboolean sm_ikesa_check_thread(gpointer _ikev2_data)
{
	struct session *session;
	struct sad_item *si;
	GSList *lsession, *sad;
	gboolean activate_session;
	struct timeval tv;

	struct ikev2_data *ikev2_data = _ikev2_data;

	sm_sessions_reader_lock();

	gettimeofday(&tv, NULL);

	for (lsession = ikev2_data->sessions; lsession;
			lsession = lsession->next) {
		session = lsession->data;

		if (!session_try_lock(session))
			break;

		/*
		 * TODO: Is it necessary to check timeouts in other states?
		 */
		if (session_get_state(session) != IKE_SM_MATURE) {
			session_unlock(session);
			continue;
		}

		sad = session->sad;
		while (sad) {
			si = sad->data;
			sad = sad->next;

			if (sad_item_get_state(si) != SA_STATE_REMOVE) {
				continue;
			}

			if (!session->cp->sa_halfclosed_wait) {
				continue;
			}

			if (tv.tv_sec - sad_item_get_remove_time(si) <
				session->cp->sa_halfclosed_wait) {
				continue;
			}

			LOG_ERROR("Removing half closed CHILD SA after"
				" (waiting for %u, msg_id=%u",
				session->cp->sa_halfclosed_wait,
				si->msg_id);

			session_sad_item_remove(session, si);
			sad_item_free(si);
		}

		activate_session = FALSE;

		if (session->create_time &&
				session->auth_limit_time &&
				(tv.tv_sec - session->create_time >
					session->auth_limit_time)) {

#ifdef AAA_CLIENT
			if (GPOINTER_TO_INT(session->aaa_data) == 1) {
				LOG_NOTICE("Session-Timeout expired, removing"
						" associations");
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			} else
#endif /*AAA_CLIENT*/
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_REAUTH));

			activate_session = TRUE;

		} else if (session->rekey_time &&
				session->cp->rekey_limits &&
				session->cp->rekey_limits->time &&
				(tv.tv_sec - session->rekey_time >
					session->cp->rekey_limits->time)) {

			session->rekey_time = 0;
			session_msg_push(session,
				sm_session_msg_new(MSG_IKE_REKEY));

			activate_session = TRUE;

#ifdef CFG_MODULE
		} else if (session->lease_time &&
				(session->lease_time < tv.tv_sec)) {

			if (session_get_ike_type(session) == IKEV2_RESPONDER) {
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_TERMINATE));
			} else {
				session->lease_time = 0;
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_LEASE_RENEW));
			}

			activate_session = TRUE;
#endif /* CFG_MODULE */

		} else if (session->last_seen &&
				(tv.tv_sec - session->last_seen >
					session->cp->ike_max_idle)) {

				session->last_seen = 0;
				session_msg_push(session,
						sm_session_msg_new(
							MSG_IKE_DPD));
				activate_session = TRUE;

		} else if (session->next_crl_update > 0 &&
				session->next_crl_update < tv.tv_sec) {
			session_msg_push(session, 
					sm_session_msg_new(MSG_IKE_CRL_UPDATE));
			activate_session = TRUE;
		}

		if (activate_session)
			sm_session_activate(session);
		else
			session_unlock(session);
	}

	sm_sessions_reader_unlock();

	return TRUE;
}

void sm_unload(struct ikev2_data *data)
{
	LOG_FUNC_START(1);
	g_thread_pool_free(data->thread_pool, TRUE, TRUE);
	LOG_FUNC_END(1);
}

int sm_init(struct ikev2_data *data, GAsyncQueue *queue)
{
	LOG_FUNC_START(1);

	ikev2_data = data;

	data->queue = queue;
	data->sessions = NULL;

	LOG_NOTICE("Starting %u sm_ike_thread threads",
				data->config->cg->sm_threads);
	data->thread_pool = g_thread_pool_new(sm_ike_thread, data,
				data->config->cg->sm_threads, TRUE, NULL);
	g_thread_create(sm_main_thread, data, FALSE, NULL);

	/*
	 * Start periodic thread
	 */
	g_timeout_add(IKESA_CHECKPOINT_INTERVAL, sm_ikesa_check_thread,
			data);

	LOG_FUNC_END(1);

	return 0;
}
