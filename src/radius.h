/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __RADIUS_H
#define __RADIUS_H

/**
 * Define standard RADIUS server port
 */
#define RADIUS_SERVER_PORT	1812

/***************************************************************************
 * Configuration structures
 ***************************************************************************/

/**
 * Structure with data necessary to access RADIUS server
 */
struct radius_server {
	/**
	 * RADIUS server IP address
	 */
	void *addr;

	/**
	 * Shared secret to be able to access RADIUS server
	 */
	char *shared_secret;
};

/**
 * Configuration for one radius domain
 */
struct radius_config {
	/**
	 * Reference counting
	 */
	gint refcnt;

	/**
	 * ID used to reference radius structures
	 */
	gchar *id;

	/**
	 * Line in configuration file where radius section is defined
	 */
	guint16 cfg_line;

	/**
	 * List of authentication servers in priority order
	 * Note: in current implementation using only first one
	 */
	GSList *auth_servers;

	/**
	 * List of accounting servers in priority order
	 */
	GSList *acct_servers;

	/**
	 * rfc2865:
	 * string identifying the NAS originating the Access-Request,
	 * Either NAS-IP-Address or NAS-Identifier MUST be present in an
	 * Access-Request packet
	 */
	char *nas_identifier;

	/**
	 * We sould take in account other possibility of using NAS-IP-Address
         * instead if that is configured.
         * I suggest to put following also (then some functions need to be
         * added/slightly changed)
         *
         * struct netaddr *nas_ip_address;
	 */

	/**
	 * rfc2865:
	 * An Access-Request SHOULD contain a NAS-Port or NAS-Port-Type
	 * attribute or both unless the type of access being requested
	 * does not involve a port or the NAS does not distinguish
	 * among its ports.
	 * ...
	 * This Attribute indicates the physical port number of the NAS which
	 * is authenticating the user.  It is only used in Access-Request
	 * packets.  Note that this is using "port" in its sense of a
	 * physical connection on the NAS, not in the sense of a TCP or UDP
	 * port number.  Either NAS-Port or NAS-Port-Type (61) or both SHOULD
	 * be present in an Access-Request packet, if the NAS differentiates
	 * among its ports.
	 *
	 * Meaning: not needed if NAS does not ..., but ...
	 */
	guint32 nas_port;

	/**
	 * Timeout period and retry count to access server in a list.
	 * Upon timeout we switch to the next one, or give up in case
	 * there are no more servers to try...
	 *
	 * The units are miliseconds...
	 */
	gint timeout;

	/**
	 * Number of retries in case of timeouts
	 */
	gint retries;

	/**
	 * How ofter accounting summary is sent to accounting RADIUS
	 * server. This value is in seconds.
	 */
	gint interim_interval;
};

/***************************************************************************
 * RADIUS state machine structure
 ***************************************************************************/

/**
 * Runtime data used during RADIUS authentication of Initiator
 */
struct radius_state {
	/**
	 * Configuration data to use
	 */
	struct radius_config *cr;

	/**
	 * Current authentication server used. Note that this is a pointer
	 * into cr->auth_servers list, so this pointer should _never_ be
	 * free'd!
	 */
	struct radius_server *auth_server;

	/**
	 * Retry count and timeout for current auth_server
	 */
	guint auth_retry_count;
	GTimeVal auth_timeval;
	void *auth_timeout;

	/**
	 * Current accounting server used. Note that this is a pointer
	 * into cr->auth_servers list, so this pointer should _never_ be
	 * free'd!
	 */
	struct radius_server *acct_server;

	/**
	 * Retry count and timeout for current acct_server
	 */
	guint acct_retry_count;
	GTimeVal acct_timeval;
	void *acct_timeout;

	/**
	 * Last sent/received packets
	 */
	void *spacket, *rpacket;
	/**
	 * Last received EAP packet (extracted from RADIUS packet)
	 */
	void *eap_packet;

	/**
	 * Socket where packet was sent (for matching with replys)
	 * FIXME - remove
	 */
	void *socket;

	/**
	 * for matching received packet from RADIUS with session
	 */
	void *proxy_state;

	/**
	 * session in state machines which this radius session belongs
	 */
	void *session;

	/**
	 * MSK
	 */
	void *msk;
	int msk_len;

	/**
	 * Mutex for locking some critical code
	 */
	GMutex *mutex;
};

/**
 * Sockets used for RADIUS connectivity - shared among ALL communication
 * to ANY RADIUS server.
 * Note: if things work without need for 'identifier handling' routines
 *       (curently removed or commented), we can simplify code with removing
 *       this structure and replacing it with just 'struct network_socket'.
 *       'radius_init' and 'radius_unload' would need a slight modification
 *       (c/p from radius_socket_new and *free functions).
 */
struct radius_socket {
	void *socket;
	gint refcnt;
};

/**
 * RADIUS internal data
 */
struct radius_data {
	/**
	 * thread waiting something to be returned from RADIUS server(s)
         */
	GThread *thread;

	/**
	 * queue for received packets from network
         */
	GAsyncQueue *queue;

	/**
	 * queue for messages to uper layer
         */
	GAsyncQueue *upper_queue;
	/**
	 * Sockets in use by radius subsystem
	 */
	GSList *sockets;
	/**
	 * RADIUS sessions - presented by list of 'radius_state' structures
	 */
	GSList *rsessions;
	/**
	 * ikev2 data
	 */
	struct ikev2_data *ikev2_data;

	/**
	 * Mutex for preventing errors when simultaneous actions use this
	 * structure or its elements
	 */
	GMutex *mutex;
};
/***************************************************************************
 *                                  EAP                                    *
 ***************************************************************************/

/**
 * EAP packet header
 */
struct radius_eap_hdr {
	/**
	 * The Code field is one octet and identifies the Type of EAP packet.
	 * EAP Codes are assigned as follows:
	 *
	 * 1 Request
         * 2 Response
         * 3 Success
         * 4 Failure
         */
	guint8 code;

	/**
	 * "The Identifier field is one octet and aids in matching Responses
	 * with Requests." - in other words only for single req/resp is same
	 */
	guint8 identifier;

	/**
	 * Length of eap packet including code, identifier and lengthh
	 * network byte order (g_htons, g_ntohs)
	 */
	guint16 length;

} __attribute__((packed));

/**
 * EAP additional data in packet, for Request/Response packet types only
 */
struct radius_eap_data {
	/**
	 * Type of Request or Response
	 */
	guint8 type;
	/**
	 * Data for type
	 */
	guint8 type_data[1];
} __attribute__((packed));

/***************************************************************************
 *                                  RADIUS                                 *
 ***************************************************************************/

/**
 * RADIUS packet header
 */
struct radius_hdr {
	/**
	 * RADIUS Codes (decimal) are assigned as follows:
	 *
	 * 1	Access-Request
	 * 2	Access-Accept
	 * 3	Access-Reject
	 * 4	Accounting-Request
	 * 5	Accounting-Response
	 * 11	Access-Challenge
	 * 12	Status-Server (experimental)
	 * 13	Status-Client (experimental)
	 * 255	Reserved
	 */
	guint8 code;

	/**
	 * The Identifier field MUST be changed whenever the content of the
	 * Attributes field changes, and whenever a valid reply has been
	 * received for a previous request.  For retransmissions, the
	 * Identifier MUST remain unchanged.
	 */
	guint8 identifier;

	/**
	 * RADIUS packet length, including header
	 *
	 * network byte order -- use g_htons, g_ntohs
	 */
	guint16 length;

	/**
	 * RADIUS authenticator
	 */
	guint8 authenticator[16];

} __attribute__((packed));

/**
 * RADIUS attributes header
 */
struct radius_attr_hdr {
        /**
         *Type of attribute
         */
	guint8 type;
        /**
         * Length of attribute, including header
         */
	guint8 length;
} __attribute__((packed));

/**
 *
 * DEFINE'S and enum's
 *
 */

/***************************************************************************
 *                                  EAP                                    *
 ***************************************************************************/

#define EAP_ID_REQ_MAX_LEN 1020
#define EAP_MSK_LEN 64
#define EAP_EMSK_LEN 64

enum {
	EAP_TYPE_NONE = 0,
	EAP_TYPE_IDENTITY = 1,    /* RFC 3748 */
	EAP_TYPE_NOTIFICATION = 2,/* RFC 3748 */
	EAP_TYPE_NAK = 3,         /* Response only, RFC 3748 */
	EAP_TYPE_MD5 = 4,         /* RFC 3748 */
	EAP_TYPE_OTP = 5,         /* RFC 3748 */
	EAP_TYPE_GTC = 6,         /* RFC 3748 */
	EAP_TYPE_TLS = 13,        /* RFC 2716 */
	EAP_TYPE_LEAP = 17,       /* Cisco proprietary */
	EAP_TYPE_SIM = 18,        /* RFC 4186 */
	EAP_TYPE_TTLS = 21,       /* draft-ietf-pppext-eap-ttls-02.txt */
	EAP_TYPE_AKA = 23,        /* RFC 4187 */
	EAP_TYPE_PEAP = 25,       /* draft-josefsson-pppext-eap-tls-eap-06.txt */
	EAP_TYPE_MSCHAPV2 = 26,   /* draft-kamath-pppext-eap-mschapv2-00.txt */
	EAP_TYPE_TLV = 33,        /* draft-josefsson-pppext-eap-tls-eap-07.txt */
	EAP_TYPE_FAST = 43,       /* draft-cam-winget-eap-fast-00.txt */
	EAP_TYPE_PAX = 46,        /* draft-clancy-eap-pax-04.txt */
	EAP_TYPE_EXPANDED = 254,  /* RFC 3748 */
	EAP_TYPE_PSK = 255,       /* EXPERIMENTAL - type not yet allocated
			                   * draft-bersani-eap-psk-09 */
	EAP_TYPE_SAKE = 255,      /* EXPERIMENTAL - type not yet allocated
			                   * draft-vanderveen-eap-sake-01 */
	EAP_TYPE_GPSK = 255,      /* EXPERIMENTAL - type not yet allocated
			                   * draft-clancy-emu-eap-shared-secret-00.txt */
};


/***************************************************************************
 *                                  RADIUS                                 *
 ***************************************************************************/

#define RADIUS_MAX_ATTR_LEN (255 - sizeof(struct radius_attr_hdr))
#define RADIUS_MIN_PACKET_LEN 20
#define RADIUS_MAX_PACKET_LEN 4096

enum {	RADIUS_CODE_ACCESS_REQUEST = 1,
	RADIUS_CODE_ACCESS_ACCEPT = 2,
	RADIUS_CODE_ACCESS_REJECT = 3,
	RADIUS_CODE_ACCOUNTING_REQUEST = 4,
	RADIUS_CODE_ACCOUNTING_RESPONSE = 5,
	RADIUS_CODE_ACCESS_CHALLENGE = 11,
	RADIUS_CODE_STATUS_SERVER = 12,
	RADIUS_CODE_STATUS_CLIENT = 13,
	RADIUS_CODE_RESERVED = 255
};

enum {	RADIUS_ATTR_USER_NAME = 1,
	RADIUS_ATTR_USER_PASSWORD = 2,
	RADIUS_ATTR_NAS_IP_ADDRESS = 4,
	RADIUS_ATTR_NAS_PORT = 5,
	RADIUS_ATTR_FRAMED_MTU = 12,
	RADIUS_ATTR_STATE = 24,
	RADIUS_ATTR_CLASS = 25,
	RADIUS_ATTR_VENDOR_SPECIFIC = 26,
	RADIUS_ATTR_SESSION_TIMEOUT = 27,
	RADIUS_ATTR_IDLE_TIMEOUT = 28,
	RADIUS_ATTR_TERMINATION_ACTION = 29,
	RADIUS_ATTR_CALLED_STATION_ID = 30,
	RADIUS_ATTR_CALLING_STATION_ID = 31,
	RADIUS_ATTR_NAS_IDENTIFIER = 32,
	RADIUS_ATTR_PROXY_STATE = 33,
	RADIUS_ATTR_ACCT_STATUS_TYPE = 40,
	RADIUS_ATTR_ACCT_DELAY_TIME = 41,
	RADIUS_ATTR_ACCT_INPUT_OCTETS = 42,
	RADIUS_ATTR_ACCT_OUTPUT_OCTETS = 43,
	RADIUS_ATTR_ACCT_SESSION_ID = 44,
	RADIUS_ATTR_ACCT_AUTHENTIC = 45,
	RADIUS_ATTR_ACCT_SESSION_TIME = 46,
	RADIUS_ATTR_ACCT_INPUT_PACKETS = 47,
	RADIUS_ATTR_ACCT_OUTPUT_PACKETS = 48,
	RADIUS_ATTR_ACCT_TERMINATE_CAUSE = 49,
	RADIUS_ATTR_ACCT_MULTI_SESSION_ID = 50,
	RADIUS_ATTR_ACCT_LINK_COUNT = 51,
	RADIUS_ATTR_ACCT_INPUT_GIGAWORDS = 52,
	RADIUS_ATTR_ACCT_OUTPUT_GIGAWORDS = 53,
	RADIUS_ATTR_EVENT_TIMESTAMP = 55,
	RADIUS_ATTR_NAS_PORT_TYPE = 61,
	RADIUS_ATTR_TUNNEL_TYPE = 64,
	RADIUS_ATTR_TUNNEL_MEDIUM_TYPE = 65,
	RADIUS_ATTR_CONNECT_INFO = 77,

	RADIUS_ATTR_EAP_MESSAGE = 79,
	RADIUS_ATTR_MESSAGE_AUTHENTICATOR = 80,

	RADIUS_ATTR_TUNNEL_PRIVATE_GROUP_ID = 81,
	RADIUS_ATTR_ACCT_INTERIM_INTERVAL = 85,

	RADIUS_ATTR_NAS_PORT_ID = 87,

	RADIUS_ATTR_NAS_IPV6_ADDRESS = 95,

	MS_MPPE_SEND_KEY = 16,
	MS_MPPE_RECV_KEY = 17
};

/***************************************************************************
 *                            EAP - internal                               *
 ***************************************************************************/

guint8 radius_eap_get_code(struct radius_eap_hdr *);
guint16 radius_eap_get_length(struct radius_eap_hdr *);
void radius_eap_dump_packet(void *, gchar *);

/***************************************************************************
 *                            RADIUS - internal                            *
 ***************************************************************************/
int radius_send_packet(void *, gchar *, guint, struct radius_state *);
gpointer radius_main_thread(gpointer);
gpointer radius_auth_timeout(gpointer);
void *radius_create_access_request_packet(void *, void *, gchar *, guint,
		guint8, struct radius_state *);

struct radius_state *radius_verify_packet (void *, int);
struct radius_attr_hdr *radius_get_attribute(struct radius_hdr *, guint8);
void *radius_get_eap_packet(struct radius_hdr *);

void radius_get_authenticator(guint8 *);
char *radius_get_server_secret(struct radius_state *);
char *radius_get_client_nas_id(struct radius_state *);
guint32 radius_get_client_nas_port(struct radius_state *);
guint8 radius_get_new_identifier();
int radius_get_packet_len(struct radius_hdr *);
guint8 radius_get_packet_code(struct radius_hdr *);
guint8 radius_get_packet_identifier(struct radius_hdr *);
void radius_dump_packet(struct radius_hdr *, gchar *);
gchar *radius_hextostring (void *, gint);
struct radius_socket *radius_socket_new ();
void radius_socket_free (struct radius_socket *);

/***************************************************************************
 * RADIUS - Interface to AAA wrapper                                       *
 ***************************************************************************/
int radius_init(GAsyncQueue *, struct ikev2_data *);
void radius_unload();

struct radius_state *radius_new_client(void *, void *, char *, int, int *);
int radius_request(struct radius_state *, void *);
int radius_get_response(struct radius_state *, void **);

void radius_state_free(struct radius_state *);

int radius_get_session_timeout_attribute_value(struct radius_state *);

gboolean radius_get_msk(struct radius_state *,  gchar **, size_t *);

/***************************************************************************
 * Functions that manipulate radius_server structure                       *
 ***************************************************************************/
struct radius_server *radius_server_new();
void radius_server_free(struct radius_server *);

/***************************************************************************
 * Functions that manipulate configuration structure                       *
 ***************************************************************************/
struct radius_config *radius_config_new();
void radius_config_free(struct radius_config *);
void radius_config_set_id(struct radius_config *, gchar *);
void radius_config_add_auth_server(struct radius_config *,
                struct radius_server *);
void radius_config_add_acct_server(struct radius_config *,
                struct radius_server *);
struct radius_config *radius_config_find_by_id (GSList *, char *);
void radius_config_dump(struct radius_config *);

/***************************************************************************
 * External dependencies                                                   *
 ***************************************************************************
aaa_wrapper.c:	aaa_msg_new
crypto.ch:	prf_hmac_md5, rand_bytes, 
network.ch:	network_socket_register_queue, network_socket_unregister,
		network_send_packet, network_msg_get_data, network_msg_free
timeout.ch	timeout_register_thread, timeout_cancel
*/

#endif /* __RADIUS_H */

