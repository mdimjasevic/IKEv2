/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __NETWORK_MSG_C

#define LOGGERNAME	"network_msg"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif /* HAVE_UNISTD_H */
#include <errno.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <net/if.h>

#include <linux/pfkeyv2.h>
#include <linux/ipsec.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "config.h"
#include "network_msg.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

/*******************************************************************************
 * FUNCTIONS THAT MANIPULATE NETWORK_MSG STRUCTURE
 ******************************************************************************/

/**
 * Function to allocate memory for network message.
 *
 * \return Pointer to a new message structure or NULL in case of an error.
 */
struct network_msg *network_msg_alloc(void)
{
	struct network_msg *msg;

	LOG_FUNC_START(1);

	msg = g_malloc0(sizeof(struct network_msg));

	LOG_FUNC_END(1);

	return msg;
}

/**
 * Function to free memory occupied by network message.
 *
 * @param msg	Pointer to a message structure
 */
void network_msg_free(struct network_msg *msg)
{

	LOG_FUNC_START(1);

	if (msg) {
		if (msg->buffer)
			g_free (msg->buffer);

		if (msg->srcaddr)
			netaddr_free(msg->srcaddr);

		if (msg->dstaddr)
			netaddr_free(msg->dstaddr);

		g_free (msg);
	}

	LOG_FUNC_END(1);
}

/**
 * Given a network_msg structure return IP address from which it was sent.
 *
 * @param msg	Network message
 *
 * \return Address, or NULL in case of an error
 *
 * This function removes address from structure before returning so it's
 * callees duty to free this structre.
 */
struct netaddr *network_msg_get_srcaddr(struct network_msg *msg)
{
	struct netaddr *addr;

	LOG_FUNC_START(1);

	if (msg == NULL)
		return NULL;

	addr = msg->srcaddr;
	msg->srcaddr = NULL;

	LOG_FUNC_END(1);

	return addr;
}

/**
 * Given a network_msg structure return IP address to which it was sent.
 *
 * @param msg	Network message
 *
 * \return Address, or NULL in case of an error
 *
 * This function removes address from structure before returning so it's
 * callees duty to free this structre.
 */
struct netaddr *network_msg_get_dstaddr(struct network_msg *msg)
{
	struct netaddr *addr;

	LOG_FUNC_START(1);

	if (msg == NULL)
		return NULL;

	addr = msg->dstaddr;
	msg->dstaddr = NULL;

	LOG_FUNC_END(1);

	return addr;
}

/**
 * Given a network_msg structure return received data.
 *
 * @param msg		Network message
 * @param buffer	Output parameter with pointer to received data
 *
 * \return Data size, or NULL in case of an error. In case of an error
 *         buffer is also NULL.
 *
 * After this function returns pointer to data, the data is detached
 * from network_msg structure and so the callee has to free buffer.
 */
ssize_t network_msg_get_data(struct network_msg *msg, void **buffer)
{

	LOG_FUNC_START(1);

	if (msg == NULL || msg->buffer == NULL) {
		*buffer = NULL;
		return -1;
	}

	*buffer = msg->buffer;
	msg->buffer = NULL;

	LOG_FUNC_END(1);

	return msg->size;
}


/**
 * Given a network_msg structure return socket to which it was sent.
 *
 * @param msg	Network message
 *
 * \return Address, or NULL in case of an error
 *
 * This function removes address from structure before returning so it's
 * callees duty to free this structre.
 */
struct network_socket *network_msg_get_ns(struct network_msg *msg)
{
	struct network_socket *ns;

	LOG_FUNC_START(1);

	if (msg == NULL)
		return NULL;

	ns = msg->ns;
	msg->ns = NULL;

	LOG_FUNC_END(1);

	return ns;
}

