/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CRYPTO_H
#define __CRYPTO_H

#include <openssl/des.h>
#include <openssl/dh.h>

/*
 * Number of bytes from random source seeded to openssl PRNG.
 */
#define RAND_INIT_SIZE	1024

/*
 * Diffie-Hellman related functions
 */
DH *dh_ke_generate(guint16);

/*
 * Functions for collecting random data
 */
int rand_bytes(unsigned char *, int);

/*
 * Pseudo-random functions
 */
void prf_hmac_md5(unsigned char *, int, unsigned char *, int, caddr_t);
void prf_hmac_sha1(unsigned char *, int, unsigned char *, int, caddr_t);
int prf(transform_t *, unsigned char *, int, unsigned char *, int, char **);
int prf_plus(transform_t *, char *, guint32, char *, guint32,
		char **, guint32);
guint32 prf_digest_length_get(guint32);
guint16 prf_has_fixed_key_length(transform_t *transform);

/*
 * Encryption related functions
 */
void xcrypt_des(char *, guint32, DES_cblock *, char *, int);
guint32 encr_keylen_get(transform_t *);
guint32 encr_blocklen_get(transform_t *);
void encr_build_key(transform_t *, char *);
void encrypt_3des(char *, guint32, DES_cblock *, char *, int);
void crypto_encrypt(char *, guint32, transform_t *, char *, char *, guint32);
void crypto_decrypt(char *, guint32, transform_t *, char *, char *, guint32);

/*
 * Integrity function
 */
guint32 integ_keylen_get(transform_t *);
guint32 integ_chksumlen_get(transform_t *);
int integ(transform_t *, char *, guint32, char *, guint32, char **);

void crypto_unload(void);
int crypto_init(const char *);

#endif /* __CRYPTO_H */
