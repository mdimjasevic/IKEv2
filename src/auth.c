/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#define __AUTH_C

#define LOGGERNAME	"auth"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <ctype.h>

#include <openssl/bn.h>
#include <openssl/dh.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "ts.h"
#include "auth.h"
#ifdef CERT_AUTH
#include "cert.h"
#endif /* CERT_AUTH */

#ifdef DMALLOC
#include "dmalloc.h"
#endif

#define MAXLEN		512 

FILE *config_file_in;

/*******************************************************************************
 * ID structure functions
 ******************************************************************************/

/**
 * Allocate a new ID structure
 */
struct id *id_new()
{
	struct id *id;

	id = g_malloc0(sizeof(struct id));
	id->type = IKEV2_IDT_ANY;

	return id;
}

/**
 * Allocate a new ID structure and initialize it from string representation
 *
 * @param idstr		String representation of the ID
 *
 * \return Pointer to ID, or NULL in case of an error
 *
 * In case of successful execution of this function, input argument idstr
 * is no longer used, so it's the task of the callee to free it, or do
 * whatever it want's to do.
 *
 * The ID has the following format
 *
 * <type>:<value>
 *
 * The value depends on the type, and the type can be
 *
 *	ipv4	IPv4 address, e.g. 192.168.0.5
 *	ipv6	IPv4 address, e.g. 2001::1
 *	ip	Autodetect IPv4 or IPv6 address
 *	fqdn	Fully qualified domain name, e.g. vpngw.example.com
 *	mail	Mail address, vpngw@example.com
 *	key	Key, 0x01234567890123454677
 *	dn	Distinguished name, X.509 type, eg.
 *		"C=HR, O=FER, OU=ZEMRIS, CN=Stjepan Gros"
 *
 * Braces ([]) are allowed around value.
 *
 * This function recognizes glob character as well as the ways of
 * specifying the groups of IDs.
 */
struct id *id_new_from_str(gchar *idstr)
{
	struct id *id;
	struct in_addr in;
	struct in6_addr in6;
	gchar *p;
	int i, val, offset, netmask;
	gboolean brace;

	brace = FALSE;

	if (!strcmp(idstr, ID_ANY)) {
		id = id_new();
		id->flags = ID_F_ANY;
		return id;
	}

	if (!strncmp(idstr, "ipv4:", 5) || !strncmp(idstr, "ip:", 3)) {

		if (!strncmp(idstr, "ipv4:", 5))
			offset = 5;
		else
			offset = 3;

		if (*(idstr + offset) == '[') {
			offset++;
			brace = TRUE;
			if (idstr[strlen(idstr) - 1] != ']') {
				LOG_ERROR("Missing closing brace");
				return NULL;
			}

			idstr[strlen(idstr) - 1] = 0;
		}

		/*
		 * Check for the special word any
		 */
		if (!strcmp(idstr + offset, ID_ANY)) {
			id = id_new();
			id->type = IKEV2_IDT_IPV4_ADDR;
			id->flags = ID_F_ANY;
			return id;
		}

		/*
		 * Check if network mask is given.
		 */
		if ((p = strchr(idstr + offset, '/')) != NULL) {
			netmask = atoi(p);
			*p = 0;
		}

		/*
		 * Try to convert IP address
		 */
		if (inet_pton(AF_INET, idstr + offset, &in)) {

			/*
			 * If successful, first check if network mask is
			 * given and if it is within bounds.
			 */
			if (p && (netmask > 32 || netmask < 0)) {
				LOG_WARNING("Illegal network mask!");
				*p = '/';
				return NULL;
			}

			id = id_new();

			id->type = IKEV2_IDT_IPV4_ADDR;
			id->ts = ts_new();

			if (p) {
				ts_set_saddr_from_netaddr(id->ts, (void *)&in,
						netmask);
				ts_set_eaddr_from_netaddr(id->ts, (void *)&in,
						netmask);
			} else {
				ts_set_saddr_from_inaddr(id->ts, (void *)&in);
				ts_set_eaddr_from_inaddr(id->ts, (void *)&in);
			}

			ts_set_sport(id->ts, 0);
			ts_set_eport(id->ts, 65535);

			if (brace)
				idstr[strlen(idstr)] = ']';

			if (p)
				*p = '/';

			return id;

		} else
			if (offset > 3) {
				LOG_WARNING("Unrecognized IPv4 address %s",
						idstr + offset);
				if (p)
					*p = '/';

				return NULL;
			}

		/*
		 * Return back slash character
		 */
		if (p)
			*p = '/';
	}

	if (brace) {
		brace = FALSE;
		idstr[strlen(idstr)] = ']';
	}

	if (!strncmp(idstr, "ipv6:", 5) || !strncmp(idstr, "ip:", 3)) {

		if (!strncmp(idstr, "ipv6:", 5))
			offset = 5;
		else
			offset = 3;

		if (*(idstr + offset) == '[') {
			offset++;
			brace = TRUE;
			if (idstr[strlen(idstr) - 1] != ']') {
				LOG_ERROR("Missing closing brace");
				return NULL;
			}

			idstr[strlen(idstr) - 1] = 0;
		}

		if (!strcmp(idstr + offset, ID_ANY)) {
			id = id_new();
			id->type = IKEV2_IDT_IPV4_ADDR;
			id->flags = ID_F_ANY;
			return id;
		}

		/*
		 * Check if network mask is given.
		 */
		if ((p = strchr(idstr + offset, '/')) != NULL) {
			netmask = atoi(p);
			*p = 0;
		}

		/*
		 * Try to convert IP address
		 */
		if (inet_pton(AF_INET6, idstr + offset, &in6)) {

			/*
			 * Now, check if netmask was given and if it
			 * is within bounds
			 */
			if (p && (netmask > 128 || netmask < 0)) {
				LOG_WARNING("Illegal network mask!");
				*p = '/';
				return NULL;
			}

			id = id_new();

			id->type = IKEV2_IDT_IPV6_ADDR;
			id->ts = ts_new();

			if (p) {
				ts_set_saddr_from_netaddr6(id->ts, (void *)&in6,
						netmask);
				ts_set_eaddr_from_netaddr6(id->ts, (void *)&in6,
						netmask);
			} else {
				ts_set_saddr_from_in6addr(id->ts, (void *)&in6);
				ts_set_eaddr_from_in6addr(id->ts, (void *)&in6);
			}

			ts_set_sport(id->ts, 0);
			ts_set_eport(id->ts, 65535);

			if (brace)
				idstr[strlen(idstr)] = ']';

			if (p)
				*p = '/';

			return id;

		} else {
			LOG_WARNING("Error in ID string represenation");

			if (p)
				*p = '/';

			return NULL;
		}

		/*
		 * Return back slash character
		 */
		if (p)
			*p = '/';
	}

	if (brace) {
		brace = FALSE;
		idstr[strlen(idstr)] = ']';
	}

	if (!strncmp(idstr, "fqdn:", 5)) {

		offset = 5;
		if (*(idstr + offset) == '[') {
			offset++;
			brace = TRUE;

			if (idstr[strlen(idstr) - 1] != ']') {
				LOG_ERROR("Missing closing brace");
				return NULL;
			}

			idstr[strlen(idstr) - 1] = 0;
		}

		id = id_new();
		id->type = IKEV2_IDT_FQDN;

		if (!strcmp(idstr + offset, ID_ANY)) {
			id->flags = ID_F_ANY;
			return id;
		}

		/*
		 * If the first character is * mark that there is a
		 * single glob character and skip it.
		 */
		if (*(idstr + offset) == '*') {
			id->flags |= ID_F_GLOBCHAR;
			offset++;
		}

		id->len = strlen(idstr + offset);

		id->id = g_memdup(idstr + offset, id->len);

		if (brace) {
			brace = FALSE;
			idstr[strlen(idstr)] = ']';
		}
		return id;

	}

	if (brace) {
		brace = FALSE;
		idstr[strlen(idstr)] = ']';
	}

	if (!strncmp(idstr, "mail:", 5)) {

		offset = 5;
		if (*(idstr + offset) == '[') {
			offset++;
			brace = TRUE;

			if (idstr[strlen(idstr) - 1] != ']') {
				LOG_ERROR("Missing closing brace");
				return NULL;
			}

			idstr[strlen(idstr) - 1] = 0;
		}

		id = id_new();
		id->type = IKEV2_IDT_RFC822_ADDR;

		if (!strcmp(idstr + offset, ID_ANY)) {
			id->flags = ID_F_ANY;
			return id;
		}

		/*
		 * If the first character is * just skip it since
		 * we assume that if the first character is @ we
		 * do match on domain name.
		 */
		if (*(idstr + offset) == '*')
			offset++;

		id->len = strlen(idstr + offset);

		id->id = g_memdup(idstr + offset, id->len);

		if (brace) {
			brace = FALSE;
			idstr[strlen(idstr)] = ']';
		}
		return id;

	}

	if (brace) {
		brace = FALSE;
		idstr[strlen(idstr)] = ']';
	}

	if (!strncmp(idstr, "dn:", 3)) {

		offset = 3;
		if (*(idstr + offset) == '[') {
			offset++;

			brace = TRUE;
			if (idstr[strlen(idstr) - 1] != ']') {
				LOG_ERROR("Missing closing brace");
				return NULL;
			}

			idstr[strlen(idstr) - 1] = 0;
		}

		id = id_new();
		id->type = IKEV2_IDT_DER_ASN1_DN;

		if (!strcmp(idstr + offset, ID_ANY)) {
			id->flags = ID_F_ANY;
			return id;
		}

		id->derasn1dn = crypto_string_to_derasn1dn(idstr + offset);

		if (id->derasn1dn == NULL) {
			LOG_WARNING("Unrecognized ID");
			id_free(id);
			return NULL;
		}

		if (brace) {
			brace = FALSE;
			idstr[strlen(idstr)] = ']';
		}
		return id;

	}

	if (brace) {
		brace = FALSE;
		idstr[strlen(idstr) - 1] = ']';
	}

	if (!strncmp(idstr, "key:0x", 6)) {

		id = id_new();
		id->type = IKEV2_IDT_KEY_ID;

		idstr += 6;
		id->len = (strlen(idstr) + 1) >> 1;
		id->id = g_malloc0(id->len);

		i = 0;
		while(*idstr) {
			if (isdigit(*idstr))
				val = *idstr - '0';
			else if (isxdigit(*idstr))
				val = toupper(*idstr) - 'A' + 10;
			else {
				LOG_WARNING("Unrecognized hex digit %c",
						*idstr);
				id_free(id);
				return NULL;
			}

			if (!(i & 1)) {
				id->id[i >> 1] = val << 4;
			} else {
				id->id[i >> 1] |= val;
			}

			i++;
			idstr++;
		}

		if ((i & 1) == 0)
			LOG_WARNING("Odd number of digits in KEY_ID");

		return id;
	} else
		LOG_DEBUG("Unsupported ID '%s'", idstr);

	return NULL;
}

/**
 * Free ID structure
 *
 * @param id
 */
void id_free(struct id *id)
{
	if (id) {

		switch(id->type) {
		case IKEV2_IDT_IPV4_ADDR:
		case IKEV2_IDT_IPV6_ADDR:
			ts_free(id->ts);
			break;

		case IKEV2_IDT_FQDN:
		case IKEV2_IDT_RFC822_ADDR:
			g_free(id->id);
			break;

		case IKEV2_IDT_DER_ASN1_DN:
			crypto_derasn1dn_free(id->derasn1dn);
			break;

		case IKEV2_IDT_DER_ASN1_GN:
			g_assert_not_reached();
		}
	}
}

/**
 * Free list of ID structures
 *
 * @param idl
 */
void id_list_free(GSList *idl)
{
	while (idl) {
		id_free(idl->data);
		idl = idl->next;
	}
}

/**
 * Return size of the ID itself
 *
 * @param id
 *
 * \return
 */
gint id_get_size(struct id *id)
{
	switch (id->type) {
	case IKEV2_IDT_IPV4_ADDR:
		return 4;

	case IKEV2_IDT_FQDN:
	case IKEV2_IDT_RFC822_ADDR:
	case IKEV2_IDT_KEY_ID:
		return id->len;

	case IKEV2_IDT_IPV6_ADDR:
		return 16;

	case IKEV2_IDT_DER_ASN1_DN:
		return strlen(id->derasn1dn);

	case IKEV2_IDT_DER_ASN1_GN:
	default:
		g_assert_not_reached();
	}
}

/**
 * Return pointer to a first octet of the ID
 *
 * @param id
 *
 * \return
 */
gpointer id_get_ptr(struct id *id)
{
	switch (id->type) {
	case IKEV2_IDT_IPV4_ADDR:
	case IKEV2_IDT_IPV6_ADDR:
		netaddr_get_ipaddr(ts_get_saddr(id->ts));

	case IKEV2_IDT_FQDN:
	case IKEV2_IDT_RFC822_ADDR:
	case IKEV2_IDT_KEY_ID:
		return id->id;

	case IKEV2_IDT_DER_ASN1_DN:
		return id->derasn1dn;
		break;

	case IKEV2_IDT_DER_ASN1_GN:
		g_assert_not_reached();
		break;

	default:
		LOG_DEBUG("Unknown ID type");
		g_assert_not_reached();
	}

	return NULL;
}

/**
 * Search for a given ID in a list
 *
 * @param id_list
 * @param id
 *
 * \return TRUE if found, otherwise FALSE
 */
gboolean id_find(GSList *id_list, struct id *id)
{
	while (id_list) {
		if (id_is_equal(id, id_list->data))
			return TRUE;

		id_list = id_list->next;
	}

	return FALSE;
}

/**
 * Check if ID1 is subset of ID2
 *
 * @param id1
 * @param id2
 *
 * \return TRUE if relation holds, otherwise false
 *
 * Note that ID1 can not be wildcard of any form
 */
gboolean id_is_subset(struct id *id1, struct id *id2)
{
	gchar *p;

	/*
	 * If ID2 is NULL than we return FALSE
	 */
	if (!id2)
		return FALSE;

	/*
	 * If ID1 is NULL than treat it as subset of any ID
	 * But, can this case legally occur during execution of
	 * ikev2?
	 */
	if (!id1)
		return TRUE;

	/*
	 * If ID2 specifies any ID value, then return TRUE, or
	 * any ID value of the same type as ID1
	 */
	if (id2->flags & ID_F_ANY &&
			(id2->type == IKEV2_IDT_ANY || id1->type == id2->type))
		return TRUE;

	/*
	 * If id1 and id2 are of different types then there is no match
	 */
	if (id2->type != id1->type && id2->type != IKEV2_IDT_ANY)
		return FALSE;

	switch (id1->type) {
	case IKEV2_IDT_IPV4_ADDR:
	case IKEV2_IDT_IPV6_ADDR:
		return ts_is_subset(id1->ts, id2->ts);

	case IKEV2_IDT_FQDN:
		if (id2->id[0] == '.') {
			/*
			 * Compare sizes, if wildcard style FQDN
			 * is longer than the particular ID then
			 * return FALSE as there is no way that
			 * id1 can be subset of id2.
			 */
			if (strlen(id2->id) < strlen(id1->id))
				return FALSE;

			/*
			 * Find the first character in ID1 that
			 * will be compared to ID2
			 */
			p = id2->id + strlen(id2->id) - strlen(id1->id);

			/*
			 * If there was star given in the ID2, and
			 * there is one more dot, then, there is no
			 * match. In other words, a.b.c is _not_
			 * subset of *.c, but it _is_ subset of .c
			 */
			if (id1->flags & ID_F_GLOBCHAR &&
					strchr(id1->id, '.') > p)
				return FALSE;

			/*
			 * Do the comparison...
			 */
			return !strcmp(p, id2->id);

		} else {
			if (id1->len != id2->len)
				return FALSE;

			return !memcmp(id1->id, id2->id, id1->len);
		}
		break;

	case IKEV2_IDT_RFC822_ADDR:
		if (id2->id[0] == '@')
			return !memcmp(strchr(id1->id, '@') + 1, id2->id,
					MIN(id1->len, id2->len));
		else
			return !memcmp(id1->id, id2->id, 
					MIN(id1->len, id2->len));
		break;

	case IKEV2_IDT_KEY_ID:
		/*
		 * KEY_ID is only compared for equality
		 */
		return !memcmp(id1->id, id2->id,
				MIN(id1->len, id2->len));

	case IKEV2_IDT_DER_ASN1_DN:
		return crypto_derasn1dn_cmp(id1->derasn1dn, id2->derasn1dn);

	case IKEV2_IDT_DER_ASN1_GN:
		g_assert_not_reached();

	default:
		LOG_DEBUG("Unknown ID type %u", id1->type);
		g_assert_not_reached();
	}
}

/**
 * Check if ID2 is strictly equal of ID1
 *
 * @param id1
 * @param id2
 *
 * \return TRUE if the two IDs are equal, otherwise false
 *
 * @todo This function, as of yet, does very simple comparison
 */
gboolean id_is_equal(struct id *id1, struct id *id2)
{

	/*
	 * If one of the IDs is NULL return FALSE
	 */
	if (!id1 || !id2)
		return FALSE;

	/*
	 * IDs of different types can not be compared
	 */
	if (id1->type != id2->type)
		return FALSE;

	switch (id1->type) {
	case IKEV2_IDT_IPV4_ADDR:
	case IKEV2_IDT_IPV6_ADDR:
		return ts_is_equal(id1->ts, id2->ts);

	case IKEV2_IDT_FQDN:
	case IKEV2_IDT_RFC822_ADDR:
	case IKEV2_IDT_KEY_ID:
		return !memcmp(id1->id, id2->id,
				MIN(id1->len, id2->len));

	case IKEV2_IDT_DER_ASN1_DN:
		return crypto_derasn1dn_cmp(id1->derasn1dn, id2->derasn1dn);

	case IKEV2_IDT_DER_ASN1_GN:
		g_assert_not_reached();

	default:
		LOG_DEBUG("Unknown ID types");
		g_assert_not_reached();
	}

	return FALSE;
}

/**
 * Dump ID structure to log
 */
void id_dump(char *loggername, struct id *id)
{
	gchar *p;

	p = id_id2str(id);

	LOG_DEBUG("%s", p);
	
	g_free(p);
}

/**
 * Get string representation of ID
 *
 * @param id
 *
 * \return Pointer to a string. Caller has to free this storage with
 * a call to g_free
 */
gchar *id_id2str(struct id *id)
{
	gchar *c = NULL, *c1;
	gchar ipaddr[INET6_ADDRSTRLEN];
	gint i;

	if (!id)
		return(g_strdup("<null>"));

	switch (id->type) {
	case IKEV2_IDT_FQDN:
		if (id->flags == ID_F_ANY) {
			c = strdup("fqdn:any");
			break;
		}

		c = g_malloc(id->len + 6);
		strcpy(c, "fqdn:");
		memcpy(c + 5, id->id, id->len);
		c[id->len + 5] = 0;
		break;

	case IKEV2_IDT_RFC822_ADDR:
		if (id->flags == ID_F_ANY) {
			c = strdup("mail:any");
			break;
		}

		c = g_malloc(id->len + 6);
		strcpy(c, "mail:");
		memcpy(c + 5, id->id, id->len);
		c[id->len + 5] = 0;
		break;

	case IKEV2_IDT_IPV4_ADDR:
		if (id->flags == ID_F_ANY) {
			c = strdup("ipv4:any");
			break;
		} 

		netaddr2str(ts_get_saddr(id->ts), ipaddr, INET6_ADDRSTRLEN);
		c = g_malloc(strlen(ipaddr) + 6);
		strcpy(c, "ipv4:");
		memcpy(c + 5, ipaddr, strlen(ipaddr) + 1);
		break;

	case IKEV2_IDT_IPV6_ADDR:
		if (id->flags == ID_F_ANY) {
			c = strdup("ipv6:any");
			break;
		}

		netaddr2str(ts_get_saddr(id->ts), ipaddr, INET6_ADDRSTRLEN);
		c = g_malloc(strlen(ipaddr) + 6);
		strcpy(c, "ipv6:");
		memcpy(c + 5, ipaddr, strlen(ipaddr) + 1);
		break;

	case IKEV2_IDT_DER_ASN1_DN:
		c1 = crypto_derasn1dn_to_string(id->derasn1dn);
		c = g_malloc(strlen(c1) + 4);
		strcpy(c, "dn:");
		strcat(c, c1);
		g_free(c1);
		break;

	case IKEV2_IDT_KEY_ID:
		c = g_malloc((id->len << 2) + 1);
		c[(id->len << 1) + 1] = 0;
		for (i = 0; i < id->len; i++) {

			c[i << 1] = (id->id[i] >> 4) & 0x0F;
			c[i << 1] += c[i << 1] > 9 ? ('a' - 10) : '0';

			c[(i << 1) + 1] = id->id[i] & 0x0F;
			c[(i << 1) + 1] += c[(i << 1) + 1] > 9
						? ('a' - 10) : '0';

		}
		break;

	case IKEV2_IDT_ANY:
		c = strdup("any");
		break;

	case IKEV2_IDT_DER_ASN1_GN:
		LOG_BUG("Not implemented");
		break;

	default:
		LOG_DEBUG("Unknown ID type");
	}

	return c;
}

/**
 * Get binary representation of ID
 *
 * @param id
 * @param p	Memory buffer where to store ID's binary representation
 *
 * \return Number of octets used in buffer, or -1 in case of an error
 *
 * This function is used to encode ID's value to be transfered in
 * IKEv2's payloads.
 */
gint id_id2bin(struct id *id, gpointer p)
{
	struct netaddr *n;

	/*
	 * If there is no ID signal an error. This could be internal
	 * error.
	 */
	if (!id)
		g_assert_not_reached();

	if (id->flags == ID_F_ANY)
		return -1;

	switch (id->type) {
	case IKEV2_IDT_FQDN:
	case IKEV2_IDT_RFC822_ADDR:
	case IKEV2_IDT_KEY_ID:
		memcpy(p, id->id, id->len);
		return id->len;

	case IKEV2_IDT_IPV4_ADDR:
	case IKEV2_IDT_IPV6_ADDR:
		n = ts_get_saddr(id->ts);
		memcpy(p, netaddr_get_ipaddr(n), netaddr_get_ipaddr_size(n));
		return netaddr_get_ipaddr_size(n);

	case IKEV2_IDT_DER_ASN1_DN:
		memcpy(p, id->derasn1dn, strlen(id->derasn1dn));
		return strlen(id->derasn1dn);

	case IKEV2_IDT_ANY:
	case IKEV2_IDT_DER_ASN1_GN:
	default:
		return -1;
	}
}

/**
 * Generate ID structure from  binary representation of ID
 *
 * @param id
 *
 * \return ID or NULL in case of an error
 *
 * This function is used to decode ID's from the IKEv2's payloads.
 */
struct id *id_bin2id(guint8 type, gint len, gpointer p)
{
	struct id *id;
	struct netaddr *n;

	if (!len || !p)
		g_assert_not_reached();

	id = id_new();
	id->type = type;

	switch (type) {
	case IKEV2_IDT_FQDN:
	case IKEV2_IDT_RFC822_ADDR:
	case IKEV2_IDT_KEY_ID:
		id->id = g_malloc(len);
		memcpy(id->id, p, len);
		id->len = len;
		break;

	case IKEV2_IDT_IPV4_ADDR:
		n = netaddr_new_from_inaddr(p);
		id->ts = ts_new_and_set(0, 0, 65535, n, n);
		netaddr_free(n);
		break;

	case IKEV2_IDT_IPV6_ADDR:
		n = ts_get_saddr(id->ts);
		memcpy(p, netaddr_get_ipaddr(n), netaddr_get_ipaddr_size(n));
		return netaddr_get_ipaddr_size(n);

	case IKEV2_IDT_DER_ASN1_DN:
		id->derasn1dn = g_malloc(len + 1);
		memcpy(id->derasn1dn, p, len);
		((char *)(id->derasn1dn))[len] = 0;
		break;

	case IKEV2_IDT_ANY:
	case IKEV2_IDT_DER_ASN1_GN:
	default:
		LOG_ERROR("Unhandled ID types!");
		id_free(id);
		id = NULL;
	}

	return id;
}

/**
 * Dump list of ID structures to log
 */
void id_list_dump(char *loggername, GSList *idl)
{
	while (idl) {
		id_dump(loggername, idl->data);
		idl = idl->next;
	}
}

/*******************************************************************************
 * PSK structure functions
 ******************************************************************************/

/**
 * Allocate new psk_item structure
 *
 * \return Pointer to a newly allocated structure or NULL in case of an error
 */
struct psk_item *psk_item_new(void)
{
	struct psk_item *pi;

	LOG_FUNC_START(1);

	pi = g_malloc0(sizeof(struct psk_item));

	LOG_FUNC_END(1);

	return pi;
}

/**
 * Free psk_item structure
 *
 * @param pi	psk_item structure to be released
 */
void psk_item_free(struct psk_item *pi)
{
	if (pi) {
		id_free(pi->id);

		if (pi->secret)
			g_free(pi->secret);

		g_free(pi);
	}
}

/**
 * Free list of psk_item structures
 */
void psk_item_list_free(GSList *pil)
{
	while (pil) {
		psk_item_free(pil->data);
		pil = g_slist_remove(pil, pil->data);
	}
}

/**
 * Set ID of PSK
 *
 * @param pi	PSK item to set ID
 * @param id	ID to set
 *
 * Note that ID must not be freed after this call since reference to it
 * is kept in psk_item structure.
 */
void psk_item_set_id(struct psk_item *pi, struct id *id)
{
	pi->id = id;
}

/**
 * Retrieve ID of a PSK
 *
 * @param pi	psk_item structure
 *
 * \return Pointer to a ID structure.
 *
 * This pointer should be treated as read-only!
 */
struct id *psk_item_get_id(struct psk_item *pi)
{
	return pi->id;
}

/**
 * Set PSK secret
 *
 * @param pi		PSK item to set secret
 * @param secret	secret to set
 *
 * Note that secret must not be freed after this call since reference to it
 * is kept in psk_item structure.
 */
void psk_item_set_secret(struct psk_item *pi, char *secret)
{
	pi->secret = secret;
}

/**
 * Retrieve secret of a PSK structure
 *
 * @param pi	psk_item structure
 *
 * \return Pointer to a secret.
 *
 * This pointer should be treated as read-only!
 */
const char *psk_item_get_secret(struct psk_item *pi)
{
	return pi->secret;
}

/**
 * Search for a secret for a given ID in a list
 *
 * @param ids		List of PSK structures
 * @param id		ID to search for
 *
 * \return Pointer to secret, or NULL if not found
 *
 * Note that callee should not free, or otherwise modify returned value.
 */
char *psk_secret_find_by_id(GSList *ids, struct id *id)
{
	char *secret;
	struct psk_item *psk_item;

	LOG_FUNC_START(1);

	id_dump(LOGGERNAME, id);

	while (ids) {
		psk_item = ids->data;

		if (id_is_equal(id, psk_item->id))
			break;

		ids = ids->next;
	}

	if (!ids)
		secret = NULL;
	else {
//		id_dump(LOGGERNAME, psk_item->id);
		secret = psk_item->secret;
	}

	LOG_FUNC_END(1);

	return secret;
}

/**
 * Search for a given ID in a list
 *
 * @param ids		List of PSK structures
 * @param id		ID to search for
 *
 * \return psk_item with given ID or NULL if not found
 */
struct psk_item *psk_find_by_id(GSList *ids, struct id *id)
{
	struct psk_item *psk_item;

	LOG_FUNC_START(1);

	id_dump(LOGGERNAME, id);

	while (ids) {
		psk_item = ids->data;

		if (id_is_equal(id, psk_item->id))
			break;

		ids = ids->next;
	}

	if (!ids)
		psk_item = NULL;
	else
		id_dump(LOGGERNAME, psk_item->id);

	LOG_FUNC_END(1);

	return psk_item;
}

/**
 * Read pairs of IDs and secrets from a file
 *
 * @param psk_file	File with IDs and shared secrets
 *
 * \returns list of IDs with shared secrets, or NULL in case of an error
 *
 * @todo It should be possible to more flexibly give IDs in this file!
 */
GSList *psk_read_from_file(const char *psk_file)
{
	GSList *items;
	struct psk_item *pi;
	struct id *id;
	char buffer[MAXLEN], sid[MAXLEN], ssecret[MAXLEN];
	char *secret;
	FILE *psk_file_in;

	LOG_FUNC_START(1);

	items = NULL;

	if ((psk_file_in = fopen(psk_file, "r")) == NULL) {
		LOG_ERROR("Could not open psk file %s", psk_file);
		goto out;
	}

	while (fgets(buffer, MAXLEN, psk_file_in) != NULL) {
		if (sscanf(buffer, "%512s%512s\n", sid, ssecret) == 2) {

			id = NULL;

			id = id_new_from_str(sid);
			if (id == NULL)
				continue;

			secret = g_strdup(ssecret);

			pi = psk_item_new();

			psk_item_set_id(pi, id);
			psk_item_set_secret(pi, secret);

			items = g_slist_append(items, pi);
		}
	}

	fclose(psk_file_in);

out:
	LOG_FUNC_END(1);

	return items;
}

/*******************************************************************************
 * LIMITS structure functions
 ******************************************************************************/

/**
 * Allocate new limits structure
 */
struct limits *limits_new()
{
	struct limits *limits;

	limits = g_malloc0(sizeof(struct limits));

	return limits;
}

/**
 * Free limits structure
 */
void limits_free(struct limits *limits)
{
	if (limits)
		g_free(limits);
}

/**
 * Dump limits structure
 */
void limits_dump(struct limits *limits)
{
	if (limits) {
		LOG_DEBUG("time=%u", limits->time);
		LOG_DEBUG("allocs=%u", limits->allocs);
		LOG_DEBUG("octets=%u", limits->octets);
	} else
		LOG_DEBUG("limits is set to NULL");
}

