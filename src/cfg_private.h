/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifdef CFG_PRIVATE

#ifndef __CFG_PRIVATE_H
#define __CFG_PRIVATE_H

/**
 * PRIVATE provider's configuration attributes
 */
#define PRIVATE_ATTR_IPV4_POOL		"ipv4_addr_pool"
#define PRIVATE_ATTR_IPV4_NETMAKS	"ipv4_netmask"
#define PRIVATE_ATTR_IPV4_NBNS		"ipv4_nbns"
#define PRIVATE_ATTR_IPV4_DHCP		"ipv4_dhcp"
#define PRIVATE_ATTR_IPV4_DNS		"ipv4_dns"

#define PRIVATE_ATTR_IPV6_POOL		"ipv6_addr_pool"
#define PRIVATE_ATTR_IPV6_DHCP		"ipv6_dhcp"
#define PRIVATE_ATTR_IPV6_DNS		"ipv6_dns"

/**
 * Configuration structure
 */
struct private_cfg {

	/**
	 * Lock to protect from concurrent modifications of this
	 * structure!
	 */
	GMutex *mutex;

	/**
	 * Reference counter for structure
	 */
	gint refcnt;

	/**
	 * First address of the IPv4 pool assigned to provider
	 */
	struct netaddr *ipv4_pool_start;

	/**
	 * End address of the IPv4 pool assigned to provider
	 */
	struct netaddr *ipv4_pool_end;

	/**
	 * Bitmap to specify free/allocated addresses
	 */
	gchar *ipv4_bitmap;

	struct in_addr ipv4_netmask;

	GSList *ipv4_nbns;
	GSList *ipv4_dhcp;
	GSList *ipv4_dns;

	/**
	 * Start address of the IPv6 pool assigned to provider
	 */
	struct netaddr *ipv6_pool_start;

	/**
	 * End address of the IPv6 pool assigned to provider
	 */
	struct netaddr *ipv6_pool_end;

	GSList *ipv6_dhcp;
	GSList *ipv6_dns;

};

/**
 * Structure with data allocated to a single client. Note that
 * we only support a single IP address per client. Each client
 * can have one instance of this structure for IPv4 address and
 * another instance for IPv6 address.
 */
struct private_client {

	/**
	 * Pointer to provider instance used to obtain data
	 */
	struct private_cfg *pcfg;

	/**
	 * Has the client assigned/bound IP addresses
	 */
	gboolean isbound;

	/**
	 * Is the client in error state
	 */
	gboolean iserror;

	/**
	 * List of IPv4 addresses allocated to the client
	 */
	GSList *addrs4;

	/**
	 * List of IPv4 addresses allocated to the client
	 */
	GSList *addrs6;

};

/**
 * Runtime data
 */
struct private_provider_data {

	/**
	 * Reference counting
	 */
	gint refcnt;

	/**
	 * List of currently active clients
	 */
	GSList *clients;

	/**
	 * Lock for protecting clients list
	 */
	GStaticRWLock clients_lock;
};

#endif /* __CFG_PRIVATE_H */

/******************************************************************************
 * private_cfg manipulation functions
 ******************************************************************************/
struct private_cfg *private_cfg_new();
void private_cfg_ref(struct private_cfg *);
void private_cfg_free(struct private_cfg *);

/******************************************************************************
 * Initialization functions
 ******************************************************************************/
void cfg_private_instance_free(struct cfg_config_provider *);
gint cfg_private_instance_init(struct cfg_config_provider *);
void cfg_private_unload();
gint cfg_private_init();

#endif /* CFG_PRIVATE */
