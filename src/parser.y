/***************************************************************************
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by    *
 * the Free Software Foundation; either version 2 of the License, or       *
 * (at your option) any later version.                                     *
 *                                                                         *
 ***************************************************************************/

%parse-param {struct config *config}

%{
#define __PARSER_C

#define LOGGERNAME	"parser"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif /* HAVE_STDLIB_H */
#ifdef HAVE_STRING_H
#include <string.h>
#endif /* HAVE_STRING_H */
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif /* HAVE_SYS_TYPES_H */
#ifdef HAVE_NETINET_IN_H
#include <netinet/ip.h>
#endif /* HAVE_NETINET_IN_H */
#include <linux/ipsec.h>

#include <openssl/dh.h>
#include <openssl/bn.h>

#include <sys/types.h>
#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif /* HAVE_SYS_SOCKET_H */
#ifdef HAVE_ARPA_INET
#include <arpa/inet.h>
#endif /* HAVE_ARPA_INET */
#include <netdb.h>

#include <glib.h>

#include "ikev2_consts.h"
#include "logging.h"
#include "netlib.h"
#include "transforms.h"
#include "ts.h"
#include "proposals.h"
#include "spd.h"
#include "auth.h"
#include "cert.h"
#include "radius.h"
#include "cfg.h"
#include "cfg_module.h"
#include "config.h"
#include "crypto.h"

#ifdef DMALLOC
#include "dmalloc.h"
#endif

extern int line_number;

/*
 * Global variables necessary during parsing phase
 */
struct config_log_item *cli;
struct radius_config *radius;
struct radius_server *rs;
struct config_remote *cr;
struct config_peer *cp;
struct limits *limits;

gint auth_counter;
guint32 auth_methods;

/*
 * Old global variables necessary during parsing phase. Some
 * of them will be eventually removed
 */
struct config_csa *config_csa = NULL;
GSList *ts_list = NULL;
struct ts *ts = NULL;
GSList *id_list = NULL;
struct id *id = NULL;
proposal_t *proposal = NULL;
transform_t *transform = NULL;
struct cert_item *cert_item = NULL;
struct ca_item *ca_item = NULL;
struct ca_item *peer_ca_item = NULL;
struct cert_item *peer_cert_item = NULL;
struct revocation_item *revocation_item = NULL;
char *string_asn1dn = NULL;
struct cfg_config_provider *ccp = NULL;
struct cfg_av *cfg_av = NULL;
struct config_listen *cl = NULL;

/**
 * For parsing options in all the sections
 */
guint32 option;
guint16 option_context;

GSList *sl_iterator;

#define YYCONFLOG(format, ...)	\
		ikev2_log(LOGGERNAME, LOG_PRIORITY_ERROR, \
			"Error in configuration file, line %d, " format, \
			line_number, ##__VA_ARGS__)

void yyerror (struct config *config, const char *s)
{
	ikev2_log(LOGGERNAME, LOG_PRIORITY_ERROR, "line %u: %s",
			line_number, s);
}

%}

%error-verbose
%union
{
	guint64 number;
	char *string;
	struct ts *ts_item;
	GSList *ts_list;
	GSList *id_list;
	GSList *cfg_items;
	GSList *provider_list;
	GSList *enc_algs;
	GSList *auth_algs;
	transform_t *transform;
	struct id *id;
}

/*
 * All the tokens, aphabetically sorted
 */
%token		T_ACCT_SERVER
%token		T_ADDRESS
%token		T_ALLOCS
%token		T_ANY
%token		T_AUTH_METHOD
%token		T_AUTH_SERVER
%token		T_AUTHLIMIT
%token		T_CA
%token		T_CERT
%token		T_CFG
%token		T_CHECK_INTERVAL
%token		T_COLON
%token		T_COMMA
%token		T_CRL
%token		T_DH_GROUP
%token		T_DOS_TRESHOLD
%token		T_DSS_SIG
%token		T_EAP
%token		T_ECDSS_SIG
%token		T_EBRACE 
%token <number>	T_FACILITY
%token		T_FILE
%token		T_FROM
%token		T_GENERAL
%token		T_HARDLIMIT
%token		T_HYPHEN
%token		T_ICMP
%token <string>	T_ID
%token		T_IKE_MAX_IDLE
%token		T_IKESA_MAX
%token		T_IKESA_MAX_HALFOPENED
%token		T_INTERFACE
%token		T_INTERIM_INTERVAL
%token		T_ICMPCODE
%token		T_ICMPTYPE
%token		T_IP
%token <string>	T_IPADDR
%token <string>	T_IPADDR_AND_PORT
%token		T_IPSECLEVEL 
%token		T_IPSECMODE 
%token		T_IPSECPROTO 
%token		T_IPV4
%token		T_IPV6
%token		T_KERNELSPD
%token		T_KSPD_FLUSH
%token		T_KSPD_ROSYNC
%token		T_KSPD_SYNC
%token		T_KSPD_GENERATE
%token		T_LISTEN
%token		T_LOCAL
%token		T_LOG
%token <number>	T_LOG_FLAG
%token		T_LOGGING
%token		T_LOGLEVEL
%token <number>	T_LOGVALUE
%token		T_MODE
%token		T_MODE_INITIATOR
%token		T_MODE_RESPONDER
%token		T_MODE_UNSPEC
%token		T_TYPE
%token		T_PROVIDES
%token		T_MY_IDENTIFIER
%token		T_NAS_IDENTIFIER
%token		T_NATT
%token		T_NATT_PORT
%token		T_NATT_KEEPALIVE
%token <string>	T_NETADDR
%token		T_NETWORK
%token		T_NONCE_SIZE
%token <number>	T_NUMBER
%token		T_OBRACE 
%token		T_OCTETS 
%token <number>	T_OCTETUNIT
%token		T_OFF
%token		T_ON
%token <number>	T_ONOFF
%token 		T_OPTION
%token		T_OPAQUE
%token		T_PASSIVE
%token		T_PEER
%token		T_PEER_AUTH_METHOD
%token		T_PIDFILE
%token		T_PORT
%token		T_PROPOSAL 
%token		T_PROPOSAL_ENCR
%token		T_PROPOSAL_AUTH
%token		T_PROPOSAL_PRF
%token		T_PROTO
%token		T_PROVIDER
%token		T_PROVIDERID
%token		T_CFG_PROVIDER
%token		T_PSK
%token		T_PSK_FILE
%token <string>	T_QSTRING
%token		T_RADIUS_SERVER
%token		T_RANDOM_DEVICE 
%token		T_REKEYLIMIT
%token		T_REMOTE
%token		T_REMOTE_ID
%token		T_RESPONSE_TIMEOUT 
%token		T_RESPONSE_RETRIES 
%token		T_RETRIES
%token		T_RSA_SIG
%token		T_SAINFO 
%token		T_SANCHK_OFF
%token		T_SANCHK_WARNING
%token		T_SANCHK_ERROR
%token		T_SANITY_CHECK
%token		T_SCRIPT_UP
%token		T_SCRIPT_DOWN
%token		T_SEMICOLON 
%token <string>	T_SEQ 
%token		T_SHARED_SECRET
%token		T_SM_THREADS
%token		T_SOFTLIMIT 
%token		T_SPDINSTALL 
%token		T_STDERR
%token		T_SUBSYSTEM
%token		T_SYSLOG
%token		T_TIME
%token		T_TIMEOUT
%token		T_TIMESTAMP
%token <number>	T_TIMEUNIT
%token		T_TYPE_X509
%token		T_SKIP_ID_VERIFY
%token		T_WPA_CONF

%token		T_NBNS
%token		T_SUBNETS
%token		T_SUBNETS6
%token		T_CFG_REQUEST
%token		T_APPVER
%token		T_ADDR
%token		T_ADDR6
%token		T_REQUEST
%token		T_DHCP
%token		T_DHCP6
%token		T_DNS
%token		T_DNS6

%token QUOTE 
%token OCSP
%token PFPFLAG
%token T_MOBIKE
%token T_COOKIE2
%token RETURN_ROUTABILITY
%token RETURN_ROUTABILITY_TIMEOUT
%token SA_HALFCLOSED_WAIT
%token PFS

%%

commands: /* empty */
	| commands command
	;

command: 
	general_section
	| logging_section
	| radius_section
	| cfg_section
	| remote_section
	| peer_section
	;

/*
 * Productions to parse general section in configuration file
 */
general_section:
	T_GENERAL T_OBRACE general_items T_EBRACE
	;

general_items:
	general_items general_item
	| general_item
	;

general_item:
	random_device_set
	| dos_treshold
	| sm_threads
	| ikesa_max
	| ikesa_max_halfopened
	| mode
	| ca_spec
	| cert_spec
	| psk_file
	| kernel_spd
	| listen
	| general_options
	| pidfile
	;

pidfile:
	T_PIDFILE T_SEQ
{
	config->cg->pidfile = $2;
}
	;

general_options:
	T_OPTION
{
	option = 0;
	option_context = OPTION_SECTION_GENERAL;
}
	option_list T_SEMICOLON
{
	config->cg->options = option;
}
	;

option_list:
	option_list T_COMMA option_item
	| option_item
	;

option_item:
	T_SEQ
{
	int i = 0;

	while (config_options[i].name) {

		if (config_options[i].section == option_context &&
				!strcmp($1, config_options[i].name)) {
			option |= config_options[i].value;
			break;
		}

		i++;
	}

	if (!config_options[i].name) {
		LOG_WARNING("Ignoring unrecognized option %s in line %d",
				$1, line_number);
	}
}
	;

listen:
	T_LISTEN
{
	cl = config_listen_new();
	cl->port = IKEV2_PORT;
	cl->natt_port = IKEV2_PORT_NATT;
}
	listen_family listen_ifaddr listen_port listen_natt_port T_SEMICOLON
{
	config->cg->listen_data = g_slist_append(config->cg->listen_data, cl);
}
	;

listen_family:
	| T_IPV4
{
	cl->afamily = AF_INET;
}
	| T_IPV6
{
	cl->afamily = AF_INET6;
}
	;

listen_ifaddr:
	| T_SEQ
{
	cl->addr = netaddr_new();
	netaddr_set_ifname(cl->addr, $1);
	g_free($1);
}
	| T_IPADDR
{
	char ipnum[sizeof(struct in6_addr)];

	if (inet_pton(AF_INET, $1, ipnum) > 0) {
		cl->addr = netaddr_new_from_inaddr((struct in_addr *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $1, ipnum) > 0) {
			cl->addr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
		} else {
			LOG_ERROR("Unrecognized IP address %s (line %d)",
					$1, line_number);
			YYABORT;
		}
	}

	if (cl->afamily != AF_UNSPEC &&
			netaddr_get_family(cl->addr) != cl->afamily) {
		LOG_ERROR("Address family doesn't match expected one (line %d)",
				line_number);
		YYABORT;
	}
}
	| T_SEQ T_COLON T_IPADDR
{
	char ipnum[sizeof(struct in6_addr)];

	if (inet_pton(AF_INET, $3, ipnum) > 0) {
		cl->addr = netaddr_new_from_inaddr((struct in_addr *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $3, ipnum) > 0) {
			cl->addr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
		} else {
			LOG_ERROR("Unrecognized IP address %s (line %d)",
					$3, line_number);
			YYABORT;
		}
	}

	if (cl->afamily != AF_UNSPEC &&
			netaddr_get_family(cl->addr) != cl->afamily) {
		LOG_ERROR("Address family doesn't match expected one (line %d)",
				line_number);
		YYABORT;
	}

	netaddr_set_ifname(cl->addr, $1);
	g_free($1);
}
	;

listen_port:
	| T_PORT T_NUMBER
{
	if ($2 < 0 || $2 > 65535) {
		LOG_ERROR("Invalid port value %d (line %d)", $2, line_number);
		YYABORT;
	}

	cl->port = $2;
}
	;

listen_natt_port:
	| T_NATT_PORT T_NUMBER
{
	if ($2 < 0 || $2 > 65535) {
		LOG_ERROR("Invalid port value %d (line %d)", $2, line_number);
		YYABORT;
	}

	cl->natt_port = $2;
}
	;

random_device_set:
	T_RANDOM_DEVICE T_QSTRING T_SEMICOLON
{
	config->cg->random_device = $2;
}
	;

dos_treshold:
	T_DOS_TRESHOLD T_NUMBER T_SEMICOLON
{
	config->cg->dos_treshold = $2;
}
	;

sm_threads: T_SM_THREADS T_NUMBER T_SEMICOLON
{
	config->cg->sm_threads = $2;
}
        ;

ikesa_max:
	T_IKESA_MAX T_NUMBER T_SEMICOLON
{
	config->cg->ikesa_max = $2;
}
        ;

ikesa_max_halfopened:
	T_IKESA_MAX_HALFOPENED T_NUMBER T_SEMICOLON
{
	config->cg->ikesa_max_halfopened = $2;
}
        ;

mode: T_MODE mode_val T_SEMICOLON
{
	config->cg->mode = $<number>2;
}
	;

mode_val: T_MODE_RESPONDER
{
	$<number>$ = IKEV2_MODE_RESPONDER;
}
	| T_MODE_INITIATOR
{
	$<number>$ = IKEV2_MODE_INITIATOR;
}
	| T_MODE_UNSPEC
{
	$<number>$ = IKEV2_MODE_UNSPEC;
}
        ;

ca_spec:
	T_CA ca_item crl_item T_SEMICOLON
	;

ca_item:
	T_TYPE_X509 T_QSTRING
{
	ca_item = ca_item_new();

	ca_item->type = IKEV2_CERT_X509_SIGNATURE;
	ca_item->file = $2;
}
	;

crl_item:
{
	config_general_add_ca(config->cg, ca_item);
}
	| T_CRL T_QSTRING T_LOCAL T_QSTRING T_CHECK_INTERVAL time_value
{
	ca_item->crl_url = $2;
	ca_item->crl_file = $4;
	ca_item->check_interval = $<number>6;

	config_general_add_ca(config->cg, ca_item);
}
	| T_CRL T_QSTRING T_LOCAL T_QSTRING 
{
	ca_item->crl_url = $2;
	ca_item->crl_file = $4;

	config_general_add_ca(config->cg, ca_item);
}
	;

cert_spec:
	T_CERT cert_item T_SEMICOLON
	;

cert_item:
	T_TYPE_X509 T_QSTRING T_QSTRING
{
	cert_item = cert_item_new();

	cert_item->type = IKEV2_CERT_X509_SIGNATURE;	
	cert_item->file = $2;
	cert_item->priv_key_file = $3;

}
	cert_item_url
	;

cert_item_url:
{
	config_general_add_cert(config->cg, cert_item);
}
	| T_QSTRING
{
	cert_item->url = $1;
	cert_item->type = IKEV2_CERT_HASH_AND_URL;
	config_general_add_cert(config->cg, cert_item);
}
	;

psk_file:
	T_PSK_FILE T_QSTRING T_SEMICOLON
{
	config->cg->psk_file = $2;
}
	;

kernel_spd:
	T_KERNELSPD T_KSPD_FLUSH T_SEMICOLON
{
	config->cg->kernel_spd = KERNEL_SPD_FLUSH;
}
	| T_KERNELSPD T_KSPD_ROSYNC T_SEMICOLON
{
	config->cg->kernel_spd = KERNEL_SPD_ROSYNC;
}
	| T_KERNELSPD T_KSPD_SYNC T_SEMICOLON
{
	config->cg->kernel_spd = KERNEL_SPD_SYNC;
}
	| T_KERNELSPD T_KSPD_GENERATE T_SEMICOLON
{
	config->cg->kernel_spd = KERNEL_SPD_GENERATE;
}
	;

/*
 * Productions to parse logging section in configuration file
 */
logging_section:
	T_LOGGING
{
	config->logging = config_log_new();
}
	T_OBRACE logging_items T_EBRACE
	;

logging_items:
	logging_items logging_item
	| logging_item
	;

logging_item:
	logging_file
	| logging_syslog
	| logging_stderr
	;

logging_file:
	T_FILE T_QSTRING
{
	cli = config_log_item_new();

	cli->filename = $2;
	config_log_add_config_log_item(config->logging, cli);
}
	T_OBRACE logging_file_items T_EBRACE
        ;

logging_stderr:
	T_STDERR
{
	cli = config_log_item_new();

	cli->filename = g_strdup("stderr");
	config_log_add_config_log_item(config->logging, cli);
}
	T_OBRACE logging_file_items T_EBRACE
        ;

logging_syslog:
	T_SYSLOG T_FACILITY
{
	cli = config_log_item_new();

	if (config->logging->facility != -1) {
		YYCONFLOG("Only one syslog block can occure!");
		YYABORT;
	}

	config->logging->facility = $2;

	config_log_add_config_log_item(config->logging, cli);
}
	T_OBRACE logging_file_items T_EBRACE
        ;

logging_file_items:
	logging_file_items logging_file_item
	| logging_file_item
	;

logging_file_item:
	T_LOG log_flag_list T_SEMICOLON
	| T_LOGLEVEL T_LOGVALUE T_SEMICOLON
{
	cli->level = $2;
}
	| T_SUBSYSTEM subsystem_list T_SEMICOLON
	;

log_flag_list:
	log_flag_list T_COMMA T_LOG_FLAG
{
	cli->flags |= $3;
}
	| T_LOG_FLAG
{
	cli->flags |= $1;
}
	;

subsystem_list:
	subsystem_list T_COMMA T_SEQ
{
	config_log_item_add_subsystem(cli, $3);
}
	| T_SEQ
{
	config_log_item_add_subsystem(cli, $1);
}
	;

/*
 * Radius servers configuration parser
 */
radius_section:
	T_RADIUS_SERVER T_SEQ T_OBRACE
{
#ifdef RADIUS_CLIENT
	radius = radius_config_new();

	radius->cfg_line = line_number;

	radius_config_set_id(radius, $2);
	config_add_radius(config, radius);
#else /* RADIUS_CLIENT */
	LOG_ERROR("RADIUS cofiguration parameters not compiled in!");
	YYABORT;
#endif /* RADIUS_CLIENT */
}
	radius_items T_EBRACE
	;

radius_items:
	radius_items radius_item
	| radius_item
	;

radius_item:
	auth_server
	| acct_server
	| nas_identifier
	| interim_interval
	| radius_timeout
	| radius_retries
	;

auth_server:
	T_AUTH_SERVER T_OBRACE
{
#ifdef RADIUS_CLIENT
	rs = radius_server_new();

	radius_config_add_auth_server(radius, rs);
#else /* RADIUS_CLIENT */
	LOG_ERROR("RADIUS cofiguration parameters not compiled in!");
#endif /* RADIUS_CLIENT */
}
	auth_server_items T_EBRACE
	;

acct_server:
	T_ACCT_SERVER T_OBRACE
{
#ifdef RADIUS_CLIENT
	rs = radius_server_new();

	radius_config_add_acct_server(radius, rs);
#else /* RADIUS_CLIENT */
	LOG_ERROR("RADIUS cofiguration parameters not compiled in!");
#endif /* RADIUS_CLIENT */
}
	auth_server_items T_EBRACE
	;

auth_server_items:
	auth_server_items auth_server_item
	| auth_server_item
	;

auth_server_item:
	T_SHARED_SECRET T_SEQ T_SEMICOLON
{
	rs->shared_secret = g_strdup($2);
}
	| T_ADDRESS T_IPADDR T_SEMICOLON
{
	char ipnum[sizeof(struct in6_addr)];
	netaddr_t *netaddr;

	if (inet_pton(AF_INET, $2, ipnum) > 0) {
		netaddr = netaddr_new_from_inaddr((struct in_addr *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $2, ipnum) > 0) {
			netaddr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $2);
			YYABORT;
		}
	}

	if (!netaddr) {
		YYCONFLOG("Error converting IP address %s", $2);
		YYABORT;
	}

	netaddr_set_port(netaddr, RADIUS_SERVER_PORT);

	rs->addr = netaddr;
}
	| T_ADDRESS T_IPADDR_AND_PORT T_SEMICOLON
{
	char *p, ipnum[sizeof(struct in6_addr)];
	guint32 port = 0;
	netaddr_t *netaddr;

	p = strchr($2, ':');

	*p = 0;
	port = atol(p + 1);
	if (port > 65535) {
		YYCONFLOG("Invalid port number (%d)", port);
		YYABORT;
	}

	if (inet_pton(AF_INET, $2, ipnum) > 0) {
		netaddr = netaddr_new_from_inaddr((struct in_addr *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $2, (void *)ipnum) > 0) {
			netaddr = netaddr_new_from_in6addr(
						(struct in6_addr *)ipnum);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $2);
			YYABORT;
		}
	}

	if (!netaddr) {
		YYCONFLOG("Error converting IP address %s", $2);
		YYABORT;
	}

	netaddr_set_port(netaddr, port);

	rs->addr = netaddr;

	*p = ':';
}
	| T_ADDRESS T_SEQ T_SEMICOLON
{
	YYCONFLOG("Specifying DNS names is not currently supported", $2);
}
	;

nas_identifier:
	T_NAS_IDENTIFIER T_SEQ T_SEMICOLON
{
#ifdef RADIUS_CLIENT
	if (radius->nas_identifier) {
		LOG_WARNING("Line %u: NAS identifier already set", line_number);
		g_free(radius->nas_identifier);
	}

	radius->nas_identifier = g_strdup($2);
#else /* RADIUS_CLIENT */
	LOG_ERROR("RADIUS cofiguration parameters not compiled in!");
	YYABORT;
#endif /* RADIUS_CLIENT */
}
	;

interim_interval:
	T_INTERIM_INTERVAL T_NUMBER T_SEMICOLON
{
	radius->interim_interval = $2;
}
	;

radius_timeout:
	T_TIMEOUT radius_timeout_value T_SEMICOLON
	;

radius_timeout_value:
	T_NUMBER T_TIMEUNIT 
{
	radius->timeout = $1 * $2 * 1000;
}
	;

radius_retries:
	T_RETRIES radius_retries_value T_SEMICOLON
	;

radius_retries_value:
	T_NUMBER
{
	radius->retries = $1;
}
	;

cfg_section:
	T_CFG T_OBRACE provider_sections T_EBRACE
	;

provider_sections:
	provider_sections provider_section
	| provider_section
	;

provider_section:
	T_PROVIDER T_SEQ T_OBRACE
{
#ifdef CFG_MODULE
	ccp = cfg_config_provider_new();
	ccp->id = $2;

	config_add_provider(config, ccp);
#else /* CFG_MODULE */
	YYCONFLOG("Support for CFG on responder is not compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}
	provider_items T_EBRACE
	;

provider_items:
	provider_items provider_item
	| provider_item
	;

provider_item:
	T_TYPE T_SEQ T_SEMICOLON
{
#ifdef CFG_MODULE
#ifdef CFG_DHCPV4
	if (!strcasecmp($2, "dhcp"))
		ccp->provider_type = CFG_PROTO_DHCPV4;
#endif /* CFG_DHCPV4 */

#ifdef CFG_PRIVATE
	if (!strcasecmp($2, "private"))
		ccp->provider_type = CFG_PROTO_PRIVATE;
#endif /* CFG_PRIVATE */

#ifdef CFG_DHCPV6
	if (!strcasecmp($2, "dhcpv6"))
		ccp->provider_type = CFG_PROTO_PRIVATE;
#endif /* CFG_DHCPV6 */

	if (!ccp->provider_type) {
		LOG_ERROR("Unrecognized provider type %s in line %d",
				$2, line_number);
		YYABORT;
	}
#else /* CFG_MODULE */
	LOG_ERROR("No configuration provider compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}
	| T_PROVIDES provides_items T_SEMICOLON
	| T_SEQ T_SEQ T_SEMICOLON
{
#ifdef CFG_MODULE
	cfg_av = cfg_av_new();
	cfg_av->line = line_number;
	cfg_av->val_type = CFG_AV_TYPE_SINGLE;
	cfg_av->attribute = $1;
	cfg_av->string = $2;
	ccp->av = g_slist_append(ccp->av, cfg_av);
#endif /* CFG_MODULE */
}
	| T_SEQ T_SEQ T_HYPHEN T_SEQ T_SEMICOLON
{
#ifdef CFG_MODULE
	cfg_av = cfg_av_new();
	cfg_av->line = line_number;
	cfg_av->val_type = CFG_AV_TYPE_RANGE;
	cfg_av->attribute = $1;
	cfg_av->lower = $2;
	cfg_av->upper = $4;
	ccp->av = g_slist_append(ccp->av, cfg_av);
#endif /* CFG_MODULE */
}
	| T_SEQ T_SEQ T_COMMA
{
#ifdef CFG_MODULE
	cfg_av = cfg_av_new();
	cfg_av->line = line_number;
	cfg_av->val_type = CFG_AV_TYPE_LIST;
	cfg_av->attribute = $1;
	cfg_av->list = g_slist_append(cfg_av->list, $2);
	ccp->av = g_slist_append(ccp->av, cfg_av);
#endif /* CFG_MODULE */
}
	value_list T_SEMICOLON
	;

value_list:
	value_list T_COMMA T_SEQ
{
#ifdef CFG_MODULE
	cfg_av->list = g_slist_append(cfg_av->list, $3);
#endif /* CFG_MODULE */
}
	| T_SEQ
{
#ifdef CFG_MODULE
	cfg_av->list = g_slist_append(cfg_av->list, $1);
#endif /* CFG_MODULE */
}
	;

provides_items:
	provides_items T_COMMA provides_item
	| provides_item
	;

provides_item:
	T_SEQ
{
#ifdef CFG_MODULE
	if (!strcmp($1, "addr"))
		ccp->flags |= OPTION_F4_ADDR;
	else if (!strcmp($1, "netmask"))
		ccp->flags |= OPTION_F4_NETMASK;
	else if (!strcmp($1, "dns"))
		ccp->flags |= OPTION_F4_DNS;
	else if (!strcmp($1, "dhcp"))
		ccp->flags |= OPTION_F4_DHCP;
	else if (!strcmp($1, "nbns"))
		ccp->flags |= OPTION_F4_NBNS;
	else if (!strcmp($1, "subnets"))
		ccp->flags |= OPTION_F4_SUBNETS;
	else if (!strcmp($1, "addr6"))
		ccp->flags |= OPTION_F6_ADDR;
	else if (!strcmp($1, "dns6"))
		ccp->flags |= OPTION_F6_DNS;
	else if (!strcmp($1, "dhcp6"))
		ccp->flags |= OPTION_F6_DHCP;
	else if (!strcmp($1, "subnets6"))
		ccp->flags |= OPTION_F6_SUBNETS;
	else {
		LOG_ERROR("Unrecognized parameter %s in line %u",
				$1, line_number);
		YYABORT;
	}
#endif /* CFG_MODULE */
}
	;

remote_section: T_REMOTE
{
	LOG_TRACE("Parsing 'remote section' section");

	cr = config_remote_new();

	cr->cfg_line = line_number;
}
	remote_addresses T_OBRACE remote_items T_EBRACE
{
	cr->ts_list = $<ts_list>3;

	config_add_remote(config, cr);
}
	;

remote_addresses:
	ts_list
{
	$<ts_list>$ = $<ts_list>1;
}
	| T_ANY
{
	GSList *ts_list;
	char saddr[16], daddr[16];

	memset(saddr, 0, 16);
	memset(daddr, -1, 16);

	ts = ts_new();

	ts_list = g_slist_append(NULL, ts);

	ts_set_saddr_from_inaddr(ts, (void *)saddr);
	ts_set_eaddr_from_inaddr(ts, (void *)daddr);
	ts_set_sport(ts, 0);
	ts_set_eport(ts, 65535);

	ts = ts_new();

	ts_list = g_slist_append(ts_list, ts);

	ts_set_saddr_from_in6addr(ts, (void *)saddr);
	ts_set_eaddr_from_in6addr(ts, (void *)daddr);
	ts_set_sport(ts, 0);
	ts_set_eport(ts, 65535);

	$<ts_list>$ = ts_list;
}
	;

ts_list:
	ts_list T_COMMA ts_item
{
	$<ts_list>$ = g_slist_append($<ts_list>1, $<ts_item>3);
}
	| ts_item
{
	$<ts_list>$ = g_slist_append(NULL, $<ts_item>1);
}
	;

ts_item: T_IPADDR
{
	char ipnum[16];

	ts = ts_new();

	if (inet_pton(AF_INET, $1, ipnum) > 0) {
		ts_set_saddr_from_inaddr(ts, (void *)ipnum);
		ts_set_eaddr_from_inaddr(ts, (void *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $1, ipnum) > 0) {
			ts_set_saddr_from_in6addr(ts, (void *)ipnum);
			ts_set_eaddr_from_in6addr(ts, (void *)ipnum);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $1);
			YYABORT;
		}
	}

	ts_set_sport(ts, 0);
	ts_set_eport(ts, 65535);

	$<ts_item>$ = ts;
}
	| T_NETADDR
{
	char *p, ipnum[16];
	guint8 netmask = 0;

	ts = ts_new();

	p = strchr($1, '/');

	*p = 0;
	netmask = atoi(p + 1);

	if (inet_pton(AF_INET, $1, ipnum) > 0) {
		if (netmask > 32) {
			YYCONFLOG("Invalid network mask (%d)", netmask);
			YYABORT;
		}
		ts_set_saddr_from_netaddr(ts, (void *)ipnum, netmask);
		ts_set_eaddr_from_netaddr(ts, (void *)ipnum, netmask);
	} else {
		if (netmask > 128) {
			YYCONFLOG("Invalid network mask (%d)", netmask);
			YYABORT;
		}
		if (inet_pton(AF_INET6, $1, (void *)ipnum) > 0) {
			ts_set_saddr_from_netaddr6(ts, (void *)ipnum, netmask);
			ts_set_eaddr_from_netaddr6(ts, (void *)ipnum, netmask);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $1);
			YYABORT;
		}
	}

	*p = '/';

	ts_set_sport(ts, 0);
	ts_set_eport(ts, 65535);

	$<ts_item>$ = ts;
}
	| T_IPADDR T_HYPHEN T_IPADDR
{
	char ipnum[16];

	ts = ts_new();

	if (inet_pton(AF_INET, $1, ipnum) > 0) {
		ts_set_saddr_from_inaddr(ts, (void *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $1, (void *)ipnum) > 0) {
			ts_set_saddr_from_in6addr(ts, (void *)ipnum);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $1);
			YYABORT;
		}
	}

	if (inet_pton(AF_INET, $3, ipnum) > 0) {
		ts_set_eaddr_from_inaddr(ts, (void *)ipnum);
	} else {
		if (inet_pton(AF_INET6, $3, (void *)ipnum) > 0) {
			ts_set_eaddr_from_in6addr(ts, (void *)ipnum);
		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $1);
			YYABORT;
		}
	}

	ts_set_sport(ts, 0);
	ts_set_eport(ts, 65535);

	$<ts_item>$ = ts;
}
	;

remote_items:
	remote_items remote_item 
	| remote_item
	;

remote_item:
	proposal 
	| natt_item
	| natt_keepalive_item
	| response_timeout_item
	| response_retries_item
	| remote_nonce_item
	| remote_id
	| remote_options
	/*
	 * FIXME
	 * Introducing MOBIKE-related production rules
	 */
	| remote_mobike_item
	| remote_cookie2_item
	| remote_rrc_item
	| remote_rrc_timeout_item
	;

remote_options:
	T_OPTION
{
	option = 0;
	option_context = OPTION_SECTION_REMOTE;
}
	option_list T_SEMICOLON
{
	cr->options = option;
}
	;

proposal:
	T_PROPOSAL T_OBRACE
{
	LOG_DEBUG("Parsing proposal section");

	proposal = proposal_new();

	proposal_set_protocol(proposal, IKEV2_PROTOCOL_IKE);
	config_remote_add_proposal(cr, proposal);
}
	proposal_items T_EBRACE
	;

proposal_items:
	proposal_items proposal_item 
	| proposal_item
	;

proposal_item:
	T_PROPOSAL_ENCR enc_algorithms T_SEMICOLON 
	| T_PROPOSAL_AUTH auth_algorithms T_SEMICOLON 
	| remote_dh_set
	| T_PROPOSAL_PRF prf_functions T_SEMICOLON
	;

enc_algorithms:
	enc_algorithms T_COMMA enc_algorithm
	| enc_algorithm
	;

enc_algorithm:
	T_SEQ
{
	transform = transform_find_by_name(IKEV2_TRANSFORM_ENCR,
						TRANSFORM_IKE, $1);

	if (!transform) {
		YYCONFLOG("Unsupported encryption_algorithm parameter (%s)",
				$1);
		YYABORT;
	}

	proposal_transform_append(proposal, transform);
};

auth_algorithms:
	auth_algorithms T_COMMA auth_algorithm
	| auth_algorithm
	;

auth_algorithm:
	T_SEQ
{
	transform = transform_find_by_name(IKEV2_TRANSFORM_INTEGRITY,
					TRANSFORM_IKE, $1);

	if (!transform) {
		YYCONFLOG("Unsupported auth_algorithm parameter (%s)",
				$1);
		YYABORT;
	}

	proposal_transform_append(proposal, transform);
};

prf_functions:
	prf_functions T_COMMA prf_function
	| prf_function
	;

prf_function:
	T_SEQ
{
	transform = transform_find_by_name(IKEV2_TRANSFORM_PRF,
					TRANSFORM_IKE, $1);

	if (!transform) {
		YYCONFLOG("Unsupported pseudo_random_function parameter (%s)",
				$1);
		YYABORT;
	}

	proposal_transform_append(proposal, transform);
};

remote_dh_set:
	T_DH_GROUP remote_dh_groups T_SEMICOLON
	;

remote_dh_groups:
	remote_dh_group T_COMMA remote_dh_groups
	| remote_dh_group
	;

remote_dh_group:
	T_SEQ
{
	transform = transform_find_by_name(IKEV2_TRANSFORM_DH_GROUP,
					TRANSFORM_IKE, $1);

	if (!transform) {
		YYCONFLOG("Unsupported dh_group parameter (%s)",
				$1);
		YYABORT;
	}

	proposal_transform_append(proposal, transform);
};

/*
 * FIXME
 * MOBIKE support in parser
 */
remote_mobike_item: T_MOBIKE remote_mobike_value T_SEMICOLON
	;

remote_mobike_value: T_ON
{
#ifdef MOBIKE
	cr->mobike = MOBIKE_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cr->mobike = MOBIKE_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
};

peer_mobike_item: T_MOBIKE peer_mobike_value T_SEMICOLON
	;

peer_mobike_value: T_ON
{
#ifdef MOBIKE
	cp->mobike = MOBIKE_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cp->mobike = MOBIKE_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif
};

remote_cookie2_item: T_COOKIE2 remote_cookie2_value T_SEMICOLON
	;

remote_cookie2_value: T_ON
{
#ifdef MOBIKE
	cr->cookie2 = COOKIE2_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cr->cookie2 = COOKIE2_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
};

peer_cookie2_item: T_COOKIE2 peer_cookie2_value T_SEMICOLON
	;

peer_cookie2_value: T_ON
{
#ifdef MOBIKE
	cp->cookie2 = COOKIE2_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cp->cookie2 = COOKIE2_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif
};

remote_rrc_item: RETURN_ROUTABILITY remote_rrc_value T_SEMICOLON
	;

remote_rrc_value: T_ON
{
#ifdef MOBIKE
	cr->rrc = RRC_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cr->rrc = RRC_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
};

peer_rrc_item: RETURN_ROUTABILITY peer_rrc_value T_SEMICOLON
	;

peer_rrc_value: T_ON
{
#ifdef MOBIKE
	cp->rrc = RRC_ON;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif /* MOBIKE */
}
	| T_OFF
{
#ifdef MOBIKE
	cp->rrc = RRC_OFF;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif
};

remote_rrc_timeout_item:
	RETURN_ROUTABILITY_TIMEOUT time_value T_SEMICOLON
{
#ifdef MOBIKE
	cr->rrc_timeout = $<number>2 * 1000;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif
}
	;

peer_rrc_timeout_item:
	RETURN_ROUTABILITY_TIMEOUT time_value T_SEMICOLON
{
#ifdef MOBIKE
	cp->rrc_timeout = $<number>2 * 1000;
#else /* MOBIKE */
	LOG_WARNING("MOBIKE support not compiled in (line %d)", line_number);
#endif
}
	;
/*
 * FIXME
 * End of MOBIKE support part in parser
 */

natt_item: T_NATT natt_value T_SEMICOLON
	;

natt_value: T_ON
{
#ifdef NATT
	cr->natt = NATT_ON;
#else /* NATT */
	LOG_WARNING("NAT-T support not compiled in (line %d)", line_number);
#endif /* NATT */
}
	| T_OFF
{
#ifdef NATT
	cr->natt = NATT_OFF;
#else /* NATT */
	LOG_WARNING("NAT-T support not compiled in (line %d)", line_number);
#endif /* NATT */
}
	| T_PASSIVE
{
#ifdef NATT
	cr->natt = NATT_PASSIVE;
#else /* NATT */
	LOG_WARNING("NAT-T support not compiled in (line %d)", line_number);
#endif /* NATT */
}
	;

natt_keepalive_item:
	T_NATT_KEEPALIVE time_value T_SEMICOLON
{
#ifdef NATT
	cr->natt_keepalive = $<number>2 * 1000;
#else /* NATT */
	LOG_WARNING("NAT-T support not compiled in (line %d)", line_number);
#endif /* NATT */
}
	;

remote_nonce_item:
	T_NONCE_SIZE T_NUMBER T_SEMICOLON
{
	if ($2 < 16 || $2 > 256) {
		YYCONFLOG("Invalid nonce size (%d). It has to be between "
				"16 and 256 inclusive.", $2);
		YYABORT;
	}

	cr->nonce_size = $2;
}
	;

response_retries_item:
	T_RESPONSE_RETRIES T_NUMBER T_SEMICOLON
{
	cr->response_retries = $2;
}
	;

response_timeout_item:
	T_RESPONSE_TIMEOUT time_value T_SEMICOLON
{
	cr->response_timeout = $<number>2 * 1000;
}
	;

time_value:
	T_NUMBER T_TIMEUNIT
{
	$<number>$ = $1 * $2;
}
	;

remote_id:
	T_REMOTE_ID identifier T_SEMICOLON
{
	cr->rid = $<id>2;
}
	;

peer_section:
	T_PEER
{
	cp = config_peer_new();

	cp->cfg_line = line_number;
}
	identifiers T_OBRACE
{
	cp->rids = $<id_list>3;

	config_add_peer(config, cp);
}
	peer_section_items T_EBRACE
	;

identifiers:
	identifiers T_COMMA identifier
{
	$<id_list>$ = g_slist_append($<id_list>1, $<id>2);
}
	| identifier
{
	$<id_list>$ = g_slist_append(NULL, $<id>1);
}
	;

peer_section_items:
	peer_section_items peer_section_item
	| peer_section_item
	;

peer_section_item:
	auth_method
	| peer_auth_method
	| peer_my_identifier_item
	| peer_ca_spec
	| peer_cert_spec
	| sa_halfclosed_wait
	| authlimits
	| rekeylimits
	| ike_maxidle
	| sainfo_section
	| wpa_conf
	| radius_server
	| cfg_request
	| cfg_provider
	| peer_options
	/*
	 * FIXME
	 * Introducing MOBIKE-related production rules
	 */
	| peer_mobike_item
	| peer_cookie2_item
	| peer_rrc_item
	| peer_rrc_timeout_item
	;

peer_options:
	T_OPTION
{
	option = 0;
	option_context = OPTION_SECTION_PEER;
}
	option_list T_SEMICOLON
{
	cp->options = option;
}
	;

cfg_provider:
	T_CFG_PROVIDER T_OBRACE cfg_provider_items T_EBRACE
	;

cfg_provider_items:
	cfg_provider_items cfg_provider_item
	| cfg_provider_item
	;

cfg_provider_item:
	providerid_items
	| subnets
	| gwscript_up
	| gwscript_down
	;

subnets:
	T_SUBNETS subnet_addresses T_SEMICOLON
	;

subnet_addresses:
	subnet_addresses T_COMMA netaddr
	| netaddr
	;

netaddr:
	T_NETADDR
{
#ifdef CFG_MODULE
	char *p, ipnum[16];
	guint8 netmask = 0;
	struct netaddr *na = NULL;

	p = strchr($1, '/');

	*p = 0;
	netmask = atoi(p + 1);

	if (inet_pton(AF_INET, $1, ipnum) > 0) {

		if (netmask > 32) {
			YYCONFLOG("Invalid network mask (%d)", netmask);
			YYABORT;
		}

		na = netaddr_new_from_inaddr(ipnum);
		netaddr_set_prefix(na, netmask);

	} else {

		if (inet_pton(AF_INET6, $1, (void *)ipnum) > 0) {

			if (netmask > 128) {
				YYCONFLOG("Invalid network mask (%d)", netmask);
				YYABORT;
			}

			na = netaddr_new_from_in6addr(ipnum);
			netaddr_set_prefix(na, netmask);

		} else {
			YYCONFLOG("Unrecognized IP address (%s)", $1);
			YYABORT;
		}
	}

	cp->subnets = g_slist_append(cp->subnets, na);
#else /* CFG_MODULE */
	YYCONFLOG("Support for CFG payload on responder is not compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}

gwscript_up:
	T_SCRIPT_UP T_QSTRING T_SEMICOLON
{
#ifdef CFG_MODULE
	if (!g_shell_parse_argv($2, &cp->gw_up_argc, &cp->gw_up_argv, NULL)) {
		LOG_ERROR("Error in script_up command line arguments");
		YYABORT;
	}

	g_free($2);
#else /* CFG_MODULE */
	YYCONFLOG("Support for CFG payload on responder is not compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}
	;

gwscript_down:
	T_SCRIPT_DOWN T_QSTRING T_SEMICOLON
{
#ifdef CFG_MODULE
	if (!g_shell_parse_argv($2, &cp->gw_down_argc, &cp->gw_down_argv, NULL)) {
		LOG_ERROR("Error in script_down command line arguments");
		YYABORT;
	}
#else /* CFG_MODULE */
	YYCONFLOG("Support for CFG payload on responder is not compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}
	;

providerid_items:
	T_PROVIDERID providerids T_SEMICOLON
	;

providerids:
	providerids T_COMMA providerid_item
	| providerid_item
	;

providerid_item:
	T_SEQ
{
#ifdef CFG_MODULE
	cp->providers = g_slist_append(cp->providers, $1);
#else /* CFG_MODULE */
	YYCONFLOG("Support for CFG payload on responder is not compiled in!");
	YYABORT;
#endif /* CFG_MODULE */
}
	;

cfg_request:
	T_CFG_REQUEST T_OBRACE cfg_request_items T_EBRACE
	;

cfg_request_items:
	cfg_request_items cfg_request_item
	| cfg_request_item
	;

cfg_request_item:
	request
	| script_up
	| script_down
	;

request:
	T_REQUEST request_items T_SEMICOLON
	;

request_items:
	request_items T_COMMA request_item
	| request_item
	;

request_item:
	T_SEQ
{
#ifdef CFG_CLIENT
	if (!strcmp($1, "addr"))
		cp->flags |= OPTION_F4_ADDR;
	else if (!strcmp($1, "netmask"))
		cp->flags |= OPTION_F4_NETMASK;
	else if (!strcmp($1, "dns"))
		cp->flags |= OPTION_F4_DNS;
	else if (!strcmp($1, "dhcp"))
		cp->flags |= OPTION_F4_DHCP;
	else if (!strcmp($1, "nbns"))
		cp->flags |= OPTION_F4_NBNS;
	else if (!strcmp($1, "subnets"))
		cp->flags |= OPTION_F4_SUBNETS;
	else if (!strcmp($1, "addr6"))
		cp->flags |= OPTION_F6_ADDR;
	else if (!strcmp($1, "dns6"))
		cp->flags |= OPTION_F6_DNS;
	else if (!strcmp($1, "dhcp6"))
		cp->flags |= OPTION_F6_DHCP;
	else if (!strcmp($1, "subnets6"))
		cp->flags |= OPTION_F6_SUBNETS;
	else {
		LOG_ERROR("Unrecognized parameter %s in line %u",
				$1, line_number);
		YYABORT;
	}
#endif /* CFG_CLIENT */
}

script_up:
	T_SCRIPT_UP T_QSTRING T_SEMICOLON
{
#ifdef CFG_CLIENT
	if (!g_shell_parse_argv($2, &cp->up_argc, &cp->up_argv, NULL)) {
		LOG_ERROR("Error in script_up command line arguments");
		YYABORT;
	}

	g_free($2);
#endif /* CFG_CLIENT */
}
	;

script_down:
	T_SCRIPT_DOWN T_QSTRING T_SEMICOLON
{
#ifdef CFG_CLIENT
	if (!g_shell_parse_argv($2, &cp->down_argc, &cp->down_argv, NULL)) {
		LOG_ERROR("Error in script_down command line arguments");
		YYABORT;
	}
#endif /* CFG_CLIENT */
}
	;

peer_ca_spec:
	T_CA peer_ca_item peer_crl_item T_SEMICOLON
	;

peer_ca_item:
	T_TYPE_X509 T_QSTRING
{
	peer_ca_item = ca_item_new();

	peer_ca_item->type = IKEV2_CERT_X509_SIGNATURE;
	peer_ca_item->file = $2;

}
	;

peer_crl_item:
{
	config_peer_add_ca(cp, peer_ca_item);
}
	| T_CRL T_QSTRING T_LOCAL T_QSTRING T_CHECK_INTERVAL time_value
{
	peer_ca_item->crl_url = $2;
	peer_ca_item->crl_file = $4;
	peer_ca_item->check_interval = $<number>6;

	config_peer_add_ca(cp, peer_ca_item);
}
	| T_CRL T_QSTRING T_LOCAL T_QSTRING 
{
	peer_ca_item->crl_url = $2;
	peer_ca_item->crl_file = $4;

	config_peer_add_ca(cp, peer_ca_item);
}
	;

peer_cert_spec:
	T_CERT peer_cert_item T_SEMICOLON
	;

peer_cert_item:
	T_TYPE_X509 T_QSTRING T_QSTRING
{
	peer_cert_item = cert_item_new();

	peer_cert_item->type = IKEV2_CERT_X509_SIGNATURE;	
	peer_cert_item->file = $2;
	peer_cert_item->priv_key_file = $3;

}
	peer_cert_item_url
	;

peer_cert_item_url:
{
	config_peer_add_cert(cp, peer_cert_item);
}
	| T_QSTRING
{
	peer_cert_item->url = $1;
	peer_cert_item->type = IKEV2_CERT_HASH_AND_URL;
	config_peer_add_cert(cp, peer_cert_item);
}
	;

radius_server:
	T_RADIUS_SERVER T_SEQ T_SEMICOLON
{
#ifdef RADIUS_CLIENT
	cp->radius_server = $2;
#else /* RADIUS_CLIENT */
	LOG_ERROR("RADIUS cofiguration parameters not compiled in!");
	YYABORT;
#endif /* RADIUS_CLIENT */
}
	;

wpa_conf:
	T_WPA_CONF T_QSTRING T_SEMICOLON
{
#ifdef SUPPLICANT
	cp->wpa_conf = $2;
#else /* SUPPLICANT */
	LOG_ERROR("WPA supplicant support not compiled in");
	YYABORT;
#endif /* SUPPLICANT */

}
	;

auth_method:
	T_AUTH_METHOD
{
	auth_methods = auth_counter = 0;
}
	auth_method_list T_SEMICOLON
{
	cp->auth_method = auth_methods;
}
	;

peer_auth_method:
	T_PEER_AUTH_METHOD
{
	auth_methods = auth_counter = 0;
}
	auth_method_list T_SEMICOLON
{
	cp->peer_auth_method = auth_methods;
}
	;

auth_method_list:
	auth_method_list T_COMMA auth_method_item
	| auth_method_item
	;

auth_method_item:
	T_PSK
{
	auth_methods |= IKEV2_AUTH_PSK << (auth_counter++ << 8);
}
	| T_RSA_SIG
{
	auth_methods |= IKEV2_AUTH_RSA << (auth_counter++ << 8);
}
	| T_DSS_SIG
{
	auth_methods |= IKEV2_AUTH_DSS << (auth_counter++ << 8);
}
	| T_ECDSS_SIG
{
	auth_methods |= IKEV2_AUTH_ECDSS << (auth_counter++ << 8);
}
	| T_EAP
{
	auth_methods |= IKEV2_AUTH_EAP << (auth_counter++ << 8);
}
	;

peer_my_identifier_item:
	T_MY_IDENTIFIER identifier T_SEMICOLON
{
	cp->lid = $<id>2;
}
	;

identifier:
	T_ID
{
	/*
	 * TODO: Add fetching of ID from certificate, or from
	 * EAP supplicant!
	 */
	id = id_new_from_str($1);
	if (!id) {
		LOG_ERROR("Unsupported ID %s in line %d", $1, line_number);
		YYABORT;
	}

	$<id>$ = id;
}
	;

sa_halfclosed_wait:
	SA_HALFCLOSED_WAIT time_value T_SEMICOLON
{
	cp->sa_halfclosed_wait = $<number>2;
}
	;

authlimits:
	T_AUTHLIMIT T_OBRACE
{
	limits = limits_new();

	cp->auth_limits = limits;
}
	limits_items T_EBRACE
	;

rekeylimits:
	T_REKEYLIMIT T_OBRACE
{
	limits = limits_new();

	cp->rekey_limits = limits;
}
	limits_items T_EBRACE
	;

limits_items:
	limits_items limits_item
	| limits_item
	;

limits_item:
	T_TIME T_NUMBER T_TIMEUNIT T_SEMICOLON
{
	limits->time = $2 * $3;
}
	| T_ALLOCS T_NUMBER T_SEMICOLON
{
	limits->allocs = $2;
}
	| T_OCTETS T_NUMBER T_OCTETUNIT T_SEMICOLON
{
	limits->octets = $2 * $3;
}
	;

ike_maxidle:
	T_IKE_MAX_IDLE time_value T_SEMICOLON
{
	cp->ike_max_idle = $<number>2;
}
	;

/*
 * Productions to parse sainfo section in peer sections
 */

sainfo_section: T_SAINFO
{
	LOG_DEBUG("Parsing 'sainfo' section");

	if ((config_csa = config_csa_new()) == NULL)
		YYABORT;

	config_peer_add_sainfo(cp, config_csa);
	config_csa->cfg_line = line_number;
}
	sainfo_ts_spec T_OBRACE sainfo_items T_EBRACE
	;

sainfo_ts_spec:
	sainfo_ts_spec_local sainfo_ts_spec_remote sainfo_ts_spec_proto
{
	ts_set_ip_proto_id($<ts_item>1, $<number>3);
	ts_set_ip_proto_id($<ts_item>2, $<number>3);
}
	;

sainfo_ts_spec_local:
	T_LOCAL ts_item
{
	ts = $<ts_item>2;
	config_csa->tsl = g_slist_append(NULL, ts);
}
	sainfo_ts_spec_port
{
	$<ts_item>$ = $<ts_item>2;
}
	;

sainfo_ts_spec_remote:
	T_REMOTE ts_item
{
	ts = $<ts_item>2;
	config_csa->tsr = g_slist_append(NULL, ts);
}
	sainfo_ts_spec_port
{
	$<ts_item>$ = $<ts_item>2;
}
	;

sainfo_ts_spec_port:
{
	$<number>$ = 0;
}
	| T_PORT T_NUMBER
{
	if ($2 < 0 || $2 > 65535) {
		YYCONFLOG("Illegal port value %d", $2);
		YYABORT;
	}

	ts_set_sport(ts, $2);
	ts_set_eport(ts, $2);
}
	| T_PORT T_NUMBER T_HYPHEN T_NUMBER
{
	if ($2 < 0 || $2 > 65535) {
		YYCONFLOG("Illegal port value %d", $2);
		YYABORT;
	}

	if ($4 < 0 || $4 > 65535) {
		YYCONFLOG("Illegal port value %d", $4);
		YYABORT;
	}

	ts_set_sport(ts, $2);
	ts_set_eport(ts, $4);
}
	| T_PORT T_SEQ
{
	struct servent *servent;

	if ((servent = getservbyname($2, NULL)) == NULL) {
		YYCONFLOG("Unknown protocol %s", $2);
		YYABORT;
	}

	ts_set_sport(ts, ntohs(servent->s_port));
	ts_set_eport(ts, ntohs(servent->s_port));
}
	| T_PORT T_ANY
{
	/*
	 * Nothing has to be done here, ANY is default...
	 */
}
	| T_PORT T_OPAQUE
{
	ts_set_sport(ts, 65535);
	ts_set_eport(ts, 0);
}
	;

sainfo_ts_spec_proto:
{
	$<number>$ = 0;
}
	| T_PROTO T_SEQ
{
	struct protoent *protoent;

	if ((protoent = getprotobyname($2)) == NULL) {
		YYCONFLOG("Unknown protocol %s", $2);
		YYABORT;
	}

	$<number>$ = protoent->p_proto;
}
	| T_PROTO T_NUMBER
{
	if ($2 < 0 || $2 > 255) {
		YYCONFLOG("Illegal protocol value %d", $2);
		YYABORT;
	}

	$<number>$ = $2;
}
	| T_PROTO T_ICMP
	;

sainfo_items:
	sainfo_items sainfo_item 
	| sainfo_item
	;

sainfo_item:
	softlimit
	| hardlimit
	| T_PROPOSAL_ENCR sainfo_enc_algorithms T_SEMICOLON
{
	config_csa->encr_algs = $<enc_algs>2;
}
	| T_PROPOSAL_AUTH sainfo_auth_algorithms T_SEMICOLON
{
	config_csa->auth_algs = $<auth_algs>2;
}
	| PFPFLAG pfpvalue T_SEMICOLON
	| T_IPSECLEVEL T_SEQ T_SEMICOLON
{
	if (!strcasecmp($2, "default"))
		config_csa->ipsec_level = IKEV2_PROTOCOL_ESP;
	else if (!strcasecmp($2, "use"))
		config_csa->ipsec_level = IKEV2_PROTOCOL_AH;
	else if (!strcasecmp($2, "require"))
		config_csa->ipsec_level = IKEV2_PROTOCOL_UNSPEC;
	else if (!strcasecmp($2, "unique"))
		config_csa->ipsec_level = IKEV2_PROTOCOL_UNSPEC;
	else {
		LOG_ERROR("Unknow ipsec level %s in line %u",
				$2, line_number);
		YYABORT;
	}
}
	| T_IPSECMODE T_SEQ T_SEMICOLON
{
	if (!strcasecmp($2, "tunnel"))
		config_csa->ipsec_mode = IPSEC_MODE_TUNNEL;
	else if (!strcasecmp($2, "transport"))
		config_csa->ipsec_mode = IPSEC_MODE_TRANSPORT;
#ifdef HAVE_BEET
	else if (!strcasecmp($2, "beet"))
		config_csa->ipsec_mode = IPSEC_MODE_BEET;
#endif /* HAVE_BEET */
	else if (!strcasecmp($2, "any"))
		config_csa->ipsec_mode = IPSEC_MODE_ANY;
	else {
		LOG_ERROR("Unknow ipsec mode %s in line %u",
				$2, line_number);
		YYABORT;
	}
}
	| T_IPSECPROTO T_SEQ T_SEMICOLON
{
	if (!strcasecmp($2, "esp"))
		config_csa->ipsec_proto = IKEV2_PROTOCOL_ESP;
	else if (!strcasecmp($2, "ah"))
		config_csa->ipsec_proto = IKEV2_PROTOCOL_AH;
	else if (!strcasecmp($2, "any"))
		config_csa->ipsec_proto = IKEV2_PROTOCOL_UNSPEC;
	else {
		LOG_ERROR("Unknow ipsec protocol %s in line %u",
				$2, line_number);
		YYABORT;
	}
}
	| T_SPDINSTALL T_SEMICOLON
{
	config_csa->status = CSA_INSTALL;
}
	| PFS pfs_value T_SEMICOLON
	| T_DH_GROUP sainfo_dh_groups T_SEMICOLON
	;

softlimit:
	T_SOFTLIMIT T_OBRACE
{
	limits = limits_new();

	config_csa->soft_limits = limits;
}
	limits_items T_EBRACE
	;

hardlimit:
	T_HARDLIMIT T_OBRACE
{
	limits = limits_new();

	config_csa->hard_limits = limits;
}
	limits_items T_EBRACE
	;

sainfo_dh_groups:
	sainfo_dh_group T_COMMA sainfo_dh_groups
	| sainfo_dh_group
	;

sainfo_dh_group:
	T_SEQ
{
	transform = NULL;

	if (strlen(yylval.string))
		transform = transform_find_by_name(IKEV2_TRANSFORM_DH_GROUP,
					TRANSFORM_IKE, yylval.string);

	if (!transform) {
		YYCONFLOG("Unsupported dh_group parameter (%s)",
			yylval.string);
		YYABORT;
	}

	proposal_transform_append(proposal, transform);
};

pfs_value:
	T_ON
{
	config_csa->pfs_flag = PFS_ON;
}
	| T_OFF
{
	config_csa->pfs_flag = PFS_OFF;
}
	| T_PASSIVE
{
	config_csa->pfs_flag = PFS_PASSIVE;
}
	;

pfpvalue: T_ON
	| T_OFF
	;

sainfo_enc_algorithms:
	sainfo_enc_algorithm T_COMMA sainfo_enc_algorithms
{
	$<enc_algs>$ = g_slist_append($<enc_algs>1, $<transform>2);
}
	| sainfo_enc_algorithm
{
	$<enc_algs>$ = g_slist_append(NULL, $<transform>1);
}
	;

sainfo_enc_algorithm:
	T_SEQ
{
	transform = NULL;

	if (strlen($1))
		transform = transform_find_by_name(IKEV2_TRANSFORM_ENCR,
					TRANSFORM_KERNEL, $1);

	if (!transform) {
		YYCONFLOG("Unsupported encryption_algorithm parameter (%s)",
			$1);
		YYABORT;
	}

	$<transform>$ = transform;
}
	;

sainfo_auth_algorithms:
	sainfo_auth_algorithms T_COMMA sainfo_auth_algorithm
{
	$<enc_algs>$ = g_slist_append($<enc_algs>1, $<transform>3);
}
	| sainfo_auth_algorithm
{
	$<enc_algs>$ = g_slist_append(NULL, $<transform>1);
}
	;

sainfo_auth_algorithm:
	T_SEQ
{
	transform = NULL;

	if (strlen($1))
		transform = transform_find_by_name(IKEV2_TRANSFORM_INTEGRITY,
					TRANSFORM_KERNEL, $1);

	if (!transform) {
		YYCONFLOG("Unsupported auth_algorithm parameter (%s)",
			$1);
		YYABORT;
	}

	$<transform>$ = transform;
};

%%


